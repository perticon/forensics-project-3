
void** g28;

int64_t quote();

void** fun_2700();

void fun_2a20();

signed char dev_debug = 0;

void** stderr = reinterpret_cast<void**>(0);

void fun_2a80(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void lbuf_putint(void** rdi, void** rsi, void** rdx, void** rcx, ...);

/* print_uintmaxes.part.0 */
void print_uintmaxes_part_0(void** rdi, void** rsi, void** rdx, void** rcx, ...);

void lbuf_putc(void** dil, ...);

void factor();

signed char print_exponents = 0;

void fun_2740();

void fun_2a50(void** rdi, void** rsi, int64_t rdx, void** rcx);

void** stdout = reinterpret_cast<void**>(0);

void fun_2800(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_27c0();

void** mp_factor(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_25e0(void** rdi, void** rsi, ...);

void fun_2940(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_29b0(int64_t rdi, void** rsi, void** rdx, void** rcx);

void fun_2ac0(void** rdi, void** rsi, void** rdx, void** rcx);

uint32_t print_factors(void** rdi, void** rsi, int64_t rdx, void** rcx) {
    void** r13_5;
    void* rsp6;
    void** rax7;
    void** v8;
    uint32_t eax9;
    uint64_t r12_10;
    void* rax11;
    void** r13_12;
    void** rdx13;
    int32_t eax14;
    void** rsi15;
    void** r14_16;
    void** rbp17;
    uint32_t eax18;
    int64_t rax19;
    void*** rdx20;
    void*** rbp21;
    uint64_t rdi22;
    void** rdx23;
    uint64_t rax24;
    uint32_t eax25;
    void** tmp64_26;
    void** rbp27;
    void* rax28;
    void** tmp64_29;
    int1_t zf30;
    void** r9_31;
    int1_t zf32;
    void** r9_33;
    void* rsp34;
    void** r12_35;
    void** rdx36;
    signed char v37;
    void** rbp38;
    uint32_t ebx39;
    signed char v40;
    unsigned char v41;
    void** v42;
    int1_t zf43;
    uint32_t eax44;
    unsigned char v45;
    void** rdi46;
    unsigned char v47;
    int64_t v48;
    void** v49;
    void** v50;
    void* rdx51;
    void** r14_52;
    void** rdi53;
    void** rdx54;
    void** rsp55;
    void** rdi56;
    void** rax57;
    void** rsi58;
    void** r13_59;
    void** r9_60;
    void** rax61;
    int64_t v62;
    void** v63;
    void** v64;
    void** r9_65;
    void** rdi66;
    void** rax67;
    void** r15_68;
    uint64_t rbp69;
    void* rbx70;
    int64_t* v71;
    void** rdi72;
    void** rax73;
    void** rdi74;
    void** v75;
    int1_t zf76;
    void** rdx77;
    void*** v78;
    void** rax79;
    void** v80;
    void** rdi81;
    uint64_t rdi82;
    void** v83;
    void** r9_84;
    uint64_t v85;

    r13_5 = rdi;
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x138);
    rax7 = g28;
    v8 = rax7;
    eax9 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi));
    if (*reinterpret_cast<signed char*>(&eax9) == 32) {
        do {
            eax9 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_5 + 1));
            ++r13_5;
        } while (*reinterpret_cast<signed char*>(&eax9) == 32);
    }
    *reinterpret_cast<int32_t*>(&r12_10) = 4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_10) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax11) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax9) == 43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    r13_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_5) + reinterpret_cast<uint64_t>(rax11));
    rdx13 = r13_12;
    do {
        eax14 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdx13));
        ++rdx13;
        if (!eax14) 
            break;
        *reinterpret_cast<int32_t*>(&r12_10) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_10) + 4) = 0;
    } while (eax14 - 48 <= 9);
    goto addr_623e_7;
    rsi15 = r13_12;
    *reinterpret_cast<int32_t*>(&r14_16) = 0;
    *reinterpret_cast<int32_t*>(&r14_16 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rbp17) = 0;
    *reinterpret_cast<int32_t*>(&rbp17 + 4) = 0;
    if (*reinterpret_cast<int32_t*>(&r12_10)) {
        addr_623e_7:
        quote();
        fun_2700();
        fun_2a20();
        eax18 = 0;
    } else {
        do {
            *reinterpret_cast<int32_t*>(&rax19) = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rsi15));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax19) + 4) = 0;
            ++rsi15;
            if (!*reinterpret_cast<int32_t*>(&rax19)) 
                break;
            *reinterpret_cast<uint32_t*>(&rcx) = static_cast<uint32_t>(rax19 - 48);
            *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
        } while (reinterpret_cast<unsigned char>(rbp17) <= reinterpret_cast<unsigned char>(0x1999999999999999) && (rdx20 = reinterpret_cast<void***>(r14_16 + reinterpret_cast<unsigned char>(r14_16) * 4), rbp21 = reinterpret_cast<void***>(rbp17 + reinterpret_cast<unsigned char>(rbp17) * 4), rdi22 = reinterpret_cast<unsigned char>(r14_16) >> 61, rdx23 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdx20) + reinterpret_cast<uint64_t>(rdx20)), rax24 = reinterpret_cast<unsigned char>(r14_16) >> 63, eax25 = *reinterpret_cast<int32_t*>(&rax24) + *reinterpret_cast<int32_t*>(&rdi22) + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx23) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r14_16) + reinterpret_cast<unsigned char>(r14_16))), tmp64_26 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx23) + reinterpret_cast<unsigned char>(rcx)), rdx13 = tmp64_26, rbp27 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbp21) + reinterpret_cast<uint64_t>(rbp21)), r14_16 = rdx13, *reinterpret_cast<uint32_t*>(&rcx) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_26) < reinterpret_cast<unsigned char>(rdx23)), *reinterpret_cast<int32_t*>(&rcx + 4) = 0, *reinterpret_cast<uint32_t*>(&rax28) = eax25 + *reinterpret_cast<uint32_t*>(&rcx), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax28) + 4) = 0, tmp64_29 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp27) + reinterpret_cast<uint64_t>(rax28)), rbp17 = tmp64_29, reinterpret_cast<unsigned char>(tmp64_29) >= reinterpret_cast<unsigned char>(rbp27)));
        goto addr_62ff_12;
        if (reinterpret_cast<signed char>(rbp17) < reinterpret_cast<signed char>(0)) {
            addr_62ff_12:
            zf30 = dev_debug == 0;
            if (!zf30) {
                rcx = stderr;
                fun_2a80("[using arbitrary-precision arithmetic] ", 1, 39, rcx, 0x1999999999999999, r9_31);
                rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                goto addr_630c_15;
            }
        } else {
            zf32 = dev_debug == 0;
            if (!zf32) {
                rcx = stderr;
                *reinterpret_cast<int32_t*>(&rdx13) = 36;
                *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
                fun_2a80("[using single-precision arithmetic] ", 1, 36, rcx, 0x1999999999999999, r9_33);
                rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
            }
            if (!rbp17) {
                lbuf_putint(r14_16, 0, rdx13, rcx);
                rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
            } else {
                print_uintmaxes_part_0(rbp17, r14_16, rdx13, rcx);
                rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
            }
            r12_35 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp34) + 32);
            lbuf_putc(58, 58);
            rdx36 = r12_35;
            factor();
            if (v37) {
                rbp38 = r12_35;
                while (1) {
                    ebx39 = 0;
                    if (!v40) {
                        addr_654c_24:
                        ++rbp38;
                        *reinterpret_cast<uint32_t*>(&rdx36) = v41;
                        *reinterpret_cast<int32_t*>(&rdx36 + 4) = 0;
                        if (*reinterpret_cast<uint32_t*>(&rdx36) > reinterpret_cast<uint32_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rbp38)) - reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&r12_35)))) 
                            continue; else 
                            break;
                    } else {
                        do {
                            lbuf_putc(32, 32);
                            lbuf_putint(v42, 0, rdx36, rcx);
                            zf43 = print_exponents == 0;
                            eax44 = v45;
                            if (zf43) 
                                continue;
                            if (*reinterpret_cast<unsigned char*>(&eax44) > 1) 
                                break;
                            ++ebx39;
                        } while (ebx39 < eax44);
                        goto addr_654c_24;
                    }
                    lbuf_putc(94);
                    *reinterpret_cast<uint32_t*>(&rdi46) = v47;
                    *reinterpret_cast<int32_t*>(&rdi46 + 4) = 0;
                    lbuf_putint(rdi46, 0, rdx36, rcx);
                    goto addr_654c_24;
                }
            }
            if (v48) {
                lbuf_putc(32, 32);
                if (!v49) {
                    lbuf_putint(v50, 0, rdx36, rcx);
                } else {
                    print_uintmaxes_part_0(v49, v50, rdx36, rcx);
                }
            }
            lbuf_putc(10, 10);
            eax18 = 1;
        }
    }
    addr_626c_35:
    rdx51 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
    if (rdx51) {
        fun_2740();
    } else {
        return eax18;
    }
    addr_630c_15:
    r14_52 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 32);
    fun_2a50(r14_52, r13_12, 10, rcx);
    rdi53 = stdout;
    rdx54 = r14_52;
    fun_2800(rdi53, 10, rdx54, rcx);
    rsp55 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) - 8 + 8 - 8 + 8);
    rdi56 = stdout;
    rax57 = *reinterpret_cast<void***>(rdi56 + 40);
    if (reinterpret_cast<unsigned char>(rax57) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi56 + 48))) {
        fun_27c0();
        rsp55 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp55 - 8) + 8);
    } else {
        rdx54 = rax57 + 1;
        *reinterpret_cast<void***>(rdi56 + 40) = rdx54;
        *reinterpret_cast<void***>(rax57) = reinterpret_cast<void**>(58);
    }
    rsi58 = rsp55;
    *reinterpret_cast<int32_t*>(&r13_59) = 0;
    *reinterpret_cast<int32_t*>(&r13_59 + 4) = 0;
    mp_factor(r14_52, rsi58, rdx54, rcx, 0x1999999999999999, r9_60);
    *reinterpret_cast<int32_t*>(&rax61) = 0;
    *reinterpret_cast<int32_t*>(&rax61 + 4) = 0;
    if (!v62) {
        addr_645c_42:
        fun_25e0(v63, rsi58, v63, rsi58);
        fun_25e0(v64, rsi58, v64, rsi58);
        fun_2940(r14_52, rsi58, rdx54, rcx, 0x1999999999999999, r9_65);
        rdi66 = stdout;
        rax67 = *reinterpret_cast<void***>(rdi66 + 40);
        if (reinterpret_cast<unsigned char>(rax67) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi66 + 48))) {
            *reinterpret_cast<int32_t*>(&rsi58) = 10;
            *reinterpret_cast<int32_t*>(&rsi58 + 4) = 0;
            fun_27c0();
        } else {
            rdx54 = rax67 + 1;
            *reinterpret_cast<void***>(rdi66 + 40) = rdx54;
            *reinterpret_cast<void***>(rax67) = reinterpret_cast<void**>(10);
        }
    } else {
        do {
            *reinterpret_cast<int32_t*>(&r15_68) = 0;
            *reinterpret_cast<int32_t*>(&r15_68 + 4) = 0;
            rbp69 = reinterpret_cast<unsigned char>(rax61) * 8;
            rbx70 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax61) << 4);
            if (v71[reinterpret_cast<unsigned char>(rax61)]) {
                do {
                    rdi72 = stdout;
                    rax73 = *reinterpret_cast<void***>(rdi72 + 40);
                    if (reinterpret_cast<unsigned char>(rax73) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi72 + 48))) {
                        *reinterpret_cast<void***>(rdi72 + 40) = rax73 + 1;
                        *reinterpret_cast<void***>(rax73) = reinterpret_cast<void**>(32);
                    } else {
                        fun_27c0();
                    }
                    rdi74 = stdout;
                    *reinterpret_cast<int32_t*>(&rsi58) = 10;
                    *reinterpret_cast<int32_t*>(&rsi58 + 4) = 0;
                    fun_2800(rdi74, 10, reinterpret_cast<unsigned char>(v75) + reinterpret_cast<uint64_t>(rbx70), rcx);
                    zf76 = print_exponents == 0;
                    rdx77 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(v78) + rbp69);
                    if (zf76) 
                        continue;
                    if (reinterpret_cast<unsigned char>(rdx77) > reinterpret_cast<unsigned char>(1)) 
                        break;
                    *reinterpret_cast<int32_t*>(&rax79) = static_cast<int32_t>(reinterpret_cast<uint64_t>(r15_68 + 1));
                    *reinterpret_cast<int32_t*>(&rax79 + 4) = 0;
                    r15_68 = rax79;
                } while (reinterpret_cast<unsigned char>(rax79) < reinterpret_cast<unsigned char>(rdx77));
                continue;
            } else {
                continue;
            }
            rsi58 = reinterpret_cast<void**>("^%lu");
            fun_29b0(1, "^%lu", rdx77, rcx);
            rdx54 = v80;
            *reinterpret_cast<int32_t*>(&rax61) = static_cast<int32_t>(reinterpret_cast<uint64_t>(r13_59 + 1));
            *reinterpret_cast<int32_t*>(&rax61 + 4) = 0;
            r13_59 = rax61;
        } while (reinterpret_cast<unsigned char>(rax61) < reinterpret_cast<unsigned char>(rdx54));
        if (!rdx54) 
            goto addr_645c_42; else 
            goto addr_6435_57;
    }
    rdi81 = stdout;
    fun_2ac0(rdi81, rsi58, rdx54, rcx);
    eax18 = 1;
    goto addr_626c_35;
    addr_6435_57:
    *reinterpret_cast<int32_t*>(&rdi82) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi82) + 4) = 0;
    do {
        fun_2940((rdi82 << 4) + reinterpret_cast<unsigned char>(v83), rsi58, rdx54, rcx, 0x1999999999999999, r9_84);
        *reinterpret_cast<int32_t*>(&rdi82) = static_cast<int32_t>(r12_10 + 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi82) + 4) = 0;
        r12_10 = rdi82;
    } while (rdi82 < v85);
    goto addr_645c_42;
}

void fun_2810(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, ...);

void** gd004 = reinterpret_cast<void**>(0x73);

void** gd00c = reinterpret_cast<void**>(0x6f);

/* __PRETTY_FUNCTION__.6 */
void** __PRETTY_FUNCTION___6 = reinterpret_cast<void**>(0x6d);

void** gd5a0 = reinterpret_cast<void**>(0);

void** g3e5 = reinterpret_cast<void**>(0xa0);

void** gd1d0 = reinterpret_cast<void**>(40);

uint64_t g3ed = 0x331c8bf239f54162;

void** mulredc2(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, void** a8) {
    int64_t rbp9;
    uint64_t r12_10;
    uint64_t rcx11;
    uint64_t rcx12;
    uint64_t tmp64_13;
    uint64_t tmp64_14;
    uint64_t tmp64_15;
    uint64_t r13_16;
    uint64_t tmp64_17;
    void* rcx18;
    void* tmp64_19;
    void** rcx20;
    void** tmp64_21;
    void** tmp64_22;
    void** tmp64_23;
    void** rsi24;
    void** rdx25;
    void** rax26;
    void** r12_27;
    void** rbx28;
    void** rax29;
    void** r8_30;
    void** r10_31;
    void** r14_32;
    void** r13_33;
    void** v34;
    int32_t ebx35;
    void** v36;
    void** rbp37;
    void** rax38;
    void** rax39;
    void** rax40;
    uint64_t rbx41;
    void** rax42;
    void** rax43;
    void** rax44;
    int64_t v45;

    rbp9 = reinterpret_cast<int64_t>(-reinterpret_cast<unsigned char>(a8));
    if (reinterpret_cast<signed char>(rsi) < reinterpret_cast<signed char>(0)) {
        addr_3217_2:
        fun_2810("(a1 >> (W_TYPE_SIZE - 1)) == 0", "src/factor.c", 0x3e5, "mulredc2", r8, r9);
    } else {
        if (reinterpret_cast<signed char>(rcx) < reinterpret_cast<signed char>(0)) {
            addr_31f8_4:
            fun_2810("(b1 >> (W_TYPE_SIZE - 1)) == 0", "src/factor.c", 0x3e6, "mulredc2", r8, r9);
            goto addr_3217_2;
        } else {
            if (reinterpret_cast<signed char>(r9) < reinterpret_cast<signed char>(0)) {
                fun_2810("(m1 >> (W_TYPE_SIZE - 1)) == 0", "src/factor.c", 0x3e7, "mulredc2", r8, r9);
                goto addr_31f8_4;
            } else {
                r12_10 = reinterpret_cast<unsigned char>(rdx) * reinterpret_cast<unsigned char>(r8);
                rcx11 = reinterpret_cast<unsigned char>(rdx) * reinterpret_cast<unsigned char>(rcx);
                rcx12 = rcx11 - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(rcx11 < 1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(r12_10 < 1)))));
                tmp64_13 = rcx12 + __intrinsic();
                tmp64_14 = tmp64_13 + __intrinsic();
                tmp64_15 = tmp64_14 + rbp9 * r12_10 * reinterpret_cast<unsigned char>(r9);
                r13_16 = reinterpret_cast<unsigned char>(rsi) * reinterpret_cast<unsigned char>(r8);
                tmp64_17 = r13_16 + tmp64_15;
                rcx18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rsi) * reinterpret_cast<unsigned char>(rcx));
                tmp64_19 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx18) + (reinterpret_cast<int64_t>(__intrinsic()) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(tmp64_13 < rcx12)) + reinterpret_cast<uint1_t>(tmp64_14 < tmp64_13) + reinterpret_cast<uint64_t>(__intrinsic()) + reinterpret_cast<uint1_t>(tmp64_15 < tmp64_14)));
                rcx20 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(tmp64_19) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(tmp64_19) < 1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(tmp64_17 < 1))))));
                tmp64_21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx20) + reinterpret_cast<uint64_t>(__intrinsic()));
                tmp64_22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_21) + (reinterpret_cast<int64_t>(__intrinsic()) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(tmp64_17 < r13_16))));
                tmp64_23 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_22) + rbp9 * tmp64_17 * reinterpret_cast<unsigned char>(r9));
                rsi24 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__intrinsic()) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(tmp64_19) < reinterpret_cast<uint64_t>(rcx18))) + (reinterpret_cast<unsigned char>(tmp64_21) < reinterpret_cast<unsigned char>(rcx20)) + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_22) < reinterpret_cast<unsigned char>(tmp64_21)) + reinterpret_cast<uint64_t>(__intrinsic()) + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_23) < reinterpret_cast<unsigned char>(tmp64_22)));
                rdx25 = rsi24;
                rax26 = tmp64_23;
                if (reinterpret_cast<unsigned char>(r9) < reinterpret_cast<unsigned char>(rsi24)) 
                    goto addr_31ac_8;
                if (r9 != rsi24) 
                    goto addr_31b8_10; else 
                    goto addr_31d2_11;
            }
        }
    }
    r12_27 = r8;
    rbx28 = *reinterpret_cast<void***>(r9);
    rax29 = *reinterpret_cast<void***>(r9 + 8);
    r8_30 = gd004;
    r10_31 = gd00c;
    r14_32 = __PRETTY_FUNCTION___6;
    r13_33 = gd5a0;
    v34 = rbx28;
    ebx35 = 64;
    v36 = rax29;
    rbp37 = g3e5;
    do {
        if (*reinterpret_cast<unsigned char*>(&rbp37) & 1) {
            rax38 = mulredc2("(a1 >> (W_TYPE_SIZE - 1)) == 0", v36, v34, r10_31, r8_30, r13_33, r14_32, r12_27);
            v34 = rax38;
            rax39 = gd1d0;
            v36 = rax39;
            r8_30 = r8_30;
            r10_31 = r10_31;
        }
        rbp37 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp37) >> 1);
        rax40 = mulredc2("(a1 >> (W_TYPE_SIZE - 1)) == 0", r10_31, r8_30, r10_31, r8_30, r13_33, r14_32, r12_27);
        r10_31 = gd1d0;
        r8_30 = rax40;
        --ebx35;
    } while (ebx35);
    rbx41 = g3ed;
    if (rbx41) {
        do {
            if (*reinterpret_cast<unsigned char*>(&rbx41) & 1) {
                rax42 = mulredc2("(a1 >> (W_TYPE_SIZE - 1)) == 0", v36, v34, r10_31, r8_30, r13_33, r14_32, r12_27);
                v34 = rax42;
                rax43 = gd1d0;
                v36 = rax43;
                r8_30 = r8_30;
                r10_31 = r10_31;
            }
            rax44 = mulredc2("(a1 >> (W_TYPE_SIZE - 1)) == 0", r10_31, r8_30, r10_31, r8_30, r13_33, r14_32, r12_27);
            rbx41 = rbx41 >> 1;
            r10_31 = gd1d0;
            r8_30 = rax44;
        } while (rbx41);
    }
    gd1d0 = v36;
    goto v45;
    addr_31b8_10:
    *reinterpret_cast<void***>(rdi) = rdx25;
    return rax26;
    addr_31d2_11:
    if (reinterpret_cast<unsigned char>(a7) <= reinterpret_cast<unsigned char>(tmp64_23)) {
        addr_31ac_8:
        rdx25 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi24) - (reinterpret_cast<unsigned char>(r9) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rsi24) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r9) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_23) < reinterpret_cast<unsigned char>(a7))))))));
        rax26 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_23) - reinterpret_cast<unsigned char>(a7));
        goto addr_31b8_10;
    } else {
        goto addr_31b8_10;
    }
}

void** gcd_odd(void** rdi, void** rsi) {
    void** rax3;
    uint64_t rax4;
    void** rdx5;
    void* rdx6;
    uint64_t rcx7;

    rax3 = rsi;
    if (!(*reinterpret_cast<unsigned char*>(&rsi) & 1)) {
        rax3 = rdi;
        rdi = rsi;
    }
    if (!rdi) {
        return rax3;
    } else {
        rax4 = reinterpret_cast<unsigned char>(rax3) >> 1;
        while (1) {
            rdx5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) >> 1);
            if (*reinterpret_cast<unsigned char*>(&rdi) & 1) {
                rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx5) - rax4);
                if (!rdx6) 
                    break;
                rcx7 = reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rdx6) >> 63);
                rax4 = rax4 + (reinterpret_cast<uint64_t>(rdx6) & rcx7);
                rdx5 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rdx6) ^ rcx7) - rcx7);
            }
            rdi = rdx5;
        }
        return reinterpret_cast<unsigned char>(rdi) | 1;
    }
}

void** umaxtostr(void** rdi, void* rsi, ...);

void** g150f8 = reinterpret_cast<void**>(0);

struct s0 {
    unsigned char f0;
    void** f1;
};

unsigned char millerrabin(void** rdi, struct s0* rsi, void** rdx, struct s0* rcx, uint32_t r8d, void** r9);

void** fun_28d0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, ...);

void** xrealloc(void** rdi, void** rsi, ...);

void fun_2ab0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, ...);

void fun_27a0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, ...);

int32_t fun_2a00(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, ...);

/* print_uintmaxes.part.0 */
void print_uintmaxes_part_0(void** rdi, void** rsi, void** rdx, void** rcx, ...) {
    void* rsp5;
    void** r10_6;
    void** rbp7;
    int32_t r9d8;
    void** r8_9;
    void** rsi10;
    void** rdx11;
    void** rdi12;
    void* rsp13;
    void** rax14;
    void** v15;
    void* rbx16;
    void** rax17;
    void** rcx18;
    void** rsi19;
    void* rdx20;
    void** rbx21;
    void* rcx22;
    void** rax23;
    uint64_t rdx24;
    void* rdx25;
    void* rdi26;
    void** rax27;
    void* rax28;
    void** r13_29;
    void** r12_30;
    void** rcx31;
    void** rdx32;
    struct s0* r14_33;
    void** v34;
    void** v35;
    void** v36;
    void** r14_37;
    void** v38;
    void** rbx39;
    void** rax40;
    void** rsi41;
    void** v42;
    void** rax43;
    void** rdi44;
    void** r15_45;
    void** rdi46;
    int64_t v47;
    struct s0* rbp48;
    struct s0* rbx49;
    void** r15_50;
    int32_t eax51;
    uint1_t zf52;
    int64_t v53;
    void** r15_54;
    void** rdx55;
    void** rax56;
    void** rsi57;
    void** rax58;
    void** rax59;
    void** rax60;
    void* v61;
    void** rax62;
    void** r14_63;
    void** rdi64;
    void** r15_65;
    void** r14_66;
    void** rax67;
    void** rsi68;
    void** rax69;
    void** rax70;

    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    r10_6 = rdi;
    rbp7 = rsi;
    r9d8 = 64;
    *reinterpret_cast<int32_t*>(&r8_9) = 0;
    *reinterpret_cast<int32_t*>(&r8_9 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rsi10) = 0x3b9aca00;
    *reinterpret_cast<int32_t*>(&rsi10 + 4) = 0;
    rdx11 = rdi - (__intrinsic() >> 11) * 0x3b9aca00;
    do {
        r8_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_9) + reinterpret_cast<unsigned char>(r8_9));
        rsi10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi10) >> 1);
        if (reinterpret_cast<unsigned char>(rsi10) < reinterpret_cast<unsigned char>(rdx11) || rsi10 == rdx11 && reinterpret_cast<unsigned char>(0) <= reinterpret_cast<unsigned char>(rbp7)) {
            ++r8_9;
            rdx11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx11) - (reinterpret_cast<unsigned char>(rsi10) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx11) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rsi10) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp7) < reinterpret_cast<unsigned char>(0))))))));
            rbp7 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(rbp7)));
        }
        --r9d8;
    } while (r9d8);
    if (reinterpret_cast<unsigned char>(r10_6) <= reinterpret_cast<unsigned char>(0x3b9ac9ff)) {
        lbuf_putint(r8_9, 0, rdx11, 0);
        rdi12 = rbp7;
        rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8 + 8);
    } else {
        print_uintmaxes_part_0(reinterpret_cast<unsigned char>(__intrinsic()) >> 11, r8_9, __intrinsic(), 0);
        rdi12 = rbp7;
        rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8 + 8);
    }
    rax14 = g28;
    v15 = rax14;
    rbx16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 - 8 - 40);
    rax17 = umaxtostr(rdi12, rbx16);
    rcx18 = g150f8;
    rsi19 = rax17;
    rdx20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax17) - reinterpret_cast<uint64_t>(rbx16));
    rbx21 = reinterpret_cast<void**>(20 - reinterpret_cast<uint64_t>(rdx20));
    if (reinterpret_cast<unsigned char>(rbx21) < reinterpret_cast<unsigned char>(9)) {
        rcx22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx20) + reinterpret_cast<unsigned char>(9) + 0xffffffffffffffec);
        rax23 = g150f8;
        if (reinterpret_cast<uint64_t>(rcx22) < 8) {
            if (*reinterpret_cast<unsigned char*>(&rcx22) & 4) {
                *reinterpret_cast<void***>(rax23) = reinterpret_cast<void**>(0x30303030);
                *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(rax23) + reinterpret_cast<uint64_t>(rcx22) + 0xfffffffffffffffc) = 0x30303030;
            } else {
                if (rcx22 && (*reinterpret_cast<void***>(rax23) = reinterpret_cast<void**>(48), !!(*reinterpret_cast<unsigned char*>(&rcx22) & 2))) {
                    *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(rax23) + reinterpret_cast<uint64_t>(rcx22) + 0xfffffffffffffffe) = reinterpret_cast<int16_t>(millerrabin);
                }
            }
        } else {
            *reinterpret_cast<void***>(rax23) = reinterpret_cast<void**>(0x3030303030303030);
            r8_9 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax23 + 8) & 0xfffffffffffffff8);
            *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rax23) + reinterpret_cast<uint64_t>(rcx22) + 0xfffffffffffffff8) = reinterpret_cast<void**>(0x3030303030303030);
            rdx24 = reinterpret_cast<unsigned char>(rax23) - reinterpret_cast<unsigned char>(r8_9) + reinterpret_cast<uint64_t>(rcx22) & 0xfffffffffffffff8;
            if (rdx24 >= 8) {
                *reinterpret_cast<int32_t*>(&rdx25) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx25) + 4) = 0;
                rdi26 = reinterpret_cast<void*>(rdx24 & 0xfffffffffffffff8);
                do {
                    *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r8_9) + reinterpret_cast<uint64_t>(rdx25)) = reinterpret_cast<void**>(0x3030303030303030);
                    rdx25 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx25) + 8);
                } while (reinterpret_cast<uint64_t>(rdx25) < reinterpret_cast<uint64_t>(rdi26));
            }
        }
        rcx18 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rcx22) + reinterpret_cast<unsigned char>(rax23));
    }
    rax27 = fun_28d0(rcx18, rsi19, rbx21, rcx18, r8_9);
    g150f8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax27) + reinterpret_cast<unsigned char>(rbx21));
    rax28 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(g28));
    if (!rax28) {
        return;
    }
    fun_2740();
    r13_29 = rcx18;
    r12_30 = rsi19;
    rcx31 = *reinterpret_cast<void***>(rcx18);
    rdx32 = *reinterpret_cast<void***>(rcx18 + 8);
    r14_33 = reinterpret_cast<struct s0*>(*reinterpret_cast<void***>(rcx18 + 16) + 0xffffffffffffffff);
    v34 = *reinterpret_cast<void***>(rcx18 + 16);
    v35 = rcx31;
    v36 = rdx32;
    if (reinterpret_cast<int64_t>(r14_33) >= reinterpret_cast<int64_t>(0)) 
        goto addr_361b_22;
    r14_37 = v34 + 1;
    v38 = r14_37;
    rbx39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_37) << 4);
    rax40 = xrealloc(v35, rbx39);
    rsi41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_37) * 8);
    v42 = rax40;
    rax43 = xrealloc(v36, rsi41);
    rdi44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax40) + reinterpret_cast<unsigned char>(rbx39) + 0xfffffffffffffff0);
    r15_45 = rax43;
    fun_2ab0(rdi44, rsi41, rdx32, rcx31, r8_9);
    rdi46 = rdi44;
    addr_370b_24:
    fun_27a0(rdi46, r12_30, rdx32, rcx31, r8_9);
    *reinterpret_cast<void***>(r15_45 + reinterpret_cast<unsigned char>(v34) * 8) = reinterpret_cast<void**>(1);
    *reinterpret_cast<void***>(r13_29 + 8) = r15_45;
    *reinterpret_cast<void***>(r13_29) = v42;
    *reinterpret_cast<void***>(r13_29 + 16) = v38;
    goto v47;
    addr_361b_22:
    rbp48 = r14_33;
    rbx49 = r14_33;
    rdx32 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r14_33) << 4);
    r15_50 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx31) + reinterpret_cast<unsigned char>(rdx32));
    do {
        eax51 = fun_2a00(r15_50, r12_30, rdx32, rcx31, r8_9);
        zf52 = reinterpret_cast<uint1_t>(eax51 == 0);
        if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(eax51 < 0) | zf52)) 
            break;
        rbx49 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbx49) - 1);
        r15_50 = r15_50 - 16;
    } while (rbx49 != 0xffffffffffffffff);
    goto addr_3670_27;
    if (zf52) {
        *reinterpret_cast<void***>(v36 + reinterpret_cast<uint64_t>(rbx49) * 8) = *reinterpret_cast<void***>(v36 + reinterpret_cast<uint64_t>(rbx49) * 8) + 1;
        goto v53;
    }
    r15_54 = v34 + 1;
    v38 = r15_54;
    rdx55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_54) << 4);
    rax56 = xrealloc(v35, rdx55);
    rsi57 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_54) * 8);
    v42 = rax56;
    rax58 = xrealloc(v36, rsi57);
    rdx32 = rdx55;
    r15_45 = rax58;
    rax59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v42) + reinterpret_cast<unsigned char>(rdx32) + 0xfffffffffffffff0);
    fun_2ab0(rax59, rsi57, rdx32, rcx31, r8_9);
    rax60 = reinterpret_cast<void**>(&rbx49->f1);
    v34 = rax60;
    v61 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax60) << 4);
    rax62 = rax59;
    if (reinterpret_cast<int64_t>(rbx49) < reinterpret_cast<int64_t>(r14_33)) {
        addr_36da_31:
        r14_63 = rax62;
    } else {
        goto addr_3701_33;
    }
    do {
        rdi64 = r14_63;
        r14_63 = r14_63 - 16;
        fun_27a0(rdi64, r14_63, rdx32, rcx31, r8_9);
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r15_45 + reinterpret_cast<uint64_t>(rbp48) * 8) + 8) = *reinterpret_cast<void***>(r15_45 + reinterpret_cast<uint64_t>(rbp48) * 8);
        rbp48 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbp48) - 1);
    } while (reinterpret_cast<int64_t>(rbx49) < reinterpret_cast<int64_t>(rbp48));
    addr_3701_33:
    rdi46 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(v61) + reinterpret_cast<unsigned char>(v42));
    goto addr_370b_24;
    addr_3670_27:
    r15_65 = v34 + 1;
    v38 = r15_65;
    r14_66 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_65) << 4);
    rax67 = xrealloc(v35, r14_66);
    rsi68 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_65) * 8);
    v42 = rax67;
    rax69 = xrealloc(v36, rsi68);
    r15_45 = rax69;
    rax70 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v42) + reinterpret_cast<unsigned char>(r14_66) + 0xfffffffffffffff0);
    fun_2ab0(rax70, rsi68, rdx32, rcx31, r8_9);
    rax62 = rax70;
    v61 = reinterpret_cast<void*>(0);
    v34 = reinterpret_cast<void**>(0);
    goto addr_36da_31;
}

void lbuf_putint(void** rdi, void** rsi, void** rdx, void** rcx, ...) {
    void** rax5;
    void** v6;
    void* rbx7;
    void** rax8;
    void** rcx9;
    void** rsi10;
    void* rdx11;
    void** rbx12;
    void* rcx13;
    void** rax14;
    void** r8_15;
    uint64_t rdx16;
    void* rdx17;
    void* rdi18;
    void** rax19;
    void* rax20;
    void** r13_21;
    void** r12_22;
    void** rcx23;
    void** rdx24;
    struct s0* r14_25;
    void** v26;
    void** v27;
    void** v28;
    void** r14_29;
    void** v30;
    void** rbx31;
    void** rax32;
    void** rsi33;
    void** v34;
    void** rax35;
    void** rdi36;
    void** r15_37;
    void** rdi38;
    int64_t v39;
    struct s0* rbp40;
    struct s0* rbx41;
    void** r15_42;
    int32_t eax43;
    uint1_t zf44;
    int64_t v45;
    void** r15_46;
    void** rdx47;
    void** rax48;
    void** rsi49;
    void** rax50;
    void** rax51;
    void** rax52;
    void* v53;
    void** rax54;
    void** r14_55;
    void** rdi56;
    void** r15_57;
    void** r14_58;
    void** rax59;
    void** rsi60;
    void** rax61;
    void** rax62;

    rax5 = g28;
    v6 = rax5;
    rbx7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 40);
    rax8 = umaxtostr(rdi, rbx7);
    rcx9 = g150f8;
    rsi10 = rax8;
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<uint64_t>(rbx7));
    rbx12 = reinterpret_cast<void**>(20 - reinterpret_cast<uint64_t>(rdx11));
    if (reinterpret_cast<unsigned char>(rbx12) < reinterpret_cast<unsigned char>(rsi)) {
        rcx13 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx11) + reinterpret_cast<unsigned char>(rsi) + 0xffffffffffffffec);
        rax14 = g150f8;
        if (reinterpret_cast<uint64_t>(rcx13) < 8) {
            if (*reinterpret_cast<unsigned char*>(&rcx13) & 4) {
                *reinterpret_cast<void***>(rax14) = reinterpret_cast<void**>(0x30303030);
                *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(rax14) + reinterpret_cast<uint64_t>(rcx13) + 0xfffffffffffffffc) = 0x30303030;
            } else {
                if (rcx13 && (*reinterpret_cast<void***>(rax14) = reinterpret_cast<void**>(48), !!(*reinterpret_cast<unsigned char*>(&rcx13) & 2))) {
                    *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(rax14) + reinterpret_cast<uint64_t>(rcx13) + 0xfffffffffffffffe) = reinterpret_cast<int16_t>(millerrabin);
                }
            }
        } else {
            *reinterpret_cast<void***>(rax14) = reinterpret_cast<void**>(0x3030303030303030);
            r8_15 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax14 + 8) & 0xfffffffffffffff8);
            *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rax14) + reinterpret_cast<uint64_t>(rcx13) + 0xfffffffffffffff8) = reinterpret_cast<void**>(0x3030303030303030);
            rdx16 = reinterpret_cast<unsigned char>(rax14) - reinterpret_cast<unsigned char>(r8_15) + reinterpret_cast<uint64_t>(rcx13) & 0xfffffffffffffff8;
            if (rdx16 >= 8) {
                *reinterpret_cast<int32_t*>(&rdx17) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx17) + 4) = 0;
                rdi18 = reinterpret_cast<void*>(rdx16 & 0xfffffffffffffff8);
                do {
                    *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r8_15) + reinterpret_cast<uint64_t>(rdx17)) = reinterpret_cast<void**>(0x3030303030303030);
                    rdx17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx17) + 8);
                } while (reinterpret_cast<uint64_t>(rdx17) < reinterpret_cast<uint64_t>(rdi18));
            }
        }
        rcx9 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rcx13) + reinterpret_cast<unsigned char>(rax14));
    }
    rax19 = fun_28d0(rcx9, rsi10, rbx12, rcx9, r8_15);
    g150f8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax19) + reinterpret_cast<unsigned char>(rbx12));
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v6) - reinterpret_cast<unsigned char>(g28));
    if (!rax20) {
        return;
    }
    fun_2740();
    r13_21 = rcx9;
    r12_22 = rsi10;
    rcx23 = *reinterpret_cast<void***>(rcx9);
    rdx24 = *reinterpret_cast<void***>(rcx9 + 8);
    r14_25 = reinterpret_cast<struct s0*>(*reinterpret_cast<void***>(rcx9 + 16) + 0xffffffffffffffff);
    v26 = *reinterpret_cast<void***>(rcx9 + 16);
    v27 = rcx23;
    v28 = rdx24;
    if (reinterpret_cast<int64_t>(r14_25) >= reinterpret_cast<int64_t>(0)) 
        goto addr_361b_15;
    r14_29 = v26 + 1;
    v30 = r14_29;
    rbx31 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_29) << 4);
    rax32 = xrealloc(v27, rbx31);
    rsi33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_29) * 8);
    v34 = rax32;
    rax35 = xrealloc(v28, rsi33);
    rdi36 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax32) + reinterpret_cast<unsigned char>(rbx31) + 0xfffffffffffffff0);
    r15_37 = rax35;
    fun_2ab0(rdi36, rsi33, rdx24, rcx23, r8_15);
    rdi38 = rdi36;
    addr_370b_17:
    fun_27a0(rdi38, r12_22, rdx24, rcx23, r8_15);
    *reinterpret_cast<void***>(r15_37 + reinterpret_cast<unsigned char>(v26) * 8) = reinterpret_cast<void**>(1);
    *reinterpret_cast<void***>(r13_21 + 8) = r15_37;
    *reinterpret_cast<void***>(r13_21) = v34;
    *reinterpret_cast<void***>(r13_21 + 16) = v30;
    goto v39;
    addr_361b_15:
    rbp40 = r14_25;
    rbx41 = r14_25;
    rdx24 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r14_25) << 4);
    r15_42 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx23) + reinterpret_cast<unsigned char>(rdx24));
    do {
        eax43 = fun_2a00(r15_42, r12_22, rdx24, rcx23, r8_15);
        zf44 = reinterpret_cast<uint1_t>(eax43 == 0);
        if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(eax43 < 0) | zf44)) 
            break;
        rbx41 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbx41) - 1);
        r15_42 = r15_42 - 16;
    } while (rbx41 != 0xffffffffffffffff);
    goto addr_3670_20;
    if (zf44) {
        *reinterpret_cast<void***>(v28 + reinterpret_cast<uint64_t>(rbx41) * 8) = *reinterpret_cast<void***>(v28 + reinterpret_cast<uint64_t>(rbx41) * 8) + 1;
        goto v45;
    }
    r15_46 = v26 + 1;
    v30 = r15_46;
    rdx47 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_46) << 4);
    rax48 = xrealloc(v27, rdx47);
    rsi49 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_46) * 8);
    v34 = rax48;
    rax50 = xrealloc(v28, rsi49);
    rdx24 = rdx47;
    r15_37 = rax50;
    rax51 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v34) + reinterpret_cast<unsigned char>(rdx24) + 0xfffffffffffffff0);
    fun_2ab0(rax51, rsi49, rdx24, rcx23, r8_15);
    rax52 = reinterpret_cast<void**>(&rbx41->f1);
    v26 = rax52;
    v53 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax52) << 4);
    rax54 = rax51;
    if (reinterpret_cast<int64_t>(rbx41) < reinterpret_cast<int64_t>(r14_25)) {
        addr_36da_24:
        r14_55 = rax54;
    } else {
        goto addr_3701_26;
    }
    do {
        rdi56 = r14_55;
        r14_55 = r14_55 - 16;
        fun_27a0(rdi56, r14_55, rdx24, rcx23, r8_15);
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r15_37 + reinterpret_cast<uint64_t>(rbp40) * 8) + 8) = *reinterpret_cast<void***>(r15_37 + reinterpret_cast<uint64_t>(rbp40) * 8);
        rbp40 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbp40) - 1);
    } while (reinterpret_cast<int64_t>(rbx41) < reinterpret_cast<int64_t>(rbp40));
    addr_3701_26:
    rdi38 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(v53) + reinterpret_cast<unsigned char>(v34));
    goto addr_370b_17;
    addr_3670_20:
    r15_57 = v26 + 1;
    v30 = r15_57;
    r14_58 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_57) << 4);
    rax59 = xrealloc(v27, r14_58);
    rsi60 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_57) * 8);
    v34 = rax59;
    rax61 = xrealloc(v28, rsi60);
    r15_37 = rax61;
    rax62 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v34) + reinterpret_cast<unsigned char>(r14_58) + 0xfffffffffffffff0);
    fun_2ab0(rax62, rsi60, rdx24, rcx23, r8_15);
    rax54 = rax62;
    v53 = reinterpret_cast<void*>(0);
    v26 = reinterpret_cast<void**>(0);
    goto addr_36da_24;
}

void fun_2780(void** rdi, void** rsi, ...);

void fun_2650(void** rdi, void** rsi, int64_t rdx, void** rcx);

void** fun_25d0(void** rdi, ...);

void fun_2960(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_2680(void** rdi, int64_t rsi, void** rdx, void** rcx);

unsigned char mp_millerrabin(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_2660();

/* gcd2_odd.part.0 */
void** gcd2_odd_part_0(void*** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

/* prime_p.part.0 */
signed char prime_p_part_0(void** rdi, void** rsi, void** rdx, void** rcx);

void factor_using_pollard_rho(void** rdi, void** rsi, void** rdx, void** rcx);

void factor_insert_multiplicity(void** rdi, void** rsi, signed char dl, ...);

unsigned char prime2_p(void** rdi, void** rsi);

void** mod2(void*** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

struct s1 {
    signed char[54720] pad54720;
    unsigned char fd5c0;
};

/* factor_insert_large.part.0 */
void factor_insert_large_part_0();

struct s2 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
    void** f18;
    signed char[7] pad32;
    int64_t f20;
    void** f28;
    signed char[7] pad48;
    int64_t f30;
    void** f38;
    signed char[7] pad64;
    int64_t f40;
    void** f48;
    signed char[7] pad80;
    int64_t f50;
    void** f58;
    signed char[7] pad96;
    int64_t f60;
    void** f68;
    signed char[66263] pad66368;
    unsigned char f10340;
};

struct s3 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s4 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s5 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s6 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s7 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s8 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s9 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s10 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s11 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s12 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s13 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s14 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void factor_using_pollard_rho2(void** rdi, void** rsi, void** rdx, void** rcx);

void** powm(void** rdi, struct s0* rsi, void** rdx, struct s0* rcx, void** r8, void** r9);

void** fun_25f0();

void fun_28c0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_2760(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_2730(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t fun_2aa0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

/* mp_prime_p.part.0 */
signed char mp_prime_p_part_0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rbx7;
    void* rsp8;
    void** rax9;
    void** v10;
    void** r12_11;
    void** r15_12;
    void** rbp13;
    void** r14_14;
    void** v15;
    void** rax16;
    void** r13_17;
    void** v18;
    void** r9_19;
    void** r8_20;
    void** rcx21;
    unsigned char al22;
    void* rsp23;
    unsigned char v24;
    void* rsp25;
    void** rsi26;
    void* rsp27;
    void** rdi28;
    void** rcx29;
    void** rdx30;
    void** rsi31;
    void* rax32;
    uint32_t eax33;
    void* rsp34;
    void** r15_35;
    void** rbp36;
    void*** rsp37;
    void** v38;
    void** v39;
    void** rax40;
    void** v41;
    uint64_t rcx42;
    void** rdx43;
    int64_t rcx44;
    uint64_t rcx45;
    uint1_t cf46;
    void** rax47;
    void** rsi48;
    uint1_t cf49;
    int1_t cf50;
    void** v51;
    void** v52;
    void** tmp64_53;
    void** rax54;
    void** r12_55;
    void** r10_56;
    void** v57;
    void** v58;
    void** v59;
    void** v60;
    void** r14_61;
    void** r15_62;
    void*** v63;
    void** r9_64;
    uint64_t rax65;
    int64_t rax66;
    void* rax67;
    void* rdx68;
    void* rax69;
    void** rax70;
    void** rbp71;
    int64_t rax72;
    void** v73;
    int64_t v74;
    void** rax75;
    void** tmp64_76;
    void** rdx77;
    void* v78;
    void** v79;
    void** rdx80;
    void** r8_81;
    void** rcx82;
    void** tmp64_83;
    uint1_t cf84;
    void** rax85;
    void** v86;
    void** rax87;
    int64_t rax88;
    void** v89;
    void** rax90;
    void** v91;
    void** v92;
    void** rdx93;
    void** r15_94;
    void** r12_95;
    void** r13_96;
    void** rsi97;
    void** rbp98;
    void** rax99;
    void** tmp64_100;
    void** rcx101;
    void* v102;
    void** v103;
    void** r11_104;
    void** r10_105;
    void** v106;
    void** r15_107;
    void** r12_108;
    void*** rbp109;
    void** r9_110;
    void** rax111;
    void** tmp64_112;
    void** rdx113;
    void* v114;
    void** rcx115;
    void** rdx116;
    void** rdx117;
    void** rsi118;
    void** tmp64_119;
    uint1_t cf120;
    void** rax121;
    void** rdi122;
    void** r8_123;
    uint64_t rax124;
    int64_t rax125;
    void** r13_126;
    void* rax127;
    void* rdx128;
    void** rdx129;
    void* rax130;
    signed char al131;
    void** rdx132;
    void** rsi133;
    void** rdi134;
    unsigned char al135;
    void** rax136;
    void* rsp137;
    void** rax138;
    void* rsp139;
    void** rax140;
    uint64_t rax141;
    struct s1* rax142;
    void* rax143;
    void* rdx144;
    void* rax145;
    unsigned char al146;
    void** r8_147;
    void** r9_148;
    void** v149;
    void** rbp150;
    void** v151;
    void*** rsp152;
    void** r12_153;
    int64_t r13_154;
    void** r11_155;
    void** r10_156;
    void*** r14_157;
    struct s2* rbx158;
    uint64_t r15_159;
    void** rcx160;
    void** rdx161;
    void** r10_162;
    void** r13_163;
    void* rax164;
    int32_t ecx165;
    int32_t ecx166;
    int32_t edx167;
    int64_t rbx168;
    void** rcx169;
    void** rdx170;
    struct s3* rcx171;
    void* rcx172;
    void** rcx173;
    void** rdx174;
    void** rdx175;
    struct s4* rcx176;
    struct s5* rsi177;
    void* rsi178;
    void* rcx179;
    struct s6* rsi180;
    void* rsi181;
    void** rcx182;
    void** rdx183;
    void** v184;
    void** rdx185;
    uint32_t ecx186;
    uint32_t edx187;
    void** rsi188;
    struct s7* rdi189;
    void* rdi190;
    void** rdx191;
    void** rdx192;
    uint32_t ecx193;
    uint32_t edx194;
    void** rsi195;
    struct s8* rdi196;
    void* rdi197;
    void** rdx198;
    void** rdx199;
    uint32_t ecx200;
    uint32_t edx201;
    void** rsi202;
    struct s9* rdi203;
    void* rdi204;
    void** rdx205;
    int64_t rax206;
    void* rax207;
    int64_t rax208;
    struct s10* rax209;
    uint32_t ecx210;
    uint32_t edx211;
    void** rsi212;
    struct s11* rdi213;
    void* rdi214;
    void** rdx215;
    struct s12* rcx216;
    struct s13* rsi217;
    void* rcx218;
    void* rsi219;
    void** rcx220;
    void** rdi221;
    void** v222;
    void** v223;
    unsigned char al224;
    int64_t rax225;
    struct s14* rax226;
    void* rax227;
    int64_t v228;
    signed char al229;
    unsigned char al230;
    void** r9_231;
    void** rsi232;
    void** rdi233;
    void** rdi234;
    void** rdx235;
    void** rsi236;
    int64_t rax237;
    void** r8_238;
    void*** r11_239;
    signed char* r10_240;
    uint32_t r9d241;
    int32_t ebx242;
    void* rcx243;
    void* rax244;
    int64_t rsi245;
    void* rax246;
    uint32_t r9d247;
    uint32_t eax248;
    void** r12_249;
    void** rcx250;
    int32_t esi251;
    void** rax252;
    void* rdx253;
    void* rdi254;
    uint1_t cf255;
    int64_t rbp256;
    int64_t r15_257;
    int64_t r14_258;
    void** rbp259;
    void** rcx260;
    void** r11_261;
    uint64_t rax262;
    int64_t rax263;
    void* rax264;
    void* rdx265;
    void* rax266;
    void** r8_267;
    void* r10_268;
    void* r9_269;
    void* rax270;
    void** r13_271;
    int64_t rax272;
    int64_t rax273;
    void** rdi274;
    void** rax275;
    int64_t rcx276;
    void* rdi277;
    void* rax278;
    void* r11_279;
    void* rsi280;
    void* rax281;
    void** rbx282;
    void** rsi283;
    void** rax284;
    void** rcx285;
    void** r11_286;
    void** rdx287;
    void** rax288;
    void** r8_289;
    signed char al290;
    void** rdx291;
    void** rsi292;
    void** rcx293;
    int1_t zf294;
    signed char al295;
    struct s0* r14_296;
    void** rbp297;
    struct s0** rsp298;
    void** rax299;
    void** v300;
    uint32_t eax301;
    struct s0* v302;
    uint32_t v303;
    void** rcx304;
    void** r15_305;
    uint64_t rax306;
    int32_t esi307;
    int64_t rax308;
    void* rax309;
    void* rdx310;
    void* rax311;
    void** rdx312;
    struct s0* r13_313;
    void** rax314;
    uint64_t rdi315;
    uint1_t cf316;
    int64_t rbx317;
    void** rbx318;
    unsigned char al319;
    void* v320;
    struct s0* v321;
    uint32_t eax322;
    unsigned char v323;
    uint32_t v324;
    int64_t rax325;
    void* v326;
    void** rax327;
    void* rax328;
    void** v329;
    void** v330;
    void** rdx331;
    void** v332;
    void** rdi333;
    void*** v334;
    void** v335;
    void** rax336;
    void* rax337;
    void** rax338;
    void** rdx339;
    void** rcx340;
    int32_t esi341;
    void** rax342;
    uint64_t rdi343;
    uint1_t cf344;
    unsigned char al345;
    void* r11_346;
    uint64_t v347;
    void** rax348;
    void** rdx349;
    unsigned char al350;
    int64_t v351;
    void** rdx352;
    void*** v353;
    void** v354;
    void** rax355;

    rbx7 = rdi;
    rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x98);
    rax9 = g28;
    v10 = rax9;
    r12_11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp8) + 96);
    r15_12 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp8) + 64);
    rbp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp8) + 80);
    r14_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp8) + 0x70);
    v15 = r15_12;
    fun_2780(r15_12, rbp13, r15_12, rbp13);
    fun_2650(r12_11, rbx7, 1, r14_14);
    rax16 = fun_25d0(r12_11);
    r13_17 = rax16;
    v18 = rax16;
    fun_2960(r15_12, r12_11, rax16, r14_14);
    fun_2680(rbp13, 2, rax16, r14_14);
    r9_19 = r13_17;
    r8_20 = r15_12;
    rcx21 = r14_14;
    al22 = mp_millerrabin(rbx7, r12_11, rbp13, rcx21, r8_20, r9_19);
    rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    v24 = al22;
    if (al22) {
        r13_17 = reinterpret_cast<void**>(0x10340);
        fun_27a0(r14_14, r12_11, rbp13, rcx21, r8_20, r14_14, r12_11, rbp13, rcx21, r8_20);
        rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
        rsi26 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp25) + 32);
        mp_factor(r14_14, rsi26, rbp13, rcx21, r8_20, r9_19);
        rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
        goto addr_4458_3;
    }
    addr_455c_4:
    rdi28 = v15;
    rcx29 = r14_14;
    rdx30 = r12_11;
    rsi31 = rbp13;
    fun_2660();
    rax32 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (!rax32) {
        eax33 = v24;
        return *reinterpret_cast<signed char*>(&eax33);
    }
    fun_2740();
    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8 - 8 + 8);
    while (1) {
        r15_35 = rdi28;
        rbp36 = rsi31;
        rsp37 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp34) - 8 - 8 - 8 - 8 - 8 - 8 - 0x88);
        v38 = rdx30;
        v39 = rcx29;
        rax40 = g28;
        v41 = rax40;
        rcx42 = reinterpret_cast<unsigned char>(rcx29) - (reinterpret_cast<unsigned char>(rcx29) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx29) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx29) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi28) < reinterpret_cast<unsigned char>(2)))))));
        *reinterpret_cast<int32_t*>(&rdx43) = 0;
        *reinterpret_cast<int32_t*>(&rdx43 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rcx42) & 64;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx44) + 4) = 0;
        rcx45 = reinterpret_cast<uint64_t>(rcx44 + 63);
        cf46 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi28) < reinterpret_cast<unsigned char>(2));
        *reinterpret_cast<unsigned char*>(&rdx43) = cf46;
        rax47 = reinterpret_cast<void**>(-static_cast<uint64_t>(reinterpret_cast<uint1_t>(0 < static_cast<uint64_t>(cf46))) + 1);
        do {
            rsi48 = rdx43;
            rdx43 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx43) + reinterpret_cast<unsigned char>(rdx43));
            rax47 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax47) + reinterpret_cast<unsigned char>(rax47)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rsi48) >> 63));
            if (reinterpret_cast<unsigned char>(r15_35) < reinterpret_cast<unsigned char>(rax47) || r15_35 == rax47 && reinterpret_cast<unsigned char>(rbp36) <= reinterpret_cast<unsigned char>(rdx43)) {
                cf49 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx43) < reinterpret_cast<unsigned char>(rbp36));
                rdx43 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx43) - reinterpret_cast<unsigned char>(rbp36));
                rax47 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax47) - (reinterpret_cast<unsigned char>(r15_35) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax47) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r15_35) + static_cast<uint64_t>(cf49))))));
            }
            cf50 = rcx45 < 1;
            --rcx45;
        } while (!cf50);
        v51 = rdx43;
        v52 = rax47;
        tmp64_53 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx43) + reinterpret_cast<unsigned char>(rdx43));
        rax54 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax47) + reinterpret_cast<unsigned char>(rax47) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_53) < reinterpret_cast<unsigned char>(rdx43))));
        r12_55 = tmp64_53;
        r10_56 = rax54;
        if (reinterpret_cast<unsigned char>(rax54) > reinterpret_cast<unsigned char>(r15_35) || rax54 == r15_35 && reinterpret_cast<unsigned char>(tmp64_53) >= reinterpret_cast<unsigned char>(rbp36)) {
            r12_55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_53) - reinterpret_cast<unsigned char>(rbp36));
            r10_56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax54) - (reinterpret_cast<unsigned char>(r15_35) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax54) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r15_35) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_53) < reinterpret_cast<unsigned char>(rbp36))))))));
        }
        v57 = r10_56;
        v58 = r10_56;
        if (r15_35) 
            goto addr_468c_14;
        if (rbp36 == 1) 
            goto addr_4ad5_16;
        addr_468c_14:
        v59 = r12_55;
        r13_17 = r15_35;
        *reinterpret_cast<int32_t*>(&rbx7) = 1;
        *reinterpret_cast<int32_t*>(&rbx7 + 4) = 0;
        v60 = reinterpret_cast<void**>(1);
        r14_61 = reinterpret_cast<void**>(rsp37 + 0x70);
        r15_62 = r12_55;
        v63 = rsp37 + 0x68;
        while (1) {
            r9_64 = r13_17;
            r13_17 = rbx7;
            rax65 = reinterpret_cast<unsigned char>(rbp36) >> 1;
            rbx7 = rbp36;
            *reinterpret_cast<uint32_t*>(&rax66) = *reinterpret_cast<uint32_t*>(&rax65) & 0x7f;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax66) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rax67) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax66);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax67) + 4) = 0;
            rdx68 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax67) + reinterpret_cast<int64_t>(rax67) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax67) * reinterpret_cast<int64_t>(rax67) * reinterpret_cast<unsigned char>(rbp36)));
            rax69 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx68) + reinterpret_cast<uint64_t>(rdx68) - reinterpret_cast<uint64_t>(rdx68) * reinterpret_cast<uint64_t>(rdx68) * reinterpret_cast<unsigned char>(rbp36));
            rax70 = rbp36;
            rbp71 = r10_56;
            *reinterpret_cast<uint32_t*>(&rax72) = *reinterpret_cast<uint32_t*>(&rax70) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax72) + 4) = 0;
            v73 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax69) + reinterpret_cast<uint64_t>(rax69) - reinterpret_cast<uint64_t>(rax69) * reinterpret_cast<uint64_t>(rax69) * reinterpret_cast<unsigned char>(rbp36));
            v74 = rax72;
            while (1) {
                rax75 = mulredc2(r14_61, rbp71, r12_55, rbp71, r12_55, r9_64, rbx7, v52);
                tmp64_76 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax75) + reinterpret_cast<unsigned char>(v38));
                rdx77 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v78) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_76) < reinterpret_cast<unsigned char>(rax75))));
                v79 = rdx77;
                r12_55 = tmp64_76;
                rbp71 = rdx77;
                if (reinterpret_cast<unsigned char>(rdx77) > reinterpret_cast<unsigned char>(r9_64) || rdx77 == r9_64 && reinterpret_cast<unsigned char>(tmp64_76) >= reinterpret_cast<unsigned char>(rbx7)) {
                    rdx80 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx77) - (reinterpret_cast<unsigned char>(r9_64) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx77) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r9_64) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_76) < reinterpret_cast<unsigned char>(rbx7))))))));
                    v79 = rdx80;
                    r12_55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_76) - reinterpret_cast<unsigned char>(rbx7));
                    rbp71 = rdx80;
                }
                r8_81 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v59) - reinterpret_cast<unsigned char>(r12_55));
                rcx82 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v57) - (reinterpret_cast<unsigned char>(rbp71) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v57) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp71) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v59) < reinterpret_cast<unsigned char>(r12_55))))))));
                if (reinterpret_cast<signed char>(rcx82) < reinterpret_cast<signed char>(0)) {
                    tmp64_83 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_81) + reinterpret_cast<unsigned char>(rbx7));
                    cf84 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_83) < reinterpret_cast<unsigned char>(r8_81));
                    r8_81 = tmp64_83;
                    rcx82 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx82) + reinterpret_cast<unsigned char>(r9_64) + static_cast<uint64_t>(cf84));
                }
                rax85 = mulredc2(r14_61, v52, v51, rcx82, r8_81, r9_64, rbx7, v52);
                v51 = rax85;
                v52 = v86;
                rax87 = r13_17;
                *reinterpret_cast<uint32_t*>(&rax88) = *reinterpret_cast<uint32_t*>(&rax87) & 31;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax88) + 4) = 0;
                rsp37 = rsp37 - 8 - 8 - 8 + 8 + 8 + 8 - 8 - 8 - 8 + 8 + 8 + 8;
                r9_64 = r9_64;
                if (rax88 == 1) {
                    if (!v74) 
                        goto addr_4cba_24;
                    if (reinterpret_cast<unsigned char>(v51) | reinterpret_cast<unsigned char>(v52)) 
                        goto addr_48c0_26;
                } else {
                    addr_47d9_27:
                    --r13_17;
                    if (r13_17) 
                        continue; else 
                        goto addr_47e3_28;
                }
                v89 = r9_64;
                rax90 = rbx7;
                if (r9_64) 
                    break;
                addr_48ee_30:
                if (!reinterpret_cast<int1_t>(rax90 == 1)) 
                    goto addr_4920_31;
                v58 = rbp71;
                r15_62 = r12_55;
                goto addr_47d9_27;
                addr_48c0_26:
                rax90 = gcd2_odd_part_0(v63, v52, v51, r9_64, rbx7, r9_64);
                rsp37 = rsp37 - 8 + 8;
                r9_64 = r9_64;
                if (v89) 
                    goto addr_4920_31; else 
                    goto addr_48ee_30;
                addr_47e3_28:
                v57 = rbp71;
                r15_62 = r12_55;
                v91 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v60) + reinterpret_cast<unsigned char>(v60));
                if (v60) {
                    v92 = r12_55;
                    rdx93 = r12_55;
                    r15_94 = r13_17;
                    r12_95 = v73;
                    r13_96 = v38;
                    rsi97 = rbp71;
                    rbp98 = r9_64;
                    do {
                        rax99 = mulredc2(r14_61, rsi97, rdx93, rsi97, rdx93, rbp98, rbx7, r12_95);
                        tmp64_100 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax99) + reinterpret_cast<unsigned char>(r13_96));
                        rcx101 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v102) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_100) < reinterpret_cast<unsigned char>(rax99))));
                        rdx93 = tmp64_100;
                        rsi97 = rcx101;
                        rsp37 = rsp37 - 8 - 8 - 8 + 8 + 8 + 8;
                        if (reinterpret_cast<unsigned char>(rcx101) > reinterpret_cast<unsigned char>(rbp98) || rcx101 == rbp98 && reinterpret_cast<unsigned char>(tmp64_100) >= reinterpret_cast<unsigned char>(rbx7)) {
                            rdx93 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_100) - reinterpret_cast<unsigned char>(rbx7));
                            rsi97 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx101) - (reinterpret_cast<unsigned char>(rbp98) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx101) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp98) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_100) < reinterpret_cast<unsigned char>(rbx7))))))));
                        }
                        ++r15_94;
                    } while (v60 != r15_94);
                    r12_55 = v92;
                    r9_64 = rbp98;
                    r15_62 = rdx93;
                    rbp71 = rsi97;
                }
                r13_17 = v60;
                v59 = r12_55;
                r12_55 = r15_62;
                v58 = rbp71;
                v60 = v91;
            }
            addr_4920_31:
            v103 = r12_55;
            r11_104 = r15_62;
            r10_105 = v58;
            v106 = r13_17;
            r15_107 = r14_61;
            r13_17 = rbx7;
            r12_108 = v59;
            r14_61 = v73;
            rbp109 = v63;
            rbx7 = r9_64;
            do {
                r9_110 = rbx7;
                rax111 = mulredc2(r15_107, r10_105, r11_104, r10_105, r11_104, r9_110, r13_17, r14_61);
                tmp64_112 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax111) + reinterpret_cast<unsigned char>(v38));
                rdx113 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v114) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_112) < reinterpret_cast<unsigned char>(rax111))));
                v58 = rdx113;
                r11_104 = tmp64_112;
                rcx115 = r13_17;
                r10_105 = rdx113;
                rsp37 = rsp37 - 8 - 8 - 8 + 8 + 8 + 8;
                if (reinterpret_cast<unsigned char>(rdx113) > reinterpret_cast<unsigned char>(rbx7) || rdx113 == rbx7 && reinterpret_cast<unsigned char>(tmp64_112) >= reinterpret_cast<unsigned char>(r13_17)) {
                    rdx116 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx113) - (reinterpret_cast<unsigned char>(rbx7) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx113) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbx7) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_112) < reinterpret_cast<unsigned char>(r13_17))))))));
                    v58 = rdx116;
                    r11_104 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_112) - reinterpret_cast<unsigned char>(r13_17));
                    r10_105 = rdx116;
                }
                rdx117 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_108) - reinterpret_cast<unsigned char>(r11_104));
                rsi118 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v57) - (reinterpret_cast<unsigned char>(r10_105) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v57) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r10_105) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_108) < reinterpret_cast<unsigned char>(r11_104))))))));
                if (reinterpret_cast<signed char>(rsi118) < reinterpret_cast<signed char>(0)) {
                    tmp64_119 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx117) + reinterpret_cast<unsigned char>(r13_17));
                    cf120 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_119) < reinterpret_cast<unsigned char>(rdx117));
                    rdx117 = tmp64_119;
                    rsi118 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi118) + reinterpret_cast<unsigned char>(rbx7) + static_cast<uint64_t>(cf120));
                }
                if (reinterpret_cast<unsigned char>(rsi118) | reinterpret_cast<unsigned char>(rdx117)) {
                    rcx115 = rbx7;
                    rax121 = gcd2_odd_part_0(rbp109, rsi118, rdx117, rcx115, r13_17, r9_110);
                    rsp37 = rsp37 - 8 + 8;
                    rdi122 = v89;
                    if (rdi122) 
                        goto addr_4a05_46;
                } else {
                    rdi122 = rbx7;
                    v89 = rbx7;
                    rax121 = r13_17;
                    if (rdi122) 
                        goto addr_4a05_46;
                }
            } while (reinterpret_cast<int1_t>(rax121 == 1));
            r8_123 = rax121;
            rax124 = reinterpret_cast<unsigned char>(rax121) >> 1;
            *reinterpret_cast<uint32_t*>(&rax125) = *reinterpret_cast<uint32_t*>(&rax124) & 0x7f;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax125) + 4) = 0;
            r13_126 = rbx7;
            r14_61 = r15_107;
            *reinterpret_cast<uint32_t*>(&rax127) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax125);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax127) + 4) = 0;
            rbx7 = v106;
            rdx128 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax127) + reinterpret_cast<int64_t>(rax127) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax127) * reinterpret_cast<int64_t>(rax127) * reinterpret_cast<unsigned char>(r8_123)));
            rdx129 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdx128) * reinterpret_cast<uint64_t>(rdx128) * reinterpret_cast<unsigned char>(r8_123));
            rax130 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx128) + reinterpret_cast<uint64_t>(rdx128) - reinterpret_cast<unsigned char>(rdx129));
            rcx115 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax130) + reinterpret_cast<uint64_t>(rax130) - reinterpret_cast<uint64_t>(rax130) * reinterpret_cast<uint64_t>(rax130) * reinterpret_cast<unsigned char>(r8_123));
            rbp36 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_17) * reinterpret_cast<unsigned char>(rcx115));
            if (reinterpret_cast<unsigned char>(r13_126) < reinterpret_cast<unsigned char>(r8_123)) {
                *reinterpret_cast<int32_t*>(&r13_17) = 0;
                *reinterpret_cast<int32_t*>(&r13_17 + 4) = 0;
            } else {
                rdx129 = __intrinsic();
                r9_110 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r13_126) - reinterpret_cast<unsigned char>(rdx129)) * reinterpret_cast<unsigned char>(rcx115));
                r13_17 = r9_110;
            }
            if (reinterpret_cast<unsigned char>(r8_123) <= reinterpret_cast<unsigned char>(1) || reinterpret_cast<unsigned char>(r8_123) > reinterpret_cast<unsigned char>(0x17ded78) && (al131 = prime_p_part_0(r8_123, rsi118, rdx129, rcx115), rsp37 = rsp37 - 8 + 8, r8_123 = r8_123, al131 == 0)) {
                rdx132 = v39;
                rsi133 = v38 + 1;
                factor_using_pollard_rho(r8_123, rsi133, rdx132, rcx115);
                rsp37 = rsp37 - 8 + 8;
            } else {
                *reinterpret_cast<int32_t*>(&rdx132) = 1;
                *reinterpret_cast<int32_t*>(&rdx132 + 4) = 0;
                rsi133 = r8_123;
                factor_insert_multiplicity(v39, rsi133, 1);
                rsp37 = rsp37 - 8 + 8;
            }
            if (!r13_17) 
                goto addr_4aa0_56;
            rsi133 = rbp36;
            rdi134 = r13_17;
            al135 = prime2_p(rdi134, rsi133);
            rsp37 = rsp37 - 8 + 8;
            if (al135) 
                goto addr_4c8b_58;
            rax136 = mod2(rsp37 + 80, v79, v103, r13_17, rbp36, r9_110);
            rsp137 = reinterpret_cast<void*>(rsp37 - 8 + 8);
            r12_55 = rax136;
            rax138 = mod2(reinterpret_cast<int64_t>(rsp137) + 88, v57, v59, r13_17, rbp36, r9_110);
            rsp139 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp137) - 8 + 8);
            v59 = rax138;
            rax140 = mod2(reinterpret_cast<int64_t>(rsp139) + 96, v58, r11_104, r13_17, rbp36, r9_110);
            rsp37 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp139) - 8 + 8);
            r10_56 = v79;
            r15_62 = rax140;
        }
        addr_4a05_46:
        if (rbx7 != rdi122) 
            goto addr_4a19_60;
        if (rax121 == r13_17) 
            goto addr_4ce0_62;
        addr_4a19_60:
        rbx7 = reinterpret_cast<void**>(0xd5c0);
        rsi133 = rax121;
        v60 = rax121;
        rax141 = reinterpret_cast<unsigned char>(rax121) >> 1;
        *reinterpret_cast<uint32_t*>(&rax142) = *reinterpret_cast<uint32_t*>(&rax141) & 0x7f;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax142) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax143) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(0xd5c0) + reinterpret_cast<uint64_t>(rax142));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax143) + 4) = 0;
        rdx144 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax143) + reinterpret_cast<int64_t>(rax143) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax143) * reinterpret_cast<int64_t>(rax143) * reinterpret_cast<unsigned char>(rax121)));
        rax145 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx144) + reinterpret_cast<uint64_t>(rdx144) - reinterpret_cast<uint64_t>(rdx144) * reinterpret_cast<uint64_t>(rdx144) * reinterpret_cast<unsigned char>(rax121));
        rdx132 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax145) + reinterpret_cast<uint64_t>(rax145) - reinterpret_cast<uint64_t>(rax145) * reinterpret_cast<uint64_t>(rax145) * reinterpret_cast<unsigned char>(rax121));
        rbp36 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_17) * reinterpret_cast<unsigned char>(rdx132));
        al146 = prime2_p(rdi122, rsi133);
        rsp37 = rsp37 - 8 + 8;
        if (!al146) 
            goto addr_4c4f_63;
        if (!v89) 
            goto addr_4ca3_65;
        rdi134 = v39;
        if (!*reinterpret_cast<void***>(rdi134 + 8)) 
            goto addr_4a94_67;
        addr_4cfe_68:
        factor_insert_large_part_0();
        r8_147 = rdi134;
        r9_148 = rdx132;
        v149 = r13_17;
        rbp150 = rsi133;
        v151 = rbx7;
        rsp152 = rsp37 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 56;
        *reinterpret_cast<unsigned char*>(rdx132 + 0xfa) = 0;
        *reinterpret_cast<void***>(rdx132 + 8) = reinterpret_cast<void**>(0);
        if (rdi134) 
            goto addr_4d50_70;
        if (reinterpret_cast<unsigned char>(rsi133) <= reinterpret_cast<unsigned char>(1)) 
            goto addr_4d41_72;
        addr_4d50_70:
        if (*reinterpret_cast<unsigned char*>(&rbp150) & 1) {
            addr_4df3_73:
            if (!r8_147) {
                *reinterpret_cast<int32_t*>(&r12_153) = 3;
                *reinterpret_cast<int32_t*>(&r12_153 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r13_154) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_154) + 4) = 0;
                r11_155 = reinterpret_cast<void**>(0x5555555555555555);
                r10_156 = reinterpret_cast<void**>(0xaaaaaaaaaaaaaaab);
            } else {
                r14_157 = reinterpret_cast<void***>(0xd648);
                *reinterpret_cast<uint32_t*>(&rbx158) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx158) + 4) = 0;
                r15_159 = 0xaaaaaaaaaaaaaaab;
                *reinterpret_cast<int32_t*>(&r12_153) = 3;
                *reinterpret_cast<int32_t*>(&r12_153 + 4) = 0;
                while (1) {
                    rcx160 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp150) * r15_159);
                    rdx161 = __intrinsic();
                    if (reinterpret_cast<unsigned char>(rdx161) <= reinterpret_cast<unsigned char>(r8_147)) {
                        r10_162 = *r14_157;
                        while ((r13_163 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(r8_147) - reinterpret_cast<unsigned char>(rdx161)) * r15_159), reinterpret_cast<unsigned char>(r13_163) <= reinterpret_cast<unsigned char>(r10_162)) && (factor_insert_multiplicity(r9_148, r12_153, 1), rsp152 = rsp152 - 8 + 8, r8_147 = r13_163, r9_148 = r9_148, r10_162 = r10_162, rbp150 = rcx160, rdx161 = __intrinsic(), reinterpret_cast<unsigned char>(rdx161) <= reinterpret_cast<unsigned char>(r13_163))) {
                            rcx160 = reinterpret_cast<void**>(r15_159 * reinterpret_cast<unsigned char>(rcx160));
                        }
                    }
                    *reinterpret_cast<uint32_t*>(&rax164) = rbx158->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax164) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&r13_154) = *reinterpret_cast<uint32_t*>(&rbx158);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_154) + 4) = 0;
                    r14_157 = r14_157 + 16;
                    r12_153 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_153) + reinterpret_cast<uint64_t>(rax164));
                    if (!r8_147) 
                        break;
                    if (*reinterpret_cast<uint32_t*>(&rbx158) > 0x29b) 
                        break;
                    r15_159 = *reinterpret_cast<uint64_t*>(r14_157 - 8);
                    rbx158 = reinterpret_cast<struct s2*>(&rbx158->pad8);
                }
                if (*reinterpret_cast<uint32_t*>(&r13_154) > 0x29b) 
                    goto addr_5200_84; else 
                    goto addr_4edd_85;
            }
        } else {
            if (rbp150) {
                __asm__("bsf rdx, rbp");
                ecx165 = 64 - *reinterpret_cast<int32_t*>(&rdx132);
                ecx166 = *reinterpret_cast<int32_t*>(&rdx132);
                rbp150 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp150) >> *reinterpret_cast<signed char*>(&ecx166)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_147) << *reinterpret_cast<unsigned char*>(&ecx165)));
                factor_insert_multiplicity(r9_148, 2, *reinterpret_cast<signed char*>(&rdx132));
                rsp152 = rsp152 - 8 + 8;
                r8_147 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_147) >> *reinterpret_cast<signed char*>(&ecx166));
                r9_148 = r9_148;
                goto addr_4df3_73;
            } else {
                __asm__("bsf rcx, r8");
                edx167 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rcx115 + 64));
                rbp150 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_147) >> *reinterpret_cast<signed char*>(&rcx115));
                *reinterpret_cast<int32_t*>(&r12_153) = 3;
                *reinterpret_cast<int32_t*>(&r12_153 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r13_154) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_154) + 4) = 0;
                factor_insert_multiplicity(r9_148, 2, *reinterpret_cast<signed char*>(&edx167));
                rsp152 = rsp152 - 8 + 8;
                r9_148 = r9_148;
                *reinterpret_cast<uint32_t*>(&r8_147) = 0;
                *reinterpret_cast<int32_t*>(&r8_147 + 4) = 0;
                r11_155 = reinterpret_cast<void**>(0x5555555555555555);
                r10_156 = reinterpret_cast<void**>(0xaaaaaaaaaaaaaaab);
            }
        }
        addr_4ef9_89:
        *reinterpret_cast<int32_t*>(&rbx168) = static_cast<int32_t>(r13_154 + 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx168) + 4) = 0;
        r13_154 = rbx168;
        rbx158 = reinterpret_cast<struct s2*>((rbx168 << 4) + reinterpret_cast<unsigned char>(0xd640));
        while (1) {
            *reinterpret_cast<int32_t*>(&r15_159) = static_cast<int32_t>(r13_154 - 1);
            rcx169 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp150) * reinterpret_cast<unsigned char>(r10_156));
            if (reinterpret_cast<unsigned char>(r11_155) >= reinterpret_cast<unsigned char>(rcx169)) {
                do {
                    factor_insert_multiplicity(r9_148, r12_153, 1);
                    rsp152 = rsp152 - 8 + 8;
                    r10_156 = r10_156;
                    r11_155 = r11_155;
                    r9_148 = r9_148;
                    rbp150 = rcx169;
                    rcx169 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx169) * reinterpret_cast<unsigned char>(r10_156));
                    r8_147 = r8_147;
                } while (reinterpret_cast<unsigned char>(r11_155) >= reinterpret_cast<unsigned char>(rcx169));
                rdx170 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx158->f0) * reinterpret_cast<unsigned char>(rbp150));
                if (reinterpret_cast<unsigned char>(rbx158->f8) < reinterpret_cast<unsigned char>(rdx170)) 
                    goto addr_4f29_93;
                goto addr_5050_95;
            }
            rdx170 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx158->f0) * reinterpret_cast<unsigned char>(rbp150));
            if (reinterpret_cast<unsigned char>(rbx158->f8) >= reinterpret_cast<unsigned char>(rdx170)) {
                addr_5050_95:
                *reinterpret_cast<uint32_t*>(&rcx171) = *reinterpret_cast<uint32_t*>(&r13_154);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx171) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rcx172) = rcx171->f10340;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx172) + 4) = 0;
                rcx173 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx172) + reinterpret_cast<unsigned char>(r12_153));
            } else {
                addr_4f29_93:
                rdx174 = reinterpret_cast<void**>(rbx158->f10 * reinterpret_cast<unsigned char>(rbp150));
                if (reinterpret_cast<unsigned char>(rdx174) <= reinterpret_cast<unsigned char>(rbx158->f18)) 
                    goto addr_50b0_97; else 
                    goto addr_4f3b_98;
            }
            do {
                rbp150 = rdx170;
                factor_insert_multiplicity(r9_148, rcx173, 1);
                rsp152 = rsp152 - 8 + 8;
                r9_148 = r9_148;
                rcx173 = rcx173;
                r8_147 = r8_147;
                rdx170 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx158->f0) * reinterpret_cast<unsigned char>(rbp150));
            } while (reinterpret_cast<unsigned char>(rdx170) <= reinterpret_cast<unsigned char>(rbx158->f8));
            rdx174 = reinterpret_cast<void**>(rbx158->f10 * reinterpret_cast<unsigned char>(rbp150));
            if (reinterpret_cast<unsigned char>(rdx174) > reinterpret_cast<unsigned char>(rbx158->f18)) {
                addr_4f3b_98:
                rdx175 = reinterpret_cast<void**>(rbx158->f20 * reinterpret_cast<unsigned char>(rbp150));
                if (reinterpret_cast<unsigned char>(rdx175) <= reinterpret_cast<unsigned char>(rbx158->f28)) {
                    addr_5120_101:
                    *reinterpret_cast<int32_t*>(&rcx176) = static_cast<int32_t>(r13_154 + 2);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx176) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi177) = *reinterpret_cast<uint32_t*>(&r13_154);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi177) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi178) = rsi177->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi178) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rcx179) = rcx176->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx179) + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rsi180) = static_cast<int32_t>(r13_154 + 1);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi180) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi181) = rsi180->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi181) + 4) = 0;
                    rcx182 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx179) + reinterpret_cast<int64_t>(rsi178) + reinterpret_cast<unsigned char>(r12_153) + reinterpret_cast<uint64_t>(rsi181));
                } else {
                    addr_4f4d_102:
                    rdx183 = reinterpret_cast<void**>(rbx158->f30 * reinterpret_cast<unsigned char>(rbp150));
                    if (reinterpret_cast<unsigned char>(rdx183) <= reinterpret_cast<unsigned char>(rbx158->f38)) 
                        goto addr_5198_103; else 
                        goto addr_4f5f_104;
                }
            } else {
                goto addr_50b0_97;
            }
            do {
                rbp150 = rdx175;
                v184 = rcx182;
                factor_insert_multiplicity(r9_148, rcx182, 1);
                rsp152 = rsp152 - 8 + 8;
                r9_148 = r9_148;
                rcx182 = v184;
                r8_147 = r8_147;
                rdx175 = reinterpret_cast<void**>(rbx158->f20 * reinterpret_cast<unsigned char>(rbp150));
            } while (reinterpret_cast<unsigned char>(rdx175) <= reinterpret_cast<unsigned char>(rbx158->f28));
            rdx183 = reinterpret_cast<void**>(rbx158->f30 * reinterpret_cast<unsigned char>(rbp150));
            if (reinterpret_cast<unsigned char>(rdx183) > reinterpret_cast<unsigned char>(rbx158->f38)) {
                addr_4f5f_104:
                rdx185 = reinterpret_cast<void**>(rbx158->f40 * reinterpret_cast<unsigned char>(rbp150));
                if (reinterpret_cast<unsigned char>(rdx185) <= reinterpret_cast<unsigned char>(rbx158->f48)) {
                    rbp150 = rdx185;
                    ecx186 = static_cast<uint32_t>(r13_154 + 5);
                    while (1) {
                        edx187 = *reinterpret_cast<uint32_t*>(&r13_154);
                        rsi188 = r12_153;
                        do {
                            *reinterpret_cast<uint32_t*>(&rdi189) = edx187;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi189) + 4) = 0;
                            ++edx187;
                            *reinterpret_cast<uint32_t*>(&rdi190) = rdi189->f10340;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi190) + 4) = 0;
                            rsi188 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi188) + reinterpret_cast<uint64_t>(rdi190));
                        } while (edx187 != ecx186);
                        v184 = r8_147;
                        factor_insert_multiplicity(r9_148, rsi188, 1);
                        rsp152 = rsp152 - 8 + 8;
                        r9_148 = r9_148;
                        r8_147 = v184;
                        ecx186 = ecx186;
                        rdx191 = reinterpret_cast<void**>(rbx158->f40 * reinterpret_cast<unsigned char>(rbp150));
                        if (reinterpret_cast<unsigned char>(rdx191) > reinterpret_cast<unsigned char>(rbx158->f48)) 
                            break;
                        rbp150 = rdx191;
                    }
                }
            } else {
                goto addr_5198_103;
            }
            rdx192 = reinterpret_cast<void**>(rbx158->f50 * reinterpret_cast<unsigned char>(rbp150));
            if (reinterpret_cast<unsigned char>(rdx192) <= reinterpret_cast<unsigned char>(rbx158->f58)) {
                rbp150 = rdx192;
                ecx193 = static_cast<uint32_t>(r13_154 + 6);
                while (1) {
                    edx194 = *reinterpret_cast<uint32_t*>(&r13_154);
                    rsi195 = r12_153;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdi196) = edx194;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi196) + 4) = 0;
                        ++edx194;
                        *reinterpret_cast<uint32_t*>(&rdi197) = rdi196->f10340;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi197) + 4) = 0;
                        rsi195 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi195) + reinterpret_cast<uint64_t>(rdi197));
                    } while (edx194 != ecx193);
                    v184 = r8_147;
                    factor_insert_multiplicity(r9_148, rsi195, 1);
                    rsp152 = rsp152 - 8 + 8;
                    r9_148 = r9_148;
                    r8_147 = v184;
                    ecx193 = ecx193;
                    rdx198 = reinterpret_cast<void**>(rbx158->f50 * reinterpret_cast<unsigned char>(rbp150));
                    if (reinterpret_cast<unsigned char>(rdx198) > reinterpret_cast<unsigned char>(rbx158->f58)) 
                        break;
                    rbp150 = rdx198;
                }
            }
            rdx199 = reinterpret_cast<void**>(rbx158->f60 * reinterpret_cast<unsigned char>(rbp150));
            if (reinterpret_cast<unsigned char>(rdx199) <= reinterpret_cast<unsigned char>(rbx158->f68)) {
                rbp150 = rdx199;
                ecx200 = static_cast<uint32_t>(r13_154 + 7);
                while (1) {
                    edx201 = *reinterpret_cast<uint32_t*>(&r13_154);
                    rsi202 = r12_153;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdi203) = edx201;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi203) + 4) = 0;
                        ++edx201;
                        *reinterpret_cast<uint32_t*>(&rdi204) = rdi203->f10340;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi204) + 4) = 0;
                        rsi202 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi202) + reinterpret_cast<uint64_t>(rdi204));
                    } while (edx201 != ecx200);
                    v184 = r8_147;
                    factor_insert_multiplicity(r9_148, rsi202, 1);
                    rsp152 = rsp152 - 8 + 8;
                    r9_148 = r9_148;
                    r8_147 = v184;
                    ecx200 = ecx200;
                    rdx205 = reinterpret_cast<void**>(rbx158->f60 * reinterpret_cast<unsigned char>(rbp150));
                    if (reinterpret_cast<unsigned char>(rdx205) > reinterpret_cast<unsigned char>(rbx158->f68)) 
                        break;
                    rbp150 = rdx205;
                }
            }
            *reinterpret_cast<int32_t*>(&rax206) = *reinterpret_cast<int32_t*>(&r15_159);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax206) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rax207) = *reinterpret_cast<unsigned char*>(0x10080 + rax206);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax207) + 4) = 0;
            r12_153 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_153) + reinterpret_cast<uint64_t>(rax207));
            if (reinterpret_cast<unsigned char>(rbp150) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r12_153) * reinterpret_cast<unsigned char>(r12_153))) 
                break;
            *reinterpret_cast<uint32_t*>(&rax208) = static_cast<uint32_t>(r13_154 + 7);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax208) + 4) = 0;
            rbx158 = reinterpret_cast<struct s2*>(reinterpret_cast<uint64_t>(rbx158) + 0x80);
            *reinterpret_cast<uint32_t*>(&r13_154) = *reinterpret_cast<uint32_t*>(&r13_154) + 8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_154) + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&rax208) > 0x29b) 
                break;
            rax209 = reinterpret_cast<struct s10*>((rax208 << 4) + reinterpret_cast<unsigned char>(0xd640));
            r10_156 = rax209->f0;
            r11_155 = rax209->f8;
            continue;
            addr_5198_103:
            rbp150 = rdx183;
            ecx210 = static_cast<uint32_t>(r13_154 + 4);
            while (1) {
                edx211 = *reinterpret_cast<uint32_t*>(&r13_154);
                rsi212 = r12_153;
                do {
                    *reinterpret_cast<uint32_t*>(&rdi213) = edx211;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi213) + 4) = 0;
                    ++edx211;
                    *reinterpret_cast<uint32_t*>(&rdi214) = rdi213->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi214) + 4) = 0;
                    rsi212 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi212) + reinterpret_cast<uint64_t>(rdi214));
                } while (ecx210 != edx211);
                factor_insert_multiplicity(r9_148, rsi212, 1);
                rsp152 = rsp152 - 8 + 8;
                r9_148 = r9_148;
                r8_147 = r8_147;
                ecx210 = ecx210;
                rdx215 = reinterpret_cast<void**>(rbx158->f30 * reinterpret_cast<unsigned char>(rbp150));
                if (reinterpret_cast<unsigned char>(rdx215) > reinterpret_cast<unsigned char>(rbx158->f38)) 
                    goto addr_4f5f_104;
                rbp150 = rdx215;
            }
            addr_50b0_97:
            *reinterpret_cast<int32_t*>(&rcx216) = static_cast<int32_t>(r13_154 + 1);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx216) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rsi217) = *reinterpret_cast<uint32_t*>(&r13_154);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi217) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx218) = rcx216->f10340;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx218) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rsi219) = rsi217->f10340;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi219) + 4) = 0;
            rcx220 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx218) + reinterpret_cast<int64_t>(rsi219) + reinterpret_cast<unsigned char>(r12_153));
            do {
                rbp150 = rdx174;
                factor_insert_multiplicity(r9_148, rcx220, 1);
                rsp152 = rsp152 - 8 + 8;
                r9_148 = r9_148;
                rcx220 = rcx220;
                r8_147 = r8_147;
                rdx174 = reinterpret_cast<void**>(rbx158->f10 * reinterpret_cast<unsigned char>(rbp150));
            } while (reinterpret_cast<unsigned char>(rdx174) <= reinterpret_cast<unsigned char>(rbx158->f18));
            rdx175 = reinterpret_cast<void**>(rbx158->f20 * reinterpret_cast<unsigned char>(rbp150));
            if (reinterpret_cast<unsigned char>(rdx175) > reinterpret_cast<unsigned char>(rbx158->f28)) 
                goto addr_4f4d_102;
            goto addr_5120_101;
        }
        addr_5200_84:
        if (!r8_147) 
            goto addr_5209_136;
        rdi221 = r8_147;
        v222 = r9_148;
        v223 = r8_147;
        al224 = prime2_p(rdi221, rbp150);
        rsp152 = rsp152 - 8 + 8;
        if (al224) 
            goto addr_53a9_138;
        rsi31 = rbp150;
        rcx29 = v222;
        *reinterpret_cast<int32_t*>(&rdx30) = 1;
        *reinterpret_cast<int32_t*>(&rdx30 + 4) = 0;
        rbx7 = v151;
        rdi28 = v223;
        r13_17 = v149;
        rsp34 = reinterpret_cast<void*>(rsp152 + 56 + 8 + 8 + 8 + 8 + 8 + 8);
        continue;
        addr_4edd_85:
        *reinterpret_cast<uint32_t*>(&rax225) = *reinterpret_cast<uint32_t*>(&r13_154);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax225) + 4) = 0;
        rax226 = reinterpret_cast<struct s14*>((rax225 << 4) + 0xd640);
        r10_156 = rax226->f0;
        r11_155 = rax226->f8;
        goto addr_4ef9_89;
        addr_4c8b_58:
        if (*reinterpret_cast<void***>(v39 + 8)) 
            goto addr_4cfe_68; else 
            goto addr_4c97_140;
    }
    addr_4cba_24:
    fun_2810("b0 & 1", "src/factor.c", 0x1e0, "gcd2_odd", r8_81, r9_64);
    do {
        fun_2740();
        addr_4ce0_62:
        factor_using_pollard_rho2(rbx7, r13_17, v38 + 1, v39);
        addr_4ad5_16:
        rax227 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v41) - reinterpret_cast<unsigned char>(g28));
    } while (rax227);
    goto v228;
    addr_4aa0_56:
    if (reinterpret_cast<unsigned char>(rbp36) <= reinterpret_cast<unsigned char>(1)) {
        addr_4c74_143:
        factor_using_pollard_rho(rbp36, v38, v39, rcx115);
        goto addr_4ad5_16;
    } else {
        addr_4aaa_144:
        if (reinterpret_cast<unsigned char>(rbp36) <= reinterpret_cast<unsigned char>(0x17ded78) || (al229 = prime_p_part_0(rbp36, rsi133, rdx132, rcx115), !!al229)) {
            factor_insert_multiplicity(v39, rbp36, 1, v39, rbp36, 1);
            goto addr_4ad5_16;
        }
    }
    addr_4c4f_63:
    rcx115 = v39;
    rsi133 = v60;
    rdx132 = v38 + 1;
    factor_using_pollard_rho2(v89, rsi133, rdx132, rcx115);
    if (reinterpret_cast<unsigned char>(rbp36) > reinterpret_cast<unsigned char>(1)) 
        goto addr_4aaa_144; else 
        goto addr_4c74_143;
    addr_4ca3_65:
    *reinterpret_cast<int32_t*>(&rdx132) = 1;
    *reinterpret_cast<int32_t*>(&rdx132 + 4) = 0;
    rsi133 = v60;
    factor_insert_multiplicity(v39, rsi133, 1);
    goto addr_4aa0_56;
    addr_4a94_67:
    *reinterpret_cast<void***>(v39) = v60;
    *reinterpret_cast<void***>(v39 + 8) = v89;
    goto addr_4aa0_56;
    addr_5209_136:
    if (reinterpret_cast<unsigned char>(rbp150) <= reinterpret_cast<unsigned char>(1)) {
        addr_4d41_72:
        goto v60;
    } else {
        al230 = prime2_p(0, rbp150);
        r9_231 = r9_148;
        if (al230) {
            rsi232 = rbp150;
            rdi233 = r9_231;
        } else {
            rdi234 = rbp150;
            rdx235 = r9_231;
            *reinterpret_cast<int32_t*>(&rsi236) = 1;
            *reinterpret_cast<int32_t*>(&rsi236 + 4) = 0;
            goto addr_5740_149;
        }
    }
    addr_2f20_150:
    *reinterpret_cast<uint32_t*>(&rax237) = *reinterpret_cast<unsigned char*>(rdi233 + 0xfa);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax237) + 4) = 0;
    r8_238 = rsi232;
    r11_239 = reinterpret_cast<void***>(rdi233 + 16);
    r10_240 = reinterpret_cast<signed char*>(rdi233 + 0xe0);
    r9d241 = *reinterpret_cast<uint32_t*>(&rax237);
    if (!*reinterpret_cast<uint32_t*>(&rax237)) 
        goto addr_2fa1_151;
    ebx242 = static_cast<int32_t>(rax237 - 1);
    rcx243 = reinterpret_cast<void*>(static_cast<int64_t>(ebx242));
    rax244 = rcx243;
    do {
        *reinterpret_cast<int32_t*>(&rsi245) = *reinterpret_cast<int32_t*>(&rax244);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi245) + 4) = 0;
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi233 + reinterpret_cast<uint64_t>(rax244) * 8) + 16)) <= reinterpret_cast<unsigned char>(r8_238)) 
            break;
        rax244 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax244) - 1);
        *reinterpret_cast<int32_t*>(&rsi245) = *reinterpret_cast<int32_t*>(&rax244);
    } while (*reinterpret_cast<int32_t*>(&rax244) != -1);
    goto addr_2f80_155;
    if (*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi233 + reinterpret_cast<uint64_t>(rax244) * 8) + 16) == r8_238) {
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_240) + reinterpret_cast<uint64_t>(rax244)) = reinterpret_cast<signed char>(*reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_240) + reinterpret_cast<uint64_t>(rax244)) + 1);
        goto v60;
    }
    rax246 = reinterpret_cast<void*>(static_cast<int64_t>(static_cast<int32_t>(rsi245 + 1)));
    r11_239 = r11_239 + reinterpret_cast<uint64_t>(rax246) * 8;
    r10_240 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_240) + reinterpret_cast<uint64_t>(rax246));
    if (*reinterpret_cast<int32_t*>(&rsi245) >= ebx242) {
        addr_2fa1_151:
        r9d247 = r9d241 + 1;
        *r11_239 = r8_238;
        *r10_240 = 1;
        *reinterpret_cast<unsigned char*>(rdi233 + 0xfa) = *reinterpret_cast<unsigned char*>(&r9d247);
        goto v60;
    }
    do {
        addr_2f80_155:
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi233 + reinterpret_cast<uint64_t>(rcx243) * 8) + 24) = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi233 + reinterpret_cast<uint64_t>(rcx243) * 8) + 16);
        eax248 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi233) + reinterpret_cast<uint64_t>(rcx243) + 0xe0);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi233) + reinterpret_cast<uint64_t>(rcx243) + 0xe1) = *reinterpret_cast<signed char*>(&eax248);
        rcx243 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx243) - 1);
    } while (*reinterpret_cast<int32_t*>(&rsi245) < *reinterpret_cast<int32_t*>(&rcx243));
    goto addr_2fa1_151;
    addr_5740_149:
    v222 = rsi236;
    v184 = rdx235;
    if (reinterpret_cast<unsigned char>(rdi234) <= reinterpret_cast<unsigned char>(1)) {
        addr_5b07_160:
        fun_2810("(1) < (n)", "src/factor.c", 0x5be, "factor_using_pollard_rho", r8_147, r9_231);
    } else {
        r12_249 = rdi234;
        do {
            rcx250 = r12_249;
            esi251 = 64;
            *reinterpret_cast<int32_t*>(&rax252) = 1;
            *reinterpret_cast<int32_t*>(&rax252 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx253) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx253) + 4) = 0;
            rdi254 = reinterpret_cast<void*>(0);
            do {
                r8_147 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx250) << 63);
                rcx250 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx250) >> 1);
                rdx253 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx253) >> 1 | reinterpret_cast<unsigned char>(r8_147));
                if (reinterpret_cast<unsigned char>(rcx250) < reinterpret_cast<unsigned char>(rax252) || rcx250 == rax252 && reinterpret_cast<uint64_t>(rdx253) <= reinterpret_cast<uint64_t>(rdi254)) {
                    cf255 = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi254) < reinterpret_cast<uint64_t>(rdx253));
                    rdi254 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi254) - reinterpret_cast<uint64_t>(rdx253));
                    rax252 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax252) - (reinterpret_cast<unsigned char>(rcx250) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax252) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx250) + static_cast<uint64_t>(cf255))))));
                }
                --esi251;
            } while (esi251);
            *reinterpret_cast<int32_t*>(&rbp256) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp256) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r15_257) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_257) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r14_258) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_258) + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rbp256) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_249) - reinterpret_cast<uint64_t>(rdi254) > reinterpret_cast<uint64_t>(rdi254));
            rbp259 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbp256) & reinterpret_cast<unsigned char>(r12_249)) + (reinterpret_cast<uint64_t>(rdi254) + reinterpret_cast<uint64_t>(rdi254) - reinterpret_cast<unsigned char>(r12_249)));
            rcx260 = rbp259;
            while (reinterpret_cast<unsigned char>(v222) < reinterpret_cast<unsigned char>(r12_249)) {
                r11_261 = rcx260;
                rax262 = reinterpret_cast<unsigned char>(r12_249) >> 1;
                *reinterpret_cast<uint32_t*>(&rax263) = *reinterpret_cast<uint32_t*>(&rax262) & 0x7f;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax263) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax264) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax263);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax264) + 4) = 0;
                rdx265 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax264) + reinterpret_cast<int64_t>(rax264) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax264) * reinterpret_cast<int64_t>(rax264) * reinterpret_cast<unsigned char>(r12_249)));
                rax266 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx265) + reinterpret_cast<uint64_t>(rdx265) - reinterpret_cast<uint64_t>(rdx265) * reinterpret_cast<uint64_t>(rdx265) * reinterpret_cast<unsigned char>(r12_249));
                r8_267 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax266) + reinterpret_cast<uint64_t>(rax266) - reinterpret_cast<uint64_t>(rax266) * reinterpret_cast<uint64_t>(rax266) * reinterpret_cast<unsigned char>(r12_249));
                r10_268 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_249) - reinterpret_cast<unsigned char>(v222));
                r9_269 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v222) - reinterpret_cast<unsigned char>(r12_249));
                while (1) {
                    rax270 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rax270 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax270) + reinterpret_cast<unsigned char>(r12_249));
                    }
                    rbp259 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rbp259) - (reinterpret_cast<unsigned char>(rbp259) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp259) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp259) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax270) < reinterpret_cast<uint64_t>(r10_268))))))) & reinterpret_cast<unsigned char>(r12_249)) + (reinterpret_cast<uint64_t>(rax270) + reinterpret_cast<uint64_t>(r9_269)));
                    r13_271 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        r13_271 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_271) + reinterpret_cast<unsigned char>(r12_249));
                    }
                    rax272 = r14_258;
                    *reinterpret_cast<uint32_t*>(&rax273) = *reinterpret_cast<uint32_t*>(&rax272) & 31;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax273) + 4) = 0;
                    if (rax273 == 1) {
                        rdi274 = r13_271;
                        rax275 = gcd_odd(rdi274, r12_249);
                        if (!reinterpret_cast<int1_t>(rax275 == 1)) 
                            break;
                    }
                    --r14_258;
                    if (r14_258) 
                        continue;
                    rcx276 = r15_257 + r15_257;
                    if (!r15_257) {
                        r15_257 = rcx276;
                        r11_261 = rbp259;
                    } else {
                        do {
                            rdi277 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                            rax278 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi277) + reinterpret_cast<unsigned char>(r12_249));
                            if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                                rdi277 = rax278;
                            }
                            ++r14_258;
                        } while (r15_257 != r14_258);
                        r11_261 = rbp259;
                        r15_257 = rcx276;
                        rbp259 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax278) - (reinterpret_cast<uint64_t>(rax278) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax278) < reinterpret_cast<uint64_t>(rax278) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi277) < reinterpret_cast<uint64_t>(r10_268)))) & reinterpret_cast<unsigned char>(r12_249)) + (reinterpret_cast<uint64_t>(rdi277) + reinterpret_cast<uint64_t>(r9_269)));
                    }
                }
                r9_231 = r8_267;
                r8_147 = r11_261;
                r11_279 = r9_269;
                do {
                    rsi280 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    rax281 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi280) + reinterpret_cast<unsigned char>(r12_249));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rsi280 = rax281;
                    }
                    rbx282 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax281) - (reinterpret_cast<uint64_t>(rax281) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax281) < reinterpret_cast<uint64_t>(rax281) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rsi280) < reinterpret_cast<uint64_t>(r10_268)))) & reinterpret_cast<unsigned char>(r12_249)) + (reinterpret_cast<uint64_t>(rsi280) + reinterpret_cast<uint64_t>(r11_279)));
                    rsi283 = r12_249;
                    rdi274 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rdi274) - (reinterpret_cast<unsigned char>(rdi274) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi274) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rdi274) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_147) < reinterpret_cast<unsigned char>(rbx282))))))) & reinterpret_cast<unsigned char>(r12_249)) + (reinterpret_cast<unsigned char>(r8_147) - reinterpret_cast<unsigned char>(rbx282)));
                    rax284 = gcd_odd(rdi274, rsi283);
                } while (rax284 == 1);
                rcx285 = r8_147;
                r11_286 = rax284;
                if (rax284 == r12_249) 
                    goto addr_5af7_189;
                rdx287 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_249) % reinterpret_cast<unsigned char>(r11_286));
                rax288 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_249) / reinterpret_cast<unsigned char>(r11_286));
                r8_289 = rax288;
                r12_249 = rax288;
                if (reinterpret_cast<unsigned char>(r11_286) > reinterpret_cast<unsigned char>(1) && (reinterpret_cast<unsigned char>(r11_286) <= reinterpret_cast<unsigned char>(0x17ded78) || (al290 = prime_p_part_0(r11_286, rsi283, rdx287, rcx285), r11_286 = r11_286, rcx285 = rcx285, r8_289 = rax288, !!al290))) {
                    *reinterpret_cast<int32_t*>(&rdx291) = 1;
                    *reinterpret_cast<int32_t*>(&rdx291 + 4) = 0;
                    rsi292 = r11_286;
                    factor_insert_multiplicity(v184, rsi292, 1);
                    r8_147 = r8_289;
                    rcx293 = rcx285;
                    zf294 = reinterpret_cast<int1_t>(r8_147 == 1);
                    if (reinterpret_cast<unsigned char>(r8_147) <= reinterpret_cast<unsigned char>(1)) 
                        goto addr_5aca_192; else 
                        goto addr_5a2f_193;
                }
                rdx291 = v184;
                rsi292 = v222 + 1;
                factor_using_pollard_rho(r11_286, rsi292, rdx291, rcx285);
                r8_147 = r8_289;
                rcx293 = rcx285;
                zf294 = reinterpret_cast<int1_t>(r8_147 == 1);
                if (reinterpret_cast<unsigned char>(r8_147) > reinterpret_cast<unsigned char>(1)) {
                    addr_5a2f_193:
                    if (reinterpret_cast<unsigned char>(r8_147) <= reinterpret_cast<unsigned char>(0x17ded78)) 
                        goto addr_5ad7_195;
                    al295 = prime_p_part_0(r8_147, rsi292, rdx291, rcx293);
                    r8_147 = r8_147;
                    if (al295) 
                        goto addr_5ad7_195;
                } else {
                    addr_5aca_192:
                    if (zf294) 
                        goto addr_5b45_197; else 
                        goto addr_5acc_198;
                }
                rbp259 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp259) % reinterpret_cast<unsigned char>(r8_147));
                rcx260 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx293) % reinterpret_cast<unsigned char>(r8_147));
                continue;
                addr_5acc_198:
                *reinterpret_cast<int32_t*>(&rcx260) = 0;
                *reinterpret_cast<int32_t*>(&rcx260 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rbp259) = 0;
                *reinterpret_cast<int32_t*>(&rbp259 + 4) = 0;
            }
            break;
            addr_5af7_189:
            ++v222;
        } while (reinterpret_cast<unsigned char>(r12_249) > reinterpret_cast<unsigned char>(1));
        goto addr_5b07_160;
    }
    fun_2810("a < n", "src/factor.c", 0x5c4, "factor_using_pollard_rho", r8_147, r9_231);
    addr_5b45_197:
    goto v60;
    addr_5ad7_195:
    rdi233 = v184;
    rsi232 = r8_147;
    goto addr_2f20_150;
    addr_53a9_138:
    if (!*reinterpret_cast<void***>(v222 + 8)) {
        *reinterpret_cast<void***>(v222) = rbp150;
        *reinterpret_cast<void***>(v222 + 8) = v223;
        goto addr_4d41_72;
    }
    factor_insert_large_part_0();
    r14_296 = reinterpret_cast<struct s0*>(rdi221 + 0xffffffffffffffff);
    rbp297 = rdi221;
    rsp298 = reinterpret_cast<struct s0**>(rsp152 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x148);
    rax299 = g28;
    v300 = rax299;
    eax301 = 0;
    v302 = r14_296;
    if (*reinterpret_cast<uint32_t*>(&rdi221) & 1) 
        goto addr_5478_204;
    v303 = 0;
    r14_296 = v302;
    addr_5490_206:
    rcx304 = rbp297;
    *reinterpret_cast<int32_t*>(&r15_305) = 0;
    *reinterpret_cast<int32_t*>(&r15_305 + 4) = 0;
    rax306 = reinterpret_cast<unsigned char>(rbp297) >> 1;
    esi307 = 64;
    *reinterpret_cast<uint32_t*>(&rax308) = *reinterpret_cast<uint32_t*>(&rax306) & 0x7f;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax308) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax309) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax308);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax309) + 4) = 0;
    rdx310 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax309) + reinterpret_cast<int64_t>(rax309) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax309) * reinterpret_cast<int64_t>(rax309) * reinterpret_cast<unsigned char>(rbp297)));
    rax311 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx310) + reinterpret_cast<uint64_t>(rdx310) - reinterpret_cast<uint64_t>(rdx310) * reinterpret_cast<uint64_t>(rdx310) * reinterpret_cast<unsigned char>(rbp297));
    *reinterpret_cast<int32_t*>(&rdx312) = 0;
    *reinterpret_cast<int32_t*>(&rdx312 + 4) = 0;
    r13_313 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax311) + reinterpret_cast<uint64_t>(rax311) - reinterpret_cast<uint64_t>(rax311) * reinterpret_cast<uint64_t>(rax311) * reinterpret_cast<unsigned char>(rbp297));
    *reinterpret_cast<int32_t*>(&rax314) = 1;
    *reinterpret_cast<int32_t*>(&rax314 + 4) = 0;
    do {
        rdi315 = reinterpret_cast<unsigned char>(rcx304) << 63;
        rcx304 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx304) >> 1);
        rdx312 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx312) >> 1) | rdi315);
        if (reinterpret_cast<unsigned char>(rcx304) < reinterpret_cast<unsigned char>(rax314) || rcx304 == rax314 && reinterpret_cast<unsigned char>(rdx312) <= reinterpret_cast<unsigned char>(r15_305)) {
            cf316 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_305) < reinterpret_cast<unsigned char>(rdx312));
            r15_305 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_305) - reinterpret_cast<unsigned char>(rdx312));
            rax314 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax314) - (reinterpret_cast<unsigned char>(rcx304) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax314) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx304) + static_cast<uint64_t>(cf316))))));
        }
        --esi307;
    } while (esi307);
    *reinterpret_cast<int32_t*>(&rbx317) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx317) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&r8_147) = v303;
    *reinterpret_cast<int32_t*>(&r8_147 + 4) = 0;
    r9_231 = r15_305;
    *reinterpret_cast<unsigned char*>(&rbx317) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp297) - reinterpret_cast<unsigned char>(r15_305)) > reinterpret_cast<unsigned char>(r15_305));
    rbx318 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbx317) & reinterpret_cast<unsigned char>(rbp297)) + (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r15_305) + reinterpret_cast<unsigned char>(r15_305)) - reinterpret_cast<unsigned char>(rbp297)));
    al319 = millerrabin(rbp297, r13_313, rbx318, r14_296, *reinterpret_cast<uint32_t*>(&r8_147), r9_231);
    if (al319) {
        v320 = reinterpret_cast<void*>(rsp298 - 1 + 1 + 6);
        factor();
        v321 = r14_296;
        r14_296 = r13_313;
        eax322 = v323;
        r13_313 = reinterpret_cast<struct s0*>(0x10340);
        v324 = eax322;
        *reinterpret_cast<uint32_t*>(&rax325) = eax322 - 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax325) + 4) = 0;
        v326 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v320) + rax325 * 8);
        r12_153 = reinterpret_cast<void**>(2);
        rax327 = rbx318;
        rbx318 = r15_305;
        r15_305 = rax327;
        goto addr_55b0_212;
    }
    addr_56fc_213:
    addr_5690_214:
    rax328 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v300) - reinterpret_cast<unsigned char>(g28));
    if (rax328) {
        fun_2740();
    } else {
        goto v223;
    }
    addr_5719_217:
    *reinterpret_cast<int32_t*>(&rdx235) = 0x4f2;
    *reinterpret_cast<int32_t*>(&rdx235 + 4) = 0;
    rsi236 = reinterpret_cast<void**>("src/factor.c");
    rdi234 = reinterpret_cast<void**>("(s1) < (n)");
    fun_2810("(s1) < (n)", "src/factor.c", 0x4f2, "prime_p", r8_147, r9_231);
    goto addr_5740_149;
    addr_5478_204:
    do {
        r14_296 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(r14_296) >> 1);
        ++eax301;
    } while (!(*reinterpret_cast<unsigned char*>(&r14_296) & 1));
    v303 = eax301;
    goto addr_5490_206;
    addr_4c97_140:
    *reinterpret_cast<void***>(v39) = rbp36;
    *reinterpret_cast<void***>(v39 + 8) = r13_17;
    goto addr_4ad5_16;
    addr_5689_220:
    goto addr_5690_214;
    addr_4548_221:
    fun_25e0(v329, rsi26, v329, rsi26);
    fun_25e0(v330, rsi26, v330, rsi26);
    rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8 - 8 + 8);
    goto addr_455c_4;
    addr_45a0_222:
    v24 = 0;
    rdx331 = v332;
    addr_4524_223:
    if (rdx331) {
        *reinterpret_cast<int32_t*>(&rbx7) = 0;
        *reinterpret_cast<int32_t*>(&rbx7 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdi333) = 0;
        *reinterpret_cast<int32_t*>(&rdi333 + 4) = 0;
        do {
            fun_2940((reinterpret_cast<unsigned char>(rdi333) << 4) + reinterpret_cast<uint64_t>(v334), rsi26, rdx331, rcx21, r8_20, r9_19);
            rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8);
            *reinterpret_cast<int32_t*>(&rdi333) = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbx7 + 1));
            *reinterpret_cast<int32_t*>(&rdi333 + 4) = 0;
            rbx7 = rdi333;
        } while (reinterpret_cast<unsigned char>(rdi333) < reinterpret_cast<unsigned char>(v335));
        goto addr_4548_221;
    }
    addr_5680_226:
    while (rax336 == rbx318) {
        while (1) {
            *reinterpret_cast<uint32_t*>(&rax337) = r13_313->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax337) + 4) = 0;
            r12_153 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_153) + reinterpret_cast<uint64_t>(rax337));
            rax338 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx318) * reinterpret_cast<unsigned char>(r12_153));
            rdx339 = __intrinsic();
            r15_305 = rax338;
            if (rdx339) {
                if (reinterpret_cast<unsigned char>(rbp297) <= reinterpret_cast<unsigned char>(rdx339)) 
                    goto addr_5719_217;
                rcx340 = rbp297;
                esi341 = 64;
                *reinterpret_cast<int32_t*>(&rax342) = 0;
                *reinterpret_cast<int32_t*>(&rax342 + 4) = 0;
                do {
                    rdi343 = reinterpret_cast<unsigned char>(rcx340) << 63;
                    rcx340 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx340) >> 1);
                    rax342 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax342) >> 1) | rdi343);
                    if (reinterpret_cast<unsigned char>(rcx340) < reinterpret_cast<unsigned char>(rdx339) || rcx340 == rdx339 && reinterpret_cast<unsigned char>(rax342) <= reinterpret_cast<unsigned char>(r15_305)) {
                        cf344 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_305) < reinterpret_cast<unsigned char>(rax342));
                        r15_305 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_305) - reinterpret_cast<unsigned char>(rax342));
                        rdx339 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx339) - (reinterpret_cast<unsigned char>(rcx340) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx339) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx340) + static_cast<uint64_t>(cf344))))));
                    }
                    --esi341;
                } while (esi341);
            } else {
                r15_305 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax338) % reinterpret_cast<unsigned char>(rbp297));
            }
            *reinterpret_cast<uint32_t*>(&r8_147) = v303;
            *reinterpret_cast<int32_t*>(&r8_147 + 4) = 0;
            r9_231 = rbx318;
            al345 = millerrabin(rbp297, r14_296, r15_305, v321, *reinterpret_cast<uint32_t*>(&r8_147), r9_231);
            if (!al345) 
                goto addr_56fc_213;
            r13_313 = reinterpret_cast<struct s0*>(&r13_313->f1);
            if (reinterpret_cast<int1_t>(0x105dc == r13_313)) 
                break;
            addr_55b0_212:
            if (!v324) 
                goto addr_5690_214;
            r11_346 = v320;
            do {
                r8_147 = rbx318;
                rax336 = powm(r15_305, reinterpret_cast<uint64_t>(v302) / v347, rbp297, r14_296, r8_147, r9_231);
                if (v326 == r11_346) 
                    goto addr_5680_226;
                r11_346 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r11_346) + 8);
            } while (rax336 != rbx318);
        }
        fun_2700();
        fun_2a20();
        rax336 = fun_25f0();
    }
    goto addr_5689_220;
    addr_4520_242:
    while (!*reinterpret_cast<int32_t*>(&rax348)) {
        while (1) {
            *reinterpret_cast<uint32_t*>(&rdx349) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_17));
            *reinterpret_cast<int32_t*>(&rdx349 + 4) = 0;
            fun_28c0(rbp13, rbp13, rdx349, rcx21, r8_20, r9_19);
            r9_19 = v18;
            rcx21 = r14_14;
            r8_20 = v15;
            rsi26 = r12_11;
            al350 = mp_millerrabin(rbx7, rsi26, rbp13, rcx21, r8_20, r9_19);
            rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8 - 8 + 8);
            if (!al350) 
                goto addr_45a0_222;
            ++r13_17;
            if (reinterpret_cast<int1_t>(r13_17 == 0x105dc)) 
                break;
            addr_4458_3:
            if (!v351) 
                goto addr_4548_221;
            *reinterpret_cast<int32_t*>(&r15_12) = 0;
            *reinterpret_cast<int32_t*>(&r15_12 + 4) = 0;
            do {
                rdx352 = r15_12;
                ++r15_12;
                fun_2760(r14_14, r12_11, (reinterpret_cast<unsigned char>(rdx352) << 4) + reinterpret_cast<uint64_t>(v353), rcx21, r8_20, r9_19);
                rcx21 = rbx7;
                fun_2730(r14_14, rbp13, r14_14, rcx21, r8_20, r9_19);
                *reinterpret_cast<int32_t*>(&rsi26) = 1;
                *reinterpret_cast<int32_t*>(&rsi26 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rax348) = fun_2aa0(r14_14, 1, r14_14, rcx21, r8_20, r9_19);
                rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8 - 8 + 8 - 8 + 8);
                rdx331 = v354;
                if (reinterpret_cast<unsigned char>(r15_12) >= reinterpret_cast<unsigned char>(rdx331)) 
                    goto addr_4520_242;
            } while (*reinterpret_cast<int32_t*>(&rax348));
        }
        rax355 = fun_2700();
        *reinterpret_cast<int32_t*>(&rsi26) = 0;
        *reinterpret_cast<int32_t*>(&rsi26 + 4) = 0;
        rdx331 = rax355;
        fun_2a20();
        rax348 = fun_25f0();
        rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8 - 8 + 8 - 8 + 8);
    }
    goto addr_4524_223;
}

void fun_2a90(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, void** a8, void** a9, void** a10, void** a11, void** a12);

void fun_28b0(void** rdi, int64_t rsi);

void fun_29f0(void** rdi, void** rsi, ...);

void fun_27b0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_29d0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_26c0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_2a40(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void mp_factor_insert(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_2840(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t fun_28f0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_27e0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

struct s15 {
    signed char[54720] pad54720;
    unsigned char fd5c0;
};

struct s16 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
    void** f18;
    signed char[7] pad32;
    int64_t f20;
    void** f28;
    signed char[7] pad48;
    int64_t f30;
    void** f38;
    signed char[7] pad64;
    int64_t f40;
    void** f48;
    signed char[7] pad80;
    int64_t f50;
    void** f58;
    signed char[7] pad96;
    int64_t f60;
    void** f68;
    signed char[66263] pad66368;
    unsigned char f10340;
};

struct s17 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s18 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s19 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s20 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s21 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s22 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s23 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s24 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s25 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s26 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s27 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s28 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void mp_factor_using_pollard_rho(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rbp7;
    void* rsp8;
    void** v9;
    void** v10;
    void** rax11;
    void** v12;
    int1_t zf13;
    void** rdi14;
    void** v15;
    void** v16;
    void** v17;
    void** v18;
    void** v19;
    void** rdx20;
    void** r15_21;
    void** rbx22;
    void** r14_23;
    void** r12_24;
    void** r13_25;
    void* rsp26;
    void** rax27;
    void*** rsp28;
    void** rax29;
    void* rsp30;
    void** v31;
    int32_t eax32;
    void*** rsp33;
    void** r15_34;
    void** rbx35;
    void** rdx36;
    void** rax37;
    int64_t rax38;
    int32_t eax39;
    void*** rsp40;
    void* rsp41;
    void** v42;
    void** v43;
    void** r14_44;
    void** rbx45;
    void** v46;
    void** r13_47;
    void** r15_48;
    void** rbx49;
    void** r13_50;
    void** v51;
    void** r14_52;
    void** r12_53;
    int32_t eax54;
    void** rdx55;
    int32_t eax56;
    void* rsp57;
    int32_t eax58;
    signed char al59;
    int1_t zf60;
    void* rsp61;
    int32_t eax62;
    void* rsp63;
    int32_t eax64;
    signed char al65;
    void** r8_66;
    void** r9_67;
    void** rsi68;
    void** rdi69;
    void** rcx70;
    void** rdx71;
    void* rax72;
    void* rsp73;
    void** rax74;
    void** v75;
    int32_t v76;
    int1_t zf77;
    void** rdi78;
    void** r13_79;
    void** v80;
    void** rax81;
    void** rbx82;
    void** rdx83;
    void* rsp84;
    int32_t eax85;
    void* rsp86;
    void* rax87;
    void** rsi88;
    int32_t eax89;
    void* rsp90;
    int32_t eax91;
    int32_t eax92;
    int1_t zf93;
    int32_t eax94;
    signed char al95;
    void* rax96;
    void* rax97;
    void* rax98;
    void** r12_99;
    void** rdx100;
    int64_t v101;
    void** rbx102;
    void* rsp103;
    void** rax104;
    void** v105;
    void** r12_106;
    void** r15_107;
    void** rbp108;
    void** r14_109;
    void** v110;
    void** rax111;
    void** r13_112;
    void** v113;
    void** r9_114;
    void** r8_115;
    void** rcx116;
    unsigned char al117;
    void* rsp118;
    void* rsp119;
    void** rsi120;
    void* rsp121;
    void** rdi122;
    void** rcx123;
    void** rdx124;
    void** rsi125;
    void* rax126;
    int64_t v127;
    void* rsp128;
    void** r15_129;
    void** rbp130;
    void*** rsp131;
    void** v132;
    void** v133;
    void** rax134;
    void** v135;
    uint64_t rcx136;
    void** rdx137;
    int64_t rcx138;
    uint64_t rcx139;
    uint1_t cf140;
    void** rax141;
    void** rsi142;
    uint1_t cf143;
    int1_t cf144;
    void** v145;
    void** v146;
    void** tmp64_147;
    void** rax148;
    void** r12_149;
    void** r10_150;
    void** v151;
    void** v152;
    void** v153;
    void** v154;
    void** r14_155;
    void** r15_156;
    void*** v157;
    void** r9_158;
    uint64_t rax159;
    int64_t rax160;
    void* rax161;
    void* rdx162;
    void* rax163;
    void** rax164;
    void** rbp165;
    int64_t rax166;
    void** v167;
    int64_t v168;
    void** rax169;
    void** tmp64_170;
    void** rdx171;
    void* v172;
    void** v173;
    void** rdx174;
    void** r8_175;
    void** rcx176;
    void** tmp64_177;
    uint1_t cf178;
    void** rax179;
    void** v180;
    void** rax181;
    int64_t rax182;
    void** v183;
    void** rax184;
    void** v185;
    void** v186;
    void** rdx187;
    void** r15_188;
    void** r12_189;
    void** r13_190;
    void** rsi191;
    void** rbp192;
    void** rax193;
    void** tmp64_194;
    void** rcx195;
    void* v196;
    void** v197;
    void** r11_198;
    void** r10_199;
    void** v200;
    void** r15_201;
    void** r12_202;
    void*** rbp203;
    void** r9_204;
    void** rax205;
    void** tmp64_206;
    void** rdx207;
    void* v208;
    void** rcx209;
    void** rdx210;
    void** rdx211;
    void** rsi212;
    void** tmp64_213;
    uint1_t cf214;
    void** rax215;
    void** rdi216;
    void** r8_217;
    uint64_t rax218;
    int64_t rax219;
    void** r13_220;
    void* rax221;
    void* rdx222;
    void** rdx223;
    void* rax224;
    signed char al225;
    void** rdx226;
    void** rsi227;
    void** rdi228;
    unsigned char al229;
    void** rax230;
    void* rsp231;
    void** rax232;
    void* rsp233;
    void** rax234;
    uint64_t rax235;
    struct s15* rax236;
    void* rax237;
    void* rdx238;
    void* rax239;
    unsigned char al240;
    void** r8_241;
    void** r9_242;
    void** v243;
    void** rbp244;
    void** v245;
    void*** rsp246;
    void** r12_247;
    int64_t r13_248;
    void** r11_249;
    void** r10_250;
    void*** r14_251;
    struct s16* rbx252;
    uint64_t r15_253;
    void** rcx254;
    void** rdx255;
    void** r10_256;
    void** r13_257;
    void* rax258;
    int32_t ecx259;
    int32_t ecx260;
    int32_t edx261;
    int64_t rbx262;
    void** rcx263;
    void** rdx264;
    struct s17* rcx265;
    void* rcx266;
    void** rcx267;
    void** rdx268;
    void** rdx269;
    struct s18* rcx270;
    struct s19* rsi271;
    void* rsi272;
    void* rcx273;
    struct s20* rsi274;
    void* rsi275;
    void** rcx276;
    void** rdx277;
    void** v278;
    void** rdx279;
    uint32_t ecx280;
    uint32_t edx281;
    void** rsi282;
    struct s21* rdi283;
    void* rdi284;
    void** rdx285;
    void** rdx286;
    uint32_t ecx287;
    uint32_t edx288;
    void** rsi289;
    struct s22* rdi290;
    void* rdi291;
    void** rdx292;
    void** rdx293;
    uint32_t ecx294;
    uint32_t edx295;
    void** rsi296;
    struct s23* rdi297;
    void* rdi298;
    void** rdx299;
    int64_t rax300;
    void* rax301;
    int64_t rax302;
    struct s24* rax303;
    uint32_t ecx304;
    uint32_t edx305;
    void** rsi306;
    struct s25* rdi307;
    void* rdi308;
    void** rdx309;
    struct s26* rcx310;
    struct s27* rsi311;
    void* rcx312;
    void* rsi313;
    void** rcx314;
    void** rdi315;
    void** v316;
    void** v317;
    unsigned char al318;
    int64_t rax319;
    struct s28* rax320;
    void* rax321;
    int64_t v322;
    signed char al323;
    unsigned char al324;
    void** r9_325;
    void** rsi326;
    void** rdi327;
    void** rdi328;
    void** rdx329;
    void** rsi330;
    int64_t rax331;
    void** r8_332;
    void*** r11_333;
    signed char* r10_334;
    uint32_t r9d335;
    int32_t ebx336;
    void* rcx337;
    void* rax338;
    int64_t rsi339;
    void* rax340;
    uint32_t r9d341;
    uint32_t eax342;
    void** r12_343;
    void** rcx344;
    int32_t esi345;
    void** rax346;
    void* rdx347;
    void* rdi348;
    uint1_t cf349;
    int64_t rbp350;
    int64_t r15_351;
    int64_t r14_352;
    void** rbp353;
    void** rcx354;
    void** r11_355;
    uint64_t rax356;
    int64_t rax357;
    void* rax358;
    void* rdx359;
    void* rax360;
    void** r8_361;
    void* r10_362;
    void* r9_363;
    void* rax364;
    void** r13_365;
    int64_t rax366;
    int64_t rax367;
    void** rdi368;
    void** rax369;
    int64_t rcx370;
    void* rdi371;
    void* rax372;
    void* r11_373;
    void* rsi374;
    void* rax375;
    void** rbx376;
    void** rsi377;
    void** rax378;
    void** rcx379;
    void** r11_380;
    void** rdx381;
    void** rax382;
    void** r8_383;
    signed char al384;
    void** rdx385;
    void** rsi386;
    void** rcx387;
    int1_t zf388;
    signed char al389;
    struct s0* r14_390;
    void** rbp391;
    struct s0** rsp392;
    void** rax393;
    void** v394;
    uint32_t eax395;
    struct s0* v396;
    uint32_t v397;
    void** rcx398;
    void** r15_399;
    uint64_t rax400;
    int32_t esi401;
    int64_t rax402;
    void* rax403;
    void* rdx404;
    void* rax405;
    void** rdx406;
    struct s0* r13_407;
    void** rax408;
    uint64_t rdi409;
    uint1_t cf410;
    int64_t rbx411;
    void** rbx412;
    unsigned char al413;
    void* v414;
    struct s0* v415;
    uint32_t eax416;
    unsigned char v417;
    uint32_t v418;
    int64_t rax419;
    void* v420;
    void** rax421;
    void* rax422;
    void** rax423;
    void** rax424;
    void** rdi425;
    void** r15_426;
    void** rdi427;
    int64_t rbp428;
    int64_t rbx429;
    void** r15_430;
    int32_t eax431;
    uint1_t zf432;
    void** rax433;
    void** v434;
    void** rax435;
    void** rax436;
    int64_t rax437;
    void* v438;
    void** rax439;
    void** r14_440;
    void** rdi441;
    void** rax442;
    void** rax443;
    void** rax444;
    void** v445;
    void** v446;
    void** rdx447;
    void** v448;
    void** rdi449;
    void*** v450;
    void** v451;
    void** rax452;
    void* rax453;
    void** rax454;
    void** rdx455;
    void** rcx456;
    int32_t esi457;
    void** rax458;
    uint64_t rdi459;
    uint1_t cf460;
    unsigned char al461;
    void* r11_462;
    uint64_t v463;
    void** rax464;
    void** rdx465;
    unsigned char al466;
    int64_t v467;
    void** rdx468;
    void*** v469;
    void** v470;
    void** rax471;

    while (1) {
        rbp7 = rdi;
        rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8);
        v9 = rsi;
        v10 = rdx;
        rax11 = g28;
        v12 = rax11;
        zf13 = dev_debug == 0;
        if (!zf13) {
            rdi14 = stderr;
            rcx = rsi;
            fun_2a90(rdi14, 1, "[pollard-rho (%lu)] ", rcx, r8, r9, v15, v9, v16, v17, v18, v19);
            rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
        }
        *reinterpret_cast<int32_t*>(&rdx20) = 0;
        *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
        r15_21 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp8) + 64);
        *reinterpret_cast<int32_t*>(&rbx22) = 1;
        *reinterpret_cast<int32_t*>(&rbx22 + 4) = 0;
        r14_23 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp8) + 0x90);
        r12_24 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp8) + 0x80);
        r13_25 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp8) + 0x70);
        fun_2780(r12_24, r14_23, r12_24, r14_23);
        rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
        rax27 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp26) + 96);
        v16 = rax27;
        fun_28b0(rax27, 2);
        fun_28b0(r15_21, 2);
        rsp28 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp26) - 8 + 8 - 8 + 8);
        rax29 = reinterpret_cast<void**>(rsp28 + 80);
        v15 = rax29;
        fun_28b0(rax29, 2);
        fun_29f0(r13_25, 1);
        rsp30 = reinterpret_cast<void*>(rsp28 - 8 + 8 - 8 + 8);
        v17 = reinterpret_cast<void**>(1);
        while (v31 = reinterpret_cast<void**>(0x3d89), eax32 = fun_2aa0(rbp7, 1, rdx20, rcx, r8, r9), rsp33 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp30) - 8 + 8), !!eax32) {
            r15_34 = rbx22;
            rbx35 = r15_21;
            while (1) {
                fun_27b0(r12_24, rbx35, rbx35, rcx, r8, r9);
                fun_29d0(rbx35, r12_24, rbp7, rcx, r8, r9);
                fun_28c0(rbx35, rbx35, v9, rcx, r8, r9);
                fun_26c0(r12_24, v15, rbx35, rcx, r8, r9);
                fun_27b0(r14_23, r13_25, r12_24, rcx, r8, r9);
                rdx36 = rbp7;
                fun_29d0(r13_25, r14_23, rdx36, rcx, r8, r9);
                rsp33 = rsp33 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
                rax37 = r15_34;
                *reinterpret_cast<uint32_t*>(&rax38) = *reinterpret_cast<uint32_t*>(&rax37) & 31;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax38) + 4) = 0;
                if (rax38 != 1) {
                    --r15_34;
                    if (r15_34) 
                        continue;
                } else {
                    rdx36 = rbp7;
                    fun_2a40(r12_24, r13_25, rdx36, rcx, r8, r9);
                    eax39 = fun_2aa0(r12_24, 1, rdx36, rcx, r8, r9);
                    rsp40 = rsp33 - 8 + 8 - 8 + 8;
                    if (eax39) 
                        break;
                    fun_27a0(v16, rbx35, rdx36, rcx, r8, v16, rbx35, rdx36, rcx, r8);
                    rsp33 = rsp40 - 8 + 8;
                    --r15_34;
                    if (r15_34) 
                        continue;
                }
                fun_27a0(v15, rbx35, rdx36, rcx, r8, v15, rbx35, rdx36, rcx, r8);
                rsp41 = reinterpret_cast<void*>(rsp33 - 8 + 8);
                rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v17) + reinterpret_cast<unsigned char>(v17));
                v42 = rcx;
                if (v17) {
                    v43 = r14_23;
                    r14_44 = rbx35;
                    rbx45 = v9;
                    v46 = r13_25;
                    r13_47 = r15_34;
                    r15_48 = v17;
                    do {
                        ++r13_47;
                        fun_27b0(r12_24, r14_44, r14_44, rcx, r8, r9);
                        fun_29d0(r14_44, r12_24, rbp7, rcx, r8, r9);
                        rdx36 = rbx45;
                        fun_28c0(r14_44, r14_44, rdx36, rcx, r8, r9);
                        rsp41 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp41) - 8 + 8 - 8 + 8 - 8 + 8);
                    } while (r15_48 != r13_47);
                    rbx35 = r14_44;
                    r13_25 = v46;
                    r14_23 = v43;
                }
                fun_27a0(v16, rbx35, rdx36, rcx, r8, v16, rbx35, rdx36, rcx, r8);
                rsp33 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp41) - 8 + 8);
                r15_34 = v17;
                v17 = v42;
            }
            v18 = r15_34;
            r15_21 = rbx35;
            rbx49 = v9;
            v19 = r13_25;
            r13_50 = v16;
            v51 = r14_23;
            r14_52 = r12_24;
            r12_53 = v15;
            do {
                fun_27b0(r14_52, r13_50, r13_50, rcx, r8, r9);
                fun_29d0(r13_50, r14_52, rbp7, rcx, r8, r9);
                fun_28c0(r13_50, r13_50, rbx49, rcx, r8, r9);
                fun_26c0(r14_52, r12_53, r13_50, rcx, r8, r9);
                fun_2a40(r14_52, r14_52, rbp7, rcx, r8, r9);
                eax54 = fun_2aa0(r14_52, 1, rbp7, rcx, r8, r9);
                rsp40 = rsp40 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
            } while (!eax54);
            v15 = r12_53;
            r12_24 = r14_52;
            rdx55 = r12_24;
            v9 = rbx49;
            r14_23 = v51;
            v16 = r13_50;
            rbx22 = v18;
            r13_25 = v19;
            fun_2760(rbp7, rbp7, rdx55, rcx, r8, r9);
            eax56 = fun_2aa0(r12_24, 1, rdx55, rcx, r8, r9);
            rsp57 = reinterpret_cast<void*>(rsp40 - 8 + 8 - 8 + 8);
            if (reinterpret_cast<uint1_t>(eax56 < 0) | reinterpret_cast<uint1_t>(eax56 == 0) || (eax58 = fun_2aa0(r12_24, 0x17ded79, rdx55, rcx, r8, r9), rsp57 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp57) - 8 + 8), eax58 >= 0) && (al59 = mp_prime_p_part_0(r12_24, 0x17ded79, rdx55, rcx, r8, r9), rsp57 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp57) - 8 + 8), !al59)) {
                zf60 = dev_debug == 0;
                if (!zf60) {
                    rcx = stderr;
                    fun_2a80("[composite factor--restarting pollard-rho] ", 1, 43, rcx, r8, r9);
                    rsp57 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp57) - 8 + 8);
                }
                rdx55 = v10;
                mp_factor_using_pollard_rho(r12_24, v9 + 1, rdx55, rcx, r8, r9);
                rsp61 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp57) - 8 + 8);
            } else {
                mp_factor_insert(v10, r12_24, rdx55, rcx, r8, r9);
                rsp61 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp57) - 8 + 8);
            }
            eax62 = fun_2aa0(rbp7, 1, rdx55, rcx, r8, r9);
            rsp63 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) - 8 + 8);
            if (reinterpret_cast<uint1_t>(eax62 < 0) | reinterpret_cast<uint1_t>(eax62 == 0)) 
                goto addr_401c_23;
            eax64 = fun_2aa0(rbp7, 0x17ded79, rdx55, rcx, r8, r9);
            rsp63 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp63) - 8 + 8);
            if (eax64 < 0) 
                goto addr_405d_25;
            al65 = mp_prime_p_part_0(rbp7, 0x17ded79, rdx55, rcx, r8, r9);
            rsp63 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp63) - 8 + 8);
            if (al65) 
                goto addr_405d_25;
            addr_401c_23:
            fun_29d0(r15_21, r15_21, rbp7, rcx, r8, r9);
            fun_29d0(v15, v15, rbp7, rcx, r8, r9);
            rdx20 = rbp7;
            fun_29d0(v16, v16, rdx20, rcx, r8, r9);
            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp63) - 8 + 8 - 8 + 8 - 8 + 8);
        }
        addr_406a_27:
        r8_66 = r15_21;
        r9_67 = v16;
        rsi68 = r14_23;
        rdi69 = r13_25;
        rcx70 = v15;
        fun_2660();
        rdx71 = v31;
        rax72 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v12) - reinterpret_cast<unsigned char>(g28));
        if (!rax72) 
            break;
        fun_2740();
        rsp73 = reinterpret_cast<void*>(rsp33 - 8 - 8 - 8 + 8 + 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 72);
        rax74 = g28;
        v75 = rax74;
        if (!v76) 
            goto addr_413c_30;
        zf77 = dev_debug == 0;
        rbp7 = rdi69;
        r12_24 = rsi68;
        if (!zf77) {
            rcx70 = stderr;
            *reinterpret_cast<int32_t*>(&rdx71) = 17;
            *reinterpret_cast<int32_t*>(&rdx71 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rsi68) = 1;
            *reinterpret_cast<int32_t*>(&rsi68 + 4) = 0;
            fun_2a80("[trial division] ", 1, 17, rcx70, r8_66, r9_67);
            rsp73 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp73) - 8 + 8);
        }
        rdi78 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp73) + 16);
        r13_79 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp73) + 32);
        v80 = rdi78;
        fun_2ab0(rdi78, rsi68, rdx71, rcx70, r8_66, rdi78, rsi68, rdx71, rcx70, r8_66);
        rax81 = fun_25d0(rbp7, rbp7);
        rbx82 = rax81;
        rdx83 = rax81;
        fun_2840(rbp7, rbp7, rdx83, rcx70, r8_66, r9_67);
        rsp84 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp73) - 8 + 8 - 8 + 8 - 8 + 8);
        if (rbx82) {
            do {
                fun_29f0(r13_79, 2, r13_79, 2);
                mp_factor_insert(r12_24, r13_79, rdx83, rcx70, r8_66, r9_67);
                fun_2940(r13_79, r13_79, rdx83, rcx70, r8_66, r9_67);
                rsp84 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp84) - 8 + 8 - 8 + 8 - 8 + 8);
                --rbx82;
            } while (rbx82);
        }
        *reinterpret_cast<int32_t*>(&rbx22) = 1;
        *reinterpret_cast<int32_t*>(&rbx22 + 4) = 0;
        *reinterpret_cast<int32_t*>(&r15_21) = 3;
        *reinterpret_cast<int32_t*>(&r15_21 + 4) = 0;
        r13_25 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp84) + 32);
        while (1) {
            eax85 = fun_28f0(rbp7, r15_21, rdx83, rcx70, r8_66, r9_67);
            rsp86 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp84) - 8 + 8);
            if (!eax85) {
                do {
                    *reinterpret_cast<int32_t*>(&r14_23) = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbx22 + 1));
                    *reinterpret_cast<uint32_t*>(&rax87) = *reinterpret_cast<unsigned char*>(rbx22 + 0x10340);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax87) + 4) = 0;
                    r15_21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_21) + reinterpret_cast<uint64_t>(rax87));
                    rsi88 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_21) * reinterpret_cast<unsigned char>(r15_21));
                    eax89 = fun_2aa0(rbp7, rsi88, rdx83, rcx70, r8_66, r9_67);
                    rsp90 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp86) - 8 + 8);
                    if (eax89 < 0) 
                        goto addr_4268_39;
                    if (*reinterpret_cast<int32_t*>(&r14_23) == 0x29d) 
                        goto addr_4268_39;
                    *reinterpret_cast<int32_t*>(&rbx22) = *reinterpret_cast<int32_t*>(&r14_23);
                    *reinterpret_cast<int32_t*>(&rbx22 + 4) = 0;
                    eax91 = fun_28f0(rbp7, r15_21, rdx83, rcx70, r8_66, r9_67);
                    rsp86 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp90) - 8 + 8);
                } while (!eax91);
            }
            rdx83 = r15_21;
            fun_27e0(rbp7, rbp7, rdx83, rcx70, r8_66, r9_67);
            fun_29f0(r13_25, r15_21, r13_25, r15_21);
            mp_factor_insert(r12_24, r13_25, rdx83, rcx70, r8_66, r9_67);
            fun_2940(r13_25, r13_25, rdx83, rcx70, r8_66, r9_67);
            rsp84 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp86) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        }
        addr_4268_39:
        fun_2940(v80, rsi88, rdx83, rcx70, r8_66, r9_67);
        rdi69 = rbp7;
        eax92 = fun_2aa0(rdi69, 1, rdx83, rcx70, r8_66, r9_67);
        rsp73 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp90) - 8 + 8 - 8 + 8);
        if (!eax92) 
            goto addr_413c_30;
        zf93 = dev_debug == 0;
        if (!zf93) {
            rcx70 = stderr;
            *reinterpret_cast<int32_t*>(&rdx83) = 19;
            *reinterpret_cast<int32_t*>(&rdx83 + 4) = 0;
            fun_2a80("[is number prime?] ", 1, 19, rcx70, r8_66, r9_67);
            rdi69 = rbp7;
            eax92 = fun_2aa0(rdi69, 1, 19, rcx70, r8_66, r9_67);
            rsp73 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp73) - 8 + 8 - 8 + 8);
        }
        if (reinterpret_cast<uint1_t>(eax92 < 0) | reinterpret_cast<uint1_t>(eax92 == 0)) 
            goto addr_42b5_46;
        rdi69 = rbp7;
        eax94 = fun_2aa0(rdi69, 0x17ded79, rdx83, rcx70, r8_66, r9_67);
        rsp73 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp73) - 8 + 8);
        if (eax94 < 0) 
            goto addr_4318_48;
        rdi69 = rbp7;
        al95 = mp_prime_p_part_0(rdi69, 0x17ded79, rdx83, rcx70, r8_66, r9_67);
        rsp73 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp73) - 8 + 8);
        if (al95) 
            goto addr_4318_48;
        addr_42b5_46:
        rax96 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v75) - reinterpret_cast<unsigned char>(g28));
        if (rax96) 
            goto addr_4377_50;
        continue;
        addr_405d_25:
        v31 = reinterpret_cast<void**>(0x406a);
        mp_factor_insert(v10, rbp7, rdx55, rcx, r8, r9);
        rsp33 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp63) - 8 + 8);
        goto addr_406a_27;
    }
    return;
    addr_413c_30:
    rax97 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v75) - reinterpret_cast<unsigned char>(g28));
    if (!rax97) {
        goto v15;
    }
    addr_4318_48:
    rax98 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v75) - reinterpret_cast<unsigned char>(g28));
    if (rax98) {
        addr_4377_50:
        fun_2740();
    } else {
        r12_99 = rbp7;
        rdx100 = reinterpret_cast<void**>(0);
        v101 = 0;
        if (1) 
            goto addr_37ca_56; else 
            goto addr_361b_57;
    }
    rbx102 = rdi69;
    rsp103 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp73) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x98);
    rax104 = g28;
    v105 = rax104;
    r12_106 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp103) + 96);
    r15_107 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp103) + 64);
    rbp108 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp103) + 80);
    r14_109 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp103) + 0x70);
    v110 = r15_107;
    fun_2780(r15_107, rbp108, r15_107, rbp108);
    fun_2650(r12_106, rbx102, 1, r14_109);
    rax111 = fun_25d0(r12_106);
    r13_112 = rax111;
    v113 = rax111;
    fun_2960(r15_107, r12_106, rax111, r14_109);
    fun_2680(rbp108, 2, rax111, r14_109);
    r9_114 = r13_112;
    r8_115 = r15_107;
    rcx116 = r14_109;
    al117 = mp_millerrabin(rbx102, r12_106, rbp108, rcx116, r8_115, r9_114);
    rsp118 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp103) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    if (al117) {
        r13_112 = reinterpret_cast<void**>(0x10340);
        fun_27a0(r14_109, r12_106, rbp108, rcx116, r8_115, r14_109, r12_106, rbp108, rcx116, r8_115);
        rsp119 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp118) - 8 + 8);
        rsi120 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp119) + 32);
        mp_factor(r14_109, rsi120, rbp108, rcx116, r8_115, r9_114);
        rsp121 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp119) - 8 + 8);
        goto addr_4458_60;
    }
    addr_455c_61:
    rdi122 = v110;
    rcx123 = r14_109;
    rdx124 = r12_106;
    rsi125 = rbp108;
    fun_2660();
    rax126 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v105) - reinterpret_cast<unsigned char>(g28));
    if (!rax126) {
        goto v127;
    }
    fun_2740();
    rsp128 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp118) - 8 + 8 - 8 + 8);
    while (1) {
        r15_129 = rdi122;
        rbp130 = rsi125;
        rsp131 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp128) - 8 - 8 - 8 - 8 - 8 - 8 - 0x88);
        v132 = rdx124;
        v133 = rcx123;
        rax134 = g28;
        v135 = rax134;
        rcx136 = reinterpret_cast<unsigned char>(rcx123) - (reinterpret_cast<unsigned char>(rcx123) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx123) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx123) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi122) < reinterpret_cast<unsigned char>(2)))))));
        *reinterpret_cast<int32_t*>(&rdx137) = 0;
        *reinterpret_cast<int32_t*>(&rdx137 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx138) = *reinterpret_cast<uint32_t*>(&rcx136) & 64;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx138) + 4) = 0;
        rcx139 = reinterpret_cast<uint64_t>(rcx138 + 63);
        cf140 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi122) < reinterpret_cast<unsigned char>(2));
        *reinterpret_cast<unsigned char*>(&rdx137) = cf140;
        rax141 = reinterpret_cast<void**>(-static_cast<uint64_t>(reinterpret_cast<uint1_t>(0 < static_cast<uint64_t>(cf140))) + 1);
        do {
            rsi142 = rdx137;
            rdx137 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx137) + reinterpret_cast<unsigned char>(rdx137));
            rax141 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax141) + reinterpret_cast<unsigned char>(rax141)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rsi142) >> 63));
            if (reinterpret_cast<unsigned char>(r15_129) < reinterpret_cast<unsigned char>(rax141) || r15_129 == rax141 && reinterpret_cast<unsigned char>(rbp130) <= reinterpret_cast<unsigned char>(rdx137)) {
                cf143 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx137) < reinterpret_cast<unsigned char>(rbp130));
                rdx137 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx137) - reinterpret_cast<unsigned char>(rbp130));
                rax141 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax141) - (reinterpret_cast<unsigned char>(r15_129) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax141) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r15_129) + static_cast<uint64_t>(cf143))))));
            }
            cf144 = rcx139 < 1;
            --rcx139;
        } while (!cf144);
        v145 = rdx137;
        v146 = rax141;
        tmp64_147 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx137) + reinterpret_cast<unsigned char>(rdx137));
        rax148 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax141) + reinterpret_cast<unsigned char>(rax141) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_147) < reinterpret_cast<unsigned char>(rdx137))));
        r12_149 = tmp64_147;
        r10_150 = rax148;
        if (reinterpret_cast<unsigned char>(rax148) > reinterpret_cast<unsigned char>(r15_129) || rax148 == r15_129 && reinterpret_cast<unsigned char>(tmp64_147) >= reinterpret_cast<unsigned char>(rbp130)) {
            r12_149 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_147) - reinterpret_cast<unsigned char>(rbp130));
            r10_150 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax148) - (reinterpret_cast<unsigned char>(r15_129) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax148) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r15_129) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_147) < reinterpret_cast<unsigned char>(rbp130))))))));
        }
        v151 = r10_150;
        v152 = r10_150;
        if (r15_129) 
            goto addr_468c_71;
        if (rbp130 == 1) 
            goto addr_4ad5_73;
        addr_468c_71:
        v153 = r12_149;
        r13_112 = r15_129;
        *reinterpret_cast<int32_t*>(&rbx102) = 1;
        *reinterpret_cast<int32_t*>(&rbx102 + 4) = 0;
        v154 = reinterpret_cast<void**>(1);
        r14_155 = reinterpret_cast<void**>(rsp131 + 0x70);
        r15_156 = r12_149;
        v157 = rsp131 + 0x68;
        while (1) {
            r9_158 = r13_112;
            r13_112 = rbx102;
            rax159 = reinterpret_cast<unsigned char>(rbp130) >> 1;
            rbx102 = rbp130;
            *reinterpret_cast<uint32_t*>(&rax160) = *reinterpret_cast<uint32_t*>(&rax159) & 0x7f;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax160) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rax161) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax160);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax161) + 4) = 0;
            rdx162 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax161) + reinterpret_cast<int64_t>(rax161) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax161) * reinterpret_cast<int64_t>(rax161) * reinterpret_cast<unsigned char>(rbp130)));
            rax163 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx162) + reinterpret_cast<uint64_t>(rdx162) - reinterpret_cast<uint64_t>(rdx162) * reinterpret_cast<uint64_t>(rdx162) * reinterpret_cast<unsigned char>(rbp130));
            rax164 = rbp130;
            rbp165 = r10_150;
            *reinterpret_cast<uint32_t*>(&rax166) = *reinterpret_cast<uint32_t*>(&rax164) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax166) + 4) = 0;
            v167 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax163) + reinterpret_cast<uint64_t>(rax163) - reinterpret_cast<uint64_t>(rax163) * reinterpret_cast<uint64_t>(rax163) * reinterpret_cast<unsigned char>(rbp130));
            v168 = rax166;
            while (1) {
                rax169 = mulredc2(r14_155, rbp165, r12_149, rbp165, r12_149, r9_158, rbx102, v146);
                tmp64_170 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax169) + reinterpret_cast<unsigned char>(v132));
                rdx171 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v172) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_170) < reinterpret_cast<unsigned char>(rax169))));
                v173 = rdx171;
                r12_149 = tmp64_170;
                rbp165 = rdx171;
                if (reinterpret_cast<unsigned char>(rdx171) > reinterpret_cast<unsigned char>(r9_158) || rdx171 == r9_158 && reinterpret_cast<unsigned char>(tmp64_170) >= reinterpret_cast<unsigned char>(rbx102)) {
                    rdx174 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx171) - (reinterpret_cast<unsigned char>(r9_158) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx171) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r9_158) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_170) < reinterpret_cast<unsigned char>(rbx102))))))));
                    v173 = rdx174;
                    r12_149 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_170) - reinterpret_cast<unsigned char>(rbx102));
                    rbp165 = rdx174;
                }
                r8_175 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v153) - reinterpret_cast<unsigned char>(r12_149));
                rcx176 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v151) - (reinterpret_cast<unsigned char>(rbp165) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v151) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp165) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v153) < reinterpret_cast<unsigned char>(r12_149))))))));
                if (reinterpret_cast<signed char>(rcx176) < reinterpret_cast<signed char>(0)) {
                    tmp64_177 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_175) + reinterpret_cast<unsigned char>(rbx102));
                    cf178 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_177) < reinterpret_cast<unsigned char>(r8_175));
                    r8_175 = tmp64_177;
                    rcx176 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx176) + reinterpret_cast<unsigned char>(r9_158) + static_cast<uint64_t>(cf178));
                }
                rax179 = mulredc2(r14_155, v146, v145, rcx176, r8_175, r9_158, rbx102, v146);
                v145 = rax179;
                v146 = v180;
                rax181 = r13_112;
                *reinterpret_cast<uint32_t*>(&rax182) = *reinterpret_cast<uint32_t*>(&rax181) & 31;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax182) + 4) = 0;
                rsp131 = rsp131 - 8 - 8 - 8 + 8 + 8 + 8 - 8 - 8 - 8 + 8 + 8 + 8;
                r9_158 = r9_158;
                if (rax182 == 1) {
                    if (!v168) 
                        goto addr_4cba_81;
                    if (reinterpret_cast<unsigned char>(v145) | reinterpret_cast<unsigned char>(v146)) 
                        goto addr_48c0_83;
                } else {
                    addr_47d9_84:
                    --r13_112;
                    if (r13_112) 
                        continue; else 
                        goto addr_47e3_85;
                }
                v183 = r9_158;
                rax184 = rbx102;
                if (r9_158) 
                    break;
                addr_48ee_87:
                if (!reinterpret_cast<int1_t>(rax184 == 1)) 
                    goto addr_4920_88;
                v152 = rbp165;
                r15_156 = r12_149;
                goto addr_47d9_84;
                addr_48c0_83:
                rax184 = gcd2_odd_part_0(v157, v146, v145, r9_158, rbx102, r9_158);
                rsp131 = rsp131 - 8 + 8;
                r9_158 = r9_158;
                if (v183) 
                    goto addr_4920_88; else 
                    goto addr_48ee_87;
                addr_47e3_85:
                v151 = rbp165;
                r15_156 = r12_149;
                v185 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v154) + reinterpret_cast<unsigned char>(v154));
                if (v154) {
                    v186 = r12_149;
                    rdx187 = r12_149;
                    r15_188 = r13_112;
                    r12_189 = v167;
                    r13_190 = v132;
                    rsi191 = rbp165;
                    rbp192 = r9_158;
                    do {
                        rax193 = mulredc2(r14_155, rsi191, rdx187, rsi191, rdx187, rbp192, rbx102, r12_189);
                        tmp64_194 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax193) + reinterpret_cast<unsigned char>(r13_190));
                        rcx195 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v196) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_194) < reinterpret_cast<unsigned char>(rax193))));
                        rdx187 = tmp64_194;
                        rsi191 = rcx195;
                        rsp131 = rsp131 - 8 - 8 - 8 + 8 + 8 + 8;
                        if (reinterpret_cast<unsigned char>(rcx195) > reinterpret_cast<unsigned char>(rbp192) || rcx195 == rbp192 && reinterpret_cast<unsigned char>(tmp64_194) >= reinterpret_cast<unsigned char>(rbx102)) {
                            rdx187 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_194) - reinterpret_cast<unsigned char>(rbx102));
                            rsi191 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx195) - (reinterpret_cast<unsigned char>(rbp192) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx195) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp192) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_194) < reinterpret_cast<unsigned char>(rbx102))))))));
                        }
                        ++r15_188;
                    } while (v154 != r15_188);
                    r12_149 = v186;
                    r9_158 = rbp192;
                    r15_156 = rdx187;
                    rbp165 = rsi191;
                }
                r13_112 = v154;
                v153 = r12_149;
                r12_149 = r15_156;
                v152 = rbp165;
                v154 = v185;
            }
            addr_4920_88:
            v197 = r12_149;
            r11_198 = r15_156;
            r10_199 = v152;
            v200 = r13_112;
            r15_201 = r14_155;
            r13_112 = rbx102;
            r12_202 = v153;
            r14_155 = v167;
            rbp203 = v157;
            rbx102 = r9_158;
            do {
                r9_204 = rbx102;
                rax205 = mulredc2(r15_201, r10_199, r11_198, r10_199, r11_198, r9_204, r13_112, r14_155);
                tmp64_206 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax205) + reinterpret_cast<unsigned char>(v132));
                rdx207 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v208) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_206) < reinterpret_cast<unsigned char>(rax205))));
                v152 = rdx207;
                r11_198 = tmp64_206;
                rcx209 = r13_112;
                r10_199 = rdx207;
                rsp131 = rsp131 - 8 - 8 - 8 + 8 + 8 + 8;
                if (reinterpret_cast<unsigned char>(rdx207) > reinterpret_cast<unsigned char>(rbx102) || rdx207 == rbx102 && reinterpret_cast<unsigned char>(tmp64_206) >= reinterpret_cast<unsigned char>(r13_112)) {
                    rdx210 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx207) - (reinterpret_cast<unsigned char>(rbx102) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx207) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbx102) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_206) < reinterpret_cast<unsigned char>(r13_112))))))));
                    v152 = rdx210;
                    r11_198 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_206) - reinterpret_cast<unsigned char>(r13_112));
                    r10_199 = rdx210;
                }
                rdx211 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_202) - reinterpret_cast<unsigned char>(r11_198));
                rsi212 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v151) - (reinterpret_cast<unsigned char>(r10_199) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v151) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r10_199) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_202) < reinterpret_cast<unsigned char>(r11_198))))))));
                if (reinterpret_cast<signed char>(rsi212) < reinterpret_cast<signed char>(0)) {
                    tmp64_213 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx211) + reinterpret_cast<unsigned char>(r13_112));
                    cf214 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_213) < reinterpret_cast<unsigned char>(rdx211));
                    rdx211 = tmp64_213;
                    rsi212 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi212) + reinterpret_cast<unsigned char>(rbx102) + static_cast<uint64_t>(cf214));
                }
                if (reinterpret_cast<unsigned char>(rsi212) | reinterpret_cast<unsigned char>(rdx211)) {
                    rcx209 = rbx102;
                    rax215 = gcd2_odd_part_0(rbp203, rsi212, rdx211, rcx209, r13_112, r9_204);
                    rsp131 = rsp131 - 8 + 8;
                    rdi216 = v183;
                    if (rdi216) 
                        goto addr_4a05_103;
                } else {
                    rdi216 = rbx102;
                    v183 = rbx102;
                    rax215 = r13_112;
                    if (rdi216) 
                        goto addr_4a05_103;
                }
            } while (reinterpret_cast<int1_t>(rax215 == 1));
            r8_217 = rax215;
            rax218 = reinterpret_cast<unsigned char>(rax215) >> 1;
            *reinterpret_cast<uint32_t*>(&rax219) = *reinterpret_cast<uint32_t*>(&rax218) & 0x7f;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax219) + 4) = 0;
            r13_220 = rbx102;
            r14_155 = r15_201;
            *reinterpret_cast<uint32_t*>(&rax221) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax219);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax221) + 4) = 0;
            rbx102 = v200;
            rdx222 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax221) + reinterpret_cast<int64_t>(rax221) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax221) * reinterpret_cast<int64_t>(rax221) * reinterpret_cast<unsigned char>(r8_217)));
            rdx223 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdx222) * reinterpret_cast<uint64_t>(rdx222) * reinterpret_cast<unsigned char>(r8_217));
            rax224 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx222) + reinterpret_cast<uint64_t>(rdx222) - reinterpret_cast<unsigned char>(rdx223));
            rcx209 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax224) + reinterpret_cast<uint64_t>(rax224) - reinterpret_cast<uint64_t>(rax224) * reinterpret_cast<uint64_t>(rax224) * reinterpret_cast<unsigned char>(r8_217));
            rbp130 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_112) * reinterpret_cast<unsigned char>(rcx209));
            if (reinterpret_cast<unsigned char>(r13_220) < reinterpret_cast<unsigned char>(r8_217)) {
                *reinterpret_cast<int32_t*>(&r13_112) = 0;
                *reinterpret_cast<int32_t*>(&r13_112 + 4) = 0;
            } else {
                rdx223 = __intrinsic();
                r9_204 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r13_220) - reinterpret_cast<unsigned char>(rdx223)) * reinterpret_cast<unsigned char>(rcx209));
                r13_112 = r9_204;
            }
            if (reinterpret_cast<unsigned char>(r8_217) <= reinterpret_cast<unsigned char>(1) || reinterpret_cast<unsigned char>(r8_217) > reinterpret_cast<unsigned char>(0x17ded78) && (al225 = prime_p_part_0(r8_217, rsi212, rdx223, rcx209), rsp131 = rsp131 - 8 + 8, r8_217 = r8_217, al225 == 0)) {
                rdx226 = v133;
                rsi227 = v132 + 1;
                factor_using_pollard_rho(r8_217, rsi227, rdx226, rcx209);
                rsp131 = rsp131 - 8 + 8;
            } else {
                *reinterpret_cast<int32_t*>(&rdx226) = 1;
                *reinterpret_cast<int32_t*>(&rdx226 + 4) = 0;
                rsi227 = r8_217;
                factor_insert_multiplicity(v133, rsi227, 1);
                rsp131 = rsp131 - 8 + 8;
            }
            if (!r13_112) 
                goto addr_4aa0_113;
            rsi227 = rbp130;
            rdi228 = r13_112;
            al229 = prime2_p(rdi228, rsi227);
            rsp131 = rsp131 - 8 + 8;
            if (al229) 
                goto addr_4c8b_115;
            rax230 = mod2(rsp131 + 80, v173, v197, r13_112, rbp130, r9_204);
            rsp231 = reinterpret_cast<void*>(rsp131 - 8 + 8);
            r12_149 = rax230;
            rax232 = mod2(reinterpret_cast<int64_t>(rsp231) + 88, v151, v153, r13_112, rbp130, r9_204);
            rsp233 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp231) - 8 + 8);
            v153 = rax232;
            rax234 = mod2(reinterpret_cast<int64_t>(rsp233) + 96, v152, r11_198, r13_112, rbp130, r9_204);
            rsp131 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp233) - 8 + 8);
            r10_150 = v173;
            r15_156 = rax234;
        }
        addr_4a05_103:
        if (rbx102 != rdi216) 
            goto addr_4a19_117;
        if (rax215 == r13_112) 
            goto addr_4ce0_119;
        addr_4a19_117:
        rbx102 = reinterpret_cast<void**>(0xd5c0);
        rsi227 = rax215;
        v154 = rax215;
        rax235 = reinterpret_cast<unsigned char>(rax215) >> 1;
        *reinterpret_cast<uint32_t*>(&rax236) = *reinterpret_cast<uint32_t*>(&rax235) & 0x7f;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax236) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax237) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(0xd5c0) + reinterpret_cast<uint64_t>(rax236));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax237) + 4) = 0;
        rdx238 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax237) + reinterpret_cast<int64_t>(rax237) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax237) * reinterpret_cast<int64_t>(rax237) * reinterpret_cast<unsigned char>(rax215)));
        rax239 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx238) + reinterpret_cast<uint64_t>(rdx238) - reinterpret_cast<uint64_t>(rdx238) * reinterpret_cast<uint64_t>(rdx238) * reinterpret_cast<unsigned char>(rax215));
        rdx226 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax239) + reinterpret_cast<uint64_t>(rax239) - reinterpret_cast<uint64_t>(rax239) * reinterpret_cast<uint64_t>(rax239) * reinterpret_cast<unsigned char>(rax215));
        rbp130 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_112) * reinterpret_cast<unsigned char>(rdx226));
        al240 = prime2_p(rdi216, rsi227);
        rsp131 = rsp131 - 8 + 8;
        if (!al240) 
            goto addr_4c4f_120;
        if (!v183) 
            goto addr_4ca3_122;
        rdi228 = v133;
        if (!*reinterpret_cast<void***>(rdi228 + 8)) 
            goto addr_4a94_124;
        addr_4cfe_125:
        factor_insert_large_part_0();
        r8_241 = rdi228;
        r9_242 = rdx226;
        v243 = r13_112;
        rbp244 = rsi227;
        v245 = rbx102;
        rsp246 = rsp131 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 56;
        *reinterpret_cast<unsigned char*>(rdx226 + 0xfa) = 0;
        *reinterpret_cast<void***>(rdx226 + 8) = reinterpret_cast<void**>(0);
        if (rdi228) 
            goto addr_4d50_127;
        if (reinterpret_cast<unsigned char>(rsi227) <= reinterpret_cast<unsigned char>(1)) 
            goto addr_4d41_129;
        addr_4d50_127:
        if (*reinterpret_cast<unsigned char*>(&rbp244) & 1) {
            addr_4df3_130:
            if (!r8_241) {
                *reinterpret_cast<int32_t*>(&r12_247) = 3;
                *reinterpret_cast<int32_t*>(&r12_247 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r13_248) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_248) + 4) = 0;
                r11_249 = reinterpret_cast<void**>(0x5555555555555555);
                r10_250 = reinterpret_cast<void**>(0xaaaaaaaaaaaaaaab);
            } else {
                r14_251 = reinterpret_cast<void***>(0xd648);
                *reinterpret_cast<uint32_t*>(&rbx252) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx252) + 4) = 0;
                r15_253 = 0xaaaaaaaaaaaaaaab;
                *reinterpret_cast<int32_t*>(&r12_247) = 3;
                *reinterpret_cast<int32_t*>(&r12_247 + 4) = 0;
                while (1) {
                    rcx254 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp244) * r15_253);
                    rdx255 = __intrinsic();
                    if (reinterpret_cast<unsigned char>(rdx255) <= reinterpret_cast<unsigned char>(r8_241)) {
                        r10_256 = *r14_251;
                        while ((r13_257 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(r8_241) - reinterpret_cast<unsigned char>(rdx255)) * r15_253), reinterpret_cast<unsigned char>(r13_257) <= reinterpret_cast<unsigned char>(r10_256)) && (factor_insert_multiplicity(r9_242, r12_247, 1), rsp246 = rsp246 - 8 + 8, r8_241 = r13_257, r9_242 = r9_242, r10_256 = r10_256, rbp244 = rcx254, rdx255 = __intrinsic(), reinterpret_cast<unsigned char>(rdx255) <= reinterpret_cast<unsigned char>(r13_257))) {
                            rcx254 = reinterpret_cast<void**>(r15_253 * reinterpret_cast<unsigned char>(rcx254));
                        }
                    }
                    *reinterpret_cast<uint32_t*>(&rax258) = rbx252->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax258) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&r13_248) = *reinterpret_cast<uint32_t*>(&rbx252);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_248) + 4) = 0;
                    r14_251 = r14_251 + 16;
                    r12_247 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_247) + reinterpret_cast<uint64_t>(rax258));
                    if (!r8_241) 
                        break;
                    if (*reinterpret_cast<uint32_t*>(&rbx252) > 0x29b) 
                        break;
                    r15_253 = *reinterpret_cast<uint64_t*>(r14_251 - 8);
                    rbx252 = reinterpret_cast<struct s16*>(&rbx252->pad8);
                }
                if (*reinterpret_cast<uint32_t*>(&r13_248) > 0x29b) 
                    goto addr_5200_141; else 
                    goto addr_4edd_142;
            }
        } else {
            if (rbp244) {
                __asm__("bsf rdx, rbp");
                ecx259 = 64 - *reinterpret_cast<int32_t*>(&rdx226);
                ecx260 = *reinterpret_cast<int32_t*>(&rdx226);
                rbp244 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp244) >> *reinterpret_cast<signed char*>(&ecx260)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_241) << *reinterpret_cast<unsigned char*>(&ecx259)));
                factor_insert_multiplicity(r9_242, 2, *reinterpret_cast<signed char*>(&rdx226));
                rsp246 = rsp246 - 8 + 8;
                r8_241 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_241) >> *reinterpret_cast<signed char*>(&ecx260));
                r9_242 = r9_242;
                goto addr_4df3_130;
            } else {
                __asm__("bsf rcx, r8");
                edx261 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rcx209 + 64));
                rbp244 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_241) >> *reinterpret_cast<signed char*>(&rcx209));
                *reinterpret_cast<int32_t*>(&r12_247) = 3;
                *reinterpret_cast<int32_t*>(&r12_247 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r13_248) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_248) + 4) = 0;
                factor_insert_multiplicity(r9_242, 2, *reinterpret_cast<signed char*>(&edx261));
                rsp246 = rsp246 - 8 + 8;
                r9_242 = r9_242;
                *reinterpret_cast<uint32_t*>(&r8_241) = 0;
                *reinterpret_cast<int32_t*>(&r8_241 + 4) = 0;
                r11_249 = reinterpret_cast<void**>(0x5555555555555555);
                r10_250 = reinterpret_cast<void**>(0xaaaaaaaaaaaaaaab);
            }
        }
        addr_4ef9_146:
        *reinterpret_cast<int32_t*>(&rbx262) = static_cast<int32_t>(r13_248 + 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx262) + 4) = 0;
        r13_248 = rbx262;
        rbx252 = reinterpret_cast<struct s16*>((rbx262 << 4) + reinterpret_cast<unsigned char>(0xd640));
        while (1) {
            *reinterpret_cast<int32_t*>(&r15_253) = static_cast<int32_t>(r13_248 - 1);
            rcx263 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp244) * reinterpret_cast<unsigned char>(r10_250));
            if (reinterpret_cast<unsigned char>(r11_249) >= reinterpret_cast<unsigned char>(rcx263)) {
                do {
                    factor_insert_multiplicity(r9_242, r12_247, 1);
                    rsp246 = rsp246 - 8 + 8;
                    r10_250 = r10_250;
                    r11_249 = r11_249;
                    r9_242 = r9_242;
                    rbp244 = rcx263;
                    rcx263 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx263) * reinterpret_cast<unsigned char>(r10_250));
                    r8_241 = r8_241;
                } while (reinterpret_cast<unsigned char>(r11_249) >= reinterpret_cast<unsigned char>(rcx263));
                rdx264 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx252->f0) * reinterpret_cast<unsigned char>(rbp244));
                if (reinterpret_cast<unsigned char>(rbx252->f8) < reinterpret_cast<unsigned char>(rdx264)) 
                    goto addr_4f29_150;
                goto addr_5050_152;
            }
            rdx264 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx252->f0) * reinterpret_cast<unsigned char>(rbp244));
            if (reinterpret_cast<unsigned char>(rbx252->f8) >= reinterpret_cast<unsigned char>(rdx264)) {
                addr_5050_152:
                *reinterpret_cast<uint32_t*>(&rcx265) = *reinterpret_cast<uint32_t*>(&r13_248);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx265) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rcx266) = rcx265->f10340;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx266) + 4) = 0;
                rcx267 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx266) + reinterpret_cast<unsigned char>(r12_247));
            } else {
                addr_4f29_150:
                rdx268 = reinterpret_cast<void**>(rbx252->f10 * reinterpret_cast<unsigned char>(rbp244));
                if (reinterpret_cast<unsigned char>(rdx268) <= reinterpret_cast<unsigned char>(rbx252->f18)) 
                    goto addr_50b0_154; else 
                    goto addr_4f3b_155;
            }
            do {
                rbp244 = rdx264;
                factor_insert_multiplicity(r9_242, rcx267, 1);
                rsp246 = rsp246 - 8 + 8;
                r9_242 = r9_242;
                rcx267 = rcx267;
                r8_241 = r8_241;
                rdx264 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx252->f0) * reinterpret_cast<unsigned char>(rbp244));
            } while (reinterpret_cast<unsigned char>(rdx264) <= reinterpret_cast<unsigned char>(rbx252->f8));
            rdx268 = reinterpret_cast<void**>(rbx252->f10 * reinterpret_cast<unsigned char>(rbp244));
            if (reinterpret_cast<unsigned char>(rdx268) > reinterpret_cast<unsigned char>(rbx252->f18)) {
                addr_4f3b_155:
                rdx269 = reinterpret_cast<void**>(rbx252->f20 * reinterpret_cast<unsigned char>(rbp244));
                if (reinterpret_cast<unsigned char>(rdx269) <= reinterpret_cast<unsigned char>(rbx252->f28)) {
                    addr_5120_158:
                    *reinterpret_cast<int32_t*>(&rcx270) = static_cast<int32_t>(r13_248 + 2);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx270) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi271) = *reinterpret_cast<uint32_t*>(&r13_248);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi271) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi272) = rsi271->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi272) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rcx273) = rcx270->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx273) + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rsi274) = static_cast<int32_t>(r13_248 + 1);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi274) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi275) = rsi274->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi275) + 4) = 0;
                    rcx276 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx273) + reinterpret_cast<int64_t>(rsi272) + reinterpret_cast<unsigned char>(r12_247) + reinterpret_cast<uint64_t>(rsi275));
                } else {
                    addr_4f4d_159:
                    rdx277 = reinterpret_cast<void**>(rbx252->f30 * reinterpret_cast<unsigned char>(rbp244));
                    if (reinterpret_cast<unsigned char>(rdx277) <= reinterpret_cast<unsigned char>(rbx252->f38)) 
                        goto addr_5198_160; else 
                        goto addr_4f5f_161;
                }
            } else {
                goto addr_50b0_154;
            }
            do {
                rbp244 = rdx269;
                v278 = rcx276;
                factor_insert_multiplicity(r9_242, rcx276, 1);
                rsp246 = rsp246 - 8 + 8;
                r9_242 = r9_242;
                rcx276 = v278;
                r8_241 = r8_241;
                rdx269 = reinterpret_cast<void**>(rbx252->f20 * reinterpret_cast<unsigned char>(rbp244));
            } while (reinterpret_cast<unsigned char>(rdx269) <= reinterpret_cast<unsigned char>(rbx252->f28));
            rdx277 = reinterpret_cast<void**>(rbx252->f30 * reinterpret_cast<unsigned char>(rbp244));
            if (reinterpret_cast<unsigned char>(rdx277) > reinterpret_cast<unsigned char>(rbx252->f38)) {
                addr_4f5f_161:
                rdx279 = reinterpret_cast<void**>(rbx252->f40 * reinterpret_cast<unsigned char>(rbp244));
                if (reinterpret_cast<unsigned char>(rdx279) <= reinterpret_cast<unsigned char>(rbx252->f48)) {
                    rbp244 = rdx279;
                    ecx280 = static_cast<uint32_t>(r13_248 + 5);
                    while (1) {
                        edx281 = *reinterpret_cast<uint32_t*>(&r13_248);
                        rsi282 = r12_247;
                        do {
                            *reinterpret_cast<uint32_t*>(&rdi283) = edx281;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi283) + 4) = 0;
                            ++edx281;
                            *reinterpret_cast<uint32_t*>(&rdi284) = rdi283->f10340;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi284) + 4) = 0;
                            rsi282 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi282) + reinterpret_cast<uint64_t>(rdi284));
                        } while (edx281 != ecx280);
                        v278 = r8_241;
                        factor_insert_multiplicity(r9_242, rsi282, 1);
                        rsp246 = rsp246 - 8 + 8;
                        r9_242 = r9_242;
                        r8_241 = v278;
                        ecx280 = ecx280;
                        rdx285 = reinterpret_cast<void**>(rbx252->f40 * reinterpret_cast<unsigned char>(rbp244));
                        if (reinterpret_cast<unsigned char>(rdx285) > reinterpret_cast<unsigned char>(rbx252->f48)) 
                            break;
                        rbp244 = rdx285;
                    }
                }
            } else {
                goto addr_5198_160;
            }
            rdx286 = reinterpret_cast<void**>(rbx252->f50 * reinterpret_cast<unsigned char>(rbp244));
            if (reinterpret_cast<unsigned char>(rdx286) <= reinterpret_cast<unsigned char>(rbx252->f58)) {
                rbp244 = rdx286;
                ecx287 = static_cast<uint32_t>(r13_248 + 6);
                while (1) {
                    edx288 = *reinterpret_cast<uint32_t*>(&r13_248);
                    rsi289 = r12_247;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdi290) = edx288;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi290) + 4) = 0;
                        ++edx288;
                        *reinterpret_cast<uint32_t*>(&rdi291) = rdi290->f10340;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi291) + 4) = 0;
                        rsi289 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi289) + reinterpret_cast<uint64_t>(rdi291));
                    } while (edx288 != ecx287);
                    v278 = r8_241;
                    factor_insert_multiplicity(r9_242, rsi289, 1);
                    rsp246 = rsp246 - 8 + 8;
                    r9_242 = r9_242;
                    r8_241 = v278;
                    ecx287 = ecx287;
                    rdx292 = reinterpret_cast<void**>(rbx252->f50 * reinterpret_cast<unsigned char>(rbp244));
                    if (reinterpret_cast<unsigned char>(rdx292) > reinterpret_cast<unsigned char>(rbx252->f58)) 
                        break;
                    rbp244 = rdx292;
                }
            }
            rdx293 = reinterpret_cast<void**>(rbx252->f60 * reinterpret_cast<unsigned char>(rbp244));
            if (reinterpret_cast<unsigned char>(rdx293) <= reinterpret_cast<unsigned char>(rbx252->f68)) {
                rbp244 = rdx293;
                ecx294 = static_cast<uint32_t>(r13_248 + 7);
                while (1) {
                    edx295 = *reinterpret_cast<uint32_t*>(&r13_248);
                    rsi296 = r12_247;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdi297) = edx295;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi297) + 4) = 0;
                        ++edx295;
                        *reinterpret_cast<uint32_t*>(&rdi298) = rdi297->f10340;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi298) + 4) = 0;
                        rsi296 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi296) + reinterpret_cast<uint64_t>(rdi298));
                    } while (edx295 != ecx294);
                    v278 = r8_241;
                    factor_insert_multiplicity(r9_242, rsi296, 1);
                    rsp246 = rsp246 - 8 + 8;
                    r9_242 = r9_242;
                    r8_241 = v278;
                    ecx294 = ecx294;
                    rdx299 = reinterpret_cast<void**>(rbx252->f60 * reinterpret_cast<unsigned char>(rbp244));
                    if (reinterpret_cast<unsigned char>(rdx299) > reinterpret_cast<unsigned char>(rbx252->f68)) 
                        break;
                    rbp244 = rdx299;
                }
            }
            *reinterpret_cast<int32_t*>(&rax300) = *reinterpret_cast<int32_t*>(&r15_253);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax300) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rax301) = *reinterpret_cast<unsigned char*>(0x10080 + rax300);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax301) + 4) = 0;
            r12_247 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_247) + reinterpret_cast<uint64_t>(rax301));
            if (reinterpret_cast<unsigned char>(rbp244) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r12_247) * reinterpret_cast<unsigned char>(r12_247))) 
                break;
            *reinterpret_cast<uint32_t*>(&rax302) = static_cast<uint32_t>(r13_248 + 7);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax302) + 4) = 0;
            rbx252 = reinterpret_cast<struct s16*>(reinterpret_cast<uint64_t>(rbx252) + 0x80);
            *reinterpret_cast<uint32_t*>(&r13_248) = *reinterpret_cast<uint32_t*>(&r13_248) + 8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_248) + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&rax302) > 0x29b) 
                break;
            rax303 = reinterpret_cast<struct s24*>((rax302 << 4) + reinterpret_cast<unsigned char>(0xd640));
            r10_250 = rax303->f0;
            r11_249 = rax303->f8;
            continue;
            addr_5198_160:
            rbp244 = rdx277;
            ecx304 = static_cast<uint32_t>(r13_248 + 4);
            while (1) {
                edx305 = *reinterpret_cast<uint32_t*>(&r13_248);
                rsi306 = r12_247;
                do {
                    *reinterpret_cast<uint32_t*>(&rdi307) = edx305;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi307) + 4) = 0;
                    ++edx305;
                    *reinterpret_cast<uint32_t*>(&rdi308) = rdi307->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi308) + 4) = 0;
                    rsi306 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi306) + reinterpret_cast<uint64_t>(rdi308));
                } while (ecx304 != edx305);
                factor_insert_multiplicity(r9_242, rsi306, 1);
                rsp246 = rsp246 - 8 + 8;
                r9_242 = r9_242;
                r8_241 = r8_241;
                ecx304 = ecx304;
                rdx309 = reinterpret_cast<void**>(rbx252->f30 * reinterpret_cast<unsigned char>(rbp244));
                if (reinterpret_cast<unsigned char>(rdx309) > reinterpret_cast<unsigned char>(rbx252->f38)) 
                    goto addr_4f5f_161;
                rbp244 = rdx309;
            }
            addr_50b0_154:
            *reinterpret_cast<int32_t*>(&rcx310) = static_cast<int32_t>(r13_248 + 1);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx310) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rsi311) = *reinterpret_cast<uint32_t*>(&r13_248);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi311) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx312) = rcx310->f10340;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx312) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rsi313) = rsi311->f10340;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi313) + 4) = 0;
            rcx314 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx312) + reinterpret_cast<int64_t>(rsi313) + reinterpret_cast<unsigned char>(r12_247));
            do {
                rbp244 = rdx268;
                factor_insert_multiplicity(r9_242, rcx314, 1);
                rsp246 = rsp246 - 8 + 8;
                r9_242 = r9_242;
                rcx314 = rcx314;
                r8_241 = r8_241;
                rdx268 = reinterpret_cast<void**>(rbx252->f10 * reinterpret_cast<unsigned char>(rbp244));
            } while (reinterpret_cast<unsigned char>(rdx268) <= reinterpret_cast<unsigned char>(rbx252->f18));
            rdx269 = reinterpret_cast<void**>(rbx252->f20 * reinterpret_cast<unsigned char>(rbp244));
            if (reinterpret_cast<unsigned char>(rdx269) > reinterpret_cast<unsigned char>(rbx252->f28)) 
                goto addr_4f4d_159;
            goto addr_5120_158;
        }
        addr_5200_141:
        if (!r8_241) 
            goto addr_5209_193;
        rdi315 = r8_241;
        v316 = r9_242;
        v317 = r8_241;
        al318 = prime2_p(rdi315, rbp244);
        rsp246 = rsp246 - 8 + 8;
        if (al318) 
            goto addr_53a9_195;
        rsi125 = rbp244;
        rcx123 = v316;
        *reinterpret_cast<int32_t*>(&rdx124) = 1;
        *reinterpret_cast<int32_t*>(&rdx124 + 4) = 0;
        rbx102 = v245;
        rdi122 = v317;
        r13_112 = v243;
        rsp128 = reinterpret_cast<void*>(rsp246 + 56 + 8 + 8 + 8 + 8 + 8 + 8);
        continue;
        addr_4edd_142:
        *reinterpret_cast<uint32_t*>(&rax319) = *reinterpret_cast<uint32_t*>(&r13_248);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax319) + 4) = 0;
        rax320 = reinterpret_cast<struct s28*>((rax319 << 4) + 0xd640);
        r10_250 = rax320->f0;
        r11_249 = rax320->f8;
        goto addr_4ef9_146;
        addr_4c8b_115:
        if (*reinterpret_cast<void***>(v133 + 8)) 
            goto addr_4cfe_125; else 
            goto addr_4c97_197;
    }
    addr_4cba_81:
    fun_2810("b0 & 1", "src/factor.c", 0x1e0, "gcd2_odd", r8_175, r9_158);
    do {
        fun_2740();
        addr_4ce0_119:
        factor_using_pollard_rho2(rbx102, r13_112, v132 + 1, v133);
        addr_4ad5_73:
        rax321 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(g28));
    } while (rax321);
    goto v322;
    addr_4aa0_113:
    if (reinterpret_cast<unsigned char>(rbp130) <= reinterpret_cast<unsigned char>(1)) {
        addr_4c74_200:
        factor_using_pollard_rho(rbp130, v132, v133, rcx209);
        goto addr_4ad5_73;
    } else {
        addr_4aaa_201:
        if (reinterpret_cast<unsigned char>(rbp130) <= reinterpret_cast<unsigned char>(0x17ded78) || (al323 = prime_p_part_0(rbp130, rsi227, rdx226, rcx209), !!al323)) {
            factor_insert_multiplicity(v133, rbp130, 1, v133, rbp130, 1);
            goto addr_4ad5_73;
        }
    }
    addr_4c4f_120:
    rcx209 = v133;
    rsi227 = v154;
    rdx226 = v132 + 1;
    factor_using_pollard_rho2(v183, rsi227, rdx226, rcx209);
    if (reinterpret_cast<unsigned char>(rbp130) > reinterpret_cast<unsigned char>(1)) 
        goto addr_4aaa_201; else 
        goto addr_4c74_200;
    addr_4ca3_122:
    *reinterpret_cast<int32_t*>(&rdx226) = 1;
    *reinterpret_cast<int32_t*>(&rdx226 + 4) = 0;
    rsi227 = v154;
    factor_insert_multiplicity(v133, rsi227, 1);
    goto addr_4aa0_113;
    addr_4a94_124:
    *reinterpret_cast<void***>(v133) = v154;
    *reinterpret_cast<void***>(v133 + 8) = v183;
    goto addr_4aa0_113;
    addr_5209_193:
    if (reinterpret_cast<unsigned char>(rbp244) <= reinterpret_cast<unsigned char>(1)) {
        addr_4d41_129:
        goto v154;
    } else {
        al324 = prime2_p(0, rbp244);
        r9_325 = r9_242;
        if (al324) {
            rsi326 = rbp244;
            rdi327 = r9_325;
        } else {
            rdi328 = rbp244;
            rdx329 = r9_325;
            *reinterpret_cast<int32_t*>(&rsi330) = 1;
            *reinterpret_cast<int32_t*>(&rsi330 + 4) = 0;
            goto addr_5740_206;
        }
    }
    addr_2f20_207:
    *reinterpret_cast<uint32_t*>(&rax331) = *reinterpret_cast<unsigned char*>(rdi327 + 0xfa);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax331) + 4) = 0;
    r8_332 = rsi326;
    r11_333 = reinterpret_cast<void***>(rdi327 + 16);
    r10_334 = reinterpret_cast<signed char*>(rdi327 + 0xe0);
    r9d335 = *reinterpret_cast<uint32_t*>(&rax331);
    if (!*reinterpret_cast<uint32_t*>(&rax331)) 
        goto addr_2fa1_208;
    ebx336 = static_cast<int32_t>(rax331 - 1);
    rcx337 = reinterpret_cast<void*>(static_cast<int64_t>(ebx336));
    rax338 = rcx337;
    do {
        *reinterpret_cast<int32_t*>(&rsi339) = *reinterpret_cast<int32_t*>(&rax338);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi339) + 4) = 0;
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi327 + reinterpret_cast<uint64_t>(rax338) * 8) + 16)) <= reinterpret_cast<unsigned char>(r8_332)) 
            break;
        rax338 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax338) - 1);
        *reinterpret_cast<int32_t*>(&rsi339) = *reinterpret_cast<int32_t*>(&rax338);
    } while (*reinterpret_cast<int32_t*>(&rax338) != -1);
    goto addr_2f80_212;
    if (*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi327 + reinterpret_cast<uint64_t>(rax338) * 8) + 16) == r8_332) {
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_334) + reinterpret_cast<uint64_t>(rax338)) = reinterpret_cast<signed char>(*reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_334) + reinterpret_cast<uint64_t>(rax338)) + 1);
        goto v154;
    }
    rax340 = reinterpret_cast<void*>(static_cast<int64_t>(static_cast<int32_t>(rsi339 + 1)));
    r11_333 = r11_333 + reinterpret_cast<uint64_t>(rax340) * 8;
    r10_334 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_334) + reinterpret_cast<uint64_t>(rax340));
    if (*reinterpret_cast<int32_t*>(&rsi339) >= ebx336) {
        addr_2fa1_208:
        r9d341 = r9d335 + 1;
        *r11_333 = r8_332;
        *r10_334 = 1;
        *reinterpret_cast<unsigned char*>(rdi327 + 0xfa) = *reinterpret_cast<unsigned char*>(&r9d341);
        goto v154;
    }
    do {
        addr_2f80_212:
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi327 + reinterpret_cast<uint64_t>(rcx337) * 8) + 24) = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi327 + reinterpret_cast<uint64_t>(rcx337) * 8) + 16);
        eax342 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi327) + reinterpret_cast<uint64_t>(rcx337) + 0xe0);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi327) + reinterpret_cast<uint64_t>(rcx337) + 0xe1) = *reinterpret_cast<signed char*>(&eax342);
        rcx337 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx337) - 1);
    } while (*reinterpret_cast<int32_t*>(&rsi339) < *reinterpret_cast<int32_t*>(&rcx337));
    goto addr_2fa1_208;
    addr_5740_206:
    v316 = rsi330;
    v278 = rdx329;
    if (reinterpret_cast<unsigned char>(rdi328) <= reinterpret_cast<unsigned char>(1)) {
        addr_5b07_217:
        fun_2810("(1) < (n)", "src/factor.c", 0x5be, "factor_using_pollard_rho", r8_241, r9_325);
    } else {
        r12_343 = rdi328;
        do {
            rcx344 = r12_343;
            esi345 = 64;
            *reinterpret_cast<int32_t*>(&rax346) = 1;
            *reinterpret_cast<int32_t*>(&rax346 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx347) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx347) + 4) = 0;
            rdi348 = reinterpret_cast<void*>(0);
            do {
                r8_241 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx344) << 63);
                rcx344 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx344) >> 1);
                rdx347 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx347) >> 1 | reinterpret_cast<unsigned char>(r8_241));
                if (reinterpret_cast<unsigned char>(rcx344) < reinterpret_cast<unsigned char>(rax346) || rcx344 == rax346 && reinterpret_cast<uint64_t>(rdx347) <= reinterpret_cast<uint64_t>(rdi348)) {
                    cf349 = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi348) < reinterpret_cast<uint64_t>(rdx347));
                    rdi348 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi348) - reinterpret_cast<uint64_t>(rdx347));
                    rax346 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax346) - (reinterpret_cast<unsigned char>(rcx344) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax346) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx344) + static_cast<uint64_t>(cf349))))));
                }
                --esi345;
            } while (esi345);
            *reinterpret_cast<int32_t*>(&rbp350) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp350) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r15_351) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_351) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r14_352) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_352) + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rbp350) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_343) - reinterpret_cast<uint64_t>(rdi348) > reinterpret_cast<uint64_t>(rdi348));
            rbp353 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbp350) & reinterpret_cast<unsigned char>(r12_343)) + (reinterpret_cast<uint64_t>(rdi348) + reinterpret_cast<uint64_t>(rdi348) - reinterpret_cast<unsigned char>(r12_343)));
            rcx354 = rbp353;
            while (reinterpret_cast<unsigned char>(v316) < reinterpret_cast<unsigned char>(r12_343)) {
                r11_355 = rcx354;
                rax356 = reinterpret_cast<unsigned char>(r12_343) >> 1;
                *reinterpret_cast<uint32_t*>(&rax357) = *reinterpret_cast<uint32_t*>(&rax356) & 0x7f;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax357) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax358) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax357);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax358) + 4) = 0;
                rdx359 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax358) + reinterpret_cast<int64_t>(rax358) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax358) * reinterpret_cast<int64_t>(rax358) * reinterpret_cast<unsigned char>(r12_343)));
                rax360 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx359) + reinterpret_cast<uint64_t>(rdx359) - reinterpret_cast<uint64_t>(rdx359) * reinterpret_cast<uint64_t>(rdx359) * reinterpret_cast<unsigned char>(r12_343));
                r8_361 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax360) + reinterpret_cast<uint64_t>(rax360) - reinterpret_cast<uint64_t>(rax360) * reinterpret_cast<uint64_t>(rax360) * reinterpret_cast<unsigned char>(r12_343));
                r10_362 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_343) - reinterpret_cast<unsigned char>(v316));
                r9_363 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v316) - reinterpret_cast<unsigned char>(r12_343));
                while (1) {
                    rax364 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rax364 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax364) + reinterpret_cast<unsigned char>(r12_343));
                    }
                    rbp353 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rbp353) - (reinterpret_cast<unsigned char>(rbp353) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp353) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp353) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax364) < reinterpret_cast<uint64_t>(r10_362))))))) & reinterpret_cast<unsigned char>(r12_343)) + (reinterpret_cast<uint64_t>(rax364) + reinterpret_cast<uint64_t>(r9_363)));
                    r13_365 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        r13_365 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_365) + reinterpret_cast<unsigned char>(r12_343));
                    }
                    rax366 = r14_352;
                    *reinterpret_cast<uint32_t*>(&rax367) = *reinterpret_cast<uint32_t*>(&rax366) & 31;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax367) + 4) = 0;
                    if (rax367 == 1) {
                        rdi368 = r13_365;
                        rax369 = gcd_odd(rdi368, r12_343);
                        if (!reinterpret_cast<int1_t>(rax369 == 1)) 
                            break;
                    }
                    --r14_352;
                    if (r14_352) 
                        continue;
                    rcx370 = r15_351 + r15_351;
                    if (!r15_351) {
                        r15_351 = rcx370;
                        r11_355 = rbp353;
                    } else {
                        do {
                            rdi371 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                            rax372 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi371) + reinterpret_cast<unsigned char>(r12_343));
                            if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                                rdi371 = rax372;
                            }
                            ++r14_352;
                        } while (r15_351 != r14_352);
                        r11_355 = rbp353;
                        r15_351 = rcx370;
                        rbp353 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax372) - (reinterpret_cast<uint64_t>(rax372) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax372) < reinterpret_cast<uint64_t>(rax372) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi371) < reinterpret_cast<uint64_t>(r10_362)))) & reinterpret_cast<unsigned char>(r12_343)) + (reinterpret_cast<uint64_t>(rdi371) + reinterpret_cast<uint64_t>(r9_363)));
                    }
                }
                r9_325 = r8_361;
                r8_241 = r11_355;
                r11_373 = r9_363;
                do {
                    rsi374 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    rax375 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi374) + reinterpret_cast<unsigned char>(r12_343));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rsi374 = rax375;
                    }
                    rbx376 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax375) - (reinterpret_cast<uint64_t>(rax375) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax375) < reinterpret_cast<uint64_t>(rax375) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rsi374) < reinterpret_cast<uint64_t>(r10_362)))) & reinterpret_cast<unsigned char>(r12_343)) + (reinterpret_cast<uint64_t>(rsi374) + reinterpret_cast<uint64_t>(r11_373)));
                    rsi377 = r12_343;
                    rdi368 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rdi368) - (reinterpret_cast<unsigned char>(rdi368) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi368) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rdi368) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_241) < reinterpret_cast<unsigned char>(rbx376))))))) & reinterpret_cast<unsigned char>(r12_343)) + (reinterpret_cast<unsigned char>(r8_241) - reinterpret_cast<unsigned char>(rbx376)));
                    rax378 = gcd_odd(rdi368, rsi377);
                } while (rax378 == 1);
                rcx379 = r8_241;
                r11_380 = rax378;
                if (rax378 == r12_343) 
                    goto addr_5af7_246;
                rdx381 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_343) % reinterpret_cast<unsigned char>(r11_380));
                rax382 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_343) / reinterpret_cast<unsigned char>(r11_380));
                r8_383 = rax382;
                r12_343 = rax382;
                if (reinterpret_cast<unsigned char>(r11_380) > reinterpret_cast<unsigned char>(1) && (reinterpret_cast<unsigned char>(r11_380) <= reinterpret_cast<unsigned char>(0x17ded78) || (al384 = prime_p_part_0(r11_380, rsi377, rdx381, rcx379), r11_380 = r11_380, rcx379 = rcx379, r8_383 = rax382, !!al384))) {
                    *reinterpret_cast<int32_t*>(&rdx385) = 1;
                    *reinterpret_cast<int32_t*>(&rdx385 + 4) = 0;
                    rsi386 = r11_380;
                    factor_insert_multiplicity(v278, rsi386, 1);
                    r8_241 = r8_383;
                    rcx387 = rcx379;
                    zf388 = reinterpret_cast<int1_t>(r8_241 == 1);
                    if (reinterpret_cast<unsigned char>(r8_241) <= reinterpret_cast<unsigned char>(1)) 
                        goto addr_5aca_249; else 
                        goto addr_5a2f_250;
                }
                rdx385 = v278;
                rsi386 = v316 + 1;
                factor_using_pollard_rho(r11_380, rsi386, rdx385, rcx379);
                r8_241 = r8_383;
                rcx387 = rcx379;
                zf388 = reinterpret_cast<int1_t>(r8_241 == 1);
                if (reinterpret_cast<unsigned char>(r8_241) > reinterpret_cast<unsigned char>(1)) {
                    addr_5a2f_250:
                    if (reinterpret_cast<unsigned char>(r8_241) <= reinterpret_cast<unsigned char>(0x17ded78)) 
                        goto addr_5ad7_252;
                    al389 = prime_p_part_0(r8_241, rsi386, rdx385, rcx387);
                    r8_241 = r8_241;
                    if (al389) 
                        goto addr_5ad7_252;
                } else {
                    addr_5aca_249:
                    if (zf388) 
                        goto addr_5b45_254; else 
                        goto addr_5acc_255;
                }
                rbp353 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp353) % reinterpret_cast<unsigned char>(r8_241));
                rcx354 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx387) % reinterpret_cast<unsigned char>(r8_241));
                continue;
                addr_5acc_255:
                *reinterpret_cast<int32_t*>(&rcx354) = 0;
                *reinterpret_cast<int32_t*>(&rcx354 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rbp353) = 0;
                *reinterpret_cast<int32_t*>(&rbp353 + 4) = 0;
            }
            break;
            addr_5af7_246:
            ++v316;
        } while (reinterpret_cast<unsigned char>(r12_343) > reinterpret_cast<unsigned char>(1));
        goto addr_5b07_217;
    }
    fun_2810("a < n", "src/factor.c", 0x5c4, "factor_using_pollard_rho", r8_241, r9_325);
    addr_5b45_254:
    goto v154;
    addr_5ad7_252:
    rdi327 = v278;
    rsi326 = r8_241;
    goto addr_2f20_207;
    addr_53a9_195:
    if (!*reinterpret_cast<void***>(v316 + 8)) {
        *reinterpret_cast<void***>(v316) = rbp244;
        *reinterpret_cast<void***>(v316 + 8) = v317;
        goto addr_4d41_129;
    }
    factor_insert_large_part_0();
    r14_390 = reinterpret_cast<struct s0*>(rdi315 + 0xffffffffffffffff);
    rbp391 = rdi315;
    rsp392 = reinterpret_cast<struct s0**>(rsp246 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x148);
    rax393 = g28;
    v394 = rax393;
    eax395 = 0;
    v396 = r14_390;
    if (*reinterpret_cast<uint32_t*>(&rdi315) & 1) 
        goto addr_5478_261;
    v397 = 0;
    r14_390 = v396;
    addr_5490_263:
    rcx398 = rbp391;
    *reinterpret_cast<int32_t*>(&r15_399) = 0;
    *reinterpret_cast<int32_t*>(&r15_399 + 4) = 0;
    rax400 = reinterpret_cast<unsigned char>(rbp391) >> 1;
    esi401 = 64;
    *reinterpret_cast<uint32_t*>(&rax402) = *reinterpret_cast<uint32_t*>(&rax400) & 0x7f;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax402) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax403) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax402);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax403) + 4) = 0;
    rdx404 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax403) + reinterpret_cast<int64_t>(rax403) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax403) * reinterpret_cast<int64_t>(rax403) * reinterpret_cast<unsigned char>(rbp391)));
    rax405 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx404) + reinterpret_cast<uint64_t>(rdx404) - reinterpret_cast<uint64_t>(rdx404) * reinterpret_cast<uint64_t>(rdx404) * reinterpret_cast<unsigned char>(rbp391));
    *reinterpret_cast<int32_t*>(&rdx406) = 0;
    *reinterpret_cast<int32_t*>(&rdx406 + 4) = 0;
    r13_407 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax405) + reinterpret_cast<uint64_t>(rax405) - reinterpret_cast<uint64_t>(rax405) * reinterpret_cast<uint64_t>(rax405) * reinterpret_cast<unsigned char>(rbp391));
    *reinterpret_cast<int32_t*>(&rax408) = 1;
    *reinterpret_cast<int32_t*>(&rax408 + 4) = 0;
    do {
        rdi409 = reinterpret_cast<unsigned char>(rcx398) << 63;
        rcx398 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx398) >> 1);
        rdx406 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx406) >> 1) | rdi409);
        if (reinterpret_cast<unsigned char>(rcx398) < reinterpret_cast<unsigned char>(rax408) || rcx398 == rax408 && reinterpret_cast<unsigned char>(rdx406) <= reinterpret_cast<unsigned char>(r15_399)) {
            cf410 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_399) < reinterpret_cast<unsigned char>(rdx406));
            r15_399 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_399) - reinterpret_cast<unsigned char>(rdx406));
            rax408 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax408) - (reinterpret_cast<unsigned char>(rcx398) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax408) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx398) + static_cast<uint64_t>(cf410))))));
        }
        --esi401;
    } while (esi401);
    *reinterpret_cast<int32_t*>(&rbx411) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx411) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&r8_241) = v397;
    *reinterpret_cast<int32_t*>(&r8_241 + 4) = 0;
    r9_325 = r15_399;
    *reinterpret_cast<unsigned char*>(&rbx411) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp391) - reinterpret_cast<unsigned char>(r15_399)) > reinterpret_cast<unsigned char>(r15_399));
    rbx412 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbx411) & reinterpret_cast<unsigned char>(rbp391)) + (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r15_399) + reinterpret_cast<unsigned char>(r15_399)) - reinterpret_cast<unsigned char>(rbp391)));
    al413 = millerrabin(rbp391, r13_407, rbx412, r14_390, *reinterpret_cast<uint32_t*>(&r8_241), r9_325);
    if (al413) {
        v414 = reinterpret_cast<void*>(rsp392 - 1 + 1 + 6);
        factor();
        v415 = r14_390;
        r14_390 = r13_407;
        eax416 = v417;
        r13_407 = reinterpret_cast<struct s0*>(0x10340);
        v418 = eax416;
        *reinterpret_cast<uint32_t*>(&rax419) = eax416 - 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax419) + 4) = 0;
        v420 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v414) + rax419 * 8);
        r12_247 = reinterpret_cast<void**>(2);
        rax421 = rbx412;
        rbx412 = r15_399;
        r15_399 = rax421;
        goto addr_55b0_269;
    }
    addr_56fc_270:
    addr_5690_271:
    rax422 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v394) - reinterpret_cast<unsigned char>(g28));
    if (rax422) {
        fun_2740();
    } else {
        goto v317;
    }
    addr_5719_274:
    *reinterpret_cast<int32_t*>(&rdx329) = 0x4f2;
    *reinterpret_cast<int32_t*>(&rdx329 + 4) = 0;
    rsi330 = reinterpret_cast<void**>("src/factor.c");
    rdi328 = reinterpret_cast<void**>("(s1) < (n)");
    fun_2810("(s1) < (n)", "src/factor.c", 0x4f2, "prime_p", r8_241, r9_325);
    goto addr_5740_206;
    addr_5478_261:
    do {
        r14_390 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(r14_390) >> 1);
        ++eax395;
    } while (!(*reinterpret_cast<unsigned char*>(&r14_390) & 1));
    v397 = eax395;
    goto addr_5490_263;
    addr_4c97_197:
    *reinterpret_cast<void***>(v133) = rbp130;
    *reinterpret_cast<void***>(v133 + 8) = r13_112;
    goto addr_4ad5_73;
    addr_37ca_56:
    rax423 = xrealloc(0, 16, 0, 16);
    rax424 = xrealloc(0, 8, 0, 8);
    rdi425 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax423 + 16) + 0xfffffffffffffff0);
    r15_426 = rax424;
    fun_2ab0(rdi425, 8, 0, 0, r8_66, rdi425, 8, 0, 0, r8_66);
    rdi427 = rdi425;
    addr_370b_277:
    fun_27a0(rdi427, r12_99, rdx100, 0, r8_66, rdi427, r12_99, rdx100, 0, r8_66);
    *reinterpret_cast<void***>(r15_426 + v101 * 8) = reinterpret_cast<void**>(1);
    goto v15;
    addr_361b_57:
    rbp428 = -1;
    rbx429 = -1;
    rdx100 = reinterpret_cast<void**>(0xfffffffffffffff0);
    r15_430 = reinterpret_cast<void**>(0xfffffffffffffff0);
    do {
        eax431 = fun_2a00(r15_430, r12_99, 0xfffffffffffffff0, 0, r8_66, r15_430, r12_99, 0xfffffffffffffff0, 0, r8_66);
        zf432 = reinterpret_cast<uint1_t>(eax431 == 0);
        if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(eax431 < 0) | zf432)) 
            break;
        --rbx429;
        r15_430 = r15_430 - 16;
    } while (rbx429 != -1);
    goto addr_3670_280;
    if (zf432) {
        *reinterpret_cast<int64_t*>(rbx429 * 8) = *reinterpret_cast<int64_t*>(rbx429 * 8) + 1;
        goto v15;
    }
    rax433 = xrealloc(0, 16, 0, 16);
    v434 = rax433;
    rax435 = xrealloc(0, 8, 0, 8);
    rdx100 = reinterpret_cast<void**>(16);
    r15_426 = rax435;
    rax436 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v434) + reinterpret_cast<unsigned char>(16) + 0xfffffffffffffff0);
    fun_2ab0(rax436, 8, 16, 0, r8_66, rax436, 8, 16, 0, r8_66);
    rax437 = rbx429 + 1;
    v101 = rax437;
    v438 = reinterpret_cast<void*>(rax437 << 4);
    rax439 = rax436;
    if (rbx429 < -1) {
        addr_36da_284:
        r14_440 = rax439;
    } else {
        goto addr_3701_286;
    }
    do {
        rdi441 = r14_440;
        r14_440 = r14_440 - 16;
        fun_27a0(rdi441, r14_440, rdx100, 0, r8_66, rdi441, r14_440, rdx100, 0, r8_66);
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r15_426 + rbp428 * 8) + 8) = *reinterpret_cast<void***>(r15_426 + rbp428 * 8);
        --rbp428;
    } while (rbx429 < rbp428);
    addr_3701_286:
    rdi427 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v438) + reinterpret_cast<unsigned char>(v434));
    goto addr_370b_277;
    addr_3670_280:
    rax442 = xrealloc(0, 16, 0, 16);
    v434 = rax442;
    rax443 = xrealloc(0, 8, 0, 8);
    r15_426 = rax443;
    rax444 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(v434 + 16) + 0xfffffffffffffff0);
    fun_2ab0(rax444, 8, 0xfffffffffffffff0, 0, r8_66, rax444, 8, 0xfffffffffffffff0, 0, r8_66);
    rax439 = rax444;
    v438 = reinterpret_cast<void*>(0);
    v101 = 0;
    goto addr_36da_284;
    addr_5689_288:
    goto addr_5690_271;
    addr_4548_289:
    fun_25e0(v445, rsi120, v445, rsi120);
    fun_25e0(v446, rsi120, v446, rsi120);
    rsp118 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp121) - 8 + 8 - 8 + 8);
    goto addr_455c_61;
    addr_45a0_290:
    rdx447 = v448;
    addr_4524_291:
    if (rdx447) {
        *reinterpret_cast<int32_t*>(&rbx102) = 0;
        *reinterpret_cast<int32_t*>(&rbx102 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdi449) = 0;
        *reinterpret_cast<int32_t*>(&rdi449 + 4) = 0;
        do {
            fun_2940((reinterpret_cast<unsigned char>(rdi449) << 4) + reinterpret_cast<uint64_t>(v450), rsi120, rdx447, rcx116, r8_115, r9_114);
            rsp121 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp121) - 8 + 8);
            *reinterpret_cast<int32_t*>(&rdi449) = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbx102 + 1));
            *reinterpret_cast<int32_t*>(&rdi449 + 4) = 0;
            rbx102 = rdi449;
        } while (reinterpret_cast<unsigned char>(rdi449) < reinterpret_cast<unsigned char>(v451));
        goto addr_4548_289;
    }
    addr_5680_294:
    while (rax452 == rbx412) {
        while (1) {
            *reinterpret_cast<uint32_t*>(&rax453) = r13_407->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax453) + 4) = 0;
            r12_247 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_247) + reinterpret_cast<uint64_t>(rax453));
            rax454 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx412) * reinterpret_cast<unsigned char>(r12_247));
            rdx455 = __intrinsic();
            r15_399 = rax454;
            if (rdx455) {
                if (reinterpret_cast<unsigned char>(rbp391) <= reinterpret_cast<unsigned char>(rdx455)) 
                    goto addr_5719_274;
                rcx456 = rbp391;
                esi457 = 64;
                *reinterpret_cast<int32_t*>(&rax458) = 0;
                *reinterpret_cast<int32_t*>(&rax458 + 4) = 0;
                do {
                    rdi459 = reinterpret_cast<unsigned char>(rcx456) << 63;
                    rcx456 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx456) >> 1);
                    rax458 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax458) >> 1) | rdi459);
                    if (reinterpret_cast<unsigned char>(rcx456) < reinterpret_cast<unsigned char>(rdx455) || rcx456 == rdx455 && reinterpret_cast<unsigned char>(rax458) <= reinterpret_cast<unsigned char>(r15_399)) {
                        cf460 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_399) < reinterpret_cast<unsigned char>(rax458));
                        r15_399 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_399) - reinterpret_cast<unsigned char>(rax458));
                        rdx455 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx455) - (reinterpret_cast<unsigned char>(rcx456) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx455) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx456) + static_cast<uint64_t>(cf460))))));
                    }
                    --esi457;
                } while (esi457);
            } else {
                r15_399 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax454) % reinterpret_cast<unsigned char>(rbp391));
            }
            *reinterpret_cast<uint32_t*>(&r8_241) = v397;
            *reinterpret_cast<int32_t*>(&r8_241 + 4) = 0;
            r9_325 = rbx412;
            al461 = millerrabin(rbp391, r14_390, r15_399, v415, *reinterpret_cast<uint32_t*>(&r8_241), r9_325);
            if (!al461) 
                goto addr_56fc_270;
            r13_407 = reinterpret_cast<struct s0*>(&r13_407->f1);
            if (reinterpret_cast<int1_t>(0x105dc == r13_407)) 
                break;
            addr_55b0_269:
            if (!v418) 
                goto addr_5690_271;
            r11_462 = v414;
            do {
                r8_241 = rbx412;
                rax452 = powm(r15_399, reinterpret_cast<uint64_t>(v396) / v463, rbp391, r14_390, r8_241, r9_325);
                if (v420 == r11_462) 
                    goto addr_5680_294;
                r11_462 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r11_462) + 8);
            } while (rax452 != rbx412);
        }
        fun_2700();
        fun_2a20();
        rax452 = fun_25f0();
    }
    goto addr_5689_288;
    addr_4520_310:
    while (!*reinterpret_cast<int32_t*>(&rax464)) {
        while (1) {
            *reinterpret_cast<uint32_t*>(&rdx465) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_112));
            *reinterpret_cast<int32_t*>(&rdx465 + 4) = 0;
            fun_28c0(rbp108, rbp108, rdx465, rcx116, r8_115, r9_114);
            r9_114 = v113;
            rcx116 = r14_109;
            r8_115 = v110;
            rsi120 = r12_106;
            al466 = mp_millerrabin(rbx102, rsi120, rbp108, rcx116, r8_115, r9_114);
            rsp121 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp121) - 8 + 8 - 8 + 8);
            if (!al466) 
                goto addr_45a0_290;
            ++r13_112;
            if (reinterpret_cast<int1_t>(r13_112 == 0x105dc)) 
                break;
            addr_4458_60:
            if (!v467) 
                goto addr_4548_289;
            *reinterpret_cast<int32_t*>(&r15_107) = 0;
            *reinterpret_cast<int32_t*>(&r15_107 + 4) = 0;
            do {
                rdx468 = r15_107;
                ++r15_107;
                fun_2760(r14_109, r12_106, (reinterpret_cast<unsigned char>(rdx468) << 4) + reinterpret_cast<uint64_t>(v469), rcx116, r8_115, r9_114);
                rcx116 = rbx102;
                fun_2730(r14_109, rbp108, r14_109, rcx116, r8_115, r9_114);
                *reinterpret_cast<int32_t*>(&rsi120) = 1;
                *reinterpret_cast<int32_t*>(&rsi120 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rax464) = fun_2aa0(r14_109, 1, r14_109, rcx116, r8_115, r9_114);
                rsp121 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp121) - 8 + 8 - 8 + 8 - 8 + 8);
                rdx447 = v470;
                if (reinterpret_cast<unsigned char>(r15_107) >= reinterpret_cast<unsigned char>(rdx447)) 
                    goto addr_4520_310;
            } while (*reinterpret_cast<int32_t*>(&rax464));
        }
        rax471 = fun_2700();
        *reinterpret_cast<int32_t*>(&rsi120) = 0;
        *reinterpret_cast<int32_t*>(&rsi120 + 4) = 0;
        rdx447 = rax471;
        fun_2a20();
        rax464 = fun_25f0();
        rsp121 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp121) - 8 + 8 - 8 + 8 - 8 + 8);
    }
    goto addr_4524_291;
}

void mp_factor_insert(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r13_7;
    void** r12_8;
    void** rcx9;
    void** rdx10;
    struct s0* r14_11;
    void** v12;
    void** v13;
    void** v14;
    void** r14_15;
    void** v16;
    void** rbx17;
    void** rax18;
    void** rsi19;
    void** v20;
    void** rax21;
    void** rdi22;
    void** r15_23;
    void** rdi24;
    struct s0* rbp25;
    struct s0* rbx26;
    void** r15_27;
    int32_t eax28;
    uint1_t zf29;
    void** r15_30;
    void** rdx31;
    void** rax32;
    void** rsi33;
    void** rax34;
    void** rax35;
    void** rax36;
    void* v37;
    void** rax38;
    void** r14_39;
    void** rdi40;
    void** r15_41;
    void** r14_42;
    void** rax43;
    void** rsi44;
    void** rax45;
    void** rax46;

    r13_7 = rdi;
    r12_8 = rsi;
    rcx9 = *reinterpret_cast<void***>(rdi);
    rdx10 = *reinterpret_cast<void***>(rdi + 8);
    r14_11 = reinterpret_cast<struct s0*>(*reinterpret_cast<void***>(rdi + 16) + 0xffffffffffffffff);
    v12 = *reinterpret_cast<void***>(rdi + 16);
    v13 = rcx9;
    v14 = rdx10;
    if (reinterpret_cast<int64_t>(r14_11) < reinterpret_cast<int64_t>(0)) {
        r14_15 = v12 + 1;
        v16 = r14_15;
        rbx17 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_15) << 4);
        rax18 = xrealloc(v13, rbx17);
        rsi19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_15) * 8);
        v20 = rax18;
        rax21 = xrealloc(v14, rsi19);
        rdi22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax18) + reinterpret_cast<unsigned char>(rbx17) + 0xfffffffffffffff0);
        r15_23 = rax21;
        fun_2ab0(rdi22, rsi19, rdx10, rcx9, r8);
        rdi24 = rdi22;
    } else {
        rbp25 = r14_11;
        rbx26 = r14_11;
        rdx10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r14_11) << 4);
        r15_27 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx9) + reinterpret_cast<unsigned char>(rdx10));
        do {
            eax28 = fun_2a00(r15_27, r12_8, rdx10, rcx9, r8);
            zf29 = reinterpret_cast<uint1_t>(eax28 == 0);
            if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(eax28 < 0) | zf29)) 
                goto addr_364d_5;
            rbx26 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbx26) - 1);
            r15_27 = r15_27 - 16;
        } while (rbx26 != 0xffffffffffffffff);
        goto addr_3670_7;
    }
    addr_370b_8:
    fun_27a0(rdi24, r12_8, rdx10, rcx9, r8);
    *reinterpret_cast<void***>(r15_23 + reinterpret_cast<unsigned char>(v12) * 8) = reinterpret_cast<void**>(1);
    *reinterpret_cast<void***>(r13_7 + 8) = r15_23;
    *reinterpret_cast<void***>(r13_7) = v20;
    *reinterpret_cast<void***>(r13_7 + 16) = v16;
    return;
    addr_364d_5:
    if (zf29) {
        *reinterpret_cast<void***>(v14 + reinterpret_cast<uint64_t>(rbx26) * 8) = *reinterpret_cast<void***>(v14 + reinterpret_cast<uint64_t>(rbx26) * 8) + 1;
        return;
    }
    r15_30 = v12 + 1;
    v16 = r15_30;
    rdx31 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_30) << 4);
    rax32 = xrealloc(v13, rdx31);
    rsi33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_30) * 8);
    v20 = rax32;
    rax34 = xrealloc(v14, rsi33);
    rdx10 = rdx31;
    r15_23 = rax34;
    rax35 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v20) + reinterpret_cast<unsigned char>(rdx10) + 0xfffffffffffffff0);
    fun_2ab0(rax35, rsi33, rdx10, rcx9, r8);
    rax36 = reinterpret_cast<void**>(&rbx26->f1);
    v12 = rax36;
    v37 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax36) << 4);
    rax38 = rax35;
    if (reinterpret_cast<int64_t>(rbx26) < reinterpret_cast<int64_t>(r14_11)) {
        addr_36da_11:
        r14_39 = rax38;
    } else {
        goto addr_3701_13;
    }
    do {
        rdi40 = r14_39;
        r14_39 = r14_39 - 16;
        fun_27a0(rdi40, r14_39, rdx10, rcx9, r8);
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r15_23 + reinterpret_cast<uint64_t>(rbp25) * 8) + 8) = *reinterpret_cast<void***>(r15_23 + reinterpret_cast<uint64_t>(rbp25) * 8);
        rbp25 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbp25) - 1);
    } while (reinterpret_cast<int64_t>(rbx26) < reinterpret_cast<int64_t>(rbp25));
    addr_3701_13:
    rdi24 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(v37) + reinterpret_cast<unsigned char>(v20));
    goto addr_370b_8;
    addr_3670_7:
    r15_41 = v12 + 1;
    v16 = r15_41;
    r14_42 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_41) << 4);
    rax43 = xrealloc(v13, r14_42);
    rsi44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_41) * 8);
    v20 = rax43;
    rax45 = xrealloc(v14, rsi44);
    r15_23 = rax45;
    rax46 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v20) + reinterpret_cast<unsigned char>(r14_42) + 0xfffffffffffffff0);
    fun_2ab0(rax46, rsi44, rdx10, rcx9, r8);
    rax38 = rax46;
    v37 = reinterpret_cast<void*>(0);
    v12 = reinterpret_cast<void**>(0);
    goto addr_36da_11;
}

struct s29 {
    signed char[54720] pad54720;
    unsigned char fd5c0;
};

struct s30 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
    void** f18;
    signed char[7] pad32;
    int64_t f20;
    void** f28;
    signed char[7] pad48;
    int64_t f30;
    void** f38;
    signed char[7] pad64;
    int64_t f40;
    void** f48;
    signed char[7] pad80;
    int64_t f50;
    void** f58;
    signed char[7] pad96;
    int64_t f60;
    void** f68;
    signed char[66263] pad66368;
    unsigned char f10340;
};

struct s31 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s32 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s33 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s34 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s35 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s36 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s37 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s38 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s39 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s40 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s41 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s42 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** mp_factor(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void* rsp7;
    void** rax8;
    void** v9;
    void** eax10;
    int1_t zf11;
    void** rbp12;
    void** r12_13;
    void** rdi14;
    void** r13_15;
    void** v16;
    void** rax17;
    void** rbx18;
    void** rdx19;
    void* rsp20;
    void** rbx21;
    void** r15_22;
    void** r13_23;
    int32_t eax24;
    void* rsp25;
    void** r14_26;
    void* rax27;
    void** rsi28;
    int32_t eax29;
    void* rsp30;
    int32_t eax31;
    int32_t eax32;
    int1_t zf33;
    int32_t eax34;
    signed char al35;
    void* rax36;
    void* rsp37;
    void** v38;
    void** rax39;
    void** v40;
    int1_t zf41;
    void** rdi42;
    void** v43;
    void** v44;
    void** v45;
    void** v46;
    void** v47;
    void** rdx48;
    void* rsp49;
    void** rax50;
    void** rax51;
    int32_t eax52;
    void** r15_53;
    void** rbx54;
    void** rdx55;
    void** rax56;
    int64_t rax57;
    int32_t eax58;
    void** v59;
    void** v60;
    void** r14_61;
    void** v62;
    void** r13_63;
    void** r15_64;
    void** r13_65;
    void** v66;
    void** r14_67;
    void** r12_68;
    int32_t eax69;
    void** rdx70;
    int32_t eax71;
    int32_t eax72;
    signed char al73;
    int1_t zf74;
    int32_t eax75;
    int32_t eax76;
    signed char al77;
    void** rax78;
    void** rax79;
    void* rax80;
    void** r13_81;
    void** r12_82;
    void** rcx83;
    void** rdx84;
    struct s0* r14_85;
    void** v86;
    void** v87;
    void** v88;
    void** rbx89;
    void* rsp90;
    void** rax91;
    void** v92;
    void** r12_93;
    void** r15_94;
    void** rbp95;
    void** r14_96;
    void** v97;
    void** rax98;
    void** r13_99;
    void** v100;
    void** r9_101;
    void** r8_102;
    void** rcx103;
    unsigned char al104;
    void* rsp105;
    void* rsp106;
    void** rsi107;
    void* rsp108;
    void** rdi109;
    void** rcx110;
    void** rdx111;
    void** rsi112;
    void* rax113;
    int64_t v114;
    void* rsp115;
    void** r15_116;
    void** rbp117;
    void*** rsp118;
    void** v119;
    void** v120;
    void** rax121;
    void** v122;
    uint64_t rcx123;
    void** rdx124;
    int64_t rcx125;
    uint64_t rcx126;
    uint1_t cf127;
    void** rax128;
    void** rsi129;
    uint1_t cf130;
    int1_t cf131;
    void** v132;
    void** v133;
    void** tmp64_134;
    void** rax135;
    void** r12_136;
    void** r10_137;
    void** v138;
    void** v139;
    void** v140;
    void** v141;
    void** r14_142;
    void** r15_143;
    void*** v144;
    void** r9_145;
    uint64_t rax146;
    int64_t rax147;
    void* rax148;
    void* rdx149;
    void* rax150;
    void** rax151;
    void** rbp152;
    int64_t rax153;
    void** v154;
    int64_t v155;
    void** rax156;
    void** tmp64_157;
    void** rdx158;
    void* v159;
    void** v160;
    void** rdx161;
    void** r8_162;
    void** rcx163;
    void** tmp64_164;
    uint1_t cf165;
    void** rax166;
    void** v167;
    void** rax168;
    int64_t rax169;
    void** v170;
    void** rax171;
    void** v172;
    void** v173;
    void** rdx174;
    void** r15_175;
    void** r12_176;
    void** r13_177;
    void** rsi178;
    void** rbp179;
    void** rax180;
    void** tmp64_181;
    void** rcx182;
    void* v183;
    void** v184;
    void** r11_185;
    void** r10_186;
    void** v187;
    void** r15_188;
    void** r12_189;
    void*** rbp190;
    void** r9_191;
    void** rax192;
    void** tmp64_193;
    void** rdx194;
    void* v195;
    void** rcx196;
    void** rdx197;
    void** rdx198;
    void** rsi199;
    void** tmp64_200;
    uint1_t cf201;
    void** rax202;
    void** rdi203;
    void** r8_204;
    uint64_t rax205;
    int64_t rax206;
    void** r13_207;
    void* rax208;
    void* rdx209;
    void** rdx210;
    void* rax211;
    signed char al212;
    void** rdx213;
    void** rsi214;
    void** rdi215;
    unsigned char al216;
    void** rax217;
    void* rsp218;
    void** rax219;
    void* rsp220;
    void** rax221;
    uint64_t rax222;
    struct s29* rax223;
    void* rax224;
    void* rdx225;
    void* rax226;
    unsigned char al227;
    void** r8_228;
    void** r9_229;
    void** v230;
    void** rbp231;
    void** v232;
    void*** rsp233;
    void** r12_234;
    int64_t r13_235;
    void** r11_236;
    void** r10_237;
    void*** r14_238;
    struct s30* rbx239;
    uint64_t r15_240;
    void** rcx241;
    void** rdx242;
    void** r10_243;
    void** r13_244;
    void* rax245;
    int32_t ecx246;
    int32_t ecx247;
    int32_t edx248;
    int64_t rbx249;
    void** rcx250;
    void** rdx251;
    struct s31* rcx252;
    void* rcx253;
    void** rcx254;
    void** rdx255;
    void** rdx256;
    struct s32* rcx257;
    struct s33* rsi258;
    void* rsi259;
    void* rcx260;
    struct s34* rsi261;
    void* rsi262;
    void** rcx263;
    void** rdx264;
    void** v265;
    void** rdx266;
    uint32_t ecx267;
    uint32_t edx268;
    void** rsi269;
    struct s35* rdi270;
    void* rdi271;
    void** rdx272;
    void** rdx273;
    uint32_t ecx274;
    uint32_t edx275;
    void** rsi276;
    struct s36* rdi277;
    void* rdi278;
    void** rdx279;
    void** rdx280;
    uint32_t ecx281;
    uint32_t edx282;
    void** rsi283;
    struct s37* rdi284;
    void* rdi285;
    void** rdx286;
    int64_t rax287;
    void* rax288;
    int64_t rax289;
    struct s38* rax290;
    uint32_t ecx291;
    uint32_t edx292;
    void** rsi293;
    struct s39* rdi294;
    void* rdi295;
    void** rdx296;
    struct s40* rcx297;
    struct s41* rsi298;
    void* rcx299;
    void* rsi300;
    void** rcx301;
    void** rdi302;
    void** v303;
    void** v304;
    unsigned char al305;
    int64_t rax306;
    struct s42* rax307;
    void* rax308;
    int64_t v309;
    signed char al310;
    unsigned char al311;
    void** r9_312;
    void** rsi313;
    void** rdi314;
    void** rdi315;
    void** rdx316;
    void** rsi317;
    int64_t rax318;
    void** r8_319;
    void*** r11_320;
    signed char* r10_321;
    uint32_t r9d322;
    int32_t ebx323;
    void* rcx324;
    void* rax325;
    int64_t rsi326;
    void* rax327;
    uint32_t r9d328;
    uint32_t eax329;
    void** r12_330;
    void** rcx331;
    int32_t esi332;
    void** rax333;
    void* rdx334;
    void* rdi335;
    uint1_t cf336;
    int64_t rbp337;
    int64_t r15_338;
    int64_t r14_339;
    void** rbp340;
    void** rcx341;
    void** r11_342;
    uint64_t rax343;
    int64_t rax344;
    void* rax345;
    void* rdx346;
    void* rax347;
    void** r8_348;
    void* r10_349;
    void* r9_350;
    void* rax351;
    void** r13_352;
    int64_t rax353;
    int64_t rax354;
    void** rdi355;
    void** rax356;
    int64_t rcx357;
    void* rdi358;
    void* rax359;
    void* r11_360;
    void* rsi361;
    void* rax362;
    void** rbx363;
    void** rsi364;
    void** rax365;
    void** rcx366;
    void** r11_367;
    void** rdx368;
    void** rax369;
    void** r8_370;
    signed char al371;
    void** rdx372;
    void** rsi373;
    void** rcx374;
    int1_t zf375;
    signed char al376;
    struct s0* r14_377;
    void** rbp378;
    struct s0** rsp379;
    void** rax380;
    void** v381;
    uint32_t eax382;
    struct s0* v383;
    uint32_t v384;
    void** rcx385;
    void** r15_386;
    uint64_t rax387;
    int32_t esi388;
    int64_t rax389;
    void* rax390;
    void* rdx391;
    void* rax392;
    void** rdx393;
    struct s0* r13_394;
    void** rax395;
    uint64_t rdi396;
    uint1_t cf397;
    int64_t rbx398;
    void** rbx399;
    unsigned char al400;
    void* v401;
    struct s0* v402;
    uint32_t eax403;
    unsigned char v404;
    uint32_t v405;
    int64_t rax406;
    void* v407;
    void** rax408;
    void* rax409;
    void** r14_410;
    void** v411;
    void** rbx412;
    void** rax413;
    void** rsi414;
    void** v415;
    void** rax416;
    void** rdi417;
    void** r15_418;
    void** rdi419;
    struct s0* rbp420;
    struct s0* rbx421;
    void** r15_422;
    int32_t eax423;
    uint1_t zf424;
    void** r15_425;
    void** rdx426;
    void** rax427;
    void** rsi428;
    void** rax429;
    void** rax430;
    void** rax431;
    void* v432;
    void** rax433;
    void** r14_434;
    void** rdi435;
    void** r15_436;
    void** r14_437;
    void** rax438;
    void** rsi439;
    void** rax440;
    void** rax441;
    void** v442;
    void** v443;
    void** rdx444;
    void** v445;
    void** rdi446;
    void*** v447;
    void** v448;
    void** rax449;
    void* rax450;
    void** rax451;
    void** rdx452;
    void** rcx453;
    int32_t esi454;
    void** rax455;
    uint64_t rdi456;
    uint1_t cf457;
    unsigned char al458;
    void* r11_459;
    uint64_t v460;
    void** rax461;
    void** rdx462;
    unsigned char al463;
    int64_t v464;
    void** rdx465;
    void*** v466;
    void** v467;
    void** rax468;

    while (rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 72), rax8 = g28, v9 = rax8, eax10 = *reinterpret_cast<void***>(rdi + 4), *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0), *reinterpret_cast<void***>(rsi + 8) = reinterpret_cast<void**>(0), *reinterpret_cast<void***>(rsi + 16) = reinterpret_cast<void**>(0), !!eax10) {
        zf11 = dev_debug == 0;
        rbp12 = rdi;
        r12_13 = rsi;
        if (!zf11) {
            rcx = stderr;
            *reinterpret_cast<int32_t*>(&rdx) = 17;
            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            *reinterpret_cast<int32_t*>(&rsi) = 1;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            fun_2a80("[trial division] ", 1, 17, rcx, r8, r9);
            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
        }
        rdi14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 16);
        r13_15 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 32);
        v16 = rdi14;
        fun_2ab0(rdi14, rsi, rdx, rcx, r8, rdi14, rsi, rdx, rcx, r8);
        rax17 = fun_25d0(rbp12, rbp12);
        rbx18 = rax17;
        rdx19 = rax17;
        fun_2840(rbp12, rbp12, rdx19, rcx, r8, r9);
        rsp20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8 - 8 + 8);
        if (rbx18) {
            do {
                fun_29f0(r13_15, 2, r13_15, 2);
                mp_factor_insert(r12_13, r13_15, rdx19, rcx, r8, r9);
                fun_2940(r13_15, r13_15, rdx19, rcx, r8, r9);
                rsp20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp20) - 8 + 8 - 8 + 8 - 8 + 8);
                --rbx18;
            } while (rbx18);
        }
        *reinterpret_cast<int32_t*>(&rbx21) = 1;
        *reinterpret_cast<int32_t*>(&rbx21 + 4) = 0;
        *reinterpret_cast<int32_t*>(&r15_22) = 3;
        *reinterpret_cast<int32_t*>(&r15_22 + 4) = 0;
        r13_23 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp20) + 32);
        while (1) {
            eax24 = fun_28f0(rbp12, r15_22, rdx19, rcx, r8, r9);
            rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp20) - 8 + 8);
            if (!eax24) {
                do {
                    *reinterpret_cast<int32_t*>(&r14_26) = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbx21 + 1));
                    *reinterpret_cast<uint32_t*>(&rax27) = *reinterpret_cast<unsigned char*>(rbx21 + 0x10340);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
                    r15_22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_22) + reinterpret_cast<uint64_t>(rax27));
                    rsi28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_22) * reinterpret_cast<unsigned char>(r15_22));
                    eax29 = fun_2aa0(rbp12, rsi28, rdx19, rcx, r8, r9);
                    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
                    if (eax29 < 0) 
                        goto addr_4268_10;
                    if (*reinterpret_cast<int32_t*>(&r14_26) == 0x29d) 
                        goto addr_4268_10;
                    *reinterpret_cast<int32_t*>(&rbx21) = *reinterpret_cast<int32_t*>(&r14_26);
                    *reinterpret_cast<int32_t*>(&rbx21 + 4) = 0;
                    eax31 = fun_28f0(rbp12, r15_22, rdx19, rcx, r8, r9);
                    rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                } while (!eax31);
            }
            rdx19 = r15_22;
            fun_27e0(rbp12, rbp12, rdx19, rcx, r8, r9);
            fun_29f0(r13_23, r15_22, r13_23, r15_22);
            mp_factor_insert(r12_13, r13_23, rdx19, rcx, r8, r9);
            fun_2940(r13_23, r13_23, rdx19, rcx, r8, r9);
            rsp20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        }
        addr_4268_10:
        fun_2940(v16, rsi28, rdx19, rcx, r8, r9);
        rdi = rbp12;
        eax32 = fun_2aa0(rdi, 1, rdx19, rcx, r8, r9);
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8 - 8 + 8);
        if (!eax32) 
            break;
        zf33 = dev_debug == 0;
        if (!zf33) {
            rcx = stderr;
            *reinterpret_cast<int32_t*>(&rdx19) = 19;
            *reinterpret_cast<int32_t*>(&rdx19 + 4) = 0;
            fun_2a80("[is number prime?] ", 1, 19, rcx, r8, r9);
            rdi = rbp12;
            eax32 = fun_2aa0(rdi, 1, 19, rcx, r8, r9);
            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8);
        }
        if (reinterpret_cast<uint1_t>(eax32 < 0) | reinterpret_cast<uint1_t>(eax32 == 0)) 
            goto addr_42b5_17;
        rdi = rbp12;
        eax34 = fun_2aa0(rdi, 0x17ded79, rdx19, rcx, r8, r9);
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
        if (eax34 < 0) 
            goto addr_4318_19;
        rdi = rbp12;
        al35 = mp_prime_p_part_0(rdi, 0x17ded79, rdx19, rcx, r8, r9);
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
        if (al35) 
            goto addr_4318_19;
        addr_42b5_17:
        rax36 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
        if (rax36) 
            goto addr_4377_21;
        rbp12 = rbp12;
        rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) + 72 + 8 + 8 + 8 + 8 + 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8);
        v38 = r12_13;
        rax39 = g28;
        v40 = rax39;
        zf41 = dev_debug == 0;
        if (!zf41) {
            rdi42 = stderr;
            rcx = reinterpret_cast<void**>(1);
            fun_2a90(rdi42, 1, "[pollard-rho (%lu)] ", 1, r8, r9, v43, 1, v44, v45, v46, v47);
            rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
        }
        *reinterpret_cast<int32_t*>(&rdx48) = 0;
        *reinterpret_cast<int32_t*>(&rdx48 + 4) = 0;
        r15_22 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp37) + 64);
        *reinterpret_cast<int32_t*>(&rbx21) = 1;
        *reinterpret_cast<int32_t*>(&rbx21 + 4) = 0;
        r14_26 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp37) + 0x90);
        r12_13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp37) + 0x80);
        r13_23 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp37) + 0x70);
        fun_2780(r12_13, r14_26, r12_13, r14_26);
        rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
        rax50 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp49) + 96);
        v44 = rax50;
        fun_28b0(rax50, 2);
        fun_28b0(r15_22, 2);
        rax51 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp49) - 8 + 8 - 8 + 8 + 80);
        v43 = rax51;
        fun_28b0(rax51, 2);
        fun_29f0(r13_23, 1);
        v45 = reinterpret_cast<void**>(1);
        while (eax52 = fun_2aa0(rbp12, 1, rdx48, rcx, r8, r9), !!eax52) {
            r15_53 = rbx21;
            rbx54 = r15_22;
            while (1) {
                fun_27b0(r12_13, rbx54, rbx54, rcx, r8, r9);
                fun_29d0(rbx54, r12_13, rbp12, rcx, r8, r9);
                fun_28c0(rbx54, rbx54, 1, rcx, r8, r9);
                fun_26c0(r12_13, v43, rbx54, rcx, r8, r9);
                fun_27b0(r14_26, r13_23, r12_13, rcx, r8, r9);
                rdx55 = rbp12;
                fun_29d0(r13_23, r14_26, rdx55, rcx, r8, r9);
                rax56 = r15_53;
                *reinterpret_cast<uint32_t*>(&rax57) = *reinterpret_cast<uint32_t*>(&rax56) & 31;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax57) + 4) = 0;
                if (rax57 != 1) {
                    --r15_53;
                    if (r15_53) 
                        continue;
                } else {
                    rdx55 = rbp12;
                    fun_2a40(r12_13, r13_23, rdx55, rcx, r8, r9);
                    eax58 = fun_2aa0(r12_13, 1, rdx55, rcx, r8, r9);
                    if (eax58) 
                        break;
                    fun_27a0(v44, rbx54, rdx55, rcx, r8, v44, rbx54, rdx55, rcx, r8);
                    --r15_53;
                    if (r15_53) 
                        continue;
                }
                fun_27a0(v43, rbx54, rdx55, rcx, r8, v43, rbx54, rdx55, rcx, r8);
                rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v45) + reinterpret_cast<unsigned char>(v45));
                v59 = rcx;
                if (v45) {
                    v60 = r14_26;
                    r14_61 = rbx54;
                    v62 = r13_23;
                    r13_63 = r15_53;
                    r15_64 = v45;
                    do {
                        ++r13_63;
                        fun_27b0(r12_13, r14_61, r14_61, rcx, r8, r9);
                        fun_29d0(r14_61, r12_13, rbp12, rcx, r8, r9);
                        rdx55 = reinterpret_cast<void**>(1);
                        fun_28c0(r14_61, r14_61, 1, rcx, r8, r9);
                    } while (r15_64 != r13_63);
                    rbx54 = r14_61;
                    r13_23 = v62;
                    r14_26 = v60;
                }
                fun_27a0(v44, rbx54, rdx55, rcx, r8, v44, rbx54, rdx55, rcx, r8);
                r15_53 = v45;
                v45 = v59;
            }
            v46 = r15_53;
            r15_22 = rbx54;
            v47 = r13_23;
            r13_65 = v44;
            v66 = r14_26;
            r14_67 = r12_13;
            r12_68 = v43;
            do {
                fun_27b0(r14_67, r13_65, r13_65, rcx, r8, r9);
                fun_29d0(r13_65, r14_67, rbp12, rcx, r8, r9);
                fun_28c0(r13_65, r13_65, 1, rcx, r8, r9);
                fun_26c0(r14_67, r12_68, r13_65, rcx, r8, r9);
                fun_2a40(r14_67, r14_67, rbp12, rcx, r8, r9);
                eax69 = fun_2aa0(r14_67, 1, rbp12, rcx, r8, r9);
            } while (!eax69);
            v43 = r12_68;
            r12_13 = r14_67;
            rdx70 = r12_13;
            r14_26 = v66;
            v44 = r13_65;
            rbx21 = v46;
            r13_23 = v47;
            fun_2760(rbp12, rbp12, rdx70, rcx, r8, r9);
            eax71 = fun_2aa0(r12_13, 1, rdx70, rcx, r8, r9);
            if (reinterpret_cast<uint1_t>(eax71 < 0) | reinterpret_cast<uint1_t>(eax71 == 0) || (eax72 = fun_2aa0(r12_13, 0x17ded79, rdx70, rcx, r8, r9), eax72 >= 0) && (al73 = mp_prime_p_part_0(r12_13, 0x17ded79, rdx70, rcx, r8, r9), !al73)) {
                zf74 = dev_debug == 0;
                if (!zf74) {
                    rcx = stderr;
                    fun_2a80("[composite factor--restarting pollard-rho] ", 1, 43, rcx, r8, r9);
                }
                rdx70 = v38;
                mp_factor_using_pollard_rho(r12_13, 2, rdx70, rcx, r8, r9);
            } else {
                mp_factor_insert(v38, r12_13, rdx70, rcx, r8, r9);
            }
            eax75 = fun_2aa0(rbp12, 1, rdx70, rcx, r8, r9);
            if (reinterpret_cast<uint1_t>(eax75 < 0) | reinterpret_cast<uint1_t>(eax75 == 0)) 
                goto addr_401c_45;
            eax76 = fun_2aa0(rbp12, 0x17ded79, rdx70, rcx, r8, r9);
            if (eax76 < 0) 
                goto addr_405d_47;
            al77 = mp_prime_p_part_0(rbp12, 0x17ded79, rdx70, rcx, r8, r9);
            if (al77) 
                goto addr_405d_47;
            addr_401c_45:
            fun_29d0(r15_22, r15_22, rbp12, rcx, r8, r9);
            fun_29d0(v43, v43, rbp12, rcx, r8, r9);
            rdx48 = rbp12;
            fun_29d0(v44, v44, rdx48, rcx, r8, r9);
        }
        addr_406a_49:
        fun_2660();
        rax78 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v40) - reinterpret_cast<unsigned char>(g28));
        if (!rax78) 
            goto addr_40a2_50;
        fun_2740();
        continue;
        addr_405d_47:
        mp_factor_insert(v38, rbp12, rdx70, rcx, r8, r9);
        goto addr_406a_49;
    }
    rax79 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
    if (!rax79) {
        return rax79;
    }
    addr_4318_19:
    rax80 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
    if (rax80) {
        addr_4377_21:
        fun_2740();
    } else {
        r13_81 = r12_13;
        r12_82 = rbp12;
        rcx83 = *reinterpret_cast<void***>(r12_13);
        rdx84 = *reinterpret_cast<void***>(r12_13 + 8);
        r14_85 = reinterpret_cast<struct s0*>(*reinterpret_cast<void***>(r12_13 + 16) + 0xffffffffffffffff);
        v86 = *reinterpret_cast<void***>(r12_13 + 16);
        v87 = rcx83;
        v88 = rdx84;
        if (reinterpret_cast<int64_t>(r14_85) < reinterpret_cast<int64_t>(0)) 
            goto addr_37ca_56; else 
            goto addr_361b_57;
    }
    rbx89 = rdi;
    rsp90 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x98);
    rax91 = g28;
    v92 = rax91;
    r12_93 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp90) + 96);
    r15_94 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp90) + 64);
    rbp95 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp90) + 80);
    r14_96 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp90) + 0x70);
    v97 = r15_94;
    fun_2780(r15_94, rbp95, r15_94, rbp95);
    fun_2650(r12_93, rbx89, 1, r14_96);
    rax98 = fun_25d0(r12_93);
    r13_99 = rax98;
    v100 = rax98;
    fun_2960(r15_94, r12_93, rax98, r14_96);
    fun_2680(rbp95, 2, rax98, r14_96);
    r9_101 = r13_99;
    r8_102 = r15_94;
    rcx103 = r14_96;
    al104 = mp_millerrabin(rbx89, r12_93, rbp95, rcx103, r8_102, r9_101);
    rsp105 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp90) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    if (al104) {
        r13_99 = reinterpret_cast<void**>(0x10340);
        fun_27a0(r14_96, r12_93, rbp95, rcx103, r8_102, r14_96, r12_93, rbp95, rcx103, r8_102);
        rsp106 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp105) - 8 + 8);
        rsi107 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp106) + 32);
        mp_factor(r14_96, rsi107, rbp95, rcx103, r8_102, r9_101);
        rsp108 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp106) - 8 + 8);
        goto addr_4458_60;
    }
    addr_455c_61:
    rdi109 = v97;
    rcx110 = r14_96;
    rdx111 = r12_93;
    rsi112 = rbp95;
    fun_2660();
    rax113 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v92) - reinterpret_cast<unsigned char>(g28));
    if (!rax113) {
        goto v114;
    }
    fun_2740();
    rsp115 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp105) - 8 + 8 - 8 + 8);
    while (1) {
        r15_116 = rdi109;
        rbp117 = rsi112;
        rsp118 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp115) - 8 - 8 - 8 - 8 - 8 - 8 - 0x88);
        v119 = rdx111;
        v120 = rcx110;
        rax121 = g28;
        v122 = rax121;
        rcx123 = reinterpret_cast<unsigned char>(rcx110) - (reinterpret_cast<unsigned char>(rcx110) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx110) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx110) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi109) < reinterpret_cast<unsigned char>(2)))))));
        *reinterpret_cast<int32_t*>(&rdx124) = 0;
        *reinterpret_cast<int32_t*>(&rdx124 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx125) = *reinterpret_cast<uint32_t*>(&rcx123) & 64;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx125) + 4) = 0;
        rcx126 = reinterpret_cast<uint64_t>(rcx125 + 63);
        cf127 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi109) < reinterpret_cast<unsigned char>(2));
        *reinterpret_cast<unsigned char*>(&rdx124) = cf127;
        rax128 = reinterpret_cast<void**>(-static_cast<uint64_t>(reinterpret_cast<uint1_t>(0 < static_cast<uint64_t>(cf127))) + 1);
        do {
            rsi129 = rdx124;
            rdx124 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx124) + reinterpret_cast<unsigned char>(rdx124));
            rax128 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax128) + reinterpret_cast<unsigned char>(rax128)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rsi129) >> 63));
            if (reinterpret_cast<unsigned char>(r15_116) < reinterpret_cast<unsigned char>(rax128) || r15_116 == rax128 && reinterpret_cast<unsigned char>(rbp117) <= reinterpret_cast<unsigned char>(rdx124)) {
                cf130 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx124) < reinterpret_cast<unsigned char>(rbp117));
                rdx124 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx124) - reinterpret_cast<unsigned char>(rbp117));
                rax128 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax128) - (reinterpret_cast<unsigned char>(r15_116) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax128) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r15_116) + static_cast<uint64_t>(cf130))))));
            }
            cf131 = rcx126 < 1;
            --rcx126;
        } while (!cf131);
        v132 = rdx124;
        v133 = rax128;
        tmp64_134 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx124) + reinterpret_cast<unsigned char>(rdx124));
        rax135 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax128) + reinterpret_cast<unsigned char>(rax128) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_134) < reinterpret_cast<unsigned char>(rdx124))));
        r12_136 = tmp64_134;
        r10_137 = rax135;
        if (reinterpret_cast<unsigned char>(rax135) > reinterpret_cast<unsigned char>(r15_116) || rax135 == r15_116 && reinterpret_cast<unsigned char>(tmp64_134) >= reinterpret_cast<unsigned char>(rbp117)) {
            r12_136 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_134) - reinterpret_cast<unsigned char>(rbp117));
            r10_137 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax135) - (reinterpret_cast<unsigned char>(r15_116) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax135) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r15_116) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_134) < reinterpret_cast<unsigned char>(rbp117))))))));
        }
        v138 = r10_137;
        v139 = r10_137;
        if (r15_116) 
            goto addr_468c_71;
        if (rbp117 == 1) 
            goto addr_4ad5_73;
        addr_468c_71:
        v140 = r12_136;
        r13_99 = r15_116;
        *reinterpret_cast<int32_t*>(&rbx89) = 1;
        *reinterpret_cast<int32_t*>(&rbx89 + 4) = 0;
        v141 = reinterpret_cast<void**>(1);
        r14_142 = reinterpret_cast<void**>(rsp118 + 0x70);
        r15_143 = r12_136;
        v144 = rsp118 + 0x68;
        while (1) {
            r9_145 = r13_99;
            r13_99 = rbx89;
            rax146 = reinterpret_cast<unsigned char>(rbp117) >> 1;
            rbx89 = rbp117;
            *reinterpret_cast<uint32_t*>(&rax147) = *reinterpret_cast<uint32_t*>(&rax146) & 0x7f;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax147) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rax148) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax147);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax148) + 4) = 0;
            rdx149 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax148) + reinterpret_cast<int64_t>(rax148) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax148) * reinterpret_cast<int64_t>(rax148) * reinterpret_cast<unsigned char>(rbp117)));
            rax150 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx149) + reinterpret_cast<uint64_t>(rdx149) - reinterpret_cast<uint64_t>(rdx149) * reinterpret_cast<uint64_t>(rdx149) * reinterpret_cast<unsigned char>(rbp117));
            rax151 = rbp117;
            rbp152 = r10_137;
            *reinterpret_cast<uint32_t*>(&rax153) = *reinterpret_cast<uint32_t*>(&rax151) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax153) + 4) = 0;
            v154 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax150) + reinterpret_cast<uint64_t>(rax150) - reinterpret_cast<uint64_t>(rax150) * reinterpret_cast<uint64_t>(rax150) * reinterpret_cast<unsigned char>(rbp117));
            v155 = rax153;
            while (1) {
                rax156 = mulredc2(r14_142, rbp152, r12_136, rbp152, r12_136, r9_145, rbx89, v133);
                tmp64_157 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax156) + reinterpret_cast<unsigned char>(v119));
                rdx158 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v159) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_157) < reinterpret_cast<unsigned char>(rax156))));
                v160 = rdx158;
                r12_136 = tmp64_157;
                rbp152 = rdx158;
                if (reinterpret_cast<unsigned char>(rdx158) > reinterpret_cast<unsigned char>(r9_145) || rdx158 == r9_145 && reinterpret_cast<unsigned char>(tmp64_157) >= reinterpret_cast<unsigned char>(rbx89)) {
                    rdx161 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx158) - (reinterpret_cast<unsigned char>(r9_145) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx158) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r9_145) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_157) < reinterpret_cast<unsigned char>(rbx89))))))));
                    v160 = rdx161;
                    r12_136 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_157) - reinterpret_cast<unsigned char>(rbx89));
                    rbp152 = rdx161;
                }
                r8_162 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v140) - reinterpret_cast<unsigned char>(r12_136));
                rcx163 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v138) - (reinterpret_cast<unsigned char>(rbp152) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v138) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp152) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v140) < reinterpret_cast<unsigned char>(r12_136))))))));
                if (reinterpret_cast<signed char>(rcx163) < reinterpret_cast<signed char>(0)) {
                    tmp64_164 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_162) + reinterpret_cast<unsigned char>(rbx89));
                    cf165 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_164) < reinterpret_cast<unsigned char>(r8_162));
                    r8_162 = tmp64_164;
                    rcx163 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx163) + reinterpret_cast<unsigned char>(r9_145) + static_cast<uint64_t>(cf165));
                }
                rax166 = mulredc2(r14_142, v133, v132, rcx163, r8_162, r9_145, rbx89, v133);
                v132 = rax166;
                v133 = v167;
                rax168 = r13_99;
                *reinterpret_cast<uint32_t*>(&rax169) = *reinterpret_cast<uint32_t*>(&rax168) & 31;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax169) + 4) = 0;
                rsp118 = rsp118 - 8 - 8 - 8 + 8 + 8 + 8 - 8 - 8 - 8 + 8 + 8 + 8;
                r9_145 = r9_145;
                if (rax169 == 1) {
                    if (!v155) 
                        goto addr_4cba_81;
                    if (reinterpret_cast<unsigned char>(v132) | reinterpret_cast<unsigned char>(v133)) 
                        goto addr_48c0_83;
                } else {
                    addr_47d9_84:
                    --r13_99;
                    if (r13_99) 
                        continue; else 
                        goto addr_47e3_85;
                }
                v170 = r9_145;
                rax171 = rbx89;
                if (r9_145) 
                    break;
                addr_48ee_87:
                if (!reinterpret_cast<int1_t>(rax171 == 1)) 
                    goto addr_4920_88;
                v139 = rbp152;
                r15_143 = r12_136;
                goto addr_47d9_84;
                addr_48c0_83:
                rax171 = gcd2_odd_part_0(v144, v133, v132, r9_145, rbx89, r9_145);
                rsp118 = rsp118 - 8 + 8;
                r9_145 = r9_145;
                if (v170) 
                    goto addr_4920_88; else 
                    goto addr_48ee_87;
                addr_47e3_85:
                v138 = rbp152;
                r15_143 = r12_136;
                v172 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v141) + reinterpret_cast<unsigned char>(v141));
                if (v141) {
                    v173 = r12_136;
                    rdx174 = r12_136;
                    r15_175 = r13_99;
                    r12_176 = v154;
                    r13_177 = v119;
                    rsi178 = rbp152;
                    rbp179 = r9_145;
                    do {
                        rax180 = mulredc2(r14_142, rsi178, rdx174, rsi178, rdx174, rbp179, rbx89, r12_176);
                        tmp64_181 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax180) + reinterpret_cast<unsigned char>(r13_177));
                        rcx182 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v183) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_181) < reinterpret_cast<unsigned char>(rax180))));
                        rdx174 = tmp64_181;
                        rsi178 = rcx182;
                        rsp118 = rsp118 - 8 - 8 - 8 + 8 + 8 + 8;
                        if (reinterpret_cast<unsigned char>(rcx182) > reinterpret_cast<unsigned char>(rbp179) || rcx182 == rbp179 && reinterpret_cast<unsigned char>(tmp64_181) >= reinterpret_cast<unsigned char>(rbx89)) {
                            rdx174 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_181) - reinterpret_cast<unsigned char>(rbx89));
                            rsi178 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx182) - (reinterpret_cast<unsigned char>(rbp179) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx182) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp179) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_181) < reinterpret_cast<unsigned char>(rbx89))))))));
                        }
                        ++r15_175;
                    } while (v141 != r15_175);
                    r12_136 = v173;
                    r9_145 = rbp179;
                    r15_143 = rdx174;
                    rbp152 = rsi178;
                }
                r13_99 = v141;
                v140 = r12_136;
                r12_136 = r15_143;
                v139 = rbp152;
                v141 = v172;
            }
            addr_4920_88:
            v184 = r12_136;
            r11_185 = r15_143;
            r10_186 = v139;
            v187 = r13_99;
            r15_188 = r14_142;
            r13_99 = rbx89;
            r12_189 = v140;
            r14_142 = v154;
            rbp190 = v144;
            rbx89 = r9_145;
            do {
                r9_191 = rbx89;
                rax192 = mulredc2(r15_188, r10_186, r11_185, r10_186, r11_185, r9_191, r13_99, r14_142);
                tmp64_193 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax192) + reinterpret_cast<unsigned char>(v119));
                rdx194 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v195) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_193) < reinterpret_cast<unsigned char>(rax192))));
                v139 = rdx194;
                r11_185 = tmp64_193;
                rcx196 = r13_99;
                r10_186 = rdx194;
                rsp118 = rsp118 - 8 - 8 - 8 + 8 + 8 + 8;
                if (reinterpret_cast<unsigned char>(rdx194) > reinterpret_cast<unsigned char>(rbx89) || rdx194 == rbx89 && reinterpret_cast<unsigned char>(tmp64_193) >= reinterpret_cast<unsigned char>(r13_99)) {
                    rdx197 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx194) - (reinterpret_cast<unsigned char>(rbx89) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx194) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbx89) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_193) < reinterpret_cast<unsigned char>(r13_99))))))));
                    v139 = rdx197;
                    r11_185 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_193) - reinterpret_cast<unsigned char>(r13_99));
                    r10_186 = rdx197;
                }
                rdx198 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_189) - reinterpret_cast<unsigned char>(r11_185));
                rsi199 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v138) - (reinterpret_cast<unsigned char>(r10_186) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v138) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r10_186) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_189) < reinterpret_cast<unsigned char>(r11_185))))))));
                if (reinterpret_cast<signed char>(rsi199) < reinterpret_cast<signed char>(0)) {
                    tmp64_200 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx198) + reinterpret_cast<unsigned char>(r13_99));
                    cf201 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_200) < reinterpret_cast<unsigned char>(rdx198));
                    rdx198 = tmp64_200;
                    rsi199 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi199) + reinterpret_cast<unsigned char>(rbx89) + static_cast<uint64_t>(cf201));
                }
                if (reinterpret_cast<unsigned char>(rsi199) | reinterpret_cast<unsigned char>(rdx198)) {
                    rcx196 = rbx89;
                    rax202 = gcd2_odd_part_0(rbp190, rsi199, rdx198, rcx196, r13_99, r9_191);
                    rsp118 = rsp118 - 8 + 8;
                    rdi203 = v170;
                    if (rdi203) 
                        goto addr_4a05_103;
                } else {
                    rdi203 = rbx89;
                    v170 = rbx89;
                    rax202 = r13_99;
                    if (rdi203) 
                        goto addr_4a05_103;
                }
            } while (reinterpret_cast<int1_t>(rax202 == 1));
            r8_204 = rax202;
            rax205 = reinterpret_cast<unsigned char>(rax202) >> 1;
            *reinterpret_cast<uint32_t*>(&rax206) = *reinterpret_cast<uint32_t*>(&rax205) & 0x7f;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax206) + 4) = 0;
            r13_207 = rbx89;
            r14_142 = r15_188;
            *reinterpret_cast<uint32_t*>(&rax208) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax206);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax208) + 4) = 0;
            rbx89 = v187;
            rdx209 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax208) + reinterpret_cast<int64_t>(rax208) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax208) * reinterpret_cast<int64_t>(rax208) * reinterpret_cast<unsigned char>(r8_204)));
            rdx210 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdx209) * reinterpret_cast<uint64_t>(rdx209) * reinterpret_cast<unsigned char>(r8_204));
            rax211 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx209) + reinterpret_cast<uint64_t>(rdx209) - reinterpret_cast<unsigned char>(rdx210));
            rcx196 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax211) + reinterpret_cast<uint64_t>(rax211) - reinterpret_cast<uint64_t>(rax211) * reinterpret_cast<uint64_t>(rax211) * reinterpret_cast<unsigned char>(r8_204));
            rbp117 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_99) * reinterpret_cast<unsigned char>(rcx196));
            if (reinterpret_cast<unsigned char>(r13_207) < reinterpret_cast<unsigned char>(r8_204)) {
                *reinterpret_cast<int32_t*>(&r13_99) = 0;
                *reinterpret_cast<int32_t*>(&r13_99 + 4) = 0;
            } else {
                rdx210 = __intrinsic();
                r9_191 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r13_207) - reinterpret_cast<unsigned char>(rdx210)) * reinterpret_cast<unsigned char>(rcx196));
                r13_99 = r9_191;
            }
            if (reinterpret_cast<unsigned char>(r8_204) <= reinterpret_cast<unsigned char>(1) || reinterpret_cast<unsigned char>(r8_204) > reinterpret_cast<unsigned char>(0x17ded78) && (al212 = prime_p_part_0(r8_204, rsi199, rdx210, rcx196), rsp118 = rsp118 - 8 + 8, r8_204 = r8_204, al212 == 0)) {
                rdx213 = v120;
                rsi214 = v119 + 1;
                factor_using_pollard_rho(r8_204, rsi214, rdx213, rcx196);
                rsp118 = rsp118 - 8 + 8;
            } else {
                *reinterpret_cast<int32_t*>(&rdx213) = 1;
                *reinterpret_cast<int32_t*>(&rdx213 + 4) = 0;
                rsi214 = r8_204;
                factor_insert_multiplicity(v120, rsi214, 1);
                rsp118 = rsp118 - 8 + 8;
            }
            if (!r13_99) 
                goto addr_4aa0_113;
            rsi214 = rbp117;
            rdi215 = r13_99;
            al216 = prime2_p(rdi215, rsi214);
            rsp118 = rsp118 - 8 + 8;
            if (al216) 
                goto addr_4c8b_115;
            rax217 = mod2(rsp118 + 80, v160, v184, r13_99, rbp117, r9_191);
            rsp218 = reinterpret_cast<void*>(rsp118 - 8 + 8);
            r12_136 = rax217;
            rax219 = mod2(reinterpret_cast<int64_t>(rsp218) + 88, v138, v140, r13_99, rbp117, r9_191);
            rsp220 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp218) - 8 + 8);
            v140 = rax219;
            rax221 = mod2(reinterpret_cast<int64_t>(rsp220) + 96, v139, r11_185, r13_99, rbp117, r9_191);
            rsp118 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp220) - 8 + 8);
            r10_137 = v160;
            r15_143 = rax221;
        }
        addr_4a05_103:
        if (rbx89 != rdi203) 
            goto addr_4a19_117;
        if (rax202 == r13_99) 
            goto addr_4ce0_119;
        addr_4a19_117:
        rbx89 = reinterpret_cast<void**>(0xd5c0);
        rsi214 = rax202;
        v141 = rax202;
        rax222 = reinterpret_cast<unsigned char>(rax202) >> 1;
        *reinterpret_cast<uint32_t*>(&rax223) = *reinterpret_cast<uint32_t*>(&rax222) & 0x7f;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax223) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax224) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(0xd5c0) + reinterpret_cast<uint64_t>(rax223));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax224) + 4) = 0;
        rdx225 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax224) + reinterpret_cast<int64_t>(rax224) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax224) * reinterpret_cast<int64_t>(rax224) * reinterpret_cast<unsigned char>(rax202)));
        rax226 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx225) + reinterpret_cast<uint64_t>(rdx225) - reinterpret_cast<uint64_t>(rdx225) * reinterpret_cast<uint64_t>(rdx225) * reinterpret_cast<unsigned char>(rax202));
        rdx213 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax226) + reinterpret_cast<uint64_t>(rax226) - reinterpret_cast<uint64_t>(rax226) * reinterpret_cast<uint64_t>(rax226) * reinterpret_cast<unsigned char>(rax202));
        rbp117 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_99) * reinterpret_cast<unsigned char>(rdx213));
        al227 = prime2_p(rdi203, rsi214);
        rsp118 = rsp118 - 8 + 8;
        if (!al227) 
            goto addr_4c4f_120;
        if (!v170) 
            goto addr_4ca3_122;
        rdi215 = v120;
        if (!*reinterpret_cast<void***>(rdi215 + 8)) 
            goto addr_4a94_124;
        addr_4cfe_125:
        factor_insert_large_part_0();
        r8_228 = rdi215;
        r9_229 = rdx213;
        v230 = r13_99;
        rbp231 = rsi214;
        v232 = rbx89;
        rsp233 = rsp118 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 56;
        *reinterpret_cast<unsigned char*>(rdx213 + 0xfa) = 0;
        *reinterpret_cast<void***>(rdx213 + 8) = reinterpret_cast<void**>(0);
        if (rdi215) 
            goto addr_4d50_127;
        if (reinterpret_cast<unsigned char>(rsi214) <= reinterpret_cast<unsigned char>(1)) 
            goto addr_4d41_129;
        addr_4d50_127:
        if (*reinterpret_cast<unsigned char*>(&rbp231) & 1) {
            addr_4df3_130:
            if (!r8_228) {
                *reinterpret_cast<int32_t*>(&r12_234) = 3;
                *reinterpret_cast<int32_t*>(&r12_234 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r13_235) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_235) + 4) = 0;
                r11_236 = reinterpret_cast<void**>(0x5555555555555555);
                r10_237 = reinterpret_cast<void**>(0xaaaaaaaaaaaaaaab);
            } else {
                r14_238 = reinterpret_cast<void***>(0xd648);
                *reinterpret_cast<uint32_t*>(&rbx239) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx239) + 4) = 0;
                r15_240 = 0xaaaaaaaaaaaaaaab;
                *reinterpret_cast<int32_t*>(&r12_234) = 3;
                *reinterpret_cast<int32_t*>(&r12_234 + 4) = 0;
                while (1) {
                    rcx241 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp231) * r15_240);
                    rdx242 = __intrinsic();
                    if (reinterpret_cast<unsigned char>(rdx242) <= reinterpret_cast<unsigned char>(r8_228)) {
                        r10_243 = *r14_238;
                        while ((r13_244 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(r8_228) - reinterpret_cast<unsigned char>(rdx242)) * r15_240), reinterpret_cast<unsigned char>(r13_244) <= reinterpret_cast<unsigned char>(r10_243)) && (factor_insert_multiplicity(r9_229, r12_234, 1), rsp233 = rsp233 - 8 + 8, r8_228 = r13_244, r9_229 = r9_229, r10_243 = r10_243, rbp231 = rcx241, rdx242 = __intrinsic(), reinterpret_cast<unsigned char>(rdx242) <= reinterpret_cast<unsigned char>(r13_244))) {
                            rcx241 = reinterpret_cast<void**>(r15_240 * reinterpret_cast<unsigned char>(rcx241));
                        }
                    }
                    *reinterpret_cast<uint32_t*>(&rax245) = rbx239->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax245) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&r13_235) = *reinterpret_cast<uint32_t*>(&rbx239);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_235) + 4) = 0;
                    r14_238 = r14_238 + 16;
                    r12_234 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_234) + reinterpret_cast<uint64_t>(rax245));
                    if (!r8_228) 
                        break;
                    if (*reinterpret_cast<uint32_t*>(&rbx239) > 0x29b) 
                        break;
                    r15_240 = *reinterpret_cast<uint64_t*>(r14_238 - 8);
                    rbx239 = reinterpret_cast<struct s30*>(&rbx239->pad8);
                }
                if (*reinterpret_cast<uint32_t*>(&r13_235) > 0x29b) 
                    goto addr_5200_141; else 
                    goto addr_4edd_142;
            }
        } else {
            if (rbp231) {
                __asm__("bsf rdx, rbp");
                ecx246 = 64 - *reinterpret_cast<int32_t*>(&rdx213);
                ecx247 = *reinterpret_cast<int32_t*>(&rdx213);
                rbp231 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp231) >> *reinterpret_cast<signed char*>(&ecx247)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_228) << *reinterpret_cast<unsigned char*>(&ecx246)));
                factor_insert_multiplicity(r9_229, 2, *reinterpret_cast<signed char*>(&rdx213));
                rsp233 = rsp233 - 8 + 8;
                r8_228 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_228) >> *reinterpret_cast<signed char*>(&ecx247));
                r9_229 = r9_229;
                goto addr_4df3_130;
            } else {
                __asm__("bsf rcx, r8");
                edx248 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rcx196 + 64));
                rbp231 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_228) >> *reinterpret_cast<signed char*>(&rcx196));
                *reinterpret_cast<int32_t*>(&r12_234) = 3;
                *reinterpret_cast<int32_t*>(&r12_234 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r13_235) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_235) + 4) = 0;
                factor_insert_multiplicity(r9_229, 2, *reinterpret_cast<signed char*>(&edx248));
                rsp233 = rsp233 - 8 + 8;
                r9_229 = r9_229;
                *reinterpret_cast<uint32_t*>(&r8_228) = 0;
                *reinterpret_cast<int32_t*>(&r8_228 + 4) = 0;
                r11_236 = reinterpret_cast<void**>(0x5555555555555555);
                r10_237 = reinterpret_cast<void**>(0xaaaaaaaaaaaaaaab);
            }
        }
        addr_4ef9_146:
        *reinterpret_cast<int32_t*>(&rbx249) = static_cast<int32_t>(r13_235 + 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx249) + 4) = 0;
        r13_235 = rbx249;
        rbx239 = reinterpret_cast<struct s30*>((rbx249 << 4) + reinterpret_cast<unsigned char>(0xd640));
        while (1) {
            *reinterpret_cast<int32_t*>(&r15_240) = static_cast<int32_t>(r13_235 - 1);
            rcx250 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp231) * reinterpret_cast<unsigned char>(r10_237));
            if (reinterpret_cast<unsigned char>(r11_236) >= reinterpret_cast<unsigned char>(rcx250)) {
                do {
                    factor_insert_multiplicity(r9_229, r12_234, 1);
                    rsp233 = rsp233 - 8 + 8;
                    r10_237 = r10_237;
                    r11_236 = r11_236;
                    r9_229 = r9_229;
                    rbp231 = rcx250;
                    rcx250 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx250) * reinterpret_cast<unsigned char>(r10_237));
                    r8_228 = r8_228;
                } while (reinterpret_cast<unsigned char>(r11_236) >= reinterpret_cast<unsigned char>(rcx250));
                rdx251 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx239->f0) * reinterpret_cast<unsigned char>(rbp231));
                if (reinterpret_cast<unsigned char>(rbx239->f8) < reinterpret_cast<unsigned char>(rdx251)) 
                    goto addr_4f29_150;
                goto addr_5050_152;
            }
            rdx251 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx239->f0) * reinterpret_cast<unsigned char>(rbp231));
            if (reinterpret_cast<unsigned char>(rbx239->f8) >= reinterpret_cast<unsigned char>(rdx251)) {
                addr_5050_152:
                *reinterpret_cast<uint32_t*>(&rcx252) = *reinterpret_cast<uint32_t*>(&r13_235);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx252) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rcx253) = rcx252->f10340;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx253) + 4) = 0;
                rcx254 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx253) + reinterpret_cast<unsigned char>(r12_234));
            } else {
                addr_4f29_150:
                rdx255 = reinterpret_cast<void**>(rbx239->f10 * reinterpret_cast<unsigned char>(rbp231));
                if (reinterpret_cast<unsigned char>(rdx255) <= reinterpret_cast<unsigned char>(rbx239->f18)) 
                    goto addr_50b0_154; else 
                    goto addr_4f3b_155;
            }
            do {
                rbp231 = rdx251;
                factor_insert_multiplicity(r9_229, rcx254, 1);
                rsp233 = rsp233 - 8 + 8;
                r9_229 = r9_229;
                rcx254 = rcx254;
                r8_228 = r8_228;
                rdx251 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx239->f0) * reinterpret_cast<unsigned char>(rbp231));
            } while (reinterpret_cast<unsigned char>(rdx251) <= reinterpret_cast<unsigned char>(rbx239->f8));
            rdx255 = reinterpret_cast<void**>(rbx239->f10 * reinterpret_cast<unsigned char>(rbp231));
            if (reinterpret_cast<unsigned char>(rdx255) > reinterpret_cast<unsigned char>(rbx239->f18)) {
                addr_4f3b_155:
                rdx256 = reinterpret_cast<void**>(rbx239->f20 * reinterpret_cast<unsigned char>(rbp231));
                if (reinterpret_cast<unsigned char>(rdx256) <= reinterpret_cast<unsigned char>(rbx239->f28)) {
                    addr_5120_158:
                    *reinterpret_cast<int32_t*>(&rcx257) = static_cast<int32_t>(r13_235 + 2);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx257) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi258) = *reinterpret_cast<uint32_t*>(&r13_235);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi258) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi259) = rsi258->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi259) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rcx260) = rcx257->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx260) + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rsi261) = static_cast<int32_t>(r13_235 + 1);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi261) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi262) = rsi261->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi262) + 4) = 0;
                    rcx263 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx260) + reinterpret_cast<int64_t>(rsi259) + reinterpret_cast<unsigned char>(r12_234) + reinterpret_cast<uint64_t>(rsi262));
                } else {
                    addr_4f4d_159:
                    rdx264 = reinterpret_cast<void**>(rbx239->f30 * reinterpret_cast<unsigned char>(rbp231));
                    if (reinterpret_cast<unsigned char>(rdx264) <= reinterpret_cast<unsigned char>(rbx239->f38)) 
                        goto addr_5198_160; else 
                        goto addr_4f5f_161;
                }
            } else {
                goto addr_50b0_154;
            }
            do {
                rbp231 = rdx256;
                v265 = rcx263;
                factor_insert_multiplicity(r9_229, rcx263, 1);
                rsp233 = rsp233 - 8 + 8;
                r9_229 = r9_229;
                rcx263 = v265;
                r8_228 = r8_228;
                rdx256 = reinterpret_cast<void**>(rbx239->f20 * reinterpret_cast<unsigned char>(rbp231));
            } while (reinterpret_cast<unsigned char>(rdx256) <= reinterpret_cast<unsigned char>(rbx239->f28));
            rdx264 = reinterpret_cast<void**>(rbx239->f30 * reinterpret_cast<unsigned char>(rbp231));
            if (reinterpret_cast<unsigned char>(rdx264) > reinterpret_cast<unsigned char>(rbx239->f38)) {
                addr_4f5f_161:
                rdx266 = reinterpret_cast<void**>(rbx239->f40 * reinterpret_cast<unsigned char>(rbp231));
                if (reinterpret_cast<unsigned char>(rdx266) <= reinterpret_cast<unsigned char>(rbx239->f48)) {
                    rbp231 = rdx266;
                    ecx267 = static_cast<uint32_t>(r13_235 + 5);
                    while (1) {
                        edx268 = *reinterpret_cast<uint32_t*>(&r13_235);
                        rsi269 = r12_234;
                        do {
                            *reinterpret_cast<uint32_t*>(&rdi270) = edx268;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi270) + 4) = 0;
                            ++edx268;
                            *reinterpret_cast<uint32_t*>(&rdi271) = rdi270->f10340;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi271) + 4) = 0;
                            rsi269 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi269) + reinterpret_cast<uint64_t>(rdi271));
                        } while (edx268 != ecx267);
                        v265 = r8_228;
                        factor_insert_multiplicity(r9_229, rsi269, 1);
                        rsp233 = rsp233 - 8 + 8;
                        r9_229 = r9_229;
                        r8_228 = v265;
                        ecx267 = ecx267;
                        rdx272 = reinterpret_cast<void**>(rbx239->f40 * reinterpret_cast<unsigned char>(rbp231));
                        if (reinterpret_cast<unsigned char>(rdx272) > reinterpret_cast<unsigned char>(rbx239->f48)) 
                            break;
                        rbp231 = rdx272;
                    }
                }
            } else {
                goto addr_5198_160;
            }
            rdx273 = reinterpret_cast<void**>(rbx239->f50 * reinterpret_cast<unsigned char>(rbp231));
            if (reinterpret_cast<unsigned char>(rdx273) <= reinterpret_cast<unsigned char>(rbx239->f58)) {
                rbp231 = rdx273;
                ecx274 = static_cast<uint32_t>(r13_235 + 6);
                while (1) {
                    edx275 = *reinterpret_cast<uint32_t*>(&r13_235);
                    rsi276 = r12_234;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdi277) = edx275;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi277) + 4) = 0;
                        ++edx275;
                        *reinterpret_cast<uint32_t*>(&rdi278) = rdi277->f10340;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi278) + 4) = 0;
                        rsi276 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi276) + reinterpret_cast<uint64_t>(rdi278));
                    } while (edx275 != ecx274);
                    v265 = r8_228;
                    factor_insert_multiplicity(r9_229, rsi276, 1);
                    rsp233 = rsp233 - 8 + 8;
                    r9_229 = r9_229;
                    r8_228 = v265;
                    ecx274 = ecx274;
                    rdx279 = reinterpret_cast<void**>(rbx239->f50 * reinterpret_cast<unsigned char>(rbp231));
                    if (reinterpret_cast<unsigned char>(rdx279) > reinterpret_cast<unsigned char>(rbx239->f58)) 
                        break;
                    rbp231 = rdx279;
                }
            }
            rdx280 = reinterpret_cast<void**>(rbx239->f60 * reinterpret_cast<unsigned char>(rbp231));
            if (reinterpret_cast<unsigned char>(rdx280) <= reinterpret_cast<unsigned char>(rbx239->f68)) {
                rbp231 = rdx280;
                ecx281 = static_cast<uint32_t>(r13_235 + 7);
                while (1) {
                    edx282 = *reinterpret_cast<uint32_t*>(&r13_235);
                    rsi283 = r12_234;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdi284) = edx282;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi284) + 4) = 0;
                        ++edx282;
                        *reinterpret_cast<uint32_t*>(&rdi285) = rdi284->f10340;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi285) + 4) = 0;
                        rsi283 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi283) + reinterpret_cast<uint64_t>(rdi285));
                    } while (edx282 != ecx281);
                    v265 = r8_228;
                    factor_insert_multiplicity(r9_229, rsi283, 1);
                    rsp233 = rsp233 - 8 + 8;
                    r9_229 = r9_229;
                    r8_228 = v265;
                    ecx281 = ecx281;
                    rdx286 = reinterpret_cast<void**>(rbx239->f60 * reinterpret_cast<unsigned char>(rbp231));
                    if (reinterpret_cast<unsigned char>(rdx286) > reinterpret_cast<unsigned char>(rbx239->f68)) 
                        break;
                    rbp231 = rdx286;
                }
            }
            *reinterpret_cast<int32_t*>(&rax287) = *reinterpret_cast<int32_t*>(&r15_240);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax287) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rax288) = *reinterpret_cast<unsigned char*>(0x10080 + rax287);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax288) + 4) = 0;
            r12_234 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_234) + reinterpret_cast<uint64_t>(rax288));
            if (reinterpret_cast<unsigned char>(rbp231) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r12_234) * reinterpret_cast<unsigned char>(r12_234))) 
                break;
            *reinterpret_cast<uint32_t*>(&rax289) = static_cast<uint32_t>(r13_235 + 7);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax289) + 4) = 0;
            rbx239 = reinterpret_cast<struct s30*>(reinterpret_cast<uint64_t>(rbx239) + 0x80);
            *reinterpret_cast<uint32_t*>(&r13_235) = *reinterpret_cast<uint32_t*>(&r13_235) + 8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_235) + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&rax289) > 0x29b) 
                break;
            rax290 = reinterpret_cast<struct s38*>((rax289 << 4) + reinterpret_cast<unsigned char>(0xd640));
            r10_237 = rax290->f0;
            r11_236 = rax290->f8;
            continue;
            addr_5198_160:
            rbp231 = rdx264;
            ecx291 = static_cast<uint32_t>(r13_235 + 4);
            while (1) {
                edx292 = *reinterpret_cast<uint32_t*>(&r13_235);
                rsi293 = r12_234;
                do {
                    *reinterpret_cast<uint32_t*>(&rdi294) = edx292;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi294) + 4) = 0;
                    ++edx292;
                    *reinterpret_cast<uint32_t*>(&rdi295) = rdi294->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi295) + 4) = 0;
                    rsi293 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi293) + reinterpret_cast<uint64_t>(rdi295));
                } while (ecx291 != edx292);
                factor_insert_multiplicity(r9_229, rsi293, 1);
                rsp233 = rsp233 - 8 + 8;
                r9_229 = r9_229;
                r8_228 = r8_228;
                ecx291 = ecx291;
                rdx296 = reinterpret_cast<void**>(rbx239->f30 * reinterpret_cast<unsigned char>(rbp231));
                if (reinterpret_cast<unsigned char>(rdx296) > reinterpret_cast<unsigned char>(rbx239->f38)) 
                    goto addr_4f5f_161;
                rbp231 = rdx296;
            }
            addr_50b0_154:
            *reinterpret_cast<int32_t*>(&rcx297) = static_cast<int32_t>(r13_235 + 1);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx297) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rsi298) = *reinterpret_cast<uint32_t*>(&r13_235);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi298) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx299) = rcx297->f10340;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx299) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rsi300) = rsi298->f10340;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi300) + 4) = 0;
            rcx301 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx299) + reinterpret_cast<int64_t>(rsi300) + reinterpret_cast<unsigned char>(r12_234));
            do {
                rbp231 = rdx255;
                factor_insert_multiplicity(r9_229, rcx301, 1);
                rsp233 = rsp233 - 8 + 8;
                r9_229 = r9_229;
                rcx301 = rcx301;
                r8_228 = r8_228;
                rdx255 = reinterpret_cast<void**>(rbx239->f10 * reinterpret_cast<unsigned char>(rbp231));
            } while (reinterpret_cast<unsigned char>(rdx255) <= reinterpret_cast<unsigned char>(rbx239->f18));
            rdx256 = reinterpret_cast<void**>(rbx239->f20 * reinterpret_cast<unsigned char>(rbp231));
            if (reinterpret_cast<unsigned char>(rdx256) > reinterpret_cast<unsigned char>(rbx239->f28)) 
                goto addr_4f4d_159;
            goto addr_5120_158;
        }
        addr_5200_141:
        if (!r8_228) 
            goto addr_5209_193;
        rdi302 = r8_228;
        v303 = r9_229;
        v304 = r8_228;
        al305 = prime2_p(rdi302, rbp231);
        rsp233 = rsp233 - 8 + 8;
        if (al305) 
            goto addr_53a9_195;
        rsi112 = rbp231;
        rcx110 = v303;
        *reinterpret_cast<int32_t*>(&rdx111) = 1;
        *reinterpret_cast<int32_t*>(&rdx111 + 4) = 0;
        rbx89 = v232;
        rdi109 = v304;
        r13_99 = v230;
        rsp115 = reinterpret_cast<void*>(rsp233 + 56 + 8 + 8 + 8 + 8 + 8 + 8);
        continue;
        addr_4edd_142:
        *reinterpret_cast<uint32_t*>(&rax306) = *reinterpret_cast<uint32_t*>(&r13_235);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax306) + 4) = 0;
        rax307 = reinterpret_cast<struct s42*>((rax306 << 4) + 0xd640);
        r10_237 = rax307->f0;
        r11_236 = rax307->f8;
        goto addr_4ef9_146;
        addr_4c8b_115:
        if (*reinterpret_cast<void***>(v120 + 8)) 
            goto addr_4cfe_125; else 
            goto addr_4c97_197;
    }
    addr_4cba_81:
    fun_2810("b0 & 1", "src/factor.c", 0x1e0, "gcd2_odd", r8_162, r9_145);
    do {
        fun_2740();
        addr_4ce0_119:
        factor_using_pollard_rho2(rbx89, r13_99, v119 + 1, v120);
        addr_4ad5_73:
        rax308 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(g28));
    } while (rax308);
    goto v309;
    addr_4aa0_113:
    if (reinterpret_cast<unsigned char>(rbp117) <= reinterpret_cast<unsigned char>(1)) {
        addr_4c74_200:
        factor_using_pollard_rho(rbp117, v119, v120, rcx196);
        goto addr_4ad5_73;
    } else {
        addr_4aaa_201:
        if (reinterpret_cast<unsigned char>(rbp117) <= reinterpret_cast<unsigned char>(0x17ded78) || (al310 = prime_p_part_0(rbp117, rsi214, rdx213, rcx196), !!al310)) {
            factor_insert_multiplicity(v120, rbp117, 1, v120, rbp117, 1);
            goto addr_4ad5_73;
        }
    }
    addr_4c4f_120:
    rcx196 = v120;
    rsi214 = v141;
    rdx213 = v119 + 1;
    factor_using_pollard_rho2(v170, rsi214, rdx213, rcx196);
    if (reinterpret_cast<unsigned char>(rbp117) > reinterpret_cast<unsigned char>(1)) 
        goto addr_4aaa_201; else 
        goto addr_4c74_200;
    addr_4ca3_122:
    *reinterpret_cast<int32_t*>(&rdx213) = 1;
    *reinterpret_cast<int32_t*>(&rdx213 + 4) = 0;
    rsi214 = v141;
    factor_insert_multiplicity(v120, rsi214, 1);
    goto addr_4aa0_113;
    addr_4a94_124:
    *reinterpret_cast<void***>(v120) = v141;
    *reinterpret_cast<void***>(v120 + 8) = v170;
    goto addr_4aa0_113;
    addr_5209_193:
    if (reinterpret_cast<unsigned char>(rbp231) <= reinterpret_cast<unsigned char>(1)) {
        addr_4d41_129:
        goto v141;
    } else {
        al311 = prime2_p(0, rbp231);
        r9_312 = r9_229;
        if (al311) {
            rsi313 = rbp231;
            rdi314 = r9_312;
        } else {
            rdi315 = rbp231;
            rdx316 = r9_312;
            *reinterpret_cast<int32_t*>(&rsi317) = 1;
            *reinterpret_cast<int32_t*>(&rsi317 + 4) = 0;
            goto addr_5740_206;
        }
    }
    addr_2f20_207:
    *reinterpret_cast<uint32_t*>(&rax318) = *reinterpret_cast<unsigned char*>(rdi314 + 0xfa);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax318) + 4) = 0;
    r8_319 = rsi313;
    r11_320 = reinterpret_cast<void***>(rdi314 + 16);
    r10_321 = reinterpret_cast<signed char*>(rdi314 + 0xe0);
    r9d322 = *reinterpret_cast<uint32_t*>(&rax318);
    if (!*reinterpret_cast<uint32_t*>(&rax318)) 
        goto addr_2fa1_208;
    ebx323 = static_cast<int32_t>(rax318 - 1);
    rcx324 = reinterpret_cast<void*>(static_cast<int64_t>(ebx323));
    rax325 = rcx324;
    do {
        *reinterpret_cast<int32_t*>(&rsi326) = *reinterpret_cast<int32_t*>(&rax325);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi326) + 4) = 0;
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi314 + reinterpret_cast<uint64_t>(rax325) * 8) + 16)) <= reinterpret_cast<unsigned char>(r8_319)) 
            break;
        rax325 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax325) - 1);
        *reinterpret_cast<int32_t*>(&rsi326) = *reinterpret_cast<int32_t*>(&rax325);
    } while (*reinterpret_cast<int32_t*>(&rax325) != -1);
    goto addr_2f80_212;
    if (*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi314 + reinterpret_cast<uint64_t>(rax325) * 8) + 16) == r8_319) {
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_321) + reinterpret_cast<uint64_t>(rax325)) = reinterpret_cast<signed char>(*reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_321) + reinterpret_cast<uint64_t>(rax325)) + 1);
        goto v141;
    }
    rax327 = reinterpret_cast<void*>(static_cast<int64_t>(static_cast<int32_t>(rsi326 + 1)));
    r11_320 = r11_320 + reinterpret_cast<uint64_t>(rax327) * 8;
    r10_321 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_321) + reinterpret_cast<uint64_t>(rax327));
    if (*reinterpret_cast<int32_t*>(&rsi326) >= ebx323) {
        addr_2fa1_208:
        r9d328 = r9d322 + 1;
        *r11_320 = r8_319;
        *r10_321 = 1;
        *reinterpret_cast<unsigned char*>(rdi314 + 0xfa) = *reinterpret_cast<unsigned char*>(&r9d328);
        goto v141;
    }
    do {
        addr_2f80_212:
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi314 + reinterpret_cast<uint64_t>(rcx324) * 8) + 24) = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi314 + reinterpret_cast<uint64_t>(rcx324) * 8) + 16);
        eax329 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi314) + reinterpret_cast<uint64_t>(rcx324) + 0xe0);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi314) + reinterpret_cast<uint64_t>(rcx324) + 0xe1) = *reinterpret_cast<signed char*>(&eax329);
        rcx324 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx324) - 1);
    } while (*reinterpret_cast<int32_t*>(&rsi326) < *reinterpret_cast<int32_t*>(&rcx324));
    goto addr_2fa1_208;
    addr_5740_206:
    v303 = rsi317;
    v265 = rdx316;
    if (reinterpret_cast<unsigned char>(rdi315) <= reinterpret_cast<unsigned char>(1)) {
        addr_5b07_217:
        fun_2810("(1) < (n)", "src/factor.c", 0x5be, "factor_using_pollard_rho", r8_228, r9_312);
    } else {
        r12_330 = rdi315;
        do {
            rcx331 = r12_330;
            esi332 = 64;
            *reinterpret_cast<int32_t*>(&rax333) = 1;
            *reinterpret_cast<int32_t*>(&rax333 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx334) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx334) + 4) = 0;
            rdi335 = reinterpret_cast<void*>(0);
            do {
                r8_228 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx331) << 63);
                rcx331 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx331) >> 1);
                rdx334 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx334) >> 1 | reinterpret_cast<unsigned char>(r8_228));
                if (reinterpret_cast<unsigned char>(rcx331) < reinterpret_cast<unsigned char>(rax333) || rcx331 == rax333 && reinterpret_cast<uint64_t>(rdx334) <= reinterpret_cast<uint64_t>(rdi335)) {
                    cf336 = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi335) < reinterpret_cast<uint64_t>(rdx334));
                    rdi335 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi335) - reinterpret_cast<uint64_t>(rdx334));
                    rax333 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax333) - (reinterpret_cast<unsigned char>(rcx331) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax333) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx331) + static_cast<uint64_t>(cf336))))));
                }
                --esi332;
            } while (esi332);
            *reinterpret_cast<int32_t*>(&rbp337) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp337) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r15_338) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_338) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r14_339) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_339) + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rbp337) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_330) - reinterpret_cast<uint64_t>(rdi335) > reinterpret_cast<uint64_t>(rdi335));
            rbp340 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbp337) & reinterpret_cast<unsigned char>(r12_330)) + (reinterpret_cast<uint64_t>(rdi335) + reinterpret_cast<uint64_t>(rdi335) - reinterpret_cast<unsigned char>(r12_330)));
            rcx341 = rbp340;
            while (reinterpret_cast<unsigned char>(v303) < reinterpret_cast<unsigned char>(r12_330)) {
                r11_342 = rcx341;
                rax343 = reinterpret_cast<unsigned char>(r12_330) >> 1;
                *reinterpret_cast<uint32_t*>(&rax344) = *reinterpret_cast<uint32_t*>(&rax343) & 0x7f;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax344) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax345) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax344);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax345) + 4) = 0;
                rdx346 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax345) + reinterpret_cast<int64_t>(rax345) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax345) * reinterpret_cast<int64_t>(rax345) * reinterpret_cast<unsigned char>(r12_330)));
                rax347 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx346) + reinterpret_cast<uint64_t>(rdx346) - reinterpret_cast<uint64_t>(rdx346) * reinterpret_cast<uint64_t>(rdx346) * reinterpret_cast<unsigned char>(r12_330));
                r8_348 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax347) + reinterpret_cast<uint64_t>(rax347) - reinterpret_cast<uint64_t>(rax347) * reinterpret_cast<uint64_t>(rax347) * reinterpret_cast<unsigned char>(r12_330));
                r10_349 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_330) - reinterpret_cast<unsigned char>(v303));
                r9_350 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v303) - reinterpret_cast<unsigned char>(r12_330));
                while (1) {
                    rax351 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rax351 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax351) + reinterpret_cast<unsigned char>(r12_330));
                    }
                    rbp340 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rbp340) - (reinterpret_cast<unsigned char>(rbp340) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp340) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp340) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax351) < reinterpret_cast<uint64_t>(r10_349))))))) & reinterpret_cast<unsigned char>(r12_330)) + (reinterpret_cast<uint64_t>(rax351) + reinterpret_cast<uint64_t>(r9_350)));
                    r13_352 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        r13_352 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_352) + reinterpret_cast<unsigned char>(r12_330));
                    }
                    rax353 = r14_339;
                    *reinterpret_cast<uint32_t*>(&rax354) = *reinterpret_cast<uint32_t*>(&rax353) & 31;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax354) + 4) = 0;
                    if (rax354 == 1) {
                        rdi355 = r13_352;
                        rax356 = gcd_odd(rdi355, r12_330);
                        if (!reinterpret_cast<int1_t>(rax356 == 1)) 
                            break;
                    }
                    --r14_339;
                    if (r14_339) 
                        continue;
                    rcx357 = r15_338 + r15_338;
                    if (!r15_338) {
                        r15_338 = rcx357;
                        r11_342 = rbp340;
                    } else {
                        do {
                            rdi358 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                            rax359 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi358) + reinterpret_cast<unsigned char>(r12_330));
                            if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                                rdi358 = rax359;
                            }
                            ++r14_339;
                        } while (r15_338 != r14_339);
                        r11_342 = rbp340;
                        r15_338 = rcx357;
                        rbp340 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax359) - (reinterpret_cast<uint64_t>(rax359) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax359) < reinterpret_cast<uint64_t>(rax359) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi358) < reinterpret_cast<uint64_t>(r10_349)))) & reinterpret_cast<unsigned char>(r12_330)) + (reinterpret_cast<uint64_t>(rdi358) + reinterpret_cast<uint64_t>(r9_350)));
                    }
                }
                r9_312 = r8_348;
                r8_228 = r11_342;
                r11_360 = r9_350;
                do {
                    rsi361 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    rax362 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi361) + reinterpret_cast<unsigned char>(r12_330));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rsi361 = rax362;
                    }
                    rbx363 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax362) - (reinterpret_cast<uint64_t>(rax362) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax362) < reinterpret_cast<uint64_t>(rax362) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rsi361) < reinterpret_cast<uint64_t>(r10_349)))) & reinterpret_cast<unsigned char>(r12_330)) + (reinterpret_cast<uint64_t>(rsi361) + reinterpret_cast<uint64_t>(r11_360)));
                    rsi364 = r12_330;
                    rdi355 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rdi355) - (reinterpret_cast<unsigned char>(rdi355) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi355) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rdi355) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_228) < reinterpret_cast<unsigned char>(rbx363))))))) & reinterpret_cast<unsigned char>(r12_330)) + (reinterpret_cast<unsigned char>(r8_228) - reinterpret_cast<unsigned char>(rbx363)));
                    rax365 = gcd_odd(rdi355, rsi364);
                } while (rax365 == 1);
                rcx366 = r8_228;
                r11_367 = rax365;
                if (rax365 == r12_330) 
                    goto addr_5af7_246;
                rdx368 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_330) % reinterpret_cast<unsigned char>(r11_367));
                rax369 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_330) / reinterpret_cast<unsigned char>(r11_367));
                r8_370 = rax369;
                r12_330 = rax369;
                if (reinterpret_cast<unsigned char>(r11_367) > reinterpret_cast<unsigned char>(1) && (reinterpret_cast<unsigned char>(r11_367) <= reinterpret_cast<unsigned char>(0x17ded78) || (al371 = prime_p_part_0(r11_367, rsi364, rdx368, rcx366), r11_367 = r11_367, rcx366 = rcx366, r8_370 = rax369, !!al371))) {
                    *reinterpret_cast<int32_t*>(&rdx372) = 1;
                    *reinterpret_cast<int32_t*>(&rdx372 + 4) = 0;
                    rsi373 = r11_367;
                    factor_insert_multiplicity(v265, rsi373, 1);
                    r8_228 = r8_370;
                    rcx374 = rcx366;
                    zf375 = reinterpret_cast<int1_t>(r8_228 == 1);
                    if (reinterpret_cast<unsigned char>(r8_228) <= reinterpret_cast<unsigned char>(1)) 
                        goto addr_5aca_249; else 
                        goto addr_5a2f_250;
                }
                rdx372 = v265;
                rsi373 = v303 + 1;
                factor_using_pollard_rho(r11_367, rsi373, rdx372, rcx366);
                r8_228 = r8_370;
                rcx374 = rcx366;
                zf375 = reinterpret_cast<int1_t>(r8_228 == 1);
                if (reinterpret_cast<unsigned char>(r8_228) > reinterpret_cast<unsigned char>(1)) {
                    addr_5a2f_250:
                    if (reinterpret_cast<unsigned char>(r8_228) <= reinterpret_cast<unsigned char>(0x17ded78)) 
                        goto addr_5ad7_252;
                    al376 = prime_p_part_0(r8_228, rsi373, rdx372, rcx374);
                    r8_228 = r8_228;
                    if (al376) 
                        goto addr_5ad7_252;
                } else {
                    addr_5aca_249:
                    if (zf375) 
                        goto addr_5b45_254; else 
                        goto addr_5acc_255;
                }
                rbp340 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp340) % reinterpret_cast<unsigned char>(r8_228));
                rcx341 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx374) % reinterpret_cast<unsigned char>(r8_228));
                continue;
                addr_5acc_255:
                *reinterpret_cast<int32_t*>(&rcx341) = 0;
                *reinterpret_cast<int32_t*>(&rcx341 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rbp340) = 0;
                *reinterpret_cast<int32_t*>(&rbp340 + 4) = 0;
            }
            break;
            addr_5af7_246:
            ++v303;
        } while (reinterpret_cast<unsigned char>(r12_330) > reinterpret_cast<unsigned char>(1));
        goto addr_5b07_217;
    }
    fun_2810("a < n", "src/factor.c", 0x5c4, "factor_using_pollard_rho", r8_228, r9_312);
    addr_5b45_254:
    goto v141;
    addr_5ad7_252:
    rdi314 = v265;
    rsi313 = r8_228;
    goto addr_2f20_207;
    addr_53a9_195:
    if (!*reinterpret_cast<void***>(v303 + 8)) {
        *reinterpret_cast<void***>(v303) = rbp231;
        *reinterpret_cast<void***>(v303 + 8) = v304;
        goto addr_4d41_129;
    }
    factor_insert_large_part_0();
    r14_377 = reinterpret_cast<struct s0*>(rdi302 + 0xffffffffffffffff);
    rbp378 = rdi302;
    rsp379 = reinterpret_cast<struct s0**>(rsp233 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x148);
    rax380 = g28;
    v381 = rax380;
    eax382 = 0;
    v383 = r14_377;
    if (*reinterpret_cast<uint32_t*>(&rdi302) & 1) 
        goto addr_5478_261;
    v384 = 0;
    r14_377 = v383;
    addr_5490_263:
    rcx385 = rbp378;
    *reinterpret_cast<int32_t*>(&r15_386) = 0;
    *reinterpret_cast<int32_t*>(&r15_386 + 4) = 0;
    rax387 = reinterpret_cast<unsigned char>(rbp378) >> 1;
    esi388 = 64;
    *reinterpret_cast<uint32_t*>(&rax389) = *reinterpret_cast<uint32_t*>(&rax387) & 0x7f;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax389) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax390) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax389);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax390) + 4) = 0;
    rdx391 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax390) + reinterpret_cast<int64_t>(rax390) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax390) * reinterpret_cast<int64_t>(rax390) * reinterpret_cast<unsigned char>(rbp378)));
    rax392 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx391) + reinterpret_cast<uint64_t>(rdx391) - reinterpret_cast<uint64_t>(rdx391) * reinterpret_cast<uint64_t>(rdx391) * reinterpret_cast<unsigned char>(rbp378));
    *reinterpret_cast<int32_t*>(&rdx393) = 0;
    *reinterpret_cast<int32_t*>(&rdx393 + 4) = 0;
    r13_394 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax392) + reinterpret_cast<uint64_t>(rax392) - reinterpret_cast<uint64_t>(rax392) * reinterpret_cast<uint64_t>(rax392) * reinterpret_cast<unsigned char>(rbp378));
    *reinterpret_cast<int32_t*>(&rax395) = 1;
    *reinterpret_cast<int32_t*>(&rax395 + 4) = 0;
    do {
        rdi396 = reinterpret_cast<unsigned char>(rcx385) << 63;
        rcx385 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx385) >> 1);
        rdx393 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx393) >> 1) | rdi396);
        if (reinterpret_cast<unsigned char>(rcx385) < reinterpret_cast<unsigned char>(rax395) || rcx385 == rax395 && reinterpret_cast<unsigned char>(rdx393) <= reinterpret_cast<unsigned char>(r15_386)) {
            cf397 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_386) < reinterpret_cast<unsigned char>(rdx393));
            r15_386 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_386) - reinterpret_cast<unsigned char>(rdx393));
            rax395 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax395) - (reinterpret_cast<unsigned char>(rcx385) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax395) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx385) + static_cast<uint64_t>(cf397))))));
        }
        --esi388;
    } while (esi388);
    *reinterpret_cast<int32_t*>(&rbx398) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx398) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&r8_228) = v384;
    *reinterpret_cast<int32_t*>(&r8_228 + 4) = 0;
    r9_312 = r15_386;
    *reinterpret_cast<unsigned char*>(&rbx398) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp378) - reinterpret_cast<unsigned char>(r15_386)) > reinterpret_cast<unsigned char>(r15_386));
    rbx399 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbx398) & reinterpret_cast<unsigned char>(rbp378)) + (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r15_386) + reinterpret_cast<unsigned char>(r15_386)) - reinterpret_cast<unsigned char>(rbp378)));
    al400 = millerrabin(rbp378, r13_394, rbx399, r14_377, *reinterpret_cast<uint32_t*>(&r8_228), r9_312);
    if (al400) {
        v401 = reinterpret_cast<void*>(rsp379 - 1 + 1 + 6);
        factor();
        v402 = r14_377;
        r14_377 = r13_394;
        eax403 = v404;
        r13_394 = reinterpret_cast<struct s0*>(0x10340);
        v405 = eax403;
        *reinterpret_cast<uint32_t*>(&rax406) = eax403 - 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax406) + 4) = 0;
        v407 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v401) + rax406 * 8);
        r12_234 = reinterpret_cast<void**>(2);
        rax408 = rbx399;
        rbx399 = r15_386;
        r15_386 = rax408;
        goto addr_55b0_269;
    }
    addr_56fc_270:
    addr_5690_271:
    rax409 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v381) - reinterpret_cast<unsigned char>(g28));
    if (rax409) {
        fun_2740();
    } else {
        goto v304;
    }
    addr_5719_274:
    *reinterpret_cast<int32_t*>(&rdx316) = 0x4f2;
    *reinterpret_cast<int32_t*>(&rdx316 + 4) = 0;
    rsi317 = reinterpret_cast<void**>("src/factor.c");
    rdi315 = reinterpret_cast<void**>("(s1) < (n)");
    fun_2810("(s1) < (n)", "src/factor.c", 0x4f2, "prime_p", r8_228, r9_312);
    goto addr_5740_206;
    addr_5478_261:
    do {
        r14_377 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(r14_377) >> 1);
        ++eax382;
    } while (!(*reinterpret_cast<unsigned char*>(&r14_377) & 1));
    v384 = eax382;
    goto addr_5490_263;
    addr_4c97_197:
    *reinterpret_cast<void***>(v120) = rbp117;
    *reinterpret_cast<void***>(v120 + 8) = r13_99;
    goto addr_4ad5_73;
    addr_37ca_56:
    r14_410 = v86 + 1;
    v411 = r14_410;
    rbx412 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_410) << 4);
    rax413 = xrealloc(v87, rbx412, v87, rbx412);
    rsi414 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_410) * 8);
    v415 = rax413;
    rax416 = xrealloc(v88, rsi414, v88, rsi414);
    rdi417 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax413) + reinterpret_cast<unsigned char>(rbx412) + 0xfffffffffffffff0);
    r15_418 = rax416;
    fun_2ab0(rdi417, rsi414, rdx84, rcx83, r8, rdi417, rsi414, rdx84, rcx83, r8);
    rdi419 = rdi417;
    addr_370b_277:
    fun_27a0(rdi419, r12_82, rdx84, rcx83, r8, rdi419, r12_82, rdx84, rcx83, r8);
    *reinterpret_cast<void***>(r15_418 + reinterpret_cast<unsigned char>(v86) * 8) = reinterpret_cast<void**>(1);
    *reinterpret_cast<void***>(r13_81 + 8) = r15_418;
    *reinterpret_cast<void***>(r13_81) = v415;
    *reinterpret_cast<void***>(r13_81 + 16) = v411;
    return v411;
    addr_361b_57:
    rbp420 = r14_85;
    rbx421 = r14_85;
    rdx84 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r14_85) << 4);
    r15_422 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx83) + reinterpret_cast<unsigned char>(rdx84));
    do {
        eax423 = fun_2a00(r15_422, r12_82, rdx84, rcx83, r8, r15_422, r12_82, rdx84, rcx83, r8);
        zf424 = reinterpret_cast<uint1_t>(eax423 == 0);
        if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(eax423 < 0) | zf424)) 
            break;
        rbx421 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbx421) - 1);
        r15_422 = r15_422 - 16;
    } while (rbx421 != 0xffffffffffffffff);
    goto addr_3670_280;
    if (zf424) {
        *reinterpret_cast<void***>(v88 + reinterpret_cast<uint64_t>(rbx421) * 8) = *reinterpret_cast<void***>(v88 + reinterpret_cast<uint64_t>(rbx421) * 8) + 1;
        return v88;
    }
    r15_425 = v86 + 1;
    v411 = r15_425;
    rdx426 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_425) << 4);
    rax427 = xrealloc(v87, rdx426, v87, rdx426);
    rsi428 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_425) * 8);
    v415 = rax427;
    rax429 = xrealloc(v88, rsi428, v88, rsi428);
    rdx84 = rdx426;
    r15_418 = rax429;
    rax430 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v415) + reinterpret_cast<unsigned char>(rdx84) + 0xfffffffffffffff0);
    fun_2ab0(rax430, rsi428, rdx84, rcx83, r8, rax430, rsi428, rdx84, rcx83, r8);
    rax431 = reinterpret_cast<void**>(&rbx421->f1);
    v86 = rax431;
    v432 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax431) << 4);
    rax433 = rax430;
    if (reinterpret_cast<int64_t>(rbx421) < reinterpret_cast<int64_t>(r14_85)) {
        addr_36da_284:
        r14_434 = rax433;
    } else {
        goto addr_3701_286;
    }
    do {
        rdi435 = r14_434;
        r14_434 = r14_434 - 16;
        fun_27a0(rdi435, r14_434, rdx84, rcx83, r8, rdi435, r14_434, rdx84, rcx83, r8);
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r15_418 + reinterpret_cast<uint64_t>(rbp420) * 8) + 8) = *reinterpret_cast<void***>(r15_418 + reinterpret_cast<uint64_t>(rbp420) * 8);
        rbp420 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbp420) - 1);
    } while (reinterpret_cast<int64_t>(rbx421) < reinterpret_cast<int64_t>(rbp420));
    addr_3701_286:
    rdi419 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(v432) + reinterpret_cast<unsigned char>(v415));
    goto addr_370b_277;
    addr_3670_280:
    r15_436 = v86 + 1;
    v411 = r15_436;
    r14_437 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_436) << 4);
    rax438 = xrealloc(v87, r14_437, v87, r14_437);
    rsi439 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_436) * 8);
    v415 = rax438;
    rax440 = xrealloc(v88, rsi439, v88, rsi439);
    r15_418 = rax440;
    rax441 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v415) + reinterpret_cast<unsigned char>(r14_437) + 0xfffffffffffffff0);
    fun_2ab0(rax441, rsi439, rdx84, rcx83, r8, rax441, rsi439, rdx84, rcx83, r8);
    rax433 = rax441;
    v432 = reinterpret_cast<void*>(0);
    v86 = reinterpret_cast<void**>(0);
    goto addr_36da_284;
    addr_40a2_50:
    return rax78;
    addr_5689_288:
    goto addr_5690_271;
    addr_4548_289:
    fun_25e0(v442, rsi107, v442, rsi107);
    fun_25e0(v443, rsi107, v443, rsi107);
    rsp105 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp108) - 8 + 8 - 8 + 8);
    goto addr_455c_61;
    addr_45a0_290:
    rdx444 = v445;
    addr_4524_291:
    if (rdx444) {
        *reinterpret_cast<int32_t*>(&rbx89) = 0;
        *reinterpret_cast<int32_t*>(&rbx89 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdi446) = 0;
        *reinterpret_cast<int32_t*>(&rdi446 + 4) = 0;
        do {
            fun_2940((reinterpret_cast<unsigned char>(rdi446) << 4) + reinterpret_cast<uint64_t>(v447), rsi107, rdx444, rcx103, r8_102, r9_101);
            rsp108 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp108) - 8 + 8);
            *reinterpret_cast<int32_t*>(&rdi446) = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbx89 + 1));
            *reinterpret_cast<int32_t*>(&rdi446 + 4) = 0;
            rbx89 = rdi446;
        } while (reinterpret_cast<unsigned char>(rdi446) < reinterpret_cast<unsigned char>(v448));
        goto addr_4548_289;
    }
    addr_5680_294:
    while (rax449 == rbx399) {
        while (1) {
            *reinterpret_cast<uint32_t*>(&rax450) = r13_394->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax450) + 4) = 0;
            r12_234 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_234) + reinterpret_cast<uint64_t>(rax450));
            rax451 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx399) * reinterpret_cast<unsigned char>(r12_234));
            rdx452 = __intrinsic();
            r15_386 = rax451;
            if (rdx452) {
                if (reinterpret_cast<unsigned char>(rbp378) <= reinterpret_cast<unsigned char>(rdx452)) 
                    goto addr_5719_274;
                rcx453 = rbp378;
                esi454 = 64;
                *reinterpret_cast<int32_t*>(&rax455) = 0;
                *reinterpret_cast<int32_t*>(&rax455 + 4) = 0;
                do {
                    rdi456 = reinterpret_cast<unsigned char>(rcx453) << 63;
                    rcx453 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx453) >> 1);
                    rax455 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax455) >> 1) | rdi456);
                    if (reinterpret_cast<unsigned char>(rcx453) < reinterpret_cast<unsigned char>(rdx452) || rcx453 == rdx452 && reinterpret_cast<unsigned char>(rax455) <= reinterpret_cast<unsigned char>(r15_386)) {
                        cf457 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_386) < reinterpret_cast<unsigned char>(rax455));
                        r15_386 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_386) - reinterpret_cast<unsigned char>(rax455));
                        rdx452 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx452) - (reinterpret_cast<unsigned char>(rcx453) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx452) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx453) + static_cast<uint64_t>(cf457))))));
                    }
                    --esi454;
                } while (esi454);
            } else {
                r15_386 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax451) % reinterpret_cast<unsigned char>(rbp378));
            }
            *reinterpret_cast<uint32_t*>(&r8_228) = v384;
            *reinterpret_cast<int32_t*>(&r8_228 + 4) = 0;
            r9_312 = rbx399;
            al458 = millerrabin(rbp378, r14_377, r15_386, v402, *reinterpret_cast<uint32_t*>(&r8_228), r9_312);
            if (!al458) 
                goto addr_56fc_270;
            r13_394 = reinterpret_cast<struct s0*>(&r13_394->f1);
            if (reinterpret_cast<int1_t>(0x105dc == r13_394)) 
                break;
            addr_55b0_269:
            if (!v405) 
                goto addr_5690_271;
            r11_459 = v401;
            do {
                r8_228 = rbx399;
                rax449 = powm(r15_386, reinterpret_cast<uint64_t>(v383) / v460, rbp378, r14_377, r8_228, r9_312);
                if (v407 == r11_459) 
                    goto addr_5680_294;
                r11_459 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r11_459) + 8);
            } while (rax449 != rbx399);
        }
        fun_2700();
        fun_2a20();
        rax449 = fun_25f0();
    }
    goto addr_5689_288;
    addr_4520_310:
    while (!*reinterpret_cast<int32_t*>(&rax461)) {
        while (1) {
            *reinterpret_cast<uint32_t*>(&rdx462) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_99));
            *reinterpret_cast<int32_t*>(&rdx462 + 4) = 0;
            fun_28c0(rbp95, rbp95, rdx462, rcx103, r8_102, r9_101);
            r9_101 = v100;
            rcx103 = r14_96;
            r8_102 = v97;
            rsi107 = r12_93;
            al463 = mp_millerrabin(rbx89, rsi107, rbp95, rcx103, r8_102, r9_101);
            rsp108 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp108) - 8 + 8 - 8 + 8);
            if (!al463) 
                goto addr_45a0_290;
            ++r13_99;
            if (reinterpret_cast<int1_t>(r13_99 == 0x105dc)) 
                break;
            addr_4458_60:
            if (!v464) 
                goto addr_4548_289;
            *reinterpret_cast<int32_t*>(&r15_94) = 0;
            *reinterpret_cast<int32_t*>(&r15_94 + 4) = 0;
            do {
                rdx465 = r15_94;
                ++r15_94;
                fun_2760(r14_96, r12_93, (reinterpret_cast<unsigned char>(rdx465) << 4) + reinterpret_cast<uint64_t>(v466), rcx103, r8_102, r9_101);
                rcx103 = rbx89;
                fun_2730(r14_96, rbp95, r14_96, rcx103, r8_102, r9_101);
                *reinterpret_cast<int32_t*>(&rsi107) = 1;
                *reinterpret_cast<int32_t*>(&rsi107 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rax461) = fun_2aa0(r14_96, 1, r14_96, rcx103, r8_102, r9_101);
                rsp108 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp108) - 8 + 8 - 8 + 8 - 8 + 8);
                rdx444 = v467;
                if (reinterpret_cast<unsigned char>(r15_94) >= reinterpret_cast<unsigned char>(rdx444)) 
                    goto addr_4520_310;
            } while (*reinterpret_cast<int32_t*>(&rax461));
        }
        rax468 = fun_2700();
        *reinterpret_cast<int32_t*>(&rsi107) = 0;
        *reinterpret_cast<int32_t*>(&rsi107 + 4) = 0;
        rdx444 = rax468;
        fun_2a20();
        rax461 = fun_25f0();
        rsp108 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp108) - 8 + 8 - 8 + 8 - 8 + 8);
    }
    goto addr_4524_291;
}

void fun_2ad0(void** rdi, void** rsi, void** rdx, void** rcx);

unsigned char mp_millerrabin(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r14_7;
    void** r13_8;
    void** r12_9;
    void** rbx10;
    int32_t eax11;
    int32_t eax12;
    void** rbp13;
    int32_t eax14;
    int32_t eax15;

    r14_7 = r9;
    r13_8 = rsi;
    r12_9 = rdi;
    rbx10 = rcx;
    fun_2730(rbx10, rdx, r8, rdi, r8, r9);
    eax11 = fun_2aa0(rbx10, 1, r8, rdi, r8, r9);
    if (!eax11 || (eax12 = fun_2a00(rbx10, r13_8, r8, rdi, r8), eax12 == 0)) {
        addr_3bc4_2:
        return 1;
    } else {
        *reinterpret_cast<int32_t*>(&rbp13) = 1;
        *reinterpret_cast<int32_t*>(&rbp13 + 4) = 0;
        if (reinterpret_cast<unsigned char>(r14_7) > reinterpret_cast<unsigned char>(1)) {
            do {
                fun_2ad0(rbx10, rbx10, 2, r12_9);
                eax14 = fun_2a00(rbx10, r13_8, 2, r12_9, r8);
                if (!eax14) 
                    goto addr_3bc4_2;
                eax15 = fun_2aa0(rbx10, 1, 2, r12_9, r8, r9);
            } while (eax15 && (++rbp13, r14_7 != rbp13));
        }
    }
    return 0;
}

/* gcd2_odd.part.0 */
void** gcd2_odd_part_0(void*** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void*** r9_7;
    void** rdi8;
    void** rsi9;
    void** rsi10;
    uint64_t rdx11;
    void** rax12;
    unsigned char dl13;
    uint1_t cf14;
    void** rsi15;
    uint64_t rdx16;
    uint1_t cf17;
    void** r8_18;
    uint64_t rax19;

    r9_7 = rdi;
    rdi8 = rsi;
    rsi9 = rdx;
    if (!(*reinterpret_cast<unsigned char*>(&rdx) & 1)) {
        do {
            rsi10 = rdi8;
            rdi8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi8) >> 1);
            rdx11 = reinterpret_cast<unsigned char>(rsi9) >> 1;
            rsi9 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rsi10) << 63) | rdx11);
        } while (!(*reinterpret_cast<uint32_t*>(&rdx11) & 1));
    }
    if (!(reinterpret_cast<unsigned char>(rcx) | reinterpret_cast<unsigned char>(rdi8))) {
        addr_3a3f_5:
        rax12 = gcd_odd(r8, rsi9);
        *r9_7 = reinterpret_cast<void**>(0);
        return rax12;
    } else {
        while (1) {
            if (reinterpret_cast<unsigned char>(rdi8) > reinterpret_cast<unsigned char>(rcx) || (dl13 = reinterpret_cast<uint1_t>(rdi8 == rcx), reinterpret_cast<unsigned char>(rsi9) > reinterpret_cast<unsigned char>(r8)) && dl13) {
                cf14 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rsi9) < reinterpret_cast<unsigned char>(r8));
                rsi9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi9) - reinterpret_cast<unsigned char>(r8));
                rdi8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi8) - (reinterpret_cast<unsigned char>(rcx) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi8) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx) + static_cast<uint64_t>(cf14))))));
                do {
                    rsi15 = rdi8;
                    rdi8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi8) >> 1);
                    rdx16 = reinterpret_cast<unsigned char>(rsi9) >> 1;
                    rsi9 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rsi15) << 63) | rdx16);
                } while (!(*reinterpret_cast<uint32_t*>(&rdx16) & 1));
                if (!(reinterpret_cast<unsigned char>(rdi8) | reinterpret_cast<unsigned char>(rcx))) 
                    break;
            } else {
                if (reinterpret_cast<unsigned char>(rdi8) < reinterpret_cast<unsigned char>(rcx)) 
                    goto addr_3a0f_11;
                if (reinterpret_cast<unsigned char>(rsi9) >= reinterpret_cast<unsigned char>(r8)) 
                    goto addr_3a86_13;
                if (!dl13) 
                    goto addr_3a86_13;
                addr_3a0f_11:
                cf17 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8) < reinterpret_cast<unsigned char>(rsi9));
                r8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rsi9));
                rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx) - (reinterpret_cast<unsigned char>(rdi8) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rdi8) + static_cast<uint64_t>(cf17))))));
                do {
                    r8_18 = rcx;
                    rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx) >> 1);
                    rax19 = reinterpret_cast<unsigned char>(r8) >> 1;
                    r8 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_18) << 63) | rax19);
                } while (!(*reinterpret_cast<unsigned char*>(&rax19) & 1));
                if (!(reinterpret_cast<unsigned char>(rdi8) | reinterpret_cast<unsigned char>(rcx))) 
                    goto addr_3a3f_5;
            }
        }
    }
    goto addr_3a3f_5;
    addr_3a86_13:
    *r9_7 = rdi8;
    return rsi9;
}

struct s43 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

unsigned char millerrabin2(void** rdi, void** rsi, struct s43* rdx, void** rcx, uint32_t r8d, void** r9);

unsigned char g5ec = 0;

struct s44 {
    signed char[1490] pad1490;
    signed char f5d2;
};

struct s45 {
    signed char[1490] pad1490;
    signed char f5d2;
};

void** powm2(void** rdi, struct s43* rsi, void** rdx, void** rcx, void** r8, void** r9);

unsigned char prime2_p(void** rdi, void** rsi) {
    void** r15_3;
    void*** rsp4;
    void** rax5;
    void** v6;
    void** rbx7;
    uint64_t rax8;
    uint64_t v9;
    uint32_t v10;
    int64_t rcx11;
    uint64_t rcx12;
    uint64_t rax13;
    int64_t rax14;
    void* rax15;
    void* rdx16;
    void* rax17;
    void** rdx18;
    void** rbp19;
    uint1_t zf20;
    void** rax21;
    void** rsi22;
    uint1_t cf23;
    int1_t cf24;
    void** v25;
    void** v26;
    void** tmp64_27;
    void** rax28;
    void** r13_29;
    struct s43* r12_30;
    void** r14_31;
    void** rcx32;
    void** r9_33;
    void** v34;
    unsigned char al35;
    unsigned char v36;
    void* v37;
    int64_t v38;
    int64_t v39;
    void** v40;
    void** v41;
    uint32_t eax42;
    void* rax43;
    void** rdi44;
    struct s0* r14_45;
    void** rbp46;
    struct s0** rsp47;
    void** rax48;
    void** v49;
    uint32_t eax50;
    struct s0* v51;
    void* rdx52;
    void** r13_53;
    void* rsp54;
    void** rax55;
    void** v56;
    uint32_t eax57;
    uint64_t r12_58;
    void* rax59;
    void** r13_60;
    void** rdx61;
    int32_t eax62;
    void** rsi63;
    void** r14_64;
    void** rbp65;
    int64_t rax66;
    void*** rdx67;
    void*** rbp68;
    uint64_t rdi69;
    void** rdx70;
    uint64_t rax71;
    uint32_t eax72;
    void** tmp64_73;
    void** rbp74;
    void* rax75;
    void** tmp64_76;
    int1_t zf77;
    int1_t zf78;
    void* rsp79;
    void** r12_80;
    void** rdx81;
    signed char v82;
    void** rbp83;
    uint32_t ebx84;
    signed char v85;
    unsigned char v86;
    void** v87;
    int1_t zf88;
    uint32_t eax89;
    unsigned char v90;
    void** rdi91;
    unsigned char v92;
    int64_t v93;
    void** v94;
    void** v95;
    void* rdx96;
    void** r14_97;
    void** rdi98;
    void** rdx99;
    void** rsp100;
    void** rdi101;
    void** rax102;
    void** rsi103;
    void** r13_104;
    void** rax105;
    int64_t v106;
    void** v107;
    void** v108;
    void** rdi109;
    void** rax110;
    void** r15_111;
    uint64_t rbp112;
    void* rbx113;
    int64_t* v114;
    void** rdi115;
    void** rax116;
    void** rdi117;
    void** v118;
    int1_t zf119;
    void** rdx120;
    void*** v121;
    void** rax122;
    void** v123;
    void** rdi124;
    uint64_t rdi125;
    void** v126;
    uint64_t v127;
    uint32_t v128;
    void** rcx129;
    void** r15_130;
    uint64_t rax131;
    int32_t esi132;
    int64_t rax133;
    void* rax134;
    void* rdx135;
    void* rax136;
    void** rdx137;
    struct s0* r13_138;
    void** rax139;
    uint64_t rdi140;
    uint1_t cf141;
    int64_t rbx142;
    void** r8_143;
    void** r9_144;
    void** rbx145;
    unsigned char al146;
    unsigned char v147;
    void* v148;
    struct s0* v149;
    uint32_t eax150;
    unsigned char v151;
    uint32_t v152;
    int64_t rax153;
    void* v154;
    uint64_t r12_155;
    void** rax156;
    void* rax157;
    uint32_t eax158;
    void** v159;
    void** r12_160;
    void** rcx161;
    int32_t esi162;
    void** rax163;
    void* rdx164;
    void* rdi165;
    uint1_t cf166;
    int64_t rbp167;
    int64_t r15_168;
    int64_t r14_169;
    void** rbp170;
    void** rcx171;
    void** r11_172;
    uint64_t rax173;
    int64_t rax174;
    void* rax175;
    void* rdx176;
    void* rax177;
    void** r8_178;
    void* r10_179;
    void* r9_180;
    void* rax181;
    void** r13_182;
    int64_t rax183;
    int64_t rax184;
    void** rdi185;
    void** rax186;
    int64_t rcx187;
    void* rdi188;
    void* rax189;
    void* r11_190;
    void* rsi191;
    void* rax192;
    void** rbx193;
    void** rsi194;
    void** rax195;
    void** rcx196;
    void** r11_197;
    void** rdx198;
    void** rax199;
    void** r8_200;
    signed char al201;
    void** rdx202;
    void** rsi203;
    void** rcx204;
    int1_t zf205;
    signed char al206;
    int64_t rax207;
    void** r8_208;
    void*** r11_209;
    signed char* r10_210;
    uint32_t r9d211;
    int32_t ebx212;
    struct s44* rcx213;
    struct s44* rax214;
    int64_t rsi215;
    struct s45* rax216;
    uint32_t r9d217;
    uint32_t eax218;
    void** rax219;
    uint64_t rax220;
    void** rax221;
    void** rdx222;
    void** rcx223;
    int32_t esi224;
    void** rax225;
    uint64_t rdi226;
    uint1_t cf227;
    unsigned char al228;
    void* r11_229;
    uint64_t v230;
    void** rax231;
    void** v232;
    uint32_t v233;
    void** v234;
    void** v235;
    void** v236;
    unsigned char al237;
    void** v238;
    void** rdx239;
    void* rax240;
    void** rax241;
    uint64_t rcx242;
    void** rsi243;
    uint1_t cf244;
    int1_t cf245;
    void** v246;
    void** rax247;
    signed char v248;
    void** v249;
    signed char v250;
    unsigned char v251;
    signed char v252;

    r15_3 = rsi;
    rsp4 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1f8);
    rax5 = g28;
    v6 = rax5;
    if (rdi) {
        rbx7 = rdi;
        rax8 = reinterpret_cast<unsigned char>(rdi) - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi) < reinterpret_cast<unsigned char>(static_cast<int64_t>(reinterpret_cast<unsigned char>(static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rsi) < reinterpret_cast<unsigned char>(1))))))));
        v9 = rax8;
        if (!(rsi - 1)) {
            __asm__("bsf rcx, rax");
            v10 = static_cast<uint32_t>(rcx11 + 64);
        } else {
            __asm__("bsf rax, rdx");
            v10 = *reinterpret_cast<uint32_t*>(&rax8);
        }
        *reinterpret_cast<int32_t*>(&rcx12) = 63;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx12) + 4) = 0;
        rax13 = reinterpret_cast<unsigned char>(r15_3) >> 1;
        *reinterpret_cast<uint32_t*>(&rax14) = *reinterpret_cast<uint32_t*>(&rax13) & 0x7f;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax15) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax14);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
        rdx16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax15) + reinterpret_cast<int64_t>(rax15) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax15) * reinterpret_cast<int64_t>(rax15) * reinterpret_cast<unsigned char>(r15_3)));
        rax17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx16) + reinterpret_cast<uint64_t>(rdx16) - reinterpret_cast<uint64_t>(rdx16) * reinterpret_cast<uint64_t>(rdx16) * reinterpret_cast<unsigned char>(r15_3));
        *reinterpret_cast<int32_t*>(&rdx18) = 0;
        *reinterpret_cast<int32_t*>(&rdx18 + 4) = 0;
        rbp19 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax17) + reinterpret_cast<uint64_t>(rax17) - reinterpret_cast<uint64_t>(rax17) * reinterpret_cast<uint64_t>(rax17) * reinterpret_cast<unsigned char>(r15_3));
        zf20 = reinterpret_cast<uint1_t>(rbx7 == 1);
        if (zf20) {
            rcx12 = 0x7f;
        }
        *reinterpret_cast<unsigned char*>(&rdx18) = zf20;
        *reinterpret_cast<uint32_t*>(&rax21) = reinterpret_cast<uint1_t>(!zf20);
        *reinterpret_cast<int32_t*>(&rax21 + 4) = 0;
        do {
            rsi22 = rdx18;
            rdx18 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx18) + reinterpret_cast<unsigned char>(rdx18));
            rax21 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax21) + reinterpret_cast<unsigned char>(rax21)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rsi22) >> 63));
            if (reinterpret_cast<unsigned char>(rbx7) < reinterpret_cast<unsigned char>(rax21) || rbx7 == rax21 && reinterpret_cast<unsigned char>(r15_3) <= reinterpret_cast<unsigned char>(rdx18)) {
                cf23 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx18) < reinterpret_cast<unsigned char>(r15_3));
                rdx18 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx18) - reinterpret_cast<unsigned char>(r15_3));
                rax21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax21) - (reinterpret_cast<unsigned char>(rbx7) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax21) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbx7) + static_cast<uint64_t>(cf23))))));
            }
            cf24 = rcx12 < 1;
            --rcx12;
        } while (!cf24);
        v25 = rax21;
        v26 = rdx18;
        tmp64_27 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx18) + reinterpret_cast<unsigned char>(rdx18));
        rax28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax21) + reinterpret_cast<unsigned char>(rax21) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_27) < reinterpret_cast<unsigned char>(rdx18))));
        if (reinterpret_cast<unsigned char>(rax28) > reinterpret_cast<unsigned char>(rbx7) || rax28 == rbx7 && reinterpret_cast<unsigned char>(tmp64_27) >= reinterpret_cast<unsigned char>(r15_3)) {
        }
        r13_29 = reinterpret_cast<void**>(rsp4 + 0xa0);
        r12_30 = reinterpret_cast<struct s43*>(rsp4 + 0x90);
        r14_31 = reinterpret_cast<void**>(rsp4 + 0xb0);
        rcx32 = reinterpret_cast<void**>(rsp4 + 0x80);
        r9_33 = r13_29;
        rdi = r14_31;
        v34 = rcx32;
        al35 = millerrabin2(rdi, rbp19, r12_30, rcx32, v10, r9_33);
        rsp4 = rsp4 - 8 + 8;
        v36 = al35;
        if (al35) {
            v37 = reinterpret_cast<void*>(rsp4 + 0xe0);
            factor();
            rsp4 = rsp4 - 8 + 8;
            r9_33 = r13_29;
            r13_29 = rbp19;
            rbp19 = r15_3;
            *reinterpret_cast<int32_t*>(&rdi) = 2;
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            r15_3 = reinterpret_cast<void**>(2);
            v38 = v39;
            v40 = reinterpret_cast<void**>(0x10340);
            v41 = reinterpret_cast<void**>(rsp4 + 0xc0);
            goto addr_5d90_15;
        }
    }
    if (reinterpret_cast<unsigned char>(rsi) > reinterpret_cast<unsigned char>(1)) {
        if (reinterpret_cast<unsigned char>(rsi) <= reinterpret_cast<unsigned char>(0x17ded78)) {
            eax42 = 1;
        } else {
            rax43 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v6) - reinterpret_cast<unsigned char>(g28));
            if (rax43) {
                fun_2740();
                goto addr_61d0_21;
            } else {
                rdi44 = rsi;
                r14_45 = reinterpret_cast<struct s0*>(rdi44 + 0xffffffffffffffff);
                rbp46 = rdi44;
                rsp47 = reinterpret_cast<struct s0**>(rsp4 + 0x1f8 + 8 + 8 + 8 + 8 + 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x148);
                rax48 = g28;
                v49 = rax48;
                eax50 = 0;
                v51 = r14_45;
                if (!(*reinterpret_cast<uint32_t*>(&rdi44) & 1)) 
                    goto addr_5703_24; else 
                    goto addr_5478_25;
            }
        }
    } else {
        eax42 = 0;
    }
    addr_6015_27:
    rdx52 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v6) - reinterpret_cast<unsigned char>(g28));
    if (!rdx52) {
        return *reinterpret_cast<unsigned char*>(&eax42);
    }
    addr_61d0_21:
    r13_53 = rdi;
    rsp54 = reinterpret_cast<void*>(rsp4 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x138);
    rax55 = g28;
    v56 = rax55;
    eax57 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi));
    if (*reinterpret_cast<signed char*>(&eax57) == 32) {
        do {
            eax57 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_53 + 1));
            ++r13_53;
        } while (*reinterpret_cast<signed char*>(&eax57) == 32);
    }
    *reinterpret_cast<int32_t*>(&r12_58) = 4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_58) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax59) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax57) == 43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax59) + 4) = 0;
    r13_60 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_53) + reinterpret_cast<uint64_t>(rax59));
    rdx61 = r13_60;
    do {
        eax62 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdx61));
        ++rdx61;
        if (!eax62) 
            break;
        *reinterpret_cast<int32_t*>(&r12_58) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_58) + 4) = 0;
    } while (eax62 - 48 <= 9);
    goto addr_623e_34;
    rsi63 = r13_60;
    *reinterpret_cast<int32_t*>(&r14_64) = 0;
    *reinterpret_cast<int32_t*>(&r14_64 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rbp65) = 0;
    *reinterpret_cast<int32_t*>(&rbp65 + 4) = 0;
    if (*reinterpret_cast<int32_t*>(&r12_58)) {
        addr_623e_34:
        quote();
        fun_2700();
        fun_2a20();
    } else {
        do {
            *reinterpret_cast<int32_t*>(&rax66) = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rsi63));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax66) + 4) = 0;
            ++rsi63;
            if (!*reinterpret_cast<int32_t*>(&rax66)) 
                break;
            *reinterpret_cast<uint32_t*>(&rcx32) = static_cast<uint32_t>(rax66 - 48);
            *reinterpret_cast<int32_t*>(&rcx32 + 4) = 0;
        } while (reinterpret_cast<unsigned char>(rbp65) <= reinterpret_cast<unsigned char>(0x1999999999999999) && (rdx67 = reinterpret_cast<void***>(r14_64 + reinterpret_cast<unsigned char>(r14_64) * 4), rbp68 = reinterpret_cast<void***>(rbp65 + reinterpret_cast<unsigned char>(rbp65) * 4), rdi69 = reinterpret_cast<unsigned char>(r14_64) >> 61, rdx70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdx67) + reinterpret_cast<uint64_t>(rdx67)), rax71 = reinterpret_cast<unsigned char>(r14_64) >> 63, eax72 = *reinterpret_cast<int32_t*>(&rax71) + *reinterpret_cast<int32_t*>(&rdi69) + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx70) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r14_64) + reinterpret_cast<unsigned char>(r14_64))), tmp64_73 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx70) + reinterpret_cast<unsigned char>(rcx32)), rdx61 = tmp64_73, rbp74 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbp68) + reinterpret_cast<uint64_t>(rbp68)), r14_64 = rdx61, *reinterpret_cast<uint32_t*>(&rcx32) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_73) < reinterpret_cast<unsigned char>(rdx70)), *reinterpret_cast<int32_t*>(&rcx32 + 4) = 0, *reinterpret_cast<uint32_t*>(&rax75) = eax72 + *reinterpret_cast<uint32_t*>(&rcx32), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax75) + 4) = 0, tmp64_76 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp74) + reinterpret_cast<uint64_t>(rax75)), rbp65 = tmp64_76, reinterpret_cast<unsigned char>(tmp64_76) >= reinterpret_cast<unsigned char>(rbp74)));
        goto addr_62ff_39;
        if (reinterpret_cast<signed char>(rbp65) < reinterpret_cast<signed char>(0)) {
            addr_62ff_39:
            zf77 = dev_debug == 0;
            if (!zf77) {
                rcx32 = stderr;
                fun_2a80("[using arbitrary-precision arithmetic] ", 1, 39, rcx32, 0x1999999999999999, r9_33);
                rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
                goto addr_630c_42;
            }
        } else {
            zf78 = dev_debug == 0;
            if (!zf78) {
                rcx32 = stderr;
                *reinterpret_cast<int32_t*>(&rdx61) = 36;
                *reinterpret_cast<int32_t*>(&rdx61 + 4) = 0;
                fun_2a80("[using single-precision arithmetic] ", 1, 36, rcx32, 0x1999999999999999, r9_33);
                rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
            }
            if (!rbp65) {
                lbuf_putint(r14_64, 0, rdx61, rcx32, r14_64, 0, rdx61, rcx32);
                rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
            } else {
                print_uintmaxes_part_0(rbp65, r14_64, rdx61, rcx32, rbp65, r14_64, rdx61, rcx32);
                rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
            }
            r12_80 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp79) + 32);
            lbuf_putc(58, 58);
            rdx81 = r12_80;
            factor();
            if (v82) {
                rbp83 = r12_80;
                while (1) {
                    ebx84 = 0;
                    if (!v85) {
                        addr_654c_51:
                        ++rbp83;
                        *reinterpret_cast<uint32_t*>(&rdx81) = v86;
                        *reinterpret_cast<int32_t*>(&rdx81 + 4) = 0;
                        if (*reinterpret_cast<uint32_t*>(&rdx81) > reinterpret_cast<uint32_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rbp83)) - reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&r12_80)))) 
                            continue; else 
                            break;
                    } else {
                        do {
                            lbuf_putc(32, 32);
                            lbuf_putint(v87, 0, rdx81, rcx32, v87, 0, rdx81, rcx32);
                            zf88 = print_exponents == 0;
                            eax89 = v90;
                            if (zf88) 
                                continue;
                            if (*reinterpret_cast<unsigned char*>(&eax89) > 1) 
                                break;
                            ++ebx84;
                        } while (ebx84 < eax89);
                        goto addr_654c_51;
                    }
                    lbuf_putc(94);
                    *reinterpret_cast<uint32_t*>(&rdi91) = v92;
                    *reinterpret_cast<int32_t*>(&rdi91 + 4) = 0;
                    lbuf_putint(rdi91, 0, rdx81, rcx32, rdi91, 0, rdx81, rcx32);
                    goto addr_654c_51;
                }
            }
            if (v93) {
                lbuf_putc(32, 32);
                if (!v94) {
                    lbuf_putint(v95, 0, rdx81, rcx32, v95, 0, rdx81, rcx32);
                } else {
                    print_uintmaxes_part_0(v94, v95, rdx81, rcx32, v94, v95, rdx81, rcx32);
                }
            }
            lbuf_putc(10, 10);
        }
    }
    addr_626c_62:
    rdx96 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v56) - reinterpret_cast<unsigned char>(g28));
    if (rdx96) {
        fun_2740();
    } else {
        goto v40;
    }
    addr_630c_42:
    r14_97 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp54) + 32);
    fun_2a50(r14_97, r13_60, 10, rcx32);
    rdi98 = stdout;
    rdx99 = r14_97;
    fun_2800(rdi98, 10, rdx99, rcx32);
    rsp100 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp54) - 8 + 8 - 8 + 8);
    rdi101 = stdout;
    rax102 = *reinterpret_cast<void***>(rdi101 + 40);
    if (reinterpret_cast<unsigned char>(rax102) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi101 + 48))) {
        fun_27c0();
        rsp100 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp100 - 8) + 8);
    } else {
        rdx99 = rax102 + 1;
        *reinterpret_cast<void***>(rdi101 + 40) = rdx99;
        *reinterpret_cast<void***>(rax102) = reinterpret_cast<void**>(58);
    }
    rsi103 = rsp100;
    *reinterpret_cast<int32_t*>(&r13_104) = 0;
    *reinterpret_cast<int32_t*>(&r13_104 + 4) = 0;
    mp_factor(r14_97, rsi103, rdx99, rcx32, 0x1999999999999999, r9_33);
    *reinterpret_cast<int32_t*>(&rax105) = 0;
    *reinterpret_cast<int32_t*>(&rax105 + 4) = 0;
    if (!v106) {
        addr_645c_69:
        fun_25e0(v107, rsi103, v107, rsi103);
        fun_25e0(v108, rsi103, v108, rsi103);
        fun_2940(r14_97, rsi103, rdx99, rcx32, 0x1999999999999999, r9_33);
        rdi109 = stdout;
        rax110 = *reinterpret_cast<void***>(rdi109 + 40);
        if (reinterpret_cast<unsigned char>(rax110) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi109 + 48))) {
            *reinterpret_cast<int32_t*>(&rsi103) = 10;
            *reinterpret_cast<int32_t*>(&rsi103 + 4) = 0;
            fun_27c0();
        } else {
            rdx99 = rax110 + 1;
            *reinterpret_cast<void***>(rdi109 + 40) = rdx99;
            *reinterpret_cast<void***>(rax110) = reinterpret_cast<void**>(10);
        }
    } else {
        do {
            *reinterpret_cast<int32_t*>(&r15_111) = 0;
            *reinterpret_cast<int32_t*>(&r15_111 + 4) = 0;
            rbp112 = reinterpret_cast<unsigned char>(rax105) * 8;
            rbx113 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax105) << 4);
            if (v114[reinterpret_cast<unsigned char>(rax105)]) {
                do {
                    rdi115 = stdout;
                    rax116 = *reinterpret_cast<void***>(rdi115 + 40);
                    if (reinterpret_cast<unsigned char>(rax116) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi115 + 48))) {
                        *reinterpret_cast<void***>(rdi115 + 40) = rax116 + 1;
                        *reinterpret_cast<void***>(rax116) = reinterpret_cast<void**>(32);
                    } else {
                        fun_27c0();
                    }
                    rdi117 = stdout;
                    *reinterpret_cast<int32_t*>(&rsi103) = 10;
                    *reinterpret_cast<int32_t*>(&rsi103 + 4) = 0;
                    fun_2800(rdi117, 10, reinterpret_cast<unsigned char>(v118) + reinterpret_cast<uint64_t>(rbx113), rcx32);
                    zf119 = print_exponents == 0;
                    rdx120 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(v121) + rbp112);
                    if (zf119) 
                        continue;
                    if (reinterpret_cast<unsigned char>(rdx120) > reinterpret_cast<unsigned char>(1)) 
                        break;
                    *reinterpret_cast<int32_t*>(&rax122) = static_cast<int32_t>(reinterpret_cast<uint64_t>(r15_111 + 1));
                    *reinterpret_cast<int32_t*>(&rax122 + 4) = 0;
                    r15_111 = rax122;
                } while (reinterpret_cast<unsigned char>(rax122) < reinterpret_cast<unsigned char>(rdx120));
                continue;
            } else {
                continue;
            }
            rsi103 = reinterpret_cast<void**>("^%lu");
            fun_29b0(1, "^%lu", rdx120, rcx32);
            rdx99 = v123;
            *reinterpret_cast<int32_t*>(&rax105) = static_cast<int32_t>(reinterpret_cast<uint64_t>(r13_104 + 1));
            *reinterpret_cast<int32_t*>(&rax105 + 4) = 0;
            r13_104 = rax105;
        } while (reinterpret_cast<unsigned char>(rax105) < reinterpret_cast<unsigned char>(rdx99));
        if (!rdx99) 
            goto addr_645c_69; else 
            goto addr_6435_84;
    }
    rdi124 = stdout;
    fun_2ac0(rdi124, rsi103, rdx99, rcx32);
    goto addr_626c_62;
    addr_6435_84:
    *reinterpret_cast<int32_t*>(&rdi125) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi125) + 4) = 0;
    do {
        fun_2940((rdi125 << 4) + reinterpret_cast<unsigned char>(v126), rsi103, rdx99, rcx32, 0x1999999999999999, r9_33);
        *reinterpret_cast<int32_t*>(&rdi125) = static_cast<int32_t>(r12_58 + 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi125) + 4) = 0;
        r12_58 = rdi125;
    } while (rdi125 < v127);
    goto addr_645c_69;
    addr_5703_24:
    v128 = 0;
    r14_45 = v51;
    addr_5490_87:
    rcx129 = rbp46;
    *reinterpret_cast<int32_t*>(&r15_130) = 0;
    *reinterpret_cast<int32_t*>(&r15_130 + 4) = 0;
    rax131 = reinterpret_cast<unsigned char>(rbp46) >> 1;
    esi132 = 64;
    *reinterpret_cast<uint32_t*>(&rax133) = *reinterpret_cast<uint32_t*>(&rax131) & 0x7f;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax133) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax134) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax133);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax134) + 4) = 0;
    rdx135 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax134) + reinterpret_cast<int64_t>(rax134) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax134) * reinterpret_cast<int64_t>(rax134) * reinterpret_cast<unsigned char>(rbp46)));
    rax136 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx135) + reinterpret_cast<uint64_t>(rdx135) - reinterpret_cast<uint64_t>(rdx135) * reinterpret_cast<uint64_t>(rdx135) * reinterpret_cast<unsigned char>(rbp46));
    *reinterpret_cast<int32_t*>(&rdx137) = 0;
    *reinterpret_cast<int32_t*>(&rdx137 + 4) = 0;
    r13_138 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax136) + reinterpret_cast<uint64_t>(rax136) - reinterpret_cast<uint64_t>(rax136) * reinterpret_cast<uint64_t>(rax136) * reinterpret_cast<unsigned char>(rbp46));
    *reinterpret_cast<int32_t*>(&rax139) = 1;
    *reinterpret_cast<int32_t*>(&rax139 + 4) = 0;
    do {
        rdi140 = reinterpret_cast<unsigned char>(rcx129) << 63;
        rcx129 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx129) >> 1);
        rdx137 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx137) >> 1) | rdi140);
        if (reinterpret_cast<unsigned char>(rcx129) < reinterpret_cast<unsigned char>(rax139) || rcx129 == rax139 && reinterpret_cast<unsigned char>(rdx137) <= reinterpret_cast<unsigned char>(r15_130)) {
            cf141 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_130) < reinterpret_cast<unsigned char>(rdx137));
            r15_130 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_130) - reinterpret_cast<unsigned char>(rdx137));
            rax139 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax139) - (reinterpret_cast<unsigned char>(rcx129) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax139) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx129) + static_cast<uint64_t>(cf141))))));
        }
        --esi132;
    } while (esi132);
    *reinterpret_cast<int32_t*>(&rbx142) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx142) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&r8_143) = v128;
    *reinterpret_cast<int32_t*>(&r8_143 + 4) = 0;
    r9_144 = r15_130;
    *reinterpret_cast<unsigned char*>(&rbx142) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp46) - reinterpret_cast<unsigned char>(r15_130)) > reinterpret_cast<unsigned char>(r15_130));
    rbx145 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbx142) & reinterpret_cast<unsigned char>(rbp46)) + (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r15_130) + reinterpret_cast<unsigned char>(r15_130)) - reinterpret_cast<unsigned char>(rbp46)));
    al146 = millerrabin(rbp46, r13_138, rbx145, r14_45, *reinterpret_cast<uint32_t*>(&r8_143), r9_144);
    v147 = al146;
    if (al146) {
        v148 = reinterpret_cast<void*>(rsp47 - 1 + 1 + 6);
        factor();
        v149 = r14_45;
        r14_45 = r13_138;
        eax150 = v151;
        r13_138 = reinterpret_cast<struct s0*>(0x10340);
        v152 = eax150;
        *reinterpret_cast<uint32_t*>(&rax153) = eax150 - 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax153) + 4) = 0;
        v154 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v148) + rax153 * 8);
        r12_155 = 2;
        rax156 = rbx145;
        rbx145 = r15_130;
        r15_130 = rax156;
        goto addr_55b0_93;
    }
    addr_56fc_94:
    v147 = 0;
    addr_5690_95:
    rax157 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v49) - reinterpret_cast<unsigned char>(g28));
    if (rax157) {
        fun_2740();
    } else {
        eax158 = v147;
        return *reinterpret_cast<unsigned char*>(&eax158);
    }
    addr_5719_98:
    fun_2810("(s1) < (n)", "src/factor.c", 0x4f2, "prime_p", r8_143, r9_144);
    v159 = reinterpret_cast<void**>("src/factor.c");
    if (0) {
        addr_5b07_100:
        fun_2810("(1) < (n)", "src/factor.c", 0x5be, "factor_using_pollard_rho", r8_143, r9_144);
    } else {
        r12_160 = reinterpret_cast<void**>("(s1) < (n)");
        do {
            rcx161 = r12_160;
            esi162 = 64;
            *reinterpret_cast<int32_t*>(&rax163) = 1;
            *reinterpret_cast<int32_t*>(&rax163 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx164) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx164) + 4) = 0;
            rdi165 = reinterpret_cast<void*>(0);
            do {
                r8_143 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx161) << 63);
                rcx161 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx161) >> 1);
                rdx164 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx164) >> 1 | reinterpret_cast<unsigned char>(r8_143));
                if (reinterpret_cast<unsigned char>(rcx161) < reinterpret_cast<unsigned char>(rax163) || rcx161 == rax163 && reinterpret_cast<uint64_t>(rdx164) <= reinterpret_cast<uint64_t>(rdi165)) {
                    cf166 = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi165) < reinterpret_cast<uint64_t>(rdx164));
                    rdi165 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi165) - reinterpret_cast<uint64_t>(rdx164));
                    rax163 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax163) - (reinterpret_cast<unsigned char>(rcx161) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax163) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx161) + static_cast<uint64_t>(cf166))))));
                }
                --esi162;
            } while (esi162);
            *reinterpret_cast<int32_t*>(&rbp167) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp167) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r15_168) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_168) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r14_169) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_169) + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rbp167) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_160) - reinterpret_cast<uint64_t>(rdi165) > reinterpret_cast<uint64_t>(rdi165));
            rbp170 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbp167) & reinterpret_cast<unsigned char>(r12_160)) + (reinterpret_cast<uint64_t>(rdi165) + reinterpret_cast<uint64_t>(rdi165) - reinterpret_cast<unsigned char>(r12_160)));
            rcx171 = rbp170;
            while (reinterpret_cast<unsigned char>(v159) < reinterpret_cast<unsigned char>(r12_160)) {
                r11_172 = rcx171;
                rax173 = reinterpret_cast<unsigned char>(r12_160) >> 1;
                *reinterpret_cast<uint32_t*>(&rax174) = *reinterpret_cast<uint32_t*>(&rax173) & 0x7f;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax174) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax175) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax174);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax175) + 4) = 0;
                rdx176 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax175) + reinterpret_cast<int64_t>(rax175) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax175) * reinterpret_cast<int64_t>(rax175) * reinterpret_cast<unsigned char>(r12_160)));
                rax177 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx176) + reinterpret_cast<uint64_t>(rdx176) - reinterpret_cast<uint64_t>(rdx176) * reinterpret_cast<uint64_t>(rdx176) * reinterpret_cast<unsigned char>(r12_160));
                r8_178 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax177) + reinterpret_cast<uint64_t>(rax177) - reinterpret_cast<uint64_t>(rax177) * reinterpret_cast<uint64_t>(rax177) * reinterpret_cast<unsigned char>(r12_160));
                r10_179 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_160) - reinterpret_cast<unsigned char>(v159));
                r9_180 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v159) - reinterpret_cast<unsigned char>(r12_160));
                while (1) {
                    rax181 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rax181 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax181) + reinterpret_cast<unsigned char>(r12_160));
                    }
                    rbp170 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rbp170) - (reinterpret_cast<unsigned char>(rbp170) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp170) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp170) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax181) < reinterpret_cast<uint64_t>(r10_179))))))) & reinterpret_cast<unsigned char>(r12_160)) + (reinterpret_cast<uint64_t>(rax181) + reinterpret_cast<uint64_t>(r9_180)));
                    r13_182 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        r13_182 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_182) + reinterpret_cast<unsigned char>(r12_160));
                    }
                    rax183 = r14_169;
                    *reinterpret_cast<uint32_t*>(&rax184) = *reinterpret_cast<uint32_t*>(&rax183) & 31;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax184) + 4) = 0;
                    if (rax184 == 1) {
                        rdi185 = r13_182;
                        rax186 = gcd_odd(rdi185, r12_160);
                        if (!reinterpret_cast<int1_t>(rax186 == 1)) 
                            break;
                    }
                    --r14_169;
                    if (r14_169) 
                        continue;
                    rcx187 = r15_168 + r15_168;
                    if (!r15_168) {
                        r15_168 = rcx187;
                        r11_172 = rbp170;
                    } else {
                        do {
                            rdi188 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                            rax189 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi188) + reinterpret_cast<unsigned char>(r12_160));
                            if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                                rdi188 = rax189;
                            }
                            ++r14_169;
                        } while (r15_168 != r14_169);
                        r11_172 = rbp170;
                        r15_168 = rcx187;
                        rbp170 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax189) - (reinterpret_cast<uint64_t>(rax189) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax189) < reinterpret_cast<uint64_t>(rax189) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi188) < reinterpret_cast<uint64_t>(r10_179)))) & reinterpret_cast<unsigned char>(r12_160)) + (reinterpret_cast<uint64_t>(rdi188) + reinterpret_cast<uint64_t>(r9_180)));
                    }
                }
                r9_144 = r8_178;
                r8_143 = r11_172;
                r11_190 = r9_180;
                do {
                    rsi191 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    rax192 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi191) + reinterpret_cast<unsigned char>(r12_160));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rsi191 = rax192;
                    }
                    rbx193 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax192) - (reinterpret_cast<uint64_t>(rax192) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax192) < reinterpret_cast<uint64_t>(rax192) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rsi191) < reinterpret_cast<uint64_t>(r10_179)))) & reinterpret_cast<unsigned char>(r12_160)) + (reinterpret_cast<uint64_t>(rsi191) + reinterpret_cast<uint64_t>(r11_190)));
                    rsi194 = r12_160;
                    rdi185 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rdi185) - (reinterpret_cast<unsigned char>(rdi185) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi185) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rdi185) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_143) < reinterpret_cast<unsigned char>(rbx193))))))) & reinterpret_cast<unsigned char>(r12_160)) + (reinterpret_cast<unsigned char>(r8_143) - reinterpret_cast<unsigned char>(rbx193)));
                    rax195 = gcd_odd(rdi185, rsi194);
                } while (rax195 == 1);
                rcx196 = r8_143;
                r11_197 = rax195;
                if (rax195 == r12_160) 
                    goto addr_5af7_129;
                rdx198 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_160) % reinterpret_cast<unsigned char>(r11_197));
                rax199 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_160) / reinterpret_cast<unsigned char>(r11_197));
                r8_200 = rax199;
                r12_160 = rax199;
                if (reinterpret_cast<unsigned char>(r11_197) > reinterpret_cast<unsigned char>(1) && (reinterpret_cast<unsigned char>(r11_197) <= reinterpret_cast<unsigned char>(0x17ded78) || (al201 = prime_p_part_0(r11_197, rsi194, rdx198, rcx196), r11_197 = r11_197, rcx196 = rcx196, r8_200 = rax199, !!al201))) {
                    *reinterpret_cast<int32_t*>(&rdx202) = 1;
                    *reinterpret_cast<int32_t*>(&rdx202 + 4) = 0;
                    rsi203 = r11_197;
                    factor_insert_multiplicity(0x4f2, rsi203, 1);
                    r8_143 = r8_200;
                    rcx204 = rcx196;
                    zf205 = reinterpret_cast<int1_t>(r8_143 == 1);
                    if (reinterpret_cast<unsigned char>(r8_143) <= reinterpret_cast<unsigned char>(1)) 
                        goto addr_5aca_132; else 
                        goto addr_5a2f_133;
                }
                rdx202 = reinterpret_cast<void**>(0x4f2);
                rsi203 = v159 + 1;
                factor_using_pollard_rho(r11_197, rsi203, 0x4f2, rcx196);
                r8_143 = r8_200;
                rcx204 = rcx196;
                zf205 = reinterpret_cast<int1_t>(r8_143 == 1);
                if (reinterpret_cast<unsigned char>(r8_143) > reinterpret_cast<unsigned char>(1)) {
                    addr_5a2f_133:
                    if (reinterpret_cast<unsigned char>(r8_143) <= reinterpret_cast<unsigned char>(0x17ded78)) 
                        goto addr_5ad7_135;
                    al206 = prime_p_part_0(r8_143, rsi203, rdx202, rcx204);
                    r8_143 = r8_143;
                    if (al206) 
                        goto addr_5ad7_135;
                } else {
                    addr_5aca_132:
                    if (zf205) 
                        goto addr_5b45_137; else 
                        goto addr_5acc_138;
                }
                rbp170 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp170) % reinterpret_cast<unsigned char>(r8_143));
                rcx171 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx204) % reinterpret_cast<unsigned char>(r8_143));
                continue;
                addr_5acc_138:
                *reinterpret_cast<int32_t*>(&rcx171) = 0;
                *reinterpret_cast<int32_t*>(&rcx171 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rbp170) = 0;
                *reinterpret_cast<int32_t*>(&rbp170 + 4) = 0;
            }
            break;
            addr_5af7_129:
            ++v159;
        } while (reinterpret_cast<unsigned char>(r12_160) > reinterpret_cast<unsigned char>(1));
        goto addr_5b07_100;
    }
    fun_2810("a < n", "src/factor.c", 0x5c4, "factor_using_pollard_rho", r8_143, r9_144);
    addr_5b45_137:
    goto v51;
    addr_5ad7_135:
    *reinterpret_cast<uint32_t*>(&rax207) = g5ec;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax207) + 4) = 0;
    r8_208 = r8_143;
    r11_209 = reinterpret_cast<void***>(0x502);
    r10_210 = reinterpret_cast<signed char*>(0x5d2);
    r9d211 = *reinterpret_cast<uint32_t*>(&rax207);
    if (!*reinterpret_cast<uint32_t*>(&rax207)) 
        goto addr_2fa1_142;
    ebx212 = static_cast<int32_t>(rax207 - 1);
    rcx213 = reinterpret_cast<struct s44*>(static_cast<int64_t>(ebx212));
    rax214 = rcx213;
    do {
        *reinterpret_cast<int32_t*>(&rsi215) = *reinterpret_cast<int32_t*>(&rax214);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi215) + 4) = 0;
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(0x4f2 + reinterpret_cast<int64_t>(rax214) * 8 + 16)) <= reinterpret_cast<unsigned char>(r8_208)) 
            break;
        rax214 = reinterpret_cast<struct s44*>(reinterpret_cast<int64_t>(rax214) - 1);
        *reinterpret_cast<int32_t*>(&rsi215) = *reinterpret_cast<int32_t*>(&rax214);
    } while (*reinterpret_cast<int32_t*>(&rax214) != -1);
    goto addr_2f80_146;
    if (*reinterpret_cast<void***>(0x4f2 + reinterpret_cast<int64_t>(rax214) * 8 + 16) == r8_208) {
        rax214->f5d2 = reinterpret_cast<signed char>(rax214->f5d2 + 1);
        goto v51;
    }
    rax216 = reinterpret_cast<struct s45*>(static_cast<int64_t>(static_cast<int32_t>(rsi215 + 1)));
    r11_209 = reinterpret_cast<void***>(0x502 + reinterpret_cast<int64_t>(rax216) * 8);
    r10_210 = &rax216->f5d2;
    if (*reinterpret_cast<int32_t*>(&rsi215) >= ebx212) {
        addr_2fa1_142:
        r9d217 = r9d211 + 1;
        *r11_209 = r8_208;
        *r10_210 = 1;
        g5ec = *reinterpret_cast<unsigned char*>(&r9d217);
        goto v51;
    }
    do {
        addr_2f80_146:
        *reinterpret_cast<int64_t*>(0x4f2 + reinterpret_cast<int64_t>(rcx213) * 8 + 24) = *reinterpret_cast<int64_t*>(0x4f2 + reinterpret_cast<int64_t>(rcx213) * 8 + 16);
        eax218 = *reinterpret_cast<unsigned char*>(0x4f2 + reinterpret_cast<int64_t>(rcx213) + 0xe0);
        *reinterpret_cast<signed char*>(0x4f2 + reinterpret_cast<int64_t>(rcx213) + 0xe1) = *reinterpret_cast<signed char*>(&eax218);
        rcx213 = reinterpret_cast<struct s44*>(reinterpret_cast<int64_t>(rcx213) - 1);
    } while (*reinterpret_cast<int32_t*>(&rsi215) < *reinterpret_cast<int32_t*>(&rcx213));
    goto addr_2fa1_142;
    addr_5478_25:
    do {
        r14_45 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(r14_45) >> 1);
        ++eax50;
    } while (!(*reinterpret_cast<unsigned char*>(&r14_45) & 1));
    v128 = eax50;
    goto addr_5490_87;
    addr_5689_153:
    goto addr_5690_95;
    addr_60f8_154:
    eax42 = v36;
    goto addr_6015_27;
    addr_6146_155:
    eax42 = v36;
    goto addr_6015_27;
    addr_60f4_156:
    goto addr_60f8_154;
    addr_5680_157:
    while (rax219 == rbx145) {
        while (1) {
            *reinterpret_cast<uint32_t*>(&rax220) = r13_138->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax220) + 4) = 0;
            r12_155 = r12_155 + rax220;
            rax221 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx145) * r12_155);
            rdx222 = __intrinsic();
            r15_130 = rax221;
            if (rdx222) {
                if (reinterpret_cast<unsigned char>(rbp46) <= reinterpret_cast<unsigned char>(rdx222)) 
                    goto addr_5719_98;
                rcx223 = rbp46;
                esi224 = 64;
                *reinterpret_cast<int32_t*>(&rax225) = 0;
                *reinterpret_cast<int32_t*>(&rax225 + 4) = 0;
                do {
                    rdi226 = reinterpret_cast<unsigned char>(rcx223) << 63;
                    rcx223 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx223) >> 1);
                    rax225 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax225) >> 1) | rdi226);
                    if (reinterpret_cast<unsigned char>(rcx223) < reinterpret_cast<unsigned char>(rdx222) || rcx223 == rdx222 && reinterpret_cast<unsigned char>(rax225) <= reinterpret_cast<unsigned char>(r15_130)) {
                        cf227 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_130) < reinterpret_cast<unsigned char>(rax225));
                        r15_130 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_130) - reinterpret_cast<unsigned char>(rax225));
                        rdx222 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx222) - (reinterpret_cast<unsigned char>(rcx223) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx222) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx223) + static_cast<uint64_t>(cf227))))));
                    }
                    --esi224;
                } while (esi224);
            } else {
                r15_130 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax221) % reinterpret_cast<unsigned char>(rbp46));
            }
            *reinterpret_cast<uint32_t*>(&r8_143) = v128;
            *reinterpret_cast<int32_t*>(&r8_143 + 4) = 0;
            r9_144 = rbx145;
            al228 = millerrabin(rbp46, r14_45, r15_130, v149, *reinterpret_cast<uint32_t*>(&r8_143), r9_144);
            if (!al228) 
                goto addr_56fc_94;
            r13_138 = reinterpret_cast<struct s0*>(&r13_138->f1);
            if (reinterpret_cast<int1_t>(0x105dc == r13_138)) 
                break;
            addr_55b0_93:
            if (!v152) 
                goto addr_5690_95;
            r11_229 = v148;
            do {
                r8_143 = rbx145;
                rax219 = powm(r15_130, reinterpret_cast<uint64_t>(v51) / v230, rbp46, r14_45, r8_143, r9_144);
                if (v154 == r11_229) 
                    goto addr_5680_157;
                r11_229 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r11_229) + 8);
            } while (rax219 != rbx145);
        }
        fun_2700();
        fun_2a20();
        rax219 = fun_25f0();
    }
    goto addr_5689_153;
    while (1) {
        addr_5fc7_173:
        fun_2700();
        fun_2a20();
        fun_25f0();
        rsp4 = rsp4 - 8 + 8 - 8 + 8 - 8 + 8;
        while (1) {
            while (1) {
                r9_33 = r15_3;
                rcx32 = r14_31;
                rdi = rbx7;
                rax231 = powm2(rdi, r12_30, v41, rcx32, r13_29, r9_33);
                rsp4 = rsp4 - 8 + 8;
                if (rax231 == v232) {
                    if (v233 <= *reinterpret_cast<uint32_t*>(&rbp19)) {
                        r9_33 = r15_3;
                        rbx7 = v234;
                        rbp19 = v235;
                        r15_3 = v236;
                        al237 = reinterpret_cast<uint1_t>(v238 != v25);
                        while (1) {
                            if (al237) 
                                goto addr_6146_155;
                            do {
                                addr_5f21_179:
                                *reinterpret_cast<int32_t*>(&rdx239) = 0;
                                *reinterpret_cast<int32_t*>(&rdx239 + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&rax240) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v40));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax240) + 4) = 0;
                                r15_3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_3) + reinterpret_cast<uint64_t>(rax240));
                                rax241 = reinterpret_cast<void**>(0);
                                if (reinterpret_cast<unsigned char>(rbx7) > reinterpret_cast<unsigned char>(r15_3)) {
                                    rax241 = r15_3;
                                }
                                rcx242 = (reinterpret_cast<unsigned char>(rcx32) - (reinterpret_cast<unsigned char>(rcx32) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx32) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx32) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_3) < reinterpret_cast<unsigned char>(rbx7))))))) & 0xffffffffffffffc0) + 0x7f;
                                if (reinterpret_cast<unsigned char>(rbx7) <= reinterpret_cast<unsigned char>(r15_3)) {
                                    rdx239 = r15_3;
                                }
                                do {
                                    rsi243 = rdx239;
                                    rdx239 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx239) + reinterpret_cast<unsigned char>(rdx239));
                                    rax241 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax241) + reinterpret_cast<unsigned char>(rax241)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rsi243) >> 63));
                                    if (reinterpret_cast<unsigned char>(rbx7) < reinterpret_cast<unsigned char>(rax241) || rbx7 == rax241 && reinterpret_cast<unsigned char>(rbp19) <= reinterpret_cast<unsigned char>(rdx239)) {
                                        cf244 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx239) < reinterpret_cast<unsigned char>(rbp19));
                                        rdx239 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx239) - reinterpret_cast<unsigned char>(rbp19));
                                        rax241 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax241) - (reinterpret_cast<unsigned char>(rbx7) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax241) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbx7) + static_cast<uint64_t>(cf244))))));
                                    }
                                    cf245 = rcx242 < 1;
                                    --rcx242;
                                } while (!cf245);
                                rcx32 = v34;
                                rdi = r14_31;
                                v232 = r9_33;
                                *reinterpret_cast<unsigned char*>(&eax42) = millerrabin2(rdi, r13_29, r12_30, rcx32, v10, r9_33);
                                rsp4 = rsp4 - 8 + 8;
                                if (!*reinterpret_cast<unsigned char*>(&eax42)) 
                                    goto addr_6015_27;
                                ++v40;
                                rdi = reinterpret_cast<void**>(0x105dc);
                                r9_33 = v232;
                                if (reinterpret_cast<int1_t>(0x105dc == v40)) 
                                    goto addr_5fc7_173;
                                addr_5d90_15:
                                if (!v38) 
                                    goto addr_5d9c_189;
                                rdi = reinterpret_cast<void**>(rsp4 + 0xd8);
                                v246 = rdi;
                                rcx32 = r14_31;
                                rax247 = powm2(rdi, r12_30, v41, rcx32, r13_29, r9_33);
                                rsp4 = rsp4 - 8 + 8;
                                r9_33 = r9_33;
                                if (rax247 != v26) 
                                    goto addr_60e6_191;
                                if (!v248) 
                                    break;
                            } while (v249 == v25);
                            goto addr_61a8_194;
                            al237 = reinterpret_cast<uint1_t>(v249 != v25);
                        }
                    } else {
                        if (v238 == v25) {
                            r9_33 = r15_3;
                            rbx7 = v234;
                            rbp19 = v235;
                            r15_3 = v236;
                            goto addr_5f21_179;
                        }
                    }
                } else {
                    if (v233 <= *reinterpret_cast<uint32_t*>(&rbp19)) 
                        goto addr_60f8_154; else 
                        goto addr_5ea2_199;
                }
                addr_5d9c_189:
                if (!v250) 
                    goto addr_60f8_154;
                v246 = reinterpret_cast<void**>(rsp4 + 0xd8);
                addr_5db7_201:
                v236 = r15_3;
                r15_3 = r9_33;
                v234 = rbx7;
                rbx7 = v246;
                v232 = v26;
                v235 = rbp19;
                rbp19 = reinterpret_cast<void**>(1);
                v233 = v251;
                addr_5ea6_202:
                if (*reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(v37) + reinterpret_cast<unsigned char>(rbp19) * 8 + 8) != 2) {
                    if (v9 < *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(v37) + reinterpret_cast<unsigned char>(rbp19) * 8 + 8)) 
                        break;
                    continue;
                } else {
                    continue;
                }
                addr_60e6_191:
                if (v252) 
                    goto addr_5db7_201; else 
                    goto addr_60f4_156;
                addr_61a8_194:
                goto addr_5db7_201;
                addr_5ea2_199:
                ++rbp19;
                goto addr_5ea6_202;
            }
        }
    }
}

struct s46 {
    signed char[1490] pad1490;
    signed char f5d2;
};

struct s47 {
    signed char[1490] pad1490;
    signed char f5d2;
};

/* prime_p.part.0 */
signed char prime_p_part_0(void** rdi, void** rsi, void** rdx, void** rcx) {
    struct s0* r14_5;
    void** rbp6;
    struct s0** rsp7;
    void** rax8;
    void** v9;
    uint32_t eax10;
    struct s0* v11;
    uint32_t v12;
    void** rcx13;
    void** r15_14;
    uint64_t rax15;
    int32_t esi16;
    int64_t rax17;
    void* rax18;
    void* rdx19;
    void* rax20;
    void** rdx21;
    struct s0* r13_22;
    void** rax23;
    uint64_t rdi24;
    uint1_t cf25;
    int64_t rbx26;
    void** r8_27;
    void** r9_28;
    void** rbx29;
    unsigned char al30;
    unsigned char v31;
    void* v32;
    struct s0* v33;
    uint32_t eax34;
    unsigned char v35;
    uint32_t v36;
    int64_t rax37;
    void* v38;
    uint64_t r12_39;
    void** rax40;
    void* rax41;
    uint32_t eax42;
    void** v43;
    void** r12_44;
    void** rcx45;
    int32_t esi46;
    void** rax47;
    void* rdx48;
    void* rdi49;
    uint1_t cf50;
    int64_t rbp51;
    int64_t r15_52;
    int64_t r14_53;
    void** rbp54;
    void** rcx55;
    void** r11_56;
    uint64_t rax57;
    int64_t rax58;
    void* rax59;
    void* rdx60;
    void* rax61;
    void** r8_62;
    void* r10_63;
    void* r9_64;
    void* rax65;
    void** r13_66;
    int64_t rax67;
    int64_t rax68;
    void** rdi69;
    void** rax70;
    int64_t rcx71;
    void* rdi72;
    void* rax73;
    void* r11_74;
    void* rsi75;
    void* rax76;
    void** rbx77;
    void** rsi78;
    void** rax79;
    void** rcx80;
    void** r11_81;
    void** rdx82;
    void** rax83;
    void** r8_84;
    signed char al85;
    void** rdx86;
    void** rsi87;
    void** rcx88;
    int1_t zf89;
    signed char al90;
    int64_t rax91;
    void** r8_92;
    void*** r11_93;
    signed char* r10_94;
    uint32_t r9d95;
    int32_t ebx96;
    struct s46* rcx97;
    struct s46* rax98;
    int64_t rsi99;
    struct s47* rax100;
    uint32_t r9d101;
    uint32_t eax102;
    void** rax103;
    uint64_t rax104;
    void** rax105;
    void** rdx106;
    void** rcx107;
    int32_t esi108;
    void** rax109;
    uint64_t rdi110;
    uint1_t cf111;
    unsigned char al112;
    void* r11_113;
    uint64_t v114;

    r14_5 = reinterpret_cast<struct s0*>(rdi + 0xffffffffffffffff);
    rbp6 = rdi;
    rsp7 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x148);
    rax8 = g28;
    v9 = rax8;
    eax10 = 0;
    v11 = r14_5;
    if (!(*reinterpret_cast<uint32_t*>(&rdi) & 1)) {
        v12 = 0;
        r14_5 = v11;
    } else {
        do {
            r14_5 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(r14_5) >> 1);
            ++eax10;
        } while (!(*reinterpret_cast<unsigned char*>(&r14_5) & 1));
        v12 = eax10;
    }
    rcx13 = rbp6;
    *reinterpret_cast<int32_t*>(&r15_14) = 0;
    *reinterpret_cast<int32_t*>(&r15_14 + 4) = 0;
    rax15 = reinterpret_cast<unsigned char>(rbp6) >> 1;
    esi16 = 64;
    *reinterpret_cast<uint32_t*>(&rax17) = *reinterpret_cast<uint32_t*>(&rax15) & 0x7f;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax18) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax17);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
    rdx19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax18) + reinterpret_cast<int64_t>(rax18) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax18) * reinterpret_cast<int64_t>(rax18) * reinterpret_cast<unsigned char>(rbp6)));
    rax20 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx19) + reinterpret_cast<uint64_t>(rdx19) - reinterpret_cast<uint64_t>(rdx19) * reinterpret_cast<uint64_t>(rdx19) * reinterpret_cast<unsigned char>(rbp6));
    *reinterpret_cast<int32_t*>(&rdx21) = 0;
    *reinterpret_cast<int32_t*>(&rdx21 + 4) = 0;
    r13_22 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax20) + reinterpret_cast<uint64_t>(rax20) - reinterpret_cast<uint64_t>(rax20) * reinterpret_cast<uint64_t>(rax20) * reinterpret_cast<unsigned char>(rbp6));
    *reinterpret_cast<int32_t*>(&rax23) = 1;
    *reinterpret_cast<int32_t*>(&rax23 + 4) = 0;
    do {
        rdi24 = reinterpret_cast<unsigned char>(rcx13) << 63;
        rcx13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx13) >> 1);
        rdx21 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx21) >> 1) | rdi24);
        if (reinterpret_cast<unsigned char>(rcx13) < reinterpret_cast<unsigned char>(rax23) || rcx13 == rax23 && reinterpret_cast<unsigned char>(rdx21) <= reinterpret_cast<unsigned char>(r15_14)) {
            cf25 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_14) < reinterpret_cast<unsigned char>(rdx21));
            r15_14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_14) - reinterpret_cast<unsigned char>(rdx21));
            rax23 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax23) - (reinterpret_cast<unsigned char>(rcx13) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax23) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx13) + static_cast<uint64_t>(cf25))))));
        }
        --esi16;
    } while (esi16);
    *reinterpret_cast<int32_t*>(&rbx26) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx26) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&r8_27) = v12;
    *reinterpret_cast<int32_t*>(&r8_27 + 4) = 0;
    r9_28 = r15_14;
    *reinterpret_cast<unsigned char*>(&rbx26) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp6) - reinterpret_cast<unsigned char>(r15_14)) > reinterpret_cast<unsigned char>(r15_14));
    rbx29 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbx26) & reinterpret_cast<unsigned char>(rbp6)) + (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r15_14) + reinterpret_cast<unsigned char>(r15_14)) - reinterpret_cast<unsigned char>(rbp6)));
    al30 = millerrabin(rbp6, r13_22, rbx29, r14_5, *reinterpret_cast<uint32_t*>(&r8_27), r9_28);
    v31 = al30;
    if (al30) {
        v32 = reinterpret_cast<void*>(rsp7 - 1 + 1 + 6);
        factor();
        v33 = r14_5;
        r14_5 = r13_22;
        eax34 = v35;
        r13_22 = reinterpret_cast<struct s0*>(0x10340);
        v36 = eax34;
        *reinterpret_cast<uint32_t*>(&rax37) = eax34 - 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax37) + 4) = 0;
        v38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v32) + rax37 * 8);
        r12_39 = 2;
        rax40 = rbx29;
        rbx29 = r15_14;
        r15_14 = rax40;
        goto addr_55b0_12;
    }
    addr_56fc_13:
    v31 = 0;
    addr_5690_14:
    rax41 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
    if (rax41) {
        fun_2740();
    } else {
        eax42 = v31;
        return *reinterpret_cast<signed char*>(&eax42);
    }
    addr_5719_17:
    fun_2810("(s1) < (n)", "src/factor.c", 0x4f2, "prime_p", r8_27, r9_28);
    v43 = reinterpret_cast<void**>("src/factor.c");
    if (0) {
        addr_5b07_19:
        fun_2810("(1) < (n)", "src/factor.c", 0x5be, "factor_using_pollard_rho", r8_27, r9_28);
    } else {
        r12_44 = reinterpret_cast<void**>("(s1) < (n)");
        do {
            rcx45 = r12_44;
            esi46 = 64;
            *reinterpret_cast<int32_t*>(&rax47) = 1;
            *reinterpret_cast<int32_t*>(&rax47 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx48) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx48) + 4) = 0;
            rdi49 = reinterpret_cast<void*>(0);
            do {
                r8_27 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx45) << 63);
                rcx45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx45) >> 1);
                rdx48 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx48) >> 1 | reinterpret_cast<unsigned char>(r8_27));
                if (reinterpret_cast<unsigned char>(rcx45) < reinterpret_cast<unsigned char>(rax47) || rcx45 == rax47 && reinterpret_cast<uint64_t>(rdx48) <= reinterpret_cast<uint64_t>(rdi49)) {
                    cf50 = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi49) < reinterpret_cast<uint64_t>(rdx48));
                    rdi49 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi49) - reinterpret_cast<uint64_t>(rdx48));
                    rax47 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax47) - (reinterpret_cast<unsigned char>(rcx45) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax47) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx45) + static_cast<uint64_t>(cf50))))));
                }
                --esi46;
            } while (esi46);
            *reinterpret_cast<int32_t*>(&rbp51) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp51) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r15_52) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_52) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r14_53) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_53) + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rbp51) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_44) - reinterpret_cast<uint64_t>(rdi49) > reinterpret_cast<uint64_t>(rdi49));
            rbp54 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbp51) & reinterpret_cast<unsigned char>(r12_44)) + (reinterpret_cast<uint64_t>(rdi49) + reinterpret_cast<uint64_t>(rdi49) - reinterpret_cast<unsigned char>(r12_44)));
            rcx55 = rbp54;
            while (reinterpret_cast<unsigned char>(v43) < reinterpret_cast<unsigned char>(r12_44)) {
                r11_56 = rcx55;
                rax57 = reinterpret_cast<unsigned char>(r12_44) >> 1;
                *reinterpret_cast<uint32_t*>(&rax58) = *reinterpret_cast<uint32_t*>(&rax57) & 0x7f;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax58) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax59) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax58);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax59) + 4) = 0;
                rdx60 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax59) + reinterpret_cast<int64_t>(rax59) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax59) * reinterpret_cast<int64_t>(rax59) * reinterpret_cast<unsigned char>(r12_44)));
                rax61 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx60) + reinterpret_cast<uint64_t>(rdx60) - reinterpret_cast<uint64_t>(rdx60) * reinterpret_cast<uint64_t>(rdx60) * reinterpret_cast<unsigned char>(r12_44));
                r8_62 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax61) + reinterpret_cast<uint64_t>(rax61) - reinterpret_cast<uint64_t>(rax61) * reinterpret_cast<uint64_t>(rax61) * reinterpret_cast<unsigned char>(r12_44));
                r10_63 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_44) - reinterpret_cast<unsigned char>(v43));
                r9_64 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(r12_44));
                while (1) {
                    rax65 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rax65 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax65) + reinterpret_cast<unsigned char>(r12_44));
                    }
                    rbp54 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rbp54) - (reinterpret_cast<unsigned char>(rbp54) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp54) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp54) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax65) < reinterpret_cast<uint64_t>(r10_63))))))) & reinterpret_cast<unsigned char>(r12_44)) + (reinterpret_cast<uint64_t>(rax65) + reinterpret_cast<uint64_t>(r9_64)));
                    r13_66 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        r13_66 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_66) + reinterpret_cast<unsigned char>(r12_44));
                    }
                    rax67 = r14_53;
                    *reinterpret_cast<uint32_t*>(&rax68) = *reinterpret_cast<uint32_t*>(&rax67) & 31;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax68) + 4) = 0;
                    if (rax68 == 1) {
                        rdi69 = r13_66;
                        rax70 = gcd_odd(rdi69, r12_44);
                        if (!reinterpret_cast<int1_t>(rax70 == 1)) 
                            break;
                    }
                    --r14_53;
                    if (r14_53) 
                        continue;
                    rcx71 = r15_52 + r15_52;
                    if (!r15_52) {
                        r15_52 = rcx71;
                        r11_56 = rbp54;
                    } else {
                        do {
                            rdi72 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                            rax73 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi72) + reinterpret_cast<unsigned char>(r12_44));
                            if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                                rdi72 = rax73;
                            }
                            ++r14_53;
                        } while (r15_52 != r14_53);
                        r11_56 = rbp54;
                        r15_52 = rcx71;
                        rbp54 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax73) - (reinterpret_cast<uint64_t>(rax73) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax73) < reinterpret_cast<uint64_t>(rax73) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi72) < reinterpret_cast<uint64_t>(r10_63)))) & reinterpret_cast<unsigned char>(r12_44)) + (reinterpret_cast<uint64_t>(rdi72) + reinterpret_cast<uint64_t>(r9_64)));
                    }
                }
                r9_28 = r8_62;
                r8_27 = r11_56;
                r11_74 = r9_64;
                do {
                    rsi75 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    rax76 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi75) + reinterpret_cast<unsigned char>(r12_44));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rsi75 = rax76;
                    }
                    rbx77 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax76) - (reinterpret_cast<uint64_t>(rax76) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax76) < reinterpret_cast<uint64_t>(rax76) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rsi75) < reinterpret_cast<uint64_t>(r10_63)))) & reinterpret_cast<unsigned char>(r12_44)) + (reinterpret_cast<uint64_t>(rsi75) + reinterpret_cast<uint64_t>(r11_74)));
                    rsi78 = r12_44;
                    rdi69 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rdi69) - (reinterpret_cast<unsigned char>(rdi69) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi69) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rdi69) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_27) < reinterpret_cast<unsigned char>(rbx77))))))) & reinterpret_cast<unsigned char>(r12_44)) + (reinterpret_cast<unsigned char>(r8_27) - reinterpret_cast<unsigned char>(rbx77)));
                    rax79 = gcd_odd(rdi69, rsi78);
                } while (rax79 == 1);
                rcx80 = r8_27;
                r11_81 = rax79;
                if (rax79 == r12_44) 
                    goto addr_5af7_48;
                rdx82 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_44) % reinterpret_cast<unsigned char>(r11_81));
                rax83 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_44) / reinterpret_cast<unsigned char>(r11_81));
                r8_84 = rax83;
                r12_44 = rax83;
                if (reinterpret_cast<unsigned char>(r11_81) > reinterpret_cast<unsigned char>(1) && (reinterpret_cast<unsigned char>(r11_81) <= reinterpret_cast<unsigned char>(0x17ded78) || (al85 = prime_p_part_0(r11_81, rsi78, rdx82, rcx80), r11_81 = r11_81, rcx80 = rcx80, r8_84 = rax83, !!al85))) {
                    *reinterpret_cast<int32_t*>(&rdx86) = 1;
                    *reinterpret_cast<int32_t*>(&rdx86 + 4) = 0;
                    rsi87 = r11_81;
                    factor_insert_multiplicity(0x4f2, rsi87, 1);
                    r8_27 = r8_84;
                    rcx88 = rcx80;
                    zf89 = reinterpret_cast<int1_t>(r8_27 == 1);
                    if (reinterpret_cast<unsigned char>(r8_27) <= reinterpret_cast<unsigned char>(1)) 
                        goto addr_5aca_51; else 
                        goto addr_5a2f_52;
                }
                rdx86 = reinterpret_cast<void**>(0x4f2);
                rsi87 = v43 + 1;
                factor_using_pollard_rho(r11_81, rsi87, 0x4f2, rcx80);
                r8_27 = r8_84;
                rcx88 = rcx80;
                zf89 = reinterpret_cast<int1_t>(r8_27 == 1);
                if (reinterpret_cast<unsigned char>(r8_27) > reinterpret_cast<unsigned char>(1)) {
                    addr_5a2f_52:
                    if (reinterpret_cast<unsigned char>(r8_27) <= reinterpret_cast<unsigned char>(0x17ded78)) 
                        goto addr_5ad7_54;
                    al90 = prime_p_part_0(r8_27, rsi87, rdx86, rcx88);
                    r8_27 = r8_27;
                    if (al90) 
                        goto addr_5ad7_54;
                } else {
                    addr_5aca_51:
                    if (zf89) 
                        goto addr_5b45_56; else 
                        goto addr_5acc_57;
                }
                rbp54 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp54) % reinterpret_cast<unsigned char>(r8_27));
                rcx55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx88) % reinterpret_cast<unsigned char>(r8_27));
                continue;
                addr_5acc_57:
                *reinterpret_cast<int32_t*>(&rcx55) = 0;
                *reinterpret_cast<int32_t*>(&rcx55 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rbp54) = 0;
                *reinterpret_cast<int32_t*>(&rbp54 + 4) = 0;
            }
            break;
            addr_5af7_48:
            ++v43;
        } while (reinterpret_cast<unsigned char>(r12_44) > reinterpret_cast<unsigned char>(1));
        goto addr_5b07_19;
    }
    fun_2810("a < n", "src/factor.c", 0x5c4, "factor_using_pollard_rho", r8_27, r9_28);
    addr_5b45_56:
    goto v11;
    addr_5ad7_54:
    *reinterpret_cast<uint32_t*>(&rax91) = g5ec;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax91) + 4) = 0;
    r8_92 = r8_27;
    r11_93 = reinterpret_cast<void***>(0x502);
    r10_94 = reinterpret_cast<signed char*>(0x5d2);
    r9d95 = *reinterpret_cast<uint32_t*>(&rax91);
    if (!*reinterpret_cast<uint32_t*>(&rax91)) 
        goto addr_2fa1_61;
    ebx96 = static_cast<int32_t>(rax91 - 1);
    rcx97 = reinterpret_cast<struct s46*>(static_cast<int64_t>(ebx96));
    rax98 = rcx97;
    do {
        *reinterpret_cast<int32_t*>(&rsi99) = *reinterpret_cast<int32_t*>(&rax98);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi99) + 4) = 0;
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(0x4f2 + reinterpret_cast<int64_t>(rax98) * 8 + 16)) <= reinterpret_cast<unsigned char>(r8_92)) 
            break;
        rax98 = reinterpret_cast<struct s46*>(reinterpret_cast<int64_t>(rax98) - 1);
        *reinterpret_cast<int32_t*>(&rsi99) = *reinterpret_cast<int32_t*>(&rax98);
    } while (*reinterpret_cast<int32_t*>(&rax98) != -1);
    goto addr_2f80_65;
    if (*reinterpret_cast<void***>(0x4f2 + reinterpret_cast<int64_t>(rax98) * 8 + 16) == r8_92) {
        rax98->f5d2 = reinterpret_cast<signed char>(rax98->f5d2 + 1);
        goto v11;
    }
    rax100 = reinterpret_cast<struct s47*>(static_cast<int64_t>(static_cast<int32_t>(rsi99 + 1)));
    r11_93 = reinterpret_cast<void***>(0x502 + reinterpret_cast<int64_t>(rax100) * 8);
    r10_94 = &rax100->f5d2;
    if (*reinterpret_cast<int32_t*>(&rsi99) >= ebx96) {
        addr_2fa1_61:
        r9d101 = r9d95 + 1;
        *r11_93 = r8_92;
        *r10_94 = 1;
        g5ec = *reinterpret_cast<unsigned char*>(&r9d101);
        goto v11;
    }
    do {
        addr_2f80_65:
        *reinterpret_cast<int64_t*>(0x4f2 + reinterpret_cast<int64_t>(rcx97) * 8 + 24) = *reinterpret_cast<int64_t*>(0x4f2 + reinterpret_cast<int64_t>(rcx97) * 8 + 16);
        eax102 = *reinterpret_cast<unsigned char*>(0x4f2 + reinterpret_cast<int64_t>(rcx97) + 0xe0);
        *reinterpret_cast<signed char*>(0x4f2 + reinterpret_cast<int64_t>(rcx97) + 0xe1) = *reinterpret_cast<signed char*>(&eax102);
        rcx97 = reinterpret_cast<struct s46*>(reinterpret_cast<int64_t>(rcx97) - 1);
    } while (*reinterpret_cast<int32_t*>(&rsi99) < *reinterpret_cast<int32_t*>(&rcx97));
    goto addr_2fa1_61;
    addr_5689_70:
    goto addr_5690_14;
    addr_5680_71:
    while (rax103 == rbx29) {
        while (1) {
            *reinterpret_cast<uint32_t*>(&rax104) = r13_22->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax104) + 4) = 0;
            r12_39 = r12_39 + rax104;
            rax105 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx29) * r12_39);
            rdx106 = __intrinsic();
            r15_14 = rax105;
            if (rdx106) {
                if (reinterpret_cast<unsigned char>(rbp6) <= reinterpret_cast<unsigned char>(rdx106)) 
                    goto addr_5719_17;
                rcx107 = rbp6;
                esi108 = 64;
                *reinterpret_cast<int32_t*>(&rax109) = 0;
                *reinterpret_cast<int32_t*>(&rax109 + 4) = 0;
                do {
                    rdi110 = reinterpret_cast<unsigned char>(rcx107) << 63;
                    rcx107 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx107) >> 1);
                    rax109 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax109) >> 1) | rdi110);
                    if (reinterpret_cast<unsigned char>(rcx107) < reinterpret_cast<unsigned char>(rdx106) || rcx107 == rdx106 && reinterpret_cast<unsigned char>(rax109) <= reinterpret_cast<unsigned char>(r15_14)) {
                        cf111 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_14) < reinterpret_cast<unsigned char>(rax109));
                        r15_14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_14) - reinterpret_cast<unsigned char>(rax109));
                        rdx106 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx106) - (reinterpret_cast<unsigned char>(rcx107) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx106) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx107) + static_cast<uint64_t>(cf111))))));
                    }
                    --esi108;
                } while (esi108);
            } else {
                r15_14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax105) % reinterpret_cast<unsigned char>(rbp6));
            }
            *reinterpret_cast<uint32_t*>(&r8_27) = v12;
            *reinterpret_cast<int32_t*>(&r8_27 + 4) = 0;
            r9_28 = rbx29;
            al112 = millerrabin(rbp6, r14_5, r15_14, v33, *reinterpret_cast<uint32_t*>(&r8_27), r9_28);
            if (!al112) 
                goto addr_56fc_13;
            r13_22 = reinterpret_cast<struct s0*>(&r13_22->f1);
            if (reinterpret_cast<int1_t>(0x105dc == r13_22)) 
                break;
            addr_55b0_12:
            if (!v36) 
                goto addr_5690_14;
            r11_113 = v32;
            do {
                r8_27 = rbx29;
                rax103 = powm(r15_14, reinterpret_cast<uint64_t>(v11) / v114, rbp6, r14_5, r8_27, r9_28);
                if (v38 == r11_113) 
                    goto addr_5680_71;
                r11_113 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r11_113) + 8);
            } while (rax103 != rbx29);
        }
        fun_2700();
        fun_2a20();
        rax103 = fun_25f0();
    }
    goto addr_5689_70;
}

void factor_insert_multiplicity(void** rdi, void** rsi, signed char dl, ...) {
    int64_t rax4;
    void** r8_5;
    void*** r11_6;
    signed char* r10_7;
    uint32_t r9d8;
    int32_t ebx9;
    void* rcx10;
    void* rax11;
    int64_t rsi12;
    void* rax13;
    uint32_t r9d14;
    uint32_t eax15;

    *reinterpret_cast<uint32_t*>(&rax4) = *reinterpret_cast<unsigned char*>(rdi + 0xfa);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    r8_5 = rsi;
    r11_6 = reinterpret_cast<void***>(rdi + 16);
    r10_7 = reinterpret_cast<signed char*>(rdi + 0xe0);
    r9d8 = *reinterpret_cast<uint32_t*>(&rax4);
    if (!*reinterpret_cast<uint32_t*>(&rax4)) 
        goto addr_2fa1_2;
    ebx9 = static_cast<int32_t>(rax4 - 1);
    rcx10 = reinterpret_cast<void*>(static_cast<int64_t>(ebx9));
    rax11 = rcx10;
    do {
        *reinterpret_cast<int32_t*>(&rsi12) = *reinterpret_cast<int32_t*>(&rax11);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi12) + 4) = 0;
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi + reinterpret_cast<uint64_t>(rax11) * 8) + 16)) <= reinterpret_cast<unsigned char>(r8_5)) 
            break;
        rax11 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax11) - 1);
        *reinterpret_cast<int32_t*>(&rsi12) = *reinterpret_cast<int32_t*>(&rax11);
    } while (*reinterpret_cast<int32_t*>(&rax11) != -1);
    goto addr_2f80_6;
    if (*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi + reinterpret_cast<uint64_t>(rax11) * 8) + 16) == r8_5) {
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_7) + reinterpret_cast<uint64_t>(rax11)) = reinterpret_cast<signed char>(*reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_7) + reinterpret_cast<uint64_t>(rax11)) + dl);
        return;
    }
    rax13 = reinterpret_cast<void*>(static_cast<int64_t>(static_cast<int32_t>(rsi12 + 1)));
    r11_6 = r11_6 + reinterpret_cast<uint64_t>(rax13) * 8;
    r10_7 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_7) + reinterpret_cast<uint64_t>(rax13));
    if (*reinterpret_cast<int32_t*>(&rsi12) >= ebx9) {
        addr_2fa1_2:
        r9d14 = r9d8 + 1;
        *r11_6 = r8_5;
        *r10_7 = dl;
        *reinterpret_cast<unsigned char*>(rdi + 0xfa) = *reinterpret_cast<unsigned char*>(&r9d14);
        return;
    }
    do {
        addr_2f80_6:
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi + reinterpret_cast<uint64_t>(rcx10) * 8) + 24) = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi + reinterpret_cast<uint64_t>(rcx10) * 8) + 16);
        eax15 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<uint64_t>(rcx10) + 0xe0);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<uint64_t>(rcx10) + 0xe1) = *reinterpret_cast<signed char*>(&eax15);
        rcx10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx10) - 1);
    } while (*reinterpret_cast<int32_t*>(&rsi12) < *reinterpret_cast<int32_t*>(&rcx10));
    goto addr_2fa1_2;
}

void** mod2(void*** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void*** r9_7;
    uint32_t r10d8;
    uint32_t r10d9;
    uint32_t eax10;
    uint32_t ecx11;
    uint32_t ecx12;
    void** r8_13;
    void** rax14;
    uint32_t edi15;
    uint1_t cf16;
    uint64_t rcx17;
    int64_t v18;
    int64_t rax19;
    void** r14_20;
    int32_t eax21;
    int32_t eax22;
    void** rbp23;
    int32_t eax24;
    int32_t eax25;

    if (rcx) {
        r9_7 = rdi;
        if (rsi && (r10d8 = (r10d9 ^ 63) - (eax10 ^ 63), ecx11 = 64 - r10d8, ecx12 = r10d8, r8_13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) << *reinterpret_cast<unsigned char*>(&ecx12)), rax14 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8) >> *reinterpret_cast<signed char*>(&ecx11)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rcx) << *reinterpret_cast<unsigned char*>(&ecx12))), !(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(r10d8) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(r10d8 == 0)))) {
            edi15 = 0;
            do {
                if (reinterpret_cast<unsigned char>(rax14) < reinterpret_cast<unsigned char>(rsi) || rax14 == rsi && reinterpret_cast<unsigned char>(rdx) >= reinterpret_cast<unsigned char>(r8_13)) {
                    cf16 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx) < reinterpret_cast<unsigned char>(r8_13));
                    rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx) - reinterpret_cast<unsigned char>(r8_13));
                    rsi = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi) - (reinterpret_cast<unsigned char>(rax14) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rsi) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rax14) + static_cast<uint64_t>(cf16))))));
                }
                ++edi15;
                rcx17 = reinterpret_cast<unsigned char>(rax14) << 63;
                rax14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax14) >> 1);
                r8_13 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_13) >> 1) | rcx17);
            } while (r10d8 != edi15);
        }
        *r9_7 = rsi;
        return rdx;
    }
    v18 = rax19;
    fun_2810("d1 != 0", "src/factor.c", 0x19f, "mod2", r8, r9);
    r14_20 = r9;
    fun_2730("mod2", 0x19f, r8, "d1 != 0", r8, r9);
    eax21 = fun_2aa0("mod2", 1, r8, "d1 != 0", r8, r9);
    if (!eax21) 
        goto addr_3bc4_10;
    eax22 = fun_2a00("mod2", "src/factor.c", r8, "d1 != 0", r8);
    if (eax22) 
        goto addr_3b75_12;
    addr_3bc4_10:
    goto v18;
    addr_3b75_12:
    *reinterpret_cast<int32_t*>(&rbp23) = 1;
    *reinterpret_cast<int32_t*>(&rbp23 + 4) = 0;
    if (reinterpret_cast<unsigned char>(r14_20) > reinterpret_cast<unsigned char>(1)) {
        do {
            fun_2ad0("mod2", "mod2", 2, "d1 != 0");
            eax24 = fun_2a00("mod2", "src/factor.c", 2, "d1 != 0", r8);
            if (!eax24) 
                goto addr_3bc4_10;
            eax25 = fun_2aa0("mod2", 1, 2, "d1 != 0", r8, r9);
        } while (eax25 && (++rbp23, r14_20 != rbp23));
    }
    goto v18;
}

void factor_using_pollard_rho(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** v5;
    void** v6;
    void** r8_7;
    void** r9_8;
    void** r12_9;
    void** rcx10;
    int32_t esi11;
    void** rax12;
    void* rdx13;
    void* rdi14;
    uint1_t cf15;
    int64_t rbp16;
    int64_t r15_17;
    int64_t r14_18;
    void** rbp19;
    void** rcx20;
    void** r11_21;
    uint64_t rax22;
    int64_t rax23;
    void* rax24;
    void* rdx25;
    void* rax26;
    void** r8_27;
    void* r10_28;
    void* r9_29;
    void* rax30;
    void** r13_31;
    int64_t rax32;
    int64_t rax33;
    void** rdi34;
    void** rax35;
    int64_t rcx36;
    void* rdi37;
    void* rax38;
    void* r11_39;
    void* rsi40;
    void* rax41;
    void** rbx42;
    void** rsi43;
    void** rax44;
    void** rcx45;
    void** r11_46;
    void** rdx47;
    void** rax48;
    void** r8_49;
    signed char al50;
    void** rdx51;
    void** rsi52;
    void** rcx53;
    int1_t zf54;
    signed char al55;
    void** rdi56;
    int64_t rax57;
    void** r8_58;
    void*** r11_59;
    signed char* r10_60;
    uint32_t r9d61;
    int32_t ebx62;
    void* rcx63;
    void* rax64;
    int64_t rsi65;
    void* rax66;
    uint32_t r9d67;
    uint32_t eax68;

    v5 = rsi;
    v6 = rdx;
    if (reinterpret_cast<unsigned char>(rdi) <= reinterpret_cast<unsigned char>(1)) {
        addr_5b07_2:
        fun_2810("(1) < (n)", "src/factor.c", 0x5be, "factor_using_pollard_rho", r8_7, r9_8);
    } else {
        r12_9 = rdi;
        do {
            rcx10 = r12_9;
            esi11 = 64;
            *reinterpret_cast<int32_t*>(&rax12) = 1;
            *reinterpret_cast<int32_t*>(&rax12 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx13) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdi14 = reinterpret_cast<void*>(0);
            do {
                r8_7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) << 63);
                rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) >> 1);
                rdx13 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx13) >> 1 | reinterpret_cast<unsigned char>(r8_7));
                if (reinterpret_cast<unsigned char>(rcx10) < reinterpret_cast<unsigned char>(rax12) || rcx10 == rax12 && reinterpret_cast<uint64_t>(rdx13) <= reinterpret_cast<uint64_t>(rdi14)) {
                    cf15 = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi14) < reinterpret_cast<uint64_t>(rdx13));
                    rdi14 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi14) - reinterpret_cast<uint64_t>(rdx13));
                    rax12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax12) - (reinterpret_cast<unsigned char>(rcx10) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax12) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx10) + static_cast<uint64_t>(cf15))))));
                }
                --esi11;
            } while (esi11);
            *reinterpret_cast<int32_t*>(&rbp16) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp16) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r15_17) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_17) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r14_18) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_18) + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rbp16) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_9) - reinterpret_cast<uint64_t>(rdi14) > reinterpret_cast<uint64_t>(rdi14));
            rbp19 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbp16) & reinterpret_cast<unsigned char>(r12_9)) + (reinterpret_cast<uint64_t>(rdi14) + reinterpret_cast<uint64_t>(rdi14) - reinterpret_cast<unsigned char>(r12_9)));
            rcx20 = rbp19;
            while (reinterpret_cast<unsigned char>(v5) < reinterpret_cast<unsigned char>(r12_9)) {
                r11_21 = rcx20;
                rax22 = reinterpret_cast<unsigned char>(r12_9) >> 1;
                *reinterpret_cast<uint32_t*>(&rax23) = *reinterpret_cast<uint32_t*>(&rax22) & 0x7f;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax23) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax23);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax24) + 4) = 0;
                rdx25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax24) + reinterpret_cast<int64_t>(rax24) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax24) * reinterpret_cast<int64_t>(rax24) * reinterpret_cast<unsigned char>(r12_9)));
                rax26 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx25) + reinterpret_cast<uint64_t>(rdx25) - reinterpret_cast<uint64_t>(rdx25) * reinterpret_cast<uint64_t>(rdx25) * reinterpret_cast<unsigned char>(r12_9));
                r8_27 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax26) + reinterpret_cast<uint64_t>(rax26) - reinterpret_cast<uint64_t>(rax26) * reinterpret_cast<uint64_t>(rax26) * reinterpret_cast<unsigned char>(r12_9));
                r10_28 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_9) - reinterpret_cast<unsigned char>(v5));
                r9_29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v5) - reinterpret_cast<unsigned char>(r12_9));
                while (1) {
                    rax30 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rax30 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax30) + reinterpret_cast<unsigned char>(r12_9));
                    }
                    rbp19 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rbp19) - (reinterpret_cast<unsigned char>(rbp19) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp19) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp19) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax30) < reinterpret_cast<uint64_t>(r10_28))))))) & reinterpret_cast<unsigned char>(r12_9)) + (reinterpret_cast<uint64_t>(rax30) + reinterpret_cast<uint64_t>(r9_29)));
                    r13_31 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        r13_31 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_31) + reinterpret_cast<unsigned char>(r12_9));
                    }
                    rax32 = r14_18;
                    *reinterpret_cast<uint32_t*>(&rax33) = *reinterpret_cast<uint32_t*>(&rax32) & 31;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
                    if (rax33 == 1) {
                        rdi34 = r13_31;
                        rax35 = gcd_odd(rdi34, r12_9);
                        if (!reinterpret_cast<int1_t>(rax35 == 1)) 
                            break;
                    }
                    --r14_18;
                    if (r14_18) 
                        continue;
                    rcx36 = r15_17 + r15_17;
                    if (!r15_17) {
                        r15_17 = rcx36;
                        r11_21 = rbp19;
                    } else {
                        do {
                            rdi37 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                            rax38 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi37) + reinterpret_cast<unsigned char>(r12_9));
                            if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                                rdi37 = rax38;
                            }
                            ++r14_18;
                        } while (r15_17 != r14_18);
                        r11_21 = rbp19;
                        r15_17 = rcx36;
                        rbp19 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax38) - (reinterpret_cast<uint64_t>(rax38) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax38) < reinterpret_cast<uint64_t>(rax38) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi37) < reinterpret_cast<uint64_t>(r10_28)))) & reinterpret_cast<unsigned char>(r12_9)) + (reinterpret_cast<uint64_t>(rdi37) + reinterpret_cast<uint64_t>(r9_29)));
                    }
                }
                r9_8 = r8_27;
                r8_7 = r11_21;
                r11_39 = r9_29;
                do {
                    rsi40 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    rax41 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi40) + reinterpret_cast<unsigned char>(r12_9));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rsi40 = rax41;
                    }
                    rbx42 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax41) - (reinterpret_cast<uint64_t>(rax41) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax41) < reinterpret_cast<uint64_t>(rax41) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rsi40) < reinterpret_cast<uint64_t>(r10_28)))) & reinterpret_cast<unsigned char>(r12_9)) + (reinterpret_cast<uint64_t>(rsi40) + reinterpret_cast<uint64_t>(r11_39)));
                    rsi43 = r12_9;
                    rdi34 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rdi34) - (reinterpret_cast<unsigned char>(rdi34) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi34) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rdi34) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_7) < reinterpret_cast<unsigned char>(rbx42))))))) & reinterpret_cast<unsigned char>(r12_9)) + (reinterpret_cast<unsigned char>(r8_7) - reinterpret_cast<unsigned char>(rbx42)));
                    rax44 = gcd_odd(rdi34, rsi43);
                } while (rax44 == 1);
                rcx45 = r8_7;
                r11_46 = rax44;
                if (rax44 == r12_9) 
                    goto addr_5af7_31;
                rdx47 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_9) % reinterpret_cast<unsigned char>(r11_46));
                rax48 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_9) / reinterpret_cast<unsigned char>(r11_46));
                r8_49 = rax48;
                r12_9 = rax48;
                if (reinterpret_cast<unsigned char>(r11_46) > reinterpret_cast<unsigned char>(1) && (reinterpret_cast<unsigned char>(r11_46) <= reinterpret_cast<unsigned char>(0x17ded78) || (al50 = prime_p_part_0(r11_46, rsi43, rdx47, rcx45), r11_46 = r11_46, rcx45 = rcx45, r8_49 = rax48, !!al50))) {
                    *reinterpret_cast<int32_t*>(&rdx51) = 1;
                    *reinterpret_cast<int32_t*>(&rdx51 + 4) = 0;
                    rsi52 = r11_46;
                    factor_insert_multiplicity(v6, rsi52, 1);
                    r8_7 = r8_49;
                    rcx53 = rcx45;
                    zf54 = reinterpret_cast<int1_t>(r8_7 == 1);
                    if (reinterpret_cast<unsigned char>(r8_7) <= reinterpret_cast<unsigned char>(1)) 
                        goto addr_5aca_34; else 
                        goto addr_5a2f_35;
                }
                rdx51 = v6;
                rsi52 = v5 + 1;
                factor_using_pollard_rho(r11_46, rsi52, rdx51, rcx45);
                r8_7 = r8_49;
                rcx53 = rcx45;
                zf54 = reinterpret_cast<int1_t>(r8_7 == 1);
                if (reinterpret_cast<unsigned char>(r8_7) > reinterpret_cast<unsigned char>(1)) {
                    addr_5a2f_35:
                    if (reinterpret_cast<unsigned char>(r8_7) <= reinterpret_cast<unsigned char>(0x17ded78)) 
                        goto addr_5ad7_37;
                    al55 = prime_p_part_0(r8_7, rsi52, rdx51, rcx53);
                    r8_7 = r8_7;
                    if (al55) 
                        goto addr_5ad7_37;
                } else {
                    addr_5aca_34:
                    if (zf54) 
                        goto addr_5b45_39; else 
                        goto addr_5acc_40;
                }
                rbp19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp19) % reinterpret_cast<unsigned char>(r8_7));
                rcx20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx53) % reinterpret_cast<unsigned char>(r8_7));
                continue;
                addr_5acc_40:
                *reinterpret_cast<int32_t*>(&rcx20) = 0;
                *reinterpret_cast<int32_t*>(&rcx20 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rbp19) = 0;
                *reinterpret_cast<int32_t*>(&rbp19 + 4) = 0;
            }
            break;
            addr_5af7_31:
            ++v5;
        } while (reinterpret_cast<unsigned char>(r12_9) > reinterpret_cast<unsigned char>(1));
        goto addr_5b07_2;
    }
    fun_2810("a < n", "src/factor.c", 0x5c4, "factor_using_pollard_rho", r8_7, r9_8);
    addr_5b45_39:
    return;
    addr_5ad7_37:
    rdi56 = v6;
    *reinterpret_cast<uint32_t*>(&rax57) = *reinterpret_cast<unsigned char*>(rdi56 + 0xfa);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax57) + 4) = 0;
    r8_58 = r8_7;
    r11_59 = reinterpret_cast<void***>(rdi56 + 16);
    r10_60 = reinterpret_cast<signed char*>(rdi56 + 0xe0);
    r9d61 = *reinterpret_cast<uint32_t*>(&rax57);
    if (!*reinterpret_cast<uint32_t*>(&rax57)) 
        goto addr_2fa1_44;
    ebx62 = static_cast<int32_t>(rax57 - 1);
    rcx63 = reinterpret_cast<void*>(static_cast<int64_t>(ebx62));
    rax64 = rcx63;
    do {
        *reinterpret_cast<int32_t*>(&rsi65) = *reinterpret_cast<int32_t*>(&rax64);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi65) + 4) = 0;
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi56 + reinterpret_cast<uint64_t>(rax64) * 8) + 16)) <= reinterpret_cast<unsigned char>(r8_58)) 
            break;
        rax64 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax64) - 1);
        *reinterpret_cast<int32_t*>(&rsi65) = *reinterpret_cast<int32_t*>(&rax64);
    } while (*reinterpret_cast<int32_t*>(&rax64) != -1);
    goto addr_2f80_48;
    if (*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi56 + reinterpret_cast<uint64_t>(rax64) * 8) + 16) == r8_58) {
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_60) + reinterpret_cast<uint64_t>(rax64)) = reinterpret_cast<signed char>(*reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_60) + reinterpret_cast<uint64_t>(rax64)) + 1);
        return;
    }
    rax66 = reinterpret_cast<void*>(static_cast<int64_t>(static_cast<int32_t>(rsi65 + 1)));
    r11_59 = r11_59 + reinterpret_cast<uint64_t>(rax66) * 8;
    r10_60 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_60) + reinterpret_cast<uint64_t>(rax66));
    if (*reinterpret_cast<int32_t*>(&rsi65) >= ebx62) {
        addr_2fa1_44:
        r9d67 = r9d61 + 1;
        *r11_59 = r8_58;
        *r10_60 = 1;
        *reinterpret_cast<unsigned char*>(rdi56 + 0xfa) = *reinterpret_cast<unsigned char*>(&r9d67);
        return;
    }
    do {
        addr_2f80_48:
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi56 + reinterpret_cast<uint64_t>(rcx63) * 8) + 24) = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi56 + reinterpret_cast<uint64_t>(rcx63) * 8) + 16);
        eax68 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi56) + reinterpret_cast<uint64_t>(rcx63) + 0xe0);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi56) + reinterpret_cast<uint64_t>(rcx63) + 0xe1) = *reinterpret_cast<signed char*>(&eax68);
        rcx63 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx63) - 1);
    } while (*reinterpret_cast<int32_t*>(&rsi65) < *reinterpret_cast<int32_t*>(&rcx63));
    goto addr_2fa1_44;
}

unsigned char millerrabin(void** rdi, struct s0* rsi, void** rdx, struct s0* rcx, uint32_t r8d, void** r9) {
    void** r11_7;
    void** r12_8;
    uint32_t ebp9;
    void** r8_10;
    void** rax11;
    void** rdi12;
    int32_t eax13;
    uint32_t esi14;

    r11_7 = rdi;
    r12_8 = r9;
    ebp9 = r8d;
    r8_10 = r9;
    rax11 = powm(rdx, rcx, r11_7, rsi, r8_10, r9);
    rdi12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r11_7) - reinterpret_cast<unsigned char>(r12_8));
    *reinterpret_cast<unsigned char*>(&r8_10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r12_8 == rax11)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(rax11 == rdi12)));
    if (*reinterpret_cast<unsigned char*>(&r8_10) || ebp9 <= 1) {
        addr_30b5_2:
        eax13 = *reinterpret_cast<int32_t*>(&r8_10);
        return *reinterpret_cast<unsigned char*>(&eax13);
    } else {
        esi14 = 1;
        do {
            rax11 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
            if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                rax11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax11) + reinterpret_cast<unsigned char>(r11_7));
            }
            if (rdi12 == rax11) 
                break;
        } while (r12_8 != rax11 && (++esi14, ebp9 != esi14));
        goto addr_30b5_2;
    }
    *reinterpret_cast<int32_t*>(&r8_10) = 1;
    goto addr_30b5_2;
}

struct s48 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
    void** f18;
    signed char[7] pad32;
    int64_t f20;
    void** f28;
    signed char[7] pad48;
    int64_t f30;
    void** f38;
    signed char[7] pad64;
    int64_t f40;
    void** f48;
    signed char[7] pad80;
    int64_t f50;
    void** f58;
    signed char[7] pad96;
    int64_t f60;
    void** f68;
    signed char[66263] pad66368;
    unsigned char f10340;
};

struct s49 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s50 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s51 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s52 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s53 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s54 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s55 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s56 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s57 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s58 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s59 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s60 {
    signed char[54720] pad54720;
    unsigned char fd5c0;
};

struct s61 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void factor() {
    int64_t v1;
    void** r8_2;
    void** rdi3;
    void** r9_4;
    void** rdx5;
    void** v6;
    void** r13_7;
    void** rbp8;
    void** rsi9;
    void** v10;
    void** rbx11;
    void*** rsp12;
    void** r12_13;
    int64_t r13_14;
    void** r11_15;
    void** r10_16;
    void*** r14_17;
    struct s48* rbx18;
    uint64_t r15_19;
    void** rcx20;
    void** rdx21;
    void** r10_22;
    void** r13_23;
    void* rax24;
    int32_t ecx25;
    int32_t ecx26;
    int32_t edx27;
    void** rcx28;
    int64_t rbx29;
    void** rcx30;
    void** v31;
    void** v32;
    void** rdx33;
    struct s49* rcx34;
    void* rcx35;
    void** rcx36;
    void** rdx37;
    void** rdx38;
    struct s50* rcx39;
    struct s51* rsi40;
    void* rsi41;
    void* rcx42;
    struct s52* rsi43;
    void* rsi44;
    void** rcx45;
    void** rdx46;
    void** v47;
    void** rdx48;
    uint32_t ecx49;
    uint32_t edx50;
    void** rsi51;
    struct s53* rdi52;
    void* rdi53;
    void** rdx54;
    void** rdx55;
    uint32_t ecx56;
    uint32_t edx57;
    void** rsi58;
    struct s54* rdi59;
    void* rdi60;
    void** rdx61;
    void** rdx62;
    uint32_t ecx63;
    uint32_t edx64;
    void** rsi65;
    struct s55* rdi66;
    void* rdi67;
    void** rdx68;
    int64_t rax69;
    void* rax70;
    int64_t rax71;
    struct s56* rax72;
    uint32_t ecx73;
    uint32_t edx74;
    void** rsi75;
    struct s57* rdi76;
    void* rdi77;
    void** rdx78;
    struct s58* rcx79;
    struct s59* rsi80;
    void* rcx81;
    void* rsi82;
    void** rcx83;
    void** rdi84;
    void** v85;
    void** v86;
    unsigned char al87;
    void** r15_88;
    void** rbp89;
    void*** rsp90;
    void** v91;
    void** rax92;
    void** v93;
    uint64_t rcx94;
    void** rdx95;
    int64_t rcx96;
    uint64_t rcx97;
    uint1_t cf98;
    void** rax99;
    void** rsi100;
    uint1_t cf101;
    int1_t cf102;
    void** v103;
    void** v104;
    void** tmp64_105;
    void** rax106;
    void** r12_107;
    void** r10_108;
    void** v109;
    void** v110;
    void** v111;
    void** v112;
    void** r14_113;
    void** r15_114;
    void*** v115;
    void** r9_116;
    uint64_t rax117;
    int64_t rax118;
    void* rax119;
    void* rdx120;
    void* rax121;
    void** rax122;
    void** rbp123;
    int64_t rax124;
    void** v125;
    int64_t v126;
    void** rax127;
    void** tmp64_128;
    void** rdx129;
    void** v130;
    void** rdx131;
    void** r8_132;
    void** rcx133;
    void** tmp64_134;
    uint1_t cf135;
    void** rax136;
    void** rax137;
    int64_t rax138;
    void** rax139;
    void** v140;
    void** v141;
    void** rdx142;
    void** r15_143;
    void** r12_144;
    void** rsi145;
    void** rbp146;
    void** rax147;
    void** tmp64_148;
    void** rcx149;
    void** v150;
    void** r11_151;
    void** r10_152;
    void** v153;
    void** r15_154;
    void** r12_155;
    void*** rbp156;
    void** r9_157;
    void** rax158;
    void** tmp64_159;
    void** rdx160;
    void** rdx161;
    void** rdx162;
    void** rsi163;
    void** tmp64_164;
    uint1_t cf165;
    void** rax166;
    void** rdi167;
    void** r8_168;
    uint64_t rax169;
    int64_t rax170;
    void** r13_171;
    void* rax172;
    void* rdx173;
    void** rdx174;
    void* rax175;
    signed char al176;
    unsigned char al177;
    void** rax178;
    void* rsp179;
    void** rax180;
    void* rsp181;
    void** rax182;
    uint64_t rax183;
    struct s60* rax184;
    void* rax185;
    void* rdx186;
    void* rax187;
    unsigned char al188;
    int64_t rax189;
    struct s61* rax190;
    unsigned char al191;
    void** r9_192;
    void** rsi193;
    void** rdi194;
    void** rdi195;
    void** rdx196;
    void** rsi197;
    int64_t rax198;
    void** r8_199;
    void*** r11_200;
    signed char* r10_201;
    uint32_t r9d202;
    int32_t ebx203;
    void* rcx204;
    void* rax205;
    int64_t rsi206;
    void* rax207;
    uint32_t r9d208;
    uint32_t eax209;
    void** r12_210;
    void** rcx211;
    int32_t esi212;
    void** rax213;
    void* rdx214;
    void* rdi215;
    uint1_t cf216;
    int64_t rbp217;
    int64_t r15_218;
    int64_t r14_219;
    void** rbp220;
    void** rcx221;
    void** r11_222;
    uint64_t rax223;
    int64_t rax224;
    void* rax225;
    void* rdx226;
    void* rax227;
    void** r8_228;
    void* r10_229;
    void* r9_230;
    void* rax231;
    void** r13_232;
    int64_t rax233;
    int64_t rax234;
    void** rdi235;
    void** rax236;
    int64_t rcx237;
    void* rdi238;
    void* rax239;
    void* r11_240;
    void* rsi241;
    void* rax242;
    void** rbx243;
    void** rsi244;
    void** rax245;
    void** rcx246;
    void** r11_247;
    void** rdx248;
    void** rax249;
    void** r8_250;
    signed char al251;
    void** rdx252;
    void** rsi253;
    void** rcx254;
    int1_t zf255;
    signed char al256;
    struct s0* r14_257;
    void** rbp258;
    struct s0** rsp259;
    void** rax260;
    void** v261;
    uint32_t eax262;
    struct s0* v263;
    uint32_t v264;
    void** rcx265;
    void** r15_266;
    uint64_t rax267;
    int32_t esi268;
    int64_t rax269;
    void* rax270;
    void* rdx271;
    void* rax272;
    void** rdx273;
    struct s0* r13_274;
    void** rax275;
    uint64_t rdi276;
    uint1_t cf277;
    int64_t rbx278;
    void** rbx279;
    unsigned char al280;
    void* v281;
    struct s0* v282;
    uint32_t eax283;
    uint32_t v284;
    int64_t rax285;
    void* v286;
    void** rax287;
    void* rax288;
    void* rax289;
    signed char al290;
    void** rax291;
    void* rax292;
    void** rax293;
    void** rdx294;
    void** rcx295;
    int32_t esi296;
    void** rax297;
    uint64_t rdi298;
    uint1_t cf299;
    unsigned char al300;
    void* r11_301;
    uint64_t v302;

    while ((v1 = reinterpret_cast<int64_t>(__return_address()), r8_2 = rdi3, r9_4 = rdx5, v6 = r13_7, rbp8 = rsi9, v10 = rbx11, rsp12 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56), *reinterpret_cast<unsigned char*>(rdx5 + 0xfa) = 0, *reinterpret_cast<void***>(rdx5 + 8) = reinterpret_cast<void**>(0), !!rdi3) || reinterpret_cast<unsigned char>(rsi9) > reinterpret_cast<unsigned char>(1)) {
        if (*reinterpret_cast<unsigned char*>(&rbp8) & 1) {
            addr_4df3_3:
            if (!r8_2) {
                *reinterpret_cast<int32_t*>(&r12_13) = 3;
                *reinterpret_cast<int32_t*>(&r12_13 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r13_14) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_14) + 4) = 0;
                r11_15 = reinterpret_cast<void**>(0x5555555555555555);
                r10_16 = reinterpret_cast<void**>(0xaaaaaaaaaaaaaaab);
            } else {
                r14_17 = reinterpret_cast<void***>(0xd648);
                *reinterpret_cast<uint32_t*>(&rbx18) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx18) + 4) = 0;
                r15_19 = 0xaaaaaaaaaaaaaaab;
                *reinterpret_cast<int32_t*>(&r12_13) = 3;
                *reinterpret_cast<int32_t*>(&r12_13 + 4) = 0;
                while (1) {
                    rcx20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp8) * r15_19);
                    rdx21 = __intrinsic();
                    if (reinterpret_cast<unsigned char>(rdx21) <= reinterpret_cast<unsigned char>(r8_2)) {
                        r10_22 = *r14_17;
                        while ((r13_23 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(r8_2) - reinterpret_cast<unsigned char>(rdx21)) * r15_19), reinterpret_cast<unsigned char>(r13_23) <= reinterpret_cast<unsigned char>(r10_22)) && (factor_insert_multiplicity(r9_4, r12_13, 1), rsp12 = rsp12 - 8 + 8, r8_2 = r13_23, r9_4 = r9_4, r10_22 = r10_22, rbp8 = rcx20, rdx21 = __intrinsic(), reinterpret_cast<unsigned char>(rdx21) <= reinterpret_cast<unsigned char>(r13_23))) {
                            rcx20 = reinterpret_cast<void**>(r15_19 * reinterpret_cast<unsigned char>(rcx20));
                        }
                    }
                    *reinterpret_cast<uint32_t*>(&rax24) = rbx18->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax24) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&r13_14) = *reinterpret_cast<uint32_t*>(&rbx18);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_14) + 4) = 0;
                    r14_17 = r14_17 + 16;
                    r12_13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_13) + reinterpret_cast<uint64_t>(rax24));
                    if (!r8_2) 
                        break;
                    if (*reinterpret_cast<uint32_t*>(&rbx18) > 0x29b) 
                        break;
                    r15_19 = *reinterpret_cast<uint64_t*>(r14_17 - 8);
                    rbx18 = reinterpret_cast<struct s48*>(&rbx18->pad8);
                }
                if (*reinterpret_cast<uint32_t*>(&r13_14) > 0x29b) 
                    goto addr_5200_14; else 
                    goto addr_4edd_15;
            }
        } else {
            if (rbp8) {
                __asm__("bsf rdx, rbp");
                ecx25 = 64 - *reinterpret_cast<int32_t*>(&rdx5);
                ecx26 = *reinterpret_cast<int32_t*>(&rdx5);
                rbp8 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp8) >> *reinterpret_cast<signed char*>(&ecx26)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_2) << *reinterpret_cast<unsigned char*>(&ecx25)));
                factor_insert_multiplicity(r9_4, 2, *reinterpret_cast<signed char*>(&rdx5));
                rsp12 = rsp12 - 8 + 8;
                r8_2 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_2) >> *reinterpret_cast<signed char*>(&ecx26));
                r9_4 = r9_4;
                goto addr_4df3_3;
            } else {
                __asm__("bsf rcx, r8");
                edx27 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rcx28 + 64));
                rbp8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_2) >> *reinterpret_cast<signed char*>(&rcx28));
                *reinterpret_cast<int32_t*>(&r12_13) = 3;
                *reinterpret_cast<int32_t*>(&r12_13 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r13_14) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_14) + 4) = 0;
                factor_insert_multiplicity(r9_4, 2, *reinterpret_cast<signed char*>(&edx27));
                rsp12 = rsp12 - 8 + 8;
                r9_4 = r9_4;
                *reinterpret_cast<uint32_t*>(&r8_2) = 0;
                *reinterpret_cast<int32_t*>(&r8_2 + 4) = 0;
                r11_15 = reinterpret_cast<void**>(0x5555555555555555);
                r10_16 = reinterpret_cast<void**>(0xaaaaaaaaaaaaaaab);
            }
        }
        addr_4ef9_19:
        *reinterpret_cast<int32_t*>(&rbx29) = static_cast<int32_t>(r13_14 + 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx29) + 4) = 0;
        r13_14 = rbx29;
        rbx18 = reinterpret_cast<struct s48*>((rbx29 << 4) + reinterpret_cast<unsigned char>(0xd640));
        while (1) {
            *reinterpret_cast<int32_t*>(&r15_19) = static_cast<int32_t>(r13_14 - 1);
            rcx30 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp8) * reinterpret_cast<unsigned char>(r10_16));
            if (reinterpret_cast<unsigned char>(r11_15) >= reinterpret_cast<unsigned char>(rcx30)) {
                do {
                    v31 = rcx30;
                    v32 = r11_15;
                    factor_insert_multiplicity(r9_4, r12_13, 1);
                    rsp12 = rsp12 - 8 + 8;
                    r10_16 = r10_16;
                    r11_15 = v32;
                    r9_4 = r9_4;
                    rbp8 = v31;
                    rcx30 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v31) * reinterpret_cast<unsigned char>(r10_16));
                    r8_2 = r8_2;
                } while (reinterpret_cast<unsigned char>(r11_15) >= reinterpret_cast<unsigned char>(rcx30));
                rdx33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx18->f0) * reinterpret_cast<unsigned char>(rbp8));
                if (reinterpret_cast<unsigned char>(rbx18->f8) < reinterpret_cast<unsigned char>(rdx33)) 
                    goto addr_4f29_23;
                goto addr_5050_25;
            }
            rdx33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx18->f0) * reinterpret_cast<unsigned char>(rbp8));
            if (reinterpret_cast<unsigned char>(rbx18->f8) >= reinterpret_cast<unsigned char>(rdx33)) {
                addr_5050_25:
                *reinterpret_cast<uint32_t*>(&rcx34) = *reinterpret_cast<uint32_t*>(&r13_14);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rcx35) = rcx34->f10340;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx35) + 4) = 0;
                rcx36 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx35) + reinterpret_cast<unsigned char>(r12_13));
            } else {
                addr_4f29_23:
                rdx37 = reinterpret_cast<void**>(rbx18->f10 * reinterpret_cast<unsigned char>(rbp8));
                if (reinterpret_cast<unsigned char>(rdx37) <= reinterpret_cast<unsigned char>(rbx18->f18)) 
                    goto addr_50b0_27; else 
                    goto addr_4f3b_28;
            }
            do {
                rbp8 = rdx33;
                v32 = r8_2;
                factor_insert_multiplicity(r9_4, rcx36, 1);
                rsp12 = rsp12 - 8 + 8;
                r9_4 = r9_4;
                rcx36 = rcx36;
                r8_2 = v32;
                rdx33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx18->f0) * reinterpret_cast<unsigned char>(rbp8));
            } while (reinterpret_cast<unsigned char>(rdx33) <= reinterpret_cast<unsigned char>(rbx18->f8));
            rdx37 = reinterpret_cast<void**>(rbx18->f10 * reinterpret_cast<unsigned char>(rbp8));
            if (reinterpret_cast<unsigned char>(rdx37) > reinterpret_cast<unsigned char>(rbx18->f18)) {
                addr_4f3b_28:
                rdx38 = reinterpret_cast<void**>(rbx18->f20 * reinterpret_cast<unsigned char>(rbp8));
                if (reinterpret_cast<unsigned char>(rdx38) <= reinterpret_cast<unsigned char>(rbx18->f28)) {
                    addr_5120_31:
                    *reinterpret_cast<int32_t*>(&rcx39) = static_cast<int32_t>(r13_14 + 2);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx39) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi40) = *reinterpret_cast<uint32_t*>(&r13_14);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi40) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi41) = rsi40->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi41) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rcx42) = rcx39->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx42) + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rsi43) = static_cast<int32_t>(r13_14 + 1);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi43) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi44) = rsi43->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi44) + 4) = 0;
                    rcx45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx42) + reinterpret_cast<int64_t>(rsi41) + reinterpret_cast<unsigned char>(r12_13) + reinterpret_cast<uint64_t>(rsi44));
                } else {
                    addr_4f4d_32:
                    rdx46 = reinterpret_cast<void**>(rbx18->f30 * reinterpret_cast<unsigned char>(rbp8));
                    if (reinterpret_cast<unsigned char>(rdx46) <= reinterpret_cast<unsigned char>(rbx18->f38)) 
                        goto addr_5198_33; else 
                        goto addr_4f5f_34;
                }
            } else {
                goto addr_50b0_27;
            }
            do {
                rbp8 = rdx38;
                v32 = r8_2;
                v47 = rcx45;
                factor_insert_multiplicity(r9_4, rcx45, 1);
                rsp12 = rsp12 - 8 + 8;
                r9_4 = r9_4;
                rcx45 = v47;
                r8_2 = v32;
                rdx38 = reinterpret_cast<void**>(rbx18->f20 * reinterpret_cast<unsigned char>(rbp8));
            } while (reinterpret_cast<unsigned char>(rdx38) <= reinterpret_cast<unsigned char>(rbx18->f28));
            rdx46 = reinterpret_cast<void**>(rbx18->f30 * reinterpret_cast<unsigned char>(rbp8));
            if (reinterpret_cast<unsigned char>(rdx46) > reinterpret_cast<unsigned char>(rbx18->f38)) {
                addr_4f5f_34:
                rdx48 = reinterpret_cast<void**>(rbx18->f40 * reinterpret_cast<unsigned char>(rbp8));
                if (reinterpret_cast<unsigned char>(rdx48) <= reinterpret_cast<unsigned char>(rbx18->f48)) {
                    rbp8 = rdx48;
                    ecx49 = static_cast<uint32_t>(r13_14 + 5);
                    while (1) {
                        edx50 = *reinterpret_cast<uint32_t*>(&r13_14);
                        rsi51 = r12_13;
                        do {
                            *reinterpret_cast<uint32_t*>(&rdi52) = edx50;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi52) + 4) = 0;
                            ++edx50;
                            *reinterpret_cast<uint32_t*>(&rdi53) = rdi52->f10340;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi53) + 4) = 0;
                            rsi51 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi51) + reinterpret_cast<uint64_t>(rdi53));
                        } while (edx50 != ecx49);
                        *reinterpret_cast<uint32_t*>(&v32) = ecx49;
                        v47 = r8_2;
                        factor_insert_multiplicity(r9_4, rsi51, 1);
                        rsp12 = rsp12 - 8 + 8;
                        r9_4 = r9_4;
                        r8_2 = v47;
                        ecx49 = *reinterpret_cast<uint32_t*>(&v32);
                        rdx54 = reinterpret_cast<void**>(rbx18->f40 * reinterpret_cast<unsigned char>(rbp8));
                        if (reinterpret_cast<unsigned char>(rdx54) > reinterpret_cast<unsigned char>(rbx18->f48)) 
                            break;
                        rbp8 = rdx54;
                    }
                }
            } else {
                goto addr_5198_33;
            }
            rdx55 = reinterpret_cast<void**>(rbx18->f50 * reinterpret_cast<unsigned char>(rbp8));
            if (reinterpret_cast<unsigned char>(rdx55) <= reinterpret_cast<unsigned char>(rbx18->f58)) {
                rbp8 = rdx55;
                ecx56 = static_cast<uint32_t>(r13_14 + 6);
                while (1) {
                    edx57 = *reinterpret_cast<uint32_t*>(&r13_14);
                    rsi58 = r12_13;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdi59) = edx57;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi59) + 4) = 0;
                        ++edx57;
                        *reinterpret_cast<uint32_t*>(&rdi60) = rdi59->f10340;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi60) + 4) = 0;
                        rsi58 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi58) + reinterpret_cast<uint64_t>(rdi60));
                    } while (edx57 != ecx56);
                    *reinterpret_cast<uint32_t*>(&v32) = ecx56;
                    v47 = r8_2;
                    factor_insert_multiplicity(r9_4, rsi58, 1);
                    rsp12 = rsp12 - 8 + 8;
                    r9_4 = r9_4;
                    r8_2 = v47;
                    ecx56 = *reinterpret_cast<uint32_t*>(&v32);
                    rdx61 = reinterpret_cast<void**>(rbx18->f50 * reinterpret_cast<unsigned char>(rbp8));
                    if (reinterpret_cast<unsigned char>(rdx61) > reinterpret_cast<unsigned char>(rbx18->f58)) 
                        break;
                    rbp8 = rdx61;
                }
            }
            rdx62 = reinterpret_cast<void**>(rbx18->f60 * reinterpret_cast<unsigned char>(rbp8));
            if (reinterpret_cast<unsigned char>(rdx62) <= reinterpret_cast<unsigned char>(rbx18->f68)) {
                rbp8 = rdx62;
                ecx63 = static_cast<uint32_t>(r13_14 + 7);
                while (1) {
                    edx64 = *reinterpret_cast<uint32_t*>(&r13_14);
                    rsi65 = r12_13;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdi66) = edx64;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi66) + 4) = 0;
                        ++edx64;
                        *reinterpret_cast<uint32_t*>(&rdi67) = rdi66->f10340;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi67) + 4) = 0;
                        rsi65 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi65) + reinterpret_cast<uint64_t>(rdi67));
                    } while (edx64 != ecx63);
                    *reinterpret_cast<uint32_t*>(&v32) = ecx63;
                    v47 = r8_2;
                    factor_insert_multiplicity(r9_4, rsi65, 1);
                    rsp12 = rsp12 - 8 + 8;
                    r9_4 = r9_4;
                    r8_2 = v47;
                    ecx63 = *reinterpret_cast<uint32_t*>(&v32);
                    rdx68 = reinterpret_cast<void**>(rbx18->f60 * reinterpret_cast<unsigned char>(rbp8));
                    if (reinterpret_cast<unsigned char>(rdx68) > reinterpret_cast<unsigned char>(rbx18->f68)) 
                        break;
                    rbp8 = rdx68;
                }
            }
            *reinterpret_cast<int32_t*>(&rax69) = *reinterpret_cast<int32_t*>(&r15_19);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax69) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rax70) = *reinterpret_cast<unsigned char*>(0x10080 + rax69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax70) + 4) = 0;
            r12_13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_13) + reinterpret_cast<uint64_t>(rax70));
            if (reinterpret_cast<unsigned char>(rbp8) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r12_13) * reinterpret_cast<unsigned char>(r12_13))) 
                break;
            *reinterpret_cast<uint32_t*>(&rax71) = static_cast<uint32_t>(r13_14 + 7);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax71) + 4) = 0;
            rbx18 = reinterpret_cast<struct s48*>(reinterpret_cast<uint64_t>(rbx18) + 0x80);
            *reinterpret_cast<uint32_t*>(&r13_14) = *reinterpret_cast<uint32_t*>(&r13_14) + 8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_14) + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&rax71) > 0x29b) 
                break;
            rax72 = reinterpret_cast<struct s56*>((rax71 << 4) + reinterpret_cast<unsigned char>(0xd640));
            r10_16 = rax72->f0;
            r11_15 = rax72->f8;
            continue;
            addr_5198_33:
            rbp8 = rdx46;
            ecx73 = static_cast<uint32_t>(r13_14 + 4);
            while (1) {
                edx74 = *reinterpret_cast<uint32_t*>(&r13_14);
                rsi75 = r12_13;
                do {
                    *reinterpret_cast<uint32_t*>(&rdi76) = edx74;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi76) + 4) = 0;
                    ++edx74;
                    *reinterpret_cast<uint32_t*>(&rdi77) = rdi76->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi77) + 4) = 0;
                    rsi75 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi75) + reinterpret_cast<uint64_t>(rdi77));
                } while (ecx73 != edx74);
                *reinterpret_cast<uint32_t*>(&v32) = ecx73;
                factor_insert_multiplicity(r9_4, rsi75, 1);
                rsp12 = rsp12 - 8 + 8;
                r9_4 = r9_4;
                r8_2 = r8_2;
                ecx73 = *reinterpret_cast<uint32_t*>(&v32);
                rdx78 = reinterpret_cast<void**>(rbx18->f30 * reinterpret_cast<unsigned char>(rbp8));
                if (reinterpret_cast<unsigned char>(rdx78) > reinterpret_cast<unsigned char>(rbx18->f38)) 
                    goto addr_4f5f_34;
                rbp8 = rdx78;
            }
            addr_50b0_27:
            *reinterpret_cast<int32_t*>(&rcx79) = static_cast<int32_t>(r13_14 + 1);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx79) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rsi80) = *reinterpret_cast<uint32_t*>(&r13_14);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi80) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx81) = rcx79->f10340;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx81) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rsi82) = rsi80->f10340;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi82) + 4) = 0;
            rcx83 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx81) + reinterpret_cast<int64_t>(rsi82) + reinterpret_cast<unsigned char>(r12_13));
            do {
                rbp8 = rdx37;
                v32 = r8_2;
                factor_insert_multiplicity(r9_4, rcx83, 1);
                rsp12 = rsp12 - 8 + 8;
                r9_4 = r9_4;
                rcx83 = rcx83;
                r8_2 = v32;
                rdx37 = reinterpret_cast<void**>(rbx18->f10 * reinterpret_cast<unsigned char>(rbp8));
            } while (reinterpret_cast<unsigned char>(rdx37) <= reinterpret_cast<unsigned char>(rbx18->f18));
            rdx38 = reinterpret_cast<void**>(rbx18->f20 * reinterpret_cast<unsigned char>(rbp8));
            if (reinterpret_cast<unsigned char>(rdx38) > reinterpret_cast<unsigned char>(rbx18->f28)) 
                goto addr_4f4d_32;
            goto addr_5120_31;
        }
        addr_5200_14:
        if (!r8_2) 
            goto addr_5209_66;
        rdi84 = r8_2;
        v85 = r9_4;
        v86 = r8_2;
        al87 = prime2_p(rdi84, rbp8);
        rsp12 = rsp12 - 8 + 8;
        if (al87) 
            goto addr_53a9_68;
        rbx11 = v10;
        r13_7 = v6;
        r15_88 = v86;
        rbp89 = rbp8;
        rsp90 = rsp12 + 56 + 8 + 8 + 8 + 8 + 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x88;
        v91 = v85;
        rax92 = g28;
        v93 = rax92;
        rcx94 = reinterpret_cast<unsigned char>(v85) - (reinterpret_cast<unsigned char>(v85) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v85) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(v85) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v86) < reinterpret_cast<unsigned char>(2)))))));
        *reinterpret_cast<int32_t*>(&rdx95) = 0;
        *reinterpret_cast<int32_t*>(&rdx95 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx96) = *reinterpret_cast<uint32_t*>(&rcx94) & 64;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx96) + 4) = 0;
        rcx97 = reinterpret_cast<uint64_t>(rcx96 + 63);
        cf98 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v86) < reinterpret_cast<unsigned char>(2));
        *reinterpret_cast<unsigned char*>(&rdx95) = cf98;
        rax99 = reinterpret_cast<void**>(-static_cast<uint64_t>(reinterpret_cast<uint1_t>(0 < static_cast<uint64_t>(cf98))) + 1);
        do {
            rsi100 = rdx95;
            rdx95 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx95) + reinterpret_cast<unsigned char>(rdx95));
            rax99 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax99) + reinterpret_cast<unsigned char>(rax99)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rsi100) >> 63));
            if (reinterpret_cast<unsigned char>(r15_88) < reinterpret_cast<unsigned char>(rax99) || r15_88 == rax99 && reinterpret_cast<unsigned char>(rbp89) <= reinterpret_cast<unsigned char>(rdx95)) {
                cf101 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx95) < reinterpret_cast<unsigned char>(rbp89));
                rdx95 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx95) - reinterpret_cast<unsigned char>(rbp89));
                rax99 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax99) - (reinterpret_cast<unsigned char>(r15_88) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax99) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r15_88) + static_cast<uint64_t>(cf101))))));
            }
            cf102 = rcx97 < 1;
            --rcx97;
        } while (!cf102);
        v103 = rdx95;
        v104 = rax99;
        tmp64_105 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx95) + reinterpret_cast<unsigned char>(rdx95));
        rax106 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax99) + reinterpret_cast<unsigned char>(rax99) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_105) < reinterpret_cast<unsigned char>(rdx95))));
        r12_107 = tmp64_105;
        r10_108 = rax106;
        if (reinterpret_cast<unsigned char>(rax106) > reinterpret_cast<unsigned char>(r15_88) || rax106 == r15_88 && reinterpret_cast<unsigned char>(tmp64_105) >= reinterpret_cast<unsigned char>(rbp89)) {
            r12_107 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_105) - reinterpret_cast<unsigned char>(rbp89));
            r10_108 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax106) - (reinterpret_cast<unsigned char>(r15_88) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax106) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r15_88) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_105) < reinterpret_cast<unsigned char>(rbp89))))))));
        }
        v109 = r10_108;
        v110 = r10_108;
        if (r15_88) 
            goto addr_468c_77;
        if (rbp89 == 1) 
            goto addr_4ad5_79;
        addr_468c_77:
        v111 = r12_107;
        r13_7 = r15_88;
        *reinterpret_cast<int32_t*>(&rbx11) = 1;
        *reinterpret_cast<int32_t*>(&rbx11 + 4) = 0;
        v112 = reinterpret_cast<void**>(1);
        r14_113 = reinterpret_cast<void**>(rsp90 + 0x70);
        r15_114 = r12_107;
        v115 = rsp90 + 0x68;
        while (1) {
            r9_116 = r13_7;
            r13_7 = rbx11;
            rax117 = reinterpret_cast<unsigned char>(rbp89) >> 1;
            rbx11 = rbp89;
            *reinterpret_cast<uint32_t*>(&rax118) = *reinterpret_cast<uint32_t*>(&rax117) & 0x7f;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax118) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rax119) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax118);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax119) + 4) = 0;
            rdx120 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax119) + reinterpret_cast<int64_t>(rax119) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax119) * reinterpret_cast<int64_t>(rax119) * reinterpret_cast<unsigned char>(rbp89)));
            rax121 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx120) + reinterpret_cast<uint64_t>(rdx120) - reinterpret_cast<uint64_t>(rdx120) * reinterpret_cast<uint64_t>(rdx120) * reinterpret_cast<unsigned char>(rbp89));
            rax122 = rbp89;
            rbp123 = r10_108;
            *reinterpret_cast<uint32_t*>(&rax124) = *reinterpret_cast<uint32_t*>(&rax122) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax124) + 4) = 0;
            v125 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax121) + reinterpret_cast<uint64_t>(rax121) - reinterpret_cast<uint64_t>(rax121) * reinterpret_cast<uint64_t>(rax121) * reinterpret_cast<unsigned char>(rbp89));
            v126 = rax124;
            while (1) {
                rax127 = mulredc2(r14_113, rbp123, r12_107, rbp123, r12_107, r9_116, rbx11, v104);
                tmp64_128 = rax127 + 1;
                rdx129 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v31) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_128) < reinterpret_cast<unsigned char>(rax127))));
                v130 = rdx129;
                r12_107 = tmp64_128;
                rbp123 = rdx129;
                if (reinterpret_cast<unsigned char>(rdx129) > reinterpret_cast<unsigned char>(r9_116) || rdx129 == r9_116 && reinterpret_cast<unsigned char>(tmp64_128) >= reinterpret_cast<unsigned char>(rbx11)) {
                    rdx131 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx129) - (reinterpret_cast<unsigned char>(r9_116) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx129) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r9_116) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_128) < reinterpret_cast<unsigned char>(rbx11))))))));
                    v130 = rdx131;
                    r12_107 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_128) - reinterpret_cast<unsigned char>(rbx11));
                    rbp123 = rdx131;
                }
                r8_132 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v111) - reinterpret_cast<unsigned char>(r12_107));
                rcx133 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v109) - (reinterpret_cast<unsigned char>(rbp123) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v109) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp123) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v111) < reinterpret_cast<unsigned char>(r12_107))))))));
                if (reinterpret_cast<signed char>(rcx133) < reinterpret_cast<signed char>(0)) {
                    tmp64_134 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_132) + reinterpret_cast<unsigned char>(rbx11));
                    cf135 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_134) < reinterpret_cast<unsigned char>(r8_132));
                    r8_132 = tmp64_134;
                    rcx133 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx133) + reinterpret_cast<unsigned char>(r9_116) + static_cast<uint64_t>(cf135));
                }
                rax136 = mulredc2(r14_113, v104, v103, rcx133, r8_132, r9_116, rbx11, v104);
                v103 = rax136;
                v104 = v31;
                rax137 = r13_7;
                *reinterpret_cast<uint32_t*>(&rax138) = *reinterpret_cast<uint32_t*>(&rax137) & 31;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax138) + 4) = 0;
                rsp90 = rsp90 - 8 - 8 - 8 + 8 + 8 + 8 - 8 - 8 - 8 + 8 + 8 + 8;
                r9_116 = r9_116;
                if (rax138 == 1) {
                    if (!v126) 
                        goto addr_4cba_87;
                    if (reinterpret_cast<unsigned char>(v103) | reinterpret_cast<unsigned char>(v104)) 
                        goto addr_48c0_89;
                } else {
                    addr_47d9_90:
                    --r13_7;
                    if (r13_7) 
                        continue; else 
                        goto addr_47e3_91;
                }
                v32 = r9_116;
                rax139 = rbx11;
                if (r9_116) 
                    break;
                addr_48ee_93:
                if (!reinterpret_cast<int1_t>(rax139 == 1)) 
                    goto addr_4920_94;
                v110 = rbp123;
                r15_114 = r12_107;
                goto addr_47d9_90;
                addr_48c0_89:
                rax139 = gcd2_odd_part_0(v115, v104, v103, r9_116, rbx11, r9_116);
                rsp90 = rsp90 - 8 + 8;
                r9_116 = r9_116;
                if (v32) 
                    goto addr_4920_94; else 
                    goto addr_48ee_93;
                addr_47e3_91:
                v109 = rbp123;
                r15_114 = r12_107;
                v140 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v112) + reinterpret_cast<unsigned char>(v112));
                if (v112) {
                    v141 = r12_107;
                    rdx142 = r12_107;
                    r15_143 = r13_7;
                    r12_144 = v125;
                    rsi145 = rbp123;
                    rbp146 = r9_116;
                    do {
                        rax147 = mulredc2(r14_113, rsi145, rdx142, rsi145, rdx142, rbp146, rbx11, r12_144);
                        tmp64_148 = rax147 + 1;
                        rcx149 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v31) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_148) < reinterpret_cast<unsigned char>(rax147))));
                        rdx142 = tmp64_148;
                        rsi145 = rcx149;
                        rsp90 = rsp90 - 8 - 8 - 8 + 8 + 8 + 8;
                        if (reinterpret_cast<unsigned char>(rcx149) > reinterpret_cast<unsigned char>(rbp146) || rcx149 == rbp146 && reinterpret_cast<unsigned char>(tmp64_148) >= reinterpret_cast<unsigned char>(rbx11)) {
                            rdx142 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_148) - reinterpret_cast<unsigned char>(rbx11));
                            rsi145 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx149) - (reinterpret_cast<unsigned char>(rbp146) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx149) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp146) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_148) < reinterpret_cast<unsigned char>(rbx11))))))));
                        }
                        ++r15_143;
                    } while (v112 != r15_143);
                    r12_107 = v141;
                    r9_116 = rbp146;
                    r15_114 = rdx142;
                    rbp123 = rsi145;
                }
                r13_7 = v112;
                v111 = r12_107;
                r12_107 = r15_114;
                v110 = rbp123;
                v112 = v140;
            }
            addr_4920_94:
            v150 = r12_107;
            r11_151 = r15_114;
            r10_152 = v110;
            v153 = r13_7;
            r15_154 = r14_113;
            r13_7 = rbx11;
            r12_155 = v111;
            r14_113 = v125;
            rbp156 = v115;
            rbx11 = r9_116;
            do {
                r9_157 = rbx11;
                rax158 = mulredc2(r15_154, r10_152, r11_151, r10_152, r11_151, r9_157, r13_7, r14_113);
                tmp64_159 = rax158 + 1;
                rdx160 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v31) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_159) < reinterpret_cast<unsigned char>(rax158))));
                v110 = rdx160;
                r11_151 = tmp64_159;
                rcx28 = r13_7;
                r10_152 = rdx160;
                rsp90 = rsp90 - 8 - 8 - 8 + 8 + 8 + 8;
                if (reinterpret_cast<unsigned char>(rdx160) > reinterpret_cast<unsigned char>(rbx11) || rdx160 == rbx11 && reinterpret_cast<unsigned char>(tmp64_159) >= reinterpret_cast<unsigned char>(r13_7)) {
                    rdx161 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx160) - (reinterpret_cast<unsigned char>(rbx11) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx160) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbx11) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_159) < reinterpret_cast<unsigned char>(r13_7))))))));
                    v110 = rdx161;
                    r11_151 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_159) - reinterpret_cast<unsigned char>(r13_7));
                    r10_152 = rdx161;
                }
                rdx162 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_155) - reinterpret_cast<unsigned char>(r11_151));
                rsi163 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v109) - (reinterpret_cast<unsigned char>(r10_152) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v109) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r10_152) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_155) < reinterpret_cast<unsigned char>(r11_151))))))));
                if (reinterpret_cast<signed char>(rsi163) < reinterpret_cast<signed char>(0)) {
                    tmp64_164 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx162) + reinterpret_cast<unsigned char>(r13_7));
                    cf165 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_164) < reinterpret_cast<unsigned char>(rdx162));
                    rdx162 = tmp64_164;
                    rsi163 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi163) + reinterpret_cast<unsigned char>(rbx11) + static_cast<uint64_t>(cf165));
                }
                if (reinterpret_cast<unsigned char>(rsi163) | reinterpret_cast<unsigned char>(rdx162)) {
                    rcx28 = rbx11;
                    rax166 = gcd2_odd_part_0(rbp156, rsi163, rdx162, rcx28, r13_7, r9_157);
                    rsp90 = rsp90 - 8 + 8;
                    rdi167 = v32;
                    if (rdi167) 
                        goto addr_4a05_109;
                } else {
                    rdi167 = rbx11;
                    v32 = rbx11;
                    rax166 = r13_7;
                    if (rdi167) 
                        goto addr_4a05_109;
                }
            } while (reinterpret_cast<int1_t>(rax166 == 1));
            r8_168 = rax166;
            rax169 = reinterpret_cast<unsigned char>(rax166) >> 1;
            *reinterpret_cast<uint32_t*>(&rax170) = *reinterpret_cast<uint32_t*>(&rax169) & 0x7f;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax170) + 4) = 0;
            r13_171 = rbx11;
            r14_113 = r15_154;
            *reinterpret_cast<uint32_t*>(&rax172) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax170);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax172) + 4) = 0;
            rbx11 = v153;
            rdx173 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax172) + reinterpret_cast<int64_t>(rax172) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax172) * reinterpret_cast<int64_t>(rax172) * reinterpret_cast<unsigned char>(r8_168)));
            rdx174 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdx173) * reinterpret_cast<uint64_t>(rdx173) * reinterpret_cast<unsigned char>(r8_168));
            rax175 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx173) + reinterpret_cast<uint64_t>(rdx173) - reinterpret_cast<unsigned char>(rdx174));
            rcx28 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax175) + reinterpret_cast<uint64_t>(rax175) - reinterpret_cast<uint64_t>(rax175) * reinterpret_cast<uint64_t>(rax175) * reinterpret_cast<unsigned char>(r8_168));
            rbp89 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_7) * reinterpret_cast<unsigned char>(rcx28));
            if (reinterpret_cast<unsigned char>(r13_171) < reinterpret_cast<unsigned char>(r8_168)) {
                *reinterpret_cast<int32_t*>(&r13_7) = 0;
                *reinterpret_cast<int32_t*>(&r13_7 + 4) = 0;
            } else {
                rdx174 = __intrinsic();
                r9_157 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r13_171) - reinterpret_cast<unsigned char>(rdx174)) * reinterpret_cast<unsigned char>(rcx28));
                r13_7 = r9_157;
            }
            if (reinterpret_cast<unsigned char>(r8_168) <= reinterpret_cast<unsigned char>(1) || reinterpret_cast<unsigned char>(r8_168) > reinterpret_cast<unsigned char>(0x17ded78) && (al176 = prime_p_part_0(r8_168, rsi163, rdx174, rcx28), rsp90 = rsp90 - 8 + 8, r8_168 = r8_168, al176 == 0)) {
                rdx5 = v91;
                rsi9 = reinterpret_cast<void**>(2);
                factor_using_pollard_rho(r8_168, 2, rdx5, rcx28);
                rsp90 = rsp90 - 8 + 8;
            } else {
                *reinterpret_cast<int32_t*>(&rdx5) = 1;
                *reinterpret_cast<int32_t*>(&rdx5 + 4) = 0;
                rsi9 = r8_168;
                factor_insert_multiplicity(v91, rsi9, 1);
                rsp90 = rsp90 - 8 + 8;
            }
            if (!r13_7) 
                goto addr_4aa0_119;
            rsi9 = rbp89;
            rdi3 = r13_7;
            al177 = prime2_p(rdi3, rsi9);
            rsp90 = rsp90 - 8 + 8;
            if (al177) 
                goto addr_4c8b_121;
            rax178 = mod2(rsp90 + 80, v130, v150, r13_7, rbp89, r9_157);
            rsp179 = reinterpret_cast<void*>(rsp90 - 8 + 8);
            r12_107 = rax178;
            rax180 = mod2(reinterpret_cast<int64_t>(rsp179) + 88, v109, v111, r13_7, rbp89, r9_157);
            rsp181 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp179) - 8 + 8);
            v111 = rax180;
            rax182 = mod2(reinterpret_cast<int64_t>(rsp181) + 96, v110, r11_151, r13_7, rbp89, r9_157);
            rsp90 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp181) - 8 + 8);
            r10_108 = v130;
            r15_114 = rax182;
        }
        addr_4a05_109:
        if (rbx11 != rdi167) 
            goto addr_4a19_123;
        if (rax166 == r13_7) 
            goto addr_4ce0_125;
        addr_4a19_123:
        rbx11 = reinterpret_cast<void**>(0xd5c0);
        rsi9 = rax166;
        v112 = rax166;
        rax183 = reinterpret_cast<unsigned char>(rax166) >> 1;
        *reinterpret_cast<uint32_t*>(&rax184) = *reinterpret_cast<uint32_t*>(&rax183) & 0x7f;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax184) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax185) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(0xd5c0) + reinterpret_cast<uint64_t>(rax184));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax185) + 4) = 0;
        rdx186 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax185) + reinterpret_cast<int64_t>(rax185) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax185) * reinterpret_cast<int64_t>(rax185) * reinterpret_cast<unsigned char>(rax166)));
        rax187 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx186) + reinterpret_cast<uint64_t>(rdx186) - reinterpret_cast<uint64_t>(rdx186) * reinterpret_cast<uint64_t>(rdx186) * reinterpret_cast<unsigned char>(rax166));
        rdx5 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax187) + reinterpret_cast<uint64_t>(rax187) - reinterpret_cast<uint64_t>(rax187) * reinterpret_cast<uint64_t>(rax187) * reinterpret_cast<unsigned char>(rax166));
        rbp89 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_7) * reinterpret_cast<unsigned char>(rdx5));
        al188 = prime2_p(rdi167, rsi9);
        if (!al188) 
            goto addr_4c4f_126;
        if (!v32) 
            goto addr_4ca3_128;
        rdi3 = v91;
        if (!*reinterpret_cast<void***>(rdi3 + 8)) 
            goto addr_4a94_130;
        addr_4cfe_131:
        factor_insert_large_part_0();
        continue;
        addr_4c8b_121:
        if (*reinterpret_cast<void***>(v91 + 8)) 
            goto addr_4cfe_131; else 
            goto addr_4c97_132;
        addr_4edd_15:
        *reinterpret_cast<uint32_t*>(&rax189) = *reinterpret_cast<uint32_t*>(&r13_14);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax189) + 4) = 0;
        rax190 = reinterpret_cast<struct s61*>((rax189 << 4) + 0xd640);
        r10_16 = rax190->f0;
        r11_15 = rax190->f8;
        goto addr_4ef9_19;
    }
    goto addr_4d41_133;
    addr_5209_66:
    if (reinterpret_cast<unsigned char>(rbp8) <= reinterpret_cast<unsigned char>(1)) {
        addr_4d41_133:
        return;
    } else {
        al191 = prime2_p(0, rbp8);
        r9_192 = r9_4;
        if (al191) {
            rsi193 = rbp8;
            rdi194 = r9_192;
        } else {
            rdi195 = rbp8;
            rdx196 = r9_192;
            *reinterpret_cast<int32_t*>(&rsi197) = 1;
            *reinterpret_cast<int32_t*>(&rsi197 + 4) = 0;
            goto addr_5740_137;
        }
    }
    addr_2f20_138:
    *reinterpret_cast<uint32_t*>(&rax198) = *reinterpret_cast<unsigned char*>(rdi194 + 0xfa);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax198) + 4) = 0;
    r8_199 = rsi193;
    r11_200 = reinterpret_cast<void***>(rdi194 + 16);
    r10_201 = reinterpret_cast<signed char*>(rdi194 + 0xe0);
    r9d202 = *reinterpret_cast<uint32_t*>(&rax198);
    if (!*reinterpret_cast<uint32_t*>(&rax198)) 
        goto addr_2fa1_139;
    ebx203 = static_cast<int32_t>(rax198 - 1);
    rcx204 = reinterpret_cast<void*>(static_cast<int64_t>(ebx203));
    rax205 = rcx204;
    do {
        *reinterpret_cast<int32_t*>(&rsi206) = *reinterpret_cast<int32_t*>(&rax205);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi206) + 4) = 0;
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi194 + reinterpret_cast<uint64_t>(rax205) * 8) + 16)) <= reinterpret_cast<unsigned char>(r8_199)) 
            break;
        rax205 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax205) - 1);
        *reinterpret_cast<int32_t*>(&rsi206) = *reinterpret_cast<int32_t*>(&rax205);
    } while (*reinterpret_cast<int32_t*>(&rax205) != -1);
    goto addr_2f80_143;
    if (*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi194 + reinterpret_cast<uint64_t>(rax205) * 8) + 16) == r8_199) {
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_201) + reinterpret_cast<uint64_t>(rax205)) = reinterpret_cast<signed char>(*reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_201) + reinterpret_cast<uint64_t>(rax205)) + 1);
        goto v1;
    }
    rax207 = reinterpret_cast<void*>(static_cast<int64_t>(static_cast<int32_t>(rsi206 + 1)));
    r11_200 = r11_200 + reinterpret_cast<uint64_t>(rax207) * 8;
    r10_201 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_201) + reinterpret_cast<uint64_t>(rax207));
    if (*reinterpret_cast<int32_t*>(&rsi206) >= ebx203) {
        addr_2fa1_139:
        r9d208 = r9d202 + 1;
        *r11_200 = r8_199;
        *r10_201 = 1;
        *reinterpret_cast<unsigned char*>(rdi194 + 0xfa) = *reinterpret_cast<unsigned char*>(&r9d208);
        goto v1;
    }
    do {
        addr_2f80_143:
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi194 + reinterpret_cast<uint64_t>(rcx204) * 8) + 24) = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi194 + reinterpret_cast<uint64_t>(rcx204) * 8) + 16);
        eax209 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi194) + reinterpret_cast<uint64_t>(rcx204) + 0xe0);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi194) + reinterpret_cast<uint64_t>(rcx204) + 0xe1) = *reinterpret_cast<signed char*>(&eax209);
        rcx204 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx204) - 1);
    } while (*reinterpret_cast<int32_t*>(&rsi206) < *reinterpret_cast<int32_t*>(&rcx204));
    goto addr_2fa1_139;
    addr_5740_137:
    v85 = rsi197;
    v47 = rdx196;
    if (reinterpret_cast<unsigned char>(rdi195) <= reinterpret_cast<unsigned char>(1)) {
        addr_5b07_148:
        fun_2810("(1) < (n)", "src/factor.c", 0x5be, "factor_using_pollard_rho", r8_2, r9_192);
    } else {
        r12_210 = rdi195;
        do {
            rcx211 = r12_210;
            esi212 = 64;
            *reinterpret_cast<int32_t*>(&rax213) = 1;
            *reinterpret_cast<int32_t*>(&rax213 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx214) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx214) + 4) = 0;
            rdi215 = reinterpret_cast<void*>(0);
            do {
                r8_2 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx211) << 63);
                rcx211 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx211) >> 1);
                rdx214 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx214) >> 1 | reinterpret_cast<unsigned char>(r8_2));
                if (reinterpret_cast<unsigned char>(rcx211) < reinterpret_cast<unsigned char>(rax213) || rcx211 == rax213 && reinterpret_cast<uint64_t>(rdx214) <= reinterpret_cast<uint64_t>(rdi215)) {
                    cf216 = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi215) < reinterpret_cast<uint64_t>(rdx214));
                    rdi215 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi215) - reinterpret_cast<uint64_t>(rdx214));
                    rax213 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax213) - (reinterpret_cast<unsigned char>(rcx211) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax213) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx211) + static_cast<uint64_t>(cf216))))));
                }
                --esi212;
            } while (esi212);
            *reinterpret_cast<int32_t*>(&rbp217) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp217) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r15_218) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_218) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r14_219) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_219) + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rbp217) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_210) - reinterpret_cast<uint64_t>(rdi215) > reinterpret_cast<uint64_t>(rdi215));
            rbp220 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbp217) & reinterpret_cast<unsigned char>(r12_210)) + (reinterpret_cast<uint64_t>(rdi215) + reinterpret_cast<uint64_t>(rdi215) - reinterpret_cast<unsigned char>(r12_210)));
            rcx221 = rbp220;
            while (reinterpret_cast<unsigned char>(v85) < reinterpret_cast<unsigned char>(r12_210)) {
                r11_222 = rcx221;
                rax223 = reinterpret_cast<unsigned char>(r12_210) >> 1;
                *reinterpret_cast<uint32_t*>(&rax224) = *reinterpret_cast<uint32_t*>(&rax223) & 0x7f;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax224) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax225) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax224);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax225) + 4) = 0;
                rdx226 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax225) + reinterpret_cast<int64_t>(rax225) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax225) * reinterpret_cast<int64_t>(rax225) * reinterpret_cast<unsigned char>(r12_210)));
                rax227 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx226) + reinterpret_cast<uint64_t>(rdx226) - reinterpret_cast<uint64_t>(rdx226) * reinterpret_cast<uint64_t>(rdx226) * reinterpret_cast<unsigned char>(r12_210));
                r8_228 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax227) + reinterpret_cast<uint64_t>(rax227) - reinterpret_cast<uint64_t>(rax227) * reinterpret_cast<uint64_t>(rax227) * reinterpret_cast<unsigned char>(r12_210));
                r10_229 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_210) - reinterpret_cast<unsigned char>(v85));
                r9_230 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v85) - reinterpret_cast<unsigned char>(r12_210));
                while (1) {
                    rax231 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rax231 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax231) + reinterpret_cast<unsigned char>(r12_210));
                    }
                    rbp220 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rbp220) - (reinterpret_cast<unsigned char>(rbp220) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp220) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp220) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax231) < reinterpret_cast<uint64_t>(r10_229))))))) & reinterpret_cast<unsigned char>(r12_210)) + (reinterpret_cast<uint64_t>(rax231) + reinterpret_cast<uint64_t>(r9_230)));
                    r13_232 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        r13_232 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_232) + reinterpret_cast<unsigned char>(r12_210));
                    }
                    rax233 = r14_219;
                    *reinterpret_cast<uint32_t*>(&rax234) = *reinterpret_cast<uint32_t*>(&rax233) & 31;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax234) + 4) = 0;
                    if (rax234 == 1) {
                        rdi235 = r13_232;
                        rax236 = gcd_odd(rdi235, r12_210);
                        if (!reinterpret_cast<int1_t>(rax236 == 1)) 
                            break;
                    }
                    --r14_219;
                    if (r14_219) 
                        continue;
                    rcx237 = r15_218 + r15_218;
                    if (!r15_218) {
                        r15_218 = rcx237;
                        r11_222 = rbp220;
                    } else {
                        do {
                            rdi238 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                            rax239 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi238) + reinterpret_cast<unsigned char>(r12_210));
                            if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                                rdi238 = rax239;
                            }
                            ++r14_219;
                        } while (r15_218 != r14_219);
                        r11_222 = rbp220;
                        r15_218 = rcx237;
                        rbp220 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax239) - (reinterpret_cast<uint64_t>(rax239) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax239) < reinterpret_cast<uint64_t>(rax239) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi238) < reinterpret_cast<uint64_t>(r10_229)))) & reinterpret_cast<unsigned char>(r12_210)) + (reinterpret_cast<uint64_t>(rdi238) + reinterpret_cast<uint64_t>(r9_230)));
                    }
                }
                r9_192 = r8_228;
                r8_2 = r11_222;
                r11_240 = r9_230;
                do {
                    rsi241 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    rax242 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi241) + reinterpret_cast<unsigned char>(r12_210));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rsi241 = rax242;
                    }
                    rbx243 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax242) - (reinterpret_cast<uint64_t>(rax242) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax242) < reinterpret_cast<uint64_t>(rax242) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rsi241) < reinterpret_cast<uint64_t>(r10_229)))) & reinterpret_cast<unsigned char>(r12_210)) + (reinterpret_cast<uint64_t>(rsi241) + reinterpret_cast<uint64_t>(r11_240)));
                    rsi244 = r12_210;
                    rdi235 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rdi235) - (reinterpret_cast<unsigned char>(rdi235) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi235) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rdi235) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_2) < reinterpret_cast<unsigned char>(rbx243))))))) & reinterpret_cast<unsigned char>(r12_210)) + (reinterpret_cast<unsigned char>(r8_2) - reinterpret_cast<unsigned char>(rbx243)));
                    rax245 = gcd_odd(rdi235, rsi244);
                } while (rax245 == 1);
                rcx246 = r8_2;
                r11_247 = rax245;
                if (rax245 == r12_210) 
                    goto addr_5af7_177;
                rdx248 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_210) % reinterpret_cast<unsigned char>(r11_247));
                rax249 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_210) / reinterpret_cast<unsigned char>(r11_247));
                r8_250 = rax249;
                r12_210 = rax249;
                if (reinterpret_cast<unsigned char>(r11_247) > reinterpret_cast<unsigned char>(1) && (reinterpret_cast<unsigned char>(r11_247) <= reinterpret_cast<unsigned char>(0x17ded78) || (al251 = prime_p_part_0(r11_247, rsi244, rdx248, rcx246), r11_247 = r11_247, rcx246 = rcx246, r8_250 = rax249, !!al251))) {
                    *reinterpret_cast<int32_t*>(&rdx252) = 1;
                    *reinterpret_cast<int32_t*>(&rdx252 + 4) = 0;
                    rsi253 = r11_247;
                    factor_insert_multiplicity(v47, rsi253, 1);
                    r8_2 = r8_250;
                    rcx254 = rcx246;
                    zf255 = reinterpret_cast<int1_t>(r8_2 == 1);
                    if (reinterpret_cast<unsigned char>(r8_2) <= reinterpret_cast<unsigned char>(1)) 
                        goto addr_5aca_180; else 
                        goto addr_5a2f_181;
                }
                rdx252 = v47;
                rsi253 = v85 + 1;
                factor_using_pollard_rho(r11_247, rsi253, rdx252, rcx246);
                r8_2 = r8_250;
                rcx254 = rcx246;
                zf255 = reinterpret_cast<int1_t>(r8_2 == 1);
                if (reinterpret_cast<unsigned char>(r8_2) > reinterpret_cast<unsigned char>(1)) {
                    addr_5a2f_181:
                    if (reinterpret_cast<unsigned char>(r8_2) <= reinterpret_cast<unsigned char>(0x17ded78)) 
                        goto addr_5ad7_183;
                    al256 = prime_p_part_0(r8_2, rsi253, rdx252, rcx254);
                    r8_2 = r8_2;
                    if (al256) 
                        goto addr_5ad7_183;
                } else {
                    addr_5aca_180:
                    if (zf255) 
                        goto addr_5b45_185; else 
                        goto addr_5acc_186;
                }
                rbp220 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp220) % reinterpret_cast<unsigned char>(r8_2));
                rcx221 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx254) % reinterpret_cast<unsigned char>(r8_2));
                continue;
                addr_5acc_186:
                *reinterpret_cast<int32_t*>(&rcx221) = 0;
                *reinterpret_cast<int32_t*>(&rcx221 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rbp220) = 0;
                *reinterpret_cast<int32_t*>(&rbp220 + 4) = 0;
            }
            break;
            addr_5af7_177:
            ++v85;
        } while (reinterpret_cast<unsigned char>(r12_210) > reinterpret_cast<unsigned char>(1));
        goto addr_5b07_148;
    }
    fun_2810("a < n", "src/factor.c", 0x5c4, "factor_using_pollard_rho", r8_2, r9_192);
    addr_5b45_185:
    goto v1;
    addr_5ad7_183:
    rdi194 = v47;
    rsi193 = r8_2;
    goto addr_2f20_138;
    addr_53a9_68:
    if (!*reinterpret_cast<void***>(v85 + 8)) {
        *reinterpret_cast<void***>(v85) = rbp8;
        *reinterpret_cast<void***>(v85 + 8) = v86;
        goto addr_4d41_133;
    }
    factor_insert_large_part_0();
    r14_257 = reinterpret_cast<struct s0*>(rdi84 + 0xffffffffffffffff);
    rbp258 = rdi84;
    rsp259 = reinterpret_cast<struct s0**>(rsp12 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x148);
    rax260 = g28;
    v261 = rax260;
    eax262 = 0;
    v263 = r14_257;
    if (*reinterpret_cast<uint32_t*>(&rdi84) & 1) 
        goto addr_5478_192;
    v264 = 0;
    r14_257 = v263;
    addr_5490_194:
    rcx265 = rbp258;
    *reinterpret_cast<int32_t*>(&r15_266) = 0;
    *reinterpret_cast<int32_t*>(&r15_266 + 4) = 0;
    rax267 = reinterpret_cast<unsigned char>(rbp258) >> 1;
    esi268 = 64;
    *reinterpret_cast<uint32_t*>(&rax269) = *reinterpret_cast<uint32_t*>(&rax267) & 0x7f;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax269) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax270) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax269);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax270) + 4) = 0;
    rdx271 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax270) + reinterpret_cast<int64_t>(rax270) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax270) * reinterpret_cast<int64_t>(rax270) * reinterpret_cast<unsigned char>(rbp258)));
    rax272 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx271) + reinterpret_cast<uint64_t>(rdx271) - reinterpret_cast<uint64_t>(rdx271) * reinterpret_cast<uint64_t>(rdx271) * reinterpret_cast<unsigned char>(rbp258));
    *reinterpret_cast<int32_t*>(&rdx273) = 0;
    *reinterpret_cast<int32_t*>(&rdx273 + 4) = 0;
    r13_274 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax272) + reinterpret_cast<uint64_t>(rax272) - reinterpret_cast<uint64_t>(rax272) * reinterpret_cast<uint64_t>(rax272) * reinterpret_cast<unsigned char>(rbp258));
    *reinterpret_cast<int32_t*>(&rax275) = 1;
    *reinterpret_cast<int32_t*>(&rax275 + 4) = 0;
    do {
        rdi276 = reinterpret_cast<unsigned char>(rcx265) << 63;
        rcx265 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx265) >> 1);
        rdx273 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx273) >> 1) | rdi276);
        if (reinterpret_cast<unsigned char>(rcx265) < reinterpret_cast<unsigned char>(rax275) || rcx265 == rax275 && reinterpret_cast<unsigned char>(rdx273) <= reinterpret_cast<unsigned char>(r15_266)) {
            cf277 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_266) < reinterpret_cast<unsigned char>(rdx273));
            r15_266 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_266) - reinterpret_cast<unsigned char>(rdx273));
            rax275 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax275) - (reinterpret_cast<unsigned char>(rcx265) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax275) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx265) + static_cast<uint64_t>(cf277))))));
        }
        --esi268;
    } while (esi268);
    *reinterpret_cast<int32_t*>(&rbx278) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx278) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&r8_2) = v264;
    *reinterpret_cast<int32_t*>(&r8_2 + 4) = 0;
    r9_192 = r15_266;
    *reinterpret_cast<unsigned char*>(&rbx278) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp258) - reinterpret_cast<unsigned char>(r15_266)) > reinterpret_cast<unsigned char>(r15_266));
    rbx279 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbx278) & reinterpret_cast<unsigned char>(rbp258)) + (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r15_266) + reinterpret_cast<unsigned char>(r15_266)) - reinterpret_cast<unsigned char>(rbp258)));
    al280 = millerrabin(rbp258, r13_274, rbx279, r14_257, *reinterpret_cast<uint32_t*>(&r8_2), r9_192);
    if (al280) {
        v281 = reinterpret_cast<void*>(rsp259 - 1 + 1 + 6);
        factor();
        v282 = r14_257;
        r14_257 = r13_274;
        eax283 = *reinterpret_cast<unsigned char*>(&v112 + 2);
        r13_274 = reinterpret_cast<struct s0*>(0x10340);
        v284 = eax283;
        *reinterpret_cast<uint32_t*>(&rax285) = eax283 - 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax285) + 4) = 0;
        v286 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v281) + rax285 * 8);
        r12_13 = reinterpret_cast<void**>(2);
        rax287 = rbx279;
        rbx279 = r15_266;
        r15_266 = rax287;
        goto addr_55b0_200;
    }
    addr_56fc_201:
    addr_5690_202:
    rax288 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v261) - reinterpret_cast<unsigned char>(g28));
    if (rax288) {
        fun_2740();
    } else {
        goto v86;
    }
    addr_5719_205:
    *reinterpret_cast<int32_t*>(&rdx196) = 0x4f2;
    *reinterpret_cast<int32_t*>(&rdx196 + 4) = 0;
    rsi197 = reinterpret_cast<void**>("src/factor.c");
    rdi195 = reinterpret_cast<void**>("(s1) < (n)");
    fun_2810("(s1) < (n)", "src/factor.c", 0x4f2, "prime_p", r8_2, r9_192);
    goto addr_5740_137;
    addr_5478_192:
    do {
        r14_257 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(r14_257) >> 1);
        ++eax262;
    } while (!(*reinterpret_cast<unsigned char*>(&r14_257) & 1));
    v264 = eax262;
    goto addr_5490_194;
    addr_4cba_87:
    fun_2810("b0 & 1", "src/factor.c", 0x1e0, "gcd2_odd", r8_132, r9_116);
    do {
        fun_2740();
        addr_4ce0_125:
        factor_using_pollard_rho2(rbx11, r13_7, 2, v91);
        addr_4ad5_79:
        rax289 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v93) - reinterpret_cast<unsigned char>(g28));
    } while (rax289);
    return;
    addr_4aa0_119:
    if (reinterpret_cast<unsigned char>(rbp89) <= reinterpret_cast<unsigned char>(1)) {
        addr_4c74_210:
        factor_using_pollard_rho(rbp89, 1, v91, rcx28);
        goto addr_4ad5_79;
    } else {
        addr_4aaa_211:
        if (reinterpret_cast<unsigned char>(rbp89) <= reinterpret_cast<unsigned char>(0x17ded78) || (al290 = prime_p_part_0(rbp89, rsi9, rdx5, rcx28), !!al290)) {
            factor_insert_multiplicity(v91, rbp89, 1, v91, rbp89, 1);
            goto addr_4ad5_79;
        }
    }
    addr_4c4f_126:
    rcx28 = v91;
    rsi9 = v112;
    rdx5 = reinterpret_cast<void**>(2);
    factor_using_pollard_rho2(v32, rsi9, 2, rcx28);
    if (reinterpret_cast<unsigned char>(rbp89) > reinterpret_cast<unsigned char>(1)) 
        goto addr_4aaa_211; else 
        goto addr_4c74_210;
    addr_4ca3_128:
    *reinterpret_cast<int32_t*>(&rdx5) = 1;
    *reinterpret_cast<int32_t*>(&rdx5 + 4) = 0;
    rsi9 = v112;
    factor_insert_multiplicity(v91, rsi9, 1);
    goto addr_4aa0_119;
    addr_4a94_130:
    *reinterpret_cast<void***>(v91) = v112;
    *reinterpret_cast<void***>(v91 + 8) = v32;
    goto addr_4aa0_119;
    addr_4c97_132:
    *reinterpret_cast<void***>(v91) = rbp89;
    *reinterpret_cast<void***>(v91 + 8) = r13_7;
    goto addr_4ad5_79;
    addr_5689_213:
    goto addr_5690_202;
    addr_5680_214:
    while (rax291 == rbx279) {
        while (1) {
            *reinterpret_cast<uint32_t*>(&rax292) = r13_274->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax292) + 4) = 0;
            r12_13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_13) + reinterpret_cast<uint64_t>(rax292));
            rax293 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx279) * reinterpret_cast<unsigned char>(r12_13));
            rdx294 = __intrinsic();
            r15_266 = rax293;
            if (rdx294) {
                if (reinterpret_cast<unsigned char>(rbp258) <= reinterpret_cast<unsigned char>(rdx294)) 
                    goto addr_5719_205;
                rcx295 = rbp258;
                esi296 = 64;
                *reinterpret_cast<int32_t*>(&rax297) = 0;
                *reinterpret_cast<int32_t*>(&rax297 + 4) = 0;
                do {
                    rdi298 = reinterpret_cast<unsigned char>(rcx295) << 63;
                    rcx295 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx295) >> 1);
                    rax297 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax297) >> 1) | rdi298);
                    if (reinterpret_cast<unsigned char>(rcx295) < reinterpret_cast<unsigned char>(rdx294) || rcx295 == rdx294 && reinterpret_cast<unsigned char>(rax297) <= reinterpret_cast<unsigned char>(r15_266)) {
                        cf299 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_266) < reinterpret_cast<unsigned char>(rax297));
                        r15_266 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_266) - reinterpret_cast<unsigned char>(rax297));
                        rdx294 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx294) - (reinterpret_cast<unsigned char>(rcx295) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx294) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx295) + static_cast<uint64_t>(cf299))))));
                    }
                    --esi296;
                } while (esi296);
            } else {
                r15_266 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax293) % reinterpret_cast<unsigned char>(rbp258));
            }
            *reinterpret_cast<uint32_t*>(&r8_2) = v264;
            *reinterpret_cast<int32_t*>(&r8_2 + 4) = 0;
            r9_192 = rbx279;
            al300 = millerrabin(rbp258, r14_257, r15_266, v282, *reinterpret_cast<uint32_t*>(&r8_2), r9_192);
            if (!al300) 
                goto addr_56fc_201;
            r13_274 = reinterpret_cast<struct s0*>(&r13_274->f1);
            if (reinterpret_cast<int1_t>(0x105dc == r13_274)) 
                break;
            addr_55b0_200:
            if (!v284) 
                goto addr_5690_202;
            r11_301 = v281;
            do {
                r8_2 = rbx279;
                rax291 = powm(r15_266, reinterpret_cast<uint64_t>(v263) / v302, rbp258, r14_257, r8_2, r9_192);
                if (v286 == r11_301) 
                    goto addr_5680_214;
                r11_301 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r11_301) + 8);
            } while (rax291 != rbx279);
        }
        fun_2700();
        fun_2a20();
        rax291 = fun_25f0();
    }
    goto addr_5689_213;
}

void** powm(void** rdi, struct s0* rsi, void** rdx, struct s0* rcx, void** r8, void** r9) {
    struct s0* r9_7;
    void** rsi8;

    r9_7 = rsi;
    rsi8 = rdx;
    if (!(*reinterpret_cast<unsigned char*>(&r9_7) & 1)) {
        while (r9_7) {
            while (1) {
                addr_2fd0_3:
                if (__intrinsic() < __intrinsic()) {
                }
                r9_7 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(r9_7) >> 1);
                if (!(*reinterpret_cast<unsigned char*>(&r9_7) & 1)) 
                    break;
                r8 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                    r8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<unsigned char>(rsi8));
                }
            }
        }
        return r8;
    } else {
        r8 = rdi;
        goto addr_2fd0_3;
    }
}

unsigned char millerrabin2(void** rdi, void** rsi, struct s43* rdx, void** rcx, uint32_t r8d, void** r9) {
    void** rbp7;
    void* rsp8;
    uint32_t v9;
    void** r14_10;
    void** r8_11;
    void** rdi12;
    void** rax13;
    void** v14;
    void** rax15;
    void*** rsp16;
    void** rsi17;
    void** v18;
    void** rdx19;
    void** rax20;
    void** v21;
    void** rax22;
    void** v23;
    void** r12_24;
    void** r15_25;
    void** rbx26;
    void** rax27;
    void** v28;
    unsigned char cl29;
    unsigned char v30;
    void* rax31;
    uint32_t eax32;
    void** r13_33;
    void** rax34;
    void** v35;
    void** rax36;
    void** v37;
    void* rbx38;
    void** rax39;
    void** rcx40;
    void** rsi41;
    void* rdx42;
    void** rbx43;
    void** rax44;
    void* rax45;
    void** r13_46;
    void** r12_47;
    void** rcx48;
    void** rdx49;
    struct s0* r14_50;
    void** v51;
    void** v52;
    void** v53;
    void** r14_54;
    void** v55;
    void** rbx56;
    void** rax57;
    void** rsi58;
    void** v59;
    void** rax60;
    void** rdi61;
    void** r15_62;
    void** rdi63;
    int64_t v64;
    struct s0* rbp65;
    struct s0* rbx66;
    void** r15_67;
    int32_t eax68;
    uint1_t zf69;
    int64_t v70;
    void** r15_71;
    void** rdx72;
    void** rax73;
    void** rsi74;
    void** rax75;
    void** rax76;
    void** rax77;
    void* v78;
    void** rax79;
    void** r14_80;
    void** rdi81;
    void** r15_82;
    void** r14_83;
    void** rax84;
    void** rsi85;
    void** rax86;
    void** rax87;
    void* rcx88;
    void** rax89;
    uint64_t rdx90;
    void* rdx91;
    void* rdi92;

    rbp7 = rsi;
    rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56);
    v9 = r8d;
    r14_10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp8) + 32);
    r8_11 = rbp7;
    rdi12 = r14_10;
    rax13 = g28;
    v14 = rax13;
    rax15 = powm2(rdi12, rdx, rcx, rdi, r8_11, r9);
    rsp16 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
    rsi17 = v18;
    rdx19 = rax15;
    rax20 = *reinterpret_cast<void***>(r9);
    v21 = rax20;
    rax22 = *reinterpret_cast<void***>(r9 + 8);
    v23 = rax22;
    if (rax20 == rdx19 && rax22 == rsi17) {
        goto addr_3492_3;
    }
    r12_24 = *reinterpret_cast<void***>(rdi + 8);
    r15_25 = *reinterpret_cast<void***>(rdi);
    rbx26 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_25) - reinterpret_cast<unsigned char>(v21));
    rax27 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_24) - (reinterpret_cast<unsigned char>(v23) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_24) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(v23) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_25) < reinterpret_cast<unsigned char>(v21))))))));
    v28 = rax27;
    cl29 = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdx19 == rbx26)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi17 == rax27)));
    v30 = cl29;
    if (cl29) {
        addr_3492_3:
        v30 = 1;
        goto addr_3497_5;
    } else {
        if (v9 <= 1) {
            addr_3497_5:
            rax31 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v14) - reinterpret_cast<unsigned char>(g28));
            if (!rax31) {
                eax32 = v30;
                return *reinterpret_cast<unsigned char*>(&eax32);
            }
        } else {
            r13_33 = r12_24;
            *reinterpret_cast<uint32_t*>(&r12_24) = 1;
            do {
                r8_11 = rdx19;
                rdi12 = r14_10;
                rax34 = mulredc2(rdi12, rsi17, rdx19, rsi17, r8_11, r13_33, r15_25, rbp7);
                rsi17 = v35;
                rdx19 = rax34;
                rsp16 = rsp16 - 8 - 8 - 8 + 8 + 8 + 8;
                if (rbx26 != rdx19) 
                    continue;
                if (v28 == rsi17) 
                    goto addr_3492_3;
            } while ((v21 != rdx19 || rsi17 != v23) && (*reinterpret_cast<uint32_t*>(&r12_24) = *reinterpret_cast<uint32_t*>(&r12_24) + 1, v9 != *reinterpret_cast<uint32_t*>(&r12_24)));
            goto addr_3497_5;
        }
    }
    fun_2740();
    rax36 = g28;
    v37 = rax36;
    rbx38 = reinterpret_cast<void*>(rsp16 - 8 + 8 - 8 - 8 - 40);
    rax39 = umaxtostr(rdi12, rbx38, rdi12, rbx38);
    rcx40 = g150f8;
    rsi41 = rax39;
    rdx42 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax39) - reinterpret_cast<uint64_t>(rbx38));
    rbx43 = reinterpret_cast<void**>(20 - reinterpret_cast<uint64_t>(rdx42));
    if (reinterpret_cast<unsigned char>(rbx43) < reinterpret_cast<unsigned char>(rsi17)) 
        goto addr_3511_14;
    addr_3569_15:
    rax44 = fun_28d0(rcx40, rsi41, rbx43, rcx40, r8_11, rcx40, rsi41, rbx43, rcx40, r8_11);
    g150f8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax44) + reinterpret_cast<unsigned char>(rbx43));
    rax45 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v37) - reinterpret_cast<unsigned char>(g28));
    if (!rax45) {
        goto v21;
    }
    fun_2740();
    r13_46 = rcx40;
    r12_47 = rsi41;
    rcx48 = *reinterpret_cast<void***>(rcx40);
    rdx49 = *reinterpret_cast<void***>(rcx40 + 8);
    r14_50 = reinterpret_cast<struct s0*>(*reinterpret_cast<void***>(rcx40 + 16) + 0xffffffffffffffff);
    v51 = *reinterpret_cast<void***>(rcx40 + 16);
    v52 = rcx48;
    v53 = rdx49;
    if (reinterpret_cast<int64_t>(r14_50) >= reinterpret_cast<int64_t>(0)) 
        goto addr_361b_19;
    r14_54 = v51 + 1;
    v55 = r14_54;
    rbx56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_54) << 4);
    rax57 = xrealloc(v52, rbx56, v52, rbx56);
    rsi58 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_54) * 8);
    v59 = rax57;
    rax60 = xrealloc(v53, rsi58, v53, rsi58);
    rdi61 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax57) + reinterpret_cast<unsigned char>(rbx56) + 0xfffffffffffffff0);
    r15_62 = rax60;
    fun_2ab0(rdi61, rsi58, rdx49, rcx48, r8_11, rdi61, rsi58, rdx49, rcx48, r8_11);
    rdi63 = rdi61;
    addr_370b_21:
    fun_27a0(rdi63, r12_47, rdx49, rcx48, r8_11, rdi63, r12_47, rdx49, rcx48, r8_11);
    *reinterpret_cast<void***>(r15_62 + reinterpret_cast<unsigned char>(v51) * 8) = reinterpret_cast<void**>(1);
    *reinterpret_cast<void***>(r13_46 + 8) = r15_62;
    *reinterpret_cast<void***>(r13_46) = v59;
    *reinterpret_cast<void***>(r13_46 + 16) = v55;
    goto v64;
    addr_361b_19:
    rbp65 = r14_50;
    rbx66 = r14_50;
    rdx49 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r14_50) << 4);
    r15_67 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx48) + reinterpret_cast<unsigned char>(rdx49));
    do {
        eax68 = fun_2a00(r15_67, r12_47, rdx49, rcx48, r8_11, r15_67, r12_47, rdx49, rcx48, r8_11);
        zf69 = reinterpret_cast<uint1_t>(eax68 == 0);
        if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(eax68 < 0) | zf69)) 
            break;
        rbx66 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbx66) - 1);
        r15_67 = r15_67 - 16;
    } while (rbx66 != 0xffffffffffffffff);
    goto addr_3670_24;
    if (zf69) {
        *reinterpret_cast<void***>(v53 + reinterpret_cast<uint64_t>(rbx66) * 8) = *reinterpret_cast<void***>(v53 + reinterpret_cast<uint64_t>(rbx66) * 8) + 1;
        goto v70;
    }
    r15_71 = v51 + 1;
    v55 = r15_71;
    rdx72 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_71) << 4);
    rax73 = xrealloc(v52, rdx72, v52, rdx72);
    rsi74 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_71) * 8);
    v59 = rax73;
    rax75 = xrealloc(v53, rsi74, v53, rsi74);
    rdx49 = rdx72;
    r15_62 = rax75;
    rax76 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v59) + reinterpret_cast<unsigned char>(rdx49) + 0xfffffffffffffff0);
    fun_2ab0(rax76, rsi74, rdx49, rcx48, r8_11, rax76, rsi74, rdx49, rcx48, r8_11);
    rax77 = reinterpret_cast<void**>(&rbx66->f1);
    v51 = rax77;
    v78 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax77) << 4);
    rax79 = rax76;
    if (reinterpret_cast<int64_t>(rbx66) < reinterpret_cast<int64_t>(r14_50)) {
        addr_36da_28:
        r14_80 = rax79;
    } else {
        goto addr_3701_30;
    }
    do {
        rdi81 = r14_80;
        r14_80 = r14_80 - 16;
        fun_27a0(rdi81, r14_80, rdx49, rcx48, r8_11, rdi81, r14_80, rdx49, rcx48, r8_11);
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r15_62 + reinterpret_cast<uint64_t>(rbp65) * 8) + 8) = *reinterpret_cast<void***>(r15_62 + reinterpret_cast<uint64_t>(rbp65) * 8);
        rbp65 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbp65) - 1);
    } while (reinterpret_cast<int64_t>(rbx66) < reinterpret_cast<int64_t>(rbp65));
    addr_3701_30:
    rdi63 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(v78) + reinterpret_cast<unsigned char>(v59));
    goto addr_370b_21;
    addr_3670_24:
    r15_82 = v51 + 1;
    v55 = r15_82;
    r14_83 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_82) << 4);
    rax84 = xrealloc(v52, r14_83, v52, r14_83);
    rsi85 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_82) * 8);
    v59 = rax84;
    rax86 = xrealloc(v53, rsi85, v53, rsi85);
    r15_62 = rax86;
    rax87 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v59) + reinterpret_cast<unsigned char>(r14_83) + 0xfffffffffffffff0);
    fun_2ab0(rax87, rsi85, rdx49, rcx48, r8_11, rax87, rsi85, rdx49, rcx48, r8_11);
    rax79 = rax87;
    v78 = reinterpret_cast<void*>(0);
    v51 = reinterpret_cast<void**>(0);
    goto addr_36da_28;
    addr_3511_14:
    rcx88 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx42) + reinterpret_cast<unsigned char>(rsi17) + 0xffffffffffffffec);
    rax89 = g150f8;
    if (reinterpret_cast<uint64_t>(rcx88) < 8) {
        if (*reinterpret_cast<unsigned char*>(&rcx88) & 4) {
            *reinterpret_cast<void***>(rax89) = reinterpret_cast<void**>(0x30303030);
            *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(rax89) + reinterpret_cast<uint64_t>(rcx88) + 0xfffffffffffffffc) = 0x30303030;
        } else {
            if (rcx88 && (*reinterpret_cast<void***>(rax89) = reinterpret_cast<void**>(48), !!(*reinterpret_cast<unsigned char*>(&rcx88) & 2))) {
                *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(rax89) + reinterpret_cast<uint64_t>(rcx88) + 0xfffffffffffffffe) = reinterpret_cast<int16_t>(millerrabin);
            }
        }
    } else {
        *reinterpret_cast<void***>(rax89) = reinterpret_cast<void**>(0x3030303030303030);
        r8_11 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax89 + 8) & 0xfffffffffffffff8);
        *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rax89) + reinterpret_cast<uint64_t>(rcx88) + 0xfffffffffffffff8) = reinterpret_cast<void**>(0x3030303030303030);
        rdx90 = reinterpret_cast<unsigned char>(rax89) - reinterpret_cast<unsigned char>(r8_11) + reinterpret_cast<uint64_t>(rcx88) & 0xfffffffffffffff8;
        if (rdx90 >= 8) {
            *reinterpret_cast<int32_t*>(&rdx91) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx91) + 4) = 0;
            rdi92 = reinterpret_cast<void*>(rdx90 & 0xfffffffffffffff8);
            do {
                *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r8_11) + reinterpret_cast<uint64_t>(rdx91)) = reinterpret_cast<void**>(0x3030303030303030);
                rdx91 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx91) + 8);
            } while (reinterpret_cast<uint64_t>(rdx91) < reinterpret_cast<uint64_t>(rdi92));
        }
    }
    rcx40 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rcx88) + reinterpret_cast<unsigned char>(rax89));
    goto addr_3569_15;
}

void** powm2(void** rdi, struct s43* rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r15_7;
    void** r12_8;
    void** rbx9;
    void** rax10;
    void** r8_11;
    void** r10_12;
    void** v13;
    void** r14_14;
    void** r13_15;
    void** v16;
    int32_t ebx17;
    void** v18;
    void** rbp19;
    void** rax20;
    void** rax21;
    void** rbx22;
    void** rax23;
    void** rax24;

    r15_7 = rdi;
    r12_8 = r8;
    rbx9 = *reinterpret_cast<void***>(r9);
    rax10 = *reinterpret_cast<void***>(r9 + 8);
    r8_11 = rsi->f0;
    r10_12 = rsi->f8;
    v13 = rdx;
    r14_14 = *reinterpret_cast<void***>(rcx);
    r13_15 = *reinterpret_cast<void***>(rcx + 8);
    v16 = rbx9;
    ebx17 = 64;
    v18 = rax10;
    rbp19 = *reinterpret_cast<void***>(rdx);
    do {
        if (*reinterpret_cast<unsigned char*>(&rbp19) & 1) {
            rax20 = mulredc2(r15_7, v18, v16, r10_12, r8_11, r13_15, r14_14, r12_8);
            v16 = rax20;
            v18 = *reinterpret_cast<void***>(r15_7);
            r8_11 = r8_11;
            r10_12 = r10_12;
        }
        rbp19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp19) >> 1);
        rax21 = mulredc2(r15_7, r10_12, r8_11, r10_12, r8_11, r13_15, r14_14, r12_8);
        r10_12 = *reinterpret_cast<void***>(r15_7);
        r8_11 = rax21;
        --ebx17;
    } while (ebx17);
    rbx22 = *reinterpret_cast<void***>(v13 + 8);
    if (rbx22) {
        do {
            if (*reinterpret_cast<unsigned char*>(&rbx22) & 1) {
                rax23 = mulredc2(r15_7, v18, v16, r10_12, r8_11, r13_15, r14_14, r12_8);
                v16 = rax23;
                v18 = *reinterpret_cast<void***>(r15_7);
                r8_11 = r8_11;
                r10_12 = r10_12;
            }
            rax24 = mulredc2(r15_7, r10_12, r8_11, r10_12, r8_11, r13_15, r14_14, r12_8);
            rbx22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx22) >> 1);
            r10_12 = *reinterpret_cast<void***>(r15_7);
            r8_11 = rax24;
        } while (rbx22);
    }
    *reinterpret_cast<void***>(r15_7) = v18;
    return v16;
}

/* line_buffered.0 */
uint32_t line_buffered_0 = 0xffffffff;

void** lbuf = reinterpret_cast<void**>(0);

int32_t fun_2670();

void lbuf_flush(int64_t rdi);

void lbuf_putc(void** dil, ...) {
    int64_t rdi1;
    void** rax2;
    void** rbp3;
    uint32_t eax4;
    void** rbx5;
    int32_t eax6;
    int32_t eax7;
    void** rax8;
    void** r12_9;
    void** rbp10;
    void** rcx11;
    void** r8_12;
    void** rax13;

    *reinterpret_cast<void***>(&rdi1) = dil;
    rax2 = g150f8;
    rbp3 = rax2 + 1;
    g150f8 = rbp3;
    *reinterpret_cast<void***>(rax2) = *reinterpret_cast<void***>(&rdi1);
    if (*reinterpret_cast<void***>(&rdi1) == 10) {
        eax4 = line_buffered_0;
        rbx5 = lbuf;
        if (eax4 == 0xffffffff) {
            eax6 = fun_2670();
            if (!eax6) {
                *reinterpret_cast<int32_t*>(&rdi1) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi1) + 4) = 0;
                eax7 = fun_2670();
                eax4 = reinterpret_cast<uint1_t>(!!eax7);
                line_buffered_0 = eax4;
            } else {
                line_buffered_0 = 1;
                goto addr_3965_6;
            }
        }
        if (eax4) {
            addr_3965_6:
        } else {
            if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp3) - reinterpret_cast<unsigned char>(rbx5)) > 0x1ff) {
                rax8 = rbx5 + 0x200;
                do {
                    r12_9 = rax8;
                    --rax8;
                } while (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax8) == 10));
                g150f8 = r12_9;
                rbp10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp3) - reinterpret_cast<unsigned char>(r12_9));
                lbuf_flush(rdi1);
                rcx11 = lbuf;
                rax13 = fun_28d0(rcx11, r12_9, rbp10, rcx11, r8_12);
                g150f8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax13) + reinterpret_cast<unsigned char>(rbp10));
                return;
            }
        }
    } else {
        return;
    }
}

struct s62 {
    signed char f0;
    signed char[3] pad4;
    void** f4;
    signed char[7] pad12;
    signed char fc;
    signed char[3] pad16;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    signed char f20;
    signed char[3] pad36;
    void** f24;
};

uint64_t fun_2710();

void** strnlen1(void** rdi, uint64_t rsi, void** rdx, ...);

void** rpl_mbrtowc(void** rdi, void** rsi, void** rdx, void** rcx, ...);

uint32_t fun_2af0(void** rdi, void** rsi, ...);

void** fun_2720(void** rdi, ...);

void** trim2(void** rdi, void** rsi, void** rdx, void** rcx);

void** g10646 = reinterpret_cast<void**>(0x6d);

void** mbsstr(void** rdi, void** rsi, ...);

unsigned char** fun_2b10(void** rdi, void** rsi, ...);

int32_t fun_2910();

/* mbuiter_multi_next.part.0 */
void mbuiter_multi_next_part_0(struct s62* rdi, void** rsi, ...) {
    void** rbp3;
    int64_t* rsp4;
    int64_t v5;
    int64_t rbx6;
    uint64_t rax7;
    void** r12_8;
    void** rdx9;
    void** rax10;
    void** rax11;
    void* rsp12;
    uint32_t ecx13;
    uint32_t eax14;
    int64_t rax15;
    uint32_t eax16;
    uint32_t eax17;
    void** r8_18;
    void** r9_19;
    void** eax20;
    void** rdi21;
    void** rax22;
    void** rax23;
    void** r8_24;
    void** r9_25;
    void** r8_26;
    void** r9_27;
    uint32_t eax28;
    void** r12_29;
    void** rsi30;
    void** rax31;
    void** v32;
    void** rax33;
    struct s62* rsp34;
    int1_t zf35;
    void** r13_36;
    struct s62* rbp37;
    void** rax38;
    void** rbx39;
    uint64_t rax40;
    void** rax41;
    unsigned char** rax42;
    int64_t rdx43;
    int64_t r12_44;
    unsigned char** rax45;
    void** v46;
    unsigned char* r14_47;
    signed char v48;
    int32_t v49;
    void** rax50;
    void* v51;
    uint32_t edx52;
    unsigned char v53;
    struct s62* r12_54;
    signed char v55;
    int32_t v56;
    signed char v57;
    int32_t v58;
    signed char v59;
    int32_t v60;
    signed char v61;
    int32_t eax62;
    int32_t eax63;
    signed char v64;
    int32_t v65;
    void* v66;
    void* rax67;

    rbp3 = reinterpret_cast<void**>(&rdi->f4);
    rsp4 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8);
    v5 = rbx6;
    if (rdi->f0) {
        addr_6c77_2:
        rax7 = fun_2710();
        r12_8 = rdi->f10;
        rax10 = strnlen1(r12_8, rax7, rdx9);
        rax11 = rpl_mbrtowc(&rdi->f24, r12_8, rax10, rbp3);
        rsp12 = reinterpret_cast<void*>(rsp4 - 1 + 1 - 1 + 1 - 1 + 1);
        rdi->f18 = rax11;
        if (rax11 == 0xffffffffffffffff) {
            rdi->f18 = reinterpret_cast<void**>(1);
            rdi->f20 = 0;
            rdi->fc = 1;
            return;
        }
    } else {
        rdx9 = rdi->f10;
        ecx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx9));
        eax14 = ecx13;
        *reinterpret_cast<unsigned char*>(&eax14) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax14) >> 5);
        *reinterpret_cast<uint32_t*>(&rax15) = eax14 & 7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
        eax16 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(0x10e40) + reinterpret_cast<uint64_t>(rax15 * 4))) >> *reinterpret_cast<signed char*>(&ecx13);
        if (!(*reinterpret_cast<unsigned char*>(&eax16) & 1)) {
            rbp3 = reinterpret_cast<void**>(&rdi->f4);
            eax17 = fun_2af0(rbp3, 0x10e40);
            rsp4 = rsp4 - 1 + 1;
            if (!eax17) {
                addr_6d60_6:
                fun_2810("mbsinit (&iter->state)", "lib/mbuiter.h", 0x8f, "mbuiter_multi_next", r8_18, r9_19);
                goto addr_6d80_7;
            } else {
                rdi->f0 = 1;
                goto addr_6c77_2;
            }
        } else {
            rdi->f18 = reinterpret_cast<void**>(1);
            eax20 = reinterpret_cast<void**>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdx9))));
            rdi->f20 = 1;
            *reinterpret_cast<void***>(&rdi->f24) = eax20;
            goto addr_6c51_10;
        }
    }
    if (rax11 == 0xfffffffffffffffe) {
        rdi21 = rdi->f10;
        rax22 = fun_2720(rdi21, rdi21);
        rdi->f20 = 0;
        rdi->f18 = rax22;
        rdi->fc = 1;
        return;
    }
    if (rax11) 
        goto addr_6cca_14;
    rax23 = rdi->f10;
    rdi->f18 = reinterpret_cast<void**>(1);
    if (*reinterpret_cast<void***>(rax23)) {
        addr_6d41_16:
        fun_2810("*iter->cur.ptr == '\\0'", "lib/mbuiter.h", 0xab, "mbuiter_multi_next", r8_24, r9_25);
        rsp4 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
        goto addr_6d60_6;
    } else {
        if (*reinterpret_cast<void***>(&rdi->f24)) {
            fun_2810("iter->cur.wc == 0", "lib/mbuiter.h", 0xac, "mbuiter_multi_next", r8_26, r9_27);
            rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
            goto addr_6d41_16;
        } else {
            addr_6cca_14:
            rdi->f20 = 1;
            eax28 = fun_2af0(rbp3, r12_8, rbp3, r12_8);
            if (eax28) {
                rdi->f0 = 0;
                rdi->fc = 1;
                return;
            }
        }
    }
    addr_6c51_10:
    rdi->fc = 1;
    return;
    addr_6d80_7:
    r12_29 = reinterpret_cast<void**>("mbsinit (&iter->state)");
    *reinterpret_cast<int32_t*>(&rsi30) = 2;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    rax31 = g28;
    v32 = rax31;
    rax33 = trim2("lib/mbuiter.h", 2, 0x8f, "mbuiter_multi_next");
    rsp34 = reinterpret_cast<struct s62*>(rsp4 - 1 + 1 - 1 - 1 - 1 - 1 - 1 - 18 - 1 + 1);
    zf35 = g10646 == 0;
    r13_36 = rax33;
    if (zf35) {
        addr_6fb3_20:
    } else {
        rbp37 = rsp34;
        do {
            addr_6e22_22:
            rsi30 = r13_36;
            rax38 = mbsstr(r12_29, rsi30, r12_29, rsi30);
            rsp34 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
            rbx39 = rax38;
            if (!rax38) 
                goto addr_6fb3_20;
            rax40 = fun_2710();
            rsp34 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
            if (rax40 <= 1) {
                rax41 = fun_2720(r13_36, r13_36);
                rsp34 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                if (reinterpret_cast<unsigned char>(rbx39) <= reinterpret_cast<unsigned char>(r12_29)) 
                    goto addr_6deb_25;
                rax42 = fun_2b10(r13_36, rsi30, r13_36, rsi30);
                rsp34 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                *reinterpret_cast<uint32_t*>(&rdx43) = *reinterpret_cast<unsigned char*>(rbx39 + 0xffffffffffffffff);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx43) + 4) = 0;
                if ((*rax42)[rdx43 * 2] & 8) 
                    goto addr_6e0b_27;
                addr_6deb_25:
                *reinterpret_cast<uint32_t*>(&r12_44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbx39) + reinterpret_cast<unsigned char>(rax41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_44) + 4) = 0;
                if (!*reinterpret_cast<signed char*>(&r12_44)) 
                    goto addr_6ff3_28;
                rax45 = fun_2b10(r13_36, rsi30, r13_36, rsi30);
                rsp34 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                if (!((*rax45)[r12_44 * 2] & 8)) 
                    goto addr_6ff3_28;
                addr_6e0b_27:
                if (!*reinterpret_cast<void***>(rbx39)) 
                    goto addr_6fb3_20;
                r12_29 = rbx39 + 1;
                if (!*reinterpret_cast<void***>(rbx39 + 1)) 
                    goto addr_6fb3_20; else 
                    goto addr_6e22_22;
            }
            v46 = r12_29;
            if (reinterpret_cast<unsigned char>(rbx39) <= reinterpret_cast<unsigned char>(r12_29)) {
                addr_6fe8_32:
                *reinterpret_cast<int32_t*>(&r14_47) = 1;
            } else {
                do {
                    mbuiter_multi_next_part_0(rbp37, rsi30, rbp37, rsi30);
                    rsp34 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                    if (!v48) 
                        continue;
                    if (!v49) 
                        goto addr_2b40_36;
                    rax50 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v51) + reinterpret_cast<unsigned char>(v46));
                    v46 = rax50;
                    edx52 = v53;
                } while (reinterpret_cast<unsigned char>(rax50) < reinterpret_cast<unsigned char>(rbx39));
                if (!*reinterpret_cast<signed char*>(&edx52)) 
                    goto addr_6fe8_32; else 
                    goto addr_6ea9_39;
            }
            addr_6eb4_40:
            r12_54 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp34) + 64);
            while ((mbuiter_multi_next_part_0(r12_54, rsi30, r12_54, rsi30), rsp34 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8), v55 == 0) || v56) {
                if (!0) {
                    mbuiter_multi_next_part_0(rbp37, rsi30, rbp37, rsi30);
                    rsp34 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                }
                if (!v57) 
                    goto addr_6f0a_45;
                if (!v58) 
                    goto addr_2b40_36;
                addr_6f0a_45:
            }
            if (!0) {
                mbuiter_multi_next_part_0(rbp37, rsi30, rbp37, rsi30);
                rsp34 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
            }
            if (!v59) 
                goto addr_6f5d_50;
            if (!v60) 
                goto addr_6f6d_52;
            addr_6f5d_50:
            if (!v61) 
                goto addr_6f6d_52;
            eax62 = fun_2910();
            rsp34 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
            if (eax62) 
                continue;
            addr_6f6d_52:
            if (*reinterpret_cast<unsigned char*>(&r14_47)) 
                break; else 
                continue;
            addr_6ea9_39:
            eax63 = fun_2910();
            rsp34 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
            *reinterpret_cast<unsigned char*>(&r14_47) = reinterpret_cast<uint1_t>(eax63 == 0);
            goto addr_6eb4_40;
            mbuiter_multi_next_part_0(rbp37, rsi30, rbp37, rsi30);
            rsp34 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
        } while ((!v64 || v65) && (r12_29 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx39) + reinterpret_cast<uint64_t>(v66)), !!*reinterpret_cast<void***>(r12_29)));
        goto addr_6fb3_20;
    }
    addr_6fb6_55:
    fun_25e0(r13_36, rsi30, r13_36, rsi30);
    rax67 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v32) - reinterpret_cast<unsigned char>(g28));
    if (rax67) {
        fun_2740();
    } else {
        goto v5;
    }
    addr_6ff3_28:
    goto addr_6fb6_55;
    addr_2b40_36:
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
}

signed char mbsstr_trimmed_wordbounded(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r12_5;
    void** rsi6;
    void** rax7;
    void** v8;
    void** rax9;
    struct s62* rsp10;
    void** r13_11;
    unsigned char* r14_12;
    struct s62* rbp13;
    void** rax14;
    void** rbx15;
    uint64_t rax16;
    void** rax17;
    unsigned char** rax18;
    int64_t rdx19;
    int64_t r12_20;
    unsigned char** rax21;
    void** v22;
    signed char v23;
    int32_t v24;
    void** rax25;
    void* v26;
    uint32_t edx27;
    unsigned char v28;
    struct s62* r12_29;
    signed char v30;
    int32_t v31;
    signed char v32;
    int32_t v33;
    signed char v34;
    int32_t v35;
    signed char v36;
    int32_t eax37;
    int32_t eax38;
    signed char v39;
    int32_t v40;
    void* v41;
    void* rax42;
    int32_t eax43;

    r12_5 = rdi;
    *reinterpret_cast<int32_t*>(&rsi6) = 2;
    *reinterpret_cast<int32_t*>(&rsi6 + 4) = 0;
    rax7 = g28;
    v8 = rax7;
    rax9 = trim2(rsi, 2, rdx, rcx);
    rsp10 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 0x90 - 8 + 8);
    r13_11 = rax9;
    if (!*reinterpret_cast<void***>(r12_5)) {
        addr_6fb3_2:
        *reinterpret_cast<int32_t*>(&r14_12) = 0;
    } else {
        rbp13 = rsp10;
        do {
            addr_6e22_4:
            rsi6 = r13_11;
            rax14 = mbsstr(r12_5, rsi6);
            rsp10 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
            rbx15 = rax14;
            if (!rax14) 
                goto addr_6fb3_2;
            rax16 = fun_2710();
            rsp10 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
            if (rax16 <= 1) {
                rax17 = fun_2720(r13_11, r13_11);
                rsp10 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                r14_12 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbx15) + reinterpret_cast<unsigned char>(rax17));
                if (reinterpret_cast<unsigned char>(rbx15) <= reinterpret_cast<unsigned char>(r12_5)) 
                    goto addr_6deb_7;
                rax18 = fun_2b10(r13_11, rsi6);
                rsp10 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                *reinterpret_cast<uint32_t*>(&rdx19) = *reinterpret_cast<unsigned char*>(rbx15 + 0xffffffffffffffff);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx19) + 4) = 0;
                if ((*rax18)[rdx19 * 2] & 8) 
                    goto addr_6e0b_9;
                addr_6deb_7:
                *reinterpret_cast<uint32_t*>(&r12_20) = *r14_12;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_20) + 4) = 0;
                if (!*reinterpret_cast<signed char*>(&r12_20)) 
                    goto addr_6ff3_10;
                rax21 = fun_2b10(r13_11, rsi6);
                rsp10 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                if (!((*rax21)[r12_20 * 2] & 8)) 
                    goto addr_6ff3_10;
                addr_6e0b_9:
                if (!*reinterpret_cast<void***>(rbx15)) 
                    goto addr_6fb3_2;
                r12_5 = rbx15 + 1;
                if (!*reinterpret_cast<void***>(rbx15 + 1)) 
                    goto addr_6fb3_2; else 
                    goto addr_6e22_4;
            }
            v22 = r12_5;
            if (reinterpret_cast<unsigned char>(rbx15) <= reinterpret_cast<unsigned char>(r12_5)) {
                addr_6fe8_14:
                *reinterpret_cast<int32_t*>(&r14_12) = 1;
            } else {
                do {
                    mbuiter_multi_next_part_0(rbp13, rsi6);
                    rsp10 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                    if (!v23) 
                        continue;
                    if (!v24) 
                        goto addr_2b40_18;
                    rax25 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v26) + reinterpret_cast<unsigned char>(v22));
                    v22 = rax25;
                    edx27 = v28;
                } while (reinterpret_cast<unsigned char>(rax25) < reinterpret_cast<unsigned char>(rbx15));
                if (!*reinterpret_cast<signed char*>(&edx27)) 
                    goto addr_6fe8_14; else 
                    goto addr_6ea9_21;
            }
            addr_6eb4_22:
            r12_29 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp10) + 64);
            while ((mbuiter_multi_next_part_0(r12_29, rsi6), rsp10 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8), v30 == 0) || v31) {
                if (!0) {
                    mbuiter_multi_next_part_0(rbp13, rsi6);
                    rsp10 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                }
                if (!v32) 
                    goto addr_6f0a_27;
                if (!v33) 
                    goto addr_2b40_18;
                addr_6f0a_27:
            }
            if (!0) {
                mbuiter_multi_next_part_0(rbp13, rsi6);
                rsp10 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
            }
            if (!v34) 
                goto addr_6f5d_32;
            if (!v35) 
                goto addr_6f6d_34;
            addr_6f5d_32:
            if (!v36) 
                goto addr_6f6d_34;
            eax37 = fun_2910();
            rsp10 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
            if (eax37) 
                continue;
            addr_6f6d_34:
            if (*reinterpret_cast<unsigned char*>(&r14_12)) 
                break; else 
                continue;
            addr_6ea9_21:
            eax38 = fun_2910();
            rsp10 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
            *reinterpret_cast<unsigned char*>(&r14_12) = reinterpret_cast<uint1_t>(eax38 == 0);
            goto addr_6eb4_22;
            mbuiter_multi_next_part_0(rbp13, rsi6);
            rsp10 = reinterpret_cast<struct s62*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
        } while ((!v39 || v40) && (r12_5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx15) + reinterpret_cast<uint64_t>(v41)), !!*reinterpret_cast<void***>(r12_5)));
        goto addr_6fb3_2;
    }
    addr_6fb6_37:
    fun_25e0(r13_11, rsi6, r13_11, rsi6);
    rax42 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
    if (rax42) {
        fun_2740();
    } else {
        eax43 = *reinterpret_cast<int32_t*>(&r14_12);
        return *reinterpret_cast<signed char*>(&eax43);
    }
    addr_6ff3_10:
    *reinterpret_cast<int32_t*>(&r14_12) = 1;
    goto addr_6fb6_37;
    addr_2b40_18:
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
    fun_25f0();
}

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2710();
    if (r8d > 10) {
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x10720 + rax11 * 4) + 0x10720;
    }
}

struct s63 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

int32_t* fun_2600();

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

void fun_2820();

struct s64 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(int32_t edi, int64_t rsi, int64_t rdx, struct s63* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    int32_t* rax8;
    void** r15_9;
    int32_t v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s64* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = edi;
    rax6 = g28;
    v7 = 0x88cf;
    rax8 = fun_2600();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
        fun_25f0();
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0x15090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0xc621]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x895b;
            fun_2820();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s64*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0x15120) {
                fun_25e0(r14_19, rsi24, r14_19, rsi24);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x89ea);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2740();
        } else {
            return r14_19;
        }
    }
}

void** mbslen(void** rdi);

void** mmalloca();

struct s65 {
    void** f0;
    signed char[7] pad8;
    int64_t f8;
};

struct s66 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    unsigned char f10;
    signed char[3] pad20;
    int32_t f14;
};

struct s67 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    signed char f10;
    signed char[3] pad20;
    int32_t f14;
};

uint32_t fun_2860(void** rdi, void** rsi, void** rdx, ...);

void** g8f;

void freea(void** rdi, void** rsi, ...);

struct s68 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    signed char f10;
    signed char[3] pad20;
    int32_t f14;
};

/* mbuiter_multi_next.part.0 */
void mbuiter_multi_next_part_0(void** rdi, void** rsi, ...) {
    void** rbp3;
    int64_t* rsp4;
    int64_t v5;
    int64_t rbx6;
    uint64_t rax7;
    void** r12_8;
    void** rdx9;
    void** rax10;
    void** rax11;
    void* rsp12;
    uint32_t ecx13;
    uint32_t eax14;
    int64_t rax15;
    uint32_t eax16;
    uint32_t eax17;
    void** rsi18;
    void** r8_19;
    void** r9_20;
    void** eax21;
    void** rdi22;
    void** rax23;
    void** rax24;
    void** r8_25;
    void** r9_26;
    void** r8_27;
    void** r9_28;
    uint32_t eax29;
    void* rsp30;
    void* rbp31;
    void** rax32;
    void** v33;
    void** rax34;
    void* rsp35;
    void** v36;
    int64_t rdx37;
    uint64_t rdi38;
    uint64_t rdi39;
    void* rdx40;
    uint64_t rax41;
    void* rax42;
    void** r12_43;
    void** rax44;
    void** v45;
    void** r14_46;
    void** r15_47;
    void** r13_48;
    struct s65* rbx49;
    void* rax50;
    void*** rsp51;
    uint32_t eax52;
    unsigned char v53;
    int32_t v54;
    void** rcx55;
    void** rdx56;
    void** v57;
    void** v58;
    void*** rsp59;
    void** r8_60;
    void*** rsp61;
    void** r8_62;
    uint32_t eax63;
    int32_t v64;
    uint64_t r14_65;
    struct s66* r13_66;
    void** v67;
    uint32_t r9d68;
    struct s67* rax69;
    void** rdx70;
    void** rdi71;
    void*** rsp72;
    uint32_t eax73;
    void** r13_74;
    void** v75;
    void** v76;
    void** v77;
    void*** rsp78;
    void*** rsp79;
    void*** rsp80;
    void*** rsp81;
    void** rax82;
    void*** rsp83;
    void*** rsp84;
    signed char v85;
    void*** rsp86;
    signed char v87;
    signed char v88;
    int32_t v89;
    void* v90;
    void* v91;
    void** r14_92;
    void** v93;
    void** v94;
    struct s68* rax95;
    void** rdi96;
    void*** rsp97;
    uint32_t eax98;
    void** r14_99;
    void** v100;
    signed char v101;
    signed char v102;
    int32_t v103;
    void* v104;
    int32_t v105;
    void*** rsp106;

    rbp3 = rdi + 4;
    rsp4 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8);
    v5 = rbx6;
    if (*reinterpret_cast<void***>(rdi)) {
        addr_b367_2:
        rax7 = fun_2710();
        r12_8 = *reinterpret_cast<void***>(rdi + 16);
        rax10 = strnlen1(r12_8, rax7, rdx9);
        rax11 = rpl_mbrtowc(rdi + 36, r12_8, rax10, rbp3);
        rsp12 = reinterpret_cast<void*>(rsp4 - 1 + 1 - 1 + 1 - 1 + 1);
        *reinterpret_cast<void***>(rdi + 24) = rax11;
        if (rax11 == 0xffffffffffffffff) {
            *reinterpret_cast<void***>(rdi + 24) = reinterpret_cast<void**>(1);
            *reinterpret_cast<void***>(rdi + 32) = reinterpret_cast<void**>(0);
            *reinterpret_cast<signed char*>(rdi + 12) = 1;
            return;
        }
    } else {
        rdx9 = *reinterpret_cast<void***>(rdi + 16);
        ecx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx9));
        eax14 = ecx13;
        *reinterpret_cast<unsigned char*>(&eax14) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax14) >> 5);
        *reinterpret_cast<uint32_t*>(&rax15) = eax14 & 7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
        eax16 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(0x10e40) + reinterpret_cast<uint64_t>(rax15 * 4))) >> *reinterpret_cast<signed char*>(&ecx13);
        if (!(*reinterpret_cast<unsigned char*>(&eax16) & 1)) {
            rbp3 = rdi + 4;
            eax17 = fun_2af0(rbp3, 0x10e40);
            rsp4 = rsp4 - 1 + 1;
            if (!eax17) {
                addr_b450_6:
                rsi18 = reinterpret_cast<void**>("lib/mbuiter.h");
                fun_2810("mbsinit (&iter->state)", "lib/mbuiter.h", 0x8f, "mbuiter_multi_next", r8_19, r9_20);
                goto addr_b470_7;
            } else {
                *reinterpret_cast<void***>(rdi) = reinterpret_cast<void**>(1);
                goto addr_b367_2;
            }
        } else {
            *reinterpret_cast<void***>(rdi + 24) = reinterpret_cast<void**>(1);
            eax21 = reinterpret_cast<void**>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdx9))));
            *reinterpret_cast<void***>(rdi + 32) = reinterpret_cast<void**>(1);
            *reinterpret_cast<void***>(rdi + 36) = eax21;
            goto addr_b341_10;
        }
    }
    if (rax11 == 0xfffffffffffffffe) {
        rdi22 = *reinterpret_cast<void***>(rdi + 16);
        rax23 = fun_2720(rdi22, rdi22);
        *reinterpret_cast<void***>(rdi + 32) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rdi + 24) = rax23;
        *reinterpret_cast<signed char*>(rdi + 12) = 1;
        return;
    }
    if (rax11) 
        goto addr_b3ba_14;
    rax24 = *reinterpret_cast<void***>(rdi + 16);
    *reinterpret_cast<void***>(rdi + 24) = reinterpret_cast<void**>(1);
    if (*reinterpret_cast<void***>(rax24)) {
        addr_b431_16:
        fun_2810("*iter->cur.ptr == '\\0'", "lib/mbuiter.h", 0xab, "mbuiter_multi_next", r8_25, r9_26);
        rsp4 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
        goto addr_b450_6;
    } else {
        if (*reinterpret_cast<void***>(rdi + 36)) {
            fun_2810("iter->cur.wc == 0", "lib/mbuiter.h", 0xac, "mbuiter_multi_next", r8_27, r9_28);
            rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
            goto addr_b431_16;
        } else {
            addr_b3ba_14:
            *reinterpret_cast<void***>(rdi + 32) = reinterpret_cast<void**>(1);
            eax29 = fun_2af0(rbp3, r12_8, rbp3, r12_8);
            if (eax29) {
                *reinterpret_cast<void***>(rdi) = reinterpret_cast<void**>(0);
                *reinterpret_cast<signed char*>(rdi + 12) = 1;
                return;
            }
        }
    }
    addr_b341_10:
    *reinterpret_cast<signed char*>(rdi + 12) = 1;
    return;
    addr_b470_7:
    rsp30 = reinterpret_cast<void*>(rsp4 - 1 + 1 - 1);
    rbp31 = rsp30;
    rax32 = g28;
    v33 = rax32;
    rax34 = mbslen("lib/mbuiter.h");
    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 - 8 - 8 - 8 - 8 - 0xc8 - 8 + 8);
    v36 = rax34;
    if (56 * reinterpret_cast<unsigned char>(rax34) < reinterpret_cast<int64_t>(0)) 
        goto addr_b550_20;
    *reinterpret_cast<uint32_t*>(&rdx37) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx37) + 4) = 0;
    if (rdx37) 
        goto addr_b550_20;
    rdi38 = reinterpret_cast<unsigned char>(v36) * 8 - reinterpret_cast<unsigned char>(v36) << 3;
    if (rdi38 <= reinterpret_cast<uint64_t>("alloc")) {
        rdi39 = rdi38 + 54;
        rdx40 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp35) - (rdi39 & 0xfffffffffffff000));
        rax41 = rdi39 & 0xfffffffffffffff0;
        if (rsp35 != rdx40) {
            do {
                rsp35 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp35) - reinterpret_cast<int64_t>("strcmp"));
            } while (rsp35 != rdx40);
        }
        *reinterpret_cast<uint32_t*>(&rax42) = *reinterpret_cast<uint32_t*>(&rax41) & 0xfff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax42) + 4) = 0;
        rsp35 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp35) - reinterpret_cast<int64_t>(rax42));
        if (rax42) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp35) + reinterpret_cast<int64_t>(rax42) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp35) + reinterpret_cast<int64_t>(rax42) - 8);
        }
        r12_43 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rsp35) + 15 & 0xfffffffffffffff0) + 31 & 0xffffffffffffffe0);
        if (r12_43) 
            goto addr_b58d_28; else 
            goto addr_b550_20;
    }
    rax44 = mmalloca();
    rsp35 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp35) - 8 + 8);
    r12_43 = rax44;
    if (!r12_43) {
        addr_b550_20:
    } else {
        addr_b58d_28:
        v45 = reinterpret_cast<void**>("lib/mbuiter.h");
        r14_46 = r12_43 + 24;
        r15_47 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp31) + 0xffffffffffffff80);
        r13_48 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp31) + 0xffffffffffffffa8);
        rbx49 = reinterpret_cast<struct s65*>((reinterpret_cast<uint64_t>(v36 + reinterpret_cast<unsigned char>(v36) * 2) << 4) + reinterpret_cast<unsigned char>(r12_43));
        goto addr_b5f7_30;
    }
    addr_b557_31:
    rax50 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v33) - reinterpret_cast<unsigned char>(g28));
    if (rax50) {
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp35) - 8) = reinterpret_cast<void**>(0xb97b);
        fun_2740();
    } else {
        goto v5;
    }
    while (1) {
        addr_b5f7_30:
        rsp51 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp35) - 8);
        *rsp51 = reinterpret_cast<void**>(0xb603);
        mbuiter_multi_next_part_0(r15_47, rsi18, r15_47, rsi18);
        rsp35 = reinterpret_cast<void*>(rsp51 + 8);
        eax52 = v53;
        if (*reinterpret_cast<unsigned char*>(&eax52)) {
            if (!v54) 
                break;
            rcx55 = v45;
            rdx56 = v57;
            if (rcx55 != r13_48) 
                goto addr_b5d8_37;
        } else {
            rcx55 = v45;
            rdx56 = v58;
            if (rcx55 == r13_48) {
                rsi18 = r13_48;
                rsp59 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp35) - 8);
                *rsp59 = reinterpret_cast<void**>(0xb917);
                fun_28d0(r14_46, rsi18, rdx56, rcx55, r8_60);
                rsp35 = reinterpret_cast<void*>(rsp59 + 8);
                rdx56 = rdx56;
                *reinterpret_cast<void***>(r14_46 + 0xffffffffffffffe8) = r14_46;
                *reinterpret_cast<signed char*>(r14_46 + 0xfffffffffffffff8) = 0;
                rcx55 = rcx55;
                *reinterpret_cast<void***>(r14_46 + 0xfffffffffffffff0) = rdx56;
                goto addr_b5ec_40;
            } else {
                *reinterpret_cast<void***>(r14_46 + 0xffffffffffffffe8) = rcx55;
                *reinterpret_cast<void***>(r14_46 + 0xfffffffffffffff0) = rdx56;
                *reinterpret_cast<signed char*>(r14_46 + 0xfffffffffffffff8) = 0;
                goto addr_b5ec_40;
            }
        }
        rsi18 = r13_48;
        rsp61 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp35) - 8);
        *rsp61 = reinterpret_cast<void**>(0xb950);
        fun_28d0(r14_46, rsi18, rdx56, rcx55, r8_62);
        rsp35 = reinterpret_cast<void*>(rsp61 + 8);
        rdx56 = rdx56;
        *reinterpret_cast<void***>(r14_46 + 0xffffffffffffffe8) = r14_46;
        eax63 = *reinterpret_cast<unsigned char*>(&eax52);
        rcx55 = rcx55;
        *reinterpret_cast<void***>(r14_46 + 0xfffffffffffffff0) = rdx56;
        *reinterpret_cast<signed char*>(r14_46 + 0xfffffffffffffff8) = *reinterpret_cast<signed char*>(&eax63);
        addr_b5e5_43:
        *reinterpret_cast<int32_t*>(r14_46 + 0xfffffffffffffffc) = v64;
        addr_b5ec_40:
        r14_46 = r14_46 + 48;
        v45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx55) + reinterpret_cast<unsigned char>(rdx56));
        continue;
        addr_b5d8_37:
        *reinterpret_cast<void***>(r14_46 + 0xffffffffffffffe8) = rcx55;
        *reinterpret_cast<void***>(r14_46 + 0xfffffffffffffff0) = rdx56;
        *reinterpret_cast<signed char*>(r14_46 + 0xfffffffffffffff8) = 1;
        goto addr_b5e5_43;
    }
    *reinterpret_cast<int32_t*>(&r14_65) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_65) + 4) = 0;
    r13_66 = reinterpret_cast<struct s66*>(r12_43 + 48);
    rbx49->f8 = 1;
    v67 = reinterpret_cast<void**>(2);
    if (reinterpret_cast<unsigned char>(v36) > reinterpret_cast<unsigned char>(2)) {
        do {
            r9d68 = r13_66->f10;
            while (1) {
                rax69 = reinterpret_cast<struct s67*>((r14_65 + r14_65 * 2 << 4) + reinterpret_cast<unsigned char>(r12_43));
                if (!*reinterpret_cast<unsigned char*>(&r9d68) || !rax69->f10) {
                    rdx70 = r13_66->f8;
                    if (rdx70 != rax69->f8) 
                        goto addr_b67f_49;
                    rsi18 = rax69->f0;
                    rdi71 = r13_66->f0;
                    rsp72 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp35) - 8);
                    *rsp72 = reinterpret_cast<void**>(0xb6b9);
                    eax73 = fun_2860(rdi71, rsi18, rdx70);
                    rsp35 = reinterpret_cast<void*>(rsp72 + 8);
                    r9d68 = *reinterpret_cast<unsigned char*>(&r9d68);
                    if (!eax73) 
                        break;
                } else {
                    if (r13_66->f14 == rax69->f14) 
                        break;
                }
                addr_b67f_49:
                if (!r14_65) 
                    goto addr_b850_52;
                r14_65 = r14_65 - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx49) + r14_65 * 8));
            }
            ++r14_65;
            *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx49) + reinterpret_cast<unsigned char>(v67) * 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v67) - r14_65);
            continue;
            addr_b850_52:
            *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx49) + reinterpret_cast<unsigned char>(v67) * 8) = v67;
            ++v67;
            r13_66 = r13_66 + 2;
        } while (v36 != v67);
    }
    *reinterpret_cast<int32_t*>(&r13_74) = 0;
    *reinterpret_cast<int32_t*>(&r13_74 + 4) = 0;
    g8f = reinterpret_cast<void**>(0);
    v75 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp31) + 0xffffffffffffff40);
    v76 = reinterpret_cast<void**>("mbsinit (&iter->state)");
    v77 = reinterpret_cast<void**>("mbsinit (&iter->state)");
    goto addr_b756_57;
    addr_b8f1_58:
    rsp78 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp35) - 8);
    *rsp78 = reinterpret_cast<void**>(0xb8f9);
    freea(r12_43, rsi18, r12_43, rsi18);
    rsp35 = reinterpret_cast<void*>(rsp78 + 8);
    goto addr_b557_31;
    addr_2b82_59:
    rsp79 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp35) - 8);
    *rsp79 = reinterpret_cast<void**>(0x2b87);
    fun_25f0();
    rsp80 = rsp79 + 8 - 8;
    *rsp80 = reinterpret_cast<void**>(0x2b8c);
    fun_25f0();
    rsp81 = rsp80 + 8 - 8;
    *rsp81 = reinterpret_cast<void**>(0x2b91);
    rax82 = fun_25f0();
    rsp83 = rsp81 + 8 - 8;
    *rsp83 = rax82;
    *reinterpret_cast<int64_t*>(rsp83 - 8) = 0x2b97;
    fun_25f0();
    addr_b8e0_63:
    g8f = v76;
    goto addr_b8f1_58;
    while (1) {
        addr_b8b9_64:
        rsp84 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp35) - 8);
        *rsp84 = reinterpret_cast<void**>(0xb8c5);
        mbuiter_multi_next_part_0(v75, rsi18, v75, rsi18);
        rsp35 = reinterpret_cast<void*>(rsp84 + 8);
        if (!v85) 
            goto addr_b89e_65;
        goto addr_b890_67;
        while (1) {
            addr_b7a6_68:
            rsp86 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp35) - 8);
            *rsp86 = reinterpret_cast<void**>(0xb7ae);
            mbuiter_multi_next_part_0(r15_47, rsi18, r15_47, rsi18);
            rsp35 = reinterpret_cast<void*>(rsp86 + 8);
            if (v87) 
                goto addr_b760_69; else 
                goto addr_b7b4_70;
            addr_b7e8_71:
            if (!v88) 
                goto addr_b7ff_72;
            if (!v89) 
                goto addr_2b82_59;
            addr_b7ff_72:
            v76 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v76) + reinterpret_cast<uint64_t>(v90));
            v77 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v77) + reinterpret_cast<uint64_t>(v91));
            continue;
            addr_b849_74:
            addr_b78a_75:
            rsi18 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi18) + reinterpret_cast<unsigned char>(r14_92));
            ++r13_74;
            v77 = rsi18;
            if (v36 == r13_74) 
                goto addr_b8e0_63; else 
                continue;
            addr_b782_76:
            r14_92 = v93;
            rsi18 = v77;
            goto addr_b78a_75;
            addr_b7c0_77:
            while ((r14_92 = v94, rax95->f8 != r14_92) || (rdi96 = rax95->f0, rsp97 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp35) - 8), *rsp97 = reinterpret_cast<void**>(0xb83e), eax98 = fun_2860(rdi96, v77, r14_92), rsp35 = reinterpret_cast<void*>(rsp97 + 8), rsi18 = v77, !!eax98)) {
                while (1) {
                    if (!r13_74) 
                        goto addr_b7d3_79;
                    r14_99 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx49) + reinterpret_cast<unsigned char>(r13_74) * 8);
                    v100 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_74) - reinterpret_cast<unsigned char>(r14_99));
                    if (!r14_99) {
                        addr_b756_57:
                        if (1) 
                            goto addr_b7a6_68;
                    } else {
                        if (1) 
                            goto addr_b8b9_64;
                        if (!v101) 
                            goto addr_b89e_65; else 
                            goto addr_b890_67;
                    }
                    if (!v102) 
                        break;
                    addr_b760_69:
                    if (!v103) 
                        goto addr_b8f1_58;
                    rax95 = reinterpret_cast<struct s68*>((reinterpret_cast<uint64_t>(r13_74 + reinterpret_cast<unsigned char>(r13_74) * 2) << 4) + reinterpret_cast<unsigned char>(r12_43));
                    if (!rax95->f10) 
                        goto addr_b7c0_77;
                    if (rax95->f14 != v103) 
                        continue; else 
                        goto addr_b782_76;
                    addr_b89e_65:
                    v76 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v76) + reinterpret_cast<uint64_t>(v104));
                    --r14_99;
                    if (r14_99) 
                        goto addr_b8b9_64;
                    r13_74 = v100;
                    goto addr_b756_57;
                    addr_b890_67:
                    if (!v105) 
                        goto addr_2b82_59; else 
                        goto addr_b89e_65;
                }
                addr_b7b4_70:
                rax95 = reinterpret_cast<struct s68*>((reinterpret_cast<uint64_t>(r13_74 + reinterpret_cast<unsigned char>(r13_74) * 2) << 4) + reinterpret_cast<unsigned char>(r12_43));
            }
            goto addr_b849_74;
            addr_b7d3_79:
            if (!0) {
                rsp106 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp35) - 8);
                *rsp106 = reinterpret_cast<void**>(0xb7e8);
                mbuiter_multi_next_part_0(v75, rsi18, v75, rsi18);
                rsp35 = reinterpret_cast<void*>(rsp106 + 8);
                goto addr_b7e8_71;
            }
        }
    }
}

struct s69 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s70 {
    signed char[4088] pad4088;
    uint64_t fff8;
};

struct s71 {
    void** f0;
    signed char[7] pad8;
    int64_t f8;
    void** f10;
};

struct s72 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    signed char f10;
    signed char[3] pad20;
    int32_t f14;
};

struct s73 {
    signed char[67142] pad67142;
    void** f10646;
};

struct s74 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    signed char f10;
    signed char[3] pad20;
    int32_t f14;
};

uint32_t knuth_morris_pratt(void** rdi, void** rsi, void** rdx, void*** rcx, void** r8, void** r9) {
    void* rsp7;
    void* rbp8;
    void* rsp9;
    void** rax10;
    void** v11;
    void** r12_12;
    void** rbx13;
    void** r13_14;
    void*** r14_15;
    uint64_t rdi16;
    uint64_t rdi17;
    void* rdx18;
    uint64_t rax19;
    void* rax20;
    void** rax21;
    uint32_t eax22;
    void* rdx23;
    void** rcx24;
    uint32_t eax25;
    void** rdx26;
    void*** rsp27;
    void*** rsp28;
    void** rsp29;
    void** rbp30;
    void*** rsp31;
    struct s69* rsp32;
    uint64_t rax33;
    struct s69* rsp34;
    void** rax35;
    struct s69* rsp36;
    void** rax37;
    void*** rsp38;
    void** rdi39;
    struct s69* rsp40;
    void** rax41;
    void** rax42;
    struct s69* rsp43;
    struct s69* rsp44;
    struct s69* rsp45;
    uint32_t eax46;
    uint32_t ecx47;
    uint32_t eax48;
    int64_t rax49;
    uint32_t eax50;
    struct s69* rsp51;
    uint32_t eax52;
    void** rsi53;
    struct s69* rsp54;
    void** eax55;
    struct s69* rsp56;
    struct s69* rbp57;
    int64_t* rsp58;
    int64_t r15_59;
    void**** rsp60;
    void*** rsp61;
    void*** rsp62;
    void*** rsp63;
    void** rax64;
    int64_t* rsp65;
    void** rax66;
    struct s70* rsp67;
    int64_t rdx68;
    uint64_t rdi69;
    uint64_t rdi70;
    struct s70* rdx71;
    uint64_t rax72;
    void* rax73;
    void** r12_74;
    void*** rsp75;
    void** rax76;
    void** rax77;
    void** r14_78;
    void** r15_79;
    void** r13_80;
    struct s71* rbx81;
    void* rax82;
    void*** rsp83;
    uint32_t eax84;
    void** rdx85;
    void*** rsp86;
    void*** rsp87;
    uint32_t eax88;
    uint64_t r14_89;
    int1_t below_or_equal90;
    struct s66* r13_91;
    uint32_t r9d92;
    struct s72* rax93;
    void** rdx94;
    void** rdi95;
    void*** rsp96;
    uint32_t eax97;
    void** r13_98;
    uint32_t edx99;
    void*** rsp100;
    void** rcx101;
    void** rax102;
    void*** rsp103;
    void*** rsp104;
    void*** rsp105;
    void*** rsp106;
    void** rax107;
    void*** rsp108;
    void** rdi109;
    void*** rsp110;
    void*** rsp111;
    struct s73* rax112;
    void** rax113;
    void** r14_114;
    struct s74* rax115;
    void** rdi116;
    void*** rsp117;
    uint32_t eax118;
    void** r14_119;
    struct s73* rdx120;
    void** rdi121;
    void*** rsp122;

    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp8 = rsp7;
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 - 8 - 8 - 8 - 16);
    rax10 = g28;
    v11 = rax10;
    if (__intrinsic()) 
        goto addr_b20c_2;
    r12_12 = rdi;
    rbx13 = rsi;
    r13_14 = rdx;
    r14_15 = rcx;
    rdi16 = reinterpret_cast<unsigned char>(rdx) * 8;
    if (rdi16 <= reinterpret_cast<uint64_t>("alloc")) {
        rdi17 = rdi16 + 54;
        rdx18 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp9) - (rdi17 & 0xfffffffffffff000));
        rax19 = rdi17 & 0xfffffffffffffff0;
        if (rsp9 != rdx18) {
            do {
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp9) - reinterpret_cast<uint64_t>("strcmp"));
            } while (rsp9 != rdx18);
        }
        *reinterpret_cast<uint32_t*>(&rax20) = *reinterpret_cast<uint32_t*>(&rax19) & 0xfff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax20) + 4) = 0;
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp9) - reinterpret_cast<uint64_t>(rax20));
        if (rax20) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp9) + reinterpret_cast<uint64_t>(rax20) + 0xfffffffffffffff8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp9) + reinterpret_cast<uint64_t>(rax20) + 0xfffffffffffffff8);
        }
        rdi = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rsp9) + 15 & 0xfffffffffffffff0) + 31 & 0xffffffffffffffe0);
        if (rdi) 
            goto addr_b23b_9; else 
            goto addr_b20c_2;
    }
    rax21 = mmalloca();
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp9) - 8 + 8);
    rdi = rax21;
    if (!rdi) {
        addr_b20c_2:
        eax22 = 0;
    } else {
        addr_b23b_9:
        *reinterpret_cast<void***>(rdi + 8) = reinterpret_cast<void**>(1);
        if (reinterpret_cast<unsigned char>(r13_14) > reinterpret_cast<unsigned char>(2)) {
            *reinterpret_cast<int32_t*>(&rdx23) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rcx24) = 2;
            *reinterpret_cast<int32_t*>(&rcx24 + 4) = 0;
            do {
                eax25 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbx13) + reinterpret_cast<unsigned char>(rcx24) + 0xffffffffffffffff);
                if (*reinterpret_cast<signed char*>(&eax25) != *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx13) + reinterpret_cast<uint64_t>(rdx23))) {
                    do {
                        if (!rdx23) 
                            break;
                        rdx23 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx23) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + reinterpret_cast<uint64_t>(rdx23) * 8)));
                    } while (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx13) + reinterpret_cast<uint64_t>(rdx23)) != *reinterpret_cast<signed char*>(&eax25));
                    goto addr_b2e0_15;
                } else {
                    goto addr_b2e0_15;
                }
                *reinterpret_cast<void***>(rdi + reinterpret_cast<unsigned char>(rcx24) * 8) = rcx24;
                *reinterpret_cast<int32_t*>(&rdx23) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0;
                continue;
                addr_b2e0_15:
                rdx23 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx23) + 1);
                *reinterpret_cast<void***>(rdi + reinterpret_cast<unsigned char>(rcx24) * 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx24) - reinterpret_cast<uint64_t>(rdx23));
                ++rcx24;
            } while (r13_14 != rcx24);
            goto addr_b27d_19;
        }
    }
    addr_b20e_20:
    rdx26 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
    if (!rdx26) {
        return eax22;
    }
    rsp27 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp9) - 8);
    *rsp27 = reinterpret_cast<void**>(0xb2f5);
    fun_2740();
    rsp28 = rsp27 + 8 - 8;
    *rsp28 = r12_12;
    rsp29 = reinterpret_cast<void**>(rsp28 - 8);
    *rsp29 = rbp8;
    rbp30 = rdi + 4;
    rsp31 = reinterpret_cast<void***>(rsp29 - 1);
    *rsp31 = rbx13;
    if (!*reinterpret_cast<void***>(rdi)) 
        goto addr_b310_24;
    addr_b367_25:
    rsp32 = reinterpret_cast<struct s69*>(rsp31 - 8);
    rsp32->f0 = reinterpret_cast<void**>(0xb36c);
    rax33 = fun_2710();
    r12_12 = *reinterpret_cast<void***>(rdi + 16);
    rsp34 = reinterpret_cast<struct s69*>(&rsp32->f8 - 8);
    rsp34->f0 = reinterpret_cast<void**>(0xb37b);
    rax35 = strnlen1(r12_12, rax33, rdx26);
    rsp36 = reinterpret_cast<struct s69*>(&rsp34->f8 - 8);
    rsp36->f0 = reinterpret_cast<void**>(0xb38d);
    rax37 = rpl_mbrtowc(rdi + 36, r12_12, rax35, rbp30);
    rsp38 = &rsp36->f8;
    *reinterpret_cast<void***>(rdi + 24) = rax37;
    if (rax37 == 0xffffffffffffffff) {
        *reinterpret_cast<void***>(rdi + 24) = reinterpret_cast<void**>(1);
        *reinterpret_cast<void***>(rdi + 32) = reinterpret_cast<void**>(0);
        *reinterpret_cast<signed char*>(rdi + 12) = 1;
        goto *reinterpret_cast<int64_t*>(rsp38 + 8 + 8 + 8);
    }
    if (rax37 == 0xfffffffffffffffe) {
        rdi39 = *reinterpret_cast<void***>(rdi + 16);
        rsp40 = reinterpret_cast<struct s69*>(rsp38 - 8);
        rsp40->f0 = reinterpret_cast<void**>(0xb401);
        rax41 = fun_2720(rdi39, rdi39);
        *reinterpret_cast<void***>(rdi + 32) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rdi + 24) = rax41;
        *reinterpret_cast<signed char*>(rdi + 12) = 1;
        goto *reinterpret_cast<int64_t*>(&rsp40->f8 + 8 + 8 + 8);
    }
    if (rax37) 
        goto addr_b3ba_30;
    rax42 = *reinterpret_cast<void***>(rdi + 16);
    *reinterpret_cast<void***>(rdi + 24) = reinterpret_cast<void**>(1);
    if (*reinterpret_cast<void***>(rax42)) {
        addr_b431_32:
        rsp43 = reinterpret_cast<struct s69*>(rsp38 - 8);
        rsp43->f0 = reinterpret_cast<void**>(0xb450);
        fun_2810("*iter->cur.ptr == '\\0'", "lib/mbuiter.h", 0xab, "mbuiter_multi_next", r8, r9);
        rsp31 = &rsp43->f8;
        goto addr_b450_33;
    } else {
        if (*reinterpret_cast<void***>(rdi + 36)) {
            rsp44 = reinterpret_cast<struct s69*>(rsp38 - 8);
            rsp44->f0 = reinterpret_cast<void**>(0xb431);
            fun_2810("iter->cur.wc == 0", "lib/mbuiter.h", 0xac, "mbuiter_multi_next", r8, r9);
            rsp38 = &rsp44->f8;
            goto addr_b431_32;
        } else {
            addr_b3ba_30:
            *reinterpret_cast<void***>(rdi + 32) = reinterpret_cast<void**>(1);
            rsp45 = reinterpret_cast<struct s69*>(rsp38 - 8);
            rsp45->f0 = reinterpret_cast<void**>(0xb3c6);
            eax46 = fun_2af0(rbp30, r12_12, rbp30, r12_12);
            rsp31 = &rsp45->f8;
            if (eax46) {
                *reinterpret_cast<void***>(rdi) = reinterpret_cast<void**>(0);
                *reinterpret_cast<signed char*>(rdi + 12) = 1;
                goto *reinterpret_cast<int64_t*>(rsp31 + 8 + 8 + 8);
            }
        }
    }
    addr_b341_37:
    *reinterpret_cast<signed char*>(rdi + 12) = 1;
    goto *reinterpret_cast<int64_t*>(rsp31 + 8 + 8 + 8);
    addr_b310_24:
    rdx26 = *reinterpret_cast<void***>(rdi + 16);
    ecx47 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx26));
    eax48 = ecx47;
    *reinterpret_cast<unsigned char*>(&eax48) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax48) >> 5);
    *reinterpret_cast<uint32_t*>(&rax49) = eax48 & 7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax49) + 4) = 0;
    eax50 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(0x10e40) + reinterpret_cast<uint64_t>(rax49 * 4))) >> *reinterpret_cast<signed char*>(&ecx47);
    if (!(*reinterpret_cast<unsigned char*>(&eax50) & 1)) {
        rbp30 = rdi + 4;
        rsp51 = reinterpret_cast<struct s69*>(rsp31 - 8);
        rsp51->f0 = reinterpret_cast<void**>(0xb35c);
        eax52 = fun_2af0(rbp30, 0x10e40);
        rsp31 = &rsp51->f8;
        if (!eax52) {
            addr_b450_33:
            rsi53 = reinterpret_cast<void**>("lib/mbuiter.h");
            rsp54 = reinterpret_cast<struct s69*>(rsp31 - 8);
            rsp54->f0 = reinterpret_cast<void**>(0xb46f);
            fun_2810("mbsinit (&iter->state)", "lib/mbuiter.h", 0x8f, "mbuiter_multi_next", r8, r9);
        } else {
            *reinterpret_cast<void***>(rdi) = reinterpret_cast<void**>(1);
            goto addr_b367_25;
        }
    } else {
        *reinterpret_cast<void***>(rdi + 24) = reinterpret_cast<void**>(1);
        eax55 = reinterpret_cast<void**>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdx26))));
        *reinterpret_cast<void***>(rdi + 32) = reinterpret_cast<void**>(1);
        *reinterpret_cast<void***>(rdi + 36) = eax55;
        goto addr_b341_37;
    }
    rsp56 = reinterpret_cast<struct s69*>(&rsp54->f8 - 8);
    rsp56->f0 = rbp30;
    rbp57 = rsp56;
    rsp58 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp56) - 8);
    *rsp58 = r15_59;
    rsp60 = reinterpret_cast<void****>(rsp58 - 1);
    *rsp60 = r14_15;
    rsp61 = reinterpret_cast<void***>(rsp60 - 1);
    *rsp61 = r13_14;
    rsp62 = rsp61 - 8;
    *rsp62 = r12_12;
    rsp63 = rsp62 - 8;
    *rsp63 = rdi;
    *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff18) = reinterpret_cast<void**>(0x8f);
    *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff10) = reinterpret_cast<void**>("mbsinit (&iter->state)");
    rax64 = g28;
    *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffffc8) = rax64;
    rsp65 = reinterpret_cast<int64_t*>(rsp63 - 0xc8 - 8);
    *rsp65 = 0xb4ac;
    rax66 = mbslen("lib/mbuiter.h");
    rsp67 = reinterpret_cast<struct s70*>(rsp65 + 1);
    (rbp57 + 0xffffffffffffffe8)->f0 = rax66;
    if (56 * reinterpret_cast<unsigned char>(rax66) < reinterpret_cast<int64_t>(0)) 
        goto addr_b550_42;
    *reinterpret_cast<uint32_t*>(&rdx68) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx68) + 4) = 0;
    if (rdx68) 
        goto addr_b550_42;
    rdi69 = reinterpret_cast<unsigned char>((rbp57 + 0xffffffffffffffe8)->f0) * 8 - reinterpret_cast<unsigned char>((rbp57 + 0xffffffffffffffe8)->f0) << 3;
    if (rdi69 <= reinterpret_cast<uint64_t>("alloc")) {
        rdi70 = rdi69 + 54;
        rdx71 = reinterpret_cast<struct s70*>(reinterpret_cast<uint64_t>(rsp67) - (rdi70 & 0xfffffffffffff000));
        rax72 = rdi70 & 0xfffffffffffffff0;
        if (rsp67 != rdx71) {
            do {
                rsp67 = reinterpret_cast<struct s70*>(reinterpret_cast<uint64_t>(rsp67) - reinterpret_cast<int64_t>("strcmp"));
                *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp67) + reinterpret_cast<int64_t>("ntf_chk")) = 0xb4ac;
            } while (rsp67 != rdx71);
        }
        *reinterpret_cast<uint32_t*>(&rax73) = *reinterpret_cast<uint32_t*>(&rax72) & 0xfff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax73) + 4) = 0;
        rsp67 = reinterpret_cast<struct s70*>(reinterpret_cast<uint64_t>(rsp67) - reinterpret_cast<int64_t>(rax73));
        if (rax73) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp67) + reinterpret_cast<int64_t>(rax73) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp67) + reinterpret_cast<int64_t>(rax73) - 8);
        }
        r12_74 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rsp67) + 15 & 0xfffffffffffffff0) + 31 & 0xffffffffffffffe0);
        if (r12_74) 
            goto addr_b58d_50; else 
            goto addr_b550_42;
    }
    rsp75 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp67) - 8);
    *rsp75 = reinterpret_cast<void**>(0xb585);
    rax76 = mmalloca();
    rsp67 = reinterpret_cast<struct s70*>(rsp75 + 8);
    r12_74 = rax76;
    if (!r12_74) {
        addr_b550_42:
        *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff27) = 0;
    } else {
        addr_b58d_50:
        rax77 = (rbp57 + 0xffffffffffffffe8)->f0;
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff90) = reinterpret_cast<void**>("lib/mbuiter.h");
        r14_78 = r12_74 + 24;
        r15_79 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff80);
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff80) = reinterpret_cast<void**>(0);
        r13_80 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffffa8);
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff84) = 0;
        rbx81 = reinterpret_cast<struct s71*>((reinterpret_cast<uint64_t>(rax77 + reinterpret_cast<unsigned char>(rax77) * 2) << 4) + reinterpret_cast<unsigned char>(r12_74));
        goto addr_b5f7_52;
    }
    addr_b557_53:
    rax82 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffffc8)) - reinterpret_cast<unsigned char>(g28));
    if (rax82) {
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp67) - 8) = reinterpret_cast<void**>(0xb97b);
        fun_2740();
    } else {
        goto *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffffd8 + 8 + 8 + 8 + 8 + 8 + 8);
    }
    while (1) {
        addr_b5f7_52:
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff8c) = 0;
        rsp83 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp67) - 8);
        *rsp83 = reinterpret_cast<void**>(0xb603);
        mbuiter_multi_next_part_0(r15_79, rsi53, r15_79, rsi53);
        rsp67 = reinterpret_cast<struct s70*>(rsp83 + 8);
        eax84 = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffffa0);
        *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff27) = *reinterpret_cast<unsigned char*>(&eax84);
        if (*reinterpret_cast<unsigned char*>(&eax84)) {
            if (!*reinterpret_cast<int32_t*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffffa4)) 
                break;
            rdx85 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff98);
            if ("lib/mbuiter.h" != r13_80) 
                goto addr_b5d8_59;
        } else {
            rdx85 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff98);
            if ("lib/mbuiter.h" == r13_80) {
                rsi53 = r13_80;
                *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff30) = reinterpret_cast<void**>("lib/mbuiter.h");
                *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff38) = rdx85;
                rsp86 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp67) - 8);
                *rsp86 = reinterpret_cast<void**>(0xb917);
                fun_28d0(r14_78, rsi53, rdx85, "lib/mbuiter.h", r8);
                rsp67 = reinterpret_cast<struct s70*>(rsp86 + 8);
                rdx85 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff38);
                *reinterpret_cast<void***>(r14_78 + 0xffffffffffffffe8) = r14_78;
                *reinterpret_cast<signed char*>(r14_78 + 0xfffffffffffffff8) = 0;
                *reinterpret_cast<void***>(r14_78 + 0xfffffffffffffff0) = rdx85;
                goto addr_b5ec_62;
            } else {
                *reinterpret_cast<void***>(r14_78 + 0xffffffffffffffe8) = reinterpret_cast<void**>("lib/mbuiter.h");
                *reinterpret_cast<void***>(r14_78 + 0xfffffffffffffff0) = rdx85;
                *reinterpret_cast<signed char*>(r14_78 + 0xfffffffffffffff8) = 0;
                goto addr_b5ec_62;
            }
        }
        rsi53 = r13_80;
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff30) = reinterpret_cast<void**>("lib/mbuiter.h");
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff38) = rdx85;
        rsp87 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp67) - 8);
        *rsp87 = reinterpret_cast<void**>(0xb950);
        fun_28d0(r14_78, rsi53, rdx85, "lib/mbuiter.h", r8);
        rsp67 = reinterpret_cast<struct s70*>(rsp87 + 8);
        rdx85 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff38);
        *reinterpret_cast<void***>(r14_78 + 0xffffffffffffffe8) = r14_78;
        eax88 = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff27);
        *reinterpret_cast<void***>(r14_78 + 0xfffffffffffffff0) = rdx85;
        *reinterpret_cast<signed char*>(r14_78 + 0xfffffffffffffff8) = *reinterpret_cast<signed char*>(&eax88);
        addr_b5e5_65:
        *reinterpret_cast<int32_t*>(r14_78 + 0xfffffffffffffffc) = *reinterpret_cast<int32_t*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffffa4);
        addr_b5ec_62:
        r14_78 = r14_78 + 48;
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff90) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>("lib/mbuiter.h") + reinterpret_cast<unsigned char>(rdx85));
        continue;
        addr_b5d8_59:
        *reinterpret_cast<void***>(r14_78 + 0xffffffffffffffe8) = reinterpret_cast<void**>("lib/mbuiter.h");
        *reinterpret_cast<void***>(r14_78 + 0xfffffffffffffff0) = rdx85;
        *reinterpret_cast<signed char*>(r14_78 + 0xfffffffffffffff8) = 1;
        goto addr_b5e5_65;
    }
    *reinterpret_cast<int32_t*>(&r14_89) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_89) + 4) = 0;
    below_or_equal90 = reinterpret_cast<unsigned char>((rbp57 + 0xffffffffffffffe8)->f0) <= reinterpret_cast<unsigned char>(2);
    r13_91 = reinterpret_cast<struct s66*>(r12_74 + 48);
    rbx81->f8 = 1;
    *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff30) = reinterpret_cast<void**>(2);
    if (!below_or_equal90) {
        do {
            r9d92 = r13_91->f10;
            while (1) {
                rax93 = reinterpret_cast<struct s72*>((r14_89 + r14_89 * 2 << 4) + reinterpret_cast<unsigned char>(r12_74));
                if (!*reinterpret_cast<void***>(&r9d92) || !rax93->f10) {
                    rdx94 = r13_91->f8;
                    if (rdx94 != rax93->f8) 
                        goto addr_b67f_71;
                    rsi53 = rax93->f0;
                    rdi95 = r13_91->f0;
                    *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff38) = *reinterpret_cast<void***>(&r9d92);
                    rsp96 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp67) - 8);
                    *rsp96 = reinterpret_cast<void**>(0xb6b9);
                    eax97 = fun_2860(rdi95, rsi53, rdx94);
                    rsp67 = reinterpret_cast<struct s70*>(rsp96 + 8);
                    r9d92 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff38));
                    if (!eax97) 
                        break;
                } else {
                    if (r13_91->f14 == rax93->f14) 
                        break;
                }
                addr_b67f_71:
                if (!r14_89) 
                    goto addr_b850_74;
                r14_89 = r14_89 - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx81) + r14_89 * 8));
            }
            ++r14_89;
            rbx81->f10 = reinterpret_cast<void**>(2 - r14_89);
            continue;
            addr_b850_74:
            rbx81->f10 = reinterpret_cast<void**>(2);
            *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff30) = reinterpret_cast<void**>(3);
            r13_91 = r13_91 + 2;
        } while (!reinterpret_cast<int1_t>((rbp57 + 0xffffffffffffffe8)->f0 == 3));
    }
    *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff80) = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&r13_98) = 0;
    *reinterpret_cast<int32_t*>(&r13_98 + 4) = 0;
    *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff40) = reinterpret_cast<void**>(0);
    g8f = reinterpret_cast<void**>(0);
    *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff44) = 0;
    *reinterpret_cast<signed char*>(&(rbp57 + 0xffffffffffffffec)->f0) = 0;
    *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff84) = 0;
    *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff8c) = 0;
    *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff38) = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff40);
    *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff50) = reinterpret_cast<void**>("mbsinit (&iter->state)");
    *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff90) = reinterpret_cast<void**>("mbsinit (&iter->state)");
    goto addr_b756_79;
    addr_b27d_19:
    *r14_15 = reinterpret_cast<void**>(0);
    edx99 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_12));
    if (!*reinterpret_cast<signed char*>(&edx99)) {
        addr_b2c1_80:
        rsp100 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp9) - 8);
        *rsp100 = reinterpret_cast<void**>(0xb2c6);
        freea(rdi, rsi);
        rsp9 = reinterpret_cast<void*>(rsp100 + 8);
        eax22 = 1;
        goto addr_b20e_20;
    } else {
        rcx101 = r12_12;
        *reinterpret_cast<int32_t*>(&rax102) = 0;
        *reinterpret_cast<int32_t*>(&rax102 + 4) = 0;
        do {
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx13) + reinterpret_cast<unsigned char>(rax102)) != *reinterpret_cast<signed char*>(&edx99)) {
                if (!rax102) {
                    ++r12_12;
                    ++rcx101;
                } else {
                    r12_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_12) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + reinterpret_cast<unsigned char>(rax102) * 8)));
                    rax102 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax102) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + reinterpret_cast<unsigned char>(rax102) * 8)));
                }
            } else {
                ++rax102;
                ++rcx101;
                if (r13_14 == rax102) 
                    break;
            }
            edx99 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx101));
        } while (*reinterpret_cast<signed char*>(&edx99));
        goto addr_b2c1_80;
    }
    *r14_15 = r12_12;
    goto addr_b2c1_80;
    addr_b8f1_89:
    rsp103 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp67) - 8);
    *rsp103 = reinterpret_cast<void**>(0xb8f9);
    freea(r12_74, rsi53, r12_74, rsi53);
    rsp67 = reinterpret_cast<struct s70*>(rsp103 + 8);
    goto addr_b557_53;
    addr_2b82_90:
    rsp104 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp67) - 8);
    *rsp104 = reinterpret_cast<void**>(0x2b87);
    fun_25f0();
    rsp105 = rsp104 + 8 - 8;
    *rsp105 = reinterpret_cast<void**>(0x2b8c);
    fun_25f0();
    rsp106 = rsp105 + 8 - 8;
    *rsp106 = reinterpret_cast<void**>(0x2b91);
    rax107 = fun_25f0();
    rsp108 = rsp106 + 8 - 8;
    *rsp108 = rax107;
    *reinterpret_cast<int64_t*>(rsp108 - 8) = 0x2b97;
    fun_25f0();
    addr_b8e0_94:
    g8f = reinterpret_cast<void**>("mbsinit (&iter->state)");
    goto addr_b8f1_89;
    while (1) {
        addr_b8b9_95:
        rdi109 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff38);
        rsp110 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp67) - 8);
        *rsp110 = reinterpret_cast<void**>(0xb8c5);
        mbuiter_multi_next_part_0(rdi109, rsi53, rdi109, rsi53);
        rsp67 = reinterpret_cast<struct s70*>(rsp110 + 8);
        if (!*reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff60)) 
            goto addr_b89e_96;
        goto addr_b890_98;
        while (1) {
            addr_b7a6_99:
            rsp111 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp67) - 8);
            *rsp111 = reinterpret_cast<void**>(0xb7ae);
            mbuiter_multi_next_part_0(r15_79, rsi53, r15_79, rsi53);
            rsp67 = reinterpret_cast<struct s70*>(rsp111 + 8);
            if (*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffffa0)) 
                goto addr_b760_100; else 
                goto addr_b7b4_101;
            addr_b7e8_102:
            if (!*reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff60)) 
                goto addr_b7ff_103;
            if (!*reinterpret_cast<int32_t*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff64)) 
                goto addr_2b82_90;
            addr_b7ff_103:
            rax112 = *reinterpret_cast<struct s73**>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff58);
            *reinterpret_cast<signed char*>(&(rbp57 + 0xffffffffffffffec)->f0) = 0;
            *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff50) = reinterpret_cast<void**>(reinterpret_cast<int64_t>("mbsinit (&iter->state)") + reinterpret_cast<uint64_t>(rax112));
            rax113 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff98);
            *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff8c) = 0;
            *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff90) = reinterpret_cast<void**>(reinterpret_cast<int64_t>("mbsinit (&iter->state)") + reinterpret_cast<unsigned char>(rax113));
            continue;
            addr_b849_105:
            addr_b78a_106:
            rsi53 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>("mbsinit (&iter->state)") + reinterpret_cast<unsigned char>(r14_114));
            *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff8c) = 0;
            ++r13_98;
            *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff90) = rsi53;
            if ((rbp57 + 0xffffffffffffffe8)->f0 == r13_98) 
                goto addr_b8e0_94; else 
                continue;
            addr_b782_107:
            r14_114 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff98);
            goto addr_b78a_106;
            addr_b7c0_108:
            while ((r14_114 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff98), rax115->f8 != r14_114) || (rdi116 = rax115->f0, *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff30) = reinterpret_cast<void**>("mbsinit (&iter->state)"), rsp117 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp67) - 8), *rsp117 = reinterpret_cast<void**>(0xb83e), eax118 = fun_2860(rdi116, "mbsinit (&iter->state)", r14_114), rsp67 = reinterpret_cast<struct s70*>(rsp117 + 8), rsi53 = reinterpret_cast<void**>("mbsinit (&iter->state)"), !!eax118)) {
                while (1) {
                    if (!r13_98) 
                        goto addr_b7d3_110;
                    r14_119 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx81) + reinterpret_cast<unsigned char>(r13_98) * 8);
                    *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff30) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_98) - reinterpret_cast<unsigned char>(r14_119));
                    if (!r14_119) {
                        addr_b756_79:
                        if (1) 
                            goto addr_b7a6_99;
                    } else {
                        if (1) 
                            goto addr_b8b9_95;
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff60)) 
                            goto addr_b89e_96; else 
                            goto addr_b890_98;
                    }
                    if (!*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffffa0)) 
                        break;
                    addr_b760_100:
                    if (!*reinterpret_cast<int32_t*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffffa4)) 
                        goto addr_b8f1_89;
                    rax115 = reinterpret_cast<struct s74*>((reinterpret_cast<uint64_t>(r13_98 + reinterpret_cast<unsigned char>(r13_98) * 2) << 4) + reinterpret_cast<unsigned char>(r12_74));
                    if (!rax115->f10) 
                        goto addr_b7c0_108;
                    if (rax115->f14 != *reinterpret_cast<int32_t*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffffa4)) 
                        continue; else 
                        goto addr_b782_107;
                    addr_b89e_96:
                    rdx120 = *reinterpret_cast<struct s73**>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff58);
                    *reinterpret_cast<signed char*>(&(rbp57 + 0xffffffffffffffec)->f0) = 0;
                    *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff50) = reinterpret_cast<void**>(reinterpret_cast<int64_t>("mbsinit (&iter->state)") + reinterpret_cast<uint64_t>(rdx120));
                    --r14_119;
                    if (r14_119) 
                        goto addr_b8b9_95;
                    r13_98 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff30);
                    goto addr_b756_79;
                    addr_b890_98:
                    if (!*reinterpret_cast<int32_t*>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff64)) 
                        goto addr_2b82_90; else 
                        goto addr_b89e_96;
                }
                addr_b7b4_101:
                rax115 = reinterpret_cast<struct s74*>((reinterpret_cast<uint64_t>(r13_98 + reinterpret_cast<unsigned char>(r13_98) * 2) << 4) + reinterpret_cast<unsigned char>(r12_74));
            }
            goto addr_b849_105;
            addr_b7d3_110:
            if (!0) {
                rdi121 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp57) + 0xffffffffffffff38);
                rsp122 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp67) - 8);
                *rsp122 = reinterpret_cast<void**>(0xb7e8);
                mbuiter_multi_next_part_0(rdi121, rsi53, rdi121, rsi53);
                rsp67 = reinterpret_cast<struct s70*>(rsp122 + 8);
                goto addr_b7e8_102;
            }
        }
    }
}

struct s75 {
    void** f0;
    signed char[7] pad8;
    int64_t f8;
};

struct s76 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    signed char f10;
    signed char[3] pad20;
    int32_t f14;
};

struct s77 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    signed char f10;
    signed char[3] pad20;
    int32_t f14;
};

unsigned char knuth_morris_pratt_multibyte(void** rdi, void** rsi, void*** rdx) {
    void* rsp4;
    void* rbp5;
    void** r13_6;
    void*** v7;
    void** v8;
    void** rax9;
    void** v10;
    void** rax11;
    void* rsp12;
    void** v13;
    int64_t rdx14;
    uint64_t rdi15;
    uint64_t rdi16;
    void* rdx17;
    uint64_t rax18;
    void* rax19;
    void** r12_20;
    void** rax21;
    unsigned char v22;
    void** v23;
    void** r14_24;
    void** r15_25;
    void** r13_26;
    struct s75* rbx27;
    void* rax28;
    uint32_t eax29;
    void*** rsp30;
    uint32_t eax31;
    unsigned char v32;
    int32_t v33;
    void** rcx34;
    void** rdx35;
    void** v36;
    void** v37;
    void*** rsp38;
    void** r8_39;
    void*** rsp40;
    void** r8_41;
    uint32_t eax42;
    int32_t v43;
    uint64_t r14_44;
    struct s66* r13_45;
    void** v46;
    uint32_t r9d47;
    struct s76* rax48;
    void** rdx49;
    void** rdi50;
    void*** rsp51;
    uint32_t eax52;
    void** r13_53;
    void** v54;
    void** v55;
    void** v56;
    void*** rsp57;
    void*** rsp58;
    void*** rsp59;
    void*** rsp60;
    void** rax61;
    void*** rsp62;
    void*** rsp63;
    signed char v64;
    void*** rsp65;
    signed char v66;
    signed char v67;
    int32_t v68;
    void* v69;
    void* v70;
    void** r14_71;
    void** v72;
    void** v73;
    struct s77* rax74;
    void** rdi75;
    void*** rsp76;
    uint32_t eax77;
    void** r14_78;
    void** v79;
    signed char v80;
    signed char v81;
    int32_t v82;
    void* v83;
    int32_t v84;
    void*** rsp85;

    rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp5 = rsp4;
    r13_6 = rsi;
    v7 = rdx;
    v8 = rdi;
    rax9 = g28;
    v10 = rax9;
    rax11 = mbslen(rsi);
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 - 8 - 8 - 8 - 8 - 0xc8 - 8 + 8);
    v13 = rax11;
    if (56 * reinterpret_cast<unsigned char>(rax11) < reinterpret_cast<int64_t>(0)) 
        goto addr_b550_2;
    *reinterpret_cast<uint32_t*>(&rdx14) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx14) + 4) = 0;
    if (rdx14) 
        goto addr_b550_2;
    rdi15 = reinterpret_cast<unsigned char>(v13) * 8 - reinterpret_cast<unsigned char>(v13) << 3;
    if (rdi15 <= reinterpret_cast<uint64_t>("alloc")) {
        rdi16 = rdi15 + 54;
        rdx17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp12) - (rdi16 & 0xfffffffffffff000));
        rax18 = rdi16 & 0xfffffffffffffff0;
        if (rsp12 != rdx17) {
            do {
                rsp12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp12) - reinterpret_cast<int64_t>("strcmp"));
            } while (rsp12 != rdx17);
        }
        *reinterpret_cast<uint32_t*>(&rax19) = *reinterpret_cast<uint32_t*>(&rax18) & 0xfff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax19) + 4) = 0;
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp12) - reinterpret_cast<int64_t>(rax19));
        if (rax19) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp12) + reinterpret_cast<int64_t>(rax19) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp12) + reinterpret_cast<int64_t>(rax19) - 8);
        }
        r12_20 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rsp12) + 15 & 0xfffffffffffffff0) + 31 & 0xffffffffffffffe0);
        if (r12_20) 
            goto addr_b58d_10; else 
            goto addr_b550_2;
    }
    rax21 = mmalloca();
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp12) - 8 + 8);
    r12_20 = rax21;
    if (!r12_20) {
        addr_b550_2:
        v22 = 0;
    } else {
        addr_b58d_10:
        v23 = r13_6;
        r14_24 = r12_20 + 24;
        r15_25 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp5) + 0xffffffffffffff80);
        r13_26 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp5) + 0xffffffffffffffa8);
        rbx27 = reinterpret_cast<struct s75*>((reinterpret_cast<uint64_t>(v13 + reinterpret_cast<unsigned char>(v13) * 2) << 4) + reinterpret_cast<unsigned char>(r12_20));
        goto addr_b5f7_12;
    }
    addr_b557_13:
    rax28 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax28) {
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp12) - 8) = reinterpret_cast<void**>(0xb97b);
        fun_2740();
    } else {
        eax29 = v22;
        return *reinterpret_cast<unsigned char*>(&eax29);
    }
    while (1) {
        addr_b5f7_12:
        rsp30 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp12) - 8);
        *rsp30 = reinterpret_cast<void**>(0xb603);
        mbuiter_multi_next_part_0(r15_25, rsi);
        rsp12 = reinterpret_cast<void*>(rsp30 + 8);
        eax31 = v32;
        v22 = *reinterpret_cast<unsigned char*>(&eax31);
        if (*reinterpret_cast<unsigned char*>(&eax31)) {
            if (!v33) 
                break;
            rcx34 = v23;
            rdx35 = v36;
            if (rcx34 != r13_26) 
                goto addr_b5d8_19;
        } else {
            rcx34 = v23;
            rdx35 = v37;
            if (rcx34 == r13_26) {
                rsi = r13_26;
                rsp38 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp12) - 8);
                *rsp38 = reinterpret_cast<void**>(0xb917);
                fun_28d0(r14_24, rsi, rdx35, rcx34, r8_39);
                rsp12 = reinterpret_cast<void*>(rsp38 + 8);
                rdx35 = rdx35;
                *reinterpret_cast<void***>(r14_24 + 0xffffffffffffffe8) = r14_24;
                *reinterpret_cast<signed char*>(r14_24 + 0xfffffffffffffff8) = 0;
                rcx34 = rcx34;
                *reinterpret_cast<void***>(r14_24 + 0xfffffffffffffff0) = rdx35;
                goto addr_b5ec_22;
            } else {
                *reinterpret_cast<void***>(r14_24 + 0xffffffffffffffe8) = rcx34;
                *reinterpret_cast<void***>(r14_24 + 0xfffffffffffffff0) = rdx35;
                *reinterpret_cast<signed char*>(r14_24 + 0xfffffffffffffff8) = 0;
                goto addr_b5ec_22;
            }
        }
        rsi = r13_26;
        rsp40 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp12) - 8);
        *rsp40 = reinterpret_cast<void**>(0xb950);
        fun_28d0(r14_24, rsi, rdx35, rcx34, r8_41);
        rsp12 = reinterpret_cast<void*>(rsp40 + 8);
        rdx35 = rdx35;
        *reinterpret_cast<void***>(r14_24 + 0xffffffffffffffe8) = r14_24;
        eax42 = v22;
        rcx34 = rcx34;
        *reinterpret_cast<void***>(r14_24 + 0xfffffffffffffff0) = rdx35;
        *reinterpret_cast<signed char*>(r14_24 + 0xfffffffffffffff8) = *reinterpret_cast<signed char*>(&eax42);
        addr_b5e5_25:
        *reinterpret_cast<int32_t*>(r14_24 + 0xfffffffffffffffc) = v43;
        addr_b5ec_22:
        r14_24 = r14_24 + 48;
        v23 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx34) + reinterpret_cast<unsigned char>(rdx35));
        continue;
        addr_b5d8_19:
        *reinterpret_cast<void***>(r14_24 + 0xffffffffffffffe8) = rcx34;
        *reinterpret_cast<void***>(r14_24 + 0xfffffffffffffff0) = rdx35;
        *reinterpret_cast<signed char*>(r14_24 + 0xfffffffffffffff8) = 1;
        goto addr_b5e5_25;
    }
    *reinterpret_cast<int32_t*>(&r14_44) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_44) + 4) = 0;
    r13_45 = reinterpret_cast<struct s66*>(r12_20 + 48);
    rbx27->f8 = 1;
    v46 = reinterpret_cast<void**>(2);
    if (reinterpret_cast<unsigned char>(v13) > reinterpret_cast<unsigned char>(2)) {
        do {
            r9d47 = r13_45->f10;
            while (1) {
                rax48 = reinterpret_cast<struct s76*>((r14_44 + r14_44 * 2 << 4) + reinterpret_cast<unsigned char>(r12_20));
                if (!*reinterpret_cast<unsigned char*>(&r9d47) || !rax48->f10) {
                    rdx49 = r13_45->f8;
                    if (rdx49 != rax48->f8) 
                        goto addr_b67f_31;
                    rsi = rax48->f0;
                    rdi50 = r13_45->f0;
                    rsp51 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp12) - 8);
                    *rsp51 = reinterpret_cast<void**>(0xb6b9);
                    eax52 = fun_2860(rdi50, rsi, rdx49);
                    rsp12 = reinterpret_cast<void*>(rsp51 + 8);
                    r9d47 = *reinterpret_cast<unsigned char*>(&r9d47);
                    if (!eax52) 
                        break;
                } else {
                    if (r13_45->f14 == rax48->f14) 
                        break;
                }
                addr_b67f_31:
                if (!r14_44) 
                    goto addr_b850_34;
                r14_44 = r14_44 - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx27) + r14_44 * 8));
            }
            ++r14_44;
            *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx27) + reinterpret_cast<unsigned char>(v46) * 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v46) - r14_44);
            continue;
            addr_b850_34:
            *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx27) + reinterpret_cast<unsigned char>(v46) * 8) = v46;
            ++v46;
            r13_45 = r13_45 + 2;
        } while (v13 != v46);
    }
    *reinterpret_cast<int32_t*>(&r13_53) = 0;
    *reinterpret_cast<int32_t*>(&r13_53 + 4) = 0;
    *v7 = reinterpret_cast<void**>(0);
    v54 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp5) + 0xffffffffffffff40);
    v55 = v8;
    v56 = v8;
    goto addr_b756_39;
    addr_b8f1_40:
    rsp57 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp12) - 8);
    *rsp57 = reinterpret_cast<void**>(0xb8f9);
    freea(r12_20, rsi, r12_20, rsi);
    rsp12 = reinterpret_cast<void*>(rsp57 + 8);
    goto addr_b557_13;
    addr_2b82_41:
    rsp58 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp12) - 8);
    *rsp58 = reinterpret_cast<void**>(0x2b87);
    fun_25f0();
    rsp59 = rsp58 + 8 - 8;
    *rsp59 = reinterpret_cast<void**>(0x2b8c);
    fun_25f0();
    rsp60 = rsp59 + 8 - 8;
    *rsp60 = reinterpret_cast<void**>(0x2b91);
    rax61 = fun_25f0();
    rsp62 = rsp60 + 8 - 8;
    *rsp62 = rax61;
    *reinterpret_cast<int64_t*>(rsp62 - 8) = 0x2b97;
    fun_25f0();
    addr_b8e0_45:
    *v7 = v55;
    goto addr_b8f1_40;
    while (1) {
        addr_b8b9_46:
        rsp63 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp12) - 8);
        *rsp63 = reinterpret_cast<void**>(0xb8c5);
        mbuiter_multi_next_part_0(v54, rsi, v54, rsi);
        rsp12 = reinterpret_cast<void*>(rsp63 + 8);
        if (!v64) 
            goto addr_b89e_47;
        goto addr_b890_49;
        while (1) {
            addr_b7a6_50:
            rsp65 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp12) - 8);
            *rsp65 = reinterpret_cast<void**>(0xb7ae);
            mbuiter_multi_next_part_0(r15_25, rsi, r15_25, rsi);
            rsp12 = reinterpret_cast<void*>(rsp65 + 8);
            if (v66) 
                goto addr_b760_51; else 
                goto addr_b7b4_52;
            addr_b7e8_53:
            if (!v67) 
                goto addr_b7ff_54;
            if (!v68) 
                goto addr_2b82_41;
            addr_b7ff_54:
            v55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v55) + reinterpret_cast<uint64_t>(v69));
            v56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v56) + reinterpret_cast<uint64_t>(v70));
            continue;
            addr_b849_56:
            addr_b78a_57:
            rsi = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(r14_71));
            ++r13_53;
            v56 = rsi;
            if (v13 == r13_53) 
                goto addr_b8e0_45; else 
                continue;
            addr_b782_58:
            r14_71 = v72;
            rsi = v56;
            goto addr_b78a_57;
            addr_b7c0_59:
            while ((r14_71 = v73, rax74->f8 != r14_71) || (rdi75 = rax74->f0, rsp76 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp12) - 8), *rsp76 = reinterpret_cast<void**>(0xb83e), eax77 = fun_2860(rdi75, v56, r14_71), rsp12 = reinterpret_cast<void*>(rsp76 + 8), rsi = v56, !!eax77)) {
                while (1) {
                    if (!r13_53) 
                        goto addr_b7d3_61;
                    r14_78 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx27) + reinterpret_cast<unsigned char>(r13_53) * 8);
                    v79 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_53) - reinterpret_cast<unsigned char>(r14_78));
                    if (!r14_78) {
                        addr_b756_39:
                        if (1) 
                            goto addr_b7a6_50;
                    } else {
                        if (1) 
                            goto addr_b8b9_46;
                        if (!v80) 
                            goto addr_b89e_47; else 
                            goto addr_b890_49;
                    }
                    if (!v81) 
                        break;
                    addr_b760_51:
                    if (!v82) 
                        goto addr_b8f1_40;
                    rax74 = reinterpret_cast<struct s77*>((reinterpret_cast<uint64_t>(r13_53 + reinterpret_cast<unsigned char>(r13_53) * 2) << 4) + reinterpret_cast<unsigned char>(r12_20));
                    if (!rax74->f10) 
                        goto addr_b7c0_59;
                    if (rax74->f14 != v82) 
                        continue; else 
                        goto addr_b782_58;
                    addr_b89e_47:
                    v55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v55) + reinterpret_cast<uint64_t>(v83));
                    --r14_78;
                    if (r14_78) 
                        goto addr_b8b9_46;
                    r13_53 = v79;
                    goto addr_b756_39;
                    addr_b890_49:
                    if (!v84) 
                        goto addr_2b82_41; else 
                        goto addr_b89e_47;
                }
                addr_b7b4_52:
                rax74 = reinterpret_cast<struct s77*>((reinterpret_cast<uint64_t>(r13_53 + reinterpret_cast<unsigned char>(r13_53) * 2) << 4) + reinterpret_cast<unsigned char>(r12_20));
            }
            goto addr_b849_56;
            addr_b7d3_61:
            if (!0) {
                rsp85 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp12) - 8);
                *rsp85 = reinterpret_cast<void**>(0xb7e8);
                mbuiter_multi_next_part_0(v54, rsi, v54, rsi);
                rsp12 = reinterpret_cast<void*>(rsp85 + 8);
                goto addr_b7e8_53;
            }
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0x150a0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s78 {
    signed char[54720] pad54720;
    unsigned char fd5c0;
};

struct s79 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
    void** f18;
    signed char[7] pad32;
    int64_t f20;
    void** f28;
    signed char[7] pad48;
    int64_t f30;
    void** f38;
    signed char[7] pad64;
    int64_t f40;
    void** f48;
    signed char[7] pad80;
    int64_t f50;
    void** f58;
    signed char[7] pad96;
    int64_t f60;
    void** f68;
    signed char[66263] pad66368;
    unsigned char f10340;
};

struct s80 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s81 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s82 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s83 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s84 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s85 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s86 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s87 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s88 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s89 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s90 {
    signed char[66368] pad66368;
    unsigned char f10340;
};

struct s91 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void factor_using_pollard_rho2(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r15_5;
    void** rbp6;
    void*** rsp7;
    void** v8;
    void** v9;
    void** rax10;
    void** v11;
    uint64_t rcx12;
    void** rdx13;
    int64_t rcx14;
    uint64_t rcx15;
    uint1_t cf16;
    void** rax17;
    void** rsi18;
    uint1_t cf19;
    int1_t cf20;
    void** v21;
    void** v22;
    void** tmp64_23;
    void** rax24;
    void** r12_25;
    void** r10_26;
    void** v27;
    void** v28;
    void** v29;
    void** r13_30;
    void** rbx31;
    void** v32;
    void** r14_33;
    void** r15_34;
    void*** v35;
    void** r9_36;
    uint64_t rax37;
    int64_t rax38;
    void* rax39;
    void* rdx40;
    void* rax41;
    void** rax42;
    void** rbp43;
    int64_t rax44;
    void** v45;
    int64_t v46;
    void** rax47;
    void** tmp64_48;
    void** rdx49;
    void* v50;
    void** v51;
    void** rdx52;
    void** r8_53;
    void** rcx54;
    void** tmp64_55;
    uint1_t cf56;
    void** rax57;
    void** v58;
    void** rax59;
    int64_t rax60;
    void** v61;
    void** rax62;
    void** v63;
    void** v64;
    void** rdx65;
    void** r15_66;
    void** r12_67;
    void** r13_68;
    void** rsi69;
    void** rbp70;
    void** rax71;
    void** tmp64_72;
    void** rcx73;
    void* v74;
    void** v75;
    void** r11_76;
    void** r10_77;
    void** v78;
    void** r15_79;
    void** r12_80;
    void*** rbp81;
    void** r9_82;
    void** rax83;
    void** tmp64_84;
    void** rdx85;
    void* v86;
    void** rcx87;
    void** rdx88;
    void** rdx89;
    void** rsi90;
    void** tmp64_91;
    uint1_t cf92;
    void** rax93;
    void** rdi94;
    void** r8_95;
    uint64_t rax96;
    int64_t rax97;
    void** r13_98;
    void* rax99;
    void* rdx100;
    void** rdx101;
    void* rax102;
    signed char al103;
    void** rdx104;
    void** rsi105;
    void** rdi106;
    unsigned char al107;
    void** rax108;
    void* rsp109;
    void** rax110;
    void* rsp111;
    void** rax112;
    uint64_t rax113;
    struct s78* rax114;
    void* rax115;
    void* rdx116;
    void* rax117;
    unsigned char al118;
    void** r8_119;
    void** r9_120;
    void** v121;
    void** rbp122;
    void** v123;
    void*** rsp124;
    void** r12_125;
    int64_t r13_126;
    void** r11_127;
    void** r10_128;
    void*** r14_129;
    struct s79* rbx130;
    uint64_t r15_131;
    void** rcx132;
    void** rdx133;
    void** r10_134;
    void** r13_135;
    void* rax136;
    int32_t ecx137;
    int32_t ecx138;
    int32_t edx139;
    int64_t rbx140;
    void** rcx141;
    void** rdx142;
    struct s80* rcx143;
    void* rcx144;
    void** rcx145;
    void** rdx146;
    void** rdx147;
    struct s81* rcx148;
    struct s82* rsi149;
    void* rsi150;
    void* rcx151;
    struct s83* rsi152;
    void* rsi153;
    void** rcx154;
    void** rdx155;
    void** v156;
    void** rdx157;
    uint32_t ecx158;
    uint32_t edx159;
    void** rsi160;
    struct s84* rdi161;
    void* rdi162;
    void** rdx163;
    void** rdx164;
    uint32_t ecx165;
    uint32_t edx166;
    void** rsi167;
    struct s85* rdi168;
    void* rdi169;
    void** rdx170;
    void** rdx171;
    uint32_t ecx172;
    uint32_t edx173;
    void** rsi174;
    struct s86* rdi175;
    void* rdi176;
    void** rdx177;
    int64_t rax178;
    void* rax179;
    int64_t rax180;
    struct s87* rax181;
    uint32_t ecx182;
    uint32_t edx183;
    void** rsi184;
    struct s88* rdi185;
    void* rdi186;
    void** rdx187;
    struct s89* rcx188;
    struct s90* rsi189;
    void* rcx190;
    void* rsi191;
    void** rcx192;
    void** rdi193;
    void** v194;
    void** v195;
    unsigned char al196;
    int64_t rax197;
    struct s91* rax198;
    void* rax199;
    signed char al200;
    unsigned char al201;
    void** r9_202;
    void** rsi203;
    void** rdi204;
    void** rdi205;
    void** rdx206;
    void** rsi207;
    int64_t rax208;
    void** r8_209;
    void*** r11_210;
    signed char* r10_211;
    uint32_t r9d212;
    int32_t ebx213;
    void* rcx214;
    void* rax215;
    int64_t rsi216;
    void* rax217;
    uint32_t r9d218;
    uint32_t eax219;
    void** r12_220;
    void** rcx221;
    int32_t esi222;
    void** rax223;
    void* rdx224;
    void* rdi225;
    uint1_t cf226;
    int64_t rbp227;
    int64_t r15_228;
    int64_t r14_229;
    void** rbp230;
    void** rcx231;
    void** r11_232;
    uint64_t rax233;
    int64_t rax234;
    void* rax235;
    void* rdx236;
    void* rax237;
    void** r8_238;
    void* r10_239;
    void* r9_240;
    void* rax241;
    void** r13_242;
    int64_t rax243;
    int64_t rax244;
    void** rdi245;
    void** rax246;
    int64_t rcx247;
    void* rdi248;
    void* rax249;
    void* r11_250;
    void* rsi251;
    void* rax252;
    void** rbx253;
    void** rsi254;
    void** rax255;
    void** rcx256;
    void** r11_257;
    void** rdx258;
    void** rax259;
    void** r8_260;
    signed char al261;
    void** rdx262;
    void** rsi263;
    void** rcx264;
    int1_t zf265;
    signed char al266;
    struct s0* r14_267;
    void** rbp268;
    struct s0** rsp269;
    void** rax270;
    void** v271;
    uint32_t eax272;
    struct s0* v273;
    uint32_t v274;
    void** rcx275;
    void** r15_276;
    uint64_t rax277;
    int32_t esi278;
    int64_t rax279;
    void* rax280;
    void* rdx281;
    void* rax282;
    void** rdx283;
    struct s0* r13_284;
    void** rax285;
    uint64_t rdi286;
    uint1_t cf287;
    int64_t rbx288;
    void** rbx289;
    unsigned char al290;
    void* v291;
    struct s0* v292;
    uint32_t eax293;
    unsigned char v294;
    uint32_t v295;
    int64_t rax296;
    void* v297;
    void** rax298;
    void* rax299;
    void** rax300;
    void* rax301;
    void** rax302;
    void** rdx303;
    void** rcx304;
    int32_t esi305;
    void** rax306;
    uint64_t rdi307;
    uint1_t cf308;
    unsigned char al309;
    void* r11_310;
    uint64_t v311;

    while (1) {
        r15_5 = rdi;
        rbp6 = rsi;
        rsp7 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x88);
        v8 = rdx;
        v9 = rcx;
        rax10 = g28;
        v11 = rax10;
        rcx12 = reinterpret_cast<unsigned char>(rcx) - (reinterpret_cast<unsigned char>(rcx) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi) < reinterpret_cast<unsigned char>(2)))))));
        *reinterpret_cast<int32_t*>(&rdx13) = 0;
        *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx14) = *reinterpret_cast<uint32_t*>(&rcx12) & 64;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx14) + 4) = 0;
        rcx15 = reinterpret_cast<uint64_t>(rcx14 + 63);
        cf16 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi) < reinterpret_cast<unsigned char>(2));
        *reinterpret_cast<unsigned char*>(&rdx13) = cf16;
        rax17 = reinterpret_cast<void**>(-static_cast<uint64_t>(reinterpret_cast<uint1_t>(0 < static_cast<uint64_t>(cf16))) + 1);
        do {
            rsi18 = rdx13;
            rdx13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx13) + reinterpret_cast<unsigned char>(rdx13));
            rax17 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax17) + reinterpret_cast<unsigned char>(rax17)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rsi18) >> 63));
            if (reinterpret_cast<unsigned char>(r15_5) < reinterpret_cast<unsigned char>(rax17) || r15_5 == rax17 && reinterpret_cast<unsigned char>(rbp6) <= reinterpret_cast<unsigned char>(rdx13)) {
                cf19 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx13) < reinterpret_cast<unsigned char>(rbp6));
                rdx13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx13) - reinterpret_cast<unsigned char>(rbp6));
                rax17 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax17) - (reinterpret_cast<unsigned char>(r15_5) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax17) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r15_5) + static_cast<uint64_t>(cf19))))));
            }
            cf20 = rcx15 < 1;
            --rcx15;
        } while (!cf20);
        v21 = rdx13;
        v22 = rax17;
        tmp64_23 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx13) + reinterpret_cast<unsigned char>(rdx13));
        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax17) + reinterpret_cast<unsigned char>(rax17) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_23) < reinterpret_cast<unsigned char>(rdx13))));
        r12_25 = tmp64_23;
        r10_26 = rax24;
        if (reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(r15_5) || rax24 == r15_5 && reinterpret_cast<unsigned char>(tmp64_23) >= reinterpret_cast<unsigned char>(rbp6)) {
            r12_25 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_23) - reinterpret_cast<unsigned char>(rbp6));
            r10_26 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) - (reinterpret_cast<unsigned char>(r15_5) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r15_5) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_23) < reinterpret_cast<unsigned char>(rbp6))))))));
        }
        v27 = r10_26;
        v28 = r10_26;
        if (r15_5) 
            goto addr_468c_8;
        if (rbp6 == 1) 
            goto addr_4ad5_10;
        addr_468c_8:
        v29 = r12_25;
        r13_30 = r15_5;
        *reinterpret_cast<int32_t*>(&rbx31) = 1;
        *reinterpret_cast<int32_t*>(&rbx31 + 4) = 0;
        v32 = reinterpret_cast<void**>(1);
        r14_33 = reinterpret_cast<void**>(rsp7 + 0x70);
        r15_34 = r12_25;
        v35 = rsp7 + 0x68;
        while (1) {
            r9_36 = r13_30;
            r13_30 = rbx31;
            rax37 = reinterpret_cast<unsigned char>(rbp6) >> 1;
            rbx31 = rbp6;
            *reinterpret_cast<uint32_t*>(&rax38) = *reinterpret_cast<uint32_t*>(&rax37) & 0x7f;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax38) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rax39) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax38);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax39) + 4) = 0;
            rdx40 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax39) + reinterpret_cast<int64_t>(rax39) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax39) * reinterpret_cast<int64_t>(rax39) * reinterpret_cast<unsigned char>(rbp6)));
            rax41 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx40) + reinterpret_cast<uint64_t>(rdx40) - reinterpret_cast<uint64_t>(rdx40) * reinterpret_cast<uint64_t>(rdx40) * reinterpret_cast<unsigned char>(rbp6));
            rax42 = rbp6;
            rbp43 = r10_26;
            *reinterpret_cast<uint32_t*>(&rax44) = *reinterpret_cast<uint32_t*>(&rax42) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax44) + 4) = 0;
            v45 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax41) + reinterpret_cast<uint64_t>(rax41) - reinterpret_cast<uint64_t>(rax41) * reinterpret_cast<uint64_t>(rax41) * reinterpret_cast<unsigned char>(rbp6));
            v46 = rax44;
            while (1) {
                rax47 = mulredc2(r14_33, rbp43, r12_25, rbp43, r12_25, r9_36, rbx31, v22);
                tmp64_48 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax47) + reinterpret_cast<unsigned char>(v8));
                rdx49 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v50) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_48) < reinterpret_cast<unsigned char>(rax47))));
                v51 = rdx49;
                r12_25 = tmp64_48;
                rbp43 = rdx49;
                if (reinterpret_cast<unsigned char>(rdx49) > reinterpret_cast<unsigned char>(r9_36) || rdx49 == r9_36 && reinterpret_cast<unsigned char>(tmp64_48) >= reinterpret_cast<unsigned char>(rbx31)) {
                    rdx52 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx49) - (reinterpret_cast<unsigned char>(r9_36) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx49) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r9_36) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_48) < reinterpret_cast<unsigned char>(rbx31))))))));
                    v51 = rdx52;
                    r12_25 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_48) - reinterpret_cast<unsigned char>(rbx31));
                    rbp43 = rdx52;
                }
                r8_53 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v29) - reinterpret_cast<unsigned char>(r12_25));
                rcx54 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v27) - (reinterpret_cast<unsigned char>(rbp43) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v27) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp43) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v29) < reinterpret_cast<unsigned char>(r12_25))))))));
                if (reinterpret_cast<signed char>(rcx54) < reinterpret_cast<signed char>(0)) {
                    tmp64_55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_53) + reinterpret_cast<unsigned char>(rbx31));
                    cf56 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_55) < reinterpret_cast<unsigned char>(r8_53));
                    r8_53 = tmp64_55;
                    rcx54 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx54) + reinterpret_cast<unsigned char>(r9_36) + static_cast<uint64_t>(cf56));
                }
                rax57 = mulredc2(r14_33, v22, v21, rcx54, r8_53, r9_36, rbx31, v22);
                v21 = rax57;
                v22 = v58;
                rax59 = r13_30;
                *reinterpret_cast<uint32_t*>(&rax60) = *reinterpret_cast<uint32_t*>(&rax59) & 31;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax60) + 4) = 0;
                rsp7 = rsp7 - 8 - 8 - 8 + 8 + 8 + 8 - 8 - 8 - 8 + 8 + 8 + 8;
                r9_36 = r9_36;
                if (rax60 == 1) {
                    if (!v46) 
                        goto addr_4cba_18;
                    if (reinterpret_cast<unsigned char>(v21) | reinterpret_cast<unsigned char>(v22)) 
                        goto addr_48c0_20;
                } else {
                    addr_47d9_21:
                    --r13_30;
                    if (r13_30) 
                        continue; else 
                        goto addr_47e3_22;
                }
                v61 = r9_36;
                rax62 = rbx31;
                if (r9_36) 
                    break;
                addr_48ee_24:
                if (!reinterpret_cast<int1_t>(rax62 == 1)) 
                    goto addr_4920_25;
                v28 = rbp43;
                r15_34 = r12_25;
                goto addr_47d9_21;
                addr_48c0_20:
                rax62 = gcd2_odd_part_0(v35, v22, v21, r9_36, rbx31, r9_36);
                rsp7 = rsp7 - 8 + 8;
                r9_36 = r9_36;
                if (v61) 
                    goto addr_4920_25; else 
                    goto addr_48ee_24;
                addr_47e3_22:
                v27 = rbp43;
                r15_34 = r12_25;
                v63 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v32) + reinterpret_cast<unsigned char>(v32));
                if (v32) {
                    v64 = r12_25;
                    rdx65 = r12_25;
                    r15_66 = r13_30;
                    r12_67 = v45;
                    r13_68 = v8;
                    rsi69 = rbp43;
                    rbp70 = r9_36;
                    do {
                        rax71 = mulredc2(r14_33, rsi69, rdx65, rsi69, rdx65, rbp70, rbx31, r12_67);
                        tmp64_72 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax71) + reinterpret_cast<unsigned char>(r13_68));
                        rcx73 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v74) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_72) < reinterpret_cast<unsigned char>(rax71))));
                        rdx65 = tmp64_72;
                        rsi69 = rcx73;
                        rsp7 = rsp7 - 8 - 8 - 8 + 8 + 8 + 8;
                        if (reinterpret_cast<unsigned char>(rcx73) > reinterpret_cast<unsigned char>(rbp70) || rcx73 == rbp70 && reinterpret_cast<unsigned char>(tmp64_72) >= reinterpret_cast<unsigned char>(rbx31)) {
                            rdx65 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_72) - reinterpret_cast<unsigned char>(rbx31));
                            rsi69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx73) - (reinterpret_cast<unsigned char>(rbp70) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx73) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp70) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_72) < reinterpret_cast<unsigned char>(rbx31))))))));
                        }
                        ++r15_66;
                    } while (v32 != r15_66);
                    r12_25 = v64;
                    r9_36 = rbp70;
                    r15_34 = rdx65;
                    rbp43 = rsi69;
                }
                r13_30 = v32;
                v29 = r12_25;
                r12_25 = r15_34;
                v28 = rbp43;
                v32 = v63;
            }
            addr_4920_25:
            v75 = r12_25;
            r11_76 = r15_34;
            r10_77 = v28;
            v78 = r13_30;
            r15_79 = r14_33;
            r13_30 = rbx31;
            r12_80 = v29;
            r14_33 = v45;
            rbp81 = v35;
            rbx31 = r9_36;
            do {
                r9_82 = rbx31;
                rax83 = mulredc2(r15_79, r10_77, r11_76, r10_77, r11_76, r9_82, r13_30, r14_33);
                tmp64_84 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax83) + reinterpret_cast<unsigned char>(v8));
                rdx85 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v86) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_84) < reinterpret_cast<unsigned char>(rax83))));
                v28 = rdx85;
                r11_76 = tmp64_84;
                rcx87 = r13_30;
                r10_77 = rdx85;
                rsp7 = rsp7 - 8 - 8 - 8 + 8 + 8 + 8;
                if (reinterpret_cast<unsigned char>(rdx85) > reinterpret_cast<unsigned char>(rbx31) || rdx85 == rbx31 && reinterpret_cast<unsigned char>(tmp64_84) >= reinterpret_cast<unsigned char>(r13_30)) {
                    rdx88 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx85) - (reinterpret_cast<unsigned char>(rbx31) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx85) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbx31) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_84) < reinterpret_cast<unsigned char>(r13_30))))))));
                    v28 = rdx88;
                    r11_76 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_84) - reinterpret_cast<unsigned char>(r13_30));
                    r10_77 = rdx88;
                }
                rdx89 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_80) - reinterpret_cast<unsigned char>(r11_76));
                rsi90 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v27) - (reinterpret_cast<unsigned char>(r10_77) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v27) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r10_77) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_80) < reinterpret_cast<unsigned char>(r11_76))))))));
                if (reinterpret_cast<signed char>(rsi90) < reinterpret_cast<signed char>(0)) {
                    tmp64_91 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx89) + reinterpret_cast<unsigned char>(r13_30));
                    cf92 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_91) < reinterpret_cast<unsigned char>(rdx89));
                    rdx89 = tmp64_91;
                    rsi90 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi90) + reinterpret_cast<unsigned char>(rbx31) + static_cast<uint64_t>(cf92));
                }
                if (reinterpret_cast<unsigned char>(rsi90) | reinterpret_cast<unsigned char>(rdx89)) {
                    rcx87 = rbx31;
                    rax93 = gcd2_odd_part_0(rbp81, rsi90, rdx89, rcx87, r13_30, r9_82);
                    rsp7 = rsp7 - 8 + 8;
                    rdi94 = v61;
                    if (rdi94) 
                        goto addr_4a05_40;
                } else {
                    rdi94 = rbx31;
                    v61 = rbx31;
                    rax93 = r13_30;
                    if (rdi94) 
                        goto addr_4a05_40;
                }
            } while (reinterpret_cast<int1_t>(rax93 == 1));
            r8_95 = rax93;
            rax96 = reinterpret_cast<unsigned char>(rax93) >> 1;
            *reinterpret_cast<uint32_t*>(&rax97) = *reinterpret_cast<uint32_t*>(&rax96) & 0x7f;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax97) + 4) = 0;
            r13_98 = rbx31;
            r14_33 = r15_79;
            *reinterpret_cast<uint32_t*>(&rax99) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax97);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax99) + 4) = 0;
            rbx31 = v78;
            rdx100 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax99) + reinterpret_cast<int64_t>(rax99) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax99) * reinterpret_cast<int64_t>(rax99) * reinterpret_cast<unsigned char>(r8_95)));
            rdx101 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdx100) * reinterpret_cast<uint64_t>(rdx100) * reinterpret_cast<unsigned char>(r8_95));
            rax102 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx100) + reinterpret_cast<uint64_t>(rdx100) - reinterpret_cast<unsigned char>(rdx101));
            rcx87 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax102) + reinterpret_cast<uint64_t>(rax102) - reinterpret_cast<uint64_t>(rax102) * reinterpret_cast<uint64_t>(rax102) * reinterpret_cast<unsigned char>(r8_95));
            rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_30) * reinterpret_cast<unsigned char>(rcx87));
            if (reinterpret_cast<unsigned char>(r13_98) < reinterpret_cast<unsigned char>(r8_95)) {
                *reinterpret_cast<int32_t*>(&r13_30) = 0;
                *reinterpret_cast<int32_t*>(&r13_30 + 4) = 0;
            } else {
                rdx101 = __intrinsic();
                r9_82 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r13_98) - reinterpret_cast<unsigned char>(rdx101)) * reinterpret_cast<unsigned char>(rcx87));
                r13_30 = r9_82;
            }
            if (reinterpret_cast<unsigned char>(r8_95) <= reinterpret_cast<unsigned char>(1) || reinterpret_cast<unsigned char>(r8_95) > reinterpret_cast<unsigned char>(0x17ded78) && (al103 = prime_p_part_0(r8_95, rsi90, rdx101, rcx87), rsp7 = rsp7 - 8 + 8, r8_95 = r8_95, al103 == 0)) {
                rdx104 = v9;
                rsi105 = v8 + 1;
                factor_using_pollard_rho(r8_95, rsi105, rdx104, rcx87);
                rsp7 = rsp7 - 8 + 8;
            } else {
                *reinterpret_cast<int32_t*>(&rdx104) = 1;
                *reinterpret_cast<int32_t*>(&rdx104 + 4) = 0;
                rsi105 = r8_95;
                factor_insert_multiplicity(v9, rsi105, 1);
                rsp7 = rsp7 - 8 + 8;
            }
            if (!r13_30) 
                goto addr_4aa0_50;
            rsi105 = rbp6;
            rdi106 = r13_30;
            al107 = prime2_p(rdi106, rsi105);
            rsp7 = rsp7 - 8 + 8;
            if (al107) 
                goto addr_4c8b_52;
            rax108 = mod2(rsp7 + 80, v51, v75, r13_30, rbp6, r9_82);
            rsp109 = reinterpret_cast<void*>(rsp7 - 8 + 8);
            r12_25 = rax108;
            rax110 = mod2(reinterpret_cast<int64_t>(rsp109) + 88, v27, v29, r13_30, rbp6, r9_82);
            rsp111 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp109) - 8 + 8);
            v29 = rax110;
            rax112 = mod2(reinterpret_cast<int64_t>(rsp111) + 96, v28, r11_76, r13_30, rbp6, r9_82);
            rsp7 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp111) - 8 + 8);
            r10_26 = v51;
            r15_34 = rax112;
        }
        addr_4a05_40:
        if (rbx31 != rdi94) 
            goto addr_4a19_54;
        if (rax93 == r13_30) 
            goto addr_4ce0_56;
        addr_4a19_54:
        rbx31 = reinterpret_cast<void**>(0xd5c0);
        rsi105 = rax93;
        v32 = rax93;
        rax113 = reinterpret_cast<unsigned char>(rax93) >> 1;
        *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<uint32_t*>(&rax113) & 0x7f;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(0xd5c0) + reinterpret_cast<uint64_t>(rax114));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
        rdx116 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax115) + reinterpret_cast<int64_t>(rax115) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax115) * reinterpret_cast<int64_t>(rax115) * reinterpret_cast<unsigned char>(rax93)));
        rax117 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx116) + reinterpret_cast<uint64_t>(rdx116) - reinterpret_cast<uint64_t>(rdx116) * reinterpret_cast<uint64_t>(rdx116) * reinterpret_cast<unsigned char>(rax93));
        rdx104 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax117) + reinterpret_cast<uint64_t>(rax117) - reinterpret_cast<uint64_t>(rax117) * reinterpret_cast<uint64_t>(rax117) * reinterpret_cast<unsigned char>(rax93));
        rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_30) * reinterpret_cast<unsigned char>(rdx104));
        al118 = prime2_p(rdi94, rsi105);
        rsp7 = rsp7 - 8 + 8;
        if (!al118) 
            goto addr_4c4f_57;
        if (!v61) 
            goto addr_4ca3_59;
        rdi106 = v9;
        if (!*reinterpret_cast<void***>(rdi106 + 8)) 
            goto addr_4a94_61;
        addr_4cfe_62:
        factor_insert_large_part_0();
        r8_119 = rdi106;
        r9_120 = rdx104;
        v121 = r13_30;
        rbp122 = rsi105;
        v123 = rbx31;
        rsp124 = rsp7 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 56;
        *reinterpret_cast<unsigned char*>(rdx104 + 0xfa) = 0;
        *reinterpret_cast<void***>(rdx104 + 8) = reinterpret_cast<void**>(0);
        if (rdi106) 
            goto addr_4d50_64;
        if (reinterpret_cast<unsigned char>(rsi105) <= reinterpret_cast<unsigned char>(1)) 
            goto addr_4d41_66;
        addr_4d50_64:
        if (*reinterpret_cast<unsigned char*>(&rbp122) & 1) {
            addr_4df3_67:
            if (!r8_119) {
                *reinterpret_cast<int32_t*>(&r12_125) = 3;
                *reinterpret_cast<int32_t*>(&r12_125 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r13_126) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_126) + 4) = 0;
                r11_127 = reinterpret_cast<void**>(0x5555555555555555);
                r10_128 = reinterpret_cast<void**>(0xaaaaaaaaaaaaaaab);
            } else {
                r14_129 = reinterpret_cast<void***>(0xd648);
                *reinterpret_cast<uint32_t*>(&rbx130) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx130) + 4) = 0;
                r15_131 = 0xaaaaaaaaaaaaaaab;
                *reinterpret_cast<int32_t*>(&r12_125) = 3;
                *reinterpret_cast<int32_t*>(&r12_125 + 4) = 0;
                while (1) {
                    rcx132 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp122) * r15_131);
                    rdx133 = __intrinsic();
                    if (reinterpret_cast<unsigned char>(rdx133) <= reinterpret_cast<unsigned char>(r8_119)) {
                        r10_134 = *r14_129;
                        while ((r13_135 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(r8_119) - reinterpret_cast<unsigned char>(rdx133)) * r15_131), reinterpret_cast<unsigned char>(r13_135) <= reinterpret_cast<unsigned char>(r10_134)) && (factor_insert_multiplicity(r9_120, r12_125, 1), rsp124 = rsp124 - 8 + 8, r8_119 = r13_135, r9_120 = r9_120, r10_134 = r10_134, rbp122 = rcx132, rdx133 = __intrinsic(), reinterpret_cast<unsigned char>(rdx133) <= reinterpret_cast<unsigned char>(r13_135))) {
                            rcx132 = reinterpret_cast<void**>(r15_131 * reinterpret_cast<unsigned char>(rcx132));
                        }
                    }
                    *reinterpret_cast<uint32_t*>(&rax136) = rbx130->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax136) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&r13_126) = *reinterpret_cast<uint32_t*>(&rbx130);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_126) + 4) = 0;
                    r14_129 = r14_129 + 16;
                    r12_125 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_125) + reinterpret_cast<uint64_t>(rax136));
                    if (!r8_119) 
                        break;
                    if (*reinterpret_cast<uint32_t*>(&rbx130) > 0x29b) 
                        break;
                    r15_131 = *reinterpret_cast<uint64_t*>(r14_129 - 8);
                    rbx130 = reinterpret_cast<struct s79*>(&rbx130->pad8);
                }
                if (*reinterpret_cast<uint32_t*>(&r13_126) > 0x29b) 
                    goto addr_5200_78; else 
                    goto addr_4edd_79;
            }
        } else {
            if (rbp122) {
                __asm__("bsf rdx, rbp");
                ecx137 = 64 - *reinterpret_cast<int32_t*>(&rdx104);
                ecx138 = *reinterpret_cast<int32_t*>(&rdx104);
                rbp122 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp122) >> *reinterpret_cast<signed char*>(&ecx138)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_119) << *reinterpret_cast<unsigned char*>(&ecx137)));
                factor_insert_multiplicity(r9_120, 2, *reinterpret_cast<signed char*>(&rdx104));
                rsp124 = rsp124 - 8 + 8;
                r8_119 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_119) >> *reinterpret_cast<signed char*>(&ecx138));
                r9_120 = r9_120;
                goto addr_4df3_67;
            } else {
                __asm__("bsf rcx, r8");
                edx139 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rcx87 + 64));
                rbp122 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_119) >> *reinterpret_cast<signed char*>(&rcx87));
                *reinterpret_cast<int32_t*>(&r12_125) = 3;
                *reinterpret_cast<int32_t*>(&r12_125 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r13_126) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_126) + 4) = 0;
                factor_insert_multiplicity(r9_120, 2, *reinterpret_cast<signed char*>(&edx139));
                rsp124 = rsp124 - 8 + 8;
                r9_120 = r9_120;
                *reinterpret_cast<uint32_t*>(&r8_119) = 0;
                *reinterpret_cast<int32_t*>(&r8_119 + 4) = 0;
                r11_127 = reinterpret_cast<void**>(0x5555555555555555);
                r10_128 = reinterpret_cast<void**>(0xaaaaaaaaaaaaaaab);
            }
        }
        addr_4ef9_83:
        *reinterpret_cast<int32_t*>(&rbx140) = static_cast<int32_t>(r13_126 + 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx140) + 4) = 0;
        r13_126 = rbx140;
        rbx130 = reinterpret_cast<struct s79*>((rbx140 << 4) + reinterpret_cast<unsigned char>(0xd640));
        while (1) {
            *reinterpret_cast<int32_t*>(&r15_131) = static_cast<int32_t>(r13_126 - 1);
            rcx141 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp122) * reinterpret_cast<unsigned char>(r10_128));
            if (reinterpret_cast<unsigned char>(r11_127) >= reinterpret_cast<unsigned char>(rcx141)) {
                do {
                    factor_insert_multiplicity(r9_120, r12_125, 1);
                    rsp124 = rsp124 - 8 + 8;
                    r10_128 = r10_128;
                    r11_127 = r11_127;
                    r9_120 = r9_120;
                    rbp122 = rcx141;
                    rcx141 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx141) * reinterpret_cast<unsigned char>(r10_128));
                    r8_119 = r8_119;
                } while (reinterpret_cast<unsigned char>(r11_127) >= reinterpret_cast<unsigned char>(rcx141));
                rdx142 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx130->f0) * reinterpret_cast<unsigned char>(rbp122));
                if (reinterpret_cast<unsigned char>(rbx130->f8) < reinterpret_cast<unsigned char>(rdx142)) 
                    goto addr_4f29_87;
                goto addr_5050_89;
            }
            rdx142 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx130->f0) * reinterpret_cast<unsigned char>(rbp122));
            if (reinterpret_cast<unsigned char>(rbx130->f8) >= reinterpret_cast<unsigned char>(rdx142)) {
                addr_5050_89:
                *reinterpret_cast<uint32_t*>(&rcx143) = *reinterpret_cast<uint32_t*>(&r13_126);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx143) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rcx144) = rcx143->f10340;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx144) + 4) = 0;
                rcx145 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx144) + reinterpret_cast<unsigned char>(r12_125));
            } else {
                addr_4f29_87:
                rdx146 = reinterpret_cast<void**>(rbx130->f10 * reinterpret_cast<unsigned char>(rbp122));
                if (reinterpret_cast<unsigned char>(rdx146) <= reinterpret_cast<unsigned char>(rbx130->f18)) 
                    goto addr_50b0_91; else 
                    goto addr_4f3b_92;
            }
            do {
                rbp122 = rdx142;
                factor_insert_multiplicity(r9_120, rcx145, 1);
                rsp124 = rsp124 - 8 + 8;
                r9_120 = r9_120;
                rcx145 = rcx145;
                r8_119 = r8_119;
                rdx142 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx130->f0) * reinterpret_cast<unsigned char>(rbp122));
            } while (reinterpret_cast<unsigned char>(rdx142) <= reinterpret_cast<unsigned char>(rbx130->f8));
            rdx146 = reinterpret_cast<void**>(rbx130->f10 * reinterpret_cast<unsigned char>(rbp122));
            if (reinterpret_cast<unsigned char>(rdx146) > reinterpret_cast<unsigned char>(rbx130->f18)) {
                addr_4f3b_92:
                rdx147 = reinterpret_cast<void**>(rbx130->f20 * reinterpret_cast<unsigned char>(rbp122));
                if (reinterpret_cast<unsigned char>(rdx147) <= reinterpret_cast<unsigned char>(rbx130->f28)) {
                    addr_5120_95:
                    *reinterpret_cast<int32_t*>(&rcx148) = static_cast<int32_t>(r13_126 + 2);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx148) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi149) = *reinterpret_cast<uint32_t*>(&r13_126);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi149) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi150) = rsi149->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi150) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rcx151) = rcx148->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx151) + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rsi152) = static_cast<int32_t>(r13_126 + 1);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi152) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi153) = rsi152->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi153) + 4) = 0;
                    rcx154 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx151) + reinterpret_cast<int64_t>(rsi150) + reinterpret_cast<unsigned char>(r12_125) + reinterpret_cast<uint64_t>(rsi153));
                } else {
                    addr_4f4d_96:
                    rdx155 = reinterpret_cast<void**>(rbx130->f30 * reinterpret_cast<unsigned char>(rbp122));
                    if (reinterpret_cast<unsigned char>(rdx155) <= reinterpret_cast<unsigned char>(rbx130->f38)) 
                        goto addr_5198_97; else 
                        goto addr_4f5f_98;
                }
            } else {
                goto addr_50b0_91;
            }
            do {
                rbp122 = rdx147;
                v156 = rcx154;
                factor_insert_multiplicity(r9_120, rcx154, 1);
                rsp124 = rsp124 - 8 + 8;
                r9_120 = r9_120;
                rcx154 = v156;
                r8_119 = r8_119;
                rdx147 = reinterpret_cast<void**>(rbx130->f20 * reinterpret_cast<unsigned char>(rbp122));
            } while (reinterpret_cast<unsigned char>(rdx147) <= reinterpret_cast<unsigned char>(rbx130->f28));
            rdx155 = reinterpret_cast<void**>(rbx130->f30 * reinterpret_cast<unsigned char>(rbp122));
            if (reinterpret_cast<unsigned char>(rdx155) > reinterpret_cast<unsigned char>(rbx130->f38)) {
                addr_4f5f_98:
                rdx157 = reinterpret_cast<void**>(rbx130->f40 * reinterpret_cast<unsigned char>(rbp122));
                if (reinterpret_cast<unsigned char>(rdx157) <= reinterpret_cast<unsigned char>(rbx130->f48)) {
                    rbp122 = rdx157;
                    ecx158 = static_cast<uint32_t>(r13_126 + 5);
                    while (1) {
                        edx159 = *reinterpret_cast<uint32_t*>(&r13_126);
                        rsi160 = r12_125;
                        do {
                            *reinterpret_cast<uint32_t*>(&rdi161) = edx159;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi161) + 4) = 0;
                            ++edx159;
                            *reinterpret_cast<uint32_t*>(&rdi162) = rdi161->f10340;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi162) + 4) = 0;
                            rsi160 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi160) + reinterpret_cast<uint64_t>(rdi162));
                        } while (edx159 != ecx158);
                        v156 = r8_119;
                        factor_insert_multiplicity(r9_120, rsi160, 1);
                        rsp124 = rsp124 - 8 + 8;
                        r9_120 = r9_120;
                        r8_119 = v156;
                        ecx158 = ecx158;
                        rdx163 = reinterpret_cast<void**>(rbx130->f40 * reinterpret_cast<unsigned char>(rbp122));
                        if (reinterpret_cast<unsigned char>(rdx163) > reinterpret_cast<unsigned char>(rbx130->f48)) 
                            break;
                        rbp122 = rdx163;
                    }
                }
            } else {
                goto addr_5198_97;
            }
            rdx164 = reinterpret_cast<void**>(rbx130->f50 * reinterpret_cast<unsigned char>(rbp122));
            if (reinterpret_cast<unsigned char>(rdx164) <= reinterpret_cast<unsigned char>(rbx130->f58)) {
                rbp122 = rdx164;
                ecx165 = static_cast<uint32_t>(r13_126 + 6);
                while (1) {
                    edx166 = *reinterpret_cast<uint32_t*>(&r13_126);
                    rsi167 = r12_125;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdi168) = edx166;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi168) + 4) = 0;
                        ++edx166;
                        *reinterpret_cast<uint32_t*>(&rdi169) = rdi168->f10340;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi169) + 4) = 0;
                        rsi167 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi167) + reinterpret_cast<uint64_t>(rdi169));
                    } while (edx166 != ecx165);
                    v156 = r8_119;
                    factor_insert_multiplicity(r9_120, rsi167, 1);
                    rsp124 = rsp124 - 8 + 8;
                    r9_120 = r9_120;
                    r8_119 = v156;
                    ecx165 = ecx165;
                    rdx170 = reinterpret_cast<void**>(rbx130->f50 * reinterpret_cast<unsigned char>(rbp122));
                    if (reinterpret_cast<unsigned char>(rdx170) > reinterpret_cast<unsigned char>(rbx130->f58)) 
                        break;
                    rbp122 = rdx170;
                }
            }
            rdx171 = reinterpret_cast<void**>(rbx130->f60 * reinterpret_cast<unsigned char>(rbp122));
            if (reinterpret_cast<unsigned char>(rdx171) <= reinterpret_cast<unsigned char>(rbx130->f68)) {
                rbp122 = rdx171;
                ecx172 = static_cast<uint32_t>(r13_126 + 7);
                while (1) {
                    edx173 = *reinterpret_cast<uint32_t*>(&r13_126);
                    rsi174 = r12_125;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdi175) = edx173;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi175) + 4) = 0;
                        ++edx173;
                        *reinterpret_cast<uint32_t*>(&rdi176) = rdi175->f10340;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi176) + 4) = 0;
                        rsi174 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi174) + reinterpret_cast<uint64_t>(rdi176));
                    } while (edx173 != ecx172);
                    v156 = r8_119;
                    factor_insert_multiplicity(r9_120, rsi174, 1);
                    rsp124 = rsp124 - 8 + 8;
                    r9_120 = r9_120;
                    r8_119 = v156;
                    ecx172 = ecx172;
                    rdx177 = reinterpret_cast<void**>(rbx130->f60 * reinterpret_cast<unsigned char>(rbp122));
                    if (reinterpret_cast<unsigned char>(rdx177) > reinterpret_cast<unsigned char>(rbx130->f68)) 
                        break;
                    rbp122 = rdx177;
                }
            }
            *reinterpret_cast<int32_t*>(&rax178) = *reinterpret_cast<int32_t*>(&r15_131);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax178) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rax179) = *reinterpret_cast<unsigned char*>(0x10080 + rax178);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax179) + 4) = 0;
            r12_125 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_125) + reinterpret_cast<uint64_t>(rax179));
            if (reinterpret_cast<unsigned char>(rbp122) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r12_125) * reinterpret_cast<unsigned char>(r12_125))) 
                break;
            *reinterpret_cast<uint32_t*>(&rax180) = static_cast<uint32_t>(r13_126 + 7);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax180) + 4) = 0;
            rbx130 = reinterpret_cast<struct s79*>(reinterpret_cast<uint64_t>(rbx130) + 0x80);
            *reinterpret_cast<uint32_t*>(&r13_126) = *reinterpret_cast<uint32_t*>(&r13_126) + 8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_126) + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&rax180) > 0x29b) 
                break;
            rax181 = reinterpret_cast<struct s87*>((rax180 << 4) + reinterpret_cast<unsigned char>(0xd640));
            r10_128 = rax181->f0;
            r11_127 = rax181->f8;
            continue;
            addr_5198_97:
            rbp122 = rdx155;
            ecx182 = static_cast<uint32_t>(r13_126 + 4);
            while (1) {
                edx183 = *reinterpret_cast<uint32_t*>(&r13_126);
                rsi184 = r12_125;
                do {
                    *reinterpret_cast<uint32_t*>(&rdi185) = edx183;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi185) + 4) = 0;
                    ++edx183;
                    *reinterpret_cast<uint32_t*>(&rdi186) = rdi185->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi186) + 4) = 0;
                    rsi184 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi184) + reinterpret_cast<uint64_t>(rdi186));
                } while (ecx182 != edx183);
                factor_insert_multiplicity(r9_120, rsi184, 1);
                rsp124 = rsp124 - 8 + 8;
                r9_120 = r9_120;
                r8_119 = r8_119;
                ecx182 = ecx182;
                rdx187 = reinterpret_cast<void**>(rbx130->f30 * reinterpret_cast<unsigned char>(rbp122));
                if (reinterpret_cast<unsigned char>(rdx187) > reinterpret_cast<unsigned char>(rbx130->f38)) 
                    goto addr_4f5f_98;
                rbp122 = rdx187;
            }
            addr_50b0_91:
            *reinterpret_cast<int32_t*>(&rcx188) = static_cast<int32_t>(r13_126 + 1);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx188) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rsi189) = *reinterpret_cast<uint32_t*>(&r13_126);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi189) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx190) = rcx188->f10340;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx190) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rsi191) = rsi189->f10340;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi191) + 4) = 0;
            rcx192 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx190) + reinterpret_cast<int64_t>(rsi191) + reinterpret_cast<unsigned char>(r12_125));
            do {
                rbp122 = rdx146;
                factor_insert_multiplicity(r9_120, rcx192, 1);
                rsp124 = rsp124 - 8 + 8;
                r9_120 = r9_120;
                rcx192 = rcx192;
                r8_119 = r8_119;
                rdx146 = reinterpret_cast<void**>(rbx130->f10 * reinterpret_cast<unsigned char>(rbp122));
            } while (reinterpret_cast<unsigned char>(rdx146) <= reinterpret_cast<unsigned char>(rbx130->f18));
            rdx147 = reinterpret_cast<void**>(rbx130->f20 * reinterpret_cast<unsigned char>(rbp122));
            if (reinterpret_cast<unsigned char>(rdx147) > reinterpret_cast<unsigned char>(rbx130->f28)) 
                goto addr_4f4d_96;
            goto addr_5120_95;
        }
        addr_5200_78:
        if (!r8_119) 
            goto addr_5209_130;
        rdi193 = r8_119;
        v194 = r9_120;
        v195 = r8_119;
        al196 = prime2_p(rdi193, rbp122);
        rsp124 = rsp124 - 8 + 8;
        if (al196) 
            goto addr_53a9_132;
        rbx31 = v123;
        r13_30 = v121;
        continue;
        addr_4edd_79:
        *reinterpret_cast<uint32_t*>(&rax197) = *reinterpret_cast<uint32_t*>(&r13_126);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax197) + 4) = 0;
        rax198 = reinterpret_cast<struct s91*>((rax197 << 4) + 0xd640);
        r10_128 = rax198->f0;
        r11_127 = rax198->f8;
        goto addr_4ef9_83;
        addr_4c8b_52:
        if (*reinterpret_cast<void***>(v9 + 8)) 
            goto addr_4cfe_62; else 
            goto addr_4c97_134;
    }
    addr_4cba_18:
    fun_2810("b0 & 1", "src/factor.c", 0x1e0, "gcd2_odd", r8_53, r9_36);
    do {
        fun_2740();
        addr_4ce0_56:
        factor_using_pollard_rho2(rbx31, r13_30, v8 + 1, v9);
        addr_4ad5_10:
        rax199 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
    } while (rax199);
    return;
    addr_4aa0_50:
    if (reinterpret_cast<unsigned char>(rbp6) <= reinterpret_cast<unsigned char>(1)) {
        addr_4c74_137:
        factor_using_pollard_rho(rbp6, v8, v9, rcx87);
        goto addr_4ad5_10;
    } else {
        addr_4aaa_138:
        if (reinterpret_cast<unsigned char>(rbp6) <= reinterpret_cast<unsigned char>(0x17ded78) || (al200 = prime_p_part_0(rbp6, rsi105, rdx104, rcx87), !!al200)) {
            factor_insert_multiplicity(v9, rbp6, 1, v9, rbp6, 1);
            goto addr_4ad5_10;
        }
    }
    addr_4c4f_57:
    rcx87 = v9;
    rsi105 = v32;
    rdx104 = v8 + 1;
    factor_using_pollard_rho2(v61, rsi105, rdx104, rcx87);
    if (reinterpret_cast<unsigned char>(rbp6) > reinterpret_cast<unsigned char>(1)) 
        goto addr_4aaa_138; else 
        goto addr_4c74_137;
    addr_4ca3_59:
    *reinterpret_cast<int32_t*>(&rdx104) = 1;
    *reinterpret_cast<int32_t*>(&rdx104 + 4) = 0;
    rsi105 = v32;
    factor_insert_multiplicity(v9, rsi105, 1);
    goto addr_4aa0_50;
    addr_4a94_61:
    *reinterpret_cast<void***>(v9) = v32;
    *reinterpret_cast<void***>(v9 + 8) = v61;
    goto addr_4aa0_50;
    addr_5209_130:
    if (reinterpret_cast<unsigned char>(rbp122) <= reinterpret_cast<unsigned char>(1)) {
        addr_4d41_66:
        goto v32;
    } else {
        al201 = prime2_p(0, rbp122);
        r9_202 = r9_120;
        if (al201) {
            rsi203 = rbp122;
            rdi204 = r9_202;
        } else {
            rdi205 = rbp122;
            rdx206 = r9_202;
            *reinterpret_cast<int32_t*>(&rsi207) = 1;
            *reinterpret_cast<int32_t*>(&rsi207 + 4) = 0;
            goto addr_5740_143;
        }
    }
    addr_2f20_144:
    *reinterpret_cast<uint32_t*>(&rax208) = *reinterpret_cast<unsigned char*>(rdi204 + 0xfa);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax208) + 4) = 0;
    r8_209 = rsi203;
    r11_210 = reinterpret_cast<void***>(rdi204 + 16);
    r10_211 = reinterpret_cast<signed char*>(rdi204 + 0xe0);
    r9d212 = *reinterpret_cast<uint32_t*>(&rax208);
    if (!*reinterpret_cast<uint32_t*>(&rax208)) 
        goto addr_2fa1_145;
    ebx213 = static_cast<int32_t>(rax208 - 1);
    rcx214 = reinterpret_cast<void*>(static_cast<int64_t>(ebx213));
    rax215 = rcx214;
    do {
        *reinterpret_cast<int32_t*>(&rsi216) = *reinterpret_cast<int32_t*>(&rax215);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi216) + 4) = 0;
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi204 + reinterpret_cast<uint64_t>(rax215) * 8) + 16)) <= reinterpret_cast<unsigned char>(r8_209)) 
            break;
        rax215 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax215) - 1);
        *reinterpret_cast<int32_t*>(&rsi216) = *reinterpret_cast<int32_t*>(&rax215);
    } while (*reinterpret_cast<int32_t*>(&rax215) != -1);
    goto addr_2f80_149;
    if (*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi204 + reinterpret_cast<uint64_t>(rax215) * 8) + 16) == r8_209) {
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_211) + reinterpret_cast<uint64_t>(rax215)) = reinterpret_cast<signed char>(*reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_211) + reinterpret_cast<uint64_t>(rax215)) + 1);
        goto v32;
    }
    rax217 = reinterpret_cast<void*>(static_cast<int64_t>(static_cast<int32_t>(rsi216 + 1)));
    r11_210 = r11_210 + reinterpret_cast<uint64_t>(rax217) * 8;
    r10_211 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_211) + reinterpret_cast<uint64_t>(rax217));
    if (*reinterpret_cast<int32_t*>(&rsi216) >= ebx213) {
        addr_2fa1_145:
        r9d218 = r9d212 + 1;
        *r11_210 = r8_209;
        *r10_211 = 1;
        *reinterpret_cast<unsigned char*>(rdi204 + 0xfa) = *reinterpret_cast<unsigned char*>(&r9d218);
        goto v32;
    }
    do {
        addr_2f80_149:
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi204 + reinterpret_cast<uint64_t>(rcx214) * 8) + 24) = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi204 + reinterpret_cast<uint64_t>(rcx214) * 8) + 16);
        eax219 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi204) + reinterpret_cast<uint64_t>(rcx214) + 0xe0);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi204) + reinterpret_cast<uint64_t>(rcx214) + 0xe1) = *reinterpret_cast<signed char*>(&eax219);
        rcx214 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx214) - 1);
    } while (*reinterpret_cast<int32_t*>(&rsi216) < *reinterpret_cast<int32_t*>(&rcx214));
    goto addr_2fa1_145;
    addr_5740_143:
    v194 = rsi207;
    v156 = rdx206;
    if (reinterpret_cast<unsigned char>(rdi205) <= reinterpret_cast<unsigned char>(1)) {
        addr_5b07_154:
        fun_2810("(1) < (n)", "src/factor.c", 0x5be, "factor_using_pollard_rho", r8_119, r9_202);
    } else {
        r12_220 = rdi205;
        do {
            rcx221 = r12_220;
            esi222 = 64;
            *reinterpret_cast<int32_t*>(&rax223) = 1;
            *reinterpret_cast<int32_t*>(&rax223 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx224) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx224) + 4) = 0;
            rdi225 = reinterpret_cast<void*>(0);
            do {
                r8_119 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx221) << 63);
                rcx221 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx221) >> 1);
                rdx224 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx224) >> 1 | reinterpret_cast<unsigned char>(r8_119));
                if (reinterpret_cast<unsigned char>(rcx221) < reinterpret_cast<unsigned char>(rax223) || rcx221 == rax223 && reinterpret_cast<uint64_t>(rdx224) <= reinterpret_cast<uint64_t>(rdi225)) {
                    cf226 = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi225) < reinterpret_cast<uint64_t>(rdx224));
                    rdi225 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi225) - reinterpret_cast<uint64_t>(rdx224));
                    rax223 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax223) - (reinterpret_cast<unsigned char>(rcx221) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax223) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx221) + static_cast<uint64_t>(cf226))))));
                }
                --esi222;
            } while (esi222);
            *reinterpret_cast<int32_t*>(&rbp227) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp227) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r15_228) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_228) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r14_229) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_229) + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rbp227) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_220) - reinterpret_cast<uint64_t>(rdi225) > reinterpret_cast<uint64_t>(rdi225));
            rbp230 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbp227) & reinterpret_cast<unsigned char>(r12_220)) + (reinterpret_cast<uint64_t>(rdi225) + reinterpret_cast<uint64_t>(rdi225) - reinterpret_cast<unsigned char>(r12_220)));
            rcx231 = rbp230;
            while (reinterpret_cast<unsigned char>(v194) < reinterpret_cast<unsigned char>(r12_220)) {
                r11_232 = rcx231;
                rax233 = reinterpret_cast<unsigned char>(r12_220) >> 1;
                *reinterpret_cast<uint32_t*>(&rax234) = *reinterpret_cast<uint32_t*>(&rax233) & 0x7f;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax234) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax235) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax234);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax235) + 4) = 0;
                rdx236 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax235) + reinterpret_cast<int64_t>(rax235) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax235) * reinterpret_cast<int64_t>(rax235) * reinterpret_cast<unsigned char>(r12_220)));
                rax237 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx236) + reinterpret_cast<uint64_t>(rdx236) - reinterpret_cast<uint64_t>(rdx236) * reinterpret_cast<uint64_t>(rdx236) * reinterpret_cast<unsigned char>(r12_220));
                r8_238 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax237) + reinterpret_cast<uint64_t>(rax237) - reinterpret_cast<uint64_t>(rax237) * reinterpret_cast<uint64_t>(rax237) * reinterpret_cast<unsigned char>(r12_220));
                r10_239 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_220) - reinterpret_cast<unsigned char>(v194));
                r9_240 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v194) - reinterpret_cast<unsigned char>(r12_220));
                while (1) {
                    rax241 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rax241 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax241) + reinterpret_cast<unsigned char>(r12_220));
                    }
                    rbp230 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rbp230) - (reinterpret_cast<unsigned char>(rbp230) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp230) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp230) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax241) < reinterpret_cast<uint64_t>(r10_239))))))) & reinterpret_cast<unsigned char>(r12_220)) + (reinterpret_cast<uint64_t>(rax241) + reinterpret_cast<uint64_t>(r9_240)));
                    r13_242 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        r13_242 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_242) + reinterpret_cast<unsigned char>(r12_220));
                    }
                    rax243 = r14_229;
                    *reinterpret_cast<uint32_t*>(&rax244) = *reinterpret_cast<uint32_t*>(&rax243) & 31;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax244) + 4) = 0;
                    if (rax244 == 1) {
                        rdi245 = r13_242;
                        rax246 = gcd_odd(rdi245, r12_220);
                        if (!reinterpret_cast<int1_t>(rax246 == 1)) 
                            break;
                    }
                    --r14_229;
                    if (r14_229) 
                        continue;
                    rcx247 = r15_228 + r15_228;
                    if (!r15_228) {
                        r15_228 = rcx247;
                        r11_232 = rbp230;
                    } else {
                        do {
                            rdi248 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                            rax249 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi248) + reinterpret_cast<unsigned char>(r12_220));
                            if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                                rdi248 = rax249;
                            }
                            ++r14_229;
                        } while (r15_228 != r14_229);
                        r11_232 = rbp230;
                        r15_228 = rcx247;
                        rbp230 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax249) - (reinterpret_cast<uint64_t>(rax249) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax249) < reinterpret_cast<uint64_t>(rax249) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi248) < reinterpret_cast<uint64_t>(r10_239)))) & reinterpret_cast<unsigned char>(r12_220)) + (reinterpret_cast<uint64_t>(rdi248) + reinterpret_cast<uint64_t>(r9_240)));
                    }
                }
                r9_202 = r8_238;
                r8_119 = r11_232;
                r11_250 = r9_240;
                do {
                    rsi251 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    rax252 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi251) + reinterpret_cast<unsigned char>(r12_220));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rsi251 = rax252;
                    }
                    rbx253 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax252) - (reinterpret_cast<uint64_t>(rax252) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax252) < reinterpret_cast<uint64_t>(rax252) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rsi251) < reinterpret_cast<uint64_t>(r10_239)))) & reinterpret_cast<unsigned char>(r12_220)) + (reinterpret_cast<uint64_t>(rsi251) + reinterpret_cast<uint64_t>(r11_250)));
                    rsi254 = r12_220;
                    rdi245 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rdi245) - (reinterpret_cast<unsigned char>(rdi245) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi245) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rdi245) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_119) < reinterpret_cast<unsigned char>(rbx253))))))) & reinterpret_cast<unsigned char>(r12_220)) + (reinterpret_cast<unsigned char>(r8_119) - reinterpret_cast<unsigned char>(rbx253)));
                    rax255 = gcd_odd(rdi245, rsi254);
                } while (rax255 == 1);
                rcx256 = r8_119;
                r11_257 = rax255;
                if (rax255 == r12_220) 
                    goto addr_5af7_183;
                rdx258 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_220) % reinterpret_cast<unsigned char>(r11_257));
                rax259 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_220) / reinterpret_cast<unsigned char>(r11_257));
                r8_260 = rax259;
                r12_220 = rax259;
                if (reinterpret_cast<unsigned char>(r11_257) > reinterpret_cast<unsigned char>(1) && (reinterpret_cast<unsigned char>(r11_257) <= reinterpret_cast<unsigned char>(0x17ded78) || (al261 = prime_p_part_0(r11_257, rsi254, rdx258, rcx256), r11_257 = r11_257, rcx256 = rcx256, r8_260 = rax259, !!al261))) {
                    *reinterpret_cast<int32_t*>(&rdx262) = 1;
                    *reinterpret_cast<int32_t*>(&rdx262 + 4) = 0;
                    rsi263 = r11_257;
                    factor_insert_multiplicity(v156, rsi263, 1);
                    r8_119 = r8_260;
                    rcx264 = rcx256;
                    zf265 = reinterpret_cast<int1_t>(r8_119 == 1);
                    if (reinterpret_cast<unsigned char>(r8_119) <= reinterpret_cast<unsigned char>(1)) 
                        goto addr_5aca_186; else 
                        goto addr_5a2f_187;
                }
                rdx262 = v156;
                rsi263 = v194 + 1;
                factor_using_pollard_rho(r11_257, rsi263, rdx262, rcx256);
                r8_119 = r8_260;
                rcx264 = rcx256;
                zf265 = reinterpret_cast<int1_t>(r8_119 == 1);
                if (reinterpret_cast<unsigned char>(r8_119) > reinterpret_cast<unsigned char>(1)) {
                    addr_5a2f_187:
                    if (reinterpret_cast<unsigned char>(r8_119) <= reinterpret_cast<unsigned char>(0x17ded78)) 
                        goto addr_5ad7_189;
                    al266 = prime_p_part_0(r8_119, rsi263, rdx262, rcx264);
                    r8_119 = r8_119;
                    if (al266) 
                        goto addr_5ad7_189;
                } else {
                    addr_5aca_186:
                    if (zf265) 
                        goto addr_5b45_191; else 
                        goto addr_5acc_192;
                }
                rbp230 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp230) % reinterpret_cast<unsigned char>(r8_119));
                rcx231 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx264) % reinterpret_cast<unsigned char>(r8_119));
                continue;
                addr_5acc_192:
                *reinterpret_cast<int32_t*>(&rcx231) = 0;
                *reinterpret_cast<int32_t*>(&rcx231 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rbp230) = 0;
                *reinterpret_cast<int32_t*>(&rbp230 + 4) = 0;
            }
            break;
            addr_5af7_183:
            ++v194;
        } while (reinterpret_cast<unsigned char>(r12_220) > reinterpret_cast<unsigned char>(1));
        goto addr_5b07_154;
    }
    fun_2810("a < n", "src/factor.c", 0x5c4, "factor_using_pollard_rho", r8_119, r9_202);
    addr_5b45_191:
    goto v32;
    addr_5ad7_189:
    rdi204 = v156;
    rsi203 = r8_119;
    goto addr_2f20_144;
    addr_53a9_132:
    if (!*reinterpret_cast<void***>(v194 + 8)) {
        *reinterpret_cast<void***>(v194) = rbp122;
        *reinterpret_cast<void***>(v194 + 8) = v195;
        goto addr_4d41_66;
    }
    factor_insert_large_part_0();
    r14_267 = reinterpret_cast<struct s0*>(rdi193 + 0xffffffffffffffff);
    rbp268 = rdi193;
    rsp269 = reinterpret_cast<struct s0**>(rsp124 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x148);
    rax270 = g28;
    v271 = rax270;
    eax272 = 0;
    v273 = r14_267;
    if (*reinterpret_cast<uint32_t*>(&rdi193) & 1) 
        goto addr_5478_198;
    v274 = 0;
    r14_267 = v273;
    addr_5490_200:
    rcx275 = rbp268;
    *reinterpret_cast<int32_t*>(&r15_276) = 0;
    *reinterpret_cast<int32_t*>(&r15_276 + 4) = 0;
    rax277 = reinterpret_cast<unsigned char>(rbp268) >> 1;
    esi278 = 64;
    *reinterpret_cast<uint32_t*>(&rax279) = *reinterpret_cast<uint32_t*>(&rax277) & 0x7f;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax279) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax280) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax279);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax280) + 4) = 0;
    rdx281 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax280) + reinterpret_cast<int64_t>(rax280) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax280) * reinterpret_cast<int64_t>(rax280) * reinterpret_cast<unsigned char>(rbp268)));
    rax282 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx281) + reinterpret_cast<uint64_t>(rdx281) - reinterpret_cast<uint64_t>(rdx281) * reinterpret_cast<uint64_t>(rdx281) * reinterpret_cast<unsigned char>(rbp268));
    *reinterpret_cast<int32_t*>(&rdx283) = 0;
    *reinterpret_cast<int32_t*>(&rdx283 + 4) = 0;
    r13_284 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax282) + reinterpret_cast<uint64_t>(rax282) - reinterpret_cast<uint64_t>(rax282) * reinterpret_cast<uint64_t>(rax282) * reinterpret_cast<unsigned char>(rbp268));
    *reinterpret_cast<int32_t*>(&rax285) = 1;
    *reinterpret_cast<int32_t*>(&rax285 + 4) = 0;
    do {
        rdi286 = reinterpret_cast<unsigned char>(rcx275) << 63;
        rcx275 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx275) >> 1);
        rdx283 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx283) >> 1) | rdi286);
        if (reinterpret_cast<unsigned char>(rcx275) < reinterpret_cast<unsigned char>(rax285) || rcx275 == rax285 && reinterpret_cast<unsigned char>(rdx283) <= reinterpret_cast<unsigned char>(r15_276)) {
            cf287 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_276) < reinterpret_cast<unsigned char>(rdx283));
            r15_276 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_276) - reinterpret_cast<unsigned char>(rdx283));
            rax285 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax285) - (reinterpret_cast<unsigned char>(rcx275) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax285) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx275) + static_cast<uint64_t>(cf287))))));
        }
        --esi278;
    } while (esi278);
    *reinterpret_cast<int32_t*>(&rbx288) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx288) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&r8_119) = v274;
    *reinterpret_cast<int32_t*>(&r8_119 + 4) = 0;
    r9_202 = r15_276;
    *reinterpret_cast<unsigned char*>(&rbx288) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp268) - reinterpret_cast<unsigned char>(r15_276)) > reinterpret_cast<unsigned char>(r15_276));
    rbx289 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbx288) & reinterpret_cast<unsigned char>(rbp268)) + (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r15_276) + reinterpret_cast<unsigned char>(r15_276)) - reinterpret_cast<unsigned char>(rbp268)));
    al290 = millerrabin(rbp268, r13_284, rbx289, r14_267, *reinterpret_cast<uint32_t*>(&r8_119), r9_202);
    if (al290) {
        v291 = reinterpret_cast<void*>(rsp269 - 1 + 1 + 6);
        factor();
        v292 = r14_267;
        r14_267 = r13_284;
        eax293 = v294;
        r13_284 = reinterpret_cast<struct s0*>(0x10340);
        v295 = eax293;
        *reinterpret_cast<uint32_t*>(&rax296) = eax293 - 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax296) + 4) = 0;
        v297 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v291) + rax296 * 8);
        r12_125 = reinterpret_cast<void**>(2);
        rax298 = rbx289;
        rbx289 = r15_276;
        r15_276 = rax298;
        goto addr_55b0_206;
    }
    addr_56fc_207:
    addr_5690_208:
    rax299 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v271) - reinterpret_cast<unsigned char>(g28));
    if (rax299) {
        fun_2740();
    } else {
        goto v195;
    }
    addr_5719_211:
    *reinterpret_cast<int32_t*>(&rdx206) = 0x4f2;
    *reinterpret_cast<int32_t*>(&rdx206 + 4) = 0;
    rsi207 = reinterpret_cast<void**>("src/factor.c");
    rdi205 = reinterpret_cast<void**>("(s1) < (n)");
    fun_2810("(s1) < (n)", "src/factor.c", 0x4f2, "prime_p", r8_119, r9_202);
    goto addr_5740_143;
    addr_5478_198:
    do {
        r14_267 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(r14_267) >> 1);
        ++eax272;
    } while (!(*reinterpret_cast<unsigned char*>(&r14_267) & 1));
    v274 = eax272;
    goto addr_5490_200;
    addr_4c97_134:
    *reinterpret_cast<void***>(v9) = rbp6;
    *reinterpret_cast<void***>(v9 + 8) = r13_30;
    goto addr_4ad5_10;
    addr_5689_214:
    goto addr_5690_208;
    addr_5680_215:
    while (rax300 == rbx289) {
        while (1) {
            *reinterpret_cast<uint32_t*>(&rax301) = r13_284->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax301) + 4) = 0;
            r12_125 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_125) + reinterpret_cast<uint64_t>(rax301));
            rax302 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx289) * reinterpret_cast<unsigned char>(r12_125));
            rdx303 = __intrinsic();
            r15_276 = rax302;
            if (rdx303) {
                if (reinterpret_cast<unsigned char>(rbp268) <= reinterpret_cast<unsigned char>(rdx303)) 
                    goto addr_5719_211;
                rcx304 = rbp268;
                esi305 = 64;
                *reinterpret_cast<int32_t*>(&rax306) = 0;
                *reinterpret_cast<int32_t*>(&rax306 + 4) = 0;
                do {
                    rdi307 = reinterpret_cast<unsigned char>(rcx304) << 63;
                    rcx304 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx304) >> 1);
                    rax306 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax306) >> 1) | rdi307);
                    if (reinterpret_cast<unsigned char>(rcx304) < reinterpret_cast<unsigned char>(rdx303) || rcx304 == rdx303 && reinterpret_cast<unsigned char>(rax306) <= reinterpret_cast<unsigned char>(r15_276)) {
                        cf308 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_276) < reinterpret_cast<unsigned char>(rax306));
                        r15_276 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_276) - reinterpret_cast<unsigned char>(rax306));
                        rdx303 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx303) - (reinterpret_cast<unsigned char>(rcx304) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx303) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx304) + static_cast<uint64_t>(cf308))))));
                    }
                    --esi305;
                } while (esi305);
            } else {
                r15_276 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax302) % reinterpret_cast<unsigned char>(rbp268));
            }
            *reinterpret_cast<uint32_t*>(&r8_119) = v274;
            *reinterpret_cast<int32_t*>(&r8_119 + 4) = 0;
            r9_202 = rbx289;
            al309 = millerrabin(rbp268, r14_267, r15_276, v292, *reinterpret_cast<uint32_t*>(&r8_119), r9_202);
            if (!al309) 
                goto addr_56fc_207;
            r13_284 = reinterpret_cast<struct s0*>(&r13_284->f1);
            if (reinterpret_cast<int1_t>(0x105dc == r13_284)) 
                break;
            addr_55b0_206:
            if (!v295) 
                goto addr_5690_208;
            r11_310 = v291;
            do {
                r8_119 = rbx289;
                rax300 = powm(r15_276, reinterpret_cast<uint64_t>(v273) / v311, rbp268, r14_267, r8_119, r9_202);
                if (v297 == r11_310) 
                    goto addr_5680_215;
                r11_310 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r11_310) + 8);
            } while (rax300 != rbx289);
        }
        fun_2700();
        fun_2a20();
        rax300 = fun_25f0();
    }
    goto addr_5689_214;
}

void** gd01d = reinterpret_cast<void**>(0x66);

/* factor_insert_large.part.0 */
void factor_insert_large_part_0() {
    int64_t v1;
    int64_t rax2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    int64_t r8_6;
    int64_t r9_7;
    void** rdi8;
    void** rsi9;
    void** rsi10;
    uint64_t rdx11;
    void** r8_12;
    unsigned char dl13;
    uint1_t cf14;
    void** rsi15;
    uint64_t rdx16;
    uint1_t cf17;
    void** r8_18;
    uint64_t rax19;

    v1 = rax2;
    rcx3 = reinterpret_cast<void**>("factor_insert_large");
    fun_2810("factors->plarge[1] == 0", "src/factor.c", 0x232, "factor_insert_large", r8_4, r9_5, "factors->plarge[1] == 0", "src/factor.c", 0x232, "factor_insert_large", r8_6, r9_7);
    rdi8 = reinterpret_cast<void**>("src/factor.c");
    rsi9 = reinterpret_cast<void**>(0x232);
    if (!0) {
        do {
            rsi10 = rdi8;
            rdi8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi8) >> 1);
            rdx11 = reinterpret_cast<unsigned char>(rsi9) >> 1;
            rsi9 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rsi10) << 63) | rdx11);
        } while (!(*reinterpret_cast<uint32_t*>(&rdx11) & 1));
    }
    if (0) {
        addr_3a3f_6:
        gcd_odd(r8_12, rsi9);
        gd01d = reinterpret_cast<void**>(0);
        goto v1;
    } else {
        while (1) {
            if (reinterpret_cast<unsigned char>(rdi8) > reinterpret_cast<unsigned char>(rcx3) || (dl13 = reinterpret_cast<uint1_t>(rdi8 == rcx3), reinterpret_cast<unsigned char>(rsi9) > reinterpret_cast<unsigned char>(r8_12)) && dl13) {
                cf14 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rsi9) < reinterpret_cast<unsigned char>(r8_12));
                rsi9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi9) - reinterpret_cast<unsigned char>(r8_12));
                rdi8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi8) - (reinterpret_cast<unsigned char>(rcx3) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi8) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx3) + static_cast<uint64_t>(cf14))))));
                do {
                    rsi15 = rdi8;
                    rdi8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi8) >> 1);
                    rdx16 = reinterpret_cast<unsigned char>(rsi9) >> 1;
                    rsi9 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rsi15) << 63) | rdx16);
                } while (!(*reinterpret_cast<uint32_t*>(&rdx16) & 1));
                if (!(reinterpret_cast<unsigned char>(rdi8) | reinterpret_cast<unsigned char>(rcx3))) 
                    break;
            } else {
                if (reinterpret_cast<unsigned char>(rdi8) < reinterpret_cast<unsigned char>(rcx3)) 
                    goto addr_3a0f_12;
                if (reinterpret_cast<unsigned char>(rsi9) >= reinterpret_cast<unsigned char>(r8_12)) 
                    goto addr_3a86_14;
                if (!dl13) 
                    goto addr_3a86_14;
                addr_3a0f_12:
                cf17 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_12) < reinterpret_cast<unsigned char>(rsi9));
                r8_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_12) - reinterpret_cast<unsigned char>(rsi9));
                rcx3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx3) - (reinterpret_cast<unsigned char>(rdi8) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx3) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rdi8) + static_cast<uint64_t>(cf17))))));
                do {
                    r8_18 = rcx3;
                    rcx3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx3) >> 1);
                    rax19 = reinterpret_cast<unsigned char>(r8_12) >> 1;
                    r8_12 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_18) << 63) | rax19);
                } while (!(*reinterpret_cast<unsigned char*>(&rax19) & 1));
                if (!(reinterpret_cast<unsigned char>(rdi8) | reinterpret_cast<unsigned char>(rcx3))) 
                    goto addr_3a3f_6;
            }
        }
    }
    goto addr_3a3f_6;
    addr_3a86_14:
    gd01d = rdi8;
    goto v1;
}

void** locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, int64_t rdx) {
    void** rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax4))) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax4 + 1))) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (*reinterpret_cast<unsigned char*>(rax4 + 2) == 49 && (*reinterpret_cast<signed char*>(rax4 + 3) == 56 && (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax4 + 4) == 48) && (*reinterpret_cast<signed char*>(rax4 + 5) == 51 && (*reinterpret_cast<signed char*>(rax4 + 6) == 48 && !*reinterpret_cast<signed char*>(rax4 + 7)))))))) {
            rax7 = reinterpret_cast<void**>(0x106bc);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0x106b7);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax4 + 1))) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rax4 + 2)) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (*reinterpret_cast<signed char*>(rax4 + 3) == 45 && (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax4 + 4) == 56) && !*reinterpret_cast<signed char*>(rax4 + 5))))) {
            rax10 = reinterpret_cast<void**>(0x106c0);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0x106b3);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

/* mbiter_multi_next.part.0 */
void mbiter_multi_next_part_0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    fun_2810("iter->cur.wc == 0", "lib/mbiter.h", 0xa3, "mbiter_multi_next", r8, r9, "iter->cur.wc == 0", "lib/mbiter.h", 0xa3, "mbiter_multi_next", r8, r9);
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t g14d08 = 0;

void fun_2033() {
    __asm__("cli ");
    goto g14d08;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2403() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2413() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2423() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2433() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2443() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2453() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2463() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2473() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2483() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2493() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2503() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2513() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2523() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2533() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2543() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2553() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2563() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2573() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2583() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2593() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_25a3() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_25b3() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __uflow = 0x2030;

void fun_25c3() {
    __asm__("cli ");
    goto __uflow;
}

int64_t __gmpz_scan1 = 0x2040;

void fun_25d3() {
    __asm__("cli ");
    goto __gmpz_scan1;
}

int64_t free = 0x2050;

void fun_25e3() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2060;

void fun_25f3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2070;

void fun_2603() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2080;

void fun_2613() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2090;

void fun_2623() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x20a0;

void fun_2633() {
    __asm__("cli ");
    goto __fpending;
}

int64_t iconv = 0x20b0;

void fun_2643() {
    __asm__("cli ");
    goto iconv;
}

int64_t __gmpz_sub_ui = 0x20c0;

void fun_2653() {
    __asm__("cli ");
    goto __gmpz_sub_ui;
}

int64_t __gmpz_clears = 0x20d0;

void fun_2663() {
    __asm__("cli ");
    goto __gmpz_clears;
}

int64_t isatty = 0x20e0;

void fun_2673() {
    __asm__("cli ");
    goto isatty;
}

int64_t __gmpz_set_ui = 0x20f0;

void fun_2683() {
    __asm__("cli ");
    goto __gmpz_set_ui;
}

int64_t iswcntrl = 0x2100;

void fun_2693() {
    __asm__("cli ");
    goto iswcntrl;
}

int64_t reallocarray = 0x2110;

void fun_26a3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t write = 0x2120;

void fun_26b3() {
    __asm__("cli ");
    goto write;
}

int64_t __gmpz_sub = 0x2130;

void fun_26c3() {
    __asm__("cli ");
    goto __gmpz_sub;
}

int64_t textdomain = 0x2140;

void fun_26d3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x2150;

void fun_26e3() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x2160;

void fun_26f3() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2170;

void fun_2703() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2180;

void fun_2713() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2190;

void fun_2723() {
    __asm__("cli ");
    goto strlen;
}

int64_t __gmpz_powm = 0x21a0;

void fun_2733() {
    __asm__("cli ");
    goto __gmpz_powm;
}

int64_t __stack_chk_fail = 0x21b0;

void fun_2743() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x21c0;

void fun_2753() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t __gmpz_divexact = 0x21d0;

void fun_2763() {
    __asm__("cli ");
    goto __gmpz_divexact;
}

int64_t mbrtowc = 0x21e0;

void fun_2773() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t __gmpz_inits = 0x21f0;

void fun_2783() {
    __asm__("cli ");
    goto __gmpz_inits;
}

int64_t strchr = 0x2200;

void fun_2793() {
    __asm__("cli ");
    goto strchr;
}

int64_t __gmpz_set = 0x2210;

void fun_27a3() {
    __asm__("cli ");
    goto __gmpz_set;
}

int64_t __gmpz_mul = 0x2220;

void fun_27b3() {
    __asm__("cli ");
    goto __gmpz_mul;
}

int64_t __overflow = 0x2230;

void fun_27c3() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x2240;

void fun_27d3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t __gmpz_tdiv_q_ui = 0x2250;

void fun_27e3() {
    __asm__("cli ");
    goto __gmpz_tdiv_q_ui;
}

int64_t lseek = 0x2260;

void fun_27f3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __gmpz_out_str = 0x2270;

void fun_2803() {
    __asm__("cli ");
    goto __gmpz_out_str;
}

int64_t __assert_fail = 0x2280;

void fun_2813() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x2290;

void fun_2823() {
    __asm__("cli ");
    goto memset;
}

int64_t strnlen = 0x22a0;

void fun_2833() {
    __asm__("cli ");
    goto strnlen;
}

int64_t __gmpz_fdiv_q_2exp = 0x22b0;

void fun_2843() {
    __asm__("cli ");
    goto __gmpz_fdiv_q_2exp;
}

int64_t memchr = 0x22c0;

void fun_2853() {
    __asm__("cli ");
    goto memchr;
}

int64_t memcmp = 0x22d0;

void fun_2863() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x22e0;

void fun_2873() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x22f0;

void fun_2883() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2300;

void fun_2893() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2310;

void fun_28a3() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t __gmpz_init_set_si = 0x2320;

void fun_28b3() {
    __asm__("cli ");
    goto __gmpz_init_set_si;
}

int64_t __gmpz_add_ui = 0x2330;

void fun_28c3() {
    __asm__("cli ");
    goto __gmpz_add_ui;
}

int64_t memcpy = 0x2340;

void fun_28d3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2350;

void fun_28e3() {
    __asm__("cli ");
    goto fileno;
}

int64_t __gmpz_divisible_ui_p = 0x2360;

void fun_28f3() {
    __asm__("cli ");
    goto __gmpz_divisible_ui_p;
}

int64_t wcwidth = 0x2370;

void fun_2903() {
    __asm__("cli ");
    goto wcwidth;
}

int64_t iswalnum = 0x2380;

void fun_2913() {
    __asm__("cli ");
    goto iswalnum;
}

int64_t malloc = 0x2390;

void fun_2923() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x23a0;

void fun_2933() {
    __asm__("cli ");
    goto fflush;
}

int64_t __gmpz_clear = 0x23b0;

void fun_2943() {
    __asm__("cli ");
    goto __gmpz_clear;
}

int64_t nl_langinfo = 0x23c0;

void fun_2953() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __gmpz_tdiv_q_2exp = 0x23d0;

void fun_2963() {
    __asm__("cli ");
    goto __gmpz_tdiv_q_2exp;
}

int64_t __freading = 0x23e0;

void fun_2973() {
    __asm__("cli ");
    goto __freading;
}

int64_t fwrite_unlocked = 0x23f0;

void fun_2983() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t realloc = 0x2400;

void fun_2993() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2410;

void fun_29a3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2420;

void fun_29b3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t iconv_close = 0x2430;

void fun_29c3() {
    __asm__("cli ");
    goto iconv_close;
}

int64_t __gmpz_mod = 0x2440;

void fun_29d3() {
    __asm__("cli ");
    goto __gmpz_mod;
}

int64_t iswspace = 0x2450;

void fun_29e3() {
    __asm__("cli ");
    goto iswspace;
}

int64_t __gmpz_init_set_ui = 0x2460;

void fun_29f3() {
    __asm__("cli ");
    goto __gmpz_init_set_ui;
}

int64_t __gmpz_cmp = 0x2470;

void fun_2a03() {
    __asm__("cli ");
    goto __gmpz_cmp;
}

int64_t memmove = 0x2480;

void fun_2a13() {
    __asm__("cli ");
    goto memmove;
}

int64_t error = 0x2490;

void fun_2a23() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x24a0;

void fun_2a33() {
    __asm__("cli ");
    goto fseeko;
}

int64_t __gmpz_gcd = 0x24b0;

void fun_2a43() {
    __asm__("cli ");
    goto __gmpz_gcd;
}

int64_t __gmpz_init_set_str = 0x24c0;

void fun_2a53() {
    __asm__("cli ");
    goto __gmpz_init_set_str;
}

int64_t __cxa_atexit = 0x24d0;

void fun_2a63() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x24e0;

void fun_2a73() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x24f0;

void fun_2a83() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2500;

void fun_2a93() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t __gmpz_cmp_ui = 0x2510;

void fun_2aa3() {
    __asm__("cli ");
    goto __gmpz_cmp_ui;
}

int64_t __gmpz_init = 0x2520;

void fun_2ab3() {
    __asm__("cli ");
    goto __gmpz_init;
}

int64_t fflush_unlocked = 0x2530;

void fun_2ac3() {
    __asm__("cli ");
    goto fflush_unlocked;
}

int64_t __gmpz_powm_ui = 0x2540;

void fun_2ad3() {
    __asm__("cli ");
    goto __gmpz_powm_ui;
}

int64_t strdup = 0x2550;

void fun_2ae3() {
    __asm__("cli ");
    goto strdup;
}

int64_t mbsinit = 0x2560;

void fun_2af3() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2570;

void fun_2b03() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2580;

void fun_2b13() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

int64_t iconv_open = 0x2590;

void fun_2b23() {
    __asm__("cli ");
    goto iconv_open;
}

int64_t __sprintf_chk = 0x25a0;

void fun_2b33() {
    __asm__("cli ");
    goto __sprintf_chk;
}

void set_program_name(void** rdi);

void** fun_29a0(int64_t rdi, ...);

void fun_26f0(int64_t rdi, void** rsi);

void fun_26d0(int64_t rdi, void** rsi);

void** xmalloc(uint64_t rdi, void** rsi, void** rdx);

void atexit(int64_t rdi, void** rsi);

int32_t fun_2750(int64_t rdi, void** rsi, int64_t rdx, void** rcx);

void usage();

void** proper_name_utf8(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx);

int64_t Version = 0x105e3;

void version_etc(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, void** r9, void** a7, int64_t a8);

int32_t fun_2a70();

int32_t optind = 0;

void init_tokenbuffer(void** rdi, void** rsi, int64_t rdx, void** rcx);

int64_t stdin = 0;

void** readtoken(int64_t rdi, void** rsi, int64_t rdx, void** rcx, ...);

int64_t fun_2ba3(int32_t edi, void** rsi, void** rdx) {
    int32_t ebp4;
    void** rbx5;
    void** rdi6;
    void** rax7;
    void** v8;
    void* rsp9;
    int1_t zf10;
    void** rax11;
    void** rsp12;
    void** rsi13;
    int64_t rdi14;
    int32_t eax15;
    void** rax16;
    void** rax17;
    void** rdi18;
    int64_t rcx19;
    int64_t r12_20;
    void** rbx21;
    uint32_t r13d22;
    int64_t rdi23;
    void** rax24;
    void** v25;
    uint32_t eax26;
    void** rdi27;
    uint32_t eax28;
    uint32_t r13d29;
    int64_t rax30;
    void* rdx31;

    __asm__("cli ");
    ebp4 = edi;
    rbx5 = rsi;
    rdi6 = *reinterpret_cast<void***>(rsi);
    rax7 = g28;
    v8 = rax7;
    set_program_name(rdi6);
    fun_29a0(6, 6);
    fun_26f0("coreutils", "/usr/local/share/locale");
    fun_26d0("coreutils", "/usr/local/share/locale");
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 32 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    zf10 = lbuf == 0;
    if (zf10) {
        rax11 = xmalloc(0x400, "/usr/local/share/locale", rdx);
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        lbuf = rax11;
        g150f8 = rax11;
    }
    atexit(0x69d0, "/usr/local/share/locale");
    atexit(lbuf_flush, "/usr/local/share/locale");
    rsp12 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 + 8);
    while (rsi13 = rbx5, *reinterpret_cast<int32_t*>(&rdi14) = ebp4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0, eax15 = fun_2750(rdi14, rsi13, "h", 0x14a00), rsp12 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp12 - 8) + 8), eax15 != -1) {
        if (eax15 == 0x68) {
            addr_2cf0_6:
            print_exponents = 1;
            continue;
        } else {
            if (eax15 > 0x68) {
                addr_2cc0_8:
                if (eax15 != 0x80) 
                    goto addr_2db6_9;
            } else {
                if (eax15 != 0xffffff7d) {
                    if (eax15 != 0xffffff7e) 
                        goto addr_2db6_9;
                    usage();
                    rsp12 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp12 - 8) + 8);
                    goto addr_2cf0_6;
                } else {
                    rax16 = proper_name_utf8("Niels Moller", 0xd161, "h", 0x14a00);
                    rbx5 = rax16;
                    rax17 = proper_name_utf8("Torbjorn Granlund", 0xd17c, "h", 0x14a00);
                    rdi18 = stdout;
                    rcx19 = Version;
                    version_etc(rdi18, "factor", "GNU coreutils", rcx19, "Paul Rubin", rax17, rbx5, 0);
                    eax15 = fun_2a70();
                    rsp12 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp12 - 8) + 8 - 8 + 8 - 8 - 8 - 8 + 8 - 8 + 8);
                    goto addr_2cc0_8;
                }
            }
        }
        dev_debug = 1;
    }
    r12_20 = optind;
    if (*reinterpret_cast<int32_t*>(&r12_20) >= ebp4) {
        rbx21 = rsp12;
        r13d22 = 1;
        init_tokenbuffer(rbx21, rsi13, "h", 0x14a00);
        while (rdi23 = stdin, rax24 = readtoken(rdi23, "\n\t ", 3, rbx21), !!(rax24 + 1)) {
            eax26 = print_factors(v25, "\n\t ", 3, rbx21);
            r13d22 = r13d22 & eax26;
        }
        fun_25e0(v25, "\n\t ", v25, "\n\t ");
    } else {
        r13d22 = 1;
        do {
            rdi27 = *reinterpret_cast<void***>(rbx5 + r12_20 * 8);
            eax28 = print_factors(rdi27, rsi13, "h", 0x14a00);
            if (!*reinterpret_cast<signed char*>(&eax28)) {
                r13d22 = 0;
            }
            ++r12_20;
        } while (ebp4 > *reinterpret_cast<int32_t*>(&r12_20));
    }
    r13d29 = r13d22 ^ 1;
    *reinterpret_cast<uint32_t*>(&rax30) = *reinterpret_cast<unsigned char*>(&r13d29);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax30) + 4) = 0;
    rdx31 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
    if (!rdx31) {
        return rax30;
    }
    addr_2dc0_26:
    fun_2740();
    addr_2db6_9:
    usage();
    goto addr_2dc0_26;
}

int64_t __libc_start_main = 0;

void fun_2dd3() {
    __asm__("cli ");
    __libc_start_main(0x2ba0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0x15008;

void fun_25b0(int64_t rdi);

int64_t fun_2e73() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_25b0(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2eb3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void* full_write(int64_t rdi);

void fun_3833() {
    void** rsi1;
    void** rbx2;
    void* rax3;
    void** rax4;

    __asm__("cli ");
    rsi1 = lbuf;
    rbx2 = g150f8;
    rax3 = full_write(1);
    if (!reinterpret_cast<int1_t>(rax3 == reinterpret_cast<unsigned char>(rbx2) - reinterpret_cast<unsigned char>(rsi1))) {
        fun_2700();
        fun_2600();
        fun_2a20();
    } else {
        rax4 = lbuf;
        g150f8 = rax4;
        return;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_2870(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_2980(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx);

int32_t fun_2890(void** rdi, void** rsi, void** rdx);

int32_t fun_2610(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

void fun_6653(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** rcx6;
    void** r12_7;
    void** rax8;
    void** rcx9;
    void** r12_10;
    void** rax11;
    void** r12_12;
    void** rax13;
    int32_t eax14;
    void** r13_15;
    void** rax16;
    void** rax17;
    int32_t eax18;
    void** rax19;
    void** rax20;
    void** rax21;
    int32_t eax22;
    void** rax23;
    void** r15_24;
    void** rax25;
    void** rax26;
    void** rax27;
    void** rdi28;
    void** r8_29;
    void** r9_30;
    void** v31;
    void** v32;
    void** v33;
    void** v34;
    void** v35;
    void** v36;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2700();
            fun_29b0(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_2700();
            fun_2870(rax8, r12_7, 5, rcx6);
            rcx9 = stdout;
            fun_2980("  -h, --exponents   print repeated factors in form p^e unless e is 1\n", 1, 69, rcx9);
            r12_10 = stdout;
            rax11 = fun_2700();
            fun_2870(rax11, r12_10, 5, rcx9);
            r12_12 = stdout;
            rax13 = fun_2700();
            fun_2870(rax13, r12_12, 5, rcx9);
            do {
                if (1) 
                    break;
                eax14 = fun_2890("factor", 0, 5);
            } while (eax14);
            r13_15 = v4;
            if (!r13_15) {
                rax16 = fun_2700();
                fun_29b0(1, rax16, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax17 = fun_29a0(5);
                if (!rax17 || (eax18 = fun_2610(rax17, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax18)) {
                    rax19 = fun_2700();
                    r13_15 = reinterpret_cast<void**>("factor");
                    fun_29b0(1, rax19, "https://www.gnu.org/software/coreutils/", "factor");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_15 = reinterpret_cast<void**>("factor");
                    goto addr_6980_9;
                }
            } else {
                rax20 = fun_2700();
                fun_29b0(1, rax20, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax21 = fun_29a0(5);
                if (!rax21 || (eax22 = fun_2610(rax21, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax22)) {
                    addr_6886_11:
                    rax23 = fun_2700();
                    fun_29b0(1, rax23, "https://www.gnu.org/software/coreutils/", "factor");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_15 == "factor")) {
                        r12_2 = reinterpret_cast<void**>(0x10b0b);
                    }
                } else {
                    addr_6980_9:
                    r15_24 = stdout;
                    rax25 = fun_2700();
                    fun_2870(rax25, r15_24, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_6886_11;
                }
            }
            rax26 = fun_2700();
            rcx6 = r12_2;
            fun_29b0(1, rax26, r13_15, rcx6);
            addr_66ae_14:
            fun_2a70();
        }
    } else {
        rax27 = fun_2700();
        rdi28 = stderr;
        rcx6 = r12_2;
        fun_2a90(rdi28, 1, rax27, rcx6, r8_29, r9_30, v31, v32, v33, v34, v35, v36);
        goto addr_66ae_14;
    }
}

int64_t file_name = 0;

void fun_69b3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_69c3(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_2620(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_69d3() {
    void** rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2600(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2700();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_6a63_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2a20();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2620(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_6a63_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2a20();
    }
}

int64_t safe_write(int64_t rdi, int64_t rsi, int64_t rdx);

int64_t fun_6a83(int32_t edi, int64_t rsi, int64_t rdx) {
    int64_t r13_4;
    int32_t r12d5;
    int64_t rbp6;
    int64_t rbx7;
    int64_t rdi8;
    int64_t rax9;
    int32_t* rax10;

    __asm__("cli ");
    if (!rdx) {
        *reinterpret_cast<int32_t*>(&r13_4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_4) + 4) = 0;
    } else {
        r12d5 = edi;
        rbp6 = rsi;
        rbx7 = rdx;
        *reinterpret_cast<int32_t*>(&r13_4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_4) + 4) = 0;
        do {
            *reinterpret_cast<int32_t*>(&rdi8) = r12d5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
            rax9 = safe_write(rdi8, rbp6, rbx7);
            if (rax9 == -1) 
                break;
            if (!rax9) 
                goto addr_6ae0_6;
            r13_4 = r13_4 + rax9;
            rbp6 = rbp6 + rax9;
            rbx7 = rbx7 - rax9;
        } while (rbx7);
    }
    return r13_4;
    addr_6ae0_6:
    rax10 = fun_2600();
    *rax10 = 28;
    return r13_4;
}

struct s92 {
    signed char[20] pad20;
    signed char f14;
};

signed char* fun_6b03(uint64_t rdi, struct s92* rsi) {
    uint64_t rcx3;
    signed char* r8_4;
    uint64_t rdx5;
    uint64_t rsi6;
    uint64_t rax7;
    int32_t eax8;
    uint64_t rax9;

    __asm__("cli ");
    rsi->f14 = 0;
    rcx3 = rdi;
    r8_4 = &rsi->f14;
    do {
        --r8_4;
        rdx5 = __intrinsic() >> 3;
        rsi6 = rdx5 + rdx5 * 4;
        rax7 = rcx3 - (rsi6 + rsi6);
        eax8 = *reinterpret_cast<int32_t*>(&rax7) + 48;
        *r8_4 = *reinterpret_cast<signed char*>(&eax8);
        rax9 = rcx3;
        rcx3 = rdx5;
    } while (rax9 > 9);
    return r8_4;
}

struct s93 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s93* fun_27d0();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_6b63(void** rdi) {
    void** rcx2;
    void** r8_3;
    void** r9_4;
    void** rbx5;
    struct s93* rax6;
    void** r12_7;
    void** rcx8;
    int32_t eax9;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2a80("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2, r8_3, r9_4);
        fun_25f0();
    } else {
        rbx5 = rdi;
        rax6 = fun_27d0();
        if (rax6 && ((r12_7 = reinterpret_cast<void**>(&rax6->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_7) - reinterpret_cast<unsigned char>(rbx5)) > reinterpret_cast<int64_t>(6)) && (eax9 = fun_2610(reinterpret_cast<int64_t>(rax6) + 0xfffffffffffffffa, "/.libs/", 7, rcx8), !eax9))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax6->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_7 + 1) == 0x74) || *reinterpret_cast<unsigned char*>(r12_7 + 2) != 45)) {
                rbx5 = r12_7;
            } else {
                rbx5 = reinterpret_cast<void**>(&rax6->f4);
                __progname = rbx5;
            }
        }
        program_name = rbx5;
        __progname_full = rbx5;
        return;
    }
}

void fun_2b30(void** rdi, void** rsi, int64_t rdx, int64_t rcx, void** r8, void** r9);

void** fun_7003(void** rdi) {
    void** rax2;
    void** rcx3;
    signed char al4;
    void** rax5;
    void** rax6;
    void** rax7;

    __asm__("cli ");
    rax2 = fun_2700();
    if (rdi == rax2 || (al4 = mbsstr_trimmed_wordbounded(rax2, rdi, 5, rcx3), !!al4)) {
        return rax2;
    } else {
        rax5 = fun_2720(rax2, rax2);
        rax6 = fun_2720(rdi, rdi);
        rax7 = xmalloc(reinterpret_cast<unsigned char>(rax5) + reinterpret_cast<unsigned char>(rax6) + 4, rdi, 5);
        fun_2b30(rax7, 1, -1, "%s (%s)", rax2, rdi);
        return rax7;
    }
}

int32_t c_strcasecmp(void** rdi, void** rsi, void** rdx);

void** xstr_iconv(void** rdi, void** rsi, void** rdx);

struct s94 {
    void** f0;
    signed char[7] pad8;
    int16_t f8;
    signed char fa;
};

int64_t fun_2790(void** rdi, void** rsi, void** rdx);

void** fun_7093(void** rdi, void** rsi) {
    void** rdx3;
    void** rbp4;
    void** rax5;
    void** r12_6;
    void** rax7;
    int32_t eax8;
    void** rax9;
    void** v10;
    void** rax11;
    void** rax12;
    void** rcx13;
    void** r8_14;
    void** rax15;
    struct s94* rbx16;
    void** rax17;
    void** rbx18;
    void** r14_19;
    int32_t eax20;
    int64_t rax21;
    int32_t eax22;
    int32_t eax23;
    void** rsi24;
    void** rcx25;
    signed char al26;
    void** r10_27;
    int32_t eax28;
    signed char al29;
    signed char al30;
    signed char al31;
    void** rax32;
    void** rax33;
    void** rax34;
    signed char al35;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rdx3) = 5;
    *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
    rbp4 = rsi;
    rax5 = fun_2700();
    r12_6 = rax5;
    rax7 = locale_charset();
    eax8 = c_strcasecmp(rax7, "UTF-8", 5);
    if (eax8) {
        rax9 = xstr_iconv(rbp4, "UTF-8", rax7);
        v10 = rax9;
        rax11 = fun_2720(rax7, rax7);
        rax12 = xmalloc(rax11 + 11, "UTF-8", rax7);
        rax15 = fun_28d0(rax12, rax7, rax11, rcx13, r8_14);
        rbx16 = reinterpret_cast<struct s94*>(reinterpret_cast<unsigned char>(rax11) + reinterpret_cast<unsigned char>(rax15));
        rbx16->f0 = reinterpret_cast<void**>(0x4c534e4152542f2f);
        rdx3 = rax15;
        rbx16->f8 = 0x5449;
        rbx16->fa = 0;
        rax17 = xstr_iconv(rbp4, "UTF-8", rdx3);
        rbp4 = rax17;
        fun_25e0(rax15, "UTF-8", rax15, "UTF-8");
        if (!rbp4) {
            addr_7293_3:
            if (!v10) {
                v10 = reinterpret_cast<void**>(0);
                rbp4 = rdi;
                *reinterpret_cast<int32_t*>(&rbx18) = 0;
                *reinterpret_cast<int32_t*>(&rbx18 + 4) = 0;
                *reinterpret_cast<int32_t*>(&r14_19) = 0;
                *reinterpret_cast<int32_t*>(&r14_19 + 4) = 0;
            } else {
                eax20 = fun_2890(r12_6, rdi, rdx3);
                if (eax20) {
                    *reinterpret_cast<int32_t*>(&rbx18) = 0;
                    *reinterpret_cast<int32_t*>(&rbx18 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rbp4) = 0;
                    *reinterpret_cast<int32_t*>(&rbp4 + 4) = 0;
                    goto addr_7304_7;
                } else {
                    r12_6 = v10;
                    goto addr_715a_9;
                }
            }
        } else {
            rax21 = fun_2790(rbp4, 63, rdx3);
            if (!rax21) {
                if (!v10) {
                    rbx18 = rbp4;
                    r14_19 = rbp4;
                } else {
                    eax22 = fun_2890(r12_6, rdi, rdx3);
                    if (eax22) {
                        rbx18 = rbp4;
                        goto addr_7304_7;
                    } else {
                        if (v10 == rbp4) {
                            r12_6 = v10;
                            goto addr_715a_9;
                        } else {
                            fun_25e0(rbp4, rdi, rbp4, rdi);
                            r12_6 = v10;
                            goto addr_715a_9;
                        }
                    }
                }
            } else {
                fun_25e0(rbp4, 63, rbp4, 63);
                goto addr_7293_3;
            }
        }
    } else {
        eax23 = fun_2890(r12_6, rdi, 5);
        if (!rbp4) {
            if (eax23) {
                rsi24 = rdi;
                al26 = mbsstr_trimmed_wordbounded(r12_6, rsi24, 5, rcx25);
                if (!al26) {
                    *reinterpret_cast<int32_t*>(&r14_19) = 0;
                    *reinterpret_cast<int32_t*>(&r14_19 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&r10_27) = 0;
                    *reinterpret_cast<int32_t*>(&r10_27 + 4) = 0;
                    rbp4 = rdi;
                    goto addr_719c_23;
                }
            } else {
                r12_6 = rdi;
                goto addr_715a_9;
            }
        } else {
            v10 = rbp4;
            rbx18 = rbp4;
            *reinterpret_cast<int32_t*>(&r14_19) = 0;
            *reinterpret_cast<int32_t*>(&r14_19 + 4) = 0;
        }
    }
    eax28 = fun_2890(r12_6, rdi, rdx3);
    if (!eax28) {
        r12_6 = rbp4;
    } else {
        rsi24 = rdi;
        al29 = mbsstr_trimmed_wordbounded(r12_6, rsi24, rdx3, 0x4c534e4152542f2f);
        *reinterpret_cast<int32_t*>(&r10_27) = 0;
        *reinterpret_cast<int32_t*>(&r10_27 + 4) = 0;
        if (al29) 
            goto addr_714d_29; else 
            goto addr_711f_30;
    }
    addr_715a_9:
    return r12_6;
    addr_711f_30:
    if (v10 && (rsi24 = v10, al30 = mbsstr_trimmed_wordbounded(r12_6, rsi24, rdx3, 0x4c534e4152542f2f), r10_27 = r10_27, !!al30) || rbx18 && (rsi24 = rbx18, al31 = mbsstr_trimmed_wordbounded(r12_6, rsi24, rdx3, 0x4c534e4152542f2f), r10_27 = r10_27, !!al31)) {
        if (!r10_27) {
            addr_714d_29:
            if (r14_19) {
                fun_25e0(r14_19, rsi24, r14_19, rsi24);
                goto addr_715a_9;
            }
        } else {
            addr_7145_33:
            fun_25e0(r10_27, rsi24, r10_27, rsi24);
            goto addr_714d_29;
        }
    } else {
        addr_719c_23:
        rax32 = fun_2720(r12_6, r12_6);
        rax33 = fun_2720(rbp4, rbp4);
        rax34 = xmalloc(reinterpret_cast<unsigned char>(rax32) + reinterpret_cast<unsigned char>(rax33) + 4, rsi24, rdx3);
        *reinterpret_cast<int32_t*>(&rsi24) = 1;
        *reinterpret_cast<int32_t*>(&rsi24 + 4) = 0;
        fun_2b30(rax34, 1, -1, "%s (%s)", r12_6, rbp4);
        if (r10_27) {
            fun_25e0(r10_27, 1, r10_27, 1);
        }
    }
    r12_6 = rax34;
    goto addr_714d_29;
    addr_7304_7:
    rsi24 = rdi;
    al35 = mbsstr_trimmed_wordbounded(r12_6, rsi24, rdx3, 0x4c534e4152542f2f);
    if (al35) {
        r10_27 = v10;
        r14_19 = rbp4;
        goto addr_7145_33;
    } else {
        r14_19 = rbp4;
        rbp4 = v10;
        r10_27 = rbp4;
        goto addr_711f_30;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_8a93(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2600();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0x15220;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_8ad3(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x15220);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_8af3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x15220);
    }
    *rdi = esi;
    return 0x15220;
}

int64_t fun_8b13(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x15220);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s95 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_8b53(struct s95* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s95*>(0x15220);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s96 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s96* fun_8b73(struct s96* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s96*>(0x15220);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x2b4f;
    if (!rdx) 
        goto 0x2b4f;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x15220;
}

struct s97 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_8bb3(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s97* r8) {
    struct s97* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s97*>(0x15220);
    }
    rax7 = fun_2600();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x8be6);
    *rax7 = r15d8;
    return rax13;
}

struct s98 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_8c33(int64_t rdi, int64_t rsi, void*** rdx, struct s98* rcx) {
    struct s98* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s98*>(0x15220);
    }
    rax6 = fun_2600();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x8c61);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x8cbc);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_8d23() {
    __asm__("cli ");
}

void** g15098 = reinterpret_cast<void**>(32);

int64_t slotvec0 = 0x100;

void fun_8d33() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rsi7;
    void** rdi8;
    void** rsi9;
    void** rsi10;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_25e0(rdi6, rsi7);
        } while (rbx4 != rbp5);
    }
    rdi8 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi8 != 0x15120) {
        fun_25e0(rdi8, rsi9);
        g15098 = reinterpret_cast<void**>(0x15120);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0x15090) {
        fun_25e0(r12_2, rsi10);
        slotvec = reinterpret_cast<void**>(0x15090);
    }
    nslots = 1;
    return;
}

void fun_8dd3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8df3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8e03(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8e23(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_8e43(int32_t edi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s63* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2b55;
    rcx5 = reinterpret_cast<struct s63*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(edi, rdx, -1, rcx5, edi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2740();
    } else {
        return rax6;
    }
}

void** fun_8ed3(int32_t edi, int32_t esi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s63* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2b5a;
    rcx6 = reinterpret_cast<struct s63*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(edi, rdx, rcx, rcx6, edi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2740();
    } else {
        return rax7;
    }
}

void** fun_8f63(int32_t edi, int64_t rsi) {
    void** rax3;
    struct s63* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x2b5f;
    rcx4 = reinterpret_cast<struct s63*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2740();
    } else {
        return rax5;
    }
}

void** fun_8ff3(int32_t edi, int64_t rsi, int64_t rdx) {
    void** rax4;
    struct s63* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x2b64;
    rcx5 = reinterpret_cast<struct s63*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2740();
    } else {
        return rax6;
    }
}

void** fun_9083(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s63* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s63*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xc190]");
    __asm__("movdqa xmm1, [rip+0xc198]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0xc181]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2740();
    } else {
        return rax10;
    }
}

void** fun_9123(int64_t rdi, uint32_t esi) {
    struct s63* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s63*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xc0f0]");
    __asm__("movdqa xmm1, [rip+0xc0f8]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0xc0e1]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2740();
    } else {
        return rax9;
    }
}

void** fun_91c3(int64_t rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xc050]");
    __asm__("movdqa xmm1, [rip+0xc058]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0xc039]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2740();
    } else {
        return rax3;
    }
}

void** fun_9253(int64_t rdi, int64_t rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xbfc0]");
    __asm__("movdqa xmm1, [rip+0xbfc8]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0xbfb6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2740();
    } else {
        return rax4;
    }
}

void** fun_92e3(int32_t edi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s63* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2b69;
    rcx5 = reinterpret_cast<struct s63*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(edi, rdx, -1, rcx5, edi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2740();
    } else {
        return rax6;
    }
}

void** fun_9383(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s63* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xbe8a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0xbe82]");
    __asm__("movdqa xmm2, [rip+0xbe8a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2b6e;
    if (!rdx) 
        goto 0x2b6e;
    rcx6 = reinterpret_cast<struct s63*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(edi, rcx, -1, rcx6, edi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2740();
    } else {
        return rax7;
    }
}

void** fun_9423(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    void** rcx6;
    struct s63* rcx7;
    void** rax8;
    void* rdx9;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xbdea]");
    __asm__("movdqa xmm1, [rip+0xbdf2]");
    __asm__("movdqa xmm2, [rip+0xbdfa]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2b73;
    if (!rdx) 
        goto 0x2b73;
    rcx7 = reinterpret_cast<struct s63*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax8 = quotearg_n_options(edi, rcx, r8, rcx7, edi, rcx, r8, rcx7);
    rdx9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx9) {
        fun_2740();
    } else {
        return rax8;
    }
}

void** fun_94d3(int64_t rdi, int64_t rsi, int64_t rdx) {
    void** rdx4;
    struct s63* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xbd3a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0xbd32]");
    __asm__("movdqa xmm2, [rip+0xbd3a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2b78;
    if (!rsi) 
        goto 0x2b78;
    rcx5 = reinterpret_cast<struct s63*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2740();
    } else {
        return rax6;
    }
}

void** fun_9573(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s63* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xbc9a]");
    __asm__("movdqa xmm1, [rip+0xbca2]");
    __asm__("movdqa xmm2, [rip+0xbcaa]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2b7d;
    if (!rsi) 
        goto 0x2b7d;
    rcx6 = reinterpret_cast<struct s63*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2740();
    } else {
        return rax7;
    }
}

void fun_9613() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_9623(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_9643() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_9663(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s99 {
    int64_t f0;
    int64_t f8;
};

void fun_9683(struct s99* rdi) {
    __asm__("cli ");
    rdi->f0 = 0;
    rdi->f8 = 0;
    return;
}

struct s100 {
    signed char[8] pad8;
    unsigned char* f8;
    unsigned char* f10;
};

struct s101 {
    void* f0;
    void** f8;
};

uint32_t fun_25c0(struct s100* rdi, unsigned char* rsi);

void* fun_96a3(struct s100* rdi, unsigned char* rsi, int64_t rdx, struct s101* rcx) {
    unsigned char* r9_5;
    struct s101* r13_6;
    struct s100* rbp7;
    void* rsp8;
    void** rax9;
    void** v10;
    void* rdi11;
    uint64_t rcx12;
    uint64_t rax13;
    int64_t rax14;
    unsigned char* rax15;
    int64_t rbx16;
    uint32_t eax17;
    void* rax18;
    void** r15_19;
    void* r12_20;
    unsigned char* r14_21;
    void* v22;
    void* r8_23;
    signed char* rdx24;
    void** rax25;
    unsigned char* rax26;
    uint32_t eax27;
    void* rax28;

    __asm__("cli ");
    __asm__("pxor xmm0, xmm0");
    r9_5 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsi) + rdx);
    r13_6 = rcx;
    rbp7 = rdi;
    rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 72);
    rax9 = g28;
    v10 = rax9;
    rdi11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) + 16);
    __asm__("movaps [rsp+0x10], xmm0");
    __asm__("movaps [rsp+0x20], xmm0");
    if (rdx) {
        do {
            *reinterpret_cast<uint32_t*>(&rcx12) = *rsi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx12) + 4) = 0;
            ++rsi;
            rax13 = rcx12 >> 3;
            *reinterpret_cast<uint32_t*>(&rax14) = *reinterpret_cast<uint32_t*>(&rax13) & 24;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
            *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rdi11) + rax14) = *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rdi11) + rax14) | 1 << *reinterpret_cast<unsigned char*>(&rcx12);
        } while (r9_5 != rsi);
        rax15 = rbp7->f8;
        if (reinterpret_cast<uint64_t>(rax15) >= reinterpret_cast<uint64_t>(rbp7->f10)) 
            goto addr_9747_5;
        goto addr_9720_7;
    }
    do {
        rax15 = rbp7->f8;
        if (reinterpret_cast<uint64_t>(rax15) < reinterpret_cast<uint64_t>(rbp7->f10)) {
            addr_9720_7:
            rbp7->f8 = rax15 + 1;
            *reinterpret_cast<uint32_t*>(&rbx16) = *rax15;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
        } else {
            addr_9747_5:
            eax17 = fun_25c0(rbp7, rsi);
            rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
            *reinterpret_cast<uint32_t*>(&rbx16) = eax17;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
            if (reinterpret_cast<int32_t>(eax17) < reinterpret_cast<int32_t>(0)) 
                goto addr_9755_9;
        }
    } while (static_cast<int1_t>(*reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rsp8) + (reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbx16))) >> 6) * 8 + 16) >> rbx16));
    rax18 = r13_6->f0;
    r15_19 = r13_6->f8;
    *reinterpret_cast<int32_t*>(&r12_20) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_20) + 4) = 0;
    r14_21 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsp8) + 8);
    v22 = rax18;
    while (1) {
        if (r12_20 != rax18) {
            r8_23 = r12_20;
            rdx24 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_19) + reinterpret_cast<uint64_t>(r12_20));
            if (*reinterpret_cast<int32_t*>(&rbx16) < reinterpret_cast<int32_t>(0)) 
                break;
        } else {
            rsi = r14_21;
            rax25 = xpalloc();
            rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
            r8_23 = r12_20;
            r15_19 = rax25;
            rdx24 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_19) + reinterpret_cast<uint64_t>(r12_20));
            if (*reinterpret_cast<int32_t*>(&rbx16) < reinterpret_cast<int32_t>(0)) 
                goto addr_980d_15;
        }
        if (static_cast<int1_t>(*reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rsp8) + (reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbx16))) >> 6) * 8 + 16) >> rbx16)) 
            break;
        *rdx24 = *reinterpret_cast<signed char*>(&rbx16);
        r12_20 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_20) + 1);
        rax26 = rbp7->f8;
        if (reinterpret_cast<uint64_t>(rax26) >= reinterpret_cast<uint64_t>(rbp7->f10)) {
            eax27 = fun_25c0(rbp7, rsi);
            rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
            *reinterpret_cast<uint32_t*>(&rbx16) = eax27;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
        } else {
            rbp7->f8 = rax26 + 1;
            *reinterpret_cast<uint32_t*>(&rbx16) = *rax26;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
        }
        rax18 = v22;
    }
    addr_9810_21:
    *rdx24 = 0;
    r13_6->f8 = r15_19;
    r13_6->f0 = v22;
    addr_975c_22:
    rax28 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax28) {
        fun_2740();
    } else {
        return r8_23;
    }
    addr_980d_15:
    goto addr_9810_21;
    addr_9755_9:
    r8_23 = reinterpret_cast<void*>(0xffffffffffffffff);
    goto addr_975c_22;
}

struct s102 {
    signed char[1] pad1;
    void** f1;
};

void** xnmalloc(void** rdi, int64_t rsi);

void** xreallocarray(void** rdi, void** rsi, int64_t rdx, void** rcx, void** r8);

void** fun_9843(int64_t rdi, struct s102* rsi, void** rdx, int64_t rcx, void** r8, void*** r9) {
    int64_t v7;
    void** v8;
    int64_t v9;
    void** v10;
    void*** v11;
    void** rax12;
    void** v13;
    void** rax14;
    void** rdi15;
    void** v16;
    void** r12_17;
    void** rax18;
    void** rbp19;
    void** rax20;
    void** r13_21;
    void** v22;
    void** rcx23;
    void** rsi24;
    void** rax25;
    void** rax26;
    void** rax27;
    void* rax28;
    void*** r14_29;
    void*** rbx30;
    void** rdx31;
    void** rax32;
    void** rax33;
    void* rax34;

    __asm__("cli ");
    v7 = rdi;
    v8 = rdx;
    v9 = rcx;
    v10 = r8;
    v11 = r9;
    rax12 = g28;
    v13 = rax12;
    if (!rsi) {
        *reinterpret_cast<int32_t*>(&rax14) = 64;
        *reinterpret_cast<int32_t*>(&rax14 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdi15) = 64;
        *reinterpret_cast<int32_t*>(&rdi15 + 4) = 0;
    } else {
        rdi15 = reinterpret_cast<void**>(&rsi->f1);
        rax14 = rdi15;
    }
    v16 = rax14;
    *reinterpret_cast<int32_t*>(&r12_17) = 0;
    *reinterpret_cast<int32_t*>(&r12_17 + 4) = 0;
    rax18 = xnmalloc(rdi15, 8);
    rbp19 = rax18;
    rax20 = xnmalloc(v16, 8);
    r13_21 = rax20;
    v22 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x78 - 8 + 8 - 8 + 8 + 80);
    while (1) {
        rcx23 = v22;
        rsi24 = v8;
        rax25 = readtoken(v7, rsi24, v9, rcx23, v7, rsi24, v9, rcx23);
        if (reinterpret_cast<signed char>(v16) <= reinterpret_cast<signed char>(r12_17)) {
            *reinterpret_cast<int32_t*>(&r8) = 8;
            *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
            rcx23 = reinterpret_cast<void**>(0xffffffffffffffff);
            rax26 = xpalloc();
            rsi24 = v16;
            rbp19 = rax26;
            rax27 = xreallocarray(r13_21, rsi24, 8, 0xffffffffffffffff, 8);
            r13_21 = rax27;
        }
        rax28 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_17) * 8);
        r14_29 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r13_21) + reinterpret_cast<uint64_t>(rax28));
        rbx30 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rbp19) + reinterpret_cast<uint64_t>(rax28));
        if (rax25 == 0xffffffffffffffff) 
            break;
        rdx31 = rax25 + 1;
        ++r12_17;
        rax32 = xnmalloc(rdx31, 1);
        *r14_29 = rax25;
        rax33 = fun_28d0(rax32, 0, rdx31, rcx23, r8);
        *rbx30 = rax33;
    }
    *rbx30 = reinterpret_cast<void**>(0);
    *r14_29 = reinterpret_cast<void**>(0);
    fun_25e0(0, rsi24, 0, rsi24);
    *reinterpret_cast<void***>(v10) = rbp19;
    if (!v11) {
        fun_25e0(r13_21, rsi24, r13_21, rsi24);
    } else {
        *v11 = r13_21;
    }
    rax34 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v13) - reinterpret_cast<unsigned char>(g28));
    if (rax34) {
        fun_2740();
    } else {
        return r12_17;
    }
}

int64_t fun_26b0(int64_t rdi, int64_t rsi, uint64_t rdx);

int64_t fun_9a13(int32_t edi, int64_t rsi, uint64_t rdx) {
    int32_t r13d4;
    int64_t rbp5;
    uint64_t rbx6;
    int64_t rdi7;
    int64_t rax8;
    int32_t* rax9;

    __asm__("cli ");
    r13d4 = edi;
    rbp5 = rsi;
    rbx6 = rdx;
    while (*reinterpret_cast<int32_t*>(&rdi7) = r13d4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0, rax8 = fun_26b0(rdi7, rbp5, rbx6), rax8 < 0) {
        rax9 = fun_2600();
        if (*rax9 == 4) 
            continue;
        if (*rax9 != 22) 
            break;
        if (rbx6 <= 0x7ff00000) 
            break;
        *reinterpret_cast<int32_t*>(&rbx6) = 0x7ff00000;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
    }
    return rax8;
}

int64_t fun_2850();

int64_t fun_9a83(int64_t rdi, int64_t rsi) {
    int64_t rax3;
    int64_t rax4;

    __asm__("cli ");
    rax3 = fun_2850();
    rax4 = rsi;
    if (rax3) {
        rax4 = rax3 - rdi + 1;
    }
    return rax4;
}

void** fun_2ae0(void** rdi, void** rsi);

void xalloc_die();

void fun_2a10(void** rdi, void** rsi, void** rdx, void** rcx);

int32_t fun_29e0(int64_t rdi, void** rsi, ...);

void** fun_9af3(void** rdi, void** rsi) {
    int32_t ebx3;
    void** rax4;
    void** v5;
    void** rax6;
    void** r12_7;
    uint64_t rax8;
    void* rsp9;
    int64_t rbp10;
    void** r13_11;
    unsigned char** rax12;
    unsigned char* rax13;
    void** rax14;
    void** rcx15;
    void** rax16;
    void** rbp17;
    void* rax18;
    unsigned char** rax19;
    unsigned char* rdx20;
    int64_t rax21;
    void** v22;
    void** r15_23;
    void** rax24;
    signed char v25;
    void** rbp26;
    void** v27;
    void** r14_28;
    void** rcx29;
    uint32_t eax30;
    int64_t rax31;
    uint32_t eax32;
    uint32_t eax33;
    void** v34;
    int32_t eax35;
    int32_t v36;
    void** rdi37;
    void** rax38;
    void** rdx39;
    uint32_t eax40;
    int64_t rdi41;
    int32_t eax42;
    void** v43;
    void** rbp44;
    int32_t r13d45;
    void** rax46;
    signed char v47;
    void** r15_48;
    void** v49;
    void** v50;
    void** r8_51;
    void** r9_52;
    void** r8_53;
    void** r9_54;
    void** r8_55;
    void** r9_56;
    void** rax57;
    void** r14_58;
    void** rax59;
    void** v60;
    signed char v61;
    void** rbp62;
    void** r14_63;
    uint32_t eax64;
    uint32_t ecx65;
    uint32_t eax66;
    int64_t rax67;
    uint32_t eax68;
    uint32_t eax69;
    int32_t eax70;
    int64_t rdi71;
    int32_t eax72;
    int64_t rdi73;
    int32_t eax74;
    int1_t zf75;
    void** rax76;
    int64_t rdi77;
    int32_t eax78;

    __asm__("cli ");
    ebx3 = *reinterpret_cast<int32_t*>(&rsi);
    rax4 = g28;
    v5 = rax4;
    rax6 = fun_2ae0(rdi, rsi);
    if (!rax6) {
        addr_a027_2:
        xalloc_die();
    } else {
        r12_7 = rax6;
        rax8 = fun_2710();
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x68 - 8 + 8 - 8 + 8);
        if (rax8 <= 1) {
            if (ebx3) {
                *reinterpret_cast<uint32_t*>(&rbp10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_7));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp10) + 4) = 0;
                if (!*reinterpret_cast<signed char*>(&rbp10)) {
                    r13_11 = r12_7;
                } else {
                    rax12 = fun_2b10(rdi, rsi);
                    r13_11 = r12_7;
                    rax13 = *rax12;
                    do {
                        if (!((rax13 + rbp10 * 2)[1] & 32)) 
                            break;
                        *reinterpret_cast<uint32_t*>(&rbp10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_11 + 1));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp10) + 4) = 0;
                        ++r13_11;
                    } while (*reinterpret_cast<signed char*>(&rbp10));
                }
                rax14 = fun_2720(r13_11);
                rsi = r13_11;
                fun_2a10(r12_7, rsi, rax14 + 1, rcx15);
                if (ebx3 == 1) 
                    goto addr_9d28_11;
            }
            rax16 = fun_2720(r12_7, r12_7);
            rbp17 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_7) + reinterpret_cast<unsigned char>(rax16) + 0xffffffffffffffff);
            if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(rbp17)) {
                addr_9d28_11:
                rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v5) - reinterpret_cast<unsigned char>(g28));
                if (rax18) {
                    fun_2740();
                    goto addr_a027_2;
                } else {
                    return r12_7;
                }
            } else {
                rax19 = fun_2b10(r12_7, rsi, r12_7, rsi);
                rdx20 = *rax19;
                do {
                    *reinterpret_cast<uint32_t*>(&rax21) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax21) + 4) = 0;
                    if (!((rdx20 + rax21 * 2)[1] & 32)) 
                        goto addr_9e24_17;
                    *reinterpret_cast<void***>(rbp17) = reinterpret_cast<void**>(0);
                    --rbp17;
                } while (reinterpret_cast<unsigned char>(r12_7) <= reinterpret_cast<unsigned char>(rbp17));
                goto addr_9d28_11;
            }
        } else {
            if (ebx3) {
                v22 = r12_7;
                r15_23 = r12_7;
                rax24 = fun_2720(r12_7);
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                v25 = 0;
                rbp26 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_7) + reinterpret_cast<unsigned char>(rax24));
                v27 = rbp26;
                if (reinterpret_cast<unsigned char>(rbp26) > reinterpret_cast<unsigned char>(r12_7)) {
                    do {
                        r14_28 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp9) + 28);
                        if (!v25) {
                            *reinterpret_cast<uint32_t*>(&rcx29) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_23));
                            *reinterpret_cast<int32_t*>(&rcx29 + 4) = 0;
                            eax30 = *reinterpret_cast<uint32_t*>(&rcx29);
                            *reinterpret_cast<unsigned char*>(&eax30) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax30) >> 5);
                            *reinterpret_cast<uint32_t*>(&rax31) = eax30 & 7;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax31) + 4) = 0;
                            eax32 = *reinterpret_cast<uint32_t*>(0x10e40 + rax31 * 4) >> *reinterpret_cast<signed char*>(&rcx29);
                            if (!(*reinterpret_cast<unsigned char*>(&eax32) & 1)) {
                                r14_28 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp9) + 28);
                                eax33 = fun_2af0(r14_28, rsi);
                                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                                if (!eax33) 
                                    goto addr_a050_24;
                                v25 = 1;
                                goto addr_9f55_26;
                            } else {
                                v34 = reinterpret_cast<void**>(1);
                                eax35 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(r15_23));
                                r15_23 = v22;
                                v36 = eax35;
                                continue;
                            }
                        } else {
                            addr_9f55_26:
                            rdi37 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp9) + 60);
                            rcx29 = r14_28;
                            rsi = r15_23;
                            rax38 = rpl_mbrtowc(rdi37, rsi, reinterpret_cast<unsigned char>(rbp26) - reinterpret_cast<unsigned char>(r15_23), rcx29);
                            rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                            v34 = rax38;
                            if (rax38 == 0xffffffffffffffff) 
                                goto addr_9cf0_28;
                        }
                        r15_23 = v22;
                        if (rax38 == 0xfffffffffffffffe) 
                            goto addr_9ff0_30;
                        if (!rax38) {
                            v34 = reinterpret_cast<void**>(1);
                            if (*reinterpret_cast<void***>(r15_23)) 
                                break;
                            *reinterpret_cast<int32_t*>(&rdx39) = v36;
                            *reinterpret_cast<int32_t*>(&rdx39 + 4) = 0;
                            if (*reinterpret_cast<int32_t*>(&rdx39)) 
                                goto addr_a04b_34;
                        }
                        eax40 = fun_2af0(r14_28, rsi, r14_28, rsi);
                        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                        if (eax40) {
                            v25 = 0;
                        }
                        *reinterpret_cast<int32_t*>(&rdi41) = v36;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi41) + 4) = 0;
                        eax42 = fun_29e0(rdi41, rsi);
                        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                    } while (eax42 && (rbp26 = v27, r15_23 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_23) + reinterpret_cast<unsigned char>(v34)), v22 = r15_23, reinterpret_cast<unsigned char>(r15_23) < reinterpret_cast<unsigned char>(rbp26)));
                    goto addr_9d08_39;
                } else {
                    goto addr_9d08_39;
                }
            } else {
                addr_9b3c_41:
                v43 = r12_7;
                rbp44 = r12_7;
                r13d45 = 0;
                rax46 = fun_2720(r12_7, r12_7);
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                v47 = 0;
                r15_48 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_7) + reinterpret_cast<unsigned char>(rax46));
                v49 = r15_48;
                v50 = reinterpret_cast<void**>(0);
                if (reinterpret_cast<unsigned char>(r15_48) <= reinterpret_cast<unsigned char>(r12_7)) {
                    goto addr_9d28_11;
                }
            }
        }
    }
    addr_a02c_43:
    rcx29 = reinterpret_cast<void**>("mbiter_multi_next");
    *reinterpret_cast<int32_t*>(&rdx39) = 0xa2;
    *reinterpret_cast<int32_t*>(&rdx39 + 4) = 0;
    rsi = reinterpret_cast<void**>("lib/mbiter.h");
    rdi37 = reinterpret_cast<void**>("*iter->cur.ptr == '\\0'");
    fun_2810("*iter->cur.ptr == '\\0'", "lib/mbiter.h", 0xa2, "mbiter_multi_next", r8_51, r9_52);
    addr_a04b_34:
    mbiter_multi_next_part_0(rdi37, rsi, rdx39, rcx29, r8_53, r9_54);
    addr_a050_24:
    fun_2810("mbsinit (&iter->state)", "lib/mbiter.h", 0x87, "mbiter_multi_next", r8_55, r9_56);
    addr_9e24_17:
    goto addr_9d28_11;
    addr_9cf0_28:
    r15_23 = v22;
    addr_9d08_39:
    rax57 = fun_2720(r15_23, r15_23);
    rsi = r15_23;
    fun_2a10(r12_7, rsi, rax57 + 1, rcx29);
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 + 8);
    if (ebx3 != 1) 
        goto addr_9b3c_41; else 
        goto addr_9d28_11;
    addr_9ff0_30:
    goto addr_9d08_39;
    do {
        r14_58 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp9) + 28);
        if (v47) {
            addr_9c6a_45:
            rdi37 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp9) + 60);
            rcx29 = r14_58;
            rsi = rbp44;
            rdx39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_48) - reinterpret_cast<unsigned char>(rbp44));
            rax59 = rpl_mbrtowc(rdi37, rsi, rdx39, rcx29);
            rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
            v60 = rax59;
            if (rax59 == 0xffffffffffffffff) {
                v61 = 0;
                rbp62 = v43;
                *reinterpret_cast<int32_t*>(&r14_63) = 1;
                *reinterpret_cast<int32_t*>(&r14_63 + 4) = 0;
                r15_48 = v49;
            } else {
                rbp62 = v43;
                if (rax59 == 0xfffffffffffffffe) {
                    r15_48 = v49;
                    v61 = 0;
                    r14_63 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_48) - reinterpret_cast<unsigned char>(rbp62));
                } else {
                    if (!rax59) {
                        v60 = reinterpret_cast<void**>(1);
                        if (*reinterpret_cast<void***>(rbp62)) 
                            goto addr_a02c_43;
                        if (v36) 
                            goto addr_a04b_34;
                    }
                    v61 = 1;
                    eax64 = fun_2af0(r14_58, rsi, r14_58, rsi);
                    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                    r15_48 = v49;
                    if (eax64) {
                        v47 = 0;
                    }
                    r14_63 = v60;
                }
            }
        } else {
            ecx65 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp44));
            eax66 = ecx65;
            *reinterpret_cast<unsigned char*>(&eax66) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax66) >> 5);
            *reinterpret_cast<uint32_t*>(&rax67) = eax66 & 7;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax67) + 4) = 0;
            eax68 = *reinterpret_cast<uint32_t*>(0x10e40 + rax67 * 4) >> *reinterpret_cast<signed char*>(&ecx65);
            if (!(*reinterpret_cast<unsigned char*>(&eax68) & 1)) {
                r14_58 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp9) + 28);
                eax69 = fun_2af0(r14_58, rsi, r14_58, rsi);
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                if (!eax69) 
                    goto addr_a050_24;
                v47 = 1;
                goto addr_9c6a_45;
            } else {
                eax70 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp44));
                *reinterpret_cast<int32_t*>(&r14_63) = 1;
                *reinterpret_cast<int32_t*>(&r14_63 + 4) = 0;
                rbp62 = v43;
                r15_48 = v49;
                v61 = 1;
                v36 = eax70;
            }
        }
        if (!r13d45) {
            r13d45 = 1;
            if (v61) {
                *reinterpret_cast<int32_t*>(&rdi71) = v36;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi71) + 4) = 0;
                r13d45 = 0;
                eax72 = fun_29e0(rdi71, rsi, rdi71, rsi);
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                *reinterpret_cast<unsigned char*>(&r13d45) = reinterpret_cast<uint1_t>(eax72 == 0);
            }
        } else {
            if (r13d45 == 1) {
                if (v61) {
                    *reinterpret_cast<int32_t*>(&rdi73) = v36;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi73) + 4) = 0;
                    eax74 = fun_29e0(rdi73, rsi, rdi73, rsi);
                    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                    zf75 = eax74 == 0;
                    rax76 = v50;
                    if (!zf75) {
                        rax76 = rbp62;
                    }
                    v50 = rax76;
                    if (!zf75) {
                        r13d45 = 2;
                    }
                }
            } else {
                if (r13d45 != 2 || !v61) {
                    r13d45 = 1;
                } else {
                    *reinterpret_cast<int32_t*>(&rdi77) = v36;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi77) + 4) = 0;
                    eax78 = fun_29e0(rdi77, rsi, rdi77, rsi);
                    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                    if (!eax78) {
                        r13d45 = 1;
                    }
                }
            }
        }
        rbp44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp62) + reinterpret_cast<unsigned char>(r14_63));
        v43 = rbp44;
    } while (reinterpret_cast<unsigned char>(rbp44) < reinterpret_cast<unsigned char>(r15_48));
    if (r13d45 == 2) {
        *reinterpret_cast<void***>(v50) = reinterpret_cast<void**>(0);
        goto addr_9d28_11;
    }
}

struct s103 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
    signed char[7] pad40;
    void** f28;
    signed char[7] pad48;
    void** f30;
    signed char[7] pad56;
    void** f38;
    signed char[7] pad64;
    void** f40;
};

void fun_28a0(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_a073(void** rdi, void** rsi, void** rdx, void** rcx, struct s103* r8, void** r9) {
    void** r12_7;
    void** v8;
    void** v9;
    void** v10;
    void** v11;
    void** v12;
    void** v13;
    void** v14;
    void** v15;
    void** v16;
    void** v17;
    void** v18;
    void** v19;
    void** rax20;
    void** v21;
    void** v22;
    void** v23;
    void** v24;
    void** v25;
    void** v26;
    void** rax27;
    void** v28;
    void** v29;
    void** v30;
    void** v31;
    void** v32;
    void** v33;
    void** r10_34;
    void** r9_35;
    void** r8_36;
    void** rcx37;
    void** r15_38;
    void** v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2a90(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2a90(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2700();
    fun_2a90(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_28a0(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2700();
    fun_2a90(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_28a0(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2700();
        fun_2a90(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x10db0 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x10db0;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_a4e3() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s104 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_a503(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s104* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s104* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2740();
    } else {
        return;
    }
}

void fun_a5a3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_a646_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_a650_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2740();
    } else {
        return;
    }
    addr_a646_5:
    goto addr_a650_7;
}

void fun_a683() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_28a0(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2700();
    fun_29b0(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2700();
    fun_29b0(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2700();
    goto fun_29b0;
}

int64_t fun_26a0();

void fun_a723(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_26a0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void** fun_2920(void** rdi);

void fun_a763(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_2920(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a783(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_2920(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a7a3(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_2920(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void** fun_2990(void** rdi, void** rsi);

void fun_a7c3(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2990(rdi, rsi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_a7f3(void** rdi, uint64_t rsi) {
    uint64_t rax3;
    void** rax4;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rsi == 0);
    rax4 = fun_2990(rdi, rsi | rax3);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a823(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_26a0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_a863() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_26a0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a8a3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_26a0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a8d3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_26a0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a923(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_26a0();
            if (rax5) 
                break;
            addr_a96d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_a96d_5;
        rax8 = fun_26a0();
        if (rax8) 
            goto addr_a956_9;
        if (rbx4) 
            goto addr_a96d_5;
        addr_a956_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_a9b3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_26a0();
            if (rax8) 
                break;
            addr_a9fa_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_a9fa_5;
        rax11 = fun_26a0();
        if (rax11) 
            goto addr_a9e2_9;
        if (!rbx6) 
            goto addr_a9e2_9;
        if (r12_4) 
            goto addr_a9fa_5;
        addr_a9e2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_aa43(void** rdi, void*** rsi, void** rdx, void** rcx, uint64_t r8) {
    void** r13_6;
    void** rdi7;
    void*** r12_8;
    void** rsi9;
    void** rcx10;
    void** rbx11;
    void** rax12;
    void** rbp13;
    void* rbp14;
    void** rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx10) >> 1) + reinterpret_cast<unsigned char>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void**>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<signed char>(rbx11) <= reinterpret_cast<signed char>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_aaed_9:
            rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_ab00_10:
                *r12_8 = reinterpret_cast<void**>(0);
            }
            addr_aaa0_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_aac6_12;
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_ab14_14;
            if (reinterpret_cast<signed char>(rcx10) <= reinterpret_cast<signed char>(rsi9)) 
                goto addr_aabd_16;
            if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) 
                goto addr_ab14_14;
            addr_aabd_16:
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_ab14_14;
            addr_aac6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2990(rdi7, rsi9);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_ab14_14;
            if (!rbp13) 
                break;
            addr_ab14_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp13) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_aaed_9;
        } else {
            if (!r13_6) 
                goto addr_ab00_10;
            goto addr_aaa0_11;
        }
    }
}

int64_t fun_2880();

void fun_ab43() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2880();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_ab73() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2880();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_aba3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2880();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_abc3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2880();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_abe3(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2920(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_28d0;
    }
}

void fun_ac23(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2920(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_28d0;
    }
}

struct s105 {
    signed char[1] pad1;
    void** f1;
};

void fun_ac63(int64_t rdi, struct s105* rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2920(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<uint64_t>(rsi)) = 0;
        goto fun_28d0;
    }
}

void fun_aca3(void** rdi) {
    void** rax2;
    void** rax3;

    __asm__("cli ");
    rax2 = fun_2720(rdi);
    rax3 = fun_2920(rax2 + 1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_28d0;
    }
}

void fun_ace3() {
    __asm__("cli ");
    fun_2700();
    fun_2a20();
    fun_25f0();
}

int32_t mem_cd_iconv();

int64_t fun_ad23() {
    int32_t eax1;
    int32_t* rax2;
    int64_t rax3;

    __asm__("cli ");
    eax1 = mem_cd_iconv();
    if (eax1 >= 0 || (rax2 = fun_2600(), *rax2 != 12)) {
        *reinterpret_cast<int32_t*>(&rax3) = eax1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        return rax3;
    } else {
        xalloc_die();
    }
}

void** str_cd_iconv(void** rdi, void** rsi);

void** fun_ad53(void** rdi, void** rsi) {
    void** rax3;
    int32_t* rax4;

    __asm__("cli ");
    rax3 = str_cd_iconv(rdi, rsi);
    if (rax3 || (rax4 = fun_2600(), *rax4 != 12)) {
        return rax3;
    } else {
        xalloc_die();
    }
}

int64_t str_iconv();

int64_t fun_ad83() {
    int64_t rax1;
    int32_t* rax2;

    __asm__("cli ");
    rax1 = str_iconv();
    if (rax1 || (rax2 = fun_2600(), *rax2 != 12)) {
        return rax1;
    } else {
        xalloc_die();
    }
}

int64_t fun_adb3(int64_t rdi, int64_t rsi) {
    int64_t rdx3;
    int64_t rax4;
    int64_t r9_5;
    uint32_t r8d6;
    uint32_t ecx7;
    uint32_t ebx8;
    uint32_t r10d9;
    int64_t rax10;

    __asm__("cli ");
    if (rdi == rsi) {
        return 0;
    } else {
        *reinterpret_cast<int32_t*>(&rdx3) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
        while (1) {
            *reinterpret_cast<uint32_t*>(&rax4) = *reinterpret_cast<unsigned char*>(rdi + rdx3);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r9_5) = *reinterpret_cast<unsigned char*>(rsi + rdx3);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_5) + 4) = 0;
            r8d6 = *reinterpret_cast<uint32_t*>(&rax4);
            ecx7 = *reinterpret_cast<uint32_t*>(&r9_5);
            ebx8 = *reinterpret_cast<uint32_t*>(&r9_5);
            r10d9 = static_cast<uint32_t>(r9_5 - 65);
            if (static_cast<uint32_t>(rax4 - 65) <= 25) {
                *reinterpret_cast<uint32_t*>(&rax4) = *reinterpret_cast<uint32_t*>(&rax4) + 32;
                r8d6 = r8d6 + 32;
                if (r10d9 > 25) {
                    addr_add8_6:
                    ++rdx3;
                    if (*reinterpret_cast<signed char*>(&r8d6) != *reinterpret_cast<signed char*>(&ecx7)) 
                        break;
                } else {
                    addr_adcd_7:
                    ebx8 = static_cast<uint32_t>(r9_5 + 32);
                    ecx7 = ecx7 + 32;
                    if (!*reinterpret_cast<uint32_t*>(&rax4)) 
                        break; else 
                        goto addr_add8_6;
                }
            } else {
                if (r10d9 <= 25) 
                    goto addr_adcd_7;
                if (*reinterpret_cast<uint32_t*>(&rax4)) 
                    goto addr_add8_6; else 
                    break;
            }
        }
        *reinterpret_cast<uint32_t*>(&rax10) = *reinterpret_cast<uint32_t*>(&rax4) - ebx8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        return rax10;
    }
}

int64_t fun_2630();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_ae23(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    int32_t* rax5;
    int32_t* rax6;

    __asm__("cli ");
    rax2 = fun_2630();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_ae7e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2600();
            *rax5 = 0;
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_ae7e_3;
            rax6 = fun_2600();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax6 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s106 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_28e0(struct s106* rdi);

int32_t fun_2970(struct s106* rdi);

int64_t fun_27f0(int64_t rdi, ...);

int32_t rpl_fflush(struct s106* rdi);

int64_t fun_26e0(struct s106* rdi);

int64_t fun_ae93(struct s106* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_28e0(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2970(rdi);
        if (!(eax3 && (eax4 = fun_28e0(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_27f0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2600();
            r12d9 = *rax8;
            rax10 = fun_26e0(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_26e0;
}

void rpl_fseeko(struct s106* rdi);

void fun_af23(struct s106* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2970(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_af73(struct s106* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_28e0(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_27f0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_2950(int64_t rdi);

signed char* fun_aff3() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2950(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

int64_t fun_2900();

int32_t fun_2690(int64_t rdi);

int64_t fun_b033(int32_t edi) {
    int64_t rax2;
    int64_t rdi3;
    int32_t eax4;
    int64_t rax5;

    __asm__("cli ");
    rax2 = fun_2900();
    if (*reinterpret_cast<int32_t*>(&rax2) < 0) {
        *reinterpret_cast<int32_t*>(&rdi3) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi3) + 4) = 0;
        eax4 = fun_2690(rdi3);
        *reinterpret_cast<uint32_t*>(&rax5) = reinterpret_cast<uint1_t>(eax4 == 0);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        return rax5;
    } else {
        return rax2;
    }
}

struct s107 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    signed char f10;
    signed char[3] pad20;
    int32_t f14;
    void** f18;
};

struct s108 {
    signed char[8] pad8;
    void** f8;
    signed char[7] pad16;
    unsigned char f10;
    signed char[3] pad20;
    int32_t f14;
    void** f18;
};

void fun_b063(struct s107* rdi, struct s108* rsi) {
    void** rsi3;
    void** rcx4;
    void** rcx5;
    void** rdx6;
    void** r8_7;
    void** rax8;
    void** rax9;
    uint32_t eax10;

    __asm__("cli ");
    rsi3 = reinterpret_cast<void**>(&rsi->f18);
    rcx4 = *reinterpret_cast<void***>(rsi3 + 0xffffffffffffffe8);
    if (rcx4 == rsi3) {
        rcx5 = reinterpret_cast<void**>(&rdi->f18);
        rdx6 = rsi->f8;
        rax8 = fun_28d0(rcx5, rsi3, rdx6, rcx5, r8_7);
        rcx4 = rax8;
    }
    rax9 = rsi->f8;
    rdi->f0 = rcx4;
    rdi->f8 = rax9;
    eax10 = rsi->f10;
    rdi->f10 = *reinterpret_cast<signed char*>(&eax10);
    if (*reinterpret_cast<signed char*>(&eax10)) {
        rdi->f14 = rsi->f14;
    }
    return;
}

int64_t fun_b0c3(uint32_t edi) {
    uint32_t eax2;
    uint32_t ecx3;
    int64_t rax4;
    int64_t rax5;

    __asm__("cli ");
    eax2 = edi;
    ecx3 = edi;
    *reinterpret_cast<unsigned char*>(&eax2) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax2) >> 5);
    *reinterpret_cast<uint32_t*>(&rax4) = eax2 & 7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax5) = *reinterpret_cast<uint32_t*>(0x10e40 + rax4 * 4) >> *reinterpret_cast<signed char*>(&ecx3) & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    return rax5;
}

uint64_t fun_2770(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_b0e3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2770(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2740();
    } else {
        return r12_7;
    }
}

void* fun_2830(void** rdi);

void** fun_b983(void** rdi, void** rsi) {
    void** v3;
    void** v4;
    void** rax5;
    void** v6;
    uint64_t rax7;
    void*** rsp8;
    void** v9;
    signed char v10;
    int32_t v11;
    unsigned char v12;
    void*** rbx13;
    void** r12_14;
    void** v15;
    void** v16;
    void** v17;
    void** v18;
    void*** v19;
    void*** v20;
    void** rax21;
    uint32_t r12d22;
    void* rdx23;
    uint32_t eax24;
    void** rbp25;
    void** rdi26;
    void*** r13_27;
    void** rdx28;
    void** r9_29;
    uint32_t esi30;
    void*** r14_31;
    void** rcx32;
    void** rbx33;
    void** r15_34;
    void** r8_35;
    void* r10_36;
    uint32_t edx37;
    uint32_t edx38;
    void* rax39;
    void** rax40;
    uint32_t eax41;
    void** v42;
    signed char v43;
    void** rdx44;
    void** v45;
    void** rcx46;
    void** v47;
    uint32_t eax48;
    signed char v49;
    unsigned char al50;
    int32_t v51;
    unsigned char al52;
    signed char v53;
    int32_t v54;
    int32_t v55;
    void** v56;
    void* v57;
    signed char v58;
    int32_t v59;
    void** rbp60;
    void* v61;
    signed char v62;
    int32_t v63;
    signed char v64;
    int32_t v65;
    signed char v66;
    void** r15_67;
    void** v68;
    void** r14_69;
    void** v70;
    int32_t v71;
    void** v72;
    void** v73;
    void** rbp74;
    void** r13_75;
    uint32_t eax76;
    void** v77;
    void* r14_78;
    uint32_t eax79;
    unsigned char v80;
    int32_t v81;
    unsigned char al82;
    uint32_t edx83;
    unsigned char v84;
    unsigned char v85;
    signed char v86;
    int32_t v87;

    __asm__("cli ");
    v3 = rdi;
    v4 = rsi;
    rax5 = g28;
    v6 = rax5;
    rax7 = fun_2710();
    rsp8 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1b8 - 8 + 8);
    if (rax7 > 1) {
        v9 = rsi;
        mbuiter_multi_next_part_0(rsp8 + 96, rsi);
        rsp8 = rsp8 - 8 + 8;
        if (!v10 || v11) {
            v12 = 1;
            *reinterpret_cast<int32_t*>(&rbx13) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx13) + 4) = 0;
            r12_14 = reinterpret_cast<void**>(rsp8 + 0x160);
            v15 = v3;
            v16 = reinterpret_cast<void**>(rsp8 + 0xe0);
            v17 = reinterpret_cast<void**>(rsp8 + 0x120);
            v18 = reinterpret_cast<void**>(0);
            v19 = reinterpret_cast<void***>(0);
            v20 = rsp8 + 88;
            goto addr_bc83_4;
        } else {
            rax21 = v3;
            goto addr_baf8_6;
        }
    }
    r12d22 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v4));
    rax21 = v3;
    if (!*reinterpret_cast<unsigned char*>(&r12d22)) {
        addr_baf8_6:
        rdx23 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v6) - reinterpret_cast<unsigned char>(g28));
        if (rdx23) {
            fun_2740();
        } else {
            return rax21;
        }
    } else {
        eax24 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v3));
        if (!*reinterpret_cast<unsigned char*>(&eax24)) {
            addr_baf6_11:
            *reinterpret_cast<int32_t*>(&rax21) = 0;
            *reinterpret_cast<int32_t*>(&rax21 + 4) = 0;
            goto addr_baf8_6;
        } else {
            rbp25 = v4;
            rdi26 = v3;
            *reinterpret_cast<int32_t*>(&r13_27) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx28) = 0;
            *reinterpret_cast<int32_t*>(&rdx28 + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_29) = 0;
            *reinterpret_cast<int32_t*>(&r9_29 + 4) = 0;
            esi30 = 1;
            r14_31 = rsp8 + 88;
            rcx32 = rbp25;
            while (1) {
                ++r13_27;
                rbx33 = rdx28 + 1;
                r15_34 = rdi26 + 1;
                if (*reinterpret_cast<unsigned char*>(&r12d22) == *reinterpret_cast<unsigned char*>(&eax24)) {
                    eax24 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp25 + 1));
                    if (!*reinterpret_cast<unsigned char*>(&eax24)) 
                        break;
                    r8_35 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi26) - reinterpret_cast<unsigned char>(rdx28));
                    r10_36 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rbp25) - reinterpret_cast<unsigned char>(rdx28));
                    do {
                        edx37 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r8_35) + reinterpret_cast<unsigned char>(rbx33));
                        if (!*reinterpret_cast<unsigned char*>(&edx37)) 
                            goto addr_baf6_11;
                        ++rbx33;
                        if (*reinterpret_cast<unsigned char*>(&edx37) != *reinterpret_cast<unsigned char*>(&eax24)) 
                            break;
                        eax24 = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(r10_36) + reinterpret_cast<unsigned char>(rbx33));
                    } while (*reinterpret_cast<unsigned char*>(&eax24));
                    goto addr_bfe0_19;
                }
                if (!*reinterpret_cast<void***>(r15_34)) 
                    goto addr_baf6_11;
                *reinterpret_cast<unsigned char*>(&eax24) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(r13_27) > 9)) & *reinterpret_cast<unsigned char*>(&esi30));
                edx38 = eax24;
                if (*reinterpret_cast<unsigned char*>(&eax24)) {
                    if (reinterpret_cast<unsigned char>(rbx33) < reinterpret_cast<unsigned char>(r13_27 + reinterpret_cast<uint64_t>(r13_27) * 4) || rcx32 && (rax39 = fun_2830(rcx32), edx38 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&edx38)), r9_29 = rbx33, rcx32 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx32) + reinterpret_cast<uint64_t>(rax39)), !!*reinterpret_cast<void***>(rcx32))) {
                        esi30 = edx38;
                    } else {
                        rax40 = fun_2720(rbp25);
                        eax41 = knuth_morris_pratt(r15_34, rbp25, rax40, r14_31, r8_35, r9_29);
                        r9_29 = r9_29;
                        esi30 = eax41;
                        if (*reinterpret_cast<signed char*>(&eax41)) 
                            goto addr_c02c_25;
                        *reinterpret_cast<int32_t*>(&rcx32) = 0;
                        *reinterpret_cast<int32_t*>(&rcx32 + 4) = 0;
                    }
                }
                eax24 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_34));
                rdi26 = r15_34;
                rdx28 = rbx33;
            }
        }
    }
    rax21 = rdi26;
    goto addr_baf8_6;
    addr_bfe0_19:
    rax21 = rdi26;
    goto addr_baf8_6;
    addr_c02c_25:
    rax21 = v42;
    goto addr_baf8_6;
    addr_bfd0_29:
    rax21 = v15;
    goto addr_baf8_6;
    while (1) {
        addr_bf4d_30:
        mbuiter_multi_next_part_0(rsp8 + 0xa0, rsi);
        rsp8 = rsp8 - 8 + 8;
        if (!v43) 
            goto addr_bf2f_31;
        goto addr_bf20_33;
        while (1) {
            addr_bfc4_34:
            while (1) {
                addr_bcd6_35:
                rdx44 = v45;
                rcx46 = v15;
                if (rdx44 != v47) 
                    goto addr_bc78_36;
                rsi = v9;
                eax48 = fun_2860(rcx46, rsi, rdx44);
                rsp8 = rsp8 - 8 + 8;
                rcx46 = rcx46;
                rdx44 = rdx44;
                if (!eax48) 
                    goto addr_bd16_38;
                while (1) {
                    addr_bc78_36:
                    v15 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx46) + reinterpret_cast<unsigned char>(rdx44));
                    addr_bc83_4:
                    mbuiter_multi_next_part_0(v16, rsi);
                    rsp8 = rsp8 - 8 + 8;
                    if (!v49) {
                        al50 = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v18) > reinterpret_cast<unsigned char>(9))) & v12);
                        if (!al50) 
                            break;
                        if (reinterpret_cast<uint64_t>(rbx13) >= reinterpret_cast<uint64_t>(v18 + reinterpret_cast<unsigned char>(v18) * 4)) 
                            goto addr_bf00_41; else 
                            goto addr_bcc8_42;
                    }
                    if (!v51) 
                        goto addr_baf6_11;
                    al52 = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v18) > reinterpret_cast<unsigned char>(9))) & v12);
                    if (al52) 
                        goto addr_bc22_45;
                    ++v18;
                    ++rbx13;
                    addr_bc42_47:
                    if (!v53) 
                        goto addr_bcd6_35;
                    if (v54 == v55) {
                        addr_bd16_38:
                        __asm__("movdqa xmm1, [rsp+0xf0]");
                        __asm__("movdqa xmm0, [rsp+0xe0]");
                        __asm__("movdqa xmm2, [rsp+0x100]");
                        __asm__("movaps [rsp+0x130], xmm1");
                        __asm__("movdqa xmm3, [rsp+0x110]");
                        v56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v56) + reinterpret_cast<uint64_t>(v57));
                        __asm__("movaps [rsp+0x120], xmm0");
                        __asm__("movaps [rsp+0x140], xmm2");
                        __asm__("movaps [rsp+0x150], xmm3");
                        mbuiter_multi_next_part_0(r12_14, rsi);
                        rsp8 = rsp8 - 8 + 8;
                        if (v58) {
                            if (!v59) 
                                goto 0x2b87;
                        }
                        ++rbx13;
                        rbp60 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v61) + reinterpret_cast<unsigned char>(v4));
                        while ((mbuiter_multi_next_part_0(r12_14, rsi, r12_14, rsi), rsp8 = rsp8 - 8 + 8, v62 == 0) || v63) {
                            if (!0) {
                                mbuiter_multi_next_part_0(v17, rsi, v17, rsi);
                                rsp8 = rsp8 - 8 + 8;
                            }
                            if (v64) {
                                if (!v65) 
                                    goto addr_baf6_11;
                                if (!v66) 
                                    goto addr_be8e_57;
                            } else {
                                addr_be8e_57:
                                r15_67 = v68;
                                r14_69 = v70;
                                if (r15_67 != r14_69) 
                                    goto addr_bc68_58; else 
                                    goto addr_bea7_59;
                            }
                            if (v65 != v71) 
                                goto addr_bc68_58;
                            r15_67 = v72;
                            r14_69 = v73;
                            rbp74 = rbp60;
                            r13_75 = v56;
                            addr_be22_62:
                            rbp60 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp74) + reinterpret_cast<unsigned char>(r14_69));
                            ++rbx13;
                            v56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_75) + reinterpret_cast<unsigned char>(r15_67));
                            continue;
                            addr_bea7_59:
                            rbp74 = rbp60;
                            r13_75 = v56;
                            rsi = rbp74;
                            eax76 = fun_2860(r13_75, rsi, r15_67);
                            rsp8 = rsp8 - 8 + 8;
                            if (!eax76) 
                                goto addr_be22_62; else 
                                goto addr_becd_63;
                        }
                        goto addr_bfd0_29;
                    }
                    addr_bc68_58:
                    rcx46 = v15;
                    rdx44 = v77;
                    continue;
                    addr_becd_63:
                    goto addr_bc68_58;
                    addr_bc22_45:
                    rsi = v18;
                    if (reinterpret_cast<uint64_t>(rbx13) >= reinterpret_cast<uint64_t>(rsi + reinterpret_cast<unsigned char>(rsi) * 4)) {
                        addr_bf00_41:
                        r14_78 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx13) - reinterpret_cast<uint64_t>(v19));
                        if (!r14_78) {
                            if (0) {
                                addr_bf8d_66:
                                eax79 = v80;
                                v12 = *reinterpret_cast<unsigned char*>(&eax79);
                                if (*reinterpret_cast<unsigned char*>(&eax79)) {
                                    if (!v81) {
                                        addr_c010_68:
                                        rsi = v4;
                                        al82 = knuth_morris_pratt_multibyte(v3, rsi, v20);
                                        rsp8 = rsp8 - 8 + 8;
                                        v12 = al82;
                                        if (!al82) 
                                            goto addr_bff8_69; else 
                                            goto addr_c02c_25;
                                    } else {
                                        addr_bff8_69:
                                        edx83 = v84;
                                    }
                                } else {
                                    v12 = 1;
                                    edx83 = v85;
                                }
                            } else {
                                goto addr_bf80_72;
                            }
                        } else {
                            if (1) 
                                goto addr_bf4d_30;
                            if (!v86) 
                                goto addr_bf2f_31;
                            addr_bf20_33:
                            *reinterpret_cast<int32_t*>(&rsi) = v87;
                            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                            if (!*reinterpret_cast<int32_t*>(&rsi)) 
                                goto addr_c002_75; else 
                                goto addr_bf2f_31;
                        }
                    } else {
                        ++v18;
                        ++rbx13;
                        v12 = al52;
                        goto addr_bc42_47;
                    }
                    v19 = rbx13;
                    ++v18;
                    ++rbx13;
                    if (*reinterpret_cast<signed char*>(&edx83)) 
                        goto addr_bc42_47; else 
                        goto addr_bfc4_34;
                    addr_bf80_72:
                    mbuiter_multi_next_part_0(rsp8 + 0xa0, rsi);
                    rsp8 = rsp8 - 8 + 8;
                    goto addr_bf8d_66;
                    addr_bf2f_31:
                    r14_78 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_78) - 1);
                    if (!r14_78) 
                        goto addr_bf80_72; else 
                        goto addr_bf4d_30;
                    addr_c002_75:
                    if (1) 
                        goto addr_bf80_72; else 
                        goto addr_c010_68;
                }
                ++v18;
                ++rbx13;
                continue;
                addr_bcc8_42:
                ++v18;
                ++rbx13;
                v12 = al50;
            }
        }
    }
}

int64_t fun_2640(int64_t rdi, ...);

int64_t fun_c053(int64_t rdi, int64_t rsi, int64_t rdx, void*** rcx, void*** r8) {
    void* rbp6;
    int64_t r12_7;
    int64_t v8;
    void*** v9;
    void*** v10;
    void** rax11;
    void** v12;
    void* rsp13;
    int64_t v14;
    void* rbx15;
    int64_t rax16;
    int64_t rax17;
    void** rax18;
    void** r15_19;
    void** rbp20;
    void** rax21;
    void** v22;
    int64_t rax23;
    int64_t v24;
    void** rsi25;
    int64_t rax26;
    int32_t* rax27;
    int64_t rax28;
    int32_t* rax29;
    void* rdx30;
    int64_t rax31;
    int32_t* rax32;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rbp6) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp6) + 4) = 0;
    r12_7 = rdx;
    v8 = rsi;
    v9 = rcx;
    v10 = r8;
    rax11 = g28;
    v12 = rax11;
    fun_2640(r12_7);
    rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - reinterpret_cast<int64_t>("strcmp") - 0x68 - 8 + 8);
    v14 = rsi;
    rbx15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) + 80);
    if (!rsi) {
        addr_c132_2:
        rax16 = fun_2640(r12_7, r12_7);
        rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8);
        if (rax16 == -1) {
            addr_c277_3:
            *reinterpret_cast<int32_t*>(&rax17) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
        } else {
            rax18 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbx15) - reinterpret_cast<uint64_t>(rbx15) + reinterpret_cast<uint64_t>(rbp6));
            r15_19 = rax18;
            if (!rax18) {
                *v10 = reinterpret_cast<void**>(0);
                *reinterpret_cast<int32_t*>(&rax17) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            } else {
                rbp20 = *v9;
                if (rbp20 && reinterpret_cast<unsigned char>(*v10) >= reinterpret_cast<unsigned char>(r15_19) || (rax21 = fun_2920(r15_19), rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8), rbp20 = rax21, !!rax21)) {
                    fun_2640(r12_7);
                    rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8);
                    v22 = r15_19;
                    rax23 = v8;
                    v24 = rax23;
                    while (rax23) {
                        rsi25 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp13) + 48);
                        rax26 = fun_2640(r12_7, r12_7);
                        rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8);
                        if (rax26 == -1) 
                            goto addr_c290_10;
                        rax23 = v24;
                    }
                    goto addr_c1f2_12;
                } else {
                    rax27 = fun_2600();
                    *rax27 = 12;
                    goto addr_c277_3;
                }
            }
        }
    } else {
        do {
            rax28 = fun_2640(r12_7, r12_7);
            rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8);
            if (rax28 != -1) 
                continue;
            rax29 = fun_2600();
            rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8);
            if (*rax29 != 7) 
                goto addr_c129_17;
            rbp6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp6) + (reinterpret_cast<uint64_t>(rbx15) - reinterpret_cast<uint64_t>(rbx15)));
        } while (v14);
        goto addr_c132_2;
    }
    addr_c22c_19:
    rdx30 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v12) - reinterpret_cast<unsigned char>(g28));
    if (rdx30) {
        fun_2740();
    } else {
        return rax17;
    }
    addr_c1f2_12:
    *reinterpret_cast<int32_t*>(&rsi25) = 0;
    *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
    rax31 = fun_2640(r12_7, r12_7);
    if (rax31 == -1) {
        addr_c29e_22:
        *reinterpret_cast<int32_t*>(&rax17) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
        if (*v9 != rbp20) {
            fun_25e0(rbp20, rsi25, rbp20, rsi25);
            *reinterpret_cast<int32_t*>(&rax17) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            goto addr_c22c_19;
        }
    } else {
        if (v22) 
            goto 0x2b8c;
        *v9 = rbp20;
        *v10 = r15_19;
        *reinterpret_cast<int32_t*>(&rax17) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
        goto addr_c22c_19;
    }
    addr_c290_10:
    rax32 = fun_2600();
    if (*rax32 == 22) 
        goto addr_c1f2_12; else 
        goto addr_c29e_22;
    addr_c129_17:
    if (*rax29 != 22) 
        goto addr_c277_3; else 
        goto addr_c132_2;
}

void** fun_c2d3(void** rdi, int64_t rsi) {
    int64_t rbp3;
    void** rax4;
    void** v5;
    void** rax6;
    void** r10_7;
    void** rbx8;
    void** rax9;
    int32_t* rax10;
    void** r12_11;
    void* rax12;
    void** r14_13;
    void** v14;
    void** rsi15;
    void** v16;
    int64_t rax17;
    int32_t* rax18;
    void** r15_19;
    void** rax20;
    int64_t rax21;
    int32_t* rax22;
    int32_t* rdx23;
    void** r15_24;
    void** rax25;
    void** rdx26;
    void** rsi27;
    void** rax28;
    void** rdi29;

    __asm__("cli ");
    rbp3 = rsi;
    rax4 = g28;
    v5 = rax4;
    rax6 = fun_2720(rdi);
    r10_7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax6) << 4);
    if (reinterpret_cast<unsigned char>(rax6) >= reinterpret_cast<unsigned char>(0x10000000)) {
        r10_7 = rax6;
    }
    rbx8 = r10_7 + 1;
    rax9 = fun_2920(rbx8);
    if (!rax9) {
        while (1) {
            rax10 = fun_2600();
            *reinterpret_cast<int32_t*>(&r12_11) = 0;
            *reinterpret_cast<int32_t*>(&r12_11 + 4) = 0;
            *rax10 = 12;
            addr_c4d2_5:
            rax12 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v5) - reinterpret_cast<unsigned char>(g28));
            if (!rax12) 
                break;
            fun_2740();
        }
        return r12_11;
    }
    r14_13 = rax9;
    fun_2640(rbp3);
    r12_11 = r14_13;
    v14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 88 - 8 + 8 - 8 + 8 - 8 + 8 + 40);
    while (rsi15 = v14, v16 = r14_13, rax17 = fun_2640(rbp3, rbp3), rax17 == -1) {
        rax18 = fun_2600();
        if (*rax18 == 22) 
            goto addr_c46d_11;
        if (*rax18 != 7) 
            goto addr_c4c7_13;
        r15_19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx8) + reinterpret_cast<unsigned char>(rbx8));
        if (reinterpret_cast<unsigned char>(rbx8) >= reinterpret_cast<unsigned char>(r15_19)) 
            goto addr_c4c0_15;
        rsi15 = r15_19;
        rax20 = fun_2990(r12_11, rsi15);
        if (!rax20) 
            goto addr_c4c0_15;
        r14_13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax20) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(r12_11)));
        rbx8 = r15_19;
        r12_11 = rax20;
    }
    addr_c46d_11:
    while (*reinterpret_cast<int32_t*>(&rsi15) = 0, *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0, rax21 = fun_2640(rbp3, rbp3), rax21 == -1) {
        rax22 = fun_2600();
        rdx23 = rax22;
        if (*rax22 != 7) 
            goto addr_c4c7_13;
        r15_24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx8) + reinterpret_cast<unsigned char>(rbx8));
        if (reinterpret_cast<unsigned char>(rbx8) >= reinterpret_cast<unsigned char>(r15_24)) 
            goto addr_c4f8_21;
        rsi15 = r15_24;
        rax25 = fun_2990(r12_11, rsi15);
        rdx23 = rax22;
        if (!rax25) 
            goto addr_c4f8_21;
        rdx26 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax25) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(r12_11)));
        rbx8 = r15_24;
        r12_11 = rax25;
        v16 = rdx26;
    }
    *reinterpret_cast<void***>(v16) = reinterpret_cast<void**>(0);
    rsi27 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v16 + 1) - reinterpret_cast<unsigned char>(r12_11));
    if (reinterpret_cast<unsigned char>(rbx8) <= reinterpret_cast<unsigned char>(rsi27)) 
        goto addr_c4d2_5;
    rax28 = fun_2990(r12_11, rsi27);
    if (rax28) {
        r12_11 = rax28;
    }
    goto addr_c4d2_5;
    addr_c4c7_13:
    rdi29 = r12_11;
    *reinterpret_cast<int32_t*>(&r12_11) = 0;
    *reinterpret_cast<int32_t*>(&r12_11 + 4) = 0;
    fun_25e0(rdi29, rsi15, rdi29, rsi15);
    goto addr_c4d2_5;
    addr_c4f8_21:
    *rdx23 = 12;
    goto addr_c4c7_13;
    addr_c4c0_15:
    *rax18 = 12;
    goto addr_c4c7_13;
}

void** fun_2b20(void** rdi, void** rsi);

int32_t fun_29c0(void** rdi, void** rsi);

void** fun_c523(void** rdi, void** rsi, void** rdx) {
    void** r13_4;
    int32_t eax5;
    void** rax6;
    void** r12_7;
    int32_t* rax8;
    void** rax9;
    void** rax10;
    int32_t* rax11;
    int32_t ebx12;
    int32_t eax13;
    void** rdi14;

    __asm__("cli ");
    if (!*reinterpret_cast<void***>(rdi) || (r13_4 = rsi, rsi = rdx, eax5 = c_strcasecmp(r13_4, rsi, rdx), !eax5)) {
        rax6 = fun_2ae0(rdi, rsi);
        r12_7 = rax6;
        if (!rax6) {
            rax8 = fun_2600();
            *rax8 = 12;
        }
    } else {
        rax9 = fun_2b20(rdx, r13_4);
        if (rax9 == 0xffffffffffffffff) {
            *reinterpret_cast<int32_t*>(&r12_7) = 0;
            *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
        } else {
            rax10 = str_cd_iconv(rdi, rax9);
            r12_7 = rax10;
            if (!rax10) {
                rax11 = fun_2600();
                ebx12 = *rax11;
                fun_29c0(rax9, rax9);
                *rax11 = ebx12;
            } else {
                eax13 = fun_29c0(rax9, rax9);
                if (eax13 < 0) {
                    rdi14 = r12_7;
                    *reinterpret_cast<int32_t*>(&r12_7) = 0;
                    *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                    fun_25e0(rdi14, rax9);
                }
            }
        }
    }
    return r12_7;
}

int32_t setlocale_null_r();

int64_t fun_c5e3() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2740();
    } else {
        return rax3;
    }
}

void* fun_c663(void** rdi) {
    void** r8_2;
    void** rax3;
    void* rdx4;
    void* rax5;

    __asm__("cli ");
    r8_2 = rdi + 32;
    if (reinterpret_cast<signed char>(r8_2) < reinterpret_cast<signed char>(0) || reinterpret_cast<unsigned char>(r8_2) < reinterpret_cast<unsigned char>(rdi)) {
        return 0;
    } else {
        rax3 = fun_2920(r8_2);
        if (!rax3) {
            return 0;
        } else {
            rdx4 = reinterpret_cast<void*>((reinterpret_cast<uint64_t>(rax3 + 16) & 0xffffffffffffffe0) - reinterpret_cast<unsigned char>(rax3) + 16);
            rax5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<uint64_t>(rdx4));
            *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rax5) - 1) = *reinterpret_cast<signed char*>(&rdx4);
            return rax5;
        }
    }
}

void fun_c6b3(int64_t rdi) {
    __asm__("cli ");
    if (*reinterpret_cast<unsigned char*>(&rdi) & 15) 
        goto 0x2b91;
    if (*reinterpret_cast<unsigned char*>(&rdi) & 16) {
        goto fun_25e0;
    } else {
        return;
    }
}

int64_t fun_c6e3(void** rdi) {
    void** rax2;
    void** v3;
    uint64_t rax4;
    signed char* rsp5;
    void** v6;
    int64_t r12_7;
    signed char v8;
    void** rbp9;
    void** rdx10;
    uint32_t ecx11;
    uint32_t eax12;
    int64_t rax13;
    uint32_t eax14;
    void** rsi15;
    uint32_t eax16;
    void** v17;
    int32_t eax18;
    int32_t v19;
    uint64_t rax20;
    void** rax21;
    void* rsp22;
    void** rdi23;
    void** rax24;
    void** rax25;
    uint32_t eax26;
    void* rax27;
    void** r8_28;
    void** r9_29;
    int64_t r8_30;
    int64_t r9_31;
    void** r8_32;
    void** r9_33;
    int64_t r8_34;
    int64_t r9_35;
    void** r8_36;
    void** r9_37;
    int64_t r8_38;
    int64_t r9_39;
    void* rax40;

    __asm__("cli ");
    rax2 = g28;
    v3 = rax2;
    rax4 = fun_2710();
    rsp5 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 88 - 8 + 8);
    if (rax4 > 1) {
        v6 = rdi;
        *reinterpret_cast<int32_t*>(&r12_7) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        v8 = 0;
        while (1) {
            rbp9 = reinterpret_cast<void**>(rsp5 + 4);
            if (!v8) {
                rdx10 = v6;
                ecx11 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10));
                eax12 = ecx11;
                *reinterpret_cast<unsigned char*>(&eax12) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax12) >> 5);
                *reinterpret_cast<uint32_t*>(&rax13) = eax12 & 7;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
                eax14 = *reinterpret_cast<uint32_t*>(0x10e40 + rax13 * 4) >> *reinterpret_cast<signed char*>(&ecx11);
                if (!(*reinterpret_cast<unsigned char*>(&eax14) & 1)) {
                    rbp9 = reinterpret_cast<void**>(rsp5 + 4);
                    eax16 = fun_2af0(rbp9, rsi15, rbp9, rsi15);
                    rsp5 = rsp5 - 8 + 8;
                    if (!eax16) 
                        break;
                    v8 = 1;
                    goto addr_c7bc_7;
                } else {
                    v17 = reinterpret_cast<void**>(1);
                    eax18 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdx10));
                    v19 = eax18;
                }
            } else {
                addr_c7bc_7:
                rax20 = fun_2710();
                rax21 = strnlen1(v6, rax20, rdx10, v6, rax20, rdx10);
                rsp22 = reinterpret_cast<void*>(rsp5 - 8 + 8 - 8 + 8);
                rdi23 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 36);
                rsi15 = v6;
                rdx10 = rax21;
                rax24 = rpl_mbrtowc(rdi23, rsi15, rdx10, rbp9, rdi23, rsi15, rdx10, rbp9);
                rsp5 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                v17 = rax24;
                if (rax24 == 0xffffffffffffffff) {
                    v17 = reinterpret_cast<void**>(1);
                    goto addr_c79e_10;
                } else {
                    if (rax24 == 0xfffffffffffffffe) {
                        rax25 = fun_2720(v6, v6);
                        rsp5 = rsp5 - 8 + 8;
                        v17 = rax25;
                        goto addr_c79e_10;
                    } else {
                        if (!rax24) {
                            v17 = reinterpret_cast<void**>(1);
                            if (*reinterpret_cast<void***>(v6)) 
                                goto addr_c8ed_15;
                            *reinterpret_cast<int32_t*>(&rdx10) = v19;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (*reinterpret_cast<int32_t*>(&rdx10)) 
                                goto addr_c90c_17;
                        }
                        eax26 = fun_2af0(rbp9, rsi15, rbp9, rsi15);
                        rsp5 = rsp5 - 8 + 8;
                        if (eax26) 
                            goto addr_c83a_19;
                    }
                }
            }
            if (!v19) 
                goto addr_c850_21;
            addr_c79e_10:
            v6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v6) + reinterpret_cast<unsigned char>(v17));
            ++r12_7;
            continue;
            addr_c83a_19:
            v8 = 0;
            if (v19) 
                goto addr_c79e_10; else 
                goto addr_c84a_22;
        }
    } else {
        rax27 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v3) - reinterpret_cast<unsigned char>(g28));
        if (!rax27) {
            goto fun_2720;
        }
    }
    fun_2810("mbsinit (&iter->state)", "lib/mbuiter.h", 0x8f, "mbuiter_multi_next", r8_28, r9_29, "mbsinit (&iter->state)", "lib/mbuiter.h", 0x8f, "mbuiter_multi_next", r8_30, r9_31);
    fun_2740();
    addr_c8ed_15:
    fun_2810("*iter->cur.ptr == '\\0'", "lib/mbuiter.h", 0xab, "mbuiter_multi_next", r8_32, r9_33, "*iter->cur.ptr == '\\0'", "lib/mbuiter.h", 0xab, "mbuiter_multi_next", r8_34, r9_35);
    addr_c90c_17:
    fun_2810("iter->cur.wc == 0", "lib/mbuiter.h", 0xac, "mbuiter_multi_next", r8_36, r9_37, "iter->cur.wc == 0", "lib/mbuiter.h", 0xac, "mbuiter_multi_next", r8_38, r9_39);
    addr_c850_21:
    rax40 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v3) - reinterpret_cast<unsigned char>(g28));
    if (!rax40) {
        return r12_7;
    }
    addr_c84a_22:
    goto addr_c850_21;
}

int64_t fun_c933(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** rax6;
    int32_t r13d7;
    void** rax8;
    int64_t rax9;

    __asm__("cli ");
    rax6 = fun_29a0(rdi);
    if (!rax6) {
        r13d7 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax8 = fun_2720(rax6);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax8)) {
            fun_28d0(rsi, rax6, rax8 + 1, rcx, r8);
            return 0;
        } else {
            r13d7 = 34;
            if (rdx) {
                fun_28d0(rsi, rax6, rdx + 0xffffffffffffffff, rcx, r8);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax9) = r13d7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    return rax9;
}

void fun_c9e3() {
    __asm__("cli ");
    goto fun_29a0;
}

void fun_c9f3() {
    __asm__("cli ");
}

void fun_ca07() {
    __asm__("cli ");
    return;
}

int32_t fun_2b00(int64_t rdi, void** rsi);

void fun_7525() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rax10;
    void** r11_11;
    void** v12;
    int32_t ebp13;
    void** rax14;
    void* r15_15;
    int32_t ebx16;
    void** rdx17;
    uint32_t eax18;
    void* r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void* v31;
    void** v32;
    void* r10_33;
    void* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rdx44;
    uint32_t ecx45;
    uint32_t edx46;
    unsigned char al47;
    void** v48;
    uint64_t v49;
    void** v50;
    void** v51;
    void** rax52;
    uint64_t rdx53;
    uint32_t edx54;
    int64_t rdx55;
    uint32_t eax56;
    uint32_t eax57;
    uint32_t eax58;
    uint1_t zf59;
    unsigned char v60;
    void** v61;
    unsigned char v62;
    void* v63;
    void* v64;
    void** v65;
    signed char* v66;
    void** r12_67;
    unsigned char v68;
    void* rbx69;
    uint32_t v70;
    void** r14_71;
    void** r13_72;
    void** rsi73;
    void* v74;
    void** r15_75;
    void** rdx76;
    void* v77;
    int64_t rax78;
    int64_t rdi79;
    int32_t v80;
    int32_t eax81;
    void* rdi82;
    uint32_t edx83;
    unsigned char v84;
    void* rdi85;
    void* v86;
    uint32_t esi87;
    uint32_t ebp88;
    void** rcx89;
    uint32_t eax90;
    uint32_t eax91;
    uint32_t eax92;
    uint32_t eax93;
    uint32_t eax94;
    uint32_t eax95;
    void* rdx96;
    void* rcx97;
    void* v98;
    unsigned char** rax99;
    uint1_t zf100;
    int32_t ecx101;
    int32_t ecx102;
    uint32_t ecx103;
    uint32_t edi104;
    int32_t ecx105;
    uint32_t edi106;
    uint32_t edx107;
    uint32_t edi108;
    uint32_t edx109;
    uint64_t rax110;
    uint32_t eax111;
    uint32_t r12d112;
    uint64_t rax113;
    uint64_t rax114;
    uint32_t r12d115;
    uint32_t ecx116;
    void** v117;
    void* rdx118;
    void* rax119;
    void* v120;
    uint64_t rax121;
    int64_t v122;
    int64_t rax123;
    int64_t rax124;
    int64_t rax125;
    int64_t v126;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2700();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        rax10 = fun_2700();
        rsp1 = rsp4 - 8 + 8;
        r11_11 = r11_5;
        v12 = rax10;
        if (rax10 == "'") {
            rax14 = gettext_quote_part_0(rax10, ebp13, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_11 = r11_5;
            v12 = rax14;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
    if (!ebx16 && (rdx17 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx17)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<uint64_t>(r13_19) > reinterpret_cast<uint64_t>(r15_15)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx17) + reinterpret_cast<uint64_t>(r15_15));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2720(v12, v12);
    rsp25 = rsp1 - 8 + 8;
    v26 = v12;
    r11_27 = r11_11;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void*>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_7823_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_7823_22; else 
                            goto addr_7c1d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_7cdd_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_8030_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_7820_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_7820_30; else 
                                goto addr_8049_32;
                        }
                    } else {
                        rdx44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2720(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx44 = rdx44;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx44) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_8030_28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2860(rdi39, rsi30, v28, rdi39, rsi30, v28);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_8030_28; else 
                            goto addr_76cc_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_8190_39:
                    ecx45 = 0x7d;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_8010_40:
                        if (r11_27 == 1) {
                            addr_7b9d_41:
                            edx46 = r8d42;
                            if (r9_37) {
                                addr_8158_42:
                                r8d42 = edx46;
                                al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = ecx45;
                                goto addr_77d7_44;
                            }
                        } else {
                            goto addr_8020_46;
                        }
                    } else {
                        addr_819f_47:
                        rax24 = v48;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_7b9d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        ecx45 = 0x7b;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_7823_22:
                                if (v49 != 1) {
                                    addr_7d79_52:
                                    v50 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax52 = fun_2720(v51, v51);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax52;
                                        goto addr_7dc4_54;
                                    }
                                } else {
                                    goto addr_7830_56;
                                }
                            } else {
                                addr_77d5_57:
                                ebp36 = 0;
                                goto addr_77d7_44;
                            }
                        } else {
                            addr_8004_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_819f_47; else 
                                goto addr_800e_59;
                        }
                    } else {
                        ecx45 = 0x7e;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_7b9d_41;
                        if (v49 == 1) 
                            goto addr_7830_56; else 
                            goto addr_7d79_52;
                    }
                }
                addr_7891_62:
                *reinterpret_cast<uint32_t*>(&rdx53) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                rax24 = reinterpret_cast<void**>(al47 | *reinterpret_cast<unsigned char*>(&rdx53));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_7728_63:
                    if (!1 && (edx54 = ecx45, *reinterpret_cast<uint32_t*>(&rdx55) = *reinterpret_cast<unsigned char*>(&edx54) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx55) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx53) = *reinterpret_cast<uint32_t*>(rdx55 * 4) >> *reinterpret_cast<signed char*>(&ecx45) & 1, !!*reinterpret_cast<uint32_t*>(&rdx53)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_774d_64:
                        *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax56 = *reinterpret_cast<uint32_t*>(&rdx53);
                        if (v22) 
                            goto addr_7a50_65;
                    } else {
                        addr_78b9_66:
                        ++r9_37;
                        eax57 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_8108_67;
                    }
                } else {
                    goto addr_78b0_69;
                }
                addr_7761_70:
                eax58 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax58) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax58) & *reinterpret_cast<unsigned char*>(&rdx53));
                if (*reinterpret_cast<unsigned char*>(&eax58)) {
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 36;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax58;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                }
                if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                }
                r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                ++r9_37;
                addr_77ac_81:
                if (reinterpret_cast<uint64_t>(r15_15) < reinterpret_cast<uint64_t>(r10_33)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<signed char*>(&ecx45);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_8108_67:
                if (*reinterpret_cast<signed char*>(&eax57)) {
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                    goto addr_77ac_81;
                }
                addr_78b0_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_774d_64; else 
                    goto addr_78b9_66;
                addr_77d7_44:
                zf59 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al47 = zf59;
                if (!zf59) 
                    goto addr_788f_91;
                if (v22) 
                    goto addr_77ef_93;
                addr_788f_91:
                ecx45 = *reinterpret_cast<uint32_t*>(&rbx43);
                goto addr_7891_62;
                addr_7dc4_54:
                v60 = *reinterpret_cast<unsigned char*>(&r8d42);
                v61 = r9_37;
                v62 = *reinterpret_cast<unsigned char*>(&r13_34);
                v63 = r15_15;
                v64 = r10_33;
                v65 = r11_27;
                v66 = r12_21;
                r12_67 = v50;
                v68 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx69 = reinterpret_cast<void*>(0);
                v70 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_71 = reinterpret_cast<void**>(rsp25 + 0xac);
                do {
                    r13_72 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v61) + reinterpret_cast<uint64_t>(rbx69));
                    rsi73 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v74) + reinterpret_cast<unsigned char>(r13_72));
                    rax24 = rpl_mbrtowc(r14_71, rsi73, reinterpret_cast<unsigned char>(v65) - reinterpret_cast<unsigned char>(r13_72), r12_67);
                    rsp25 = rsp25 - 8 + 8;
                    r15_75 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_854b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_85bb_98;
                    if (v70 == 2 && (v22 && rax24 != 1)) {
                        rdx76 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v77) + reinterpret_cast<unsigned char>(r13_72) + 1);
                        rsi73 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v77) + reinterpret_cast<unsigned char>(r15_75)) + reinterpret_cast<unsigned char>(r13_72));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax78) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx76) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax78) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax78) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax78)) 
                                goto addr_83bf_103;
                            ++rdx76;
                        } while (rsi73 != rdx76);
                    }
                    *reinterpret_cast<int32_t*>(&rdi79) = v80;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi79) + 4) = 0;
                    eax81 = fun_2b00(rdi79, rsi73);
                    if (!eax81) {
                        ebp36 = 0;
                    }
                    rbx69 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx69) + reinterpret_cast<unsigned char>(r15_75));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2af0(r12_67, rsi73, r12_67, rsi73);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi82 = rbx69;
                r8d42 = v60;
                r9_37 = v61;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                edx83 = ebp36 ^ 1;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                r11_27 = v65;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&edx83) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&edx83) & reinterpret_cast<unsigned char>(v32));
                addr_7ebe_109:
                if (reinterpret_cast<uint64_t>(rdi82) <= 1) {
                    addr_787c_110:
                    if (*reinterpret_cast<unsigned char*>(&edx83)) {
                        edx83 = reinterpret_cast<unsigned char>(v32);
                        ebp36 = 0;
                        goto addr_7ec8_112;
                    }
                } else {
                    addr_7ec8_112:
                    v84 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi85 = v86;
                    esi87 = 0;
                    ebp88 = reinterpret_cast<unsigned char>(v22);
                    rcx89 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi82) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_7f99_114;
                }
                addr_7888_115:
                al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_788f_91;
                while (1) {
                    addr_7f99_114:
                    if (*reinterpret_cast<unsigned char*>(&edx83)) {
                        *reinterpret_cast<unsigned char*>(&esi87) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax90 = esi87;
                        if (*reinterpret_cast<signed char*>(&ebp88)) 
                            goto addr_84a7_117;
                        eax91 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax91) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax91) & *reinterpret_cast<unsigned char*>(&esi87));
                        if (*reinterpret_cast<unsigned char*>(&eax91)) 
                            goto addr_7f06_119;
                    } else {
                        eax57 = (esi87 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                            }
                            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx89)) 
                            goto addr_84b5_125;
                        if (!*reinterpret_cast<signed char*>(&eax57)) {
                            r8d42 = 0;
                            goto addr_7f87_128;
                        } else {
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                            }
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 39;
                            }
                            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                            goto addr_7f87_128;
                        }
                    }
                    addr_7f35_134:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        eax92 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax92) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax92) >> 6);
                        eax93 = eax92 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = *reinterpret_cast<signed char*>(&eax93);
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        eax94 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax94) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax94) >> 3);
                        eax95 = (eax94 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = *reinterpret_cast<signed char*>(&eax95);
                    }
                    ++r9_37;
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx89)) 
                        break;
                    esi87 = edx83;
                    addr_7f87_128:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi85) + reinterpret_cast<unsigned char>(r9_37));
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                    continue;
                    addr_7f06_119:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 36;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax91;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                    goto addr_7f35_134;
                }
                ebp36 = v84;
                ecx45 = *reinterpret_cast<uint32_t*>(&rbx43);
                goto addr_77ac_81;
                addr_84b5_125:
                ebp36 = v84;
                ecx45 = *reinterpret_cast<uint32_t*>(&rbx43);
                goto addr_8108_67;
                addr_854b_96:
                rdi82 = rbx69;
                r8d42 = v60;
                r9_37 = v61;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                r11_27 = v65;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                edx83 = reinterpret_cast<unsigned char>(v32);
                goto addr_7ebe_109;
                addr_85bb_98:
                r11_27 = v65;
                rdi82 = rbx69;
                rax24 = r13_72;
                r9_37 = v61;
                r8d42 = v60;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                rdx96 = rdi82;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx97 = v98;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx97) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx96 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx96) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx96));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi82 = rdx96;
                }
                edx83 = reinterpret_cast<unsigned char>(v32);
                ebp36 = 0;
                goto addr_7ebe_109;
                addr_7830_56:
                rax99 = fun_2b10(rdi39, rsi30, rdi39, rsi30);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi82) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi82) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf100 = reinterpret_cast<uint1_t>(((*rax99 + reinterpret_cast<unsigned char>(rax24) * 2)[1] & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf100);
                *reinterpret_cast<unsigned char*>(&edx83) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf100) & reinterpret_cast<unsigned char>(v32));
                goto addr_787c_110;
                addr_800e_59:
                goto addr_8010_40;
                addr_7cdd_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_7823_22;
                ecx101 = static_cast<int32_t>(rbx43 - 65);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx101));
                if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                    goto addr_7888_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_77d5_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_7823_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_7d22_160;
                if (!v22) 
                    goto addr_80f7_162; else 
                    goto addr_8303_163;
                addr_7d22_160:
                *reinterpret_cast<uint32_t*>(&rdx53) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx53) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx53);
                if (*reinterpret_cast<unsigned char*>(&rdx53)) {
                    addr_80f7_162:
                    ++r9_37;
                    eax57 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    ecx45 = 92;
                    goto addr_8108_67;
                } else {
                    ecx45 = 92;
                    if (!v32) 
                        goto addr_7bcb_166;
                }
                ecx45 = *reinterpret_cast<uint32_t*>(&rbx43);
                ebp36 = 0;
                addr_7a33_168:
                *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax56 = *reinterpret_cast<uint32_t*>(&rdx53);
                if (!v22) 
                    goto addr_7761_70; else 
                    goto addr_7a47_169;
                addr_7bcb_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_7728_63;
                goto addr_78b0_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        ecx45 = 0x7d;
                        r8d42 = 0;
                        goto addr_8004_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_813f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_7820_30;
                    ecx102 = static_cast<int32_t>(rbx43 - 65);
                    rdx53 = 0x3ffffff53ffffff;
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx102));
                    ecx103 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                        goto addr_7718_178; else 
                        goto addr_80c2_179;
                }
                ecx45 = 0x7b;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_8004_58;
                ecx45 = 0x7c;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_7823_22;
                }
                addr_813f_175:
                edx46 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_7820_30:
                    r8d42 = 0;
                    goto addr_7823_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        ecx45 = 0x7e;
                        r8d42 = edx46;
                        al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_7891_62;
                    } else {
                        ecx45 = 0x7e;
                        goto addr_8158_42;
                    }
                }
                addr_7718_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx103;
                ecx45 = *reinterpret_cast<uint32_t*>(&rbx43);
                goto addr_7728_63;
                addr_80c2_179:
                ecx45 = *reinterpret_cast<uint32_t*>(&rbx43);
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_8020_46:
                    al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_7891_62;
                } else {
                    addr_80d2_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_7823_22;
                }
                edi104 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi104))) 
                    goto addr_8882_188;
                if (v28) 
                    goto addr_80f7_162;
                addr_8882_188:
                ecx45 = 92;
                ebp36 = 0;
                goto addr_7a33_168;
                addr_76cc_37:
                if (v22) 
                    goto addr_86c3_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_76e3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_8190_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_821b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_7823_22;
                    ecx105 = static_cast<int32_t>(rbx43 - 65);
                    rdx53 = 0x3ffffff53ffffff;
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx105));
                    ecx103 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                        goto addr_7718_178; else 
                        goto addr_81f7_199;
                }
                ecx45 = 0x7b;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_8004_58;
                ecx45 = 0x7c;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_7823_22;
                }
                addr_821b_196:
                edx46 = r8d42;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_7823_22;
                }
                addr_81f7_199:
                ecx45 = *reinterpret_cast<uint32_t*>(&rbx43);
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_8020_46;
                goto addr_80d2_186;
                addr_76e3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_7823_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_7823_22; else 
                    goto addr_76f4_206;
            }
            edi106 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            edx107 = edi106 & *reinterpret_cast<uint32_t*>(&rax24);
            if (static_cast<unsigned char>(reinterpret_cast<uint1_t>(r15_15 == 0)) & *reinterpret_cast<unsigned char*>(&edx107)) 
                goto addr_87ce_208;
            edi108 = edi106 ^ 1;
            edx109 = edi108;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi108));
            if (!rax24) 
                goto addr_8654_210;
            if (1) 
                goto addr_8652_212;
            if (!v29) 
                goto addr_828e_214;
            *reinterpret_cast<int32_t*>(&r15_15) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax110 = fun_2710();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v49 = rax110;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_87c1_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
            v31 = reinterpret_cast<void*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_7a50_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax111 = eax56 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax111)) 
            goto addr_780b_219; else 
            goto addr_7a6a_220;
        addr_77ef_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax90 = reinterpret_cast<unsigned char>(v32);
        addr_7803_221:
        if (*reinterpret_cast<signed char*>(&eax90)) 
            goto addr_7a6a_220; else 
            goto addr_780b_219;
        addr_83bf_103:
        r12d112 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v66;
        r13_34 = v64;
        r11_27 = v65;
        if (*reinterpret_cast<signed char*>(&r12d112)) {
            addr_7a6a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax113 = fun_2710();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v49 = rax113;
        } else {
            addr_83dd_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax114 = fun_2710();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v49 = rax114;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void*>(0);
            continue;
        }
        addr_8850_225:
        v31 = r13_34;
        addr_82b6_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void*>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_84a7_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_7803_221;
        addr_8303_163:
        eax90 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_7803_221;
        addr_7a47_169:
        goto addr_7a50_65;
        addr_87ce_208:
        r14_35 = r12_21;
        r12d115 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d115)) 
            goto addr_7a6a_220;
        goto addr_83dd_222;
        addr_8654_210:
        if (v26 && (*reinterpret_cast<signed char*>(&edx109) && (ecx116 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), !!*reinterpret_cast<signed char*>(&ecx116)))) {
            rsi30 = v117;
            rdx118 = r15_15;
            rax119 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<uint64_t>(r15_15));
            do {
                if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(rdx118)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<uint64_t>(rdx118)) = *reinterpret_cast<signed char*>(&ecx116);
                }
                rdx118 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx118) + 1);
                ecx116 = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax119) + reinterpret_cast<uint64_t>(rdx118));
            } while (*reinterpret_cast<signed char*>(&ecx116));
            r15_15 = rdx118;
        }
        if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v120) + reinterpret_cast<uint64_t>(r15_15)) = 0;
        }
        rax121 = reinterpret_cast<uint64_t>(v122 - reinterpret_cast<unsigned char>(g28));
        if (!rax121) 
            goto addr_86ae_236;
        fun_2740();
        rsp25 = rsp25 - 8 + 8;
        goto addr_8850_225;
        addr_8652_212:
        edx109 = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_8654_210;
        addr_828e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            edx109 = 0;
            goto addr_8654_210;
        } else {
            goto addr_82b6_226;
        }
        addr_87c1_216:
        r13_34 = reinterpret_cast<void*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_7c1d_24:
    *reinterpret_cast<uint32_t*>(&rax123) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax123) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x1084c + rax123 * 4) + 0x1084c;
    addr_8049_32:
    *reinterpret_cast<uint32_t*>(&rax124) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax124) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x1094c + rax124 * 4) + 0x1094c;
    addr_86c3_190:
    addr_780b_219:
    goto 0x74f0;
    addr_76f4_206:
    *reinterpret_cast<uint32_t*>(&rax125) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax125) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x1074c + rax125 * 4) + 0x1074c;
    addr_86ae_236:
    goto v126;
}

void fun_7710() {
}

void fun_78c8() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x75c2;
}

void fun_7921() {
    goto 0x75c2;
}

void fun_7a0e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x7891;
    }
    if (v2) 
        goto 0x8303;
    if (!r10_3) 
        goto addr_846e_5;
    if (!v4) 
        goto addr_833e_7;
    addr_846e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_833e_7:
    goto 0x7744;
}

void fun_7a2c() {
}

void fun_7ad7() {
    signed char v1;

    if (v1) {
        goto 0x7a5f;
    } else {
        goto 0x779a;
    }
}

void fun_7af1() {
    signed char v1;

    if (!v1) 
        goto 0x7aea; else 
        goto "???";
}

void fun_7b18() {
    goto 0x7a33;
}

void fun_7b98() {
}

void fun_7bb0() {
}

void fun_7bdf() {
    goto 0x7a33;
}

void fun_7c31() {
    goto 0x7bc0;
}

void fun_7c60() {
    goto 0x7bc0;
}

void fun_7c93() {
    goto 0x7bc0;
}

void fun_8060() {
    goto 0x7718;
}

void fun_835e() {
    signed char v1;

    if (v1) 
        goto 0x8303;
    goto 0x7744;
}

void fun_8405() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x7744;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x7728;
        goto 0x7744;
    }
}

void fun_8822() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x7a90;
    } else {
        goto 0x75c2;
    }
}

void fun_a148() {
    fun_2700();
}

void fun_794e() {
    goto 0x75c2;
}

void fun_7b24() {
    goto 0x7adc;
}

void fun_7beb() {
    goto 0x7718;
}

void fun_7c3d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x7bc0;
    goto 0x77ef;
}

void fun_7c6f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x7bcb;
        goto 0x75f0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x7a6a;
        goto 0x780b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x8408;
    if (r10_8 > r15_9) 
        goto addr_7b55_9;
    addr_7b5a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x8413;
    goto 0x7744;
    addr_7b55_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_7b5a_10;
}

void fun_7ca2() {
    goto 0x77d7;
}

void fun_8070() {
    goto 0x77d7;
}

void fun_880f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x792c;
    } else {
        goto 0x7a90;
    }
}

void fun_a200() {
}

void fun_7cac() {
    goto 0x7c47;
}

void fun_807a() {
    goto 0x7b9d;
}

void fun_a260() {
    fun_2700();
    goto fun_2a90;
}

void fun_797d() {
    goto 0x75c2;
}

void fun_7cb8() {
    goto 0x7c47;
}

void fun_8087() {
    goto 0x7bee;
}

void fun_a2a0() {
    fun_2700();
    goto fun_2a90;
}

void fun_79aa() {
    goto 0x75c2;
}

void fun_7cc4() {
    goto 0x7bc0;
}

void fun_a2e0() {
    fun_2700();
    goto fun_2a90;
}

void fun_79cc() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x8360;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x7891;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x7891;
    }
    if (v11) 
        goto 0x86c3;
    if (r10_12 > r15_13) 
        goto addr_8713_8;
    addr_8718_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x8451;
    addr_8713_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_8718_9;
}

struct s109 {
    signed char[24] pad24;
    void** f18;
};

struct s110 {
    signed char[16] pad16;
    void** f10;
};

struct s111 {
    signed char[8] pad8;
    void** f8;
};

void fun_a330() {
    void** r15_1;
    struct s109* rbx2;
    void** r14_3;
    struct s110* rbx4;
    void** r13_5;
    struct s111* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    void** rbp10;
    void** v11;
    void** v12;
    void** v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2700();
    fun_2a90(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0xa352, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_a388() {
    fun_2700();
    goto 0xa359;
}

struct s112 {
    signed char[32] pad32;
    void** f20;
};

struct s113 {
    signed char[24] pad24;
    void** f18;
};

struct s114 {
    signed char[16] pad16;
    void** f10;
};

struct s115 {
    signed char[8] pad8;
    void** f8;
};

struct s116 {
    signed char[40] pad40;
    void** f28;
};

void fun_a3c0() {
    void** rcx1;
    struct s112* rbx2;
    void** r15_3;
    struct s113* rbx4;
    void** r14_5;
    struct s114* rbx6;
    void** r13_7;
    struct s115* rbx8;
    void** r12_9;
    void*** rbx10;
    void** v11;
    struct s116* rbx12;
    void** rax13;
    void** rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2700();
    fun_2a90(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0xa3f4, __return_address(), rcx1);
    goto v15;
}

void fun_a438() {
    fun_2700();
    goto 0xa3fb;
}
