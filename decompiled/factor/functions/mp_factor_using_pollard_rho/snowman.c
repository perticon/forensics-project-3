void mp_factor_using_pollard_rho(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rbp7;
    void* rsp8;
    void** v9;
    void** v10;
    void** rax11;
    void** v12;
    int1_t zf13;
    void** rdi14;
    void** v15;
    void** v16;
    void** v17;
    void** v18;
    void** v19;
    void** rdx20;
    void** r15_21;
    void** rbx22;
    void** r14_23;
    void** r12_24;
    void** r13_25;
    void* rsp26;
    void** rax27;
    void*** rsp28;
    void** rax29;
    void* rsp30;
    void** v31;
    int32_t eax32;
    void*** rsp33;
    void** r15_34;
    void** rbx35;
    void** rdx36;
    void** rax37;
    int64_t rax38;
    int32_t eax39;
    void*** rsp40;
    void* rsp41;
    void** v42;
    void** v43;
    void** r14_44;
    void** rbx45;
    void** v46;
    void** r13_47;
    void** r15_48;
    void** rbx49;
    void** r13_50;
    void** v51;
    void** r14_52;
    void** r12_53;
    int32_t eax54;
    void** rdx55;
    int32_t eax56;
    void* rsp57;
    int32_t eax58;
    signed char al59;
    int1_t zf60;
    void* rsp61;
    int32_t eax62;
    void* rsp63;
    int32_t eax64;
    signed char al65;
    void** r8_66;
    void** r9_67;
    void** rsi68;
    void** rdi69;
    void** rcx70;
    void** rdx71;
    void* rax72;
    void* rsp73;
    void** rax74;
    void** v75;
    int32_t v76;
    int1_t zf77;
    void** rdi78;
    void** r13_79;
    void** v80;
    void** rax81;
    void** rbx82;
    void** rdx83;
    void* rsp84;
    int32_t eax85;
    void* rsp86;
    void* rax87;
    void** rsi88;
    int32_t eax89;
    void* rsp90;
    int32_t eax91;
    int32_t eax92;
    int1_t zf93;
    int32_t eax94;
    signed char al95;
    void* rax96;
    void* rax97;
    void* rax98;
    void** r12_99;
    void** rdx100;
    int64_t v101;
    void** rbx102;
    void* rsp103;
    void** rax104;
    void** v105;
    void** r12_106;
    void** r15_107;
    void** rbp108;
    void** r14_109;
    void** v110;
    void** rax111;
    void** r13_112;
    void** v113;
    void** r9_114;
    void** r8_115;
    void** rcx116;
    unsigned char al117;
    void* rsp118;
    void* rsp119;
    void** rsi120;
    void* rsp121;
    void** rdi122;
    void** rcx123;
    void** rdx124;
    void** rsi125;
    void* rax126;
    int64_t v127;
    void* rsp128;
    void** r15_129;
    void** rbp130;
    void*** rsp131;
    void** v132;
    void** v133;
    void** rax134;
    void** v135;
    uint64_t rcx136;
    void** rdx137;
    int64_t rcx138;
    uint64_t rcx139;
    uint1_t cf140;
    void** rax141;
    void** rsi142;
    uint1_t cf143;
    int1_t cf144;
    void** v145;
    void** v146;
    void** tmp64_147;
    void** rax148;
    void** r12_149;
    void** r10_150;
    void** v151;
    void** v152;
    void** v153;
    void** v154;
    void** r14_155;
    void** r15_156;
    void*** v157;
    void** r9_158;
    uint64_t rax159;
    int64_t rax160;
    void* rax161;
    void* rdx162;
    void* rax163;
    void** rax164;
    void** rbp165;
    int64_t rax166;
    void** v167;
    int64_t v168;
    void** rax169;
    void** tmp64_170;
    void** rdx171;
    void* v172;
    void** v173;
    void** rdx174;
    void** r8_175;
    void** rcx176;
    void** tmp64_177;
    uint1_t cf178;
    void** rax179;
    void** v180;
    void** rax181;
    int64_t rax182;
    void** v183;
    void** rax184;
    void** v185;
    void** v186;
    void** rdx187;
    void** r15_188;
    void** r12_189;
    void** r13_190;
    void** rsi191;
    void** rbp192;
    void** rax193;
    void** tmp64_194;
    void** rcx195;
    void* v196;
    void** v197;
    void** r11_198;
    void** r10_199;
    void** v200;
    void** r15_201;
    void** r12_202;
    void*** rbp203;
    void** r9_204;
    void** rax205;
    void** tmp64_206;
    void** rdx207;
    void* v208;
    void** rcx209;
    void** rdx210;
    void** rdx211;
    void** rsi212;
    void** tmp64_213;
    uint1_t cf214;
    void** rax215;
    void** rdi216;
    void** r8_217;
    uint64_t rax218;
    int64_t rax219;
    void** r13_220;
    void* rax221;
    void* rdx222;
    void** rdx223;
    void* rax224;
    signed char al225;
    void** rdx226;
    void** rsi227;
    void** rdi228;
    unsigned char al229;
    void** rax230;
    void* rsp231;
    void** rax232;
    void* rsp233;
    void** rax234;
    uint64_t rax235;
    struct s15* rax236;
    void* rax237;
    void* rdx238;
    void* rax239;
    unsigned char al240;
    void** r8_241;
    void** r9_242;
    void** v243;
    void** rbp244;
    void** v245;
    void*** rsp246;
    void** r12_247;
    int64_t r13_248;
    void** r11_249;
    void** r10_250;
    void*** r14_251;
    struct s16* rbx252;
    uint64_t r15_253;
    void** rcx254;
    void** rdx255;
    void** r10_256;
    void** r13_257;
    void* rax258;
    int32_t ecx259;
    int32_t ecx260;
    int32_t edx261;
    int64_t rbx262;
    void** rcx263;
    void** rdx264;
    struct s17* rcx265;
    void* rcx266;
    void** rcx267;
    void** rdx268;
    void** rdx269;
    struct s18* rcx270;
    struct s19* rsi271;
    void* rsi272;
    void* rcx273;
    struct s20* rsi274;
    void* rsi275;
    void** rcx276;
    void** rdx277;
    void** v278;
    void** rdx279;
    uint32_t ecx280;
    uint32_t edx281;
    void** rsi282;
    struct s21* rdi283;
    void* rdi284;
    void** rdx285;
    void** rdx286;
    uint32_t ecx287;
    uint32_t edx288;
    void** rsi289;
    struct s22* rdi290;
    void* rdi291;
    void** rdx292;
    void** rdx293;
    uint32_t ecx294;
    uint32_t edx295;
    void** rsi296;
    struct s23* rdi297;
    void* rdi298;
    void** rdx299;
    int64_t rax300;
    void* rax301;
    int64_t rax302;
    struct s24* rax303;
    uint32_t ecx304;
    uint32_t edx305;
    void** rsi306;
    struct s25* rdi307;
    void* rdi308;
    void** rdx309;
    struct s26* rcx310;
    struct s27* rsi311;
    void* rcx312;
    void* rsi313;
    void** rcx314;
    void** rdi315;
    void** v316;
    void** v317;
    unsigned char al318;
    int64_t rax319;
    struct s28* rax320;
    void* rax321;
    int64_t v322;
    signed char al323;
    unsigned char al324;
    void** r9_325;
    void** rsi326;
    void** rdi327;
    void** rdi328;
    void** rdx329;
    void** rsi330;
    int64_t rax331;
    void** r8_332;
    void*** r11_333;
    signed char* r10_334;
    uint32_t r9d335;
    int32_t ebx336;
    void* rcx337;
    void* rax338;
    int64_t rsi339;
    void* rax340;
    uint32_t r9d341;
    uint32_t eax342;
    void** r12_343;
    void** rcx344;
    int32_t esi345;
    void** rax346;
    void* rdx347;
    void* rdi348;
    uint1_t cf349;
    int64_t rbp350;
    int64_t r15_351;
    int64_t r14_352;
    void** rbp353;
    void** rcx354;
    void** r11_355;
    uint64_t rax356;
    int64_t rax357;
    void* rax358;
    void* rdx359;
    void* rax360;
    void** r8_361;
    void* r10_362;
    void* r9_363;
    void* rax364;
    void** r13_365;
    int64_t rax366;
    int64_t rax367;
    void** rdi368;
    void** rax369;
    int64_t rcx370;
    void* rdi371;
    void* rax372;
    void* r11_373;
    void* rsi374;
    void* rax375;
    void** rbx376;
    void** rsi377;
    void** rax378;
    void** rcx379;
    void** r11_380;
    void** rdx381;
    void** rax382;
    void** r8_383;
    signed char al384;
    void** rdx385;
    void** rsi386;
    void** rcx387;
    int1_t zf388;
    signed char al389;
    struct s0* r14_390;
    void** rbp391;
    struct s0** rsp392;
    void** rax393;
    void** v394;
    uint32_t eax395;
    struct s0* v396;
    uint32_t v397;
    void** rcx398;
    void** r15_399;
    uint64_t rax400;
    int32_t esi401;
    int64_t rax402;
    void* rax403;
    void* rdx404;
    void* rax405;
    void** rdx406;
    struct s0* r13_407;
    void** rax408;
    uint64_t rdi409;
    uint1_t cf410;
    int64_t rbx411;
    void** rbx412;
    unsigned char al413;
    void* v414;
    struct s0* v415;
    uint32_t eax416;
    unsigned char v417;
    uint32_t v418;
    int64_t rax419;
    void* v420;
    void** rax421;
    void* rax422;
    void** rax423;
    void** rax424;
    void** rdi425;
    void** r15_426;
    void** rdi427;
    int64_t rbp428;
    int64_t rbx429;
    void** r15_430;
    int32_t eax431;
    uint1_t zf432;
    void** rax433;
    void** v434;
    void** rax435;
    void** rax436;
    int64_t rax437;
    void* v438;
    void** rax439;
    void** r14_440;
    void** rdi441;
    void** rax442;
    void** rax443;
    void** rax444;
    void** v445;
    void** v446;
    void** rdx447;
    void** v448;
    void** rdi449;
    void*** v450;
    void** v451;
    void** rax452;
    void* rax453;
    void** rax454;
    void** rdx455;
    void** rcx456;
    int32_t esi457;
    void** rax458;
    uint64_t rdi459;
    uint1_t cf460;
    unsigned char al461;
    void* r11_462;
    uint64_t v463;
    void** rax464;
    void** rdx465;
    unsigned char al466;
    int64_t v467;
    void** rdx468;
    void*** v469;
    void** v470;
    void** rax471;

    while (1) {
        rbp7 = rdi;
        rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8);
        v9 = rsi;
        v10 = rdx;
        rax11 = g28;
        v12 = rax11;
        zf13 = dev_debug == 0;
        if (!zf13) {
            rdi14 = stderr;
            rcx = rsi;
            fun_2a90(rdi14, 1, "[pollard-rho (%lu)] ", rcx, r8, r9, v15, v9, v16, v17, v18, v19);
            rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
        }
        *reinterpret_cast<int32_t*>(&rdx20) = 0;
        *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
        r15_21 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp8) + 64);
        *reinterpret_cast<int32_t*>(&rbx22) = 1;
        *reinterpret_cast<int32_t*>(&rbx22 + 4) = 0;
        r14_23 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp8) + 0x90);
        r12_24 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp8) + 0x80);
        r13_25 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp8) + 0x70);
        fun_2780(r12_24, r14_23, r12_24, r14_23);
        rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
        rax27 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp26) + 96);
        v16 = rax27;
        fun_28b0(rax27, 2);
        fun_28b0(r15_21, 2);
        rsp28 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp26) - 8 + 8 - 8 + 8);
        rax29 = reinterpret_cast<void**>(rsp28 + 80);
        v15 = rax29;
        fun_28b0(rax29, 2);
        fun_29f0(r13_25, 1);
        rsp30 = reinterpret_cast<void*>(rsp28 - 8 + 8 - 8 + 8);
        v17 = reinterpret_cast<void**>(1);
        while (v31 = reinterpret_cast<void**>(0x3d89), eax32 = fun_2aa0(rbp7, 1, rdx20, rcx, r8, r9), rsp33 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp30) - 8 + 8), !!eax32) {
            r15_34 = rbx22;
            rbx35 = r15_21;
            while (1) {
                fun_27b0(r12_24, rbx35, rbx35, rcx, r8, r9);
                fun_29d0(rbx35, r12_24, rbp7, rcx, r8, r9);
                fun_28c0(rbx35, rbx35, v9, rcx, r8, r9);
                fun_26c0(r12_24, v15, rbx35, rcx, r8, r9);
                fun_27b0(r14_23, r13_25, r12_24, rcx, r8, r9);
                rdx36 = rbp7;
                fun_29d0(r13_25, r14_23, rdx36, rcx, r8, r9);
                rsp33 = rsp33 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
                rax37 = r15_34;
                *reinterpret_cast<uint32_t*>(&rax38) = *reinterpret_cast<uint32_t*>(&rax37) & 31;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax38) + 4) = 0;
                if (rax38 != 1) {
                    --r15_34;
                    if (r15_34) 
                        continue;
                } else {
                    rdx36 = rbp7;
                    fun_2a40(r12_24, r13_25, rdx36, rcx, r8, r9);
                    eax39 = fun_2aa0(r12_24, 1, rdx36, rcx, r8, r9);
                    rsp40 = rsp33 - 8 + 8 - 8 + 8;
                    if (eax39) 
                        break;
                    fun_27a0(v16, rbx35, rdx36, rcx, r8, v16, rbx35, rdx36, rcx, r8);
                    rsp33 = rsp40 - 8 + 8;
                    --r15_34;
                    if (r15_34) 
                        continue;
                }
                fun_27a0(v15, rbx35, rdx36, rcx, r8, v15, rbx35, rdx36, rcx, r8);
                rsp41 = reinterpret_cast<void*>(rsp33 - 8 + 8);
                rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v17) + reinterpret_cast<unsigned char>(v17));
                v42 = rcx;
                if (v17) {
                    v43 = r14_23;
                    r14_44 = rbx35;
                    rbx45 = v9;
                    v46 = r13_25;
                    r13_47 = r15_34;
                    r15_48 = v17;
                    do {
                        ++r13_47;
                        fun_27b0(r12_24, r14_44, r14_44, rcx, r8, r9);
                        fun_29d0(r14_44, r12_24, rbp7, rcx, r8, r9);
                        rdx36 = rbx45;
                        fun_28c0(r14_44, r14_44, rdx36, rcx, r8, r9);
                        rsp41 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp41) - 8 + 8 - 8 + 8 - 8 + 8);
                    } while (r15_48 != r13_47);
                    rbx35 = r14_44;
                    r13_25 = v46;
                    r14_23 = v43;
                }
                fun_27a0(v16, rbx35, rdx36, rcx, r8, v16, rbx35, rdx36, rcx, r8);
                rsp33 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp41) - 8 + 8);
                r15_34 = v17;
                v17 = v42;
            }
            v18 = r15_34;
            r15_21 = rbx35;
            rbx49 = v9;
            v19 = r13_25;
            r13_50 = v16;
            v51 = r14_23;
            r14_52 = r12_24;
            r12_53 = v15;
            do {
                fun_27b0(r14_52, r13_50, r13_50, rcx, r8, r9);
                fun_29d0(r13_50, r14_52, rbp7, rcx, r8, r9);
                fun_28c0(r13_50, r13_50, rbx49, rcx, r8, r9);
                fun_26c0(r14_52, r12_53, r13_50, rcx, r8, r9);
                fun_2a40(r14_52, r14_52, rbp7, rcx, r8, r9);
                eax54 = fun_2aa0(r14_52, 1, rbp7, rcx, r8, r9);
                rsp40 = rsp40 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
            } while (!eax54);
            v15 = r12_53;
            r12_24 = r14_52;
            rdx55 = r12_24;
            v9 = rbx49;
            r14_23 = v51;
            v16 = r13_50;
            rbx22 = v18;
            r13_25 = v19;
            fun_2760(rbp7, rbp7, rdx55, rcx, r8, r9);
            eax56 = fun_2aa0(r12_24, 1, rdx55, rcx, r8, r9);
            rsp57 = reinterpret_cast<void*>(rsp40 - 8 + 8 - 8 + 8);
            if (reinterpret_cast<uint1_t>(eax56 < 0) | reinterpret_cast<uint1_t>(eax56 == 0) || (eax58 = fun_2aa0(r12_24, 0x17ded79, rdx55, rcx, r8, r9), rsp57 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp57) - 8 + 8), eax58 >= 0) && (al59 = mp_prime_p_part_0(r12_24, 0x17ded79, rdx55, rcx, r8, r9), rsp57 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp57) - 8 + 8), !al59)) {
                zf60 = dev_debug == 0;
                if (!zf60) {
                    rcx = stderr;
                    fun_2a80("[composite factor--restarting pollard-rho] ", 1, 43, rcx, r8, r9);
                    rsp57 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp57) - 8 + 8);
                }
                rdx55 = v10;
                mp_factor_using_pollard_rho(r12_24, v9 + 1, rdx55, rcx, r8, r9);
                rsp61 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp57) - 8 + 8);
            } else {
                mp_factor_insert(v10, r12_24, rdx55, rcx, r8, r9);
                rsp61 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp57) - 8 + 8);
            }
            eax62 = fun_2aa0(rbp7, 1, rdx55, rcx, r8, r9);
            rsp63 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) - 8 + 8);
            if (reinterpret_cast<uint1_t>(eax62 < 0) | reinterpret_cast<uint1_t>(eax62 == 0)) 
                goto addr_401c_23;
            eax64 = fun_2aa0(rbp7, 0x17ded79, rdx55, rcx, r8, r9);
            rsp63 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp63) - 8 + 8);
            if (eax64 < 0) 
                goto addr_405d_25;
            al65 = mp_prime_p_part_0(rbp7, 0x17ded79, rdx55, rcx, r8, r9);
            rsp63 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp63) - 8 + 8);
            if (al65) 
                goto addr_405d_25;
            addr_401c_23:
            fun_29d0(r15_21, r15_21, rbp7, rcx, r8, r9);
            fun_29d0(v15, v15, rbp7, rcx, r8, r9);
            rdx20 = rbp7;
            fun_29d0(v16, v16, rdx20, rcx, r8, r9);
            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp63) - 8 + 8 - 8 + 8 - 8 + 8);
        }
        addr_406a_27:
        r8_66 = r15_21;
        r9_67 = v16;
        rsi68 = r14_23;
        rdi69 = r13_25;
        rcx70 = v15;
        fun_2660();
        rdx71 = v31;
        rax72 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v12) - reinterpret_cast<unsigned char>(g28));
        if (!rax72) 
            break;
        fun_2740();
        rsp73 = reinterpret_cast<void*>(rsp33 - 8 - 8 - 8 + 8 + 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 72);
        rax74 = g28;
        v75 = rax74;
        if (!v76) 
            goto addr_413c_30;
        zf77 = dev_debug == 0;
        rbp7 = rdi69;
        r12_24 = rsi68;
        if (!zf77) {
            rcx70 = stderr;
            *reinterpret_cast<int32_t*>(&rdx71) = 17;
            *reinterpret_cast<int32_t*>(&rdx71 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rsi68) = 1;
            *reinterpret_cast<int32_t*>(&rsi68 + 4) = 0;
            fun_2a80("[trial division] ", 1, 17, rcx70, r8_66, r9_67);
            rsp73 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp73) - 8 + 8);
        }
        rdi78 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp73) + 16);
        r13_79 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp73) + 32);
        v80 = rdi78;
        fun_2ab0(rdi78, rsi68, rdx71, rcx70, r8_66, rdi78, rsi68, rdx71, rcx70, r8_66);
        rax81 = fun_25d0(rbp7, rbp7);
        rbx82 = rax81;
        rdx83 = rax81;
        fun_2840(rbp7, rbp7, rdx83, rcx70, r8_66, r9_67);
        rsp84 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp73) - 8 + 8 - 8 + 8 - 8 + 8);
        if (rbx82) {
            do {
                fun_29f0(r13_79, 2, r13_79, 2);
                mp_factor_insert(r12_24, r13_79, rdx83, rcx70, r8_66, r9_67);
                fun_2940(r13_79, r13_79, rdx83, rcx70, r8_66, r9_67);
                rsp84 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp84) - 8 + 8 - 8 + 8 - 8 + 8);
                --rbx82;
            } while (rbx82);
        }
        *reinterpret_cast<int32_t*>(&rbx22) = 1;
        *reinterpret_cast<int32_t*>(&rbx22 + 4) = 0;
        *reinterpret_cast<int32_t*>(&r15_21) = 3;
        *reinterpret_cast<int32_t*>(&r15_21 + 4) = 0;
        r13_25 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp84) + 32);
        while (1) {
            eax85 = fun_28f0(rbp7, r15_21, rdx83, rcx70, r8_66, r9_67);
            rsp86 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp84) - 8 + 8);
            if (!eax85) {
                do {
                    *reinterpret_cast<int32_t*>(&r14_23) = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbx22 + 1));
                    *reinterpret_cast<uint32_t*>(&rax87) = *reinterpret_cast<unsigned char*>(rbx22 + 0x10340);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax87) + 4) = 0;
                    r15_21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_21) + reinterpret_cast<uint64_t>(rax87));
                    rsi88 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_21) * reinterpret_cast<unsigned char>(r15_21));
                    eax89 = fun_2aa0(rbp7, rsi88, rdx83, rcx70, r8_66, r9_67);
                    rsp90 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp86) - 8 + 8);
                    if (eax89 < 0) 
                        goto addr_4268_39;
                    if (*reinterpret_cast<int32_t*>(&r14_23) == 0x29d) 
                        goto addr_4268_39;
                    *reinterpret_cast<int32_t*>(&rbx22) = *reinterpret_cast<int32_t*>(&r14_23);
                    *reinterpret_cast<int32_t*>(&rbx22 + 4) = 0;
                    eax91 = fun_28f0(rbp7, r15_21, rdx83, rcx70, r8_66, r9_67);
                    rsp86 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp90) - 8 + 8);
                } while (!eax91);
            }
            rdx83 = r15_21;
            fun_27e0(rbp7, rbp7, rdx83, rcx70, r8_66, r9_67);
            fun_29f0(r13_25, r15_21, r13_25, r15_21);
            mp_factor_insert(r12_24, r13_25, rdx83, rcx70, r8_66, r9_67);
            fun_2940(r13_25, r13_25, rdx83, rcx70, r8_66, r9_67);
            rsp84 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp86) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        }
        addr_4268_39:
        fun_2940(v80, rsi88, rdx83, rcx70, r8_66, r9_67);
        rdi69 = rbp7;
        eax92 = fun_2aa0(rdi69, 1, rdx83, rcx70, r8_66, r9_67);
        rsp73 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp90) - 8 + 8 - 8 + 8);
        if (!eax92) 
            goto addr_413c_30;
        zf93 = dev_debug == 0;
        if (!zf93) {
            rcx70 = stderr;
            *reinterpret_cast<int32_t*>(&rdx83) = 19;
            *reinterpret_cast<int32_t*>(&rdx83 + 4) = 0;
            fun_2a80("[is number prime?] ", 1, 19, rcx70, r8_66, r9_67);
            rdi69 = rbp7;
            eax92 = fun_2aa0(rdi69, 1, 19, rcx70, r8_66, r9_67);
            rsp73 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp73) - 8 + 8 - 8 + 8);
        }
        if (reinterpret_cast<uint1_t>(eax92 < 0) | reinterpret_cast<uint1_t>(eax92 == 0)) 
            goto addr_42b5_46;
        rdi69 = rbp7;
        eax94 = fun_2aa0(rdi69, 0x17ded79, rdx83, rcx70, r8_66, r9_67);
        rsp73 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp73) - 8 + 8);
        if (eax94 < 0) 
            goto addr_4318_48;
        rdi69 = rbp7;
        al95 = mp_prime_p_part_0(rdi69, 0x17ded79, rdx83, rcx70, r8_66, r9_67);
        rsp73 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp73) - 8 + 8);
        if (al95) 
            goto addr_4318_48;
        addr_42b5_46:
        rax96 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v75) - reinterpret_cast<unsigned char>(g28));
        if (rax96) 
            goto addr_4377_50;
        continue;
        addr_405d_25:
        v31 = reinterpret_cast<void**>(0x406a);
        mp_factor_insert(v10, rbp7, rdx55, rcx, r8, r9);
        rsp33 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp63) - 8 + 8);
        goto addr_406a_27;
    }
    return;
    addr_413c_30:
    rax97 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v75) - reinterpret_cast<unsigned char>(g28));
    if (!rax97) {
        goto v15;
    }
    addr_4318_48:
    rax98 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v75) - reinterpret_cast<unsigned char>(g28));
    if (rax98) {
        addr_4377_50:
        fun_2740();
    } else {
        r12_99 = rbp7;
        rdx100 = reinterpret_cast<void**>(0);
        v101 = 0;
        if (1) 
            goto addr_37ca_56; else 
            goto addr_361b_57;
    }
    rbx102 = rdi69;
    rsp103 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp73) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x98);
    rax104 = g28;
    v105 = rax104;
    r12_106 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp103) + 96);
    r15_107 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp103) + 64);
    rbp108 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp103) + 80);
    r14_109 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp103) + 0x70);
    v110 = r15_107;
    fun_2780(r15_107, rbp108, r15_107, rbp108);
    fun_2650(r12_106, rbx102, 1, r14_109);
    rax111 = fun_25d0(r12_106);
    r13_112 = rax111;
    v113 = rax111;
    fun_2960(r15_107, r12_106, rax111, r14_109);
    fun_2680(rbp108, 2, rax111, r14_109);
    r9_114 = r13_112;
    r8_115 = r15_107;
    rcx116 = r14_109;
    al117 = mp_millerrabin(rbx102, r12_106, rbp108, rcx116, r8_115, r9_114);
    rsp118 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp103) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    if (al117) {
        r13_112 = reinterpret_cast<void**>(0x10340);
        fun_27a0(r14_109, r12_106, rbp108, rcx116, r8_115, r14_109, r12_106, rbp108, rcx116, r8_115);
        rsp119 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp118) - 8 + 8);
        rsi120 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp119) + 32);
        mp_factor(r14_109, rsi120, rbp108, rcx116, r8_115, r9_114);
        rsp121 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp119) - 8 + 8);
        goto addr_4458_60;
    }
    addr_455c_61:
    rdi122 = v110;
    rcx123 = r14_109;
    rdx124 = r12_106;
    rsi125 = rbp108;
    fun_2660();
    rax126 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v105) - reinterpret_cast<unsigned char>(g28));
    if (!rax126) {
        goto v127;
    }
    fun_2740();
    rsp128 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp118) - 8 + 8 - 8 + 8);
    while (1) {
        r15_129 = rdi122;
        rbp130 = rsi125;
        rsp131 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp128) - 8 - 8 - 8 - 8 - 8 - 8 - 0x88);
        v132 = rdx124;
        v133 = rcx123;
        rax134 = g28;
        v135 = rax134;
        rcx136 = reinterpret_cast<unsigned char>(rcx123) - (reinterpret_cast<unsigned char>(rcx123) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx123) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx123) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi122) < reinterpret_cast<unsigned char>(2)))))));
        *reinterpret_cast<int32_t*>(&rdx137) = 0;
        *reinterpret_cast<int32_t*>(&rdx137 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx138) = *reinterpret_cast<uint32_t*>(&rcx136) & 64;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx138) + 4) = 0;
        rcx139 = reinterpret_cast<uint64_t>(rcx138 + 63);
        cf140 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi122) < reinterpret_cast<unsigned char>(2));
        *reinterpret_cast<unsigned char*>(&rdx137) = cf140;
        rax141 = reinterpret_cast<void**>(-static_cast<uint64_t>(reinterpret_cast<uint1_t>(0 < static_cast<uint64_t>(cf140))) + 1);
        do {
            rsi142 = rdx137;
            rdx137 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx137) + reinterpret_cast<unsigned char>(rdx137));
            rax141 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax141) + reinterpret_cast<unsigned char>(rax141)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rsi142) >> 63));
            if (reinterpret_cast<unsigned char>(r15_129) < reinterpret_cast<unsigned char>(rax141) || r15_129 == rax141 && reinterpret_cast<unsigned char>(rbp130) <= reinterpret_cast<unsigned char>(rdx137)) {
                cf143 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx137) < reinterpret_cast<unsigned char>(rbp130));
                rdx137 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx137) - reinterpret_cast<unsigned char>(rbp130));
                rax141 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax141) - (reinterpret_cast<unsigned char>(r15_129) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax141) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r15_129) + static_cast<uint64_t>(cf143))))));
            }
            cf144 = rcx139 < 1;
            --rcx139;
        } while (!cf144);
        v145 = rdx137;
        v146 = rax141;
        tmp64_147 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx137) + reinterpret_cast<unsigned char>(rdx137));
        rax148 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax141) + reinterpret_cast<unsigned char>(rax141) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_147) < reinterpret_cast<unsigned char>(rdx137))));
        r12_149 = tmp64_147;
        r10_150 = rax148;
        if (reinterpret_cast<unsigned char>(rax148) > reinterpret_cast<unsigned char>(r15_129) || rax148 == r15_129 && reinterpret_cast<unsigned char>(tmp64_147) >= reinterpret_cast<unsigned char>(rbp130)) {
            r12_149 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_147) - reinterpret_cast<unsigned char>(rbp130));
            r10_150 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax148) - (reinterpret_cast<unsigned char>(r15_129) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax148) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r15_129) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_147) < reinterpret_cast<unsigned char>(rbp130))))))));
        }
        v151 = r10_150;
        v152 = r10_150;
        if (r15_129) 
            goto addr_468c_71;
        if (rbp130 == 1) 
            goto addr_4ad5_73;
        addr_468c_71:
        v153 = r12_149;
        r13_112 = r15_129;
        *reinterpret_cast<int32_t*>(&rbx102) = 1;
        *reinterpret_cast<int32_t*>(&rbx102 + 4) = 0;
        v154 = reinterpret_cast<void**>(1);
        r14_155 = reinterpret_cast<void**>(rsp131 + 0x70);
        r15_156 = r12_149;
        v157 = rsp131 + 0x68;
        while (1) {
            r9_158 = r13_112;
            r13_112 = rbx102;
            rax159 = reinterpret_cast<unsigned char>(rbp130) >> 1;
            rbx102 = rbp130;
            *reinterpret_cast<uint32_t*>(&rax160) = *reinterpret_cast<uint32_t*>(&rax159) & 0x7f;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax160) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rax161) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax160);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax161) + 4) = 0;
            rdx162 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax161) + reinterpret_cast<int64_t>(rax161) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax161) * reinterpret_cast<int64_t>(rax161) * reinterpret_cast<unsigned char>(rbp130)));
            rax163 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx162) + reinterpret_cast<uint64_t>(rdx162) - reinterpret_cast<uint64_t>(rdx162) * reinterpret_cast<uint64_t>(rdx162) * reinterpret_cast<unsigned char>(rbp130));
            rax164 = rbp130;
            rbp165 = r10_150;
            *reinterpret_cast<uint32_t*>(&rax166) = *reinterpret_cast<uint32_t*>(&rax164) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax166) + 4) = 0;
            v167 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax163) + reinterpret_cast<uint64_t>(rax163) - reinterpret_cast<uint64_t>(rax163) * reinterpret_cast<uint64_t>(rax163) * reinterpret_cast<unsigned char>(rbp130));
            v168 = rax166;
            while (1) {
                rax169 = mulredc2(r14_155, rbp165, r12_149, rbp165, r12_149, r9_158, rbx102, v146);
                tmp64_170 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax169) + reinterpret_cast<unsigned char>(v132));
                rdx171 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v172) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_170) < reinterpret_cast<unsigned char>(rax169))));
                v173 = rdx171;
                r12_149 = tmp64_170;
                rbp165 = rdx171;
                if (reinterpret_cast<unsigned char>(rdx171) > reinterpret_cast<unsigned char>(r9_158) || rdx171 == r9_158 && reinterpret_cast<unsigned char>(tmp64_170) >= reinterpret_cast<unsigned char>(rbx102)) {
                    rdx174 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx171) - (reinterpret_cast<unsigned char>(r9_158) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx171) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r9_158) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_170) < reinterpret_cast<unsigned char>(rbx102))))))));
                    v173 = rdx174;
                    r12_149 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_170) - reinterpret_cast<unsigned char>(rbx102));
                    rbp165 = rdx174;
                }
                r8_175 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v153) - reinterpret_cast<unsigned char>(r12_149));
                rcx176 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v151) - (reinterpret_cast<unsigned char>(rbp165) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v151) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp165) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v153) < reinterpret_cast<unsigned char>(r12_149))))))));
                if (reinterpret_cast<signed char>(rcx176) < reinterpret_cast<signed char>(0)) {
                    tmp64_177 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_175) + reinterpret_cast<unsigned char>(rbx102));
                    cf178 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_177) < reinterpret_cast<unsigned char>(r8_175));
                    r8_175 = tmp64_177;
                    rcx176 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx176) + reinterpret_cast<unsigned char>(r9_158) + static_cast<uint64_t>(cf178));
                }
                rax179 = mulredc2(r14_155, v146, v145, rcx176, r8_175, r9_158, rbx102, v146);
                v145 = rax179;
                v146 = v180;
                rax181 = r13_112;
                *reinterpret_cast<uint32_t*>(&rax182) = *reinterpret_cast<uint32_t*>(&rax181) & 31;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax182) + 4) = 0;
                rsp131 = rsp131 - 8 - 8 - 8 + 8 + 8 + 8 - 8 - 8 - 8 + 8 + 8 + 8;
                r9_158 = r9_158;
                if (rax182 == 1) {
                    if (!v168) 
                        goto addr_4cba_81;
                    if (reinterpret_cast<unsigned char>(v145) | reinterpret_cast<unsigned char>(v146)) 
                        goto addr_48c0_83;
                } else {
                    addr_47d9_84:
                    --r13_112;
                    if (r13_112) 
                        continue; else 
                        goto addr_47e3_85;
                }
                v183 = r9_158;
                rax184 = rbx102;
                if (r9_158) 
                    break;
                addr_48ee_87:
                if (!reinterpret_cast<int1_t>(rax184 == 1)) 
                    goto addr_4920_88;
                v152 = rbp165;
                r15_156 = r12_149;
                goto addr_47d9_84;
                addr_48c0_83:
                rax184 = gcd2_odd_part_0(v157, v146, v145, r9_158, rbx102, r9_158);
                rsp131 = rsp131 - 8 + 8;
                r9_158 = r9_158;
                if (v183) 
                    goto addr_4920_88; else 
                    goto addr_48ee_87;
                addr_47e3_85:
                v151 = rbp165;
                r15_156 = r12_149;
                v185 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v154) + reinterpret_cast<unsigned char>(v154));
                if (v154) {
                    v186 = r12_149;
                    rdx187 = r12_149;
                    r15_188 = r13_112;
                    r12_189 = v167;
                    r13_190 = v132;
                    rsi191 = rbp165;
                    rbp192 = r9_158;
                    do {
                        rax193 = mulredc2(r14_155, rsi191, rdx187, rsi191, rdx187, rbp192, rbx102, r12_189);
                        tmp64_194 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax193) + reinterpret_cast<unsigned char>(r13_190));
                        rcx195 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v196) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_194) < reinterpret_cast<unsigned char>(rax193))));
                        rdx187 = tmp64_194;
                        rsi191 = rcx195;
                        rsp131 = rsp131 - 8 - 8 - 8 + 8 + 8 + 8;
                        if (reinterpret_cast<unsigned char>(rcx195) > reinterpret_cast<unsigned char>(rbp192) || rcx195 == rbp192 && reinterpret_cast<unsigned char>(tmp64_194) >= reinterpret_cast<unsigned char>(rbx102)) {
                            rdx187 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_194) - reinterpret_cast<unsigned char>(rbx102));
                            rsi191 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx195) - (reinterpret_cast<unsigned char>(rbp192) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx195) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp192) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_194) < reinterpret_cast<unsigned char>(rbx102))))))));
                        }
                        ++r15_188;
                    } while (v154 != r15_188);
                    r12_149 = v186;
                    r9_158 = rbp192;
                    r15_156 = rdx187;
                    rbp165 = rsi191;
                }
                r13_112 = v154;
                v153 = r12_149;
                r12_149 = r15_156;
                v152 = rbp165;
                v154 = v185;
            }
            addr_4920_88:
            v197 = r12_149;
            r11_198 = r15_156;
            r10_199 = v152;
            v200 = r13_112;
            r15_201 = r14_155;
            r13_112 = rbx102;
            r12_202 = v153;
            r14_155 = v167;
            rbp203 = v157;
            rbx102 = r9_158;
            do {
                r9_204 = rbx102;
                rax205 = mulredc2(r15_201, r10_199, r11_198, r10_199, r11_198, r9_204, r13_112, r14_155);
                tmp64_206 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax205) + reinterpret_cast<unsigned char>(v132));
                rdx207 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v208) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_206) < reinterpret_cast<unsigned char>(rax205))));
                v152 = rdx207;
                r11_198 = tmp64_206;
                rcx209 = r13_112;
                r10_199 = rdx207;
                rsp131 = rsp131 - 8 - 8 - 8 + 8 + 8 + 8;
                if (reinterpret_cast<unsigned char>(rdx207) > reinterpret_cast<unsigned char>(rbx102) || rdx207 == rbx102 && reinterpret_cast<unsigned char>(tmp64_206) >= reinterpret_cast<unsigned char>(r13_112)) {
                    rdx210 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx207) - (reinterpret_cast<unsigned char>(rbx102) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx207) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbx102) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_206) < reinterpret_cast<unsigned char>(r13_112))))))));
                    v152 = rdx210;
                    r11_198 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_206) - reinterpret_cast<unsigned char>(r13_112));
                    r10_199 = rdx210;
                }
                rdx211 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_202) - reinterpret_cast<unsigned char>(r11_198));
                rsi212 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v151) - (reinterpret_cast<unsigned char>(r10_199) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v151) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r10_199) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_202) < reinterpret_cast<unsigned char>(r11_198))))))));
                if (reinterpret_cast<signed char>(rsi212) < reinterpret_cast<signed char>(0)) {
                    tmp64_213 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx211) + reinterpret_cast<unsigned char>(r13_112));
                    cf214 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_213) < reinterpret_cast<unsigned char>(rdx211));
                    rdx211 = tmp64_213;
                    rsi212 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi212) + reinterpret_cast<unsigned char>(rbx102) + static_cast<uint64_t>(cf214));
                }
                if (reinterpret_cast<unsigned char>(rsi212) | reinterpret_cast<unsigned char>(rdx211)) {
                    rcx209 = rbx102;
                    rax215 = gcd2_odd_part_0(rbp203, rsi212, rdx211, rcx209, r13_112, r9_204);
                    rsp131 = rsp131 - 8 + 8;
                    rdi216 = v183;
                    if (rdi216) 
                        goto addr_4a05_103;
                } else {
                    rdi216 = rbx102;
                    v183 = rbx102;
                    rax215 = r13_112;
                    if (rdi216) 
                        goto addr_4a05_103;
                }
            } while (reinterpret_cast<int1_t>(rax215 == 1));
            r8_217 = rax215;
            rax218 = reinterpret_cast<unsigned char>(rax215) >> 1;
            *reinterpret_cast<uint32_t*>(&rax219) = *reinterpret_cast<uint32_t*>(&rax218) & 0x7f;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax219) + 4) = 0;
            r13_220 = rbx102;
            r14_155 = r15_201;
            *reinterpret_cast<uint32_t*>(&rax221) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax219);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax221) + 4) = 0;
            rbx102 = v200;
            rdx222 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax221) + reinterpret_cast<int64_t>(rax221) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax221) * reinterpret_cast<int64_t>(rax221) * reinterpret_cast<unsigned char>(r8_217)));
            rdx223 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdx222) * reinterpret_cast<uint64_t>(rdx222) * reinterpret_cast<unsigned char>(r8_217));
            rax224 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx222) + reinterpret_cast<uint64_t>(rdx222) - reinterpret_cast<unsigned char>(rdx223));
            rcx209 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax224) + reinterpret_cast<uint64_t>(rax224) - reinterpret_cast<uint64_t>(rax224) * reinterpret_cast<uint64_t>(rax224) * reinterpret_cast<unsigned char>(r8_217));
            rbp130 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_112) * reinterpret_cast<unsigned char>(rcx209));
            if (reinterpret_cast<unsigned char>(r13_220) < reinterpret_cast<unsigned char>(r8_217)) {
                *reinterpret_cast<int32_t*>(&r13_112) = 0;
                *reinterpret_cast<int32_t*>(&r13_112 + 4) = 0;
            } else {
                rdx223 = __intrinsic();
                r9_204 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r13_220) - reinterpret_cast<unsigned char>(rdx223)) * reinterpret_cast<unsigned char>(rcx209));
                r13_112 = r9_204;
            }
            if (reinterpret_cast<unsigned char>(r8_217) <= reinterpret_cast<unsigned char>(1) || reinterpret_cast<unsigned char>(r8_217) > reinterpret_cast<unsigned char>(0x17ded78) && (al225 = prime_p_part_0(r8_217, rsi212, rdx223, rcx209), rsp131 = rsp131 - 8 + 8, r8_217 = r8_217, al225 == 0)) {
                rdx226 = v133;
                rsi227 = v132 + 1;
                factor_using_pollard_rho(r8_217, rsi227, rdx226, rcx209);
                rsp131 = rsp131 - 8 + 8;
            } else {
                *reinterpret_cast<int32_t*>(&rdx226) = 1;
                *reinterpret_cast<int32_t*>(&rdx226 + 4) = 0;
                rsi227 = r8_217;
                factor_insert_multiplicity(v133, rsi227, 1);
                rsp131 = rsp131 - 8 + 8;
            }
            if (!r13_112) 
                goto addr_4aa0_113;
            rsi227 = rbp130;
            rdi228 = r13_112;
            al229 = prime2_p(rdi228, rsi227);
            rsp131 = rsp131 - 8 + 8;
            if (al229) 
                goto addr_4c8b_115;
            rax230 = mod2(rsp131 + 80, v173, v197, r13_112, rbp130, r9_204);
            rsp231 = reinterpret_cast<void*>(rsp131 - 8 + 8);
            r12_149 = rax230;
            rax232 = mod2(reinterpret_cast<int64_t>(rsp231) + 88, v151, v153, r13_112, rbp130, r9_204);
            rsp233 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp231) - 8 + 8);
            v153 = rax232;
            rax234 = mod2(reinterpret_cast<int64_t>(rsp233) + 96, v152, r11_198, r13_112, rbp130, r9_204);
            rsp131 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp233) - 8 + 8);
            r10_150 = v173;
            r15_156 = rax234;
        }
        addr_4a05_103:
        if (rbx102 != rdi216) 
            goto addr_4a19_117;
        if (rax215 == r13_112) 
            goto addr_4ce0_119;
        addr_4a19_117:
        rbx102 = reinterpret_cast<void**>(0xd5c0);
        rsi227 = rax215;
        v154 = rax215;
        rax235 = reinterpret_cast<unsigned char>(rax215) >> 1;
        *reinterpret_cast<uint32_t*>(&rax236) = *reinterpret_cast<uint32_t*>(&rax235) & 0x7f;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax236) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax237) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(0xd5c0) + reinterpret_cast<uint64_t>(rax236));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax237) + 4) = 0;
        rdx238 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax237) + reinterpret_cast<int64_t>(rax237) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax237) * reinterpret_cast<int64_t>(rax237) * reinterpret_cast<unsigned char>(rax215)));
        rax239 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx238) + reinterpret_cast<uint64_t>(rdx238) - reinterpret_cast<uint64_t>(rdx238) * reinterpret_cast<uint64_t>(rdx238) * reinterpret_cast<unsigned char>(rax215));
        rdx226 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax239) + reinterpret_cast<uint64_t>(rax239) - reinterpret_cast<uint64_t>(rax239) * reinterpret_cast<uint64_t>(rax239) * reinterpret_cast<unsigned char>(rax215));
        rbp130 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_112) * reinterpret_cast<unsigned char>(rdx226));
        al240 = prime2_p(rdi216, rsi227);
        rsp131 = rsp131 - 8 + 8;
        if (!al240) 
            goto addr_4c4f_120;
        if (!v183) 
            goto addr_4ca3_122;
        rdi228 = v133;
        if (!*reinterpret_cast<void***>(rdi228 + 8)) 
            goto addr_4a94_124;
        addr_4cfe_125:
        factor_insert_large_part_0();
        r8_241 = rdi228;
        r9_242 = rdx226;
        v243 = r13_112;
        rbp244 = rsi227;
        v245 = rbx102;
        rsp246 = rsp131 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 56;
        *reinterpret_cast<unsigned char*>(rdx226 + 0xfa) = 0;
        *reinterpret_cast<void***>(rdx226 + 8) = reinterpret_cast<void**>(0);
        if (rdi228) 
            goto addr_4d50_127;
        if (reinterpret_cast<unsigned char>(rsi227) <= reinterpret_cast<unsigned char>(1)) 
            goto addr_4d41_129;
        addr_4d50_127:
        if (*reinterpret_cast<unsigned char*>(&rbp244) & 1) {
            addr_4df3_130:
            if (!r8_241) {
                *reinterpret_cast<int32_t*>(&r12_247) = 3;
                *reinterpret_cast<int32_t*>(&r12_247 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r13_248) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_248) + 4) = 0;
                r11_249 = reinterpret_cast<void**>(0x5555555555555555);
                r10_250 = reinterpret_cast<void**>(0xaaaaaaaaaaaaaaab);
            } else {
                r14_251 = reinterpret_cast<void***>(0xd648);
                *reinterpret_cast<uint32_t*>(&rbx252) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx252) + 4) = 0;
                r15_253 = 0xaaaaaaaaaaaaaaab;
                *reinterpret_cast<int32_t*>(&r12_247) = 3;
                *reinterpret_cast<int32_t*>(&r12_247 + 4) = 0;
                while (1) {
                    rcx254 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp244) * r15_253);
                    rdx255 = __intrinsic();
                    if (reinterpret_cast<unsigned char>(rdx255) <= reinterpret_cast<unsigned char>(r8_241)) {
                        r10_256 = *r14_251;
                        while ((r13_257 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(r8_241) - reinterpret_cast<unsigned char>(rdx255)) * r15_253), reinterpret_cast<unsigned char>(r13_257) <= reinterpret_cast<unsigned char>(r10_256)) && (factor_insert_multiplicity(r9_242, r12_247, 1), rsp246 = rsp246 - 8 + 8, r8_241 = r13_257, r9_242 = r9_242, r10_256 = r10_256, rbp244 = rcx254, rdx255 = __intrinsic(), reinterpret_cast<unsigned char>(rdx255) <= reinterpret_cast<unsigned char>(r13_257))) {
                            rcx254 = reinterpret_cast<void**>(r15_253 * reinterpret_cast<unsigned char>(rcx254));
                        }
                    }
                    *reinterpret_cast<uint32_t*>(&rax258) = rbx252->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax258) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&r13_248) = *reinterpret_cast<uint32_t*>(&rbx252);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_248) + 4) = 0;
                    r14_251 = r14_251 + 16;
                    r12_247 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_247) + reinterpret_cast<uint64_t>(rax258));
                    if (!r8_241) 
                        break;
                    if (*reinterpret_cast<uint32_t*>(&rbx252) > 0x29b) 
                        break;
                    r15_253 = *reinterpret_cast<uint64_t*>(r14_251 - 8);
                    rbx252 = reinterpret_cast<struct s16*>(&rbx252->pad8);
                }
                if (*reinterpret_cast<uint32_t*>(&r13_248) > 0x29b) 
                    goto addr_5200_141; else 
                    goto addr_4edd_142;
            }
        } else {
            if (rbp244) {
                __asm__("bsf rdx, rbp");
                ecx259 = 64 - *reinterpret_cast<int32_t*>(&rdx226);
                ecx260 = *reinterpret_cast<int32_t*>(&rdx226);
                rbp244 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp244) >> *reinterpret_cast<signed char*>(&ecx260)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_241) << *reinterpret_cast<unsigned char*>(&ecx259)));
                factor_insert_multiplicity(r9_242, 2, *reinterpret_cast<signed char*>(&rdx226));
                rsp246 = rsp246 - 8 + 8;
                r8_241 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_241) >> *reinterpret_cast<signed char*>(&ecx260));
                r9_242 = r9_242;
                goto addr_4df3_130;
            } else {
                __asm__("bsf rcx, r8");
                edx261 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rcx209 + 64));
                rbp244 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_241) >> *reinterpret_cast<signed char*>(&rcx209));
                *reinterpret_cast<int32_t*>(&r12_247) = 3;
                *reinterpret_cast<int32_t*>(&r12_247 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r13_248) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_248) + 4) = 0;
                factor_insert_multiplicity(r9_242, 2, *reinterpret_cast<signed char*>(&edx261));
                rsp246 = rsp246 - 8 + 8;
                r9_242 = r9_242;
                *reinterpret_cast<uint32_t*>(&r8_241) = 0;
                *reinterpret_cast<int32_t*>(&r8_241 + 4) = 0;
                r11_249 = reinterpret_cast<void**>(0x5555555555555555);
                r10_250 = reinterpret_cast<void**>(0xaaaaaaaaaaaaaaab);
            }
        }
        addr_4ef9_146:
        *reinterpret_cast<int32_t*>(&rbx262) = static_cast<int32_t>(r13_248 + 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx262) + 4) = 0;
        r13_248 = rbx262;
        rbx252 = reinterpret_cast<struct s16*>((rbx262 << 4) + reinterpret_cast<unsigned char>(0xd640));
        while (1) {
            *reinterpret_cast<int32_t*>(&r15_253) = static_cast<int32_t>(r13_248 - 1);
            rcx263 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp244) * reinterpret_cast<unsigned char>(r10_250));
            if (reinterpret_cast<unsigned char>(r11_249) >= reinterpret_cast<unsigned char>(rcx263)) {
                do {
                    factor_insert_multiplicity(r9_242, r12_247, 1);
                    rsp246 = rsp246 - 8 + 8;
                    r10_250 = r10_250;
                    r11_249 = r11_249;
                    r9_242 = r9_242;
                    rbp244 = rcx263;
                    rcx263 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx263) * reinterpret_cast<unsigned char>(r10_250));
                    r8_241 = r8_241;
                } while (reinterpret_cast<unsigned char>(r11_249) >= reinterpret_cast<unsigned char>(rcx263));
                rdx264 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx252->f0) * reinterpret_cast<unsigned char>(rbp244));
                if (reinterpret_cast<unsigned char>(rbx252->f8) < reinterpret_cast<unsigned char>(rdx264)) 
                    goto addr_4f29_150;
                goto addr_5050_152;
            }
            rdx264 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx252->f0) * reinterpret_cast<unsigned char>(rbp244));
            if (reinterpret_cast<unsigned char>(rbx252->f8) >= reinterpret_cast<unsigned char>(rdx264)) {
                addr_5050_152:
                *reinterpret_cast<uint32_t*>(&rcx265) = *reinterpret_cast<uint32_t*>(&r13_248);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx265) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rcx266) = rcx265->f10340;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx266) + 4) = 0;
                rcx267 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx266) + reinterpret_cast<unsigned char>(r12_247));
            } else {
                addr_4f29_150:
                rdx268 = reinterpret_cast<void**>(rbx252->f10 * reinterpret_cast<unsigned char>(rbp244));
                if (reinterpret_cast<unsigned char>(rdx268) <= reinterpret_cast<unsigned char>(rbx252->f18)) 
                    goto addr_50b0_154; else 
                    goto addr_4f3b_155;
            }
            do {
                rbp244 = rdx264;
                factor_insert_multiplicity(r9_242, rcx267, 1);
                rsp246 = rsp246 - 8 + 8;
                r9_242 = r9_242;
                rcx267 = rcx267;
                r8_241 = r8_241;
                rdx264 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx252->f0) * reinterpret_cast<unsigned char>(rbp244));
            } while (reinterpret_cast<unsigned char>(rdx264) <= reinterpret_cast<unsigned char>(rbx252->f8));
            rdx268 = reinterpret_cast<void**>(rbx252->f10 * reinterpret_cast<unsigned char>(rbp244));
            if (reinterpret_cast<unsigned char>(rdx268) > reinterpret_cast<unsigned char>(rbx252->f18)) {
                addr_4f3b_155:
                rdx269 = reinterpret_cast<void**>(rbx252->f20 * reinterpret_cast<unsigned char>(rbp244));
                if (reinterpret_cast<unsigned char>(rdx269) <= reinterpret_cast<unsigned char>(rbx252->f28)) {
                    addr_5120_158:
                    *reinterpret_cast<int32_t*>(&rcx270) = static_cast<int32_t>(r13_248 + 2);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx270) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi271) = *reinterpret_cast<uint32_t*>(&r13_248);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi271) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi272) = rsi271->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi272) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rcx273) = rcx270->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx273) + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rsi274) = static_cast<int32_t>(r13_248 + 1);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi274) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi275) = rsi274->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi275) + 4) = 0;
                    rcx276 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx273) + reinterpret_cast<int64_t>(rsi272) + reinterpret_cast<unsigned char>(r12_247) + reinterpret_cast<uint64_t>(rsi275));
                } else {
                    addr_4f4d_159:
                    rdx277 = reinterpret_cast<void**>(rbx252->f30 * reinterpret_cast<unsigned char>(rbp244));
                    if (reinterpret_cast<unsigned char>(rdx277) <= reinterpret_cast<unsigned char>(rbx252->f38)) 
                        goto addr_5198_160; else 
                        goto addr_4f5f_161;
                }
            } else {
                goto addr_50b0_154;
            }
            do {
                rbp244 = rdx269;
                v278 = rcx276;
                factor_insert_multiplicity(r9_242, rcx276, 1);
                rsp246 = rsp246 - 8 + 8;
                r9_242 = r9_242;
                rcx276 = v278;
                r8_241 = r8_241;
                rdx269 = reinterpret_cast<void**>(rbx252->f20 * reinterpret_cast<unsigned char>(rbp244));
            } while (reinterpret_cast<unsigned char>(rdx269) <= reinterpret_cast<unsigned char>(rbx252->f28));
            rdx277 = reinterpret_cast<void**>(rbx252->f30 * reinterpret_cast<unsigned char>(rbp244));
            if (reinterpret_cast<unsigned char>(rdx277) > reinterpret_cast<unsigned char>(rbx252->f38)) {
                addr_4f5f_161:
                rdx279 = reinterpret_cast<void**>(rbx252->f40 * reinterpret_cast<unsigned char>(rbp244));
                if (reinterpret_cast<unsigned char>(rdx279) <= reinterpret_cast<unsigned char>(rbx252->f48)) {
                    rbp244 = rdx279;
                    ecx280 = static_cast<uint32_t>(r13_248 + 5);
                    while (1) {
                        edx281 = *reinterpret_cast<uint32_t*>(&r13_248);
                        rsi282 = r12_247;
                        do {
                            *reinterpret_cast<uint32_t*>(&rdi283) = edx281;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi283) + 4) = 0;
                            ++edx281;
                            *reinterpret_cast<uint32_t*>(&rdi284) = rdi283->f10340;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi284) + 4) = 0;
                            rsi282 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi282) + reinterpret_cast<uint64_t>(rdi284));
                        } while (edx281 != ecx280);
                        v278 = r8_241;
                        factor_insert_multiplicity(r9_242, rsi282, 1);
                        rsp246 = rsp246 - 8 + 8;
                        r9_242 = r9_242;
                        r8_241 = v278;
                        ecx280 = ecx280;
                        rdx285 = reinterpret_cast<void**>(rbx252->f40 * reinterpret_cast<unsigned char>(rbp244));
                        if (reinterpret_cast<unsigned char>(rdx285) > reinterpret_cast<unsigned char>(rbx252->f48)) 
                            break;
                        rbp244 = rdx285;
                    }
                }
            } else {
                goto addr_5198_160;
            }
            rdx286 = reinterpret_cast<void**>(rbx252->f50 * reinterpret_cast<unsigned char>(rbp244));
            if (reinterpret_cast<unsigned char>(rdx286) <= reinterpret_cast<unsigned char>(rbx252->f58)) {
                rbp244 = rdx286;
                ecx287 = static_cast<uint32_t>(r13_248 + 6);
                while (1) {
                    edx288 = *reinterpret_cast<uint32_t*>(&r13_248);
                    rsi289 = r12_247;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdi290) = edx288;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi290) + 4) = 0;
                        ++edx288;
                        *reinterpret_cast<uint32_t*>(&rdi291) = rdi290->f10340;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi291) + 4) = 0;
                        rsi289 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi289) + reinterpret_cast<uint64_t>(rdi291));
                    } while (edx288 != ecx287);
                    v278 = r8_241;
                    factor_insert_multiplicity(r9_242, rsi289, 1);
                    rsp246 = rsp246 - 8 + 8;
                    r9_242 = r9_242;
                    r8_241 = v278;
                    ecx287 = ecx287;
                    rdx292 = reinterpret_cast<void**>(rbx252->f50 * reinterpret_cast<unsigned char>(rbp244));
                    if (reinterpret_cast<unsigned char>(rdx292) > reinterpret_cast<unsigned char>(rbx252->f58)) 
                        break;
                    rbp244 = rdx292;
                }
            }
            rdx293 = reinterpret_cast<void**>(rbx252->f60 * reinterpret_cast<unsigned char>(rbp244));
            if (reinterpret_cast<unsigned char>(rdx293) <= reinterpret_cast<unsigned char>(rbx252->f68)) {
                rbp244 = rdx293;
                ecx294 = static_cast<uint32_t>(r13_248 + 7);
                while (1) {
                    edx295 = *reinterpret_cast<uint32_t*>(&r13_248);
                    rsi296 = r12_247;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdi297) = edx295;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi297) + 4) = 0;
                        ++edx295;
                        *reinterpret_cast<uint32_t*>(&rdi298) = rdi297->f10340;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi298) + 4) = 0;
                        rsi296 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi296) + reinterpret_cast<uint64_t>(rdi298));
                    } while (edx295 != ecx294);
                    v278 = r8_241;
                    factor_insert_multiplicity(r9_242, rsi296, 1);
                    rsp246 = rsp246 - 8 + 8;
                    r9_242 = r9_242;
                    r8_241 = v278;
                    ecx294 = ecx294;
                    rdx299 = reinterpret_cast<void**>(rbx252->f60 * reinterpret_cast<unsigned char>(rbp244));
                    if (reinterpret_cast<unsigned char>(rdx299) > reinterpret_cast<unsigned char>(rbx252->f68)) 
                        break;
                    rbp244 = rdx299;
                }
            }
            *reinterpret_cast<int32_t*>(&rax300) = *reinterpret_cast<int32_t*>(&r15_253);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax300) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rax301) = *reinterpret_cast<unsigned char*>(0x10080 + rax300);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax301) + 4) = 0;
            r12_247 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_247) + reinterpret_cast<uint64_t>(rax301));
            if (reinterpret_cast<unsigned char>(rbp244) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r12_247) * reinterpret_cast<unsigned char>(r12_247))) 
                break;
            *reinterpret_cast<uint32_t*>(&rax302) = static_cast<uint32_t>(r13_248 + 7);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax302) + 4) = 0;
            rbx252 = reinterpret_cast<struct s16*>(reinterpret_cast<uint64_t>(rbx252) + 0x80);
            *reinterpret_cast<uint32_t*>(&r13_248) = *reinterpret_cast<uint32_t*>(&r13_248) + 8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_248) + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&rax302) > 0x29b) 
                break;
            rax303 = reinterpret_cast<struct s24*>((rax302 << 4) + reinterpret_cast<unsigned char>(0xd640));
            r10_250 = rax303->f0;
            r11_249 = rax303->f8;
            continue;
            addr_5198_160:
            rbp244 = rdx277;
            ecx304 = static_cast<uint32_t>(r13_248 + 4);
            while (1) {
                edx305 = *reinterpret_cast<uint32_t*>(&r13_248);
                rsi306 = r12_247;
                do {
                    *reinterpret_cast<uint32_t*>(&rdi307) = edx305;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi307) + 4) = 0;
                    ++edx305;
                    *reinterpret_cast<uint32_t*>(&rdi308) = rdi307->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi308) + 4) = 0;
                    rsi306 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi306) + reinterpret_cast<uint64_t>(rdi308));
                } while (ecx304 != edx305);
                factor_insert_multiplicity(r9_242, rsi306, 1);
                rsp246 = rsp246 - 8 + 8;
                r9_242 = r9_242;
                r8_241 = r8_241;
                ecx304 = ecx304;
                rdx309 = reinterpret_cast<void**>(rbx252->f30 * reinterpret_cast<unsigned char>(rbp244));
                if (reinterpret_cast<unsigned char>(rdx309) > reinterpret_cast<unsigned char>(rbx252->f38)) 
                    goto addr_4f5f_161;
                rbp244 = rdx309;
            }
            addr_50b0_154:
            *reinterpret_cast<int32_t*>(&rcx310) = static_cast<int32_t>(r13_248 + 1);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx310) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rsi311) = *reinterpret_cast<uint32_t*>(&r13_248);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi311) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx312) = rcx310->f10340;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx312) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rsi313) = rsi311->f10340;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi313) + 4) = 0;
            rcx314 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx312) + reinterpret_cast<int64_t>(rsi313) + reinterpret_cast<unsigned char>(r12_247));
            do {
                rbp244 = rdx268;
                factor_insert_multiplicity(r9_242, rcx314, 1);
                rsp246 = rsp246 - 8 + 8;
                r9_242 = r9_242;
                rcx314 = rcx314;
                r8_241 = r8_241;
                rdx268 = reinterpret_cast<void**>(rbx252->f10 * reinterpret_cast<unsigned char>(rbp244));
            } while (reinterpret_cast<unsigned char>(rdx268) <= reinterpret_cast<unsigned char>(rbx252->f18));
            rdx269 = reinterpret_cast<void**>(rbx252->f20 * reinterpret_cast<unsigned char>(rbp244));
            if (reinterpret_cast<unsigned char>(rdx269) > reinterpret_cast<unsigned char>(rbx252->f28)) 
                goto addr_4f4d_159;
            goto addr_5120_158;
        }
        addr_5200_141:
        if (!r8_241) 
            goto addr_5209_193;
        rdi315 = r8_241;
        v316 = r9_242;
        v317 = r8_241;
        al318 = prime2_p(rdi315, rbp244);
        rsp246 = rsp246 - 8 + 8;
        if (al318) 
            goto addr_53a9_195;
        rsi125 = rbp244;
        rcx123 = v316;
        *reinterpret_cast<int32_t*>(&rdx124) = 1;
        *reinterpret_cast<int32_t*>(&rdx124 + 4) = 0;
        rbx102 = v245;
        rdi122 = v317;
        r13_112 = v243;
        rsp128 = reinterpret_cast<void*>(rsp246 + 56 + 8 + 8 + 8 + 8 + 8 + 8);
        continue;
        addr_4edd_142:
        *reinterpret_cast<uint32_t*>(&rax319) = *reinterpret_cast<uint32_t*>(&r13_248);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax319) + 4) = 0;
        rax320 = reinterpret_cast<struct s28*>((rax319 << 4) + 0xd640);
        r10_250 = rax320->f0;
        r11_249 = rax320->f8;
        goto addr_4ef9_146;
        addr_4c8b_115:
        if (*reinterpret_cast<void***>(v133 + 8)) 
            goto addr_4cfe_125; else 
            goto addr_4c97_197;
    }
    addr_4cba_81:
    fun_2810("b0 & 1", "src/factor.c", 0x1e0, "gcd2_odd", r8_175, r9_158);
    do {
        fun_2740();
        addr_4ce0_119:
        factor_using_pollard_rho2(rbx102, r13_112, v132 + 1, v133);
        addr_4ad5_73:
        rax321 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(g28));
    } while (rax321);
    goto v322;
    addr_4aa0_113:
    if (reinterpret_cast<unsigned char>(rbp130) <= reinterpret_cast<unsigned char>(1)) {
        addr_4c74_200:
        factor_using_pollard_rho(rbp130, v132, v133, rcx209);
        goto addr_4ad5_73;
    } else {
        addr_4aaa_201:
        if (reinterpret_cast<unsigned char>(rbp130) <= reinterpret_cast<unsigned char>(0x17ded78) || (al323 = prime_p_part_0(rbp130, rsi227, rdx226, rcx209), !!al323)) {
            factor_insert_multiplicity(v133, rbp130, 1, v133, rbp130, 1);
            goto addr_4ad5_73;
        }
    }
    addr_4c4f_120:
    rcx209 = v133;
    rsi227 = v154;
    rdx226 = v132 + 1;
    factor_using_pollard_rho2(v183, rsi227, rdx226, rcx209);
    if (reinterpret_cast<unsigned char>(rbp130) > reinterpret_cast<unsigned char>(1)) 
        goto addr_4aaa_201; else 
        goto addr_4c74_200;
    addr_4ca3_122:
    *reinterpret_cast<int32_t*>(&rdx226) = 1;
    *reinterpret_cast<int32_t*>(&rdx226 + 4) = 0;
    rsi227 = v154;
    factor_insert_multiplicity(v133, rsi227, 1);
    goto addr_4aa0_113;
    addr_4a94_124:
    *reinterpret_cast<void***>(v133) = v154;
    *reinterpret_cast<void***>(v133 + 8) = v183;
    goto addr_4aa0_113;
    addr_5209_193:
    if (reinterpret_cast<unsigned char>(rbp244) <= reinterpret_cast<unsigned char>(1)) {
        addr_4d41_129:
        goto v154;
    } else {
        al324 = prime2_p(0, rbp244);
        r9_325 = r9_242;
        if (al324) {
            rsi326 = rbp244;
            rdi327 = r9_325;
        } else {
            rdi328 = rbp244;
            rdx329 = r9_325;
            *reinterpret_cast<int32_t*>(&rsi330) = 1;
            *reinterpret_cast<int32_t*>(&rsi330 + 4) = 0;
            goto addr_5740_206;
        }
    }
    addr_2f20_207:
    *reinterpret_cast<uint32_t*>(&rax331) = *reinterpret_cast<unsigned char*>(rdi327 + 0xfa);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax331) + 4) = 0;
    r8_332 = rsi326;
    r11_333 = reinterpret_cast<void***>(rdi327 + 16);
    r10_334 = reinterpret_cast<signed char*>(rdi327 + 0xe0);
    r9d335 = *reinterpret_cast<uint32_t*>(&rax331);
    if (!*reinterpret_cast<uint32_t*>(&rax331)) 
        goto addr_2fa1_208;
    ebx336 = static_cast<int32_t>(rax331 - 1);
    rcx337 = reinterpret_cast<void*>(static_cast<int64_t>(ebx336));
    rax338 = rcx337;
    do {
        *reinterpret_cast<int32_t*>(&rsi339) = *reinterpret_cast<int32_t*>(&rax338);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi339) + 4) = 0;
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi327 + reinterpret_cast<uint64_t>(rax338) * 8) + 16)) <= reinterpret_cast<unsigned char>(r8_332)) 
            break;
        rax338 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax338) - 1);
        *reinterpret_cast<int32_t*>(&rsi339) = *reinterpret_cast<int32_t*>(&rax338);
    } while (*reinterpret_cast<int32_t*>(&rax338) != -1);
    goto addr_2f80_212;
    if (*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi327 + reinterpret_cast<uint64_t>(rax338) * 8) + 16) == r8_332) {
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_334) + reinterpret_cast<uint64_t>(rax338)) = reinterpret_cast<signed char>(*reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_334) + reinterpret_cast<uint64_t>(rax338)) + 1);
        goto v154;
    }
    rax340 = reinterpret_cast<void*>(static_cast<int64_t>(static_cast<int32_t>(rsi339 + 1)));
    r11_333 = r11_333 + reinterpret_cast<uint64_t>(rax340) * 8;
    r10_334 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_334) + reinterpret_cast<uint64_t>(rax340));
    if (*reinterpret_cast<int32_t*>(&rsi339) >= ebx336) {
        addr_2fa1_208:
        r9d341 = r9d335 + 1;
        *r11_333 = r8_332;
        *r10_334 = 1;
        *reinterpret_cast<unsigned char*>(rdi327 + 0xfa) = *reinterpret_cast<unsigned char*>(&r9d341);
        goto v154;
    }
    do {
        addr_2f80_212:
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi327 + reinterpret_cast<uint64_t>(rcx337) * 8) + 24) = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi327 + reinterpret_cast<uint64_t>(rcx337) * 8) + 16);
        eax342 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi327) + reinterpret_cast<uint64_t>(rcx337) + 0xe0);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi327) + reinterpret_cast<uint64_t>(rcx337) + 0xe1) = *reinterpret_cast<signed char*>(&eax342);
        rcx337 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx337) - 1);
    } while (*reinterpret_cast<int32_t*>(&rsi339) < *reinterpret_cast<int32_t*>(&rcx337));
    goto addr_2fa1_208;
    addr_5740_206:
    v316 = rsi330;
    v278 = rdx329;
    if (reinterpret_cast<unsigned char>(rdi328) <= reinterpret_cast<unsigned char>(1)) {
        addr_5b07_217:
        fun_2810("(1) < (n)", "src/factor.c", 0x5be, "factor_using_pollard_rho", r8_241, r9_325);
    } else {
        r12_343 = rdi328;
        do {
            rcx344 = r12_343;
            esi345 = 64;
            *reinterpret_cast<int32_t*>(&rax346) = 1;
            *reinterpret_cast<int32_t*>(&rax346 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx347) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx347) + 4) = 0;
            rdi348 = reinterpret_cast<void*>(0);
            do {
                r8_241 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx344) << 63);
                rcx344 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx344) >> 1);
                rdx347 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx347) >> 1 | reinterpret_cast<unsigned char>(r8_241));
                if (reinterpret_cast<unsigned char>(rcx344) < reinterpret_cast<unsigned char>(rax346) || rcx344 == rax346 && reinterpret_cast<uint64_t>(rdx347) <= reinterpret_cast<uint64_t>(rdi348)) {
                    cf349 = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi348) < reinterpret_cast<uint64_t>(rdx347));
                    rdi348 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi348) - reinterpret_cast<uint64_t>(rdx347));
                    rax346 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax346) - (reinterpret_cast<unsigned char>(rcx344) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax346) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx344) + static_cast<uint64_t>(cf349))))));
                }
                --esi345;
            } while (esi345);
            *reinterpret_cast<int32_t*>(&rbp350) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp350) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r15_351) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_351) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r14_352) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_352) + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rbp350) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_343) - reinterpret_cast<uint64_t>(rdi348) > reinterpret_cast<uint64_t>(rdi348));
            rbp353 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbp350) & reinterpret_cast<unsigned char>(r12_343)) + (reinterpret_cast<uint64_t>(rdi348) + reinterpret_cast<uint64_t>(rdi348) - reinterpret_cast<unsigned char>(r12_343)));
            rcx354 = rbp353;
            while (reinterpret_cast<unsigned char>(v316) < reinterpret_cast<unsigned char>(r12_343)) {
                r11_355 = rcx354;
                rax356 = reinterpret_cast<unsigned char>(r12_343) >> 1;
                *reinterpret_cast<uint32_t*>(&rax357) = *reinterpret_cast<uint32_t*>(&rax356) & 0x7f;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax357) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax358) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax357);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax358) + 4) = 0;
                rdx359 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax358) + reinterpret_cast<int64_t>(rax358) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax358) * reinterpret_cast<int64_t>(rax358) * reinterpret_cast<unsigned char>(r12_343)));
                rax360 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx359) + reinterpret_cast<uint64_t>(rdx359) - reinterpret_cast<uint64_t>(rdx359) * reinterpret_cast<uint64_t>(rdx359) * reinterpret_cast<unsigned char>(r12_343));
                r8_361 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax360) + reinterpret_cast<uint64_t>(rax360) - reinterpret_cast<uint64_t>(rax360) * reinterpret_cast<uint64_t>(rax360) * reinterpret_cast<unsigned char>(r12_343));
                r10_362 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_343) - reinterpret_cast<unsigned char>(v316));
                r9_363 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v316) - reinterpret_cast<unsigned char>(r12_343));
                while (1) {
                    rax364 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rax364 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax364) + reinterpret_cast<unsigned char>(r12_343));
                    }
                    rbp353 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rbp353) - (reinterpret_cast<unsigned char>(rbp353) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp353) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp353) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax364) < reinterpret_cast<uint64_t>(r10_362))))))) & reinterpret_cast<unsigned char>(r12_343)) + (reinterpret_cast<uint64_t>(rax364) + reinterpret_cast<uint64_t>(r9_363)));
                    r13_365 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        r13_365 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_365) + reinterpret_cast<unsigned char>(r12_343));
                    }
                    rax366 = r14_352;
                    *reinterpret_cast<uint32_t*>(&rax367) = *reinterpret_cast<uint32_t*>(&rax366) & 31;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax367) + 4) = 0;
                    if (rax367 == 1) {
                        rdi368 = r13_365;
                        rax369 = gcd_odd(rdi368, r12_343);
                        if (!reinterpret_cast<int1_t>(rax369 == 1)) 
                            break;
                    }
                    --r14_352;
                    if (r14_352) 
                        continue;
                    rcx370 = r15_351 + r15_351;
                    if (!r15_351) {
                        r15_351 = rcx370;
                        r11_355 = rbp353;
                    } else {
                        do {
                            rdi371 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                            rax372 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi371) + reinterpret_cast<unsigned char>(r12_343));
                            if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                                rdi371 = rax372;
                            }
                            ++r14_352;
                        } while (r15_351 != r14_352);
                        r11_355 = rbp353;
                        r15_351 = rcx370;
                        rbp353 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax372) - (reinterpret_cast<uint64_t>(rax372) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax372) < reinterpret_cast<uint64_t>(rax372) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi371) < reinterpret_cast<uint64_t>(r10_362)))) & reinterpret_cast<unsigned char>(r12_343)) + (reinterpret_cast<uint64_t>(rdi371) + reinterpret_cast<uint64_t>(r9_363)));
                    }
                }
                r9_325 = r8_361;
                r8_241 = r11_355;
                r11_373 = r9_363;
                do {
                    rsi374 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    rax375 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi374) + reinterpret_cast<unsigned char>(r12_343));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rsi374 = rax375;
                    }
                    rbx376 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax375) - (reinterpret_cast<uint64_t>(rax375) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax375) < reinterpret_cast<uint64_t>(rax375) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rsi374) < reinterpret_cast<uint64_t>(r10_362)))) & reinterpret_cast<unsigned char>(r12_343)) + (reinterpret_cast<uint64_t>(rsi374) + reinterpret_cast<uint64_t>(r11_373)));
                    rsi377 = r12_343;
                    rdi368 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rdi368) - (reinterpret_cast<unsigned char>(rdi368) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi368) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rdi368) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_241) < reinterpret_cast<unsigned char>(rbx376))))))) & reinterpret_cast<unsigned char>(r12_343)) + (reinterpret_cast<unsigned char>(r8_241) - reinterpret_cast<unsigned char>(rbx376)));
                    rax378 = gcd_odd(rdi368, rsi377);
                } while (rax378 == 1);
                rcx379 = r8_241;
                r11_380 = rax378;
                if (rax378 == r12_343) 
                    goto addr_5af7_246;
                rdx381 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_343) % reinterpret_cast<unsigned char>(r11_380));
                rax382 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_343) / reinterpret_cast<unsigned char>(r11_380));
                r8_383 = rax382;
                r12_343 = rax382;
                if (reinterpret_cast<unsigned char>(r11_380) > reinterpret_cast<unsigned char>(1) && (reinterpret_cast<unsigned char>(r11_380) <= reinterpret_cast<unsigned char>(0x17ded78) || (al384 = prime_p_part_0(r11_380, rsi377, rdx381, rcx379), r11_380 = r11_380, rcx379 = rcx379, r8_383 = rax382, !!al384))) {
                    *reinterpret_cast<int32_t*>(&rdx385) = 1;
                    *reinterpret_cast<int32_t*>(&rdx385 + 4) = 0;
                    rsi386 = r11_380;
                    factor_insert_multiplicity(v278, rsi386, 1);
                    r8_241 = r8_383;
                    rcx387 = rcx379;
                    zf388 = reinterpret_cast<int1_t>(r8_241 == 1);
                    if (reinterpret_cast<unsigned char>(r8_241) <= reinterpret_cast<unsigned char>(1)) 
                        goto addr_5aca_249; else 
                        goto addr_5a2f_250;
                }
                rdx385 = v278;
                rsi386 = v316 + 1;
                factor_using_pollard_rho(r11_380, rsi386, rdx385, rcx379);
                r8_241 = r8_383;
                rcx387 = rcx379;
                zf388 = reinterpret_cast<int1_t>(r8_241 == 1);
                if (reinterpret_cast<unsigned char>(r8_241) > reinterpret_cast<unsigned char>(1)) {
                    addr_5a2f_250:
                    if (reinterpret_cast<unsigned char>(r8_241) <= reinterpret_cast<unsigned char>(0x17ded78)) 
                        goto addr_5ad7_252;
                    al389 = prime_p_part_0(r8_241, rsi386, rdx385, rcx387);
                    r8_241 = r8_241;
                    if (al389) 
                        goto addr_5ad7_252;
                } else {
                    addr_5aca_249:
                    if (zf388) 
                        goto addr_5b45_254; else 
                        goto addr_5acc_255;
                }
                rbp353 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp353) % reinterpret_cast<unsigned char>(r8_241));
                rcx354 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx387) % reinterpret_cast<unsigned char>(r8_241));
                continue;
                addr_5acc_255:
                *reinterpret_cast<int32_t*>(&rcx354) = 0;
                *reinterpret_cast<int32_t*>(&rcx354 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rbp353) = 0;
                *reinterpret_cast<int32_t*>(&rbp353 + 4) = 0;
            }
            break;
            addr_5af7_246:
            ++v316;
        } while (reinterpret_cast<unsigned char>(r12_343) > reinterpret_cast<unsigned char>(1));
        goto addr_5b07_217;
    }
    fun_2810("a < n", "src/factor.c", 0x5c4, "factor_using_pollard_rho", r8_241, r9_325);
    addr_5b45_254:
    goto v154;
    addr_5ad7_252:
    rdi327 = v278;
    rsi326 = r8_241;
    goto addr_2f20_207;
    addr_53a9_195:
    if (!*reinterpret_cast<void***>(v316 + 8)) {
        *reinterpret_cast<void***>(v316) = rbp244;
        *reinterpret_cast<void***>(v316 + 8) = v317;
        goto addr_4d41_129;
    }
    factor_insert_large_part_0();
    r14_390 = reinterpret_cast<struct s0*>(rdi315 + 0xffffffffffffffff);
    rbp391 = rdi315;
    rsp392 = reinterpret_cast<struct s0**>(rsp246 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x148);
    rax393 = g28;
    v394 = rax393;
    eax395 = 0;
    v396 = r14_390;
    if (*reinterpret_cast<uint32_t*>(&rdi315) & 1) 
        goto addr_5478_261;
    v397 = 0;
    r14_390 = v396;
    addr_5490_263:
    rcx398 = rbp391;
    *reinterpret_cast<int32_t*>(&r15_399) = 0;
    *reinterpret_cast<int32_t*>(&r15_399 + 4) = 0;
    rax400 = reinterpret_cast<unsigned char>(rbp391) >> 1;
    esi401 = 64;
    *reinterpret_cast<uint32_t*>(&rax402) = *reinterpret_cast<uint32_t*>(&rax400) & 0x7f;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax402) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax403) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax402);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax403) + 4) = 0;
    rdx404 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax403) + reinterpret_cast<int64_t>(rax403) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax403) * reinterpret_cast<int64_t>(rax403) * reinterpret_cast<unsigned char>(rbp391)));
    rax405 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx404) + reinterpret_cast<uint64_t>(rdx404) - reinterpret_cast<uint64_t>(rdx404) * reinterpret_cast<uint64_t>(rdx404) * reinterpret_cast<unsigned char>(rbp391));
    *reinterpret_cast<int32_t*>(&rdx406) = 0;
    *reinterpret_cast<int32_t*>(&rdx406 + 4) = 0;
    r13_407 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax405) + reinterpret_cast<uint64_t>(rax405) - reinterpret_cast<uint64_t>(rax405) * reinterpret_cast<uint64_t>(rax405) * reinterpret_cast<unsigned char>(rbp391));
    *reinterpret_cast<int32_t*>(&rax408) = 1;
    *reinterpret_cast<int32_t*>(&rax408 + 4) = 0;
    do {
        rdi409 = reinterpret_cast<unsigned char>(rcx398) << 63;
        rcx398 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx398) >> 1);
        rdx406 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx406) >> 1) | rdi409);
        if (reinterpret_cast<unsigned char>(rcx398) < reinterpret_cast<unsigned char>(rax408) || rcx398 == rax408 && reinterpret_cast<unsigned char>(rdx406) <= reinterpret_cast<unsigned char>(r15_399)) {
            cf410 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_399) < reinterpret_cast<unsigned char>(rdx406));
            r15_399 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_399) - reinterpret_cast<unsigned char>(rdx406));
            rax408 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax408) - (reinterpret_cast<unsigned char>(rcx398) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax408) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx398) + static_cast<uint64_t>(cf410))))));
        }
        --esi401;
    } while (esi401);
    *reinterpret_cast<int32_t*>(&rbx411) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx411) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&r8_241) = v397;
    *reinterpret_cast<int32_t*>(&r8_241 + 4) = 0;
    r9_325 = r15_399;
    *reinterpret_cast<unsigned char*>(&rbx411) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp391) - reinterpret_cast<unsigned char>(r15_399)) > reinterpret_cast<unsigned char>(r15_399));
    rbx412 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbx411) & reinterpret_cast<unsigned char>(rbp391)) + (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r15_399) + reinterpret_cast<unsigned char>(r15_399)) - reinterpret_cast<unsigned char>(rbp391)));
    al413 = millerrabin(rbp391, r13_407, rbx412, r14_390, *reinterpret_cast<uint32_t*>(&r8_241), r9_325);
    if (al413) {
        v414 = reinterpret_cast<void*>(rsp392 - 1 + 1 + 6);
        factor();
        v415 = r14_390;
        r14_390 = r13_407;
        eax416 = v417;
        r13_407 = reinterpret_cast<struct s0*>(0x10340);
        v418 = eax416;
        *reinterpret_cast<uint32_t*>(&rax419) = eax416 - 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax419) + 4) = 0;
        v420 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v414) + rax419 * 8);
        r12_247 = reinterpret_cast<void**>(2);
        rax421 = rbx412;
        rbx412 = r15_399;
        r15_399 = rax421;
        goto addr_55b0_269;
    }
    addr_56fc_270:
    addr_5690_271:
    rax422 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v394) - reinterpret_cast<unsigned char>(g28));
    if (rax422) {
        fun_2740();
    } else {
        goto v317;
    }
    addr_5719_274:
    *reinterpret_cast<int32_t*>(&rdx329) = 0x4f2;
    *reinterpret_cast<int32_t*>(&rdx329 + 4) = 0;
    rsi330 = reinterpret_cast<void**>("src/factor.c");
    rdi328 = reinterpret_cast<void**>("(s1) < (n)");
    fun_2810("(s1) < (n)", "src/factor.c", 0x4f2, "prime_p", r8_241, r9_325);
    goto addr_5740_206;
    addr_5478_261:
    do {
        r14_390 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(r14_390) >> 1);
        ++eax395;
    } while (!(*reinterpret_cast<unsigned char*>(&r14_390) & 1));
    v397 = eax395;
    goto addr_5490_263;
    addr_4c97_197:
    *reinterpret_cast<void***>(v133) = rbp130;
    *reinterpret_cast<void***>(v133 + 8) = r13_112;
    goto addr_4ad5_73;
    addr_37ca_56:
    rax423 = xrealloc(0, 16, 0, 16);
    rax424 = xrealloc(0, 8, 0, 8);
    rdi425 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax423 + 16) + 0xfffffffffffffff0);
    r15_426 = rax424;
    fun_2ab0(rdi425, 8, 0, 0, r8_66, rdi425, 8, 0, 0, r8_66);
    rdi427 = rdi425;
    addr_370b_277:
    fun_27a0(rdi427, r12_99, rdx100, 0, r8_66, rdi427, r12_99, rdx100, 0, r8_66);
    *reinterpret_cast<void***>(r15_426 + v101 * 8) = reinterpret_cast<void**>(1);
    goto v15;
    addr_361b_57:
    rbp428 = -1;
    rbx429 = -1;
    rdx100 = reinterpret_cast<void**>(0xfffffffffffffff0);
    r15_430 = reinterpret_cast<void**>(0xfffffffffffffff0);
    do {
        eax431 = fun_2a00(r15_430, r12_99, 0xfffffffffffffff0, 0, r8_66, r15_430, r12_99, 0xfffffffffffffff0, 0, r8_66);
        zf432 = reinterpret_cast<uint1_t>(eax431 == 0);
        if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(eax431 < 0) | zf432)) 
            break;
        --rbx429;
        r15_430 = r15_430 - 16;
    } while (rbx429 != -1);
    goto addr_3670_280;
    if (zf432) {
        *reinterpret_cast<int64_t*>(rbx429 * 8) = *reinterpret_cast<int64_t*>(rbx429 * 8) + 1;
        goto v15;
    }
    rax433 = xrealloc(0, 16, 0, 16);
    v434 = rax433;
    rax435 = xrealloc(0, 8, 0, 8);
    rdx100 = reinterpret_cast<void**>(16);
    r15_426 = rax435;
    rax436 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v434) + reinterpret_cast<unsigned char>(16) + 0xfffffffffffffff0);
    fun_2ab0(rax436, 8, 16, 0, r8_66, rax436, 8, 16, 0, r8_66);
    rax437 = rbx429 + 1;
    v101 = rax437;
    v438 = reinterpret_cast<void*>(rax437 << 4);
    rax439 = rax436;
    if (rbx429 < -1) {
        addr_36da_284:
        r14_440 = rax439;
    } else {
        goto addr_3701_286;
    }
    do {
        rdi441 = r14_440;
        r14_440 = r14_440 - 16;
        fun_27a0(rdi441, r14_440, rdx100, 0, r8_66, rdi441, r14_440, rdx100, 0, r8_66);
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r15_426 + rbp428 * 8) + 8) = *reinterpret_cast<void***>(r15_426 + rbp428 * 8);
        --rbp428;
    } while (rbx429 < rbp428);
    addr_3701_286:
    rdi427 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v438) + reinterpret_cast<unsigned char>(v434));
    goto addr_370b_277;
    addr_3670_280:
    rax442 = xrealloc(0, 16, 0, 16);
    v434 = rax442;
    rax443 = xrealloc(0, 8, 0, 8);
    r15_426 = rax443;
    rax444 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(v434 + 16) + 0xfffffffffffffff0);
    fun_2ab0(rax444, 8, 0xfffffffffffffff0, 0, r8_66, rax444, 8, 0xfffffffffffffff0, 0, r8_66);
    rax439 = rax444;
    v438 = reinterpret_cast<void*>(0);
    v101 = 0;
    goto addr_36da_284;
    addr_5689_288:
    goto addr_5690_271;
    addr_4548_289:
    fun_25e0(v445, rsi120, v445, rsi120);
    fun_25e0(v446, rsi120, v446, rsi120);
    rsp118 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp121) - 8 + 8 - 8 + 8);
    goto addr_455c_61;
    addr_45a0_290:
    rdx447 = v448;
    addr_4524_291:
    if (rdx447) {
        *reinterpret_cast<int32_t*>(&rbx102) = 0;
        *reinterpret_cast<int32_t*>(&rbx102 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdi449) = 0;
        *reinterpret_cast<int32_t*>(&rdi449 + 4) = 0;
        do {
            fun_2940((reinterpret_cast<unsigned char>(rdi449) << 4) + reinterpret_cast<uint64_t>(v450), rsi120, rdx447, rcx116, r8_115, r9_114);
            rsp121 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp121) - 8 + 8);
            *reinterpret_cast<int32_t*>(&rdi449) = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbx102 + 1));
            *reinterpret_cast<int32_t*>(&rdi449 + 4) = 0;
            rbx102 = rdi449;
        } while (reinterpret_cast<unsigned char>(rdi449) < reinterpret_cast<unsigned char>(v451));
        goto addr_4548_289;
    }
    addr_5680_294:
    while (rax452 == rbx412) {
        while (1) {
            *reinterpret_cast<uint32_t*>(&rax453) = r13_407->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax453) + 4) = 0;
            r12_247 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_247) + reinterpret_cast<uint64_t>(rax453));
            rax454 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx412) * reinterpret_cast<unsigned char>(r12_247));
            rdx455 = __intrinsic();
            r15_399 = rax454;
            if (rdx455) {
                if (reinterpret_cast<unsigned char>(rbp391) <= reinterpret_cast<unsigned char>(rdx455)) 
                    goto addr_5719_274;
                rcx456 = rbp391;
                esi457 = 64;
                *reinterpret_cast<int32_t*>(&rax458) = 0;
                *reinterpret_cast<int32_t*>(&rax458 + 4) = 0;
                do {
                    rdi459 = reinterpret_cast<unsigned char>(rcx456) << 63;
                    rcx456 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx456) >> 1);
                    rax458 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax458) >> 1) | rdi459);
                    if (reinterpret_cast<unsigned char>(rcx456) < reinterpret_cast<unsigned char>(rdx455) || rcx456 == rdx455 && reinterpret_cast<unsigned char>(rax458) <= reinterpret_cast<unsigned char>(r15_399)) {
                        cf460 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_399) < reinterpret_cast<unsigned char>(rax458));
                        r15_399 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_399) - reinterpret_cast<unsigned char>(rax458));
                        rdx455 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx455) - (reinterpret_cast<unsigned char>(rcx456) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx455) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx456) + static_cast<uint64_t>(cf460))))));
                    }
                    --esi457;
                } while (esi457);
            } else {
                r15_399 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax454) % reinterpret_cast<unsigned char>(rbp391));
            }
            *reinterpret_cast<uint32_t*>(&r8_241) = v397;
            *reinterpret_cast<int32_t*>(&r8_241 + 4) = 0;
            r9_325 = rbx412;
            al461 = millerrabin(rbp391, r14_390, r15_399, v415, *reinterpret_cast<uint32_t*>(&r8_241), r9_325);
            if (!al461) 
                goto addr_56fc_270;
            r13_407 = reinterpret_cast<struct s0*>(&r13_407->f1);
            if (reinterpret_cast<int1_t>(0x105dc == r13_407)) 
                break;
            addr_55b0_269:
            if (!v418) 
                goto addr_5690_271;
            r11_462 = v414;
            do {
                r8_241 = rbx412;
                rax452 = powm(r15_399, reinterpret_cast<uint64_t>(v396) / v463, rbp391, r14_390, r8_241, r9_325);
                if (v420 == r11_462) 
                    goto addr_5680_294;
                r11_462 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r11_462) + 8);
            } while (rax452 != rbx412);
        }
        fun_2700();
        fun_2a20();
        rax452 = fun_25f0();
    }
    goto addr_5689_288;
    addr_4520_310:
    while (!*reinterpret_cast<int32_t*>(&rax464)) {
        while (1) {
            *reinterpret_cast<uint32_t*>(&rdx465) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_112));
            *reinterpret_cast<int32_t*>(&rdx465 + 4) = 0;
            fun_28c0(rbp108, rbp108, rdx465, rcx116, r8_115, r9_114);
            r9_114 = v113;
            rcx116 = r14_109;
            r8_115 = v110;
            rsi120 = r12_106;
            al466 = mp_millerrabin(rbx102, rsi120, rbp108, rcx116, r8_115, r9_114);
            rsp121 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp121) - 8 + 8 - 8 + 8);
            if (!al466) 
                goto addr_45a0_290;
            ++r13_112;
            if (reinterpret_cast<int1_t>(r13_112 == 0x105dc)) 
                break;
            addr_4458_60:
            if (!v467) 
                goto addr_4548_289;
            *reinterpret_cast<int32_t*>(&r15_107) = 0;
            *reinterpret_cast<int32_t*>(&r15_107 + 4) = 0;
            do {
                rdx468 = r15_107;
                ++r15_107;
                fun_2760(r14_109, r12_106, (reinterpret_cast<unsigned char>(rdx468) << 4) + reinterpret_cast<uint64_t>(v469), rcx116, r8_115, r9_114);
                rcx116 = rbx102;
                fun_2730(r14_109, rbp108, r14_109, rcx116, r8_115, r9_114);
                *reinterpret_cast<int32_t*>(&rsi120) = 1;
                *reinterpret_cast<int32_t*>(&rsi120 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rax464) = fun_2aa0(r14_109, 1, r14_109, rcx116, r8_115, r9_114);
                rsp121 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp121) - 8 + 8 - 8 + 8 - 8 + 8);
                rdx447 = v470;
                if (reinterpret_cast<unsigned char>(r15_107) >= reinterpret_cast<unsigned char>(rdx447)) 
                    goto addr_4520_310;
            } while (*reinterpret_cast<int32_t*>(&rax464));
        }
        rax471 = fun_2700();
        *reinterpret_cast<int32_t*>(&rsi120) = 0;
        *reinterpret_cast<int32_t*>(&rsi120 + 4) = 0;
        rdx447 = rax471;
        fun_2a20();
        rax464 = fun_25f0();
        rsp121 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp121) - 8 + 8 - 8 + 8 - 8 + 8);
    }
    goto addr_4524_291;
}