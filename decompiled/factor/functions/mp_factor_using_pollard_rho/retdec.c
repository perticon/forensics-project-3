void mp_factor_using_pollard_rho(int32_t * n, int64_t a, int32_t * factors) {
    int64_t v1 = __readfsqword(40); // 0x3cde
    int64_t v2; // 0x3cc0
    if (*(char *)&dev_debug != 0) {
        // 0x40d6
        function_2a90();
        v2 = a;
    }
    // 0x3cfe
    function_2780();
    function_28b0();
    function_28b0();
    function_28b0();
    function_29f0();
    uint64_t v3; // 0x3cc0
    int64_t v4; // 0x3cc0
    int64_t v5; // 0x3cc0
    int64_t v6; // 0x3cc0
    int64_t v7; // bp-104, 0x3cc0
    int32_t v8; // 0x3cc0
    int32_t v9; // 0x3cc0
    int64_t v10; // 0x3d14
    if ((int32_t)function_2aa0() != 0) {
        // 0x3daa
        v10 = &v7;
        v4 = 1;
        v5 = v2;
        v8 = 1;
        while (true) {
          lab_0x3daa:;
            int32_t v11 = v8; // 0x3cc0
            int64_t v12 = v5; // 0x3e58
            int64_t v13 = v4; // 0x3cc0
            while (true) {
                // 0x3daa
                v6 = v12;
                v9 = v11;
                int64_t v14 = v13; // 0x3cc0
                int64_t v15; // 0x3cc0
                while (true) {
                    // 0x3daa
                    v3 = v14;
                    function_27b0();
                    function_29d0();
                    function_28c0();
                    function_26c0();
                    function_27b0();
                    function_29d0();
                    int64_t v16; // 0x3cc0
                    if (v3 % 32 != 1) {
                        int64_t v17 = v3 - 1; // 0x3da0
                        v16 = v17;
                        v15 = 0;
                        if (v17 == 0) {
                            // break -> 0x3e47
                            break;
                        }
                    } else {
                        // 0x3e0d
                        function_2a40();
                        if ((int32_t)function_2aa0() != 0) {
                            goto lab_0x3f10;
                        }
                        // 0x3e30
                        function_27a0();
                        int64_t v18 = v3 - 1; // 0x3e3d
                        v16 = v18;
                        v15 = v18;
                        if (v18 == 0) {
                            // break -> 0x3e47
                            break;
                        }
                    }
                    // 0x3daa
                    v14 = v16;
                }
                // 0x3e47
                function_27a0();
                int64_t v19 = v9; // 0x3e53
                int64_t v20 = v15; // 0x3e64
                if (v9 != 0) {
                    int64_t v21 = v20 + 1; // 0x3e89
                    function_27b0();
                    function_29d0();
                    function_28c0();
                    v20 = v21;
                    while (v21 != v19) {
                        // 0x3e80
                        v21 = v20 + 1;
                        function_27b0();
                        function_29d0();
                        function_28c0();
                        v20 = v21;
                    }
                }
                // 0x3ec0
                v12 = 2 * v19;
                function_27a0();
                v11 = v12;
                v13 = v19;
            }
            goto lab_0x3f67;
        }
    }
    goto lab_0x406a_2;
  lab_0x3f10:
    // 0x3f10
    function_27b0();
    function_29d0();
    function_28c0();
    function_26c0();
    function_2a40();
    if ((int32_t)function_2aa0() == 0) {
        goto lab_0x3f10;
    } else {
        goto lab_0x3f67;
    }
  lab_0x3f67:
    // 0x3f67
    function_2760();
    int64_t v22; // 0x3cc0
    if ((int32_t)function_2aa0() < 1) {
        goto lab_0x3fcb;
    } else {
        // 0x3fa6
        if ((int32_t)function_2aa0() < 0) {
            goto lab_0x404e;
        } else {
            // 0x3fbb
            if ((char)mp_prime_p_part_0(v10, 0x17ded79, v10, v6, v22) != 0) {
                goto lab_0x404e;
            } else {
                goto lab_0x3fcb;
            }
        }
    }
  lab_0x3fcb:;
    int64_t v23 = v6; // 0x3fd2
    if (*(char *)&dev_debug != 0) {
        // 0x40b4
        function_2a80();
        v23 = (int64_t)g24;
    }
    // 0x3fd8
    mp_factor_using_pollard_rho((int32_t *)&v7, a + 1, factors);
    int64_t v24 = v23; // 0x3fe9
    int64_t v25 = (int64_t)factors; // 0x3fe9
    goto lab_0x3fee;
  lab_0x3fee:;
    int64_t v26 = v24;
    if ((int32_t)function_2aa0() >= 1) {
        // 0x3fff
        if ((int32_t)function_2aa0() < 0) {
            // 0x405d
            mp_factor_insert(factors, n);
            goto lab_0x406a_2;
        }
        // 0x4010
        if ((char)mp_prime_p_part_0((int64_t)n, 0x17ded79, v25, v26, v22) != 0) {
            // 0x405d
            mp_factor_insert(factors, n);
            goto lab_0x406a_2;
        }
    }
    // 0x401c
    function_29d0();
    function_29d0();
    function_29d0();
    v4 = 0x100000000 * v3 / 0x100000000;
    v5 = v26;
    v8 = v9;
    if ((int32_t)function_2aa0() == 0) {
        // break -> 0x406a
        goto lab_0x406a_2;
    }
    goto lab_0x3daa;
  lab_0x404e:
    // 0x404e
    mp_factor_insert(factors, (int32_t *)&v7);
    v24 = v6;
    v25 = v10;
    goto lab_0x3fee;
  lab_0x406a_2:
    // 0x406a
    function_2660();
    if (v1 == __readfsqword(40)) {
        // 0x40a2
        return;
    }
    // 0x40f6
    function_2740();
    return;
}