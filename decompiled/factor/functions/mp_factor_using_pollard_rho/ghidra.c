void mp_factor_using_pollard_rho(undefined8 param_1,long param_2,undefined8 param_3)

{
  long lVar1;
  char cVar2;
  int iVar3;
  long lVar4;
  long in_FS_OFFSET;
  long local_d0;
  undefined local_a8 [16];
  undefined local_98 [16];
  undefined local_88 [16];
  undefined local_78 [16];
  undefined local_68 [16];
  undefined local_58 [24];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (dev_debug != '\0') {
    __fprintf_chk(stderr,1,"[pollard-rho (%lu)] ",param_2);
  }
  lVar4 = 1;
  __gmpz_inits(local_68,local_58,0);
  __gmpz_init_set_si(local_88,2);
  __gmpz_init_set_si(local_a8,2);
  __gmpz_init_set_si(local_98,2);
  __gmpz_init_set_ui(local_78,1);
  local_d0 = 1;
LAB_00103d7c:
  iVar3 = __gmpz_cmp_ui(param_1,1);
  lVar1 = lVar4;
  if (iVar3 != 0) {
LAB_00103daa:
    lVar4 = lVar1;
    __gmpz_mul(local_68,local_a8,local_a8);
    __gmpz_mod(local_a8,local_68,param_1);
    __gmpz_add_ui(local_a8,local_a8,param_2);
    __gmpz_sub(local_68,local_98,local_a8);
    __gmpz_mul(local_58,local_78,local_68);
    __gmpz_mod(local_78,local_58,param_1);
    if (((uint)lVar4 & 0x1f) == 1) {
      __gmpz_gcd(local_68,local_78,param_1);
      iVar3 = __gmpz_cmp_ui(local_68,1);
      if (iVar3 != 0) goto LAB_00103f10;
      __gmpz_set(local_88,local_a8);
    }
    lVar1 = lVar4 + -1;
    if (lVar4 + -1 == 0) {
      lVar4 = lVar4 + -1;
      __gmpz_set(local_98,local_a8);
      if (local_d0 != 0) {
        do {
          lVar4 = lVar4 + 1;
          __gmpz_mul(local_68,local_a8,local_a8);
          __gmpz_mod(local_a8,local_68,param_1);
          __gmpz_add_ui(local_a8,local_a8,param_2);
        } while (local_d0 != lVar4);
      }
      __gmpz_set(local_88,local_a8);
      lVar1 = local_d0;
      local_d0 = local_d0 * 2;
    }
    goto LAB_00103daa;
  }
LAB_0010406a:
  __gmpz_clears(local_78,local_58,local_68,local_98,local_a8,local_88,0);
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
LAB_00103f10:
  do {
    __gmpz_mul(local_68,local_88,local_88);
    __gmpz_mod(local_88,local_68,param_1);
    __gmpz_add_ui(local_88,local_88,param_2);
    __gmpz_sub(local_68,local_98,local_88);
    __gmpz_gcd(local_68,local_68,param_1);
    iVar3 = __gmpz_cmp_ui(local_68,1);
  } while (iVar3 == 0);
  __gmpz_divexact(param_1,param_1,local_68);
  iVar3 = __gmpz_cmp_ui(local_68,1);
  if ((iVar3 < 1) ||
     ((iVar3 = __gmpz_cmp_ui(local_68,0x17ded79), -1 < iVar3 &&
      (cVar2 = mp_prime_p_part_0(local_68), cVar2 == '\0')))) {
    if (dev_debug != '\0') {
      fwrite("[composite factor--restarting pollard-rho] ",1,0x2b,stderr);
    }
    mp_factor_using_pollard_rho(local_68,param_2 + 1,param_3);
  }
  else {
    mp_factor_insert(param_3,local_68);
  }
  iVar3 = __gmpz_cmp_ui(param_1,1);
  if ((0 < iVar3) &&
     ((iVar3 = __gmpz_cmp_ui(param_1,0x17ded79), iVar3 < 0 ||
      (cVar2 = mp_prime_p_part_0(param_1), cVar2 != '\0')))) {
    mp_factor_insert(param_3,param_1);
    goto LAB_0010406a;
  }
  __gmpz_mod(local_a8,local_a8,param_1);
  __gmpz_mod(local_98,local_98,param_1);
  __gmpz_mod(local_88,local_88,param_1);
  goto LAB_00103d7c;
}