lbuf_flush (void)
{
  size_t size = lbuf.end - lbuf.buf;
  if (full_write (STDOUT_FILENO, lbuf.buf, size) != size)
    die (EXIT_FAILURE, errno, "%s", _("write error"));
  lbuf.end = lbuf.buf;
}