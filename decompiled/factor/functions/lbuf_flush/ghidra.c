void lbuf_flush(void)

{
  char *pcVar1;
  char *pcVar2;
  int iVar3;
  long lVar4;
  undefined8 uVar5;
  int *piVar6;
  char *__src;
  void *pvVar7;
  long lVar8;
  char cVar9;
  
  lVar8 = (long)lbuf._8_8_ - (long)lbuf._0_8_;
  lVar4 = full_write(1,lbuf._0_8_,lVar8);
  if (lVar4 == lVar8) {
    lbuf._8_8_ = (char *)lbuf._0_8_;
    return;
  }
  uVar5 = dcgettext(0,"write error",5);
  piVar6 = __errno_location();
  cVar9 = '\x01';
  error(1,*piVar6,"%s",uVar5);
  pcVar1 = lbuf._8_8_ + 1;
  pcVar2 = pcVar1;
  *lbuf._8_8_ = cVar9;
  lbuf._8_8_ = pcVar2;
  pvVar7 = lbuf._0_8_;
  if (cVar9 == '\n') {
    if (line_buffered_0 == 0xffffffff) {
      iVar3 = isatty(0);
      if (iVar3 != 0) {
        line_buffered_0 = 1;
        goto LAB_00103965;
      }
      iVar3 = isatty(1);
      line_buffered_0 = (uint)(iVar3 != 0);
    }
    if (line_buffered_0 != 0) {
LAB_00103965:
      lbuf_flush();
      return;
    }
    if (0x1ff < (ulong)((long)pcVar1 - (long)pvVar7)) {
      pcVar2 = (char *)((long)pvVar7 + 0x200);
      do {
        __src = pcVar2;
        pcVar2 = __src + -1;
      } while (__src[-1] != '\n');
      lbuf._8_8_ = __src;
      lbuf_flush();
      pvVar7 = memcpy(lbuf._0_8_,__src,(long)pcVar1 - (long)__src);
      lbuf._8_8_ = (char *)((long)pvVar7 + ((long)pcVar1 - (long)__src));
      return;
    }
  }
  return;
}