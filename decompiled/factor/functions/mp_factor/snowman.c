void** mp_factor(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void* rsp7;
    void** rax8;
    void** v9;
    void** eax10;
    int1_t zf11;
    void** rbp12;
    void** r12_13;
    void** rdi14;
    void** r13_15;
    void** v16;
    void** rax17;
    void** rbx18;
    void** rdx19;
    void* rsp20;
    void** rbx21;
    void** r15_22;
    void** r13_23;
    int32_t eax24;
    void* rsp25;
    void** r14_26;
    void* rax27;
    void** rsi28;
    int32_t eax29;
    void* rsp30;
    int32_t eax31;
    int32_t eax32;
    int1_t zf33;
    int32_t eax34;
    signed char al35;
    void* rax36;
    void* rsp37;
    void** v38;
    void** rax39;
    void** v40;
    int1_t zf41;
    void** rdi42;
    void** v43;
    void** v44;
    void** v45;
    void** v46;
    void** v47;
    void** rdx48;
    void* rsp49;
    void** rax50;
    void** rax51;
    int32_t eax52;
    void** r15_53;
    void** rbx54;
    void** rdx55;
    void** rax56;
    int64_t rax57;
    int32_t eax58;
    void** v59;
    void** v60;
    void** r14_61;
    void** v62;
    void** r13_63;
    void** r15_64;
    void** r13_65;
    void** v66;
    void** r14_67;
    void** r12_68;
    int32_t eax69;
    void** rdx70;
    int32_t eax71;
    int32_t eax72;
    signed char al73;
    int1_t zf74;
    int32_t eax75;
    int32_t eax76;
    signed char al77;
    void** rax78;
    void** rax79;
    void* rax80;
    void** r13_81;
    void** r12_82;
    void** rcx83;
    void** rdx84;
    struct s0* r14_85;
    void** v86;
    void** v87;
    void** v88;
    void** rbx89;
    void* rsp90;
    void** rax91;
    void** v92;
    void** r12_93;
    void** r15_94;
    void** rbp95;
    void** r14_96;
    void** v97;
    void** rax98;
    void** r13_99;
    void** v100;
    void** r9_101;
    void** r8_102;
    void** rcx103;
    unsigned char al104;
    void* rsp105;
    void* rsp106;
    void** rsi107;
    void* rsp108;
    void** rdi109;
    void** rcx110;
    void** rdx111;
    void** rsi112;
    void* rax113;
    int64_t v114;
    void* rsp115;
    void** r15_116;
    void** rbp117;
    void*** rsp118;
    void** v119;
    void** v120;
    void** rax121;
    void** v122;
    uint64_t rcx123;
    void** rdx124;
    int64_t rcx125;
    uint64_t rcx126;
    uint1_t cf127;
    void** rax128;
    void** rsi129;
    uint1_t cf130;
    int1_t cf131;
    void** v132;
    void** v133;
    void** tmp64_134;
    void** rax135;
    void** r12_136;
    void** r10_137;
    void** v138;
    void** v139;
    void** v140;
    void** v141;
    void** r14_142;
    void** r15_143;
    void*** v144;
    void** r9_145;
    uint64_t rax146;
    int64_t rax147;
    void* rax148;
    void* rdx149;
    void* rax150;
    void** rax151;
    void** rbp152;
    int64_t rax153;
    void** v154;
    int64_t v155;
    void** rax156;
    void** tmp64_157;
    void** rdx158;
    void* v159;
    void** v160;
    void** rdx161;
    void** r8_162;
    void** rcx163;
    void** tmp64_164;
    uint1_t cf165;
    void** rax166;
    void** v167;
    void** rax168;
    int64_t rax169;
    void** v170;
    void** rax171;
    void** v172;
    void** v173;
    void** rdx174;
    void** r15_175;
    void** r12_176;
    void** r13_177;
    void** rsi178;
    void** rbp179;
    void** rax180;
    void** tmp64_181;
    void** rcx182;
    void* v183;
    void** v184;
    void** r11_185;
    void** r10_186;
    void** v187;
    void** r15_188;
    void** r12_189;
    void*** rbp190;
    void** r9_191;
    void** rax192;
    void** tmp64_193;
    void** rdx194;
    void* v195;
    void** rcx196;
    void** rdx197;
    void** rdx198;
    void** rsi199;
    void** tmp64_200;
    uint1_t cf201;
    void** rax202;
    void** rdi203;
    void** r8_204;
    uint64_t rax205;
    int64_t rax206;
    void** r13_207;
    void* rax208;
    void* rdx209;
    void** rdx210;
    void* rax211;
    signed char al212;
    void** rdx213;
    void** rsi214;
    void** rdi215;
    unsigned char al216;
    void** rax217;
    void* rsp218;
    void** rax219;
    void* rsp220;
    void** rax221;
    uint64_t rax222;
    struct s29* rax223;
    void* rax224;
    void* rdx225;
    void* rax226;
    unsigned char al227;
    void** r8_228;
    void** r9_229;
    void** v230;
    void** rbp231;
    void** v232;
    void*** rsp233;
    void** r12_234;
    int64_t r13_235;
    void** r11_236;
    void** r10_237;
    void*** r14_238;
    struct s30* rbx239;
    uint64_t r15_240;
    void** rcx241;
    void** rdx242;
    void** r10_243;
    void** r13_244;
    void* rax245;
    int32_t ecx246;
    int32_t ecx247;
    int32_t edx248;
    int64_t rbx249;
    void** rcx250;
    void** rdx251;
    struct s31* rcx252;
    void* rcx253;
    void** rcx254;
    void** rdx255;
    void** rdx256;
    struct s32* rcx257;
    struct s33* rsi258;
    void* rsi259;
    void* rcx260;
    struct s34* rsi261;
    void* rsi262;
    void** rcx263;
    void** rdx264;
    void** v265;
    void** rdx266;
    uint32_t ecx267;
    uint32_t edx268;
    void** rsi269;
    struct s35* rdi270;
    void* rdi271;
    void** rdx272;
    void** rdx273;
    uint32_t ecx274;
    uint32_t edx275;
    void** rsi276;
    struct s36* rdi277;
    void* rdi278;
    void** rdx279;
    void** rdx280;
    uint32_t ecx281;
    uint32_t edx282;
    void** rsi283;
    struct s37* rdi284;
    void* rdi285;
    void** rdx286;
    int64_t rax287;
    void* rax288;
    int64_t rax289;
    struct s38* rax290;
    uint32_t ecx291;
    uint32_t edx292;
    void** rsi293;
    struct s39* rdi294;
    void* rdi295;
    void** rdx296;
    struct s40* rcx297;
    struct s41* rsi298;
    void* rcx299;
    void* rsi300;
    void** rcx301;
    void** rdi302;
    void** v303;
    void** v304;
    unsigned char al305;
    int64_t rax306;
    struct s42* rax307;
    void* rax308;
    int64_t v309;
    signed char al310;
    unsigned char al311;
    void** r9_312;
    void** rsi313;
    void** rdi314;
    void** rdi315;
    void** rdx316;
    void** rsi317;
    int64_t rax318;
    void** r8_319;
    void*** r11_320;
    signed char* r10_321;
    uint32_t r9d322;
    int32_t ebx323;
    void* rcx324;
    void* rax325;
    int64_t rsi326;
    void* rax327;
    uint32_t r9d328;
    uint32_t eax329;
    void** r12_330;
    void** rcx331;
    int32_t esi332;
    void** rax333;
    void* rdx334;
    void* rdi335;
    uint1_t cf336;
    int64_t rbp337;
    int64_t r15_338;
    int64_t r14_339;
    void** rbp340;
    void** rcx341;
    void** r11_342;
    uint64_t rax343;
    int64_t rax344;
    void* rax345;
    void* rdx346;
    void* rax347;
    void** r8_348;
    void* r10_349;
    void* r9_350;
    void* rax351;
    void** r13_352;
    int64_t rax353;
    int64_t rax354;
    void** rdi355;
    void** rax356;
    int64_t rcx357;
    void* rdi358;
    void* rax359;
    void* r11_360;
    void* rsi361;
    void* rax362;
    void** rbx363;
    void** rsi364;
    void** rax365;
    void** rcx366;
    void** r11_367;
    void** rdx368;
    void** rax369;
    void** r8_370;
    signed char al371;
    void** rdx372;
    void** rsi373;
    void** rcx374;
    int1_t zf375;
    signed char al376;
    struct s0* r14_377;
    void** rbp378;
    struct s0** rsp379;
    void** rax380;
    void** v381;
    uint32_t eax382;
    struct s0* v383;
    uint32_t v384;
    void** rcx385;
    void** r15_386;
    uint64_t rax387;
    int32_t esi388;
    int64_t rax389;
    void* rax390;
    void* rdx391;
    void* rax392;
    void** rdx393;
    struct s0* r13_394;
    void** rax395;
    uint64_t rdi396;
    uint1_t cf397;
    int64_t rbx398;
    void** rbx399;
    unsigned char al400;
    void* v401;
    struct s0* v402;
    uint32_t eax403;
    unsigned char v404;
    uint32_t v405;
    int64_t rax406;
    void* v407;
    void** rax408;
    void* rax409;
    void** r14_410;
    void** v411;
    void** rbx412;
    void** rax413;
    void** rsi414;
    void** v415;
    void** rax416;
    void** rdi417;
    void** r15_418;
    void** rdi419;
    struct s0* rbp420;
    struct s0* rbx421;
    void** r15_422;
    int32_t eax423;
    uint1_t zf424;
    void** r15_425;
    void** rdx426;
    void** rax427;
    void** rsi428;
    void** rax429;
    void** rax430;
    void** rax431;
    void* v432;
    void** rax433;
    void** r14_434;
    void** rdi435;
    void** r15_436;
    void** r14_437;
    void** rax438;
    void** rsi439;
    void** rax440;
    void** rax441;
    void** v442;
    void** v443;
    void** rdx444;
    void** v445;
    void** rdi446;
    void*** v447;
    void** v448;
    void** rax449;
    void* rax450;
    void** rax451;
    void** rdx452;
    void** rcx453;
    int32_t esi454;
    void** rax455;
    uint64_t rdi456;
    uint1_t cf457;
    unsigned char al458;
    void* r11_459;
    uint64_t v460;
    void** rax461;
    void** rdx462;
    unsigned char al463;
    int64_t v464;
    void** rdx465;
    void*** v466;
    void** v467;
    void** rax468;

    while (rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 72), rax8 = g28, v9 = rax8, eax10 = *reinterpret_cast<void***>(rdi + 4), *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0), *reinterpret_cast<void***>(rsi + 8) = reinterpret_cast<void**>(0), *reinterpret_cast<void***>(rsi + 16) = reinterpret_cast<void**>(0), !!eax10) {
        zf11 = dev_debug == 0;
        rbp12 = rdi;
        r12_13 = rsi;
        if (!zf11) {
            rcx = stderr;
            *reinterpret_cast<int32_t*>(&rdx) = 17;
            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            *reinterpret_cast<int32_t*>(&rsi) = 1;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            fun_2a80("[trial division] ", 1, 17, rcx, r8, r9);
            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
        }
        rdi14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 16);
        r13_15 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 32);
        v16 = rdi14;
        fun_2ab0(rdi14, rsi, rdx, rcx, r8, rdi14, rsi, rdx, rcx, r8);
        rax17 = fun_25d0(rbp12, rbp12);
        rbx18 = rax17;
        rdx19 = rax17;
        fun_2840(rbp12, rbp12, rdx19, rcx, r8, r9);
        rsp20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8 - 8 + 8);
        if (rbx18) {
            do {
                fun_29f0(r13_15, 2, r13_15, 2);
                mp_factor_insert(r12_13, r13_15, rdx19, rcx, r8, r9);
                fun_2940(r13_15, r13_15, rdx19, rcx, r8, r9);
                rsp20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp20) - 8 + 8 - 8 + 8 - 8 + 8);
                --rbx18;
            } while (rbx18);
        }
        *reinterpret_cast<int32_t*>(&rbx21) = 1;
        *reinterpret_cast<int32_t*>(&rbx21 + 4) = 0;
        *reinterpret_cast<int32_t*>(&r15_22) = 3;
        *reinterpret_cast<int32_t*>(&r15_22 + 4) = 0;
        r13_23 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp20) + 32);
        while (1) {
            eax24 = fun_28f0(rbp12, r15_22, rdx19, rcx, r8, r9);
            rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp20) - 8 + 8);
            if (!eax24) {
                do {
                    *reinterpret_cast<int32_t*>(&r14_26) = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbx21 + 1));
                    *reinterpret_cast<uint32_t*>(&rax27) = *reinterpret_cast<unsigned char*>(rbx21 + 0x10340);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
                    r15_22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_22) + reinterpret_cast<uint64_t>(rax27));
                    rsi28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_22) * reinterpret_cast<unsigned char>(r15_22));
                    eax29 = fun_2aa0(rbp12, rsi28, rdx19, rcx, r8, r9);
                    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
                    if (eax29 < 0) 
                        goto addr_4268_10;
                    if (*reinterpret_cast<int32_t*>(&r14_26) == 0x29d) 
                        goto addr_4268_10;
                    *reinterpret_cast<int32_t*>(&rbx21) = *reinterpret_cast<int32_t*>(&r14_26);
                    *reinterpret_cast<int32_t*>(&rbx21 + 4) = 0;
                    eax31 = fun_28f0(rbp12, r15_22, rdx19, rcx, r8, r9);
                    rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                } while (!eax31);
            }
            rdx19 = r15_22;
            fun_27e0(rbp12, rbp12, rdx19, rcx, r8, r9);
            fun_29f0(r13_23, r15_22, r13_23, r15_22);
            mp_factor_insert(r12_13, r13_23, rdx19, rcx, r8, r9);
            fun_2940(r13_23, r13_23, rdx19, rcx, r8, r9);
            rsp20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        }
        addr_4268_10:
        fun_2940(v16, rsi28, rdx19, rcx, r8, r9);
        rdi = rbp12;
        eax32 = fun_2aa0(rdi, 1, rdx19, rcx, r8, r9);
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8 - 8 + 8);
        if (!eax32) 
            break;
        zf33 = dev_debug == 0;
        if (!zf33) {
            rcx = stderr;
            *reinterpret_cast<int32_t*>(&rdx19) = 19;
            *reinterpret_cast<int32_t*>(&rdx19 + 4) = 0;
            fun_2a80("[is number prime?] ", 1, 19, rcx, r8, r9);
            rdi = rbp12;
            eax32 = fun_2aa0(rdi, 1, 19, rcx, r8, r9);
            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8);
        }
        if (reinterpret_cast<uint1_t>(eax32 < 0) | reinterpret_cast<uint1_t>(eax32 == 0)) 
            goto addr_42b5_17;
        rdi = rbp12;
        eax34 = fun_2aa0(rdi, 0x17ded79, rdx19, rcx, r8, r9);
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
        if (eax34 < 0) 
            goto addr_4318_19;
        rdi = rbp12;
        al35 = mp_prime_p_part_0(rdi, 0x17ded79, rdx19, rcx, r8, r9);
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
        if (al35) 
            goto addr_4318_19;
        addr_42b5_17:
        rax36 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
        if (rax36) 
            goto addr_4377_21;
        rbp12 = rbp12;
        rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) + 72 + 8 + 8 + 8 + 8 + 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8);
        v38 = r12_13;
        rax39 = g28;
        v40 = rax39;
        zf41 = dev_debug == 0;
        if (!zf41) {
            rdi42 = stderr;
            rcx = reinterpret_cast<void**>(1);
            fun_2a90(rdi42, 1, "[pollard-rho (%lu)] ", 1, r8, r9, v43, 1, v44, v45, v46, v47);
            rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
        }
        *reinterpret_cast<int32_t*>(&rdx48) = 0;
        *reinterpret_cast<int32_t*>(&rdx48 + 4) = 0;
        r15_22 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp37) + 64);
        *reinterpret_cast<int32_t*>(&rbx21) = 1;
        *reinterpret_cast<int32_t*>(&rbx21 + 4) = 0;
        r14_26 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp37) + 0x90);
        r12_13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp37) + 0x80);
        r13_23 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp37) + 0x70);
        fun_2780(r12_13, r14_26, r12_13, r14_26);
        rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
        rax50 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp49) + 96);
        v44 = rax50;
        fun_28b0(rax50, 2);
        fun_28b0(r15_22, 2);
        rax51 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp49) - 8 + 8 - 8 + 8 + 80);
        v43 = rax51;
        fun_28b0(rax51, 2);
        fun_29f0(r13_23, 1);
        v45 = reinterpret_cast<void**>(1);
        while (eax52 = fun_2aa0(rbp12, 1, rdx48, rcx, r8, r9), !!eax52) {
            r15_53 = rbx21;
            rbx54 = r15_22;
            while (1) {
                fun_27b0(r12_13, rbx54, rbx54, rcx, r8, r9);
                fun_29d0(rbx54, r12_13, rbp12, rcx, r8, r9);
                fun_28c0(rbx54, rbx54, 1, rcx, r8, r9);
                fun_26c0(r12_13, v43, rbx54, rcx, r8, r9);
                fun_27b0(r14_26, r13_23, r12_13, rcx, r8, r9);
                rdx55 = rbp12;
                fun_29d0(r13_23, r14_26, rdx55, rcx, r8, r9);
                rax56 = r15_53;
                *reinterpret_cast<uint32_t*>(&rax57) = *reinterpret_cast<uint32_t*>(&rax56) & 31;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax57) + 4) = 0;
                if (rax57 != 1) {
                    --r15_53;
                    if (r15_53) 
                        continue;
                } else {
                    rdx55 = rbp12;
                    fun_2a40(r12_13, r13_23, rdx55, rcx, r8, r9);
                    eax58 = fun_2aa0(r12_13, 1, rdx55, rcx, r8, r9);
                    if (eax58) 
                        break;
                    fun_27a0(v44, rbx54, rdx55, rcx, r8, v44, rbx54, rdx55, rcx, r8);
                    --r15_53;
                    if (r15_53) 
                        continue;
                }
                fun_27a0(v43, rbx54, rdx55, rcx, r8, v43, rbx54, rdx55, rcx, r8);
                rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v45) + reinterpret_cast<unsigned char>(v45));
                v59 = rcx;
                if (v45) {
                    v60 = r14_26;
                    r14_61 = rbx54;
                    v62 = r13_23;
                    r13_63 = r15_53;
                    r15_64 = v45;
                    do {
                        ++r13_63;
                        fun_27b0(r12_13, r14_61, r14_61, rcx, r8, r9);
                        fun_29d0(r14_61, r12_13, rbp12, rcx, r8, r9);
                        rdx55 = reinterpret_cast<void**>(1);
                        fun_28c0(r14_61, r14_61, 1, rcx, r8, r9);
                    } while (r15_64 != r13_63);
                    rbx54 = r14_61;
                    r13_23 = v62;
                    r14_26 = v60;
                }
                fun_27a0(v44, rbx54, rdx55, rcx, r8, v44, rbx54, rdx55, rcx, r8);
                r15_53 = v45;
                v45 = v59;
            }
            v46 = r15_53;
            r15_22 = rbx54;
            v47 = r13_23;
            r13_65 = v44;
            v66 = r14_26;
            r14_67 = r12_13;
            r12_68 = v43;
            do {
                fun_27b0(r14_67, r13_65, r13_65, rcx, r8, r9);
                fun_29d0(r13_65, r14_67, rbp12, rcx, r8, r9);
                fun_28c0(r13_65, r13_65, 1, rcx, r8, r9);
                fun_26c0(r14_67, r12_68, r13_65, rcx, r8, r9);
                fun_2a40(r14_67, r14_67, rbp12, rcx, r8, r9);
                eax69 = fun_2aa0(r14_67, 1, rbp12, rcx, r8, r9);
            } while (!eax69);
            v43 = r12_68;
            r12_13 = r14_67;
            rdx70 = r12_13;
            r14_26 = v66;
            v44 = r13_65;
            rbx21 = v46;
            r13_23 = v47;
            fun_2760(rbp12, rbp12, rdx70, rcx, r8, r9);
            eax71 = fun_2aa0(r12_13, 1, rdx70, rcx, r8, r9);
            if (reinterpret_cast<uint1_t>(eax71 < 0) | reinterpret_cast<uint1_t>(eax71 == 0) || (eax72 = fun_2aa0(r12_13, 0x17ded79, rdx70, rcx, r8, r9), eax72 >= 0) && (al73 = mp_prime_p_part_0(r12_13, 0x17ded79, rdx70, rcx, r8, r9), !al73)) {
                zf74 = dev_debug == 0;
                if (!zf74) {
                    rcx = stderr;
                    fun_2a80("[composite factor--restarting pollard-rho] ", 1, 43, rcx, r8, r9);
                }
                rdx70 = v38;
                mp_factor_using_pollard_rho(r12_13, 2, rdx70, rcx, r8, r9);
            } else {
                mp_factor_insert(v38, r12_13, rdx70, rcx, r8, r9);
            }
            eax75 = fun_2aa0(rbp12, 1, rdx70, rcx, r8, r9);
            if (reinterpret_cast<uint1_t>(eax75 < 0) | reinterpret_cast<uint1_t>(eax75 == 0)) 
                goto addr_401c_45;
            eax76 = fun_2aa0(rbp12, 0x17ded79, rdx70, rcx, r8, r9);
            if (eax76 < 0) 
                goto addr_405d_47;
            al77 = mp_prime_p_part_0(rbp12, 0x17ded79, rdx70, rcx, r8, r9);
            if (al77) 
                goto addr_405d_47;
            addr_401c_45:
            fun_29d0(r15_22, r15_22, rbp12, rcx, r8, r9);
            fun_29d0(v43, v43, rbp12, rcx, r8, r9);
            rdx48 = rbp12;
            fun_29d0(v44, v44, rdx48, rcx, r8, r9);
        }
        addr_406a_49:
        fun_2660();
        rax78 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v40) - reinterpret_cast<unsigned char>(g28));
        if (!rax78) 
            goto addr_40a2_50;
        fun_2740();
        continue;
        addr_405d_47:
        mp_factor_insert(v38, rbp12, rdx70, rcx, r8, r9);
        goto addr_406a_49;
    }
    rax79 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
    if (!rax79) {
        return rax79;
    }
    addr_4318_19:
    rax80 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
    if (rax80) {
        addr_4377_21:
        fun_2740();
    } else {
        r13_81 = r12_13;
        r12_82 = rbp12;
        rcx83 = *reinterpret_cast<void***>(r12_13);
        rdx84 = *reinterpret_cast<void***>(r12_13 + 8);
        r14_85 = reinterpret_cast<struct s0*>(*reinterpret_cast<void***>(r12_13 + 16) + 0xffffffffffffffff);
        v86 = *reinterpret_cast<void***>(r12_13 + 16);
        v87 = rcx83;
        v88 = rdx84;
        if (reinterpret_cast<int64_t>(r14_85) < reinterpret_cast<int64_t>(0)) 
            goto addr_37ca_56; else 
            goto addr_361b_57;
    }
    rbx89 = rdi;
    rsp90 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x98);
    rax91 = g28;
    v92 = rax91;
    r12_93 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp90) + 96);
    r15_94 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp90) + 64);
    rbp95 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp90) + 80);
    r14_96 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp90) + 0x70);
    v97 = r15_94;
    fun_2780(r15_94, rbp95, r15_94, rbp95);
    fun_2650(r12_93, rbx89, 1, r14_96);
    rax98 = fun_25d0(r12_93);
    r13_99 = rax98;
    v100 = rax98;
    fun_2960(r15_94, r12_93, rax98, r14_96);
    fun_2680(rbp95, 2, rax98, r14_96);
    r9_101 = r13_99;
    r8_102 = r15_94;
    rcx103 = r14_96;
    al104 = mp_millerrabin(rbx89, r12_93, rbp95, rcx103, r8_102, r9_101);
    rsp105 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp90) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    if (al104) {
        r13_99 = reinterpret_cast<void**>(0x10340);
        fun_27a0(r14_96, r12_93, rbp95, rcx103, r8_102, r14_96, r12_93, rbp95, rcx103, r8_102);
        rsp106 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp105) - 8 + 8);
        rsi107 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp106) + 32);
        mp_factor(r14_96, rsi107, rbp95, rcx103, r8_102, r9_101);
        rsp108 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp106) - 8 + 8);
        goto addr_4458_60;
    }
    addr_455c_61:
    rdi109 = v97;
    rcx110 = r14_96;
    rdx111 = r12_93;
    rsi112 = rbp95;
    fun_2660();
    rax113 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v92) - reinterpret_cast<unsigned char>(g28));
    if (!rax113) {
        goto v114;
    }
    fun_2740();
    rsp115 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp105) - 8 + 8 - 8 + 8);
    while (1) {
        r15_116 = rdi109;
        rbp117 = rsi112;
        rsp118 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp115) - 8 - 8 - 8 - 8 - 8 - 8 - 0x88);
        v119 = rdx111;
        v120 = rcx110;
        rax121 = g28;
        v122 = rax121;
        rcx123 = reinterpret_cast<unsigned char>(rcx110) - (reinterpret_cast<unsigned char>(rcx110) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx110) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx110) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi109) < reinterpret_cast<unsigned char>(2)))))));
        *reinterpret_cast<int32_t*>(&rdx124) = 0;
        *reinterpret_cast<int32_t*>(&rdx124 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx125) = *reinterpret_cast<uint32_t*>(&rcx123) & 64;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx125) + 4) = 0;
        rcx126 = reinterpret_cast<uint64_t>(rcx125 + 63);
        cf127 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi109) < reinterpret_cast<unsigned char>(2));
        *reinterpret_cast<unsigned char*>(&rdx124) = cf127;
        rax128 = reinterpret_cast<void**>(-static_cast<uint64_t>(reinterpret_cast<uint1_t>(0 < static_cast<uint64_t>(cf127))) + 1);
        do {
            rsi129 = rdx124;
            rdx124 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx124) + reinterpret_cast<unsigned char>(rdx124));
            rax128 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax128) + reinterpret_cast<unsigned char>(rax128)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rsi129) >> 63));
            if (reinterpret_cast<unsigned char>(r15_116) < reinterpret_cast<unsigned char>(rax128) || r15_116 == rax128 && reinterpret_cast<unsigned char>(rbp117) <= reinterpret_cast<unsigned char>(rdx124)) {
                cf130 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx124) < reinterpret_cast<unsigned char>(rbp117));
                rdx124 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx124) - reinterpret_cast<unsigned char>(rbp117));
                rax128 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax128) - (reinterpret_cast<unsigned char>(r15_116) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax128) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r15_116) + static_cast<uint64_t>(cf130))))));
            }
            cf131 = rcx126 < 1;
            --rcx126;
        } while (!cf131);
        v132 = rdx124;
        v133 = rax128;
        tmp64_134 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx124) + reinterpret_cast<unsigned char>(rdx124));
        rax135 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax128) + reinterpret_cast<unsigned char>(rax128) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_134) < reinterpret_cast<unsigned char>(rdx124))));
        r12_136 = tmp64_134;
        r10_137 = rax135;
        if (reinterpret_cast<unsigned char>(rax135) > reinterpret_cast<unsigned char>(r15_116) || rax135 == r15_116 && reinterpret_cast<unsigned char>(tmp64_134) >= reinterpret_cast<unsigned char>(rbp117)) {
            r12_136 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_134) - reinterpret_cast<unsigned char>(rbp117));
            r10_137 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax135) - (reinterpret_cast<unsigned char>(r15_116) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax135) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r15_116) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_134) < reinterpret_cast<unsigned char>(rbp117))))))));
        }
        v138 = r10_137;
        v139 = r10_137;
        if (r15_116) 
            goto addr_468c_71;
        if (rbp117 == 1) 
            goto addr_4ad5_73;
        addr_468c_71:
        v140 = r12_136;
        r13_99 = r15_116;
        *reinterpret_cast<int32_t*>(&rbx89) = 1;
        *reinterpret_cast<int32_t*>(&rbx89 + 4) = 0;
        v141 = reinterpret_cast<void**>(1);
        r14_142 = reinterpret_cast<void**>(rsp118 + 0x70);
        r15_143 = r12_136;
        v144 = rsp118 + 0x68;
        while (1) {
            r9_145 = r13_99;
            r13_99 = rbx89;
            rax146 = reinterpret_cast<unsigned char>(rbp117) >> 1;
            rbx89 = rbp117;
            *reinterpret_cast<uint32_t*>(&rax147) = *reinterpret_cast<uint32_t*>(&rax146) & 0x7f;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax147) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rax148) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax147);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax148) + 4) = 0;
            rdx149 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax148) + reinterpret_cast<int64_t>(rax148) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax148) * reinterpret_cast<int64_t>(rax148) * reinterpret_cast<unsigned char>(rbp117)));
            rax150 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx149) + reinterpret_cast<uint64_t>(rdx149) - reinterpret_cast<uint64_t>(rdx149) * reinterpret_cast<uint64_t>(rdx149) * reinterpret_cast<unsigned char>(rbp117));
            rax151 = rbp117;
            rbp152 = r10_137;
            *reinterpret_cast<uint32_t*>(&rax153) = *reinterpret_cast<uint32_t*>(&rax151) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax153) + 4) = 0;
            v154 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax150) + reinterpret_cast<uint64_t>(rax150) - reinterpret_cast<uint64_t>(rax150) * reinterpret_cast<uint64_t>(rax150) * reinterpret_cast<unsigned char>(rbp117));
            v155 = rax153;
            while (1) {
                rax156 = mulredc2(r14_142, rbp152, r12_136, rbp152, r12_136, r9_145, rbx89, v133);
                tmp64_157 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax156) + reinterpret_cast<unsigned char>(v119));
                rdx158 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v159) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_157) < reinterpret_cast<unsigned char>(rax156))));
                v160 = rdx158;
                r12_136 = tmp64_157;
                rbp152 = rdx158;
                if (reinterpret_cast<unsigned char>(rdx158) > reinterpret_cast<unsigned char>(r9_145) || rdx158 == r9_145 && reinterpret_cast<unsigned char>(tmp64_157) >= reinterpret_cast<unsigned char>(rbx89)) {
                    rdx161 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx158) - (reinterpret_cast<unsigned char>(r9_145) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx158) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r9_145) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_157) < reinterpret_cast<unsigned char>(rbx89))))))));
                    v160 = rdx161;
                    r12_136 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_157) - reinterpret_cast<unsigned char>(rbx89));
                    rbp152 = rdx161;
                }
                r8_162 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v140) - reinterpret_cast<unsigned char>(r12_136));
                rcx163 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v138) - (reinterpret_cast<unsigned char>(rbp152) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v138) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp152) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v140) < reinterpret_cast<unsigned char>(r12_136))))))));
                if (reinterpret_cast<signed char>(rcx163) < reinterpret_cast<signed char>(0)) {
                    tmp64_164 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_162) + reinterpret_cast<unsigned char>(rbx89));
                    cf165 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_164) < reinterpret_cast<unsigned char>(r8_162));
                    r8_162 = tmp64_164;
                    rcx163 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx163) + reinterpret_cast<unsigned char>(r9_145) + static_cast<uint64_t>(cf165));
                }
                rax166 = mulredc2(r14_142, v133, v132, rcx163, r8_162, r9_145, rbx89, v133);
                v132 = rax166;
                v133 = v167;
                rax168 = r13_99;
                *reinterpret_cast<uint32_t*>(&rax169) = *reinterpret_cast<uint32_t*>(&rax168) & 31;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax169) + 4) = 0;
                rsp118 = rsp118 - 8 - 8 - 8 + 8 + 8 + 8 - 8 - 8 - 8 + 8 + 8 + 8;
                r9_145 = r9_145;
                if (rax169 == 1) {
                    if (!v155) 
                        goto addr_4cba_81;
                    if (reinterpret_cast<unsigned char>(v132) | reinterpret_cast<unsigned char>(v133)) 
                        goto addr_48c0_83;
                } else {
                    addr_47d9_84:
                    --r13_99;
                    if (r13_99) 
                        continue; else 
                        goto addr_47e3_85;
                }
                v170 = r9_145;
                rax171 = rbx89;
                if (r9_145) 
                    break;
                addr_48ee_87:
                if (!reinterpret_cast<int1_t>(rax171 == 1)) 
                    goto addr_4920_88;
                v139 = rbp152;
                r15_143 = r12_136;
                goto addr_47d9_84;
                addr_48c0_83:
                rax171 = gcd2_odd_part_0(v144, v133, v132, r9_145, rbx89, r9_145);
                rsp118 = rsp118 - 8 + 8;
                r9_145 = r9_145;
                if (v170) 
                    goto addr_4920_88; else 
                    goto addr_48ee_87;
                addr_47e3_85:
                v138 = rbp152;
                r15_143 = r12_136;
                v172 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v141) + reinterpret_cast<unsigned char>(v141));
                if (v141) {
                    v173 = r12_136;
                    rdx174 = r12_136;
                    r15_175 = r13_99;
                    r12_176 = v154;
                    r13_177 = v119;
                    rsi178 = rbp152;
                    rbp179 = r9_145;
                    do {
                        rax180 = mulredc2(r14_142, rsi178, rdx174, rsi178, rdx174, rbp179, rbx89, r12_176);
                        tmp64_181 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax180) + reinterpret_cast<unsigned char>(r13_177));
                        rcx182 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v183) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_181) < reinterpret_cast<unsigned char>(rax180))));
                        rdx174 = tmp64_181;
                        rsi178 = rcx182;
                        rsp118 = rsp118 - 8 - 8 - 8 + 8 + 8 + 8;
                        if (reinterpret_cast<unsigned char>(rcx182) > reinterpret_cast<unsigned char>(rbp179) || rcx182 == rbp179 && reinterpret_cast<unsigned char>(tmp64_181) >= reinterpret_cast<unsigned char>(rbx89)) {
                            rdx174 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_181) - reinterpret_cast<unsigned char>(rbx89));
                            rsi178 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx182) - (reinterpret_cast<unsigned char>(rbp179) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx182) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp179) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_181) < reinterpret_cast<unsigned char>(rbx89))))))));
                        }
                        ++r15_175;
                    } while (v141 != r15_175);
                    r12_136 = v173;
                    r9_145 = rbp179;
                    r15_143 = rdx174;
                    rbp152 = rsi178;
                }
                r13_99 = v141;
                v140 = r12_136;
                r12_136 = r15_143;
                v139 = rbp152;
                v141 = v172;
            }
            addr_4920_88:
            v184 = r12_136;
            r11_185 = r15_143;
            r10_186 = v139;
            v187 = r13_99;
            r15_188 = r14_142;
            r13_99 = rbx89;
            r12_189 = v140;
            r14_142 = v154;
            rbp190 = v144;
            rbx89 = r9_145;
            do {
                r9_191 = rbx89;
                rax192 = mulredc2(r15_188, r10_186, r11_185, r10_186, r11_185, r9_191, r13_99, r14_142);
                tmp64_193 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax192) + reinterpret_cast<unsigned char>(v119));
                rdx194 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v195) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_193) < reinterpret_cast<unsigned char>(rax192))));
                v139 = rdx194;
                r11_185 = tmp64_193;
                rcx196 = r13_99;
                r10_186 = rdx194;
                rsp118 = rsp118 - 8 - 8 - 8 + 8 + 8 + 8;
                if (reinterpret_cast<unsigned char>(rdx194) > reinterpret_cast<unsigned char>(rbx89) || rdx194 == rbx89 && reinterpret_cast<unsigned char>(tmp64_193) >= reinterpret_cast<unsigned char>(r13_99)) {
                    rdx197 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx194) - (reinterpret_cast<unsigned char>(rbx89) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx194) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbx89) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_193) < reinterpret_cast<unsigned char>(r13_99))))))));
                    v139 = rdx197;
                    r11_185 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_193) - reinterpret_cast<unsigned char>(r13_99));
                    r10_186 = rdx197;
                }
                rdx198 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_189) - reinterpret_cast<unsigned char>(r11_185));
                rsi199 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v138) - (reinterpret_cast<unsigned char>(r10_186) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v138) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r10_186) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_189) < reinterpret_cast<unsigned char>(r11_185))))))));
                if (reinterpret_cast<signed char>(rsi199) < reinterpret_cast<signed char>(0)) {
                    tmp64_200 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx198) + reinterpret_cast<unsigned char>(r13_99));
                    cf201 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_200) < reinterpret_cast<unsigned char>(rdx198));
                    rdx198 = tmp64_200;
                    rsi199 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi199) + reinterpret_cast<unsigned char>(rbx89) + static_cast<uint64_t>(cf201));
                }
                if (reinterpret_cast<unsigned char>(rsi199) | reinterpret_cast<unsigned char>(rdx198)) {
                    rcx196 = rbx89;
                    rax202 = gcd2_odd_part_0(rbp190, rsi199, rdx198, rcx196, r13_99, r9_191);
                    rsp118 = rsp118 - 8 + 8;
                    rdi203 = v170;
                    if (rdi203) 
                        goto addr_4a05_103;
                } else {
                    rdi203 = rbx89;
                    v170 = rbx89;
                    rax202 = r13_99;
                    if (rdi203) 
                        goto addr_4a05_103;
                }
            } while (reinterpret_cast<int1_t>(rax202 == 1));
            r8_204 = rax202;
            rax205 = reinterpret_cast<unsigned char>(rax202) >> 1;
            *reinterpret_cast<uint32_t*>(&rax206) = *reinterpret_cast<uint32_t*>(&rax205) & 0x7f;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax206) + 4) = 0;
            r13_207 = rbx89;
            r14_142 = r15_188;
            *reinterpret_cast<uint32_t*>(&rax208) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax206);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax208) + 4) = 0;
            rbx89 = v187;
            rdx209 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax208) + reinterpret_cast<int64_t>(rax208) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax208) * reinterpret_cast<int64_t>(rax208) * reinterpret_cast<unsigned char>(r8_204)));
            rdx210 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdx209) * reinterpret_cast<uint64_t>(rdx209) * reinterpret_cast<unsigned char>(r8_204));
            rax211 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx209) + reinterpret_cast<uint64_t>(rdx209) - reinterpret_cast<unsigned char>(rdx210));
            rcx196 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax211) + reinterpret_cast<uint64_t>(rax211) - reinterpret_cast<uint64_t>(rax211) * reinterpret_cast<uint64_t>(rax211) * reinterpret_cast<unsigned char>(r8_204));
            rbp117 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_99) * reinterpret_cast<unsigned char>(rcx196));
            if (reinterpret_cast<unsigned char>(r13_207) < reinterpret_cast<unsigned char>(r8_204)) {
                *reinterpret_cast<int32_t*>(&r13_99) = 0;
                *reinterpret_cast<int32_t*>(&r13_99 + 4) = 0;
            } else {
                rdx210 = __intrinsic();
                r9_191 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r13_207) - reinterpret_cast<unsigned char>(rdx210)) * reinterpret_cast<unsigned char>(rcx196));
                r13_99 = r9_191;
            }
            if (reinterpret_cast<unsigned char>(r8_204) <= reinterpret_cast<unsigned char>(1) || reinterpret_cast<unsigned char>(r8_204) > reinterpret_cast<unsigned char>(0x17ded78) && (al212 = prime_p_part_0(r8_204, rsi199, rdx210, rcx196), rsp118 = rsp118 - 8 + 8, r8_204 = r8_204, al212 == 0)) {
                rdx213 = v120;
                rsi214 = v119 + 1;
                factor_using_pollard_rho(r8_204, rsi214, rdx213, rcx196);
                rsp118 = rsp118 - 8 + 8;
            } else {
                *reinterpret_cast<int32_t*>(&rdx213) = 1;
                *reinterpret_cast<int32_t*>(&rdx213 + 4) = 0;
                rsi214 = r8_204;
                factor_insert_multiplicity(v120, rsi214, 1);
                rsp118 = rsp118 - 8 + 8;
            }
            if (!r13_99) 
                goto addr_4aa0_113;
            rsi214 = rbp117;
            rdi215 = r13_99;
            al216 = prime2_p(rdi215, rsi214);
            rsp118 = rsp118 - 8 + 8;
            if (al216) 
                goto addr_4c8b_115;
            rax217 = mod2(rsp118 + 80, v160, v184, r13_99, rbp117, r9_191);
            rsp218 = reinterpret_cast<void*>(rsp118 - 8 + 8);
            r12_136 = rax217;
            rax219 = mod2(reinterpret_cast<int64_t>(rsp218) + 88, v138, v140, r13_99, rbp117, r9_191);
            rsp220 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp218) - 8 + 8);
            v140 = rax219;
            rax221 = mod2(reinterpret_cast<int64_t>(rsp220) + 96, v139, r11_185, r13_99, rbp117, r9_191);
            rsp118 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp220) - 8 + 8);
            r10_137 = v160;
            r15_143 = rax221;
        }
        addr_4a05_103:
        if (rbx89 != rdi203) 
            goto addr_4a19_117;
        if (rax202 == r13_99) 
            goto addr_4ce0_119;
        addr_4a19_117:
        rbx89 = reinterpret_cast<void**>(0xd5c0);
        rsi214 = rax202;
        v141 = rax202;
        rax222 = reinterpret_cast<unsigned char>(rax202) >> 1;
        *reinterpret_cast<uint32_t*>(&rax223) = *reinterpret_cast<uint32_t*>(&rax222) & 0x7f;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax223) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax224) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(0xd5c0) + reinterpret_cast<uint64_t>(rax223));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax224) + 4) = 0;
        rdx225 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax224) + reinterpret_cast<int64_t>(rax224) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax224) * reinterpret_cast<int64_t>(rax224) * reinterpret_cast<unsigned char>(rax202)));
        rax226 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx225) + reinterpret_cast<uint64_t>(rdx225) - reinterpret_cast<uint64_t>(rdx225) * reinterpret_cast<uint64_t>(rdx225) * reinterpret_cast<unsigned char>(rax202));
        rdx213 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax226) + reinterpret_cast<uint64_t>(rax226) - reinterpret_cast<uint64_t>(rax226) * reinterpret_cast<uint64_t>(rax226) * reinterpret_cast<unsigned char>(rax202));
        rbp117 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_99) * reinterpret_cast<unsigned char>(rdx213));
        al227 = prime2_p(rdi203, rsi214);
        rsp118 = rsp118 - 8 + 8;
        if (!al227) 
            goto addr_4c4f_120;
        if (!v170) 
            goto addr_4ca3_122;
        rdi215 = v120;
        if (!*reinterpret_cast<void***>(rdi215 + 8)) 
            goto addr_4a94_124;
        addr_4cfe_125:
        factor_insert_large_part_0();
        r8_228 = rdi215;
        r9_229 = rdx213;
        v230 = r13_99;
        rbp231 = rsi214;
        v232 = rbx89;
        rsp233 = rsp118 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 56;
        *reinterpret_cast<unsigned char*>(rdx213 + 0xfa) = 0;
        *reinterpret_cast<void***>(rdx213 + 8) = reinterpret_cast<void**>(0);
        if (rdi215) 
            goto addr_4d50_127;
        if (reinterpret_cast<unsigned char>(rsi214) <= reinterpret_cast<unsigned char>(1)) 
            goto addr_4d41_129;
        addr_4d50_127:
        if (*reinterpret_cast<unsigned char*>(&rbp231) & 1) {
            addr_4df3_130:
            if (!r8_228) {
                *reinterpret_cast<int32_t*>(&r12_234) = 3;
                *reinterpret_cast<int32_t*>(&r12_234 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r13_235) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_235) + 4) = 0;
                r11_236 = reinterpret_cast<void**>(0x5555555555555555);
                r10_237 = reinterpret_cast<void**>(0xaaaaaaaaaaaaaaab);
            } else {
                r14_238 = reinterpret_cast<void***>(0xd648);
                *reinterpret_cast<uint32_t*>(&rbx239) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx239) + 4) = 0;
                r15_240 = 0xaaaaaaaaaaaaaaab;
                *reinterpret_cast<int32_t*>(&r12_234) = 3;
                *reinterpret_cast<int32_t*>(&r12_234 + 4) = 0;
                while (1) {
                    rcx241 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp231) * r15_240);
                    rdx242 = __intrinsic();
                    if (reinterpret_cast<unsigned char>(rdx242) <= reinterpret_cast<unsigned char>(r8_228)) {
                        r10_243 = *r14_238;
                        while ((r13_244 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(r8_228) - reinterpret_cast<unsigned char>(rdx242)) * r15_240), reinterpret_cast<unsigned char>(r13_244) <= reinterpret_cast<unsigned char>(r10_243)) && (factor_insert_multiplicity(r9_229, r12_234, 1), rsp233 = rsp233 - 8 + 8, r8_228 = r13_244, r9_229 = r9_229, r10_243 = r10_243, rbp231 = rcx241, rdx242 = __intrinsic(), reinterpret_cast<unsigned char>(rdx242) <= reinterpret_cast<unsigned char>(r13_244))) {
                            rcx241 = reinterpret_cast<void**>(r15_240 * reinterpret_cast<unsigned char>(rcx241));
                        }
                    }
                    *reinterpret_cast<uint32_t*>(&rax245) = rbx239->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax245) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&r13_235) = *reinterpret_cast<uint32_t*>(&rbx239);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_235) + 4) = 0;
                    r14_238 = r14_238 + 16;
                    r12_234 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_234) + reinterpret_cast<uint64_t>(rax245));
                    if (!r8_228) 
                        break;
                    if (*reinterpret_cast<uint32_t*>(&rbx239) > 0x29b) 
                        break;
                    r15_240 = *reinterpret_cast<uint64_t*>(r14_238 - 8);
                    rbx239 = reinterpret_cast<struct s30*>(&rbx239->pad8);
                }
                if (*reinterpret_cast<uint32_t*>(&r13_235) > 0x29b) 
                    goto addr_5200_141; else 
                    goto addr_4edd_142;
            }
        } else {
            if (rbp231) {
                __asm__("bsf rdx, rbp");
                ecx246 = 64 - *reinterpret_cast<int32_t*>(&rdx213);
                ecx247 = *reinterpret_cast<int32_t*>(&rdx213);
                rbp231 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp231) >> *reinterpret_cast<signed char*>(&ecx247)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_228) << *reinterpret_cast<unsigned char*>(&ecx246)));
                factor_insert_multiplicity(r9_229, 2, *reinterpret_cast<signed char*>(&rdx213));
                rsp233 = rsp233 - 8 + 8;
                r8_228 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_228) >> *reinterpret_cast<signed char*>(&ecx247));
                r9_229 = r9_229;
                goto addr_4df3_130;
            } else {
                __asm__("bsf rcx, r8");
                edx248 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rcx196 + 64));
                rbp231 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_228) >> *reinterpret_cast<signed char*>(&rcx196));
                *reinterpret_cast<int32_t*>(&r12_234) = 3;
                *reinterpret_cast<int32_t*>(&r12_234 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r13_235) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_235) + 4) = 0;
                factor_insert_multiplicity(r9_229, 2, *reinterpret_cast<signed char*>(&edx248));
                rsp233 = rsp233 - 8 + 8;
                r9_229 = r9_229;
                *reinterpret_cast<uint32_t*>(&r8_228) = 0;
                *reinterpret_cast<int32_t*>(&r8_228 + 4) = 0;
                r11_236 = reinterpret_cast<void**>(0x5555555555555555);
                r10_237 = reinterpret_cast<void**>(0xaaaaaaaaaaaaaaab);
            }
        }
        addr_4ef9_146:
        *reinterpret_cast<int32_t*>(&rbx249) = static_cast<int32_t>(r13_235 + 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx249) + 4) = 0;
        r13_235 = rbx249;
        rbx239 = reinterpret_cast<struct s30*>((rbx249 << 4) + reinterpret_cast<unsigned char>(0xd640));
        while (1) {
            *reinterpret_cast<int32_t*>(&r15_240) = static_cast<int32_t>(r13_235 - 1);
            rcx250 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp231) * reinterpret_cast<unsigned char>(r10_237));
            if (reinterpret_cast<unsigned char>(r11_236) >= reinterpret_cast<unsigned char>(rcx250)) {
                do {
                    factor_insert_multiplicity(r9_229, r12_234, 1);
                    rsp233 = rsp233 - 8 + 8;
                    r10_237 = r10_237;
                    r11_236 = r11_236;
                    r9_229 = r9_229;
                    rbp231 = rcx250;
                    rcx250 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx250) * reinterpret_cast<unsigned char>(r10_237));
                    r8_228 = r8_228;
                } while (reinterpret_cast<unsigned char>(r11_236) >= reinterpret_cast<unsigned char>(rcx250));
                rdx251 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx239->f0) * reinterpret_cast<unsigned char>(rbp231));
                if (reinterpret_cast<unsigned char>(rbx239->f8) < reinterpret_cast<unsigned char>(rdx251)) 
                    goto addr_4f29_150;
                goto addr_5050_152;
            }
            rdx251 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx239->f0) * reinterpret_cast<unsigned char>(rbp231));
            if (reinterpret_cast<unsigned char>(rbx239->f8) >= reinterpret_cast<unsigned char>(rdx251)) {
                addr_5050_152:
                *reinterpret_cast<uint32_t*>(&rcx252) = *reinterpret_cast<uint32_t*>(&r13_235);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx252) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rcx253) = rcx252->f10340;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx253) + 4) = 0;
                rcx254 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx253) + reinterpret_cast<unsigned char>(r12_234));
            } else {
                addr_4f29_150:
                rdx255 = reinterpret_cast<void**>(rbx239->f10 * reinterpret_cast<unsigned char>(rbp231));
                if (reinterpret_cast<unsigned char>(rdx255) <= reinterpret_cast<unsigned char>(rbx239->f18)) 
                    goto addr_50b0_154; else 
                    goto addr_4f3b_155;
            }
            do {
                rbp231 = rdx251;
                factor_insert_multiplicity(r9_229, rcx254, 1);
                rsp233 = rsp233 - 8 + 8;
                r9_229 = r9_229;
                rcx254 = rcx254;
                r8_228 = r8_228;
                rdx251 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx239->f0) * reinterpret_cast<unsigned char>(rbp231));
            } while (reinterpret_cast<unsigned char>(rdx251) <= reinterpret_cast<unsigned char>(rbx239->f8));
            rdx255 = reinterpret_cast<void**>(rbx239->f10 * reinterpret_cast<unsigned char>(rbp231));
            if (reinterpret_cast<unsigned char>(rdx255) > reinterpret_cast<unsigned char>(rbx239->f18)) {
                addr_4f3b_155:
                rdx256 = reinterpret_cast<void**>(rbx239->f20 * reinterpret_cast<unsigned char>(rbp231));
                if (reinterpret_cast<unsigned char>(rdx256) <= reinterpret_cast<unsigned char>(rbx239->f28)) {
                    addr_5120_158:
                    *reinterpret_cast<int32_t*>(&rcx257) = static_cast<int32_t>(r13_235 + 2);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx257) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi258) = *reinterpret_cast<uint32_t*>(&r13_235);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi258) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi259) = rsi258->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi259) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rcx260) = rcx257->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx260) + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rsi261) = static_cast<int32_t>(r13_235 + 1);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi261) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi262) = rsi261->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi262) + 4) = 0;
                    rcx263 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx260) + reinterpret_cast<int64_t>(rsi259) + reinterpret_cast<unsigned char>(r12_234) + reinterpret_cast<uint64_t>(rsi262));
                } else {
                    addr_4f4d_159:
                    rdx264 = reinterpret_cast<void**>(rbx239->f30 * reinterpret_cast<unsigned char>(rbp231));
                    if (reinterpret_cast<unsigned char>(rdx264) <= reinterpret_cast<unsigned char>(rbx239->f38)) 
                        goto addr_5198_160; else 
                        goto addr_4f5f_161;
                }
            } else {
                goto addr_50b0_154;
            }
            do {
                rbp231 = rdx256;
                v265 = rcx263;
                factor_insert_multiplicity(r9_229, rcx263, 1);
                rsp233 = rsp233 - 8 + 8;
                r9_229 = r9_229;
                rcx263 = v265;
                r8_228 = r8_228;
                rdx256 = reinterpret_cast<void**>(rbx239->f20 * reinterpret_cast<unsigned char>(rbp231));
            } while (reinterpret_cast<unsigned char>(rdx256) <= reinterpret_cast<unsigned char>(rbx239->f28));
            rdx264 = reinterpret_cast<void**>(rbx239->f30 * reinterpret_cast<unsigned char>(rbp231));
            if (reinterpret_cast<unsigned char>(rdx264) > reinterpret_cast<unsigned char>(rbx239->f38)) {
                addr_4f5f_161:
                rdx266 = reinterpret_cast<void**>(rbx239->f40 * reinterpret_cast<unsigned char>(rbp231));
                if (reinterpret_cast<unsigned char>(rdx266) <= reinterpret_cast<unsigned char>(rbx239->f48)) {
                    rbp231 = rdx266;
                    ecx267 = static_cast<uint32_t>(r13_235 + 5);
                    while (1) {
                        edx268 = *reinterpret_cast<uint32_t*>(&r13_235);
                        rsi269 = r12_234;
                        do {
                            *reinterpret_cast<uint32_t*>(&rdi270) = edx268;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi270) + 4) = 0;
                            ++edx268;
                            *reinterpret_cast<uint32_t*>(&rdi271) = rdi270->f10340;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi271) + 4) = 0;
                            rsi269 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi269) + reinterpret_cast<uint64_t>(rdi271));
                        } while (edx268 != ecx267);
                        v265 = r8_228;
                        factor_insert_multiplicity(r9_229, rsi269, 1);
                        rsp233 = rsp233 - 8 + 8;
                        r9_229 = r9_229;
                        r8_228 = v265;
                        ecx267 = ecx267;
                        rdx272 = reinterpret_cast<void**>(rbx239->f40 * reinterpret_cast<unsigned char>(rbp231));
                        if (reinterpret_cast<unsigned char>(rdx272) > reinterpret_cast<unsigned char>(rbx239->f48)) 
                            break;
                        rbp231 = rdx272;
                    }
                }
            } else {
                goto addr_5198_160;
            }
            rdx273 = reinterpret_cast<void**>(rbx239->f50 * reinterpret_cast<unsigned char>(rbp231));
            if (reinterpret_cast<unsigned char>(rdx273) <= reinterpret_cast<unsigned char>(rbx239->f58)) {
                rbp231 = rdx273;
                ecx274 = static_cast<uint32_t>(r13_235 + 6);
                while (1) {
                    edx275 = *reinterpret_cast<uint32_t*>(&r13_235);
                    rsi276 = r12_234;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdi277) = edx275;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi277) + 4) = 0;
                        ++edx275;
                        *reinterpret_cast<uint32_t*>(&rdi278) = rdi277->f10340;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi278) + 4) = 0;
                        rsi276 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi276) + reinterpret_cast<uint64_t>(rdi278));
                    } while (edx275 != ecx274);
                    v265 = r8_228;
                    factor_insert_multiplicity(r9_229, rsi276, 1);
                    rsp233 = rsp233 - 8 + 8;
                    r9_229 = r9_229;
                    r8_228 = v265;
                    ecx274 = ecx274;
                    rdx279 = reinterpret_cast<void**>(rbx239->f50 * reinterpret_cast<unsigned char>(rbp231));
                    if (reinterpret_cast<unsigned char>(rdx279) > reinterpret_cast<unsigned char>(rbx239->f58)) 
                        break;
                    rbp231 = rdx279;
                }
            }
            rdx280 = reinterpret_cast<void**>(rbx239->f60 * reinterpret_cast<unsigned char>(rbp231));
            if (reinterpret_cast<unsigned char>(rdx280) <= reinterpret_cast<unsigned char>(rbx239->f68)) {
                rbp231 = rdx280;
                ecx281 = static_cast<uint32_t>(r13_235 + 7);
                while (1) {
                    edx282 = *reinterpret_cast<uint32_t*>(&r13_235);
                    rsi283 = r12_234;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdi284) = edx282;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi284) + 4) = 0;
                        ++edx282;
                        *reinterpret_cast<uint32_t*>(&rdi285) = rdi284->f10340;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi285) + 4) = 0;
                        rsi283 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi283) + reinterpret_cast<uint64_t>(rdi285));
                    } while (edx282 != ecx281);
                    v265 = r8_228;
                    factor_insert_multiplicity(r9_229, rsi283, 1);
                    rsp233 = rsp233 - 8 + 8;
                    r9_229 = r9_229;
                    r8_228 = v265;
                    ecx281 = ecx281;
                    rdx286 = reinterpret_cast<void**>(rbx239->f60 * reinterpret_cast<unsigned char>(rbp231));
                    if (reinterpret_cast<unsigned char>(rdx286) > reinterpret_cast<unsigned char>(rbx239->f68)) 
                        break;
                    rbp231 = rdx286;
                }
            }
            *reinterpret_cast<int32_t*>(&rax287) = *reinterpret_cast<int32_t*>(&r15_240);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax287) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rax288) = *reinterpret_cast<unsigned char*>(0x10080 + rax287);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax288) + 4) = 0;
            r12_234 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_234) + reinterpret_cast<uint64_t>(rax288));
            if (reinterpret_cast<unsigned char>(rbp231) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r12_234) * reinterpret_cast<unsigned char>(r12_234))) 
                break;
            *reinterpret_cast<uint32_t*>(&rax289) = static_cast<uint32_t>(r13_235 + 7);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax289) + 4) = 0;
            rbx239 = reinterpret_cast<struct s30*>(reinterpret_cast<uint64_t>(rbx239) + 0x80);
            *reinterpret_cast<uint32_t*>(&r13_235) = *reinterpret_cast<uint32_t*>(&r13_235) + 8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_235) + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&rax289) > 0x29b) 
                break;
            rax290 = reinterpret_cast<struct s38*>((rax289 << 4) + reinterpret_cast<unsigned char>(0xd640));
            r10_237 = rax290->f0;
            r11_236 = rax290->f8;
            continue;
            addr_5198_160:
            rbp231 = rdx264;
            ecx291 = static_cast<uint32_t>(r13_235 + 4);
            while (1) {
                edx292 = *reinterpret_cast<uint32_t*>(&r13_235);
                rsi293 = r12_234;
                do {
                    *reinterpret_cast<uint32_t*>(&rdi294) = edx292;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi294) + 4) = 0;
                    ++edx292;
                    *reinterpret_cast<uint32_t*>(&rdi295) = rdi294->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi295) + 4) = 0;
                    rsi293 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi293) + reinterpret_cast<uint64_t>(rdi295));
                } while (ecx291 != edx292);
                factor_insert_multiplicity(r9_229, rsi293, 1);
                rsp233 = rsp233 - 8 + 8;
                r9_229 = r9_229;
                r8_228 = r8_228;
                ecx291 = ecx291;
                rdx296 = reinterpret_cast<void**>(rbx239->f30 * reinterpret_cast<unsigned char>(rbp231));
                if (reinterpret_cast<unsigned char>(rdx296) > reinterpret_cast<unsigned char>(rbx239->f38)) 
                    goto addr_4f5f_161;
                rbp231 = rdx296;
            }
            addr_50b0_154:
            *reinterpret_cast<int32_t*>(&rcx297) = static_cast<int32_t>(r13_235 + 1);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx297) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rsi298) = *reinterpret_cast<uint32_t*>(&r13_235);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi298) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx299) = rcx297->f10340;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx299) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rsi300) = rsi298->f10340;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi300) + 4) = 0;
            rcx301 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx299) + reinterpret_cast<int64_t>(rsi300) + reinterpret_cast<unsigned char>(r12_234));
            do {
                rbp231 = rdx255;
                factor_insert_multiplicity(r9_229, rcx301, 1);
                rsp233 = rsp233 - 8 + 8;
                r9_229 = r9_229;
                rcx301 = rcx301;
                r8_228 = r8_228;
                rdx255 = reinterpret_cast<void**>(rbx239->f10 * reinterpret_cast<unsigned char>(rbp231));
            } while (reinterpret_cast<unsigned char>(rdx255) <= reinterpret_cast<unsigned char>(rbx239->f18));
            rdx256 = reinterpret_cast<void**>(rbx239->f20 * reinterpret_cast<unsigned char>(rbp231));
            if (reinterpret_cast<unsigned char>(rdx256) > reinterpret_cast<unsigned char>(rbx239->f28)) 
                goto addr_4f4d_159;
            goto addr_5120_158;
        }
        addr_5200_141:
        if (!r8_228) 
            goto addr_5209_193;
        rdi302 = r8_228;
        v303 = r9_229;
        v304 = r8_228;
        al305 = prime2_p(rdi302, rbp231);
        rsp233 = rsp233 - 8 + 8;
        if (al305) 
            goto addr_53a9_195;
        rsi112 = rbp231;
        rcx110 = v303;
        *reinterpret_cast<int32_t*>(&rdx111) = 1;
        *reinterpret_cast<int32_t*>(&rdx111 + 4) = 0;
        rbx89 = v232;
        rdi109 = v304;
        r13_99 = v230;
        rsp115 = reinterpret_cast<void*>(rsp233 + 56 + 8 + 8 + 8 + 8 + 8 + 8);
        continue;
        addr_4edd_142:
        *reinterpret_cast<uint32_t*>(&rax306) = *reinterpret_cast<uint32_t*>(&r13_235);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax306) + 4) = 0;
        rax307 = reinterpret_cast<struct s42*>((rax306 << 4) + 0xd640);
        r10_237 = rax307->f0;
        r11_236 = rax307->f8;
        goto addr_4ef9_146;
        addr_4c8b_115:
        if (*reinterpret_cast<void***>(v120 + 8)) 
            goto addr_4cfe_125; else 
            goto addr_4c97_197;
    }
    addr_4cba_81:
    fun_2810("b0 & 1", "src/factor.c", 0x1e0, "gcd2_odd", r8_162, r9_145);
    do {
        fun_2740();
        addr_4ce0_119:
        factor_using_pollard_rho2(rbx89, r13_99, v119 + 1, v120);
        addr_4ad5_73:
        rax308 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(g28));
    } while (rax308);
    goto v309;
    addr_4aa0_113:
    if (reinterpret_cast<unsigned char>(rbp117) <= reinterpret_cast<unsigned char>(1)) {
        addr_4c74_200:
        factor_using_pollard_rho(rbp117, v119, v120, rcx196);
        goto addr_4ad5_73;
    } else {
        addr_4aaa_201:
        if (reinterpret_cast<unsigned char>(rbp117) <= reinterpret_cast<unsigned char>(0x17ded78) || (al310 = prime_p_part_0(rbp117, rsi214, rdx213, rcx196), !!al310)) {
            factor_insert_multiplicity(v120, rbp117, 1, v120, rbp117, 1);
            goto addr_4ad5_73;
        }
    }
    addr_4c4f_120:
    rcx196 = v120;
    rsi214 = v141;
    rdx213 = v119 + 1;
    factor_using_pollard_rho2(v170, rsi214, rdx213, rcx196);
    if (reinterpret_cast<unsigned char>(rbp117) > reinterpret_cast<unsigned char>(1)) 
        goto addr_4aaa_201; else 
        goto addr_4c74_200;
    addr_4ca3_122:
    *reinterpret_cast<int32_t*>(&rdx213) = 1;
    *reinterpret_cast<int32_t*>(&rdx213 + 4) = 0;
    rsi214 = v141;
    factor_insert_multiplicity(v120, rsi214, 1);
    goto addr_4aa0_113;
    addr_4a94_124:
    *reinterpret_cast<void***>(v120) = v141;
    *reinterpret_cast<void***>(v120 + 8) = v170;
    goto addr_4aa0_113;
    addr_5209_193:
    if (reinterpret_cast<unsigned char>(rbp231) <= reinterpret_cast<unsigned char>(1)) {
        addr_4d41_129:
        goto v141;
    } else {
        al311 = prime2_p(0, rbp231);
        r9_312 = r9_229;
        if (al311) {
            rsi313 = rbp231;
            rdi314 = r9_312;
        } else {
            rdi315 = rbp231;
            rdx316 = r9_312;
            *reinterpret_cast<int32_t*>(&rsi317) = 1;
            *reinterpret_cast<int32_t*>(&rsi317 + 4) = 0;
            goto addr_5740_206;
        }
    }
    addr_2f20_207:
    *reinterpret_cast<uint32_t*>(&rax318) = *reinterpret_cast<unsigned char*>(rdi314 + 0xfa);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax318) + 4) = 0;
    r8_319 = rsi313;
    r11_320 = reinterpret_cast<void***>(rdi314 + 16);
    r10_321 = reinterpret_cast<signed char*>(rdi314 + 0xe0);
    r9d322 = *reinterpret_cast<uint32_t*>(&rax318);
    if (!*reinterpret_cast<uint32_t*>(&rax318)) 
        goto addr_2fa1_208;
    ebx323 = static_cast<int32_t>(rax318 - 1);
    rcx324 = reinterpret_cast<void*>(static_cast<int64_t>(ebx323));
    rax325 = rcx324;
    do {
        *reinterpret_cast<int32_t*>(&rsi326) = *reinterpret_cast<int32_t*>(&rax325);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi326) + 4) = 0;
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi314 + reinterpret_cast<uint64_t>(rax325) * 8) + 16)) <= reinterpret_cast<unsigned char>(r8_319)) 
            break;
        rax325 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax325) - 1);
        *reinterpret_cast<int32_t*>(&rsi326) = *reinterpret_cast<int32_t*>(&rax325);
    } while (*reinterpret_cast<int32_t*>(&rax325) != -1);
    goto addr_2f80_212;
    if (*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi314 + reinterpret_cast<uint64_t>(rax325) * 8) + 16) == r8_319) {
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_321) + reinterpret_cast<uint64_t>(rax325)) = reinterpret_cast<signed char>(*reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_321) + reinterpret_cast<uint64_t>(rax325)) + 1);
        goto v141;
    }
    rax327 = reinterpret_cast<void*>(static_cast<int64_t>(static_cast<int32_t>(rsi326 + 1)));
    r11_320 = r11_320 + reinterpret_cast<uint64_t>(rax327) * 8;
    r10_321 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_321) + reinterpret_cast<uint64_t>(rax327));
    if (*reinterpret_cast<int32_t*>(&rsi326) >= ebx323) {
        addr_2fa1_208:
        r9d328 = r9d322 + 1;
        *r11_320 = r8_319;
        *r10_321 = 1;
        *reinterpret_cast<unsigned char*>(rdi314 + 0xfa) = *reinterpret_cast<unsigned char*>(&r9d328);
        goto v141;
    }
    do {
        addr_2f80_212:
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi314 + reinterpret_cast<uint64_t>(rcx324) * 8) + 24) = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi314 + reinterpret_cast<uint64_t>(rcx324) * 8) + 16);
        eax329 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi314) + reinterpret_cast<uint64_t>(rcx324) + 0xe0);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi314) + reinterpret_cast<uint64_t>(rcx324) + 0xe1) = *reinterpret_cast<signed char*>(&eax329);
        rcx324 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx324) - 1);
    } while (*reinterpret_cast<int32_t*>(&rsi326) < *reinterpret_cast<int32_t*>(&rcx324));
    goto addr_2fa1_208;
    addr_5740_206:
    v303 = rsi317;
    v265 = rdx316;
    if (reinterpret_cast<unsigned char>(rdi315) <= reinterpret_cast<unsigned char>(1)) {
        addr_5b07_217:
        fun_2810("(1) < (n)", "src/factor.c", 0x5be, "factor_using_pollard_rho", r8_228, r9_312);
    } else {
        r12_330 = rdi315;
        do {
            rcx331 = r12_330;
            esi332 = 64;
            *reinterpret_cast<int32_t*>(&rax333) = 1;
            *reinterpret_cast<int32_t*>(&rax333 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx334) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx334) + 4) = 0;
            rdi335 = reinterpret_cast<void*>(0);
            do {
                r8_228 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx331) << 63);
                rcx331 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx331) >> 1);
                rdx334 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx334) >> 1 | reinterpret_cast<unsigned char>(r8_228));
                if (reinterpret_cast<unsigned char>(rcx331) < reinterpret_cast<unsigned char>(rax333) || rcx331 == rax333 && reinterpret_cast<uint64_t>(rdx334) <= reinterpret_cast<uint64_t>(rdi335)) {
                    cf336 = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi335) < reinterpret_cast<uint64_t>(rdx334));
                    rdi335 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi335) - reinterpret_cast<uint64_t>(rdx334));
                    rax333 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax333) - (reinterpret_cast<unsigned char>(rcx331) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax333) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx331) + static_cast<uint64_t>(cf336))))));
                }
                --esi332;
            } while (esi332);
            *reinterpret_cast<int32_t*>(&rbp337) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp337) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r15_338) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_338) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r14_339) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_339) + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rbp337) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_330) - reinterpret_cast<uint64_t>(rdi335) > reinterpret_cast<uint64_t>(rdi335));
            rbp340 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbp337) & reinterpret_cast<unsigned char>(r12_330)) + (reinterpret_cast<uint64_t>(rdi335) + reinterpret_cast<uint64_t>(rdi335) - reinterpret_cast<unsigned char>(r12_330)));
            rcx341 = rbp340;
            while (reinterpret_cast<unsigned char>(v303) < reinterpret_cast<unsigned char>(r12_330)) {
                r11_342 = rcx341;
                rax343 = reinterpret_cast<unsigned char>(r12_330) >> 1;
                *reinterpret_cast<uint32_t*>(&rax344) = *reinterpret_cast<uint32_t*>(&rax343) & 0x7f;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax344) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax345) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax344);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax345) + 4) = 0;
                rdx346 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax345) + reinterpret_cast<int64_t>(rax345) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax345) * reinterpret_cast<int64_t>(rax345) * reinterpret_cast<unsigned char>(r12_330)));
                rax347 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx346) + reinterpret_cast<uint64_t>(rdx346) - reinterpret_cast<uint64_t>(rdx346) * reinterpret_cast<uint64_t>(rdx346) * reinterpret_cast<unsigned char>(r12_330));
                r8_348 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax347) + reinterpret_cast<uint64_t>(rax347) - reinterpret_cast<uint64_t>(rax347) * reinterpret_cast<uint64_t>(rax347) * reinterpret_cast<unsigned char>(r12_330));
                r10_349 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_330) - reinterpret_cast<unsigned char>(v303));
                r9_350 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v303) - reinterpret_cast<unsigned char>(r12_330));
                while (1) {
                    rax351 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rax351 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax351) + reinterpret_cast<unsigned char>(r12_330));
                    }
                    rbp340 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rbp340) - (reinterpret_cast<unsigned char>(rbp340) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp340) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp340) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax351) < reinterpret_cast<uint64_t>(r10_349))))))) & reinterpret_cast<unsigned char>(r12_330)) + (reinterpret_cast<uint64_t>(rax351) + reinterpret_cast<uint64_t>(r9_350)));
                    r13_352 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        r13_352 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_352) + reinterpret_cast<unsigned char>(r12_330));
                    }
                    rax353 = r14_339;
                    *reinterpret_cast<uint32_t*>(&rax354) = *reinterpret_cast<uint32_t*>(&rax353) & 31;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax354) + 4) = 0;
                    if (rax354 == 1) {
                        rdi355 = r13_352;
                        rax356 = gcd_odd(rdi355, r12_330);
                        if (!reinterpret_cast<int1_t>(rax356 == 1)) 
                            break;
                    }
                    --r14_339;
                    if (r14_339) 
                        continue;
                    rcx357 = r15_338 + r15_338;
                    if (!r15_338) {
                        r15_338 = rcx357;
                        r11_342 = rbp340;
                    } else {
                        do {
                            rdi358 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                            rax359 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi358) + reinterpret_cast<unsigned char>(r12_330));
                            if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                                rdi358 = rax359;
                            }
                            ++r14_339;
                        } while (r15_338 != r14_339);
                        r11_342 = rbp340;
                        r15_338 = rcx357;
                        rbp340 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax359) - (reinterpret_cast<uint64_t>(rax359) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax359) < reinterpret_cast<uint64_t>(rax359) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi358) < reinterpret_cast<uint64_t>(r10_349)))) & reinterpret_cast<unsigned char>(r12_330)) + (reinterpret_cast<uint64_t>(rdi358) + reinterpret_cast<uint64_t>(r9_350)));
                    }
                }
                r9_312 = r8_348;
                r8_228 = r11_342;
                r11_360 = r9_350;
                do {
                    rsi361 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    rax362 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi361) + reinterpret_cast<unsigned char>(r12_330));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rsi361 = rax362;
                    }
                    rbx363 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax362) - (reinterpret_cast<uint64_t>(rax362) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax362) < reinterpret_cast<uint64_t>(rax362) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rsi361) < reinterpret_cast<uint64_t>(r10_349)))) & reinterpret_cast<unsigned char>(r12_330)) + (reinterpret_cast<uint64_t>(rsi361) + reinterpret_cast<uint64_t>(r11_360)));
                    rsi364 = r12_330;
                    rdi355 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rdi355) - (reinterpret_cast<unsigned char>(rdi355) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi355) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rdi355) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_228) < reinterpret_cast<unsigned char>(rbx363))))))) & reinterpret_cast<unsigned char>(r12_330)) + (reinterpret_cast<unsigned char>(r8_228) - reinterpret_cast<unsigned char>(rbx363)));
                    rax365 = gcd_odd(rdi355, rsi364);
                } while (rax365 == 1);
                rcx366 = r8_228;
                r11_367 = rax365;
                if (rax365 == r12_330) 
                    goto addr_5af7_246;
                rdx368 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_330) % reinterpret_cast<unsigned char>(r11_367));
                rax369 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_330) / reinterpret_cast<unsigned char>(r11_367));
                r8_370 = rax369;
                r12_330 = rax369;
                if (reinterpret_cast<unsigned char>(r11_367) > reinterpret_cast<unsigned char>(1) && (reinterpret_cast<unsigned char>(r11_367) <= reinterpret_cast<unsigned char>(0x17ded78) || (al371 = prime_p_part_0(r11_367, rsi364, rdx368, rcx366), r11_367 = r11_367, rcx366 = rcx366, r8_370 = rax369, !!al371))) {
                    *reinterpret_cast<int32_t*>(&rdx372) = 1;
                    *reinterpret_cast<int32_t*>(&rdx372 + 4) = 0;
                    rsi373 = r11_367;
                    factor_insert_multiplicity(v265, rsi373, 1);
                    r8_228 = r8_370;
                    rcx374 = rcx366;
                    zf375 = reinterpret_cast<int1_t>(r8_228 == 1);
                    if (reinterpret_cast<unsigned char>(r8_228) <= reinterpret_cast<unsigned char>(1)) 
                        goto addr_5aca_249; else 
                        goto addr_5a2f_250;
                }
                rdx372 = v265;
                rsi373 = v303 + 1;
                factor_using_pollard_rho(r11_367, rsi373, rdx372, rcx366);
                r8_228 = r8_370;
                rcx374 = rcx366;
                zf375 = reinterpret_cast<int1_t>(r8_228 == 1);
                if (reinterpret_cast<unsigned char>(r8_228) > reinterpret_cast<unsigned char>(1)) {
                    addr_5a2f_250:
                    if (reinterpret_cast<unsigned char>(r8_228) <= reinterpret_cast<unsigned char>(0x17ded78)) 
                        goto addr_5ad7_252;
                    al376 = prime_p_part_0(r8_228, rsi373, rdx372, rcx374);
                    r8_228 = r8_228;
                    if (al376) 
                        goto addr_5ad7_252;
                } else {
                    addr_5aca_249:
                    if (zf375) 
                        goto addr_5b45_254; else 
                        goto addr_5acc_255;
                }
                rbp340 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp340) % reinterpret_cast<unsigned char>(r8_228));
                rcx341 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx374) % reinterpret_cast<unsigned char>(r8_228));
                continue;
                addr_5acc_255:
                *reinterpret_cast<int32_t*>(&rcx341) = 0;
                *reinterpret_cast<int32_t*>(&rcx341 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rbp340) = 0;
                *reinterpret_cast<int32_t*>(&rbp340 + 4) = 0;
            }
            break;
            addr_5af7_246:
            ++v303;
        } while (reinterpret_cast<unsigned char>(r12_330) > reinterpret_cast<unsigned char>(1));
        goto addr_5b07_217;
    }
    fun_2810("a < n", "src/factor.c", 0x5c4, "factor_using_pollard_rho", r8_228, r9_312);
    addr_5b45_254:
    goto v141;
    addr_5ad7_252:
    rdi314 = v265;
    rsi313 = r8_228;
    goto addr_2f20_207;
    addr_53a9_195:
    if (!*reinterpret_cast<void***>(v303 + 8)) {
        *reinterpret_cast<void***>(v303) = rbp231;
        *reinterpret_cast<void***>(v303 + 8) = v304;
        goto addr_4d41_129;
    }
    factor_insert_large_part_0();
    r14_377 = reinterpret_cast<struct s0*>(rdi302 + 0xffffffffffffffff);
    rbp378 = rdi302;
    rsp379 = reinterpret_cast<struct s0**>(rsp233 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x148);
    rax380 = g28;
    v381 = rax380;
    eax382 = 0;
    v383 = r14_377;
    if (*reinterpret_cast<uint32_t*>(&rdi302) & 1) 
        goto addr_5478_261;
    v384 = 0;
    r14_377 = v383;
    addr_5490_263:
    rcx385 = rbp378;
    *reinterpret_cast<int32_t*>(&r15_386) = 0;
    *reinterpret_cast<int32_t*>(&r15_386 + 4) = 0;
    rax387 = reinterpret_cast<unsigned char>(rbp378) >> 1;
    esi388 = 64;
    *reinterpret_cast<uint32_t*>(&rax389) = *reinterpret_cast<uint32_t*>(&rax387) & 0x7f;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax389) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax390) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax389);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax390) + 4) = 0;
    rdx391 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax390) + reinterpret_cast<int64_t>(rax390) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax390) * reinterpret_cast<int64_t>(rax390) * reinterpret_cast<unsigned char>(rbp378)));
    rax392 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx391) + reinterpret_cast<uint64_t>(rdx391) - reinterpret_cast<uint64_t>(rdx391) * reinterpret_cast<uint64_t>(rdx391) * reinterpret_cast<unsigned char>(rbp378));
    *reinterpret_cast<int32_t*>(&rdx393) = 0;
    *reinterpret_cast<int32_t*>(&rdx393 + 4) = 0;
    r13_394 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax392) + reinterpret_cast<uint64_t>(rax392) - reinterpret_cast<uint64_t>(rax392) * reinterpret_cast<uint64_t>(rax392) * reinterpret_cast<unsigned char>(rbp378));
    *reinterpret_cast<int32_t*>(&rax395) = 1;
    *reinterpret_cast<int32_t*>(&rax395 + 4) = 0;
    do {
        rdi396 = reinterpret_cast<unsigned char>(rcx385) << 63;
        rcx385 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx385) >> 1);
        rdx393 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx393) >> 1) | rdi396);
        if (reinterpret_cast<unsigned char>(rcx385) < reinterpret_cast<unsigned char>(rax395) || rcx385 == rax395 && reinterpret_cast<unsigned char>(rdx393) <= reinterpret_cast<unsigned char>(r15_386)) {
            cf397 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_386) < reinterpret_cast<unsigned char>(rdx393));
            r15_386 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_386) - reinterpret_cast<unsigned char>(rdx393));
            rax395 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax395) - (reinterpret_cast<unsigned char>(rcx385) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax395) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx385) + static_cast<uint64_t>(cf397))))));
        }
        --esi388;
    } while (esi388);
    *reinterpret_cast<int32_t*>(&rbx398) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx398) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&r8_228) = v384;
    *reinterpret_cast<int32_t*>(&r8_228 + 4) = 0;
    r9_312 = r15_386;
    *reinterpret_cast<unsigned char*>(&rbx398) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp378) - reinterpret_cast<unsigned char>(r15_386)) > reinterpret_cast<unsigned char>(r15_386));
    rbx399 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbx398) & reinterpret_cast<unsigned char>(rbp378)) + (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r15_386) + reinterpret_cast<unsigned char>(r15_386)) - reinterpret_cast<unsigned char>(rbp378)));
    al400 = millerrabin(rbp378, r13_394, rbx399, r14_377, *reinterpret_cast<uint32_t*>(&r8_228), r9_312);
    if (al400) {
        v401 = reinterpret_cast<void*>(rsp379 - 1 + 1 + 6);
        factor();
        v402 = r14_377;
        r14_377 = r13_394;
        eax403 = v404;
        r13_394 = reinterpret_cast<struct s0*>(0x10340);
        v405 = eax403;
        *reinterpret_cast<uint32_t*>(&rax406) = eax403 - 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax406) + 4) = 0;
        v407 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v401) + rax406 * 8);
        r12_234 = reinterpret_cast<void**>(2);
        rax408 = rbx399;
        rbx399 = r15_386;
        r15_386 = rax408;
        goto addr_55b0_269;
    }
    addr_56fc_270:
    addr_5690_271:
    rax409 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v381) - reinterpret_cast<unsigned char>(g28));
    if (rax409) {
        fun_2740();
    } else {
        goto v304;
    }
    addr_5719_274:
    *reinterpret_cast<int32_t*>(&rdx316) = 0x4f2;
    *reinterpret_cast<int32_t*>(&rdx316 + 4) = 0;
    rsi317 = reinterpret_cast<void**>("src/factor.c");
    rdi315 = reinterpret_cast<void**>("(s1) < (n)");
    fun_2810("(s1) < (n)", "src/factor.c", 0x4f2, "prime_p", r8_228, r9_312);
    goto addr_5740_206;
    addr_5478_261:
    do {
        r14_377 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(r14_377) >> 1);
        ++eax382;
    } while (!(*reinterpret_cast<unsigned char*>(&r14_377) & 1));
    v384 = eax382;
    goto addr_5490_263;
    addr_4c97_197:
    *reinterpret_cast<void***>(v120) = rbp117;
    *reinterpret_cast<void***>(v120 + 8) = r13_99;
    goto addr_4ad5_73;
    addr_37ca_56:
    r14_410 = v86 + 1;
    v411 = r14_410;
    rbx412 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_410) << 4);
    rax413 = xrealloc(v87, rbx412, v87, rbx412);
    rsi414 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_410) * 8);
    v415 = rax413;
    rax416 = xrealloc(v88, rsi414, v88, rsi414);
    rdi417 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax413) + reinterpret_cast<unsigned char>(rbx412) + 0xfffffffffffffff0);
    r15_418 = rax416;
    fun_2ab0(rdi417, rsi414, rdx84, rcx83, r8, rdi417, rsi414, rdx84, rcx83, r8);
    rdi419 = rdi417;
    addr_370b_277:
    fun_27a0(rdi419, r12_82, rdx84, rcx83, r8, rdi419, r12_82, rdx84, rcx83, r8);
    *reinterpret_cast<void***>(r15_418 + reinterpret_cast<unsigned char>(v86) * 8) = reinterpret_cast<void**>(1);
    *reinterpret_cast<void***>(r13_81 + 8) = r15_418;
    *reinterpret_cast<void***>(r13_81) = v415;
    *reinterpret_cast<void***>(r13_81 + 16) = v411;
    return v411;
    addr_361b_57:
    rbp420 = r14_85;
    rbx421 = r14_85;
    rdx84 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r14_85) << 4);
    r15_422 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx83) + reinterpret_cast<unsigned char>(rdx84));
    do {
        eax423 = fun_2a00(r15_422, r12_82, rdx84, rcx83, r8, r15_422, r12_82, rdx84, rcx83, r8);
        zf424 = reinterpret_cast<uint1_t>(eax423 == 0);
        if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(eax423 < 0) | zf424)) 
            break;
        rbx421 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbx421) - 1);
        r15_422 = r15_422 - 16;
    } while (rbx421 != 0xffffffffffffffff);
    goto addr_3670_280;
    if (zf424) {
        *reinterpret_cast<void***>(v88 + reinterpret_cast<uint64_t>(rbx421) * 8) = *reinterpret_cast<void***>(v88 + reinterpret_cast<uint64_t>(rbx421) * 8) + 1;
        return v88;
    }
    r15_425 = v86 + 1;
    v411 = r15_425;
    rdx426 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_425) << 4);
    rax427 = xrealloc(v87, rdx426, v87, rdx426);
    rsi428 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_425) * 8);
    v415 = rax427;
    rax429 = xrealloc(v88, rsi428, v88, rsi428);
    rdx84 = rdx426;
    r15_418 = rax429;
    rax430 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v415) + reinterpret_cast<unsigned char>(rdx84) + 0xfffffffffffffff0);
    fun_2ab0(rax430, rsi428, rdx84, rcx83, r8, rax430, rsi428, rdx84, rcx83, r8);
    rax431 = reinterpret_cast<void**>(&rbx421->f1);
    v86 = rax431;
    v432 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax431) << 4);
    rax433 = rax430;
    if (reinterpret_cast<int64_t>(rbx421) < reinterpret_cast<int64_t>(r14_85)) {
        addr_36da_284:
        r14_434 = rax433;
    } else {
        goto addr_3701_286;
    }
    do {
        rdi435 = r14_434;
        r14_434 = r14_434 - 16;
        fun_27a0(rdi435, r14_434, rdx84, rcx83, r8, rdi435, r14_434, rdx84, rcx83, r8);
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r15_418 + reinterpret_cast<uint64_t>(rbp420) * 8) + 8) = *reinterpret_cast<void***>(r15_418 + reinterpret_cast<uint64_t>(rbp420) * 8);
        rbp420 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbp420) - 1);
    } while (reinterpret_cast<int64_t>(rbx421) < reinterpret_cast<int64_t>(rbp420));
    addr_3701_286:
    rdi419 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(v432) + reinterpret_cast<unsigned char>(v415));
    goto addr_370b_277;
    addr_3670_280:
    r15_436 = v86 + 1;
    v411 = r15_436;
    r14_437 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_436) << 4);
    rax438 = xrealloc(v87, r14_437, v87, r14_437);
    rsi439 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_436) * 8);
    v415 = rax438;
    rax440 = xrealloc(v88, rsi439, v88, rsi439);
    r15_418 = rax440;
    rax441 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v415) + reinterpret_cast<unsigned char>(r14_437) + 0xfffffffffffffff0);
    fun_2ab0(rax441, rsi439, rdx84, rcx83, r8, rax441, rsi439, rdx84, rcx83, r8);
    rax433 = rax441;
    v432 = reinterpret_cast<void*>(0);
    v86 = reinterpret_cast<void**>(0);
    goto addr_36da_284;
    addr_40a2_50:
    return rax78;
    addr_5689_288:
    goto addr_5690_271;
    addr_4548_289:
    fun_25e0(v442, rsi107, v442, rsi107);
    fun_25e0(v443, rsi107, v443, rsi107);
    rsp105 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp108) - 8 + 8 - 8 + 8);
    goto addr_455c_61;
    addr_45a0_290:
    rdx444 = v445;
    addr_4524_291:
    if (rdx444) {
        *reinterpret_cast<int32_t*>(&rbx89) = 0;
        *reinterpret_cast<int32_t*>(&rbx89 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdi446) = 0;
        *reinterpret_cast<int32_t*>(&rdi446 + 4) = 0;
        do {
            fun_2940((reinterpret_cast<unsigned char>(rdi446) << 4) + reinterpret_cast<uint64_t>(v447), rsi107, rdx444, rcx103, r8_102, r9_101);
            rsp108 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp108) - 8 + 8);
            *reinterpret_cast<int32_t*>(&rdi446) = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbx89 + 1));
            *reinterpret_cast<int32_t*>(&rdi446 + 4) = 0;
            rbx89 = rdi446;
        } while (reinterpret_cast<unsigned char>(rdi446) < reinterpret_cast<unsigned char>(v448));
        goto addr_4548_289;
    }
    addr_5680_294:
    while (rax449 == rbx399) {
        while (1) {
            *reinterpret_cast<uint32_t*>(&rax450) = r13_394->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax450) + 4) = 0;
            r12_234 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_234) + reinterpret_cast<uint64_t>(rax450));
            rax451 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx399) * reinterpret_cast<unsigned char>(r12_234));
            rdx452 = __intrinsic();
            r15_386 = rax451;
            if (rdx452) {
                if (reinterpret_cast<unsigned char>(rbp378) <= reinterpret_cast<unsigned char>(rdx452)) 
                    goto addr_5719_274;
                rcx453 = rbp378;
                esi454 = 64;
                *reinterpret_cast<int32_t*>(&rax455) = 0;
                *reinterpret_cast<int32_t*>(&rax455 + 4) = 0;
                do {
                    rdi456 = reinterpret_cast<unsigned char>(rcx453) << 63;
                    rcx453 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx453) >> 1);
                    rax455 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax455) >> 1) | rdi456);
                    if (reinterpret_cast<unsigned char>(rcx453) < reinterpret_cast<unsigned char>(rdx452) || rcx453 == rdx452 && reinterpret_cast<unsigned char>(rax455) <= reinterpret_cast<unsigned char>(r15_386)) {
                        cf457 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_386) < reinterpret_cast<unsigned char>(rax455));
                        r15_386 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_386) - reinterpret_cast<unsigned char>(rax455));
                        rdx452 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx452) - (reinterpret_cast<unsigned char>(rcx453) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx452) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx453) + static_cast<uint64_t>(cf457))))));
                    }
                    --esi454;
                } while (esi454);
            } else {
                r15_386 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax451) % reinterpret_cast<unsigned char>(rbp378));
            }
            *reinterpret_cast<uint32_t*>(&r8_228) = v384;
            *reinterpret_cast<int32_t*>(&r8_228 + 4) = 0;
            r9_312 = rbx399;
            al458 = millerrabin(rbp378, r14_377, r15_386, v402, *reinterpret_cast<uint32_t*>(&r8_228), r9_312);
            if (!al458) 
                goto addr_56fc_270;
            r13_394 = reinterpret_cast<struct s0*>(&r13_394->f1);
            if (reinterpret_cast<int1_t>(0x105dc == r13_394)) 
                break;
            addr_55b0_269:
            if (!v405) 
                goto addr_5690_271;
            r11_459 = v401;
            do {
                r8_228 = rbx399;
                rax449 = powm(r15_386, reinterpret_cast<uint64_t>(v383) / v460, rbp378, r14_377, r8_228, r9_312);
                if (v407 == r11_459) 
                    goto addr_5680_294;
                r11_459 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r11_459) + 8);
            } while (rax449 != rbx399);
        }
        fun_2700();
        fun_2a20();
        rax449 = fun_25f0();
    }
    goto addr_5689_288;
    addr_4520_310:
    while (!*reinterpret_cast<int32_t*>(&rax461)) {
        while (1) {
            *reinterpret_cast<uint32_t*>(&rdx462) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_99));
            *reinterpret_cast<int32_t*>(&rdx462 + 4) = 0;
            fun_28c0(rbp95, rbp95, rdx462, rcx103, r8_102, r9_101);
            r9_101 = v100;
            rcx103 = r14_96;
            r8_102 = v97;
            rsi107 = r12_93;
            al463 = mp_millerrabin(rbx89, rsi107, rbp95, rcx103, r8_102, r9_101);
            rsp108 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp108) - 8 + 8 - 8 + 8);
            if (!al463) 
                goto addr_45a0_290;
            ++r13_99;
            if (reinterpret_cast<int1_t>(r13_99 == 0x105dc)) 
                break;
            addr_4458_60:
            if (!v464) 
                goto addr_4548_289;
            *reinterpret_cast<int32_t*>(&r15_94) = 0;
            *reinterpret_cast<int32_t*>(&r15_94 + 4) = 0;
            do {
                rdx465 = r15_94;
                ++r15_94;
                fun_2760(r14_96, r12_93, (reinterpret_cast<unsigned char>(rdx465) << 4) + reinterpret_cast<uint64_t>(v466), rcx103, r8_102, r9_101);
                rcx103 = rbx89;
                fun_2730(r14_96, rbp95, r14_96, rcx103, r8_102, r9_101);
                *reinterpret_cast<int32_t*>(&rsi107) = 1;
                *reinterpret_cast<int32_t*>(&rsi107 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rax461) = fun_2aa0(r14_96, 1, r14_96, rcx103, r8_102, r9_101);
                rsp108 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp108) - 8 + 8 - 8 + 8 - 8 + 8);
                rdx444 = v467;
                if (reinterpret_cast<unsigned char>(r15_94) >= reinterpret_cast<unsigned char>(rdx444)) 
                    goto addr_4520_310;
            } while (*reinterpret_cast<int32_t*>(&rax461));
        }
        rax468 = fun_2700();
        *reinterpret_cast<int32_t*>(&rsi107) = 0;
        *reinterpret_cast<int32_t*>(&rsi107 + 4) = 0;
        rdx444 = rax468;
        fun_2a20();
        rax461 = fun_25f0();
        rsp108 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp108) - 8 + 8 - 8 + 8 - 8 + 8);
    }
    goto addr_4524_291;
}