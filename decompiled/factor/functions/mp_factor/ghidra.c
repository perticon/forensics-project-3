void mp_factor(long param_1,undefined8 *param_2)

{
  char cVar1;
  int iVar2;
  long lVar3;
  ulong uVar4;
  uint uVar5;
  long in_FS_OFFSET;
  undefined local_68 [16];
  undefined local_58 [24];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  iVar2 = *(int *)(param_1 + 4);
  *param_2 = 0;
  param_2[1] = 0;
  param_2[2] = 0;
  if (iVar2 == 0) {
LAB_0010413c:
    if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
      return;
    }
LAB_00104377:
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  if (dev_debug != '\0') {
    fwrite("[trial division] ",1,0x11,stderr);
  }
  __gmpz_init();
  lVar3 = __gmpz_scan1(param_1,0);
  __gmpz_fdiv_q_2exp(param_1,param_1,lVar3);
  for (; lVar3 != 0; lVar3 = lVar3 + -1) {
    __gmpz_init_set_ui(local_58,2);
    mp_factor_insert(param_2,local_58);
    __gmpz_clear(local_58);
  }
  uVar4 = 1;
  lVar3 = 3;
  do {
    iVar2 = __gmpz_divisible_ui_p(param_1,lVar3);
    while (iVar2 == 0) {
      uVar5 = (int)uVar4 + 1;
      lVar3 = lVar3 + (ulong)(byte)primes_diff[uVar4];
      iVar2 = __gmpz_cmp_ui(param_1,lVar3 * lVar3);
      if ((iVar2 < 0) || (uVar5 == 0x29d)) {
        __gmpz_clear(local_68);
        iVar2 = __gmpz_cmp_ui(param_1,1);
        if (iVar2 == 0) goto LAB_0010413c;
        if (dev_debug != '\0') {
          fwrite("[is number prime?] ",1,0x13,stderr);
          iVar2 = __gmpz_cmp_ui(param_1,1);
        }
        if ((iVar2 < 1) ||
           ((iVar2 = __gmpz_cmp_ui(param_1,0x17ded79), -1 < iVar2 &&
            (cVar1 = mp_prime_p_part_0(param_1), cVar1 == '\0')))) {
          if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
            mp_factor_using_pollard_rho(param_1,1,param_2);
            return;
          }
        }
        else if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
          mp_factor_insert(param_2,param_1);
          return;
        }
        goto LAB_00104377;
      }
      uVar4 = (ulong)uVar5;
      iVar2 = __gmpz_divisible_ui_p(param_1,lVar3);
    }
    __gmpz_tdiv_q_ui(param_1,param_1,lVar3);
    __gmpz_init_set_ui(local_58,lVar3);
    mp_factor_insert(param_2,local_58);
    __gmpz_clear(local_58);
  } while( true );
}