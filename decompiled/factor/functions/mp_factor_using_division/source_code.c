mp_factor_using_division (mpz_t t, struct mp_factors *factors)
{
  mpz_t q;
  unsigned long int p;

  devmsg ("[trial division] ");

  mpz_init (q);

  p = mpz_scan1 (t, 0);
  mpz_fdiv_q_2exp (t, t, p);
  while (p)
    {
      mp_factor_insert_ui (factors, 2);
      --p;
    }

  p = 3;
  for (unsigned int i = 1; i <= PRIMES_PTAB_ENTRIES;)
    {
      if (! mpz_divisible_ui_p (t, p))
        {
          p += primes_diff[i++];
          if (mpz_cmp_ui (t, p * p) < 0)
            break;
        }
      else
        {
          mpz_tdiv_q_ui (t, t, p);
          mp_factor_insert_ui (factors, p);
        }
    }

  mpz_clear (q);
}