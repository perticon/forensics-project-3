bool millerrabin2(int64_t * np, int64_t ni, int64_t * bp, int64_t * qp, uint32_t k, int64_t * one) {
    int64_t v1 = (int64_t)one;
    int64_t v2; // bp-104, 0x33a0
    int64_t v3 = &v2; // 0x33bc
    __readfsqword(40);
    int64_t v4; // bp-72, 0x33a0
    int64_t v5 = powm2(&v4, bp, qp, np, ni, one); // 0x33e0
    v2 = v1;
    int64_t v6 = *(int64_t *)(v1 + 8); // 0x33f7
    if (v5 != v1 || v6 != v4) {
        int64_t v7 = &v4; // 0x33cd
        int64_t v8 = *(int64_t *)((int64_t)np + 8); // 0x3406
        int64_t v9 = v7 - v1; // 0x3413
        if (v5 == v9 == v4 == (int64_t)(&v4 < one) - v6 + v8) {
            // 0x3492
            *(char *)(v3 + 31) = 1;
            goto lab_0x3497_2;
        } else {
            if (k < 2) {
                goto lab_0x3497_2;
            } else {
                int64_t v10 = v5; // 0x3477
                int64_t v11 = v4; // 0x33a0
                int64_t v12 = 1; // 0x33a0
                while (true) {
                    int64_t v13 = v11;
                    int64_t v14 = v10;
                    *(int64_t *)(v3 - 8) = ni;
                    *(int64_t *)(v3 - 16) = v7;
                    v10 = mulredc2(&v4, v13, v14, v13, v14, v8, (int64_t)&g34, (int64_t)&g34);
                    int64_t v15 = *(int64_t *)(v3 + 32); // 0x347c
                    if (v9 == v10) {
                        // 0x348b
                        if (*(int64_t *)(v3 + 16) == v15) {
                            // break -> 0x3492
                            break;
                        }
                    }
                    // 0x3450
                    if (v2 == v10) {
                        // 0x3456
                        if (v15 == *(int64_t *)(v3 + 8)) {
                            goto lab_0x3497_2;
                        }
                    }
                    int64_t v16 = v12 + 1; // 0x345d
                    v11 = v15;
                    v12 = v16 & 0xffffffff;
                    if (*(int32_t *)(v3 + 24) == (int32_t)v16) {
                        goto lab_0x3497_2;
                    }
                }
                // 0x3492
                *(char *)(v3 + 31) = 1;
                goto lab_0x3497_2;
            }
        }
    } else {
        // 0x3492
        *(char *)(v3 + 31) = 1;
        goto lab_0x3497_2;
    }
  lab_0x3497_2:
    // 0x3497
    if (*(int64_t *)(v3 + 40) != __readfsqword(40)) {
        // 0x34cb
        return function_2740() % 2 != 0;
    }
    // 0x34a7
    return *(char *)(v3 + 31) % 2 != 0;
}