bool millerrabin2(ulong *param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,
                 uint param_5,ulong *param_6)

{
  ulong uVar1;
  ulong uVar2;
  ulong uVar3;
  ulong uVar4;
  ulong uVar5;
  ulong uVar6;
  ulong uVar7;
  uint uVar8;
  long in_FS_OFFSET;
  bool local_49;
  ulong local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  uVar5 = powm2(&local_48,param_3,param_4,param_1,param_2);
  uVar1 = *param_6;
  uVar2 = param_6[1];
  if ((uVar1 != uVar5) || (uVar2 != local_48)) {
    uVar3 = param_1[1];
    uVar4 = *param_1;
    uVar7 = uVar4 - uVar1;
    uVar6 = (uVar3 - uVar2) - (ulong)(uVar4 < uVar1);
    local_49 = uVar5 == uVar7 && local_48 == uVar6;
    if (uVar5 != uVar7 || local_48 != uVar6) {
      if (1 < param_5) {
        uVar8 = 1;
        do {
          uVar5 = mulredc2(&local_48,local_48,uVar5,local_48,uVar5,uVar3,uVar4,param_2);
          if ((uVar7 == uVar5) && (uVar6 == local_48)) goto LAB_00103492;
        } while (((uVar1 != uVar5) || (local_48 != uVar2)) && (uVar8 = uVar8 + 1, param_5 != uVar8))
        ;
      }
      goto LAB_00103497;
    }
  }
LAB_00103492:
  local_49 = true;
LAB_00103497:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return local_49;
}