unsigned char millerrabin2(void** rdi, void** rsi, struct s43* rdx, void** rcx, uint32_t r8d, void** r9) {
    void** rbp7;
    void* rsp8;
    uint32_t v9;
    void** r14_10;
    void** r8_11;
    void** rdi12;
    void** rax13;
    void** v14;
    void** rax15;
    void*** rsp16;
    void** rsi17;
    void** v18;
    void** rdx19;
    void** rax20;
    void** v21;
    void** rax22;
    void** v23;
    void** r12_24;
    void** r15_25;
    void** rbx26;
    void** rax27;
    void** v28;
    unsigned char cl29;
    unsigned char v30;
    void* rax31;
    uint32_t eax32;
    void** r13_33;
    void** rax34;
    void** v35;
    void** rax36;
    void** v37;
    void* rbx38;
    void** rax39;
    void** rcx40;
    void** rsi41;
    void* rdx42;
    void** rbx43;
    void** rax44;
    void* rax45;
    void** r13_46;
    void** r12_47;
    void** rcx48;
    void** rdx49;
    struct s0* r14_50;
    void** v51;
    void** v52;
    void** v53;
    void** r14_54;
    void** v55;
    void** rbx56;
    void** rax57;
    void** rsi58;
    void** v59;
    void** rax60;
    void** rdi61;
    void** r15_62;
    void** rdi63;
    int64_t v64;
    struct s0* rbp65;
    struct s0* rbx66;
    void** r15_67;
    int32_t eax68;
    uint1_t zf69;
    int64_t v70;
    void** r15_71;
    void** rdx72;
    void** rax73;
    void** rsi74;
    void** rax75;
    void** rax76;
    void** rax77;
    void* v78;
    void** rax79;
    void** r14_80;
    void** rdi81;
    void** r15_82;
    void** r14_83;
    void** rax84;
    void** rsi85;
    void** rax86;
    void** rax87;
    void* rcx88;
    void** rax89;
    uint64_t rdx90;
    void* rdx91;
    void* rdi92;

    rbp7 = rsi;
    rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56);
    v9 = r8d;
    r14_10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp8) + 32);
    r8_11 = rbp7;
    rdi12 = r14_10;
    rax13 = g28;
    v14 = rax13;
    rax15 = powm2(rdi12, rdx, rcx, rdi, r8_11, r9);
    rsp16 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
    rsi17 = v18;
    rdx19 = rax15;
    rax20 = *reinterpret_cast<void***>(r9);
    v21 = rax20;
    rax22 = *reinterpret_cast<void***>(r9 + 8);
    v23 = rax22;
    if (rax20 == rdx19 && rax22 == rsi17) {
        goto addr_3492_3;
    }
    r12_24 = *reinterpret_cast<void***>(rdi + 8);
    r15_25 = *reinterpret_cast<void***>(rdi);
    rbx26 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_25) - reinterpret_cast<unsigned char>(v21));
    rax27 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_24) - (reinterpret_cast<unsigned char>(v23) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_24) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(v23) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_25) < reinterpret_cast<unsigned char>(v21))))))));
    v28 = rax27;
    cl29 = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdx19 == rbx26)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi17 == rax27)));
    v30 = cl29;
    if (cl29) {
        addr_3492_3:
        v30 = 1;
        goto addr_3497_5;
    } else {
        if (v9 <= 1) {
            addr_3497_5:
            rax31 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v14) - reinterpret_cast<unsigned char>(g28));
            if (!rax31) {
                eax32 = v30;
                return *reinterpret_cast<unsigned char*>(&eax32);
            }
        } else {
            r13_33 = r12_24;
            *reinterpret_cast<uint32_t*>(&r12_24) = 1;
            do {
                r8_11 = rdx19;
                rdi12 = r14_10;
                rax34 = mulredc2(rdi12, rsi17, rdx19, rsi17, r8_11, r13_33, r15_25, rbp7);
                rsi17 = v35;
                rdx19 = rax34;
                rsp16 = rsp16 - 8 - 8 - 8 + 8 + 8 + 8;
                if (rbx26 != rdx19) 
                    continue;
                if (v28 == rsi17) 
                    goto addr_3492_3;
            } while ((v21 != rdx19 || rsi17 != v23) && (*reinterpret_cast<uint32_t*>(&r12_24) = *reinterpret_cast<uint32_t*>(&r12_24) + 1, v9 != *reinterpret_cast<uint32_t*>(&r12_24)));
            goto addr_3497_5;
        }
    }
    fun_2740();
    rax36 = g28;
    v37 = rax36;
    rbx38 = reinterpret_cast<void*>(rsp16 - 8 + 8 - 8 - 8 - 40);
    rax39 = umaxtostr(rdi12, rbx38, rdi12, rbx38);
    rcx40 = g150f8;
    rsi41 = rax39;
    rdx42 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax39) - reinterpret_cast<uint64_t>(rbx38));
    rbx43 = reinterpret_cast<void**>(20 - reinterpret_cast<uint64_t>(rdx42));
    if (reinterpret_cast<unsigned char>(rbx43) < reinterpret_cast<unsigned char>(rsi17)) 
        goto addr_3511_14;
    addr_3569_15:
    rax44 = fun_28d0(rcx40, rsi41, rbx43, rcx40, r8_11, rcx40, rsi41, rbx43, rcx40, r8_11);
    g150f8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax44) + reinterpret_cast<unsigned char>(rbx43));
    rax45 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v37) - reinterpret_cast<unsigned char>(g28));
    if (!rax45) {
        goto v21;
    }
    fun_2740();
    r13_46 = rcx40;
    r12_47 = rsi41;
    rcx48 = *reinterpret_cast<void***>(rcx40);
    rdx49 = *reinterpret_cast<void***>(rcx40 + 8);
    r14_50 = reinterpret_cast<struct s0*>(*reinterpret_cast<void***>(rcx40 + 16) + 0xffffffffffffffff);
    v51 = *reinterpret_cast<void***>(rcx40 + 16);
    v52 = rcx48;
    v53 = rdx49;
    if (reinterpret_cast<int64_t>(r14_50) >= reinterpret_cast<int64_t>(0)) 
        goto addr_361b_19;
    r14_54 = v51 + 1;
    v55 = r14_54;
    rbx56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_54) << 4);
    rax57 = xrealloc(v52, rbx56, v52, rbx56);
    rsi58 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_54) * 8);
    v59 = rax57;
    rax60 = xrealloc(v53, rsi58, v53, rsi58);
    rdi61 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax57) + reinterpret_cast<unsigned char>(rbx56) + 0xfffffffffffffff0);
    r15_62 = rax60;
    fun_2ab0(rdi61, rsi58, rdx49, rcx48, r8_11, rdi61, rsi58, rdx49, rcx48, r8_11);
    rdi63 = rdi61;
    addr_370b_21:
    fun_27a0(rdi63, r12_47, rdx49, rcx48, r8_11, rdi63, r12_47, rdx49, rcx48, r8_11);
    *reinterpret_cast<void***>(r15_62 + reinterpret_cast<unsigned char>(v51) * 8) = reinterpret_cast<void**>(1);
    *reinterpret_cast<void***>(r13_46 + 8) = r15_62;
    *reinterpret_cast<void***>(r13_46) = v59;
    *reinterpret_cast<void***>(r13_46 + 16) = v55;
    goto v64;
    addr_361b_19:
    rbp65 = r14_50;
    rbx66 = r14_50;
    rdx49 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r14_50) << 4);
    r15_67 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx48) + reinterpret_cast<unsigned char>(rdx49));
    do {
        eax68 = fun_2a00(r15_67, r12_47, rdx49, rcx48, r8_11, r15_67, r12_47, rdx49, rcx48, r8_11);
        zf69 = reinterpret_cast<uint1_t>(eax68 == 0);
        if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(eax68 < 0) | zf69)) 
            break;
        rbx66 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbx66) - 1);
        r15_67 = r15_67 - 16;
    } while (rbx66 != 0xffffffffffffffff);
    goto addr_3670_24;
    if (zf69) {
        *reinterpret_cast<void***>(v53 + reinterpret_cast<uint64_t>(rbx66) * 8) = *reinterpret_cast<void***>(v53 + reinterpret_cast<uint64_t>(rbx66) * 8) + 1;
        goto v70;
    }
    r15_71 = v51 + 1;
    v55 = r15_71;
    rdx72 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_71) << 4);
    rax73 = xrealloc(v52, rdx72, v52, rdx72);
    rsi74 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_71) * 8);
    v59 = rax73;
    rax75 = xrealloc(v53, rsi74, v53, rsi74);
    rdx49 = rdx72;
    r15_62 = rax75;
    rax76 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v59) + reinterpret_cast<unsigned char>(rdx49) + 0xfffffffffffffff0);
    fun_2ab0(rax76, rsi74, rdx49, rcx48, r8_11, rax76, rsi74, rdx49, rcx48, r8_11);
    rax77 = reinterpret_cast<void**>(&rbx66->f1);
    v51 = rax77;
    v78 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax77) << 4);
    rax79 = rax76;
    if (reinterpret_cast<int64_t>(rbx66) < reinterpret_cast<int64_t>(r14_50)) {
        addr_36da_28:
        r14_80 = rax79;
    } else {
        goto addr_3701_30;
    }
    do {
        rdi81 = r14_80;
        r14_80 = r14_80 - 16;
        fun_27a0(rdi81, r14_80, rdx49, rcx48, r8_11, rdi81, r14_80, rdx49, rcx48, r8_11);
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r15_62 + reinterpret_cast<uint64_t>(rbp65) * 8) + 8) = *reinterpret_cast<void***>(r15_62 + reinterpret_cast<uint64_t>(rbp65) * 8);
        rbp65 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbp65) - 1);
    } while (reinterpret_cast<int64_t>(rbx66) < reinterpret_cast<int64_t>(rbp65));
    addr_3701_30:
    rdi63 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(v78) + reinterpret_cast<unsigned char>(v59));
    goto addr_370b_21;
    addr_3670_24:
    r15_82 = v51 + 1;
    v55 = r15_82;
    r14_83 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_82) << 4);
    rax84 = xrealloc(v52, r14_83, v52, r14_83);
    rsi85 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_82) * 8);
    v59 = rax84;
    rax86 = xrealloc(v53, rsi85, v53, rsi85);
    r15_62 = rax86;
    rax87 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v59) + reinterpret_cast<unsigned char>(r14_83) + 0xfffffffffffffff0);
    fun_2ab0(rax87, rsi85, rdx49, rcx48, r8_11, rax87, rsi85, rdx49, rcx48, r8_11);
    rax79 = rax87;
    v78 = reinterpret_cast<void*>(0);
    v51 = reinterpret_cast<void**>(0);
    goto addr_36da_28;
    addr_3511_14:
    rcx88 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx42) + reinterpret_cast<unsigned char>(rsi17) + 0xffffffffffffffec);
    rax89 = g150f8;
    if (reinterpret_cast<uint64_t>(rcx88) < 8) {
        if (*reinterpret_cast<unsigned char*>(&rcx88) & 4) {
            *reinterpret_cast<void***>(rax89) = reinterpret_cast<void**>(0x30303030);
            *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(rax89) + reinterpret_cast<uint64_t>(rcx88) + 0xfffffffffffffffc) = 0x30303030;
        } else {
            if (rcx88 && (*reinterpret_cast<void***>(rax89) = reinterpret_cast<void**>(48), !!(*reinterpret_cast<unsigned char*>(&rcx88) & 2))) {
                *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(rax89) + reinterpret_cast<uint64_t>(rcx88) + 0xfffffffffffffffe) = reinterpret_cast<int16_t>(millerrabin);
            }
        }
    } else {
        *reinterpret_cast<void***>(rax89) = reinterpret_cast<void**>(0x3030303030303030);
        r8_11 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax89 + 8) & 0xfffffffffffffff8);
        *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rax89) + reinterpret_cast<uint64_t>(rcx88) + 0xfffffffffffffff8) = reinterpret_cast<void**>(0x3030303030303030);
        rdx90 = reinterpret_cast<unsigned char>(rax89) - reinterpret_cast<unsigned char>(r8_11) + reinterpret_cast<uint64_t>(rcx88) & 0xfffffffffffffff8;
        if (rdx90 >= 8) {
            *reinterpret_cast<int32_t*>(&rdx91) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx91) + 4) = 0;
            rdi92 = reinterpret_cast<void*>(rdx90 & 0xfffffffffffffff8);
            do {
                *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r8_11) + reinterpret_cast<uint64_t>(rdx91)) = reinterpret_cast<void**>(0x3030303030303030);
                rdx91 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx91) + 8);
            } while (reinterpret_cast<uint64_t>(rdx91) < reinterpret_cast<uint64_t>(rdi92));
        }
    }
    rcx40 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rcx88) + reinterpret_cast<unsigned char>(rax89));
    goto addr_3569_15;
}