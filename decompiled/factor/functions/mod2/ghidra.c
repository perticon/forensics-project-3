ulong mod2(ulong *param_1,ulong param_2,ulong param_3,ulong param_4,ulong param_5)

{
  long lVar1;
  long lVar2;
  byte bVar3;
  ulong uVar4;
  ulong uVar5;
  int iVar6;
  ulong uVar7;
  int iVar8;
  bool bVar9;
  
  if (param_4 != 0) {
    if (param_2 != 0) {
      lVar1 = 0x3f;
      if (param_4 != 0) {
        for (; param_4 >> lVar1 == 0; lVar1 = lVar1 + -1) {
        }
      }
      lVar2 = 0x3f;
      if (param_2 != 0) {
        for (; param_2 >> lVar2 == 0; lVar2 = lVar2 + -1) {
        }
      }
      iVar8 = ((uint)lVar1 ^ 0x3f) - ((uint)lVar2 ^ 0x3f);
      bVar3 = (byte)iVar8;
      uVar7 = param_5 << (bVar3 & 0x3f);
      uVar4 = param_5 >> (0x40 - bVar3 & 0x3f) | param_4 << (bVar3 & 0x3f);
      if (0 < iVar8) {
        iVar6 = 0;
        do {
          if ((uVar4 < param_2) || ((uVar4 == param_2 && (uVar7 <= param_3)))) {
            bVar9 = param_3 < uVar7;
            param_3 = param_3 - uVar7;
            param_2 = (param_2 - uVar4) - (ulong)bVar9;
          }
          iVar6 = iVar6 + 1;
          uVar5 = uVar4 << 0x3f;
          uVar4 = uVar4 >> 1;
          uVar7 = uVar7 >> 1 | uVar5;
        } while (iVar8 != iVar6);
      }
    }
    *param_1 = param_2;
    return param_3;
  }
                    /* WARNING: Subroutine does not return */
  __assert_fail("d1 != 0","src/factor.c",0x19f,(char *)&__PRETTY_FUNCTION___2);
}