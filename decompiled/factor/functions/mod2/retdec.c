int64_t mod2(int64_t a1, int64_t a2, int64_t result, int64_t a4, uint64_t a5) {
    if (a4 == 0) {
        // 0x3b09
        return function_2810();
    }
    // 0x3a95
    if (a2 == 0) {
        // 0x3b02
        *(int64_t *)a1 = 0;
        return result;
    }
    uint64_t v1 = llvm_ctlz_i64(a4, true) - llvm_ctlz_i64(a2, true); // 0x3ab4
    int32_t v2 = v1; // 0x3aba
    if (v2 < 1) {
        // 0x3b02
        *(int64_t *)a1 = a2;
        return result;
    }
    uint64_t v3 = v1 % 64; // 0x3ac3
    int64_t v4 = a5 >> -v1 % 64 | a4 << v3; // 0x3ad3
    int64_t v5 = result; // 0x3ad3
    int64_t v6 = a2; // 0x3ad3
    int64_t v7 = 0; // 0x3ad3
    int64_t v8 = a5 << v3; // 0x3ad3
    uint64_t v9; // 0x3a90
    uint64_t v10; // 0x3a90
    int64_t v11; // 0x3a90
    int64_t v12; // 0x3a90
    while (true) {
      lab_0x3ad8:
        // 0x3ad8
        v9 = v8;
        int64_t v13 = v6;
        uint64_t v14 = v5;
        v10 = v4;
        if (v13 > v10) {
            // 0x3ae4
            v11 = v14 - v9;
            v12 = v13 - v10 + (int64_t)(v14 < v9);
            goto lab_0x3aea;
        } else {
            // 0x3add
            v11 = v14;
            v12 = v13;
            if (v13 != v10 || v14 < v9) {
                goto lab_0x3aea;
            } else {
                // 0x3ae4
                v11 = v14 - v9;
                v12 = v13 - v10 + (int64_t)(v14 < v9);
                goto lab_0x3aea;
            }
        }
    }
  lab_0x3b02:;
    // 0x3b02
    int64_t v15; // 0x3a90
    *(int64_t *)a1 = v15;
    int64_t result2; // 0x3a90
    return result2;
  lab_0x3aea:
    // 0x3aea
    v6 = v12;
    v5 = v11;
    int64_t v16 = v7 + 1; // 0x3af0
    v4 = v10 / 2;
    v7 = v16 & 0xffffffff;
    v8 = v9 / 2 | 0x8000000000000000 * v10;
    result2 = v5;
    v15 = v6;
    if (v2 == (int32_t)v16) {
        // break -> 0x3b02
        goto lab_0x3b02;
    }
    goto lab_0x3ad8;
}