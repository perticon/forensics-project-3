void** mod2(void*** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void*** r9_7;
    uint32_t r10d8;
    uint32_t r10d9;
    uint32_t eax10;
    uint32_t ecx11;
    uint32_t ecx12;
    void** r8_13;
    void** rax14;
    uint32_t edi15;
    uint1_t cf16;
    uint64_t rcx17;
    int64_t v18;
    int64_t rax19;
    void** r14_20;
    int32_t eax21;
    int32_t eax22;
    void** rbp23;
    int32_t eax24;
    int32_t eax25;

    if (rcx) {
        r9_7 = rdi;
        if (rsi && (r10d8 = (r10d9 ^ 63) - (eax10 ^ 63), ecx11 = 64 - r10d8, ecx12 = r10d8, r8_13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) << *reinterpret_cast<unsigned char*>(&ecx12)), rax14 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8) >> *reinterpret_cast<signed char*>(&ecx11)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rcx) << *reinterpret_cast<unsigned char*>(&ecx12))), !(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(r10d8) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(r10d8 == 0)))) {
            edi15 = 0;
            do {
                if (reinterpret_cast<unsigned char>(rax14) < reinterpret_cast<unsigned char>(rsi) || rax14 == rsi && reinterpret_cast<unsigned char>(rdx) >= reinterpret_cast<unsigned char>(r8_13)) {
                    cf16 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx) < reinterpret_cast<unsigned char>(r8_13));
                    rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx) - reinterpret_cast<unsigned char>(r8_13));
                    rsi = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi) - (reinterpret_cast<unsigned char>(rax14) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rsi) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rax14) + static_cast<uint64_t>(cf16))))));
                }
                ++edi15;
                rcx17 = reinterpret_cast<unsigned char>(rax14) << 63;
                rax14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax14) >> 1);
                r8_13 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_13) >> 1) | rcx17);
            } while (r10d8 != edi15);
        }
        *r9_7 = rsi;
        return rdx;
    }
    v18 = rax19;
    fun_2810("d1 != 0", "src/factor.c", 0x19f, "mod2", r8, r9);
    r14_20 = r9;
    fun_2730("mod2", 0x19f, r8, "d1 != 0", r8, r9);
    eax21 = fun_2aa0("mod2", 1, r8, "d1 != 0", r8, r9);
    if (!eax21) 
        goto addr_3bc4_10;
    eax22 = fun_2a00("mod2", "src/factor.c", r8, "d1 != 0", r8);
    if (eax22) 
        goto addr_3b75_12;
    addr_3bc4_10:
    goto v18;
    addr_3b75_12:
    *reinterpret_cast<int32_t*>(&rbp23) = 1;
    *reinterpret_cast<int32_t*>(&rbp23 + 4) = 0;
    if (reinterpret_cast<unsigned char>(r14_20) > reinterpret_cast<unsigned char>(1)) {
        do {
            fun_2ad0("mod2", "mod2", 2, "d1 != 0");
            eax24 = fun_2a00("mod2", "src/factor.c", 2, "d1 != 0", r8);
            if (!eax24) 
                goto addr_3bc4_10;
            eax25 = fun_2aa0("mod2", 1, 2, "d1 != 0", r8, r9);
        } while (eax25 && (++rbp23, r14_20 != rbp23));
    }
    goto v18;
}