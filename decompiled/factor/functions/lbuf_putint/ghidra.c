void lbuf_putint(undefined8 param_1,ulong param_2)

{
  undefined8 *puVar1;
  void *pvVar2;
  undefined8 *puVar3;
  ulong uVar4;
  ulong uVar5;
  ulong __n;
  long in_FS_OFFSET;
  undefined auStack56 [24];
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  pvVar2 = (void *)umaxtostr(param_1,auStack56);
  puVar3 = lbuf._8_8_;
  __n = 0x14 - ((long)pvVar2 - (long)auStack56);
  if (__n < param_2) {
    param_2 = ((long)pvVar2 - (long)auStack56) + -0x14 + param_2;
    if (param_2 < 8) {
      if ((param_2 & 4) == 0) {
        if ((param_2 != 0) && (*(undefined *)lbuf._8_8_ = 0x30, (param_2 & 2) != 0)) {
          *(undefined2 *)((long)puVar3 + (param_2 - 2)) = 0x3030;
        }
      }
      else {
        *(undefined4 *)lbuf._8_8_ = 0x30303030;
        *(undefined4 *)((long)puVar3 + (param_2 - 4)) = 0x30303030;
      }
    }
    else {
      puVar1 = lbuf._8_8_ + 1;
      *lbuf._8_8_ = 0x3030303030303030;
      *(undefined8 *)((long)puVar3 + (param_2 - 8)) = 0x3030303030303030;
      uVar4 = (ulong)((long)puVar3 + (param_2 - ((ulong)puVar1 & 0xfffffffffffffff8))) &
              0xfffffffffffffff8;
      if (7 < uVar4) {
        uVar5 = 0;
        do {
          *(undefined8 *)(((ulong)puVar1 & 0xfffffffffffffff8) + uVar5) = 0x3030303030303030;
          uVar5 = uVar5 + 8;
        } while (uVar5 < uVar4);
      }
    }
    puVar3 = (undefined8 *)(param_2 + (long)puVar3);
  }
  pvVar2 = memcpy(puVar3,pvVar2,__n);
  lbuf._8_8_ = (undefined8 *)((long)pvVar2 + __n);
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}