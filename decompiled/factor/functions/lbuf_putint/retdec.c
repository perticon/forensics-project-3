void lbuf_putint(int64_t i, uint64_t min_width) {
    int64_t v1 = __readfsqword(40); // 0x34d9
    int64_t v2; // bp-56, 0x34d0
    char * v3 = umaxtostr(i, (char *)&v2); // 0x34ef
    int64_t v4 = (int64_t)v3 - (int64_t)&v2; // 0x3501
    uint64_t v5 = 20 - v4; // 0x3509
    if (v5 < min_width) {
        uint64_t v6 = min_width - 20 + v4; // 0x3511
        int64_t v7 = g26; // 0x3516
        if (v6 < 8) {
            if ((v6 & 4) != 0) {
                // 0x35be
                *(int32_t *)v7 = 0x30303030;
                *(int32_t *)(v7 + v6 - 4) = 0x30303030;
            } else {
                if (v6 != 0) {
                    // 0x35aa
                    *(char *)v7 = 48;
                    if ((v6 & 2) != 0) {
                        // 0x35b2
                        *(int16_t *)(v7 + v6 - 2) = 0x3030;
                    }
                }
            }
        } else {
            // 0x352d
            *(int64_t *)v7 = 0x3030303030303030;
            int64_t v8 = v7 + 8 & -8; // 0x3537
            int64_t v9 = v7 + v6;
            *(int64_t *)(v9 - 8) = 0x3030303030303030;
            uint64_t v10 = v9 - v8 & -8; // 0x3546
            int64_t v11 = 0; // 0x354e
            if (v10 != 0) {
                *(int64_t *)(v11 + v8) = 0x3030303030303030;
                int64_t v12 = v11 + 8; // 0x355d
                v11 = v12;
                while (v12 < v10) {
                    // 0x3559
                    *(int64_t *)(v11 + v8) = 0x3030303030303030;
                    v12 = v11 + 8;
                    v11 = v12;
                }
            }
        }
    }
    // 0x3569
    g26 = function_28d0() + v5;
    if (v1 == __readfsqword(40)) {
        // 0x358f
        return;
    }
    // 0x35ce
    function_2740();
}