void** gcd_odd(void** rdi, void** rsi) {
    void** rax3;
    uint64_t rax4;
    void** rdx5;
    void* rdx6;
    uint64_t rcx7;

    rax3 = rsi;
    if (!(*reinterpret_cast<unsigned char*>(&rsi) & 1)) {
        rax3 = rdi;
        rdi = rsi;
    }
    if (!rdi) {
        return rax3;
    } else {
        rax4 = reinterpret_cast<unsigned char>(rax3) >> 1;
        while (1) {
            rdx5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) >> 1);
            if (*reinterpret_cast<unsigned char*>(&rdi) & 1) {
                rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx5) - rax4);
                if (!rdx6) 
                    break;
                rcx7 = reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rdx6) >> 63);
                rax4 = rax4 + (reinterpret_cast<uint64_t>(rdx6) & rcx7);
                rdx5 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rdx6) ^ rcx7) - rcx7);
            }
            rdi = rdx5;
        }
        return reinterpret_cast<unsigned char>(rdi) | 1;
    }
}