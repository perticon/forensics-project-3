int64_t gcd_odd(int64_t a, uint64_t b) {
    uint64_t result = b % 2 != 0 ? b : a;
    int64_t v1 = b % 2 != 0 ? a : b;
    if (v1 == 0) {
        // 0x2f18
        return result;
    }
    int64_t v2 = result / 2; // 0x2ed7
    int64_t v3 = v1; // 0x2ed7
    uint64_t v4; // 0x2ec0
    while (true) {
        // 0x2ef9
        v4 = v3;
        int64_t v5 = v2;
        int64_t v6 = v4 / 2; // 0x2efc
        v2 = v5;
        v3 = v6;
        if (v4 % 2 != 0) {
            int64_t v7 = v6 - v5; // 0x2f05
            if (v7 == 0) {
                // break -> 0x2f0a
                break;
            }
            int64_t v8 = v7 >> 63; // 0x2ee6
            v2 = (v8 & v7) + v5;
            v3 = (v8 ^ v7) - v8;
        }
    }
    // 0x2f0a
    return v4 | 1;
}