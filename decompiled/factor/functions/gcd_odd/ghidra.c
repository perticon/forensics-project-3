ulong gcd_odd(ulong param_1,ulong param_2)

{
  ulong uVar1;
  ulong uVar2;
  ulong uVar3;
  
  uVar1 = param_2;
  if ((param_2 & 1) == 0) {
    uVar1 = param_1;
    param_1 = param_2;
  }
  if (param_1 == 0) {
    return uVar1;
  }
  uVar1 = uVar1 >> 1;
  while( true ) {
    do {
      uVar2 = param_1;
      param_1 = uVar2 >> 1;
    } while ((uVar2 & 1) == 0);
    uVar3 = (uVar2 >> 1) - uVar1;
    if (uVar3 == 0) break;
    uVar2 = (long)uVar3 >> 0x3f;
    uVar1 = uVar1 + (uVar3 & uVar2);
    param_1 = (uVar3 ^ uVar2) - uVar2;
  }
  return uVar2 | 1;
}