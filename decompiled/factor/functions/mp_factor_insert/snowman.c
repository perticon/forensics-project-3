void mp_factor_insert(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r13_7;
    void** r12_8;
    void** rcx9;
    void** rdx10;
    struct s0* r14_11;
    void** v12;
    void** v13;
    void** v14;
    void** r14_15;
    void** v16;
    void** rbx17;
    void** rax18;
    void** rsi19;
    void** v20;
    void** rax21;
    void** rdi22;
    void** r15_23;
    void** rdi24;
    struct s0* rbp25;
    struct s0* rbx26;
    void** r15_27;
    int32_t eax28;
    uint1_t zf29;
    void** r15_30;
    void** rdx31;
    void** rax32;
    void** rsi33;
    void** rax34;
    void** rax35;
    void** rax36;
    void* v37;
    void** rax38;
    void** r14_39;
    void** rdi40;
    void** r15_41;
    void** r14_42;
    void** rax43;
    void** rsi44;
    void** rax45;
    void** rax46;

    r13_7 = rdi;
    r12_8 = rsi;
    rcx9 = *reinterpret_cast<void***>(rdi);
    rdx10 = *reinterpret_cast<void***>(rdi + 8);
    r14_11 = reinterpret_cast<struct s0*>(*reinterpret_cast<void***>(rdi + 16) + 0xffffffffffffffff);
    v12 = *reinterpret_cast<void***>(rdi + 16);
    v13 = rcx9;
    v14 = rdx10;
    if (reinterpret_cast<int64_t>(r14_11) < reinterpret_cast<int64_t>(0)) {
        r14_15 = v12 + 1;
        v16 = r14_15;
        rbx17 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_15) << 4);
        rax18 = xrealloc(v13, rbx17);
        rsi19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_15) * 8);
        v20 = rax18;
        rax21 = xrealloc(v14, rsi19);
        rdi22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax18) + reinterpret_cast<unsigned char>(rbx17) + 0xfffffffffffffff0);
        r15_23 = rax21;
        fun_2ab0(rdi22, rsi19, rdx10, rcx9, r8);
        rdi24 = rdi22;
    } else {
        rbp25 = r14_11;
        rbx26 = r14_11;
        rdx10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r14_11) << 4);
        r15_27 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx9) + reinterpret_cast<unsigned char>(rdx10));
        do {
            eax28 = fun_2a00(r15_27, r12_8, rdx10, rcx9, r8);
            zf29 = reinterpret_cast<uint1_t>(eax28 == 0);
            if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(eax28 < 0) | zf29)) 
                goto addr_364d_5;
            rbx26 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbx26) - 1);
            r15_27 = r15_27 - 16;
        } while (rbx26 != 0xffffffffffffffff);
        goto addr_3670_7;
    }
    addr_370b_8:
    fun_27a0(rdi24, r12_8, rdx10, rcx9, r8);
    *reinterpret_cast<void***>(r15_23 + reinterpret_cast<unsigned char>(v12) * 8) = reinterpret_cast<void**>(1);
    *reinterpret_cast<void***>(r13_7 + 8) = r15_23;
    *reinterpret_cast<void***>(r13_7) = v20;
    *reinterpret_cast<void***>(r13_7 + 16) = v16;
    return;
    addr_364d_5:
    if (zf29) {
        *reinterpret_cast<void***>(v14 + reinterpret_cast<uint64_t>(rbx26) * 8) = *reinterpret_cast<void***>(v14 + reinterpret_cast<uint64_t>(rbx26) * 8) + 1;
        return;
    }
    r15_30 = v12 + 1;
    v16 = r15_30;
    rdx31 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_30) << 4);
    rax32 = xrealloc(v13, rdx31);
    rsi33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_30) * 8);
    v20 = rax32;
    rax34 = xrealloc(v14, rsi33);
    rdx10 = rdx31;
    r15_23 = rax34;
    rax35 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v20) + reinterpret_cast<unsigned char>(rdx10) + 0xfffffffffffffff0);
    fun_2ab0(rax35, rsi33, rdx10, rcx9, r8);
    rax36 = reinterpret_cast<void**>(&rbx26->f1);
    v12 = rax36;
    v37 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax36) << 4);
    rax38 = rax35;
    if (reinterpret_cast<int64_t>(rbx26) < reinterpret_cast<int64_t>(r14_11)) {
        addr_36da_11:
        r14_39 = rax38;
    } else {
        goto addr_3701_13;
    }
    do {
        rdi40 = r14_39;
        r14_39 = r14_39 - 16;
        fun_27a0(rdi40, r14_39, rdx10, rcx9, r8);
        *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r15_23 + reinterpret_cast<uint64_t>(rbp25) * 8) + 8) = *reinterpret_cast<void***>(r15_23 + reinterpret_cast<uint64_t>(rbp25) * 8);
        rbp25 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbp25) - 1);
    } while (reinterpret_cast<int64_t>(rbx26) < reinterpret_cast<int64_t>(rbp25));
    addr_3701_13:
    rdi24 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(v37) + reinterpret_cast<unsigned char>(v20));
    goto addr_370b_8;
    addr_3670_7:
    r15_41 = v12 + 1;
    v16 = r15_41;
    r14_42 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_41) << 4);
    rax43 = xrealloc(v13, r14_42);
    rsi44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_41) * 8);
    v20 = rax43;
    rax45 = xrealloc(v14, rsi44);
    r15_23 = rax45;
    rax46 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v20) + reinterpret_cast<unsigned char>(r14_42) + 0xfffffffffffffff0);
    fun_2ab0(rax46, rsi44, rdx10, rcx9, r8);
    rax38 = rax46;
    v37 = reinterpret_cast<void*>(0);
    v12 = reinterpret_cast<void**>(0);
    goto addr_36da_11;
}