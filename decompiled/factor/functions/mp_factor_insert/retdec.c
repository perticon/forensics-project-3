void mp_factor_insert(int32_t * factors, int32_t * prime) {
    int64_t v1 = (int64_t)factors;
    int64_t * v2 = (int64_t *)(v1 + 16); // 0x35f4
    int64_t v3 = *v2; // 0x35f4
    int64_t * v4 = (int64_t *)(v1 + 8); // 0x35fb
    int64_t v5 = v3 - 1; // 0x35ff
    int64_t v6; // 0x35e0
    int64_t v7; // 0x35e0
    int64_t v8; // 0x35e0
    int64_t v9; // 0x35e0
    int64_t v10; // 0x35e0
    int32_t * v11; // 0x35e0
    int32_t * v12; // 0x35e0
    int64_t v13; // 0x35e0
    int64_t v14; // 0x35e0
    if (v5 < 0) {
        int64_t v15 = xrealloc(); // 0x37e7
        int64_t v16 = xrealloc(); // 0x3801
        function_2ab0();
        v14 = v3;
        v12 = (int32_t *)v15;
        v10 = v3 + 1;
        v7 = v16;
        goto lab_0x370b;
    } else {
        int64_t v17 = v5;
        int32_t v18 = function_2a00(); // 0x3649
        while (v18 >= 0 == (v18 != 0)) {
            // 0x3630
            if (v17 == 0) {
                int64_t v19 = xrealloc(); // 0x368d
                int64_t v20 = xrealloc(); // 0x36a4
                function_2ab0();
                v13 = 0;
                v11 = (int32_t *)v19;
                v9 = v3 + 1;
                v8 = -1;
                v6 = v20;
                goto lab_0x36da;
            }
            v17--;
            v18 = function_2a00();
        }
        if (v18 == 0) {
            int64_t * v21 = (int64_t *)(8 * v17 + *v4); // 0x3658
            *v21 = *v21 + 1;
            return;
        }
        int64_t v22 = v3 + 1; // 0x3752
        int32_t * v23 = (int32_t *)xrealloc(); // 0x377c
        int64_t v24 = xrealloc(); // 0x3781
        function_2ab0();
        int64_t v25 = v17 + 1; // 0x37a5
        v13 = v25;
        v11 = v23;
        v9 = v22;
        v8 = v17;
        v6 = v24;
        v14 = v25;
        v12 = v23;
        v10 = v22;
        v7 = v24;
        if (v17 < v5) {
            goto lab_0x36da;
        } else {
            goto lab_0x370b;
        }
    }
  lab_0x370b:
    // 0x370b
    function_27a0();
    *(int64_t *)(v7 + 8 * v14) = 1;
    *v4 = v7;
    *(int64_t *)factors = (int64_t)v12;
    *v2 = v10;
  lab_0x36da:
    // 0x36da
    function_27a0();
    int64_t v26 = 8 * v5 + v6;
    *(int64_t *)(v26 + 8) = *(int64_t *)v26;
    int64_t v27 = v5 - 1; // 0x36f8
    int64_t v28 = v27; // 0x36ff
    v14 = v13;
    v12 = v11;
    v10 = v9;
    v7 = v6;
    while (v8 < v27) {
        // 0x36e0
        function_27a0();
        v26 = 8 * v28 + v6;
        *(int64_t *)(v26 + 8) = *(int64_t *)v26;
        v27 = v28 - 1;
        v28 = v27;
        v14 = v13;
        v12 = v11;
        v10 = v9;
        v7 = v6;
    }
    goto lab_0x370b;
}