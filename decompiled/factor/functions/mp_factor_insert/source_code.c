mp_factor_insert (struct mp_factors *factors, mpz_t prime)
{
  unsigned long int nfactors = factors->nfactors;
  mpz_t         *p  = factors->p;
  unsigned long int *e  = factors->e;
  long i;

  /* Locate position for insert new or increment e.  */
  for (i = nfactors - 1; i >= 0; i--)
    {
      if (mpz_cmp (p[i], prime) <= 0)
        break;
    }

  if (i < 0 || mpz_cmp (p[i], prime) != 0)
    {
      p = xrealloc (p, (nfactors + 1) * sizeof p[0]);
      e = xrealloc (e, (nfactors + 1) * sizeof e[0]);

      mpz_init (p[nfactors]);
      for (long j = nfactors - 1; j > i; j--)
        {
          mpz_set (p[j + 1], p[j]);
          e[j + 1] = e[j];
        }
      mpz_set (p[i + 1], prime);
      e[i + 1] = 1;

      factors->p = p;
      factors->e = e;
      factors->nfactors = nfactors + 1;
    }
  else
    {
      e[i] += 1;
    }
}