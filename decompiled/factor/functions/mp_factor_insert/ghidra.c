void mp_factor_insert(long *param_1,undefined8 param_2)

{
  long *plVar1;
  long lVar2;
  int iVar3;
  long lVar4;
  long lVar5;
  long lVar6;
  long lVar7;
  long local_60;
  long local_58;
  long local_50;
  long local_48;
  
  local_48 = param_1[2];
  lVar4 = *param_1;
  lVar2 = param_1[1];
  lVar7 = local_48 + -1;
  if (lVar7 < 0) {
    local_50 = (local_48 + 1) * 0x10;
    local_58 = xrealloc(lVar4,local_50);
    lVar4 = xrealloc(lVar2,(local_48 + 1) * 8);
    local_50 = local_58 + -0x10 + local_50;
    __gmpz_init();
    local_60 = local_48;
  }
  else {
    lVar5 = lVar4 + lVar7 * 0x10;
    lVar6 = lVar7;
    do {
      iVar3 = __gmpz_cmp(lVar5,param_2);
      if (iVar3 < 1) {
        if (iVar3 == 0) {
          plVar1 = (long *)(lVar2 + lVar6 * 8);
          *plVar1 = *plVar1 + 1;
          return;
        }
        lVar5 = (local_48 + 1) * 0x10;
        local_58 = xrealloc(lVar4,lVar5);
        lVar4 = xrealloc(lVar2,(local_48 + 1) * 8);
        lVar5 = local_58 + -0x10 + lVar5;
        __gmpz_init(lVar5);
        local_60 = lVar6 + 1;
        local_50 = local_60 * 0x10;
        if (lVar7 <= lVar6) goto LAB_00103701;
        goto LAB_001036e0;
      }
      lVar6 = lVar6 + -1;
      lVar5 = lVar5 + -0x10;
    } while (lVar6 != -1);
    lVar5 = (local_48 + 1) * 0x10;
    local_58 = xrealloc(lVar4,lVar5);
    lVar4 = xrealloc(lVar2,(local_48 + 1) * 8);
    lVar5 = local_58 + -0x10 + lVar5;
    __gmpz_init(lVar5);
    local_50 = 0;
    local_60 = 0;
LAB_001036e0:
    do {
      __gmpz_set(lVar5,lVar5 + -0x10);
      *(undefined8 *)(lVar4 + 8 + lVar7 * 8) = *(undefined8 *)(lVar4 + lVar7 * 8);
      lVar7 = lVar7 + -1;
      lVar5 = lVar5 + -0x10;
    } while (lVar6 < lVar7);
LAB_00103701:
    local_50 = local_50 + local_58;
  }
  local_48 = local_48 + 1;
  __gmpz_set(local_50,param_2);
  *(undefined8 *)(lVar4 + local_60 * 8) = 1;
  param_1[1] = lVar4;
  *param_1 = local_58;
  param_1[2] = local_48;
  return;
}