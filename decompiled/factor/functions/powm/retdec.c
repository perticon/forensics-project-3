int64_t powm(int64_t b, int64_t e, int64_t n, int64_t ni, int64_t one) {
    int64_t v1 = e; // 0x2fca
    int64_t result = one; // 0x2fca
    int64_t v2 = e; // 0x2fca
    if (e % 2 == 0) {
        goto lab_0x3020;
    } else {
        goto lab_0x2fd0;
    }
  lab_0x3020:
    // 0x3020
    v1 = v2;
    if (v1 == 0) {
        // 0x3025
        return result;
    }
    goto lab_0x2fd0;
  lab_0x2fd0:;
    int64_t v3 = v1 / 2; // 0x2ff1
    int64_t v4 = v3; // 0x2ff8
    v2 = v3;
    if ((v1 & 2) != 0) {
        uint64_t v5 = v4;
        int64_t v6 = v5 / 2; // 0x2ff1
        v2 = v6;
        while ((v5 & 2) != 0) {
            // 0x2ffa
            v5 = v6;
            v6 = v5 / 2;
            v2 = v6;
        }
    }
    // 0x3020
    goto lab_0x3020;
}