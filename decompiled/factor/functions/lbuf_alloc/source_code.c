lbuf_alloc (void)
{
  if (lbuf.buf)
    return;

  /* Double to ensure enough space for
     previous numbers + next number.  */
  lbuf.buf = xmalloc (FACTOR_PIPE_BUF * 2);
  lbuf.end = lbuf.buf;
}