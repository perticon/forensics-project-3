void factor_using_pollard_rho2(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r15_5;
    void** rbp6;
    void*** rsp7;
    void** v8;
    void** v9;
    void** rax10;
    void** v11;
    uint64_t rcx12;
    void** rdx13;
    int64_t rcx14;
    uint64_t rcx15;
    uint1_t cf16;
    void** rax17;
    void** rsi18;
    uint1_t cf19;
    int1_t cf20;
    void** v21;
    void** v22;
    void** tmp64_23;
    void** rax24;
    void** r12_25;
    void** r10_26;
    void** v27;
    void** v28;
    void** v29;
    void** r13_30;
    void** rbx31;
    void** v32;
    void** r14_33;
    void** r15_34;
    void*** v35;
    void** r9_36;
    uint64_t rax37;
    int64_t rax38;
    void* rax39;
    void* rdx40;
    void* rax41;
    void** rax42;
    void** rbp43;
    int64_t rax44;
    void** v45;
    int64_t v46;
    void** rax47;
    void** tmp64_48;
    void** rdx49;
    void* v50;
    void** v51;
    void** rdx52;
    void** r8_53;
    void** rcx54;
    void** tmp64_55;
    uint1_t cf56;
    void** rax57;
    void** v58;
    void** rax59;
    int64_t rax60;
    void** v61;
    void** rax62;
    void** v63;
    void** v64;
    void** rdx65;
    void** r15_66;
    void** r12_67;
    void** r13_68;
    void** rsi69;
    void** rbp70;
    void** rax71;
    void** tmp64_72;
    void** rcx73;
    void* v74;
    void** v75;
    void** r11_76;
    void** r10_77;
    void** v78;
    void** r15_79;
    void** r12_80;
    void*** rbp81;
    void** r9_82;
    void** rax83;
    void** tmp64_84;
    void** rdx85;
    void* v86;
    void** rcx87;
    void** rdx88;
    void** rdx89;
    void** rsi90;
    void** tmp64_91;
    uint1_t cf92;
    void** rax93;
    void** rdi94;
    void** r8_95;
    uint64_t rax96;
    int64_t rax97;
    void** r13_98;
    void* rax99;
    void* rdx100;
    void** rdx101;
    void* rax102;
    signed char al103;
    void** rdx104;
    void** rsi105;
    void** rdi106;
    unsigned char al107;
    void** rax108;
    void* rsp109;
    void** rax110;
    void* rsp111;
    void** rax112;
    uint64_t rax113;
    struct s78* rax114;
    void* rax115;
    void* rdx116;
    void* rax117;
    unsigned char al118;
    void** r8_119;
    void** r9_120;
    void** v121;
    void** rbp122;
    void** v123;
    void*** rsp124;
    void** r12_125;
    int64_t r13_126;
    void** r11_127;
    void** r10_128;
    void*** r14_129;
    struct s79* rbx130;
    uint64_t r15_131;
    void** rcx132;
    void** rdx133;
    void** r10_134;
    void** r13_135;
    void* rax136;
    int32_t ecx137;
    int32_t ecx138;
    int32_t edx139;
    int64_t rbx140;
    void** rcx141;
    void** rdx142;
    struct s80* rcx143;
    void* rcx144;
    void** rcx145;
    void** rdx146;
    void** rdx147;
    struct s81* rcx148;
    struct s82* rsi149;
    void* rsi150;
    void* rcx151;
    struct s83* rsi152;
    void* rsi153;
    void** rcx154;
    void** rdx155;
    void** v156;
    void** rdx157;
    uint32_t ecx158;
    uint32_t edx159;
    void** rsi160;
    struct s84* rdi161;
    void* rdi162;
    void** rdx163;
    void** rdx164;
    uint32_t ecx165;
    uint32_t edx166;
    void** rsi167;
    struct s85* rdi168;
    void* rdi169;
    void** rdx170;
    void** rdx171;
    uint32_t ecx172;
    uint32_t edx173;
    void** rsi174;
    struct s86* rdi175;
    void* rdi176;
    void** rdx177;
    int64_t rax178;
    void* rax179;
    int64_t rax180;
    struct s87* rax181;
    uint32_t ecx182;
    uint32_t edx183;
    void** rsi184;
    struct s88* rdi185;
    void* rdi186;
    void** rdx187;
    struct s89* rcx188;
    struct s90* rsi189;
    void* rcx190;
    void* rsi191;
    void** rcx192;
    void** rdi193;
    void** v194;
    void** v195;
    unsigned char al196;
    int64_t rax197;
    struct s91* rax198;
    void* rax199;
    signed char al200;
    unsigned char al201;
    void** r9_202;
    void** rsi203;
    void** rdi204;
    void** rdi205;
    void** rdx206;
    void** rsi207;
    int64_t rax208;
    void** r8_209;
    void*** r11_210;
    signed char* r10_211;
    uint32_t r9d212;
    int32_t ebx213;
    void* rcx214;
    void* rax215;
    int64_t rsi216;
    void* rax217;
    uint32_t r9d218;
    uint32_t eax219;
    void** r12_220;
    void** rcx221;
    int32_t esi222;
    void** rax223;
    void* rdx224;
    void* rdi225;
    uint1_t cf226;
    int64_t rbp227;
    int64_t r15_228;
    int64_t r14_229;
    void** rbp230;
    void** rcx231;
    void** r11_232;
    uint64_t rax233;
    int64_t rax234;
    void* rax235;
    void* rdx236;
    void* rax237;
    void** r8_238;
    void* r10_239;
    void* r9_240;
    void* rax241;
    void** r13_242;
    int64_t rax243;
    int64_t rax244;
    void** rdi245;
    void** rax246;
    int64_t rcx247;
    void* rdi248;
    void* rax249;
    void* r11_250;
    void* rsi251;
    void* rax252;
    void** rbx253;
    void** rsi254;
    void** rax255;
    void** rcx256;
    void** r11_257;
    void** rdx258;
    void** rax259;
    void** r8_260;
    signed char al261;
    void** rdx262;
    void** rsi263;
    void** rcx264;
    int1_t zf265;
    signed char al266;
    struct s0* r14_267;
    void** rbp268;
    struct s0** rsp269;
    void** rax270;
    void** v271;
    uint32_t eax272;
    struct s0* v273;
    uint32_t v274;
    void** rcx275;
    void** r15_276;
    uint64_t rax277;
    int32_t esi278;
    int64_t rax279;
    void* rax280;
    void* rdx281;
    void* rax282;
    void** rdx283;
    struct s0* r13_284;
    void** rax285;
    uint64_t rdi286;
    uint1_t cf287;
    int64_t rbx288;
    void** rbx289;
    unsigned char al290;
    void* v291;
    struct s0* v292;
    uint32_t eax293;
    unsigned char v294;
    uint32_t v295;
    int64_t rax296;
    void* v297;
    void** rax298;
    void* rax299;
    void** rax300;
    void* rax301;
    void** rax302;
    void** rdx303;
    void** rcx304;
    int32_t esi305;
    void** rax306;
    uint64_t rdi307;
    uint1_t cf308;
    unsigned char al309;
    void* r11_310;
    uint64_t v311;

    while (1) {
        r15_5 = rdi;
        rbp6 = rsi;
        rsp7 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x88);
        v8 = rdx;
        v9 = rcx;
        rax10 = g28;
        v11 = rax10;
        rcx12 = reinterpret_cast<unsigned char>(rcx) - (reinterpret_cast<unsigned char>(rcx) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi) < reinterpret_cast<unsigned char>(2)))))));
        *reinterpret_cast<int32_t*>(&rdx13) = 0;
        *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx14) = *reinterpret_cast<uint32_t*>(&rcx12) & 64;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx14) + 4) = 0;
        rcx15 = reinterpret_cast<uint64_t>(rcx14 + 63);
        cf16 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi) < reinterpret_cast<unsigned char>(2));
        *reinterpret_cast<unsigned char*>(&rdx13) = cf16;
        rax17 = reinterpret_cast<void**>(-static_cast<uint64_t>(reinterpret_cast<uint1_t>(0 < static_cast<uint64_t>(cf16))) + 1);
        do {
            rsi18 = rdx13;
            rdx13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx13) + reinterpret_cast<unsigned char>(rdx13));
            rax17 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax17) + reinterpret_cast<unsigned char>(rax17)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rsi18) >> 63));
            if (reinterpret_cast<unsigned char>(r15_5) < reinterpret_cast<unsigned char>(rax17) || r15_5 == rax17 && reinterpret_cast<unsigned char>(rbp6) <= reinterpret_cast<unsigned char>(rdx13)) {
                cf19 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx13) < reinterpret_cast<unsigned char>(rbp6));
                rdx13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx13) - reinterpret_cast<unsigned char>(rbp6));
                rax17 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax17) - (reinterpret_cast<unsigned char>(r15_5) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax17) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r15_5) + static_cast<uint64_t>(cf19))))));
            }
            cf20 = rcx15 < 1;
            --rcx15;
        } while (!cf20);
        v21 = rdx13;
        v22 = rax17;
        tmp64_23 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx13) + reinterpret_cast<unsigned char>(rdx13));
        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax17) + reinterpret_cast<unsigned char>(rax17) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_23) < reinterpret_cast<unsigned char>(rdx13))));
        r12_25 = tmp64_23;
        r10_26 = rax24;
        if (reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(r15_5) || rax24 == r15_5 && reinterpret_cast<unsigned char>(tmp64_23) >= reinterpret_cast<unsigned char>(rbp6)) {
            r12_25 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_23) - reinterpret_cast<unsigned char>(rbp6));
            r10_26 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) - (reinterpret_cast<unsigned char>(r15_5) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r15_5) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_23) < reinterpret_cast<unsigned char>(rbp6))))))));
        }
        v27 = r10_26;
        v28 = r10_26;
        if (r15_5) 
            goto addr_468c_8;
        if (rbp6 == 1) 
            goto addr_4ad5_10;
        addr_468c_8:
        v29 = r12_25;
        r13_30 = r15_5;
        *reinterpret_cast<int32_t*>(&rbx31) = 1;
        *reinterpret_cast<int32_t*>(&rbx31 + 4) = 0;
        v32 = reinterpret_cast<void**>(1);
        r14_33 = reinterpret_cast<void**>(rsp7 + 0x70);
        r15_34 = r12_25;
        v35 = rsp7 + 0x68;
        while (1) {
            r9_36 = r13_30;
            r13_30 = rbx31;
            rax37 = reinterpret_cast<unsigned char>(rbp6) >> 1;
            rbx31 = rbp6;
            *reinterpret_cast<uint32_t*>(&rax38) = *reinterpret_cast<uint32_t*>(&rax37) & 0x7f;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax38) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rax39) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax38);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax39) + 4) = 0;
            rdx40 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax39) + reinterpret_cast<int64_t>(rax39) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax39) * reinterpret_cast<int64_t>(rax39) * reinterpret_cast<unsigned char>(rbp6)));
            rax41 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx40) + reinterpret_cast<uint64_t>(rdx40) - reinterpret_cast<uint64_t>(rdx40) * reinterpret_cast<uint64_t>(rdx40) * reinterpret_cast<unsigned char>(rbp6));
            rax42 = rbp6;
            rbp43 = r10_26;
            *reinterpret_cast<uint32_t*>(&rax44) = *reinterpret_cast<uint32_t*>(&rax42) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax44) + 4) = 0;
            v45 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax41) + reinterpret_cast<uint64_t>(rax41) - reinterpret_cast<uint64_t>(rax41) * reinterpret_cast<uint64_t>(rax41) * reinterpret_cast<unsigned char>(rbp6));
            v46 = rax44;
            while (1) {
                rax47 = mulredc2(r14_33, rbp43, r12_25, rbp43, r12_25, r9_36, rbx31, v22);
                tmp64_48 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax47) + reinterpret_cast<unsigned char>(v8));
                rdx49 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v50) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_48) < reinterpret_cast<unsigned char>(rax47))));
                v51 = rdx49;
                r12_25 = tmp64_48;
                rbp43 = rdx49;
                if (reinterpret_cast<unsigned char>(rdx49) > reinterpret_cast<unsigned char>(r9_36) || rdx49 == r9_36 && reinterpret_cast<unsigned char>(tmp64_48) >= reinterpret_cast<unsigned char>(rbx31)) {
                    rdx52 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx49) - (reinterpret_cast<unsigned char>(r9_36) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx49) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r9_36) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_48) < reinterpret_cast<unsigned char>(rbx31))))))));
                    v51 = rdx52;
                    r12_25 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_48) - reinterpret_cast<unsigned char>(rbx31));
                    rbp43 = rdx52;
                }
                r8_53 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v29) - reinterpret_cast<unsigned char>(r12_25));
                rcx54 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v27) - (reinterpret_cast<unsigned char>(rbp43) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v27) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp43) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v29) < reinterpret_cast<unsigned char>(r12_25))))))));
                if (reinterpret_cast<signed char>(rcx54) < reinterpret_cast<signed char>(0)) {
                    tmp64_55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_53) + reinterpret_cast<unsigned char>(rbx31));
                    cf56 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_55) < reinterpret_cast<unsigned char>(r8_53));
                    r8_53 = tmp64_55;
                    rcx54 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx54) + reinterpret_cast<unsigned char>(r9_36) + static_cast<uint64_t>(cf56));
                }
                rax57 = mulredc2(r14_33, v22, v21, rcx54, r8_53, r9_36, rbx31, v22);
                v21 = rax57;
                v22 = v58;
                rax59 = r13_30;
                *reinterpret_cast<uint32_t*>(&rax60) = *reinterpret_cast<uint32_t*>(&rax59) & 31;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax60) + 4) = 0;
                rsp7 = rsp7 - 8 - 8 - 8 + 8 + 8 + 8 - 8 - 8 - 8 + 8 + 8 + 8;
                r9_36 = r9_36;
                if (rax60 == 1) {
                    if (!v46) 
                        goto addr_4cba_18;
                    if (reinterpret_cast<unsigned char>(v21) | reinterpret_cast<unsigned char>(v22)) 
                        goto addr_48c0_20;
                } else {
                    addr_47d9_21:
                    --r13_30;
                    if (r13_30) 
                        continue; else 
                        goto addr_47e3_22;
                }
                v61 = r9_36;
                rax62 = rbx31;
                if (r9_36) 
                    break;
                addr_48ee_24:
                if (!reinterpret_cast<int1_t>(rax62 == 1)) 
                    goto addr_4920_25;
                v28 = rbp43;
                r15_34 = r12_25;
                goto addr_47d9_21;
                addr_48c0_20:
                rax62 = gcd2_odd_part_0(v35, v22, v21, r9_36, rbx31, r9_36);
                rsp7 = rsp7 - 8 + 8;
                r9_36 = r9_36;
                if (v61) 
                    goto addr_4920_25; else 
                    goto addr_48ee_24;
                addr_47e3_22:
                v27 = rbp43;
                r15_34 = r12_25;
                v63 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v32) + reinterpret_cast<unsigned char>(v32));
                if (v32) {
                    v64 = r12_25;
                    rdx65 = r12_25;
                    r15_66 = r13_30;
                    r12_67 = v45;
                    r13_68 = v8;
                    rsi69 = rbp43;
                    rbp70 = r9_36;
                    do {
                        rax71 = mulredc2(r14_33, rsi69, rdx65, rsi69, rdx65, rbp70, rbx31, r12_67);
                        tmp64_72 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax71) + reinterpret_cast<unsigned char>(r13_68));
                        rcx73 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v74) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_72) < reinterpret_cast<unsigned char>(rax71))));
                        rdx65 = tmp64_72;
                        rsi69 = rcx73;
                        rsp7 = rsp7 - 8 - 8 - 8 + 8 + 8 + 8;
                        if (reinterpret_cast<unsigned char>(rcx73) > reinterpret_cast<unsigned char>(rbp70) || rcx73 == rbp70 && reinterpret_cast<unsigned char>(tmp64_72) >= reinterpret_cast<unsigned char>(rbx31)) {
                            rdx65 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_72) - reinterpret_cast<unsigned char>(rbx31));
                            rsi69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx73) - (reinterpret_cast<unsigned char>(rbp70) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx73) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp70) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_72) < reinterpret_cast<unsigned char>(rbx31))))))));
                        }
                        ++r15_66;
                    } while (v32 != r15_66);
                    r12_25 = v64;
                    r9_36 = rbp70;
                    r15_34 = rdx65;
                    rbp43 = rsi69;
                }
                r13_30 = v32;
                v29 = r12_25;
                r12_25 = r15_34;
                v28 = rbp43;
                v32 = v63;
            }
            addr_4920_25:
            v75 = r12_25;
            r11_76 = r15_34;
            r10_77 = v28;
            v78 = r13_30;
            r15_79 = r14_33;
            r13_30 = rbx31;
            r12_80 = v29;
            r14_33 = v45;
            rbp81 = v35;
            rbx31 = r9_36;
            do {
                r9_82 = rbx31;
                rax83 = mulredc2(r15_79, r10_77, r11_76, r10_77, r11_76, r9_82, r13_30, r14_33);
                tmp64_84 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax83) + reinterpret_cast<unsigned char>(v8));
                rdx85 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v86) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_84) < reinterpret_cast<unsigned char>(rax83))));
                v28 = rdx85;
                r11_76 = tmp64_84;
                rcx87 = r13_30;
                r10_77 = rdx85;
                rsp7 = rsp7 - 8 - 8 - 8 + 8 + 8 + 8;
                if (reinterpret_cast<unsigned char>(rdx85) > reinterpret_cast<unsigned char>(rbx31) || rdx85 == rbx31 && reinterpret_cast<unsigned char>(tmp64_84) >= reinterpret_cast<unsigned char>(r13_30)) {
                    rdx88 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx85) - (reinterpret_cast<unsigned char>(rbx31) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx85) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbx31) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_84) < reinterpret_cast<unsigned char>(r13_30))))))));
                    v28 = rdx88;
                    r11_76 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_84) - reinterpret_cast<unsigned char>(r13_30));
                    r10_77 = rdx88;
                }
                rdx89 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_80) - reinterpret_cast<unsigned char>(r11_76));
                rsi90 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v27) - (reinterpret_cast<unsigned char>(r10_77) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v27) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r10_77) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_80) < reinterpret_cast<unsigned char>(r11_76))))))));
                if (reinterpret_cast<signed char>(rsi90) < reinterpret_cast<signed char>(0)) {
                    tmp64_91 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx89) + reinterpret_cast<unsigned char>(r13_30));
                    cf92 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_91) < reinterpret_cast<unsigned char>(rdx89));
                    rdx89 = tmp64_91;
                    rsi90 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi90) + reinterpret_cast<unsigned char>(rbx31) + static_cast<uint64_t>(cf92));
                }
                if (reinterpret_cast<unsigned char>(rsi90) | reinterpret_cast<unsigned char>(rdx89)) {
                    rcx87 = rbx31;
                    rax93 = gcd2_odd_part_0(rbp81, rsi90, rdx89, rcx87, r13_30, r9_82);
                    rsp7 = rsp7 - 8 + 8;
                    rdi94 = v61;
                    if (rdi94) 
                        goto addr_4a05_40;
                } else {
                    rdi94 = rbx31;
                    v61 = rbx31;
                    rax93 = r13_30;
                    if (rdi94) 
                        goto addr_4a05_40;
                }
            } while (reinterpret_cast<int1_t>(rax93 == 1));
            r8_95 = rax93;
            rax96 = reinterpret_cast<unsigned char>(rax93) >> 1;
            *reinterpret_cast<uint32_t*>(&rax97) = *reinterpret_cast<uint32_t*>(&rax96) & 0x7f;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax97) + 4) = 0;
            r13_98 = rbx31;
            r14_33 = r15_79;
            *reinterpret_cast<uint32_t*>(&rax99) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax97);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax99) + 4) = 0;
            rbx31 = v78;
            rdx100 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax99) + reinterpret_cast<int64_t>(rax99) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax99) * reinterpret_cast<int64_t>(rax99) * reinterpret_cast<unsigned char>(r8_95)));
            rdx101 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdx100) * reinterpret_cast<uint64_t>(rdx100) * reinterpret_cast<unsigned char>(r8_95));
            rax102 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx100) + reinterpret_cast<uint64_t>(rdx100) - reinterpret_cast<unsigned char>(rdx101));
            rcx87 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax102) + reinterpret_cast<uint64_t>(rax102) - reinterpret_cast<uint64_t>(rax102) * reinterpret_cast<uint64_t>(rax102) * reinterpret_cast<unsigned char>(r8_95));
            rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_30) * reinterpret_cast<unsigned char>(rcx87));
            if (reinterpret_cast<unsigned char>(r13_98) < reinterpret_cast<unsigned char>(r8_95)) {
                *reinterpret_cast<int32_t*>(&r13_30) = 0;
                *reinterpret_cast<int32_t*>(&r13_30 + 4) = 0;
            } else {
                rdx101 = __intrinsic();
                r9_82 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r13_98) - reinterpret_cast<unsigned char>(rdx101)) * reinterpret_cast<unsigned char>(rcx87));
                r13_30 = r9_82;
            }
            if (reinterpret_cast<unsigned char>(r8_95) <= reinterpret_cast<unsigned char>(1) || reinterpret_cast<unsigned char>(r8_95) > reinterpret_cast<unsigned char>(0x17ded78) && (al103 = prime_p_part_0(r8_95, rsi90, rdx101, rcx87), rsp7 = rsp7 - 8 + 8, r8_95 = r8_95, al103 == 0)) {
                rdx104 = v9;
                rsi105 = v8 + 1;
                factor_using_pollard_rho(r8_95, rsi105, rdx104, rcx87);
                rsp7 = rsp7 - 8 + 8;
            } else {
                *reinterpret_cast<int32_t*>(&rdx104) = 1;
                *reinterpret_cast<int32_t*>(&rdx104 + 4) = 0;
                rsi105 = r8_95;
                factor_insert_multiplicity(v9, rsi105, 1);
                rsp7 = rsp7 - 8 + 8;
            }
            if (!r13_30) 
                goto addr_4aa0_50;
            rsi105 = rbp6;
            rdi106 = r13_30;
            al107 = prime2_p(rdi106, rsi105);
            rsp7 = rsp7 - 8 + 8;
            if (al107) 
                goto addr_4c8b_52;
            rax108 = mod2(rsp7 + 80, v51, v75, r13_30, rbp6, r9_82);
            rsp109 = reinterpret_cast<void*>(rsp7 - 8 + 8);
            r12_25 = rax108;
            rax110 = mod2(reinterpret_cast<int64_t>(rsp109) + 88, v27, v29, r13_30, rbp6, r9_82);
            rsp111 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp109) - 8 + 8);
            v29 = rax110;
            rax112 = mod2(reinterpret_cast<int64_t>(rsp111) + 96, v28, r11_76, r13_30, rbp6, r9_82);
            rsp7 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp111) - 8 + 8);
            r10_26 = v51;
            r15_34 = rax112;
        }
        addr_4a05_40:
        if (rbx31 != rdi94) 
            goto addr_4a19_54;
        if (rax93 == r13_30) 
            goto addr_4ce0_56;
        addr_4a19_54:
        rbx31 = reinterpret_cast<void**>(0xd5c0);
        rsi105 = rax93;
        v32 = rax93;
        rax113 = reinterpret_cast<unsigned char>(rax93) >> 1;
        *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<uint32_t*>(&rax113) & 0x7f;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(0xd5c0) + reinterpret_cast<uint64_t>(rax114));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
        rdx116 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax115) + reinterpret_cast<int64_t>(rax115) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax115) * reinterpret_cast<int64_t>(rax115) * reinterpret_cast<unsigned char>(rax93)));
        rax117 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx116) + reinterpret_cast<uint64_t>(rdx116) - reinterpret_cast<uint64_t>(rdx116) * reinterpret_cast<uint64_t>(rdx116) * reinterpret_cast<unsigned char>(rax93));
        rdx104 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax117) + reinterpret_cast<uint64_t>(rax117) - reinterpret_cast<uint64_t>(rax117) * reinterpret_cast<uint64_t>(rax117) * reinterpret_cast<unsigned char>(rax93));
        rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_30) * reinterpret_cast<unsigned char>(rdx104));
        al118 = prime2_p(rdi94, rsi105);
        rsp7 = rsp7 - 8 + 8;
        if (!al118) 
            goto addr_4c4f_57;
        if (!v61) 
            goto addr_4ca3_59;
        rdi106 = v9;
        if (!*reinterpret_cast<void***>(rdi106 + 8)) 
            goto addr_4a94_61;
        addr_4cfe_62:
        factor_insert_large_part_0();
        r8_119 = rdi106;
        r9_120 = rdx104;
        v121 = r13_30;
        rbp122 = rsi105;
        v123 = rbx31;
        rsp124 = rsp7 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 56;
        *reinterpret_cast<unsigned char*>(rdx104 + 0xfa) = 0;
        *reinterpret_cast<void***>(rdx104 + 8) = reinterpret_cast<void**>(0);
        if (rdi106) 
            goto addr_4d50_64;
        if (reinterpret_cast<unsigned char>(rsi105) <= reinterpret_cast<unsigned char>(1)) 
            goto addr_4d41_66;
        addr_4d50_64:
        if (*reinterpret_cast<unsigned char*>(&rbp122) & 1) {
            addr_4df3_67:
            if (!r8_119) {
                *reinterpret_cast<int32_t*>(&r12_125) = 3;
                *reinterpret_cast<int32_t*>(&r12_125 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r13_126) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_126) + 4) = 0;
                r11_127 = reinterpret_cast<void**>(0x5555555555555555);
                r10_128 = reinterpret_cast<void**>(0xaaaaaaaaaaaaaaab);
            } else {
                r14_129 = reinterpret_cast<void***>(0xd648);
                *reinterpret_cast<uint32_t*>(&rbx130) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx130) + 4) = 0;
                r15_131 = 0xaaaaaaaaaaaaaaab;
                *reinterpret_cast<int32_t*>(&r12_125) = 3;
                *reinterpret_cast<int32_t*>(&r12_125 + 4) = 0;
                while (1) {
                    rcx132 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp122) * r15_131);
                    rdx133 = __intrinsic();
                    if (reinterpret_cast<unsigned char>(rdx133) <= reinterpret_cast<unsigned char>(r8_119)) {
                        r10_134 = *r14_129;
                        while ((r13_135 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(r8_119) - reinterpret_cast<unsigned char>(rdx133)) * r15_131), reinterpret_cast<unsigned char>(r13_135) <= reinterpret_cast<unsigned char>(r10_134)) && (factor_insert_multiplicity(r9_120, r12_125, 1), rsp124 = rsp124 - 8 + 8, r8_119 = r13_135, r9_120 = r9_120, r10_134 = r10_134, rbp122 = rcx132, rdx133 = __intrinsic(), reinterpret_cast<unsigned char>(rdx133) <= reinterpret_cast<unsigned char>(r13_135))) {
                            rcx132 = reinterpret_cast<void**>(r15_131 * reinterpret_cast<unsigned char>(rcx132));
                        }
                    }
                    *reinterpret_cast<uint32_t*>(&rax136) = rbx130->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax136) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&r13_126) = *reinterpret_cast<uint32_t*>(&rbx130);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_126) + 4) = 0;
                    r14_129 = r14_129 + 16;
                    r12_125 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_125) + reinterpret_cast<uint64_t>(rax136));
                    if (!r8_119) 
                        break;
                    if (*reinterpret_cast<uint32_t*>(&rbx130) > 0x29b) 
                        break;
                    r15_131 = *reinterpret_cast<uint64_t*>(r14_129 - 8);
                    rbx130 = reinterpret_cast<struct s79*>(&rbx130->pad8);
                }
                if (*reinterpret_cast<uint32_t*>(&r13_126) > 0x29b) 
                    goto addr_5200_78; else 
                    goto addr_4edd_79;
            }
        } else {
            if (rbp122) {
                __asm__("bsf rdx, rbp");
                ecx137 = 64 - *reinterpret_cast<int32_t*>(&rdx104);
                ecx138 = *reinterpret_cast<int32_t*>(&rdx104);
                rbp122 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp122) >> *reinterpret_cast<signed char*>(&ecx138)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_119) << *reinterpret_cast<unsigned char*>(&ecx137)));
                factor_insert_multiplicity(r9_120, 2, *reinterpret_cast<signed char*>(&rdx104));
                rsp124 = rsp124 - 8 + 8;
                r8_119 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_119) >> *reinterpret_cast<signed char*>(&ecx138));
                r9_120 = r9_120;
                goto addr_4df3_67;
            } else {
                __asm__("bsf rcx, r8");
                edx139 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rcx87 + 64));
                rbp122 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_119) >> *reinterpret_cast<signed char*>(&rcx87));
                *reinterpret_cast<int32_t*>(&r12_125) = 3;
                *reinterpret_cast<int32_t*>(&r12_125 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r13_126) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_126) + 4) = 0;
                factor_insert_multiplicity(r9_120, 2, *reinterpret_cast<signed char*>(&edx139));
                rsp124 = rsp124 - 8 + 8;
                r9_120 = r9_120;
                *reinterpret_cast<uint32_t*>(&r8_119) = 0;
                *reinterpret_cast<int32_t*>(&r8_119 + 4) = 0;
                r11_127 = reinterpret_cast<void**>(0x5555555555555555);
                r10_128 = reinterpret_cast<void**>(0xaaaaaaaaaaaaaaab);
            }
        }
        addr_4ef9_83:
        *reinterpret_cast<int32_t*>(&rbx140) = static_cast<int32_t>(r13_126 + 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx140) + 4) = 0;
        r13_126 = rbx140;
        rbx130 = reinterpret_cast<struct s79*>((rbx140 << 4) + reinterpret_cast<unsigned char>(0xd640));
        while (1) {
            *reinterpret_cast<int32_t*>(&r15_131) = static_cast<int32_t>(r13_126 - 1);
            rcx141 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp122) * reinterpret_cast<unsigned char>(r10_128));
            if (reinterpret_cast<unsigned char>(r11_127) >= reinterpret_cast<unsigned char>(rcx141)) {
                do {
                    factor_insert_multiplicity(r9_120, r12_125, 1);
                    rsp124 = rsp124 - 8 + 8;
                    r10_128 = r10_128;
                    r11_127 = r11_127;
                    r9_120 = r9_120;
                    rbp122 = rcx141;
                    rcx141 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx141) * reinterpret_cast<unsigned char>(r10_128));
                    r8_119 = r8_119;
                } while (reinterpret_cast<unsigned char>(r11_127) >= reinterpret_cast<unsigned char>(rcx141));
                rdx142 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx130->f0) * reinterpret_cast<unsigned char>(rbp122));
                if (reinterpret_cast<unsigned char>(rbx130->f8) < reinterpret_cast<unsigned char>(rdx142)) 
                    goto addr_4f29_87;
                goto addr_5050_89;
            }
            rdx142 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx130->f0) * reinterpret_cast<unsigned char>(rbp122));
            if (reinterpret_cast<unsigned char>(rbx130->f8) >= reinterpret_cast<unsigned char>(rdx142)) {
                addr_5050_89:
                *reinterpret_cast<uint32_t*>(&rcx143) = *reinterpret_cast<uint32_t*>(&r13_126);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx143) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rcx144) = rcx143->f10340;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx144) + 4) = 0;
                rcx145 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx144) + reinterpret_cast<unsigned char>(r12_125));
            } else {
                addr_4f29_87:
                rdx146 = reinterpret_cast<void**>(rbx130->f10 * reinterpret_cast<unsigned char>(rbp122));
                if (reinterpret_cast<unsigned char>(rdx146) <= reinterpret_cast<unsigned char>(rbx130->f18)) 
                    goto addr_50b0_91; else 
                    goto addr_4f3b_92;
            }
            do {
                rbp122 = rdx142;
                factor_insert_multiplicity(r9_120, rcx145, 1);
                rsp124 = rsp124 - 8 + 8;
                r9_120 = r9_120;
                rcx145 = rcx145;
                r8_119 = r8_119;
                rdx142 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx130->f0) * reinterpret_cast<unsigned char>(rbp122));
            } while (reinterpret_cast<unsigned char>(rdx142) <= reinterpret_cast<unsigned char>(rbx130->f8));
            rdx146 = reinterpret_cast<void**>(rbx130->f10 * reinterpret_cast<unsigned char>(rbp122));
            if (reinterpret_cast<unsigned char>(rdx146) > reinterpret_cast<unsigned char>(rbx130->f18)) {
                addr_4f3b_92:
                rdx147 = reinterpret_cast<void**>(rbx130->f20 * reinterpret_cast<unsigned char>(rbp122));
                if (reinterpret_cast<unsigned char>(rdx147) <= reinterpret_cast<unsigned char>(rbx130->f28)) {
                    addr_5120_95:
                    *reinterpret_cast<int32_t*>(&rcx148) = static_cast<int32_t>(r13_126 + 2);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx148) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi149) = *reinterpret_cast<uint32_t*>(&r13_126);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi149) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi150) = rsi149->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi150) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rcx151) = rcx148->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx151) + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rsi152) = static_cast<int32_t>(r13_126 + 1);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi152) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rsi153) = rsi152->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi153) + 4) = 0;
                    rcx154 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx151) + reinterpret_cast<int64_t>(rsi150) + reinterpret_cast<unsigned char>(r12_125) + reinterpret_cast<uint64_t>(rsi153));
                } else {
                    addr_4f4d_96:
                    rdx155 = reinterpret_cast<void**>(rbx130->f30 * reinterpret_cast<unsigned char>(rbp122));
                    if (reinterpret_cast<unsigned char>(rdx155) <= reinterpret_cast<unsigned char>(rbx130->f38)) 
                        goto addr_5198_97; else 
                        goto addr_4f5f_98;
                }
            } else {
                goto addr_50b0_91;
            }
            do {
                rbp122 = rdx147;
                v156 = rcx154;
                factor_insert_multiplicity(r9_120, rcx154, 1);
                rsp124 = rsp124 - 8 + 8;
                r9_120 = r9_120;
                rcx154 = v156;
                r8_119 = r8_119;
                rdx147 = reinterpret_cast<void**>(rbx130->f20 * reinterpret_cast<unsigned char>(rbp122));
            } while (reinterpret_cast<unsigned char>(rdx147) <= reinterpret_cast<unsigned char>(rbx130->f28));
            rdx155 = reinterpret_cast<void**>(rbx130->f30 * reinterpret_cast<unsigned char>(rbp122));
            if (reinterpret_cast<unsigned char>(rdx155) > reinterpret_cast<unsigned char>(rbx130->f38)) {
                addr_4f5f_98:
                rdx157 = reinterpret_cast<void**>(rbx130->f40 * reinterpret_cast<unsigned char>(rbp122));
                if (reinterpret_cast<unsigned char>(rdx157) <= reinterpret_cast<unsigned char>(rbx130->f48)) {
                    rbp122 = rdx157;
                    ecx158 = static_cast<uint32_t>(r13_126 + 5);
                    while (1) {
                        edx159 = *reinterpret_cast<uint32_t*>(&r13_126);
                        rsi160 = r12_125;
                        do {
                            *reinterpret_cast<uint32_t*>(&rdi161) = edx159;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi161) + 4) = 0;
                            ++edx159;
                            *reinterpret_cast<uint32_t*>(&rdi162) = rdi161->f10340;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi162) + 4) = 0;
                            rsi160 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi160) + reinterpret_cast<uint64_t>(rdi162));
                        } while (edx159 != ecx158);
                        v156 = r8_119;
                        factor_insert_multiplicity(r9_120, rsi160, 1);
                        rsp124 = rsp124 - 8 + 8;
                        r9_120 = r9_120;
                        r8_119 = v156;
                        ecx158 = ecx158;
                        rdx163 = reinterpret_cast<void**>(rbx130->f40 * reinterpret_cast<unsigned char>(rbp122));
                        if (reinterpret_cast<unsigned char>(rdx163) > reinterpret_cast<unsigned char>(rbx130->f48)) 
                            break;
                        rbp122 = rdx163;
                    }
                }
            } else {
                goto addr_5198_97;
            }
            rdx164 = reinterpret_cast<void**>(rbx130->f50 * reinterpret_cast<unsigned char>(rbp122));
            if (reinterpret_cast<unsigned char>(rdx164) <= reinterpret_cast<unsigned char>(rbx130->f58)) {
                rbp122 = rdx164;
                ecx165 = static_cast<uint32_t>(r13_126 + 6);
                while (1) {
                    edx166 = *reinterpret_cast<uint32_t*>(&r13_126);
                    rsi167 = r12_125;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdi168) = edx166;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi168) + 4) = 0;
                        ++edx166;
                        *reinterpret_cast<uint32_t*>(&rdi169) = rdi168->f10340;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi169) + 4) = 0;
                        rsi167 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi167) + reinterpret_cast<uint64_t>(rdi169));
                    } while (edx166 != ecx165);
                    v156 = r8_119;
                    factor_insert_multiplicity(r9_120, rsi167, 1);
                    rsp124 = rsp124 - 8 + 8;
                    r9_120 = r9_120;
                    r8_119 = v156;
                    ecx165 = ecx165;
                    rdx170 = reinterpret_cast<void**>(rbx130->f50 * reinterpret_cast<unsigned char>(rbp122));
                    if (reinterpret_cast<unsigned char>(rdx170) > reinterpret_cast<unsigned char>(rbx130->f58)) 
                        break;
                    rbp122 = rdx170;
                }
            }
            rdx171 = reinterpret_cast<void**>(rbx130->f60 * reinterpret_cast<unsigned char>(rbp122));
            if (reinterpret_cast<unsigned char>(rdx171) <= reinterpret_cast<unsigned char>(rbx130->f68)) {
                rbp122 = rdx171;
                ecx172 = static_cast<uint32_t>(r13_126 + 7);
                while (1) {
                    edx173 = *reinterpret_cast<uint32_t*>(&r13_126);
                    rsi174 = r12_125;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdi175) = edx173;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi175) + 4) = 0;
                        ++edx173;
                        *reinterpret_cast<uint32_t*>(&rdi176) = rdi175->f10340;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi176) + 4) = 0;
                        rsi174 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi174) + reinterpret_cast<uint64_t>(rdi176));
                    } while (edx173 != ecx172);
                    v156 = r8_119;
                    factor_insert_multiplicity(r9_120, rsi174, 1);
                    rsp124 = rsp124 - 8 + 8;
                    r9_120 = r9_120;
                    r8_119 = v156;
                    ecx172 = ecx172;
                    rdx177 = reinterpret_cast<void**>(rbx130->f60 * reinterpret_cast<unsigned char>(rbp122));
                    if (reinterpret_cast<unsigned char>(rdx177) > reinterpret_cast<unsigned char>(rbx130->f68)) 
                        break;
                    rbp122 = rdx177;
                }
            }
            *reinterpret_cast<int32_t*>(&rax178) = *reinterpret_cast<int32_t*>(&r15_131);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax178) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rax179) = *reinterpret_cast<unsigned char*>(0x10080 + rax178);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax179) + 4) = 0;
            r12_125 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_125) + reinterpret_cast<uint64_t>(rax179));
            if (reinterpret_cast<unsigned char>(rbp122) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r12_125) * reinterpret_cast<unsigned char>(r12_125))) 
                break;
            *reinterpret_cast<uint32_t*>(&rax180) = static_cast<uint32_t>(r13_126 + 7);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax180) + 4) = 0;
            rbx130 = reinterpret_cast<struct s79*>(reinterpret_cast<uint64_t>(rbx130) + 0x80);
            *reinterpret_cast<uint32_t*>(&r13_126) = *reinterpret_cast<uint32_t*>(&r13_126) + 8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_126) + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&rax180) > 0x29b) 
                break;
            rax181 = reinterpret_cast<struct s87*>((rax180 << 4) + reinterpret_cast<unsigned char>(0xd640));
            r10_128 = rax181->f0;
            r11_127 = rax181->f8;
            continue;
            addr_5198_97:
            rbp122 = rdx155;
            ecx182 = static_cast<uint32_t>(r13_126 + 4);
            while (1) {
                edx183 = *reinterpret_cast<uint32_t*>(&r13_126);
                rsi184 = r12_125;
                do {
                    *reinterpret_cast<uint32_t*>(&rdi185) = edx183;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi185) + 4) = 0;
                    ++edx183;
                    *reinterpret_cast<uint32_t*>(&rdi186) = rdi185->f10340;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi186) + 4) = 0;
                    rsi184 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi184) + reinterpret_cast<uint64_t>(rdi186));
                } while (ecx182 != edx183);
                factor_insert_multiplicity(r9_120, rsi184, 1);
                rsp124 = rsp124 - 8 + 8;
                r9_120 = r9_120;
                r8_119 = r8_119;
                ecx182 = ecx182;
                rdx187 = reinterpret_cast<void**>(rbx130->f30 * reinterpret_cast<unsigned char>(rbp122));
                if (reinterpret_cast<unsigned char>(rdx187) > reinterpret_cast<unsigned char>(rbx130->f38)) 
                    goto addr_4f5f_98;
                rbp122 = rdx187;
            }
            addr_50b0_91:
            *reinterpret_cast<int32_t*>(&rcx188) = static_cast<int32_t>(r13_126 + 1);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx188) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rsi189) = *reinterpret_cast<uint32_t*>(&r13_126);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi189) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx190) = rcx188->f10340;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx190) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rsi191) = rsi189->f10340;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi191) + 4) = 0;
            rcx192 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx190) + reinterpret_cast<int64_t>(rsi191) + reinterpret_cast<unsigned char>(r12_125));
            do {
                rbp122 = rdx146;
                factor_insert_multiplicity(r9_120, rcx192, 1);
                rsp124 = rsp124 - 8 + 8;
                r9_120 = r9_120;
                rcx192 = rcx192;
                r8_119 = r8_119;
                rdx146 = reinterpret_cast<void**>(rbx130->f10 * reinterpret_cast<unsigned char>(rbp122));
            } while (reinterpret_cast<unsigned char>(rdx146) <= reinterpret_cast<unsigned char>(rbx130->f18));
            rdx147 = reinterpret_cast<void**>(rbx130->f20 * reinterpret_cast<unsigned char>(rbp122));
            if (reinterpret_cast<unsigned char>(rdx147) > reinterpret_cast<unsigned char>(rbx130->f28)) 
                goto addr_4f4d_96;
            goto addr_5120_95;
        }
        addr_5200_78:
        if (!r8_119) 
            goto addr_5209_130;
        rdi193 = r8_119;
        v194 = r9_120;
        v195 = r8_119;
        al196 = prime2_p(rdi193, rbp122);
        rsp124 = rsp124 - 8 + 8;
        if (al196) 
            goto addr_53a9_132;
        rbx31 = v123;
        r13_30 = v121;
        continue;
        addr_4edd_79:
        *reinterpret_cast<uint32_t*>(&rax197) = *reinterpret_cast<uint32_t*>(&r13_126);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax197) + 4) = 0;
        rax198 = reinterpret_cast<struct s91*>((rax197 << 4) + 0xd640);
        r10_128 = rax198->f0;
        r11_127 = rax198->f8;
        goto addr_4ef9_83;
        addr_4c8b_52:
        if (*reinterpret_cast<void***>(v9 + 8)) 
            goto addr_4cfe_62; else 
            goto addr_4c97_134;
    }
    addr_4cba_18:
    fun_2810("b0 & 1", "src/factor.c", 0x1e0, "gcd2_odd", r8_53, r9_36);
    do {
        fun_2740();
        addr_4ce0_56:
        factor_using_pollard_rho2(rbx31, r13_30, v8 + 1, v9);
        addr_4ad5_10:
        rax199 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
    } while (rax199);
    return;
    addr_4aa0_50:
    if (reinterpret_cast<unsigned char>(rbp6) <= reinterpret_cast<unsigned char>(1)) {
        addr_4c74_137:
        factor_using_pollard_rho(rbp6, v8, v9, rcx87);
        goto addr_4ad5_10;
    } else {
        addr_4aaa_138:
        if (reinterpret_cast<unsigned char>(rbp6) <= reinterpret_cast<unsigned char>(0x17ded78) || (al200 = prime_p_part_0(rbp6, rsi105, rdx104, rcx87), !!al200)) {
            factor_insert_multiplicity(v9, rbp6, 1, v9, rbp6, 1);
            goto addr_4ad5_10;
        }
    }
    addr_4c4f_57:
    rcx87 = v9;
    rsi105 = v32;
    rdx104 = v8 + 1;
    factor_using_pollard_rho2(v61, rsi105, rdx104, rcx87);
    if (reinterpret_cast<unsigned char>(rbp6) > reinterpret_cast<unsigned char>(1)) 
        goto addr_4aaa_138; else 
        goto addr_4c74_137;
    addr_4ca3_59:
    *reinterpret_cast<int32_t*>(&rdx104) = 1;
    *reinterpret_cast<int32_t*>(&rdx104 + 4) = 0;
    rsi105 = v32;
    factor_insert_multiplicity(v9, rsi105, 1);
    goto addr_4aa0_50;
    addr_4a94_61:
    *reinterpret_cast<void***>(v9) = v32;
    *reinterpret_cast<void***>(v9 + 8) = v61;
    goto addr_4aa0_50;
    addr_5209_130:
    if (reinterpret_cast<unsigned char>(rbp122) <= reinterpret_cast<unsigned char>(1)) {
        addr_4d41_66:
        goto v32;
    } else {
        al201 = prime2_p(0, rbp122);
        r9_202 = r9_120;
        if (al201) {
            rsi203 = rbp122;
            rdi204 = r9_202;
        } else {
            rdi205 = rbp122;
            rdx206 = r9_202;
            *reinterpret_cast<int32_t*>(&rsi207) = 1;
            *reinterpret_cast<int32_t*>(&rsi207 + 4) = 0;
            goto addr_5740_143;
        }
    }
    addr_2f20_144:
    *reinterpret_cast<uint32_t*>(&rax208) = *reinterpret_cast<unsigned char*>(rdi204 + 0xfa);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax208) + 4) = 0;
    r8_209 = rsi203;
    r11_210 = reinterpret_cast<void***>(rdi204 + 16);
    r10_211 = reinterpret_cast<signed char*>(rdi204 + 0xe0);
    r9d212 = *reinterpret_cast<uint32_t*>(&rax208);
    if (!*reinterpret_cast<uint32_t*>(&rax208)) 
        goto addr_2fa1_145;
    ebx213 = static_cast<int32_t>(rax208 - 1);
    rcx214 = reinterpret_cast<void*>(static_cast<int64_t>(ebx213));
    rax215 = rcx214;
    do {
        *reinterpret_cast<int32_t*>(&rsi216) = *reinterpret_cast<int32_t*>(&rax215);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi216) + 4) = 0;
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi204 + reinterpret_cast<uint64_t>(rax215) * 8) + 16)) <= reinterpret_cast<unsigned char>(r8_209)) 
            break;
        rax215 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax215) - 1);
        *reinterpret_cast<int32_t*>(&rsi216) = *reinterpret_cast<int32_t*>(&rax215);
    } while (*reinterpret_cast<int32_t*>(&rax215) != -1);
    goto addr_2f80_149;
    if (*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi204 + reinterpret_cast<uint64_t>(rax215) * 8) + 16) == r8_209) {
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_211) + reinterpret_cast<uint64_t>(rax215)) = reinterpret_cast<signed char>(*reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_211) + reinterpret_cast<uint64_t>(rax215)) + 1);
        goto v32;
    }
    rax217 = reinterpret_cast<void*>(static_cast<int64_t>(static_cast<int32_t>(rsi216 + 1)));
    r11_210 = r11_210 + reinterpret_cast<uint64_t>(rax217) * 8;
    r10_211 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_211) + reinterpret_cast<uint64_t>(rax217));
    if (*reinterpret_cast<int32_t*>(&rsi216) >= ebx213) {
        addr_2fa1_145:
        r9d218 = r9d212 + 1;
        *r11_210 = r8_209;
        *r10_211 = 1;
        *reinterpret_cast<unsigned char*>(rdi204 + 0xfa) = *reinterpret_cast<unsigned char*>(&r9d218);
        goto v32;
    }
    do {
        addr_2f80_149:
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi204 + reinterpret_cast<uint64_t>(rcx214) * 8) + 24) = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi204 + reinterpret_cast<uint64_t>(rcx214) * 8) + 16);
        eax219 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi204) + reinterpret_cast<uint64_t>(rcx214) + 0xe0);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi204) + reinterpret_cast<uint64_t>(rcx214) + 0xe1) = *reinterpret_cast<signed char*>(&eax219);
        rcx214 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx214) - 1);
    } while (*reinterpret_cast<int32_t*>(&rsi216) < *reinterpret_cast<int32_t*>(&rcx214));
    goto addr_2fa1_145;
    addr_5740_143:
    v194 = rsi207;
    v156 = rdx206;
    if (reinterpret_cast<unsigned char>(rdi205) <= reinterpret_cast<unsigned char>(1)) {
        addr_5b07_154:
        fun_2810("(1) < (n)", "src/factor.c", 0x5be, "factor_using_pollard_rho", r8_119, r9_202);
    } else {
        r12_220 = rdi205;
        do {
            rcx221 = r12_220;
            esi222 = 64;
            *reinterpret_cast<int32_t*>(&rax223) = 1;
            *reinterpret_cast<int32_t*>(&rax223 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx224) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx224) + 4) = 0;
            rdi225 = reinterpret_cast<void*>(0);
            do {
                r8_119 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx221) << 63);
                rcx221 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx221) >> 1);
                rdx224 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx224) >> 1 | reinterpret_cast<unsigned char>(r8_119));
                if (reinterpret_cast<unsigned char>(rcx221) < reinterpret_cast<unsigned char>(rax223) || rcx221 == rax223 && reinterpret_cast<uint64_t>(rdx224) <= reinterpret_cast<uint64_t>(rdi225)) {
                    cf226 = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi225) < reinterpret_cast<uint64_t>(rdx224));
                    rdi225 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi225) - reinterpret_cast<uint64_t>(rdx224));
                    rax223 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax223) - (reinterpret_cast<unsigned char>(rcx221) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax223) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx221) + static_cast<uint64_t>(cf226))))));
                }
                --esi222;
            } while (esi222);
            *reinterpret_cast<int32_t*>(&rbp227) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp227) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r15_228) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_228) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r14_229) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_229) + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rbp227) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_220) - reinterpret_cast<uint64_t>(rdi225) > reinterpret_cast<uint64_t>(rdi225));
            rbp230 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbp227) & reinterpret_cast<unsigned char>(r12_220)) + (reinterpret_cast<uint64_t>(rdi225) + reinterpret_cast<uint64_t>(rdi225) - reinterpret_cast<unsigned char>(r12_220)));
            rcx231 = rbp230;
            while (reinterpret_cast<unsigned char>(v194) < reinterpret_cast<unsigned char>(r12_220)) {
                r11_232 = rcx231;
                rax233 = reinterpret_cast<unsigned char>(r12_220) >> 1;
                *reinterpret_cast<uint32_t*>(&rax234) = *reinterpret_cast<uint32_t*>(&rax233) & 0x7f;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax234) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax235) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax234);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax235) + 4) = 0;
                rdx236 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax235) + reinterpret_cast<int64_t>(rax235) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax235) * reinterpret_cast<int64_t>(rax235) * reinterpret_cast<unsigned char>(r12_220)));
                rax237 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx236) + reinterpret_cast<uint64_t>(rdx236) - reinterpret_cast<uint64_t>(rdx236) * reinterpret_cast<uint64_t>(rdx236) * reinterpret_cast<unsigned char>(r12_220));
                r8_238 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax237) + reinterpret_cast<uint64_t>(rax237) - reinterpret_cast<uint64_t>(rax237) * reinterpret_cast<uint64_t>(rax237) * reinterpret_cast<unsigned char>(r12_220));
                r10_239 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_220) - reinterpret_cast<unsigned char>(v194));
                r9_240 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v194) - reinterpret_cast<unsigned char>(r12_220));
                while (1) {
                    rax241 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rax241 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax241) + reinterpret_cast<unsigned char>(r12_220));
                    }
                    rbp230 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rbp230) - (reinterpret_cast<unsigned char>(rbp230) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp230) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp230) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax241) < reinterpret_cast<uint64_t>(r10_239))))))) & reinterpret_cast<unsigned char>(r12_220)) + (reinterpret_cast<uint64_t>(rax241) + reinterpret_cast<uint64_t>(r9_240)));
                    r13_242 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        r13_242 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_242) + reinterpret_cast<unsigned char>(r12_220));
                    }
                    rax243 = r14_229;
                    *reinterpret_cast<uint32_t*>(&rax244) = *reinterpret_cast<uint32_t*>(&rax243) & 31;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax244) + 4) = 0;
                    if (rax244 == 1) {
                        rdi245 = r13_242;
                        rax246 = gcd_odd(rdi245, r12_220);
                        if (!reinterpret_cast<int1_t>(rax246 == 1)) 
                            break;
                    }
                    --r14_229;
                    if (r14_229) 
                        continue;
                    rcx247 = r15_228 + r15_228;
                    if (!r15_228) {
                        r15_228 = rcx247;
                        r11_232 = rbp230;
                    } else {
                        do {
                            rdi248 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                            rax249 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi248) + reinterpret_cast<unsigned char>(r12_220));
                            if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                                rdi248 = rax249;
                            }
                            ++r14_229;
                        } while (r15_228 != r14_229);
                        r11_232 = rbp230;
                        r15_228 = rcx247;
                        rbp230 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax249) - (reinterpret_cast<uint64_t>(rax249) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax249) < reinterpret_cast<uint64_t>(rax249) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi248) < reinterpret_cast<uint64_t>(r10_239)))) & reinterpret_cast<unsigned char>(r12_220)) + (reinterpret_cast<uint64_t>(rdi248) + reinterpret_cast<uint64_t>(r9_240)));
                    }
                }
                r9_202 = r8_238;
                r8_119 = r11_232;
                r11_250 = r9_240;
                do {
                    rsi251 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    rax252 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi251) + reinterpret_cast<unsigned char>(r12_220));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rsi251 = rax252;
                    }
                    rbx253 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax252) - (reinterpret_cast<uint64_t>(rax252) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax252) < reinterpret_cast<uint64_t>(rax252) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rsi251) < reinterpret_cast<uint64_t>(r10_239)))) & reinterpret_cast<unsigned char>(r12_220)) + (reinterpret_cast<uint64_t>(rsi251) + reinterpret_cast<uint64_t>(r11_250)));
                    rsi254 = r12_220;
                    rdi245 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rdi245) - (reinterpret_cast<unsigned char>(rdi245) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi245) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rdi245) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_119) < reinterpret_cast<unsigned char>(rbx253))))))) & reinterpret_cast<unsigned char>(r12_220)) + (reinterpret_cast<unsigned char>(r8_119) - reinterpret_cast<unsigned char>(rbx253)));
                    rax255 = gcd_odd(rdi245, rsi254);
                } while (rax255 == 1);
                rcx256 = r8_119;
                r11_257 = rax255;
                if (rax255 == r12_220) 
                    goto addr_5af7_183;
                rdx258 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_220) % reinterpret_cast<unsigned char>(r11_257));
                rax259 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_220) / reinterpret_cast<unsigned char>(r11_257));
                r8_260 = rax259;
                r12_220 = rax259;
                if (reinterpret_cast<unsigned char>(r11_257) > reinterpret_cast<unsigned char>(1) && (reinterpret_cast<unsigned char>(r11_257) <= reinterpret_cast<unsigned char>(0x17ded78) || (al261 = prime_p_part_0(r11_257, rsi254, rdx258, rcx256), r11_257 = r11_257, rcx256 = rcx256, r8_260 = rax259, !!al261))) {
                    *reinterpret_cast<int32_t*>(&rdx262) = 1;
                    *reinterpret_cast<int32_t*>(&rdx262 + 4) = 0;
                    rsi263 = r11_257;
                    factor_insert_multiplicity(v156, rsi263, 1);
                    r8_119 = r8_260;
                    rcx264 = rcx256;
                    zf265 = reinterpret_cast<int1_t>(r8_119 == 1);
                    if (reinterpret_cast<unsigned char>(r8_119) <= reinterpret_cast<unsigned char>(1)) 
                        goto addr_5aca_186; else 
                        goto addr_5a2f_187;
                }
                rdx262 = v156;
                rsi263 = v194 + 1;
                factor_using_pollard_rho(r11_257, rsi263, rdx262, rcx256);
                r8_119 = r8_260;
                rcx264 = rcx256;
                zf265 = reinterpret_cast<int1_t>(r8_119 == 1);
                if (reinterpret_cast<unsigned char>(r8_119) > reinterpret_cast<unsigned char>(1)) {
                    addr_5a2f_187:
                    if (reinterpret_cast<unsigned char>(r8_119) <= reinterpret_cast<unsigned char>(0x17ded78)) 
                        goto addr_5ad7_189;
                    al266 = prime_p_part_0(r8_119, rsi263, rdx262, rcx264);
                    r8_119 = r8_119;
                    if (al266) 
                        goto addr_5ad7_189;
                } else {
                    addr_5aca_186:
                    if (zf265) 
                        goto addr_5b45_191; else 
                        goto addr_5acc_192;
                }
                rbp230 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp230) % reinterpret_cast<unsigned char>(r8_119));
                rcx231 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx264) % reinterpret_cast<unsigned char>(r8_119));
                continue;
                addr_5acc_192:
                *reinterpret_cast<int32_t*>(&rcx231) = 0;
                *reinterpret_cast<int32_t*>(&rcx231 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rbp230) = 0;
                *reinterpret_cast<int32_t*>(&rbp230 + 4) = 0;
            }
            break;
            addr_5af7_183:
            ++v194;
        } while (reinterpret_cast<unsigned char>(r12_220) > reinterpret_cast<unsigned char>(1));
        goto addr_5b07_154;
    }
    fun_2810("a < n", "src/factor.c", 0x5c4, "factor_using_pollard_rho", r8_119, r9_202);
    addr_5b45_191:
    goto v32;
    addr_5ad7_189:
    rdi204 = v156;
    rsi203 = r8_119;
    goto addr_2f20_144;
    addr_53a9_132:
    if (!*reinterpret_cast<void***>(v194 + 8)) {
        *reinterpret_cast<void***>(v194) = rbp122;
        *reinterpret_cast<void***>(v194 + 8) = v195;
        goto addr_4d41_66;
    }
    factor_insert_large_part_0();
    r14_267 = reinterpret_cast<struct s0*>(rdi193 + 0xffffffffffffffff);
    rbp268 = rdi193;
    rsp269 = reinterpret_cast<struct s0**>(rsp124 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x148);
    rax270 = g28;
    v271 = rax270;
    eax272 = 0;
    v273 = r14_267;
    if (*reinterpret_cast<uint32_t*>(&rdi193) & 1) 
        goto addr_5478_198;
    v274 = 0;
    r14_267 = v273;
    addr_5490_200:
    rcx275 = rbp268;
    *reinterpret_cast<int32_t*>(&r15_276) = 0;
    *reinterpret_cast<int32_t*>(&r15_276 + 4) = 0;
    rax277 = reinterpret_cast<unsigned char>(rbp268) >> 1;
    esi278 = 64;
    *reinterpret_cast<uint32_t*>(&rax279) = *reinterpret_cast<uint32_t*>(&rax277) & 0x7f;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax279) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax280) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax279);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax280) + 4) = 0;
    rdx281 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax280) + reinterpret_cast<int64_t>(rax280) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax280) * reinterpret_cast<int64_t>(rax280) * reinterpret_cast<unsigned char>(rbp268)));
    rax282 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx281) + reinterpret_cast<uint64_t>(rdx281) - reinterpret_cast<uint64_t>(rdx281) * reinterpret_cast<uint64_t>(rdx281) * reinterpret_cast<unsigned char>(rbp268));
    *reinterpret_cast<int32_t*>(&rdx283) = 0;
    *reinterpret_cast<int32_t*>(&rdx283 + 4) = 0;
    r13_284 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax282) + reinterpret_cast<uint64_t>(rax282) - reinterpret_cast<uint64_t>(rax282) * reinterpret_cast<uint64_t>(rax282) * reinterpret_cast<unsigned char>(rbp268));
    *reinterpret_cast<int32_t*>(&rax285) = 1;
    *reinterpret_cast<int32_t*>(&rax285 + 4) = 0;
    do {
        rdi286 = reinterpret_cast<unsigned char>(rcx275) << 63;
        rcx275 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx275) >> 1);
        rdx283 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx283) >> 1) | rdi286);
        if (reinterpret_cast<unsigned char>(rcx275) < reinterpret_cast<unsigned char>(rax285) || rcx275 == rax285 && reinterpret_cast<unsigned char>(rdx283) <= reinterpret_cast<unsigned char>(r15_276)) {
            cf287 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_276) < reinterpret_cast<unsigned char>(rdx283));
            r15_276 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_276) - reinterpret_cast<unsigned char>(rdx283));
            rax285 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax285) - (reinterpret_cast<unsigned char>(rcx275) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax285) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx275) + static_cast<uint64_t>(cf287))))));
        }
        --esi278;
    } while (esi278);
    *reinterpret_cast<int32_t*>(&rbx288) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx288) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&r8_119) = v274;
    *reinterpret_cast<int32_t*>(&r8_119 + 4) = 0;
    r9_202 = r15_276;
    *reinterpret_cast<unsigned char*>(&rbx288) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp268) - reinterpret_cast<unsigned char>(r15_276)) > reinterpret_cast<unsigned char>(r15_276));
    rbx289 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbx288) & reinterpret_cast<unsigned char>(rbp268)) + (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r15_276) + reinterpret_cast<unsigned char>(r15_276)) - reinterpret_cast<unsigned char>(rbp268)));
    al290 = millerrabin(rbp268, r13_284, rbx289, r14_267, *reinterpret_cast<uint32_t*>(&r8_119), r9_202);
    if (al290) {
        v291 = reinterpret_cast<void*>(rsp269 - 1 + 1 + 6);
        factor();
        v292 = r14_267;
        r14_267 = r13_284;
        eax293 = v294;
        r13_284 = reinterpret_cast<struct s0*>(0x10340);
        v295 = eax293;
        *reinterpret_cast<uint32_t*>(&rax296) = eax293 - 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax296) + 4) = 0;
        v297 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v291) + rax296 * 8);
        r12_125 = reinterpret_cast<void**>(2);
        rax298 = rbx289;
        rbx289 = r15_276;
        r15_276 = rax298;
        goto addr_55b0_206;
    }
    addr_56fc_207:
    addr_5690_208:
    rax299 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v271) - reinterpret_cast<unsigned char>(g28));
    if (rax299) {
        fun_2740();
    } else {
        goto v195;
    }
    addr_5719_211:
    *reinterpret_cast<int32_t*>(&rdx206) = 0x4f2;
    *reinterpret_cast<int32_t*>(&rdx206 + 4) = 0;
    rsi207 = reinterpret_cast<void**>("src/factor.c");
    rdi205 = reinterpret_cast<void**>("(s1) < (n)");
    fun_2810("(s1) < (n)", "src/factor.c", 0x4f2, "prime_p", r8_119, r9_202);
    goto addr_5740_143;
    addr_5478_198:
    do {
        r14_267 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(r14_267) >> 1);
        ++eax272;
    } while (!(*reinterpret_cast<unsigned char*>(&r14_267) & 1));
    v274 = eax272;
    goto addr_5490_200;
    addr_4c97_134:
    *reinterpret_cast<void***>(v9) = rbp6;
    *reinterpret_cast<void***>(v9 + 8) = r13_30;
    goto addr_4ad5_10;
    addr_5689_214:
    goto addr_5690_208;
    addr_5680_215:
    while (rax300 == rbx289) {
        while (1) {
            *reinterpret_cast<uint32_t*>(&rax301) = r13_284->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax301) + 4) = 0;
            r12_125 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_125) + reinterpret_cast<uint64_t>(rax301));
            rax302 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx289) * reinterpret_cast<unsigned char>(r12_125));
            rdx303 = __intrinsic();
            r15_276 = rax302;
            if (rdx303) {
                if (reinterpret_cast<unsigned char>(rbp268) <= reinterpret_cast<unsigned char>(rdx303)) 
                    goto addr_5719_211;
                rcx304 = rbp268;
                esi305 = 64;
                *reinterpret_cast<int32_t*>(&rax306) = 0;
                *reinterpret_cast<int32_t*>(&rax306 + 4) = 0;
                do {
                    rdi307 = reinterpret_cast<unsigned char>(rcx304) << 63;
                    rcx304 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx304) >> 1);
                    rax306 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax306) >> 1) | rdi307);
                    if (reinterpret_cast<unsigned char>(rcx304) < reinterpret_cast<unsigned char>(rdx303) || rcx304 == rdx303 && reinterpret_cast<unsigned char>(rax306) <= reinterpret_cast<unsigned char>(r15_276)) {
                        cf308 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_276) < reinterpret_cast<unsigned char>(rax306));
                        r15_276 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_276) - reinterpret_cast<unsigned char>(rax306));
                        rdx303 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx303) - (reinterpret_cast<unsigned char>(rcx304) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx303) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx304) + static_cast<uint64_t>(cf308))))));
                    }
                    --esi305;
                } while (esi305);
            } else {
                r15_276 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax302) % reinterpret_cast<unsigned char>(rbp268));
            }
            *reinterpret_cast<uint32_t*>(&r8_119) = v274;
            *reinterpret_cast<int32_t*>(&r8_119 + 4) = 0;
            r9_202 = rbx289;
            al309 = millerrabin(rbp268, r14_267, r15_276, v292, *reinterpret_cast<uint32_t*>(&r8_119), r9_202);
            if (!al309) 
                goto addr_56fc_207;
            r13_284 = reinterpret_cast<struct s0*>(&r13_284->f1);
            if (reinterpret_cast<int1_t>(0x105dc == r13_284)) 
                break;
            addr_55b0_206:
            if (!v295) 
                goto addr_5690_208;
            r11_310 = v291;
            do {
                r8_119 = rbx289;
                rax300 = powm(r15_276, reinterpret_cast<uint64_t>(v273) / v311, rbp268, r14_267, r8_119, r9_202);
                if (v297 == r11_310) 
                    goto addr_5680_215;
                r11_310 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r11_310) + 8);
            } while (rax300 != rbx289);
        }
        fun_2700();
        fun_2a20();
        rax300 = fun_25f0();
    }
    goto addr_5689_214;
}