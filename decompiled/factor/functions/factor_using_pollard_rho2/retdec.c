void factor_using_pollard_rho2(uint64_t n1, uint64_t n0, int64_t a, int32_t * factors) {
    // 0x45c0
    __readfsqword(40);
    int64_t v1 = n1 >= 2; // 0x460f
    int64_t v2 = n1 < 2 ? 127 : 63; // 0x460f
    int64_t v3 = n1 < 2; // 0x460f
    int64_t v4; // 0x45c0
    int64_t v5; // 0x45c0
    int64_t v6; // 0x45c0
    while (true) {
      lab_0x4610:
        // 0x4610
        v5 = v2;
        uint64_t v7 = 2 * v3; // 0x4616
        int64_t v8 = v3 >> 63 | 2 * v1; // 0x461d
        if (v8 > n1) {
            // 0x462c
            v4 = v8 - n1 + (int64_t)(v7 < n0);
            v6 = v7 - n0;
            goto lab_0x4632;
        } else {
            // 0x4625
            if (v7 < n0 || v8 != n1) {
                goto lab_0x4632;
            } else {
                // 0x462c
                v4 = v8 - n1 + (int64_t)(v7 < n0);
                v6 = v7 - n0;
                goto lab_0x4632;
            }
        }
    }
  lab_0x4638:;
    // 0x4638
    int64_t v9; // 0x45c0
    uint64_t v10 = 2 * v9; // 0x4645
    int64_t v11; // 0x45c0
    int64_t v12 = 2 * v11 | (int64_t)(v10 < v9); // 0x4648
    int64_t v13; // 0x45c0
    int64_t v14; // 0x45c0
    if (v12 > n1) {
        // 0x4662
        v13 = (int64_t)(v10 < n0) - n1 + v12;
        v14 = v10 - n0;
        goto lab_0x4673;
    } else {
        // 0x465b
        v13 = v12;
        v14 = v10;
        if (v10 < n0 || v12 != n1) {
            goto lab_0x4673;
        } else {
            // 0x4662
            v13 = (int64_t)(v10 < n0) - n1 + v12;
            v14 = v10 - n0;
            goto lab_0x4673;
        }
    }
  lab_0x4a05_2:;
    // 0x4a05
    int64_t * v15; // 0x45c0
    int64_t * v16 = v15; // 0x4a0e
    int64_t * v17; // 0x45c0
    int64_t * v18 = v17; // 0x4a0e
    int64_t v19; // 0x45c0
    int64_t v20 = v19; // 0x4a0e
    int64_t v21; // 0x45c0
    int64_t v22 = v21; // 0x4a0e
    int64_t v23; // 0x45c0
    int64_t v24; // 0x45c0
    int64_t v25; // 0x45c0
    int64_t v26; // 0x45c0
    int64_t v27; // 0x45c0
    int64_t v28; // bp-184, 0x45c0
    int64_t * v29; // 0x45c0
    int64_t v30; // 0x4a61
    if (v24 == v21 == v19 == v27) {
        goto lab_0x4ce0;
    } else {
        // 0x4a19
        v28 = v24;
        unsigned char v31 = *(char *)(v24 / 2 % 128 + (int64_t)"\x01\xab\xcd\xb7\x39\xa3\xc5\xef\xf1\x1b=\xa7)\x13\x35\xdf\xe1\x8b\xad\x97\x19\x83\xa5\xcf\xd1\xfb\x1d\x87\t\xf3\x15\xbf\xc1k\x8dw\xf9\x63\x85\xaf\xb1\xdb\xfdg\xe9\xd3\xf5\x9f\xa1KmW\xd9\x43\x65\x8f\x91\xbb\xddG\xc9\xb3\xd5\x7f\x81+M7\xb9#Eoq\x9b\xbd'\xa9\x93\xb5_a\v-\x17\x99\x03%OQ{\x9d\a\x89s\x95?A\xeb\r\xf7y\xe3\x05/1[}\xe7iSu\x1f!\xcb\xed\xd7Y\xc3\xe5\x0f\x11;]\xc7I3U\xff"); // 0x4a30
        int64_t v32 = v31; // 0x4a30
        int64_t v33 = (2 - v24 * v32) * v32; // 0x4a40
        int64_t v34 = (2 - v33 * v24) * v33; // 0x4a4f
        v30 = v34 * v21 * (2 - v34 * v24);
        if (!prime2_p(v27, v24)) {
            // 0x4c4f
            factor_using_pollard_rho2(*v29, v28, *v17 + 1, (int32_t *)*v15);
            v26 = (int64_t)"\x01\xab\xcd\xb7\x39\xa3\xc5\xef\xf1\x1b=\xa7)\x13\x35\xdf\xe1\x8b\xad\x97\x19\x83\xa5\xcf\xd1\xfb\x1d\x87\t\xf3\x15\xbf\xc1k\x8dw\xf9\x63\x85\xaf\xb1\xdb\xfdg\xe9\xd3\xf5\x9f\xa1KmW\xd9\x43\x65\x8f\x91\xbb\xddG\xc9\xb3\xd5\x7f\x81+M7\xb9#Eoq\x9b\xbd'\xa9\x93\xb5_a\v-\x17\x99\x03%OQ{\x9d\a\x89s\x95?A\xeb\r\xf7y\xe3\x05/1[}\xe7iSu\x1f!\xcb\xed\xd7Y\xc3\xe5\x0f\x11;]\xc7I3U\xff";
            v25 = v30;
            v23 = v21;
            if (v30 < 2) {
                goto lab_0x4c74;
            } else {
                goto lab_0x4aaa;
            }
        } else {
            int64_t v35 = *v29; // 0x4a76
            int64_t v36 = *v15;
            if (v35 == 0) {
                // 0x4ca3
                factor_insert_multiplicity((int32_t *)v36, v28, 1);
                goto lab_0x4aa0;
            } else {
                int64_t * v37 = (int64_t *)(v36 + 8); // 0x4a89
                if (*v37 != 0) {
                    // 0x4cfe
                    factor_insert_large_part_0();
                    return;
                }
                // 0x4a94
                *(int64_t *)v36 = v28;
                *v37 = v35;
                goto lab_0x4aa0;
            }
        }
    }
  lab_0x4aa0:;
    int64_t v38 = (int64_t)"\x01\xab\xcd\xb7\x39\xa3\xc5\xef\xf1\x1b=\xa7)\x13\x35\xdf\xe1\x8b\xad\x97\x19\x83\xa5\xcf\xd1\xfb\x1d\x87\t\xf3\x15\xbf\xc1k\x8dw\xf9\x63\x85\xaf\xb1\xdb\xfdg\xe9\xd3\xf5\x9f\xa1KmW\xd9\x43\x65\x8f\x91\xbb\xddG\xc9\xb3\xd5\x7f\x81+M7\xb9#Eoq\x9b\xbd'\xa9\x93\xb5_a\v-\x17\x99\x03%OQ{\x9d\a\x89s\x95?A\xeb\r\xf7y\xe3\x05/1[}\xe7iSu\x1f!\xcb\xed\xd7Y\xc3\xe5\x0f\x11;]\xc7I3U\xff";
    v26 = v38;
    v25 = v30;
    v23 = v21;
    int64_t v39 = v38; // 0x4aa4
    int64_t v40 = v30; // 0x4aa4
    int64_t v41 = v21; // 0x4aa4
    if (v30 < 2) {
        goto lab_0x4c74;
    } else {
        goto lab_0x4aaa;
    }
  lab_0x49bb:;
    // 0x49bb
    uint64_t v42; // 0x4999
    int64_t v43; // 0x499e
    int64_t v44 = (int64_t)(v42 < v21) - v19 + v43; // 0x49be
    int64_t * v45; // 0x45c0
    *v45 = v44;
    int64_t v46 = v44; // 0x49c9
    int64_t v47 = v42 - v21; // 0x49c9
    goto lab_0x49cc;
  lab_0x49cc:;
    // 0x49cc
    uint64_t v48; // 0x493b
    int64_t v49 = v48 - v47; // 0x49d4
    int64_t * v50; // 0x45c0
    int64_t v51 = *v50 - v46 + (int64_t)(v48 < v47); // 0x49d7
    int64_t v52 = v49; // 0x49dd
    int64_t v53 = v51; // 0x49dd
    if (v51 < 0) {
        int64_t v54 = v49 + v21; // 0x49df
        v52 = v54;
        v53 = v51 + v19 + (int64_t)(v54 < v49);
    }
    int64_t v55 = v53;
    int64_t v56 = v52;
    int64_t v57; // 0x45c0
    int64_t v58; // 0x4945
    if ((v55 || v56) != 0) {
        int64_t v59 = gcd2_odd_part_0(v58, v55, v56, v19); // 0x4959
        int64_t v60 = *v29; // 0x495e
        v57 = v59;
        v24 = v59;
        v27 = v60;
        if (v60 != 0) {
            goto lab_0x4a05_2;
        }
    } else {
        // 0x49f1
        *v29 = v19;
        v57 = v21;
        v24 = v21;
        v27 = v19;
        if (v19 != 0) {
            goto lab_0x4a05_2;
        }
    }
    uint64_t v61 = v57;
    int64_t v62 = v46; // 0x4970
    int64_t v63 = v47; // 0x4970
    if (v61 != 1) {
        // break -> 0x4afb
        goto lab_0x4afb;
    }
    goto lab_0x4976;
  lab_0x485d:;
    // 0x485d
    int64_t v72; // 0x45c0
    uint64_t v73; // 0x483c
    int64_t v74; // 0x483f
    int64_t v75 = (int64_t)(v73 < v21) - v72 + v74; // 0x4860
    int64_t * v76; // 0x45c0
    *v76 = v75;
    int64_t v77 = v73 - v21; // 0x486b
    int64_t v78 = v75; // 0x486b
    goto lab_0x486e;
  lab_0x486e:;
    // 0x486e
    int64_t v79; // 0x45c0
    int64_t v80 = v79 + 1; // 0x486e
    int64_t v81 = v77; // 0x4876
    int64_t v82 = v78; // 0x4876
    v79 = v80;
    if (v28 == v80) {
        // break -> 0x4878
        goto lab_0x4878;
    }
    goto lab_0x4820;
  lab_0x4766:;
    // 0x4766
    uint64_t v86; // 0x473e
    int64_t v87; // 0x4743
    uint64_t v88; // 0x4755
    int64_t v89 = (int64_t)(v86 < v21) - v88 + v87; // 0x4769
    *v76 = v89;
    int64_t v90 = v89; // 0x4774
    int64_t v91 = v86 - v21; // 0x4774
    goto lab_0x4777;
  lab_0x4777:;
    int64_t v92 = v91;
    int64_t v93 = v90;
    int64_t * v94; // 0x45c0
    uint64_t v95 = *v94; // 0x477c
    int64_t v96 = v95 - v92; // 0x4781
    int64_t v97 = *v50 - v93 + (int64_t)(v95 < v92); // 0x4784
    int64_t v98 = v97; // 0x478a
    int64_t v99 = v96; // 0x478a
    if (v97 < 0) {
        int64_t v100 = v96 + v21; // 0x478c
        v98 = v97 + v88 + (int64_t)(v100 < v96);
        v99 = v100;
    }
    // 0x4792
    int64_t * v66; // 0x45c0
    int64_t * v67; // 0x470b
    *v66 = *v67;
    int64_t * v68; // 0x45c0
    *v68 = v21;
    int64_t * v101; // 0x45c0
    int64_t v102 = *v101; // 0x479a
    int64_t * v103; // 0x45c0
    int64_t v104 = *v103; // 0x479f
    int64_t * v105; // 0x45c0
    *v105 = v88;
    int64_t v70; // bp-72, 0x45c0
    int64_t v106 = mulredc2(&v70, v104, v102, v98, v99, v88, (int64_t)&g34, (int64_t)&g34); // 0x47a9
    *v101 = v106;
    int64_t * v71; // 0x45c0
    int64_t v107 = *v71; // 0x47b3
    *v103 = v107;
    int64_t v108 = *v105; // 0x47ca
    int64_t v109 = v108; // 0x47d3
    int64_t v110; // 0x45c0
    int64_t v111 = v110; // 0x47d3
    int64_t v112; // 0x45c0
    int64_t v113; // 0x45c0
    int64_t * v114; // 0x4710
    int64_t * v115; // 0x45c0
    if (v112 % 32 == 1) {
        // 0x48a8
        if (*v114 == 0) {
            // break (via goto) -> 0x4cba
            goto lab_0x4cba;
        }
        int64_t v116 = *v101; // 0x48b4
        int64_t v117; // 0x45c0
        int64_t v118; // 0x45c0
        if ((v116 || v107) == 0) {
            // 0x4908
            *v29 = v108;
            v118 = v21;
            v117 = 0;
            v113 = v108;
            if (v108 != 0) {
                // break -> 0x4920
                goto lab_0x4920_3;
            }
        } else {
            int64_t v119 = gcd2_odd_part_0(*v115, v107, v116, v108); // 0x48da
            int64_t v120 = *v105; // 0x48e4
            v118 = v119;
            v117 = v120;
            v113 = v120;
            if (*v29 != 0) {
                // break -> 0x4920
                goto lab_0x4920_3;
            }
        }
        int64_t v121 = v117;
        v113 = v121;
        if (v118 != 1) {
            // break -> 0x4920
            goto lab_0x4920_3;
        }
        // 0x48f4
        *v45 = v93;
        v109 = v121;
        v111 = v92;
    }
    // 0x47d9
    v72 = v109;
    int64_t v122 = v112 - 1; // 0x47d9
    int64_t v123 = v122; // 0x47dd
    int64_t v124 = v111; // 0x47dd
    if (v122 == 0) {
        // 0x47e3
        *v50 = v93;
        *v105 = 2 * v28;
        int64_t v125; // 0x45c0
        if (v28 == 0) {
            // 0x47e3
            v125 = v28;
        } else {
            // 0x4801
            *v94 = v92;
            v81 = v92;
            v82 = v93;
            v79 = v122;
            while (true) {
              lab_0x4820:;
                int64_t v83 = v82;
                int64_t v84 = v81;
                *v66 = *v67;
                *v68 = v21;
                uint64_t v85 = mulredc2(&v70, v83, v84, v83, v84, v72, (int64_t)&g34, (int64_t)&g34); // 0x482f
                v73 = v85 + *v17;
                v74 = *v71 + (int64_t)(v73 < v85);
                *v76 = v74;
                if (v74 > v72) {
                    goto lab_0x485d;
                } else {
                    // 0x4856
                    v77 = v73;
                    v78 = v74;
                    if (v73 < v21 || v74 != v72) {
                        goto lab_0x486e;
                    } else {
                        goto lab_0x485d;
                    }
                }
            }
          lab_0x4878:;
            // 0x4878
            int64_t v126; // 0x4872
            v125 = v126;
        }
        // 0x4886
        *v94 = v92;
        *v45 = v93;
        v28 = 0;
        v123 = v125;
        v124 = v92;
    }
    int64_t v127 = v93; // 0x45c0
    int64_t v128 = v72; // 0x45c0
    int64_t v129 = v92; // 0x45c0
    int64_t v130 = v123; // 0x45c0
    int64_t v131 = v124; // 0x45c0
    goto lab_0x4718;
  lab_0x4c2c:;
    // 0x4c2c
    int64_t v135; // 0x45c0
    factor_using_pollard_rho(v135, *v17 + 1, (int32_t *)*v15);
    goto lab_0x4bb1;
  lab_0x4bb1:;
    // 0x4bb1
    unsigned char v136; // 0x4b19
    int64_t v137 = v136; // 0x4b19
    int64_t v138 = (2 - v61 * v137) * v137; // 0x4b31
    int64_t v139 = (2 - v138 * v61) * v138; // 0x4b40
    goto lab_0x4aa0;
    int64_t v140; // 0x45c0
    int64_t v141; // 0x45c0
    int64_t v142; // 0x4b1d
    int64_t v143 = v142; // 0x4c27
    int64_t v144 = (2 - v139 * v61) * v139 * v21; // 0x4c27
    int64_t v145 = 0; // 0x4c27
    goto lab_0x46b8;
  lab_0x4b9f:;
    // 0x4b9f
    int64_t v150; // 0x4b92
    factor_insert_multiplicity((int32_t *)*v15, v150, 1);
    goto lab_0x4bb1;
  lab_0x4632:
    // 0x4632
    v9 = v6;
    v11 = v4;
    v1 = v11;
    v2 = v5 - 1;
    v3 = v9;
    if (v5 == 0) {
        // break -> 0x4638
        goto lab_0x4638;
    }
    goto lab_0x4610;
  lab_0x4673:;
    int64_t v152 = &v28; // 0x45d0
    int64_t * v153; // 0x45c0
    int64_t * v154; // 0x45c0
    int64_t v155; // 0x45c0
    int64_t v156; // 0x45c0
    if (n1 == 0 == n0 == 1) {
        goto lab_0x4ad5;
    } else {
        // 0x468c
        v28 = 1;
        v67 = (int64_t *)(v152 + 24);
        v114 = (int64_t *)(v152 + 56);
        v66 = (int64_t *)(v152 - 8);
        v68 = (int64_t *)(v152 - 16);
        v105 = (int64_t *)(v152 + 32);
        v71 = (int64_t *)(v152 + 112);
        v17 = (int64_t *)(v152 + 40);
        v76 = (int64_t *)(v152 + 80);
        v50 = (int64_t *)(v152 + 88);
        v94 = (int64_t *)(v152 + 48);
        v101 = (int64_t *)(v152 + 8);
        v103 = (int64_t *)(v152 + 16);
        v115 = (int64_t *)(v152 + 64);
        v29 = (int64_t *)(v152 + 104);
        v45 = (int64_t *)(v152 + 96);
        v15 = (int64_t *)(v152 + 72);
        v143 = 1;
        v144 = n0;
        v145 = n1;
        while (true) {
          lab_0x46b8:
            // 0x46b8
            v21 = v144;
            unsigned char v146 = *(char *)(v21 / 2 % 128 + (int64_t)"\x01\xab\xcd\xb7\x39\xa3\xc5\xef\xf1\x1b=\xa7)\x13\x35\xdf\xe1\x8b\xad\x97\x19\x83\xa5\xcf\xd1\xfb\x1d\x87\t\xf3\x15\xbf\xc1k\x8dw\xf9\x63\x85\xaf\xb1\xdb\xfdg\xe9\xd3\xf5\x9f\xa1KmW\xd9\x43\x65\x8f\x91\xbb\xddG\xc9\xb3\xd5\x7f\x81+M7\xb9#Eoq\x9b\xbd'\xa9\x93\xb5_a\v-\x17\x99\x03%OQ{\x9d\a\x89s\x95?A\xeb\r\xf7y\xe3\x05/1[}\xe7iSu\x1f!\xcb\xed\xd7Y\xc3\xe5\x0f\x11;]\xc7I3U\xff"); // 0x46d1
            int64_t v147 = v146; // 0x46d1
            int64_t v148 = (2 - v21 * v147) * v147; // 0x46e1
            int64_t v149 = (2 - v148 * v21) * v148; // 0x46f0
            *v67 = (2 - v149 * v21) * v149;
            *v114 = v21 % 2;
            v127 = v13;
            v128 = v145;
            v129 = v14;
            v130 = v143;
            v131 = v14;
            while (true) {
              lab_0x4718:
                // 0x4718
                v110 = v131;
                v112 = v130;
                int64_t v132 = v129;
                int64_t v133 = v127;
                *v66 = *v67;
                *v68 = v21;
                *v105 = v128;
                uint64_t v134 = mulredc2(&v70, v133, v132, v133, v132, v128, (int64_t)&g34, (int64_t)&g34); // 0x4731
                v86 = *v17 + v134;
                v87 = *v71 + (int64_t)(v86 < v134);
                *v76 = v87;
                v88 = *v105;
                if (v87 > v88) {
                    goto lab_0x4766;
                } else {
                    // 0x475f
                    v90 = v87;
                    v91 = v86;
                    if (v86 < v21 || v87 != v88) {
                        goto lab_0x4777;
                    } else {
                        goto lab_0x4766;
                    }
                }
            }
          lab_0x4920_3:
            // 0x4920
            v19 = v113;
            *v105 = v92;
            *v114 = v112;
            v48 = *v94;
            v58 = *v115;
            v62 = *v45;
            v63 = v110;
            while (true) {
              lab_0x4976:;
                int64_t v64 = v63;
                int64_t v65 = v62;
                *v66 = *v67;
                *v68 = v21;
                uint64_t v69 = mulredc2(&v70, v65, v64, v65, v64, v19, (int64_t)&g34, (int64_t)&g34); // 0x498c
                v42 = *v17 + v69;
                v43 = *v71 + (int64_t)(v42 < v69);
                *v45 = v43;
                if (v43 > v19) {
                    goto lab_0x49bb;
                } else {
                    // 0x49b4
                    v46 = v43;
                    v47 = v42;
                    if (v42 < v21 || v43 != v19) {
                        goto lab_0x49cc;
                    } else {
                        goto lab_0x49bb;
                    }
                }
            }
          lab_0x4afb:
            // 0x4afb
            v136 = *(char *)(v61 / 2 % 128 + (int64_t)"\x01\xab\xcd\xb7\x39\xa3\xc5\xef\xf1\x1b=\xa7)\x13\x35\xdf\xe1\x8b\xad\x97\x19\x83\xa5\xcf\xd1\xfb\x1d\x87\t\xf3\x15\xbf\xc1k\x8dw\xf9\x63\x85\xaf\xb1\xdb\xfdg\xe9\xd3\xf5\x9f\xa1KmW\xd9\x43\x65\x8f\x91\xbb\xddG\xc9\xb3\xd5\x7f\x81+M7\xb9#Eoq\x9b\xbd'\xa9\x93\xb5_a\v-\x17\x99\x03%OQ{\x9d\a\x89s\x95?A\xeb\r\xf7y\xe3\x05/1[}\xe7iSu\x1f!\xcb\xed\xd7Y\xc3\xe5\x0f\x11;]\xc7I3U\xff");
            v142 = *v114;
            // 0x4b72
            v135 = v61;
            if (v61 < 2) {
                goto lab_0x4c2c;
            } else {
                // 0x4b7c
                v150 = v61;
                if (v61 < 0x17ded79) {
                    goto lab_0x4b9f;
                } else {
                    // 0x4b85
                    *v67 = v61;
                    int64_t v151 = prime_p_part_0(v61); // 0x4b8d
                    v150 = *v67;
                    v135 = v150;
                    if ((char)v151 == 0) {
                        goto lab_0x4c2c;
                    } else {
                        goto lab_0x4b9f;
                    }
                }
            }
        }
      lab_0x4cba:
        // 0x4cba
        function_2810();
        v154 = v15;
        v153 = v17;
        v156 = v21;
        v155 = v112;
        goto lab_0x4cd9;
    }
  lab_0x4ad5:
    // 0x4ad5
    if (*(int64_t *)(v152 + 120) == __readfsqword(40)) {
        // 0x4ae9
        return;
    }
    // 0x4ad5
    v154 = (int64_t *)(v152 + 72);
    v153 = (int64_t *)(v152 + 40);
    v156 = v141;
    v155 = v140;
    goto lab_0x4cd9;
  lab_0x4cd9:
    // 0x4cd9
    function_2740();
    v16 = v154;
    v18 = v153;
    v20 = v156;
    v22 = v155;
    goto lab_0x4ce0;
  lab_0x4ce0:;
    int64_t v157 = *v16; // 0x4ce5
    factor_using_pollard_rho2(v20, v22, *v18 + 1, (int32_t *)v157);
    v141 = v20;
    v140 = v22;
    goto lab_0x4ad5;
  lab_0x4c74:
    // 0x4c74
    factor_using_pollard_rho(v40, *v17, (int32_t *)*v15);
    v141 = v39;
    v140 = v41;
    goto lab_0x4ad5;
  lab_0x4aaa:
    // 0x4aaa
    if (v25 < 0x17ded79) {
        goto lab_0x4ac3;
    } else {
        int64_t v158 = prime_p_part_0(v25); // 0x4ab6
        v39 = v26;
        v40 = v25;
        v41 = v23;
        if ((char)v158 == 0) {
            goto lab_0x4c74;
        } else {
            goto lab_0x4ac3;
        }
    }
  lab_0x4ac3:
    // 0x4ac3
    factor_insert_multiplicity((int32_t *)*v15, v25, 1);
    v141 = v26;
    v140 = v23;
    goto lab_0x4ad5;
}