bool print_factors(char * input) {
    int64_t v1 = (int64_t)input;
    int64_t v2 = __readfsqword(40); // 0x61e4
    char v3 = v1;
    int64_t v4 = v1; // 0x61fc
    char v5 = v3; // 0x61fc
    int64_t v6 = v1; // 0x61fc
    if (v3 == 32) {
        v4++;
        char v7 = *(char *)v4; // 0x6200
        v5 = v7;
        v6 = v4;
        while (v7 == 32) {
            // 0x6200
            v4++;
            v7 = *(char *)v4;
            v5 = v7;
            v6 = v4;
        }
    }
    int64_t v8 = v6 + (int64_t)(v5 == 43); // 0x621b
    char v9 = *(char *)v8; // 0x6228
    int64_t v10 = v8; // 0x6231
    char v11 = v9; // 0x6231
    int64_t v12; // 0x61d0
    char v13; // 0x61d0
    int64_t v14; // 0x61d0
    int64_t v15; // 0x61d0
    if (v9 != 0) {
        while ((int32_t)v11 < 58) {
            // 0x6228
            v10++;
            v11 = *(char *)v10;
            v12 = v8;
            v13 = v9;
            v14 = 0;
            v15 = 0;
            if (v11 == 0) {
                goto lab_0x62bf;
            }
        }
    }
    // 0x623e
    quote(input);
    function_2700();
    function_2a20();
    // 0x626c
    if (v2 != __readfsqword(40)) {
        // 0x6648
        return function_2740() % 2 != 0;
    }
    // 0x6283
    return false;
  lab_0x62bf:
    // 0x62bf
    if (v15 > 0x1999999999999999) {
        // 0x62ff
    } else {
        uint64_t v16 = v14;
        uint64_t v17 = 10 * v16; // 0x62da
        int64_t v18 = ((int64_t)v13 + 0xffffffd0 & 0xffffffff) + v17; // 0x62e9
        uint64_t v19 = 10 * v15; // 0x62ef
        int64_t v20 = v18 < v17;
        int64_t v21 = v16 / 0x8000000000000000 + v16 / 0x2000000000000000 + v19 + (int64_t)(v17 < 2 * v16) + v20; // 0x62fa
        if (v21 >= v19) {
            int64_t v22 = v12 + 1;
            char v23 = *(char *)v22; // 0x62b0
            v12 = v22;
            v13 = v23;
            v14 = v18;
            v15 = v21;
            if (v23 == 0) {
                // 0x64ad
            } else {
                goto lab_0x62bf;
            }
        }
    }
}