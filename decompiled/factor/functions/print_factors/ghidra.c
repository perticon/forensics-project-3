undefined8 print_factors(char *param_1)

{
  char cVar1;
  ulong uVar2;
  ulong uVar3;
  ulong uVar4;
  undefined8 uVar5;
  undefined8 uVar6;
  ulong uVar7;
  char *pcVar8;
  long lVar9;
  uint uVar10;
  ulong uVar11;
  undefined8 *puVar12;
  ulong uVar13;
  undefined8 *puVar14;
  long in_FS_OFFSET;
  void *local_168;
  void *local_160;
  ulong local_158;
  undefined8 local_148;
  long local_140;
  undefined8 local_138 [26];
  undefined local_68 [26];
  byte local_4e;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  cVar1 = *param_1;
  while (cVar1 == ' ') {
    pcVar8 = param_1 + 1;
    param_1 = param_1 + 1;
    cVar1 = *pcVar8;
  }
  uVar13 = 4;
  param_1 = param_1 + (cVar1 == '+');
  pcVar8 = param_1;
  do {
    cVar1 = *pcVar8;
    pcVar8 = pcVar8 + 1;
    if (cVar1 == '\0') {
      lVar9 = 0;
      uVar11 = 0;
      pcVar8 = param_1;
      if ((int)uVar13 == 0) goto LAB_001062b0;
      break;
    }
    uVar13 = 0;
  } while ((int)cVar1 - 0x30U < 10);
  uVar5 = quote();
  uVar6 = dcgettext(0,"%s is not a valid positive integer",5);
  error(0,0,uVar6,uVar5);
  uVar5 = 0;
  goto LAB_0010626c;
  while( true ) {
    uVar7 = (ulong)((int)cVar1 - 0x30);
    if (0x1999999999999999 < uVar11) break;
    uVar2 = lVar9 * 10;
    uVar3 = lVar9 * 2;
    uVar10 = (uint)((ulong)lVar9 >> 0x20);
    lVar9 = uVar2 + uVar7;
    uVar4 = uVar11 * 10;
    uVar7 = (ulong)(((uVar10 >> 0x1d) - ((int)uVar10 >> 0x1f)) + (uint)(uVar2 < uVar3) +
                   (uint)CARRY8(uVar2,uVar7));
    uVar11 = uVar4 + uVar7;
    if (CARRY8(uVar4,uVar7)) break;
LAB_001062b0:
    cVar1 = *pcVar8;
    pcVar8 = pcVar8 + 1;
    if (cVar1 == '\0') {
      if (-1 < (long)uVar11) {
        if (dev_debug != '\0') {
          fwrite("[using single-precision arithmetic] ",1,0x24,stderr);
        }
        if (uVar11 == 0) {
          lbuf_putint(lVar9,0);
        }
        else {
          print_uintmaxes_part_0(uVar11,lVar9);
        }
        lbuf_putc(0x3a);
        factor(uVar11,lVar9,&local_148);
        if (local_4e != 0) {
          puVar14 = local_138;
          puVar12 = &local_148;
          do {
            uVar10 = 0;
            if (*(char *)(puVar12 + 0x1c) != '\0') {
              do {
                lbuf_putc(0x20);
                lbuf_putint(*puVar14,0);
                if ((print_exponents != '\0') && (1 < *(byte *)(puVar12 + 0x1c))) {
                  lbuf_putc(0x5e);
                  lbuf_putint(*(undefined *)(puVar12 + 0x1c),0);
                  break;
                }
                uVar10 = uVar10 + 1;
              } while (uVar10 < *(byte *)(puVar12 + 0x1c));
            }
            puVar12 = (undefined8 *)((long)puVar12 + 1);
            puVar14 = puVar14 + 1;
          } while ((uint)((int)puVar12 - (int)&local_148) < (uint)local_4e);
        }
        if (local_140 != 0) {
          lbuf_putc(0x20);
          if (local_140 == 0) {
            lbuf_putint(local_148,0);
          }
          else {
            print_uintmaxes_part_0(local_140,local_148);
          }
        }
        lbuf_putc(10);
        uVar5 = 1;
        goto LAB_0010626c;
      }
      break;
    }
  }
  if (dev_debug != '\0') {
    fwrite("[using arbitrary-precision arithmetic] ",1,0x27,stderr);
  }
  __gmpz_init_set_str(&local_148,param_1,10);
  __gmpz_out_str(stdout,10,&local_148);
  pcVar8 = stdout->_IO_write_ptr;
  if (pcVar8 < stdout->_IO_write_end) {
    stdout->_IO_write_ptr = pcVar8 + 1;
    *pcVar8 = ':';
  }
  else {
    __overflow(stdout,0x3a);
  }
  uVar11 = 0;
  mp_factor(&local_148,&local_168);
  if (local_158 != 0) {
    do {
      uVar7 = 0;
      if (*(long *)((long)local_160 + uVar11 * 8) != 0) {
        do {
          pcVar8 = stdout->_IO_write_ptr;
          if (pcVar8 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar8 + 1;
            *pcVar8 = ' ';
          }
          else {
            __overflow(stdout,0x20);
          }
          __gmpz_out_str(stdout,10,(void *)((long)local_168 + uVar11 * 0x10));
          uVar2 = *(ulong *)((long)local_160 + uVar11 * 8);
          if ((print_exponents != '\0') && (1 < uVar2)) {
            __printf_chk(1,&DAT_0010d09a);
            break;
          }
          uVar7 = (ulong)((int)uVar7 + 1);
        } while (uVar7 < uVar2);
      }
      uVar11 = (ulong)((int)uVar11 + 1);
    } while (uVar11 < local_158);
    if (local_158 != 0) {
      do {
        __gmpz_clear();
        uVar13 = (ulong)((int)uVar13 + 1);
      } while (uVar13 < local_158);
    }
  }
  free(local_168);
  free(local_160);
  __gmpz_clear(&local_148);
  pcVar8 = stdout->_IO_write_ptr;
  if (pcVar8 < stdout->_IO_write_end) {
    stdout->_IO_write_ptr = pcVar8 + 1;
    *pcVar8 = '\n';
  }
  else {
    __overflow(stdout,10);
  }
  fflush_unlocked(stdout);
  uVar5 = 1;
LAB_0010626c:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar5;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}