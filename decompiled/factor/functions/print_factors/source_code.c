print_factors (char const *input)
{
  /* Skip initial spaces and '+'.  */
  char const *str = input;
  while (*str == ' ')
    str++;
  str += *str == '+';

  uintmax_t t1, t0;

  /* Try converting the number to one or two words.  If it fails, use GMP or
     print an error message.  The 2nd condition checks that the most
     significant bit of the two-word number is clear, in a typesize neutral
     way.  */
  strtol_error err = strto2uintmax (&t1, &t0, str);

  switch (err)
    {
    case LONGINT_OK:
      if (((t1 << 1) >> 1) == t1)
        {
          devmsg ("[using single-precision arithmetic] ");
          print_factors_single (t1, t0);
          return true;
        }
      break;

    case LONGINT_OVERFLOW:
      /* Try GMP.  */
      break;

    default:
      error (0, 0, _("%s is not a valid positive integer"), quote (input));
      return false;
    }

  devmsg ("[using arbitrary-precision arithmetic] ");
  mpz_t t;
  struct mp_factors factors;

  mpz_init_set_str (t, str, 10);

  mpz_out_str (stdout, 10, t);
  putchar (':');
  mp_factor (t, &factors);

  for (unsigned int j = 0; j < factors.nfactors; j++)
    for (unsigned int k = 0; k < factors.e[j]; k++)
      {
        putchar (' ');
        mpz_out_str (stdout, 10, factors.p[j]);
        if (print_exponents && factors.e[j] > 1)
          {
            printf ("^%lu", factors.e[j]);
            break;
          }
      }

  mp_factor_clear (&factors);
  mpz_clear (t);
  putchar ('\n');
  fflush (stdout);
  return true;
}