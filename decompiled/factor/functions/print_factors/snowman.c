uint32_t print_factors(void** rdi, void** rsi, int64_t rdx, void** rcx) {
    void** r13_5;
    void* rsp6;
    void** rax7;
    void** v8;
    uint32_t eax9;
    uint64_t r12_10;
    void* rax11;
    void** r13_12;
    void** rdx13;
    int32_t eax14;
    void** rsi15;
    void** r14_16;
    void** rbp17;
    uint32_t eax18;
    int64_t rax19;
    void*** rdx20;
    void*** rbp21;
    uint64_t rdi22;
    void** rdx23;
    uint64_t rax24;
    uint32_t eax25;
    void** tmp64_26;
    void** rbp27;
    void* rax28;
    void** tmp64_29;
    int1_t zf30;
    void** r9_31;
    int1_t zf32;
    void** r9_33;
    void* rsp34;
    void** r12_35;
    void** rdx36;
    signed char v37;
    void** rbp38;
    uint32_t ebx39;
    signed char v40;
    unsigned char v41;
    void** v42;
    int1_t zf43;
    uint32_t eax44;
    unsigned char v45;
    void** rdi46;
    unsigned char v47;
    int64_t v48;
    void** v49;
    void** v50;
    void* rdx51;
    void** r14_52;
    void** rdi53;
    void** rdx54;
    void** rsp55;
    void** rdi56;
    void** rax57;
    void** rsi58;
    void** r13_59;
    void** r9_60;
    void** rax61;
    int64_t v62;
    void** v63;
    void** v64;
    void** r9_65;
    void** rdi66;
    void** rax67;
    void** r15_68;
    uint64_t rbp69;
    void* rbx70;
    int64_t* v71;
    void** rdi72;
    void** rax73;
    void** rdi74;
    void** v75;
    int1_t zf76;
    void** rdx77;
    void*** v78;
    void** rax79;
    void** v80;
    void** rdi81;
    uint64_t rdi82;
    void** v83;
    void** r9_84;
    uint64_t v85;

    r13_5 = rdi;
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x138);
    rax7 = g28;
    v8 = rax7;
    eax9 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi));
    if (*reinterpret_cast<signed char*>(&eax9) == 32) {
        do {
            eax9 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_5 + 1));
            ++r13_5;
        } while (*reinterpret_cast<signed char*>(&eax9) == 32);
    }
    *reinterpret_cast<int32_t*>(&r12_10) = 4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_10) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax11) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax9) == 43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    r13_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_5) + reinterpret_cast<uint64_t>(rax11));
    rdx13 = r13_12;
    do {
        eax14 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdx13));
        ++rdx13;
        if (!eax14) 
            break;
        *reinterpret_cast<int32_t*>(&r12_10) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_10) + 4) = 0;
    } while (eax14 - 48 <= 9);
    goto addr_623e_7;
    rsi15 = r13_12;
    *reinterpret_cast<int32_t*>(&r14_16) = 0;
    *reinterpret_cast<int32_t*>(&r14_16 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rbp17) = 0;
    *reinterpret_cast<int32_t*>(&rbp17 + 4) = 0;
    if (*reinterpret_cast<int32_t*>(&r12_10)) {
        addr_623e_7:
        quote();
        fun_2700();
        fun_2a20();
        eax18 = 0;
    } else {
        do {
            *reinterpret_cast<int32_t*>(&rax19) = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rsi15));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax19) + 4) = 0;
            ++rsi15;
            if (!*reinterpret_cast<int32_t*>(&rax19)) 
                break;
            *reinterpret_cast<uint32_t*>(&rcx) = static_cast<uint32_t>(rax19 - 48);
            *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
        } while (reinterpret_cast<unsigned char>(rbp17) <= reinterpret_cast<unsigned char>(0x1999999999999999) && (rdx20 = reinterpret_cast<void***>(r14_16 + reinterpret_cast<unsigned char>(r14_16) * 4), rbp21 = reinterpret_cast<void***>(rbp17 + reinterpret_cast<unsigned char>(rbp17) * 4), rdi22 = reinterpret_cast<unsigned char>(r14_16) >> 61, rdx23 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdx20) + reinterpret_cast<uint64_t>(rdx20)), rax24 = reinterpret_cast<unsigned char>(r14_16) >> 63, eax25 = *reinterpret_cast<int32_t*>(&rax24) + *reinterpret_cast<int32_t*>(&rdi22) + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx23) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r14_16) + reinterpret_cast<unsigned char>(r14_16))), tmp64_26 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx23) + reinterpret_cast<unsigned char>(rcx)), rdx13 = tmp64_26, rbp27 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbp21) + reinterpret_cast<uint64_t>(rbp21)), r14_16 = rdx13, *reinterpret_cast<uint32_t*>(&rcx) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_26) < reinterpret_cast<unsigned char>(rdx23)), *reinterpret_cast<int32_t*>(&rcx + 4) = 0, *reinterpret_cast<uint32_t*>(&rax28) = eax25 + *reinterpret_cast<uint32_t*>(&rcx), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax28) + 4) = 0, tmp64_29 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp27) + reinterpret_cast<uint64_t>(rax28)), rbp17 = tmp64_29, reinterpret_cast<unsigned char>(tmp64_29) >= reinterpret_cast<unsigned char>(rbp27)));
        goto addr_62ff_12;
        if (reinterpret_cast<signed char>(rbp17) < reinterpret_cast<signed char>(0)) {
            addr_62ff_12:
            zf30 = dev_debug == 0;
            if (!zf30) {
                rcx = stderr;
                fun_2a80("[using arbitrary-precision arithmetic] ", 1, 39, rcx, 0x1999999999999999, r9_31);
                rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                goto addr_630c_15;
            }
        } else {
            zf32 = dev_debug == 0;
            if (!zf32) {
                rcx = stderr;
                *reinterpret_cast<int32_t*>(&rdx13) = 36;
                *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
                fun_2a80("[using single-precision arithmetic] ", 1, 36, rcx, 0x1999999999999999, r9_33);
                rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
            }
            if (!rbp17) {
                lbuf_putint(r14_16, 0, rdx13, rcx);
                rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
            } else {
                print_uintmaxes_part_0(rbp17, r14_16, rdx13, rcx);
                rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
            }
            r12_35 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp34) + 32);
            lbuf_putc(58, 58);
            rdx36 = r12_35;
            factor();
            if (v37) {
                rbp38 = r12_35;
                while (1) {
                    ebx39 = 0;
                    if (!v40) {
                        addr_654c_24:
                        ++rbp38;
                        *reinterpret_cast<uint32_t*>(&rdx36) = v41;
                        *reinterpret_cast<int32_t*>(&rdx36 + 4) = 0;
                        if (*reinterpret_cast<uint32_t*>(&rdx36) > reinterpret_cast<uint32_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rbp38)) - reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&r12_35)))) 
                            continue; else 
                            break;
                    } else {
                        do {
                            lbuf_putc(32, 32);
                            lbuf_putint(v42, 0, rdx36, rcx);
                            zf43 = print_exponents == 0;
                            eax44 = v45;
                            if (zf43) 
                                continue;
                            if (*reinterpret_cast<unsigned char*>(&eax44) > 1) 
                                break;
                            ++ebx39;
                        } while (ebx39 < eax44);
                        goto addr_654c_24;
                    }
                    lbuf_putc(94);
                    *reinterpret_cast<uint32_t*>(&rdi46) = v47;
                    *reinterpret_cast<int32_t*>(&rdi46 + 4) = 0;
                    lbuf_putint(rdi46, 0, rdx36, rcx);
                    goto addr_654c_24;
                }
            }
            if (v48) {
                lbuf_putc(32, 32);
                if (!v49) {
                    lbuf_putint(v50, 0, rdx36, rcx);
                } else {
                    print_uintmaxes_part_0(v49, v50, rdx36, rcx);
                }
            }
            lbuf_putc(10, 10);
            eax18 = 1;
        }
    }
    addr_626c_35:
    rdx51 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
    if (rdx51) {
        fun_2740();
    } else {
        return eax18;
    }
    addr_630c_15:
    r14_52 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 32);
    fun_2a50(r14_52, r13_12, 10, rcx);
    rdi53 = stdout;
    rdx54 = r14_52;
    fun_2800(rdi53, 10, rdx54, rcx);
    rsp55 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) - 8 + 8 - 8 + 8);
    rdi56 = stdout;
    rax57 = *reinterpret_cast<void***>(rdi56 + 40);
    if (reinterpret_cast<unsigned char>(rax57) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi56 + 48))) {
        fun_27c0();
        rsp55 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp55 - 8) + 8);
    } else {
        rdx54 = rax57 + 1;
        *reinterpret_cast<void***>(rdi56 + 40) = rdx54;
        *reinterpret_cast<void***>(rax57) = reinterpret_cast<void**>(58);
    }
    rsi58 = rsp55;
    *reinterpret_cast<int32_t*>(&r13_59) = 0;
    *reinterpret_cast<int32_t*>(&r13_59 + 4) = 0;
    mp_factor(r14_52, rsi58, rdx54, rcx, 0x1999999999999999, r9_60);
    *reinterpret_cast<int32_t*>(&rax61) = 0;
    *reinterpret_cast<int32_t*>(&rax61 + 4) = 0;
    if (!v62) {
        addr_645c_42:
        fun_25e0(v63, rsi58, v63, rsi58);
        fun_25e0(v64, rsi58, v64, rsi58);
        fun_2940(r14_52, rsi58, rdx54, rcx, 0x1999999999999999, r9_65);
        rdi66 = stdout;
        rax67 = *reinterpret_cast<void***>(rdi66 + 40);
        if (reinterpret_cast<unsigned char>(rax67) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi66 + 48))) {
            *reinterpret_cast<int32_t*>(&rsi58) = 10;
            *reinterpret_cast<int32_t*>(&rsi58 + 4) = 0;
            fun_27c0();
        } else {
            rdx54 = rax67 + 1;
            *reinterpret_cast<void***>(rdi66 + 40) = rdx54;
            *reinterpret_cast<void***>(rax67) = reinterpret_cast<void**>(10);
        }
    } else {
        do {
            *reinterpret_cast<int32_t*>(&r15_68) = 0;
            *reinterpret_cast<int32_t*>(&r15_68 + 4) = 0;
            rbp69 = reinterpret_cast<unsigned char>(rax61) * 8;
            rbx70 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax61) << 4);
            if (v71[reinterpret_cast<unsigned char>(rax61)]) {
                do {
                    rdi72 = stdout;
                    rax73 = *reinterpret_cast<void***>(rdi72 + 40);
                    if (reinterpret_cast<unsigned char>(rax73) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi72 + 48))) {
                        *reinterpret_cast<void***>(rdi72 + 40) = rax73 + 1;
                        *reinterpret_cast<void***>(rax73) = reinterpret_cast<void**>(32);
                    } else {
                        fun_27c0();
                    }
                    rdi74 = stdout;
                    *reinterpret_cast<int32_t*>(&rsi58) = 10;
                    *reinterpret_cast<int32_t*>(&rsi58 + 4) = 0;
                    fun_2800(rdi74, 10, reinterpret_cast<unsigned char>(v75) + reinterpret_cast<uint64_t>(rbx70), rcx);
                    zf76 = print_exponents == 0;
                    rdx77 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(v78) + rbp69);
                    if (zf76) 
                        continue;
                    if (reinterpret_cast<unsigned char>(rdx77) > reinterpret_cast<unsigned char>(1)) 
                        break;
                    *reinterpret_cast<int32_t*>(&rax79) = static_cast<int32_t>(reinterpret_cast<uint64_t>(r15_68 + 1));
                    *reinterpret_cast<int32_t*>(&rax79 + 4) = 0;
                    r15_68 = rax79;
                } while (reinterpret_cast<unsigned char>(rax79) < reinterpret_cast<unsigned char>(rdx77));
                continue;
            } else {
                continue;
            }
            rsi58 = reinterpret_cast<void**>("^%lu");
            fun_29b0(1, "^%lu", rdx77, rcx);
            rdx54 = v80;
            *reinterpret_cast<int32_t*>(&rax61) = static_cast<int32_t>(reinterpret_cast<uint64_t>(r13_59 + 1));
            *reinterpret_cast<int32_t*>(&rax61 + 4) = 0;
            r13_59 = rax61;
        } while (reinterpret_cast<unsigned char>(rax61) < reinterpret_cast<unsigned char>(rdx54));
        if (!rdx54) 
            goto addr_645c_42; else 
            goto addr_6435_57;
    }
    rdi81 = stdout;
    fun_2ac0(rdi81, rsi58, rdx54, rcx);
    eax18 = 1;
    goto addr_626c_35;
    addr_6435_57:
    *reinterpret_cast<int32_t*>(&rdi82) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi82) + 4) = 0;
    do {
        fun_2940((rdi82 << 4) + reinterpret_cast<unsigned char>(v83), rsi58, rdx54, rcx, 0x1999999999999999, r9_84);
        *reinterpret_cast<int32_t*>(&rdi82) = static_cast<int32_t>(r12_10 + 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi82) + 4) = 0;
        r12_10 = rdi82;
    } while (rdi82 < v85);
    goto addr_645c_42;
}