void factor_insert_multiplicity(int32_t * factors, uint64_t prime, int32_t m) {
    int64_t v1 = (int64_t)factors;
    char * v2 = (char *)(v1 + 250); // 0x2f20
    unsigned char v3 = *v2; // 0x2f20
    int64_t v4 = v1 + 16; // 0x2f2b
    int64_t v5 = v1 + 224; // 0x2f2f
    int64_t v6 = v5; // 0x2f3b
    int64_t v7 = v4; // 0x2f3b
    if (v3 != 0) {
        int64_t v8 = (int64_t)v3 + 0xffffffff; // 0x2f3d
        int64_t v9 = 0x100000000 * v8 >> 32; // 0x2f40
        int64_t v10 = v9;
        uint64_t v11 = *(int64_t *)(8 * v10 + v4); // 0x2f5d
        int64_t v12; // 0x2f20
        int64_t v13; // 0x2f20
        int64_t v14; // 0x2f20
        while (v11 > prime) {
            // 0x2f50
            v14 = 0xffffffff;
            v12 = v5;
            v13 = v4;
            if (v10 == 0) {
                goto lab_0x2f80;
            }
            v10--;
            v11 = *(int64_t *)(8 * v10 + v4);
        }
        if (v11 == prime) {
            char * v15 = (char *)(v10 + v5); // 0x2fb8
            *v15 = *v15 + (char)m;
            return;
        }
        int64_t v16 = v10 & 0xffffffff; // 0x2f5b
        int64_t v17 = 0x100000000 * v10 + 0x100000000;
        int64_t v18 = (v17 >> 29) + v4; // 0x2f6b
        int64_t v19 = (v17 >> 32) + v5; // 0x2f6f
        v14 = v16;
        v12 = v19;
        v13 = v18;
        v6 = v19;
        v7 = v18;
        if (v16 < (v8 & 0xffffffff)) {
          lab_0x2f80:;
            int64_t v20 = v9;
            int64_t v21 = 8 * v20; // 0x2f80
            *(int64_t *)(v1 + 24 + v21) = *(int64_t *)(v21 + v4);
            *(char *)(v1 + 225 + v20) = *(char *)(v20 + v5);
            int64_t v22 = v20 - 1; // 0x2f99
            v6 = v12;
            v7 = v13;
            while (v14 < v22) {
                // 0x2f80
                v20 = v22;
                v21 = 8 * v20;
                *(int64_t *)(v1 + 24 + v21) = *(int64_t *)(v21 + v4);
                *(char *)(v1 + 225 + v20) = *(char *)(v20 + v5);
                v22 = v20 - 1;
                v6 = v12;
                v7 = v13;
            }
        }
    }
    // 0x2fa1
    *(int64_t *)v7 = prime;
    *(char *)v6 = (char)m;
    *v2 = v3 + 1;
}