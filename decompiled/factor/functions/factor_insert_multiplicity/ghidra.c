void factor_insert_multiplicity(long param_1,ulong param_2,char param_3)

{
  ulong *puVar1;
  byte bVar2;
  int iVar3;
  ulong uVar4;
  ulong uVar5;
  ulong uVar6;
  char *pcVar7;
  ulong *puVar8;
  bool bVar9;
  
  bVar2 = *(byte *)(param_1 + 0xfa);
  puVar8 = (ulong *)(param_1 + 0x10);
  pcVar7 = (char *)(param_1 + 0xe0);
  if (bVar2 != 0) {
    uVar5 = (ulong)(int)(bVar2 - 1);
    uVar4 = uVar5;
    do {
      uVar6 = uVar4 & 0xffffffff;
      puVar1 = (ulong *)(param_1 + 0x10 + uVar4 * 8);
      bVar9 = *puVar1 == param_2;
      if (*puVar1 < param_2 || bVar9) {
        if (bVar9) {
          pcVar7[uVar4] = pcVar7[uVar4] + param_3;
          return;
        }
        iVar3 = (int)uVar6 + 1;
        puVar8 = puVar8 + iVar3;
        pcVar7 = pcVar7 + iVar3;
        if ((int)(bVar2 - 1) <= (int)uVar6) goto LAB_00102fa1;
        break;
      }
      uVar4 = uVar4 - 1;
      uVar6 = uVar4 & 0xffffffff;
    } while ((int)uVar4 != -1);
    do {
      *(undefined8 *)(param_1 + 0x18 + uVar5 * 8) = *(undefined8 *)(param_1 + 0x10 + uVar5 * 8);
      *(undefined *)(param_1 + 0xe1 + uVar5) = *(undefined *)(param_1 + 0xe0 + uVar5);
      uVar5 = uVar5 - 1;
    } while ((int)uVar6 < (int)uVar5);
  }
LAB_00102fa1:
  *puVar8 = param_2;
  *pcVar7 = param_3;
  *(byte *)(param_1 + 0xfa) = bVar2 + 1;
  return;
}