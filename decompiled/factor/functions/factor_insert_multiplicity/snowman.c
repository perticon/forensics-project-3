void factor_insert_multiplicity(void** rdi, void** rsi, signed char dl, ...) {
    int64_t rax4;
    void** r8_5;
    void*** r11_6;
    signed char* r10_7;
    uint32_t r9d8;
    int32_t ebx9;
    void* rcx10;
    void* rax11;
    int64_t rsi12;
    void* rax13;
    uint32_t r9d14;
    uint32_t eax15;

    *reinterpret_cast<uint32_t*>(&rax4) = *reinterpret_cast<unsigned char*>(rdi + 0xfa);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    r8_5 = rsi;
    r11_6 = reinterpret_cast<void***>(rdi + 16);
    r10_7 = reinterpret_cast<signed char*>(rdi + 0xe0);
    r9d8 = *reinterpret_cast<uint32_t*>(&rax4);
    if (!*reinterpret_cast<uint32_t*>(&rax4)) 
        goto addr_2fa1_2;
    ebx9 = static_cast<int32_t>(rax4 - 1);
    rcx10 = reinterpret_cast<void*>(static_cast<int64_t>(ebx9));
    rax11 = rcx10;
    do {
        *reinterpret_cast<int32_t*>(&rsi12) = *reinterpret_cast<int32_t*>(&rax11);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi12) + 4) = 0;
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi + reinterpret_cast<uint64_t>(rax11) * 8) + 16)) <= reinterpret_cast<unsigned char>(r8_5)) 
            break;
        rax11 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax11) - 1);
        *reinterpret_cast<int32_t*>(&rsi12) = *reinterpret_cast<int32_t*>(&rax11);
    } while (*reinterpret_cast<int32_t*>(&rax11) != -1);
    goto addr_2f80_6;
    if (*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi + reinterpret_cast<uint64_t>(rax11) * 8) + 16) == r8_5) {
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_7) + reinterpret_cast<uint64_t>(rax11)) = reinterpret_cast<signed char>(*reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_7) + reinterpret_cast<uint64_t>(rax11)) + dl);
        return;
    }
    rax13 = reinterpret_cast<void*>(static_cast<int64_t>(static_cast<int32_t>(rsi12 + 1)));
    r11_6 = r11_6 + reinterpret_cast<uint64_t>(rax13) * 8;
    r10_7 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_7) + reinterpret_cast<uint64_t>(rax13));
    if (*reinterpret_cast<int32_t*>(&rsi12) >= ebx9) {
        addr_2fa1_2:
        r9d14 = r9d8 + 1;
        *r11_6 = r8_5;
        *r10_7 = dl;
        *reinterpret_cast<unsigned char*>(rdi + 0xfa) = *reinterpret_cast<unsigned char*>(&r9d14);
        return;
    }
    do {
        addr_2f80_6:
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi + reinterpret_cast<uint64_t>(rcx10) * 8) + 24) = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi + reinterpret_cast<uint64_t>(rcx10) * 8) + 16);
        eax15 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<uint64_t>(rcx10) + 0xe0);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<uint64_t>(rcx10) + 0xe1) = *reinterpret_cast<signed char*>(&eax15);
        rcx10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx10) - 1);
    } while (*reinterpret_cast<int32_t*>(&rsi12) < *reinterpret_cast<int32_t*>(&rcx10));
    goto addr_2fa1_2;
}