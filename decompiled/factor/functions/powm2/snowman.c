void** powm2(void** rdi, struct s43* rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r15_7;
    void** r12_8;
    void** rbx9;
    void** rax10;
    void** r8_11;
    void** r10_12;
    void** v13;
    void** r14_14;
    void** r13_15;
    void** v16;
    int32_t ebx17;
    void** v18;
    void** rbp19;
    void** rax20;
    void** rax21;
    void** rbx22;
    void** rax23;
    void** rax24;

    r15_7 = rdi;
    r12_8 = r8;
    rbx9 = *reinterpret_cast<void***>(r9);
    rax10 = *reinterpret_cast<void***>(r9 + 8);
    r8_11 = rsi->f0;
    r10_12 = rsi->f8;
    v13 = rdx;
    r14_14 = *reinterpret_cast<void***>(rcx);
    r13_15 = *reinterpret_cast<void***>(rcx + 8);
    v16 = rbx9;
    ebx17 = 64;
    v18 = rax10;
    rbp19 = *reinterpret_cast<void***>(rdx);
    do {
        if (*reinterpret_cast<unsigned char*>(&rbp19) & 1) {
            rax20 = mulredc2(r15_7, v18, v16, r10_12, r8_11, r13_15, r14_14, r12_8);
            v16 = rax20;
            v18 = *reinterpret_cast<void***>(r15_7);
            r8_11 = r8_11;
            r10_12 = r10_12;
        }
        rbp19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp19) >> 1);
        rax21 = mulredc2(r15_7, r10_12, r8_11, r10_12, r8_11, r13_15, r14_14, r12_8);
        r10_12 = *reinterpret_cast<void***>(r15_7);
        r8_11 = rax21;
        --ebx17;
    } while (ebx17);
    rbx22 = *reinterpret_cast<void***>(v13 + 8);
    if (rbx22) {
        do {
            if (*reinterpret_cast<unsigned char*>(&rbx22) & 1) {
                rax23 = mulredc2(r15_7, v18, v16, r10_12, r8_11, r13_15, r14_14, r12_8);
                v16 = rax23;
                v18 = *reinterpret_cast<void***>(r15_7);
                r8_11 = r8_11;
                r10_12 = r10_12;
            }
            rax24 = mulredc2(r15_7, r10_12, r8_11, r10_12, r8_11, r13_15, r14_14, r12_8);
            rbx22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx22) >> 1);
            r10_12 = *reinterpret_cast<void***>(r15_7);
            r8_11 = rax24;
        } while (rbx22);
    }
    *reinterpret_cast<void***>(r15_7) = v18;
    return v16;
}