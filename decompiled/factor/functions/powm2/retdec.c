int64_t powm2(int64_t * r1m, int64_t * bp, int64_t * ep, int64_t * np, int64_t ni, int64_t * one) {
    int64_t v1 = (int64_t)np;
    int64_t v2 = (int64_t)bp;
    int64_t v3; // bp-104, 0x3240
    int64_t v4 = &v3; // 0x3250
    int64_t v5 = *(int64_t *)(v1 + 8); // 0x326a
    int64_t * v6 = (int64_t *)(v4 - 8);
    int64_t * v7 = (int64_t *)(v4 - 16);
    int64_t * v8 = (int64_t *)(v4 + 16);
    int64_t * v9 = (int64_t *)(v4 + 8);
    int64_t * v10 = (int64_t *)(v4 + 32);
    int64_t * v11 = (int64_t *)(v4 + 24);
    int32_t v12 = 64; // 0x3280
    int64_t v13 = (int64_t)ep; // 0x3280
    int64_t v14 = *(int64_t *)(v2 + 8);
    int64_t v15 = v2; // 0x32b5
    int64_t v16 = v14; // 0x32b5
    int64_t v17; // 0x32c4
    int64_t v18; // 0x32c9
    int64_t v19; // 0x32d8
    if (v13 % 2 != 0) {
        // 0x32b7
        *v6 = ni;
        *v7 = v1;
        v17 = *v8;
        v18 = *v9;
        *v10 = v2;
        *v11 = v14;
        v19 = mulredc2(r1m, v18, v17, v14, v2, v5, (int64_t)&g34, (int64_t)&g34);
        *v8 = v19;
        *v9 = (int64_t)r1m;
        v15 = *v10;
        v16 = *v11;
    }
    int64_t v20 = v16;
    int64_t v21 = v15;
    *v6 = ni;
    *v7 = v1;
    int64_t v22 = mulredc2(r1m, v20, v21, v20, v21, v5, (int64_t)&g34, (int64_t)&g34); // 0x329e
    int64_t v23 = *v7; // 0x32a3
    v12--;
    v13 /= 2;
    while (v12 != 0) {
        // 0x32b1
        v14 = v23;
        int64_t v24 = v22;
        v15 = v24;
        v16 = v14;
        if (v13 % 2 != 0) {
            // 0x32b7
            *v6 = ni;
            *v7 = v1;
            v17 = *v8;
            v18 = *v9;
            *v10 = v24;
            *v11 = v14;
            v19 = mulredc2(r1m, v18, v17, v14, v24, v5, (int64_t)&g34, (int64_t)&g34);
            *v8 = v19;
            *v9 = (int64_t)r1m;
            v15 = *v10;
            v16 = *v11;
        }
        // 0x3288
        v20 = v16;
        v21 = v15;
        *v6 = ni;
        *v7 = v1;
        v22 = mulredc2(r1m, v20, v21, v20, v21, v5, (int64_t)&g34, (int64_t)&g34);
        v23 = *v7;
        v12--;
        v13 /= 2;
    }
    int64_t v25 = *(int64_t *)(*(int64_t *)(v4 + 40) + 8); // 0x3305
    uint64_t v26 = v25; // 0x330c
    if (v25 == 0) {
        // 0x3380
        *r1m = *v9;
        return *v8;
    }
    int64_t v27 = v22; // 0x3338
    int64_t v28 = v23; // 0x3338
    int64_t v29; // 0x3347
    int64_t v30; // 0x334c
    int64_t v31; // 0x335b
    if (v26 % 2 != 0) {
        // 0x333a
        *v6 = ni;
        *v7 = v1;
        v29 = *v8;
        v30 = *v9;
        *v10 = v22;
        *v11 = v23;
        v31 = mulredc2(r1m, v30, v29, v23, v22, v5, (int64_t)&g34, (int64_t)&g34);
        *v8 = v31;
        *v9 = v23;
        v27 = *v10;
        v28 = *v11;
    }
    int64_t v32 = v28;
    int64_t v33 = v27;
    *v6 = ni;
    *v7 = v1;
    int64_t v34 = mulredc2(r1m, v32, v33, v32, v33, v5, (int64_t)&g34, (int64_t)&g34); // 0x3323
    while (v26 >= 2) {
        // 0x3335
        v26 /= 2;
        int64_t v35 = v34;
        v27 = v35;
        v28 = v23;
        if (v26 % 2 != 0) {
            // 0x333a
            *v6 = ni;
            *v7 = v1;
            v29 = *v8;
            v30 = *v9;
            *v10 = v35;
            *v11 = v23;
            v31 = mulredc2(r1m, v30, v29, v23, v35, v5, (int64_t)&g34, (int64_t)&g34);
            *v8 = v31;
            *v9 = v23;
            v27 = *v10;
            v28 = *v11;
        }
        // 0x3310
        v32 = v28;
        v33 = v27;
        *v6 = ni;
        *v7 = v1;
        v34 = mulredc2(r1m, v32, v33, v32, v33, v5, (int64_t)&g34, (int64_t)&g34);
    }
    // 0x3380
    *r1m = *v9;
    return *v8;
}