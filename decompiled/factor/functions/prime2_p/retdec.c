bool prime2_p(uint64_t n1, uint64_t n0) {
    int64_t na[2]; // bp-384, 0x5b60
    int64_t q[2]; // bp-432, 0x5b60
    int64_t v1[2]; // 0x5ee2
    int64_t v2[2]; // 0x5bcc
    int64_t v3[2]; // 0x5c80
    int64_t v4 = __readfsqword(40); // 0x5b74
    int64_t v5; // 0x5b60
    int64_t v6; // 0x5b60
    int64_t v7; // 0x5b60
    int64_t v8; // 0x5b60
    int64_t v9; // 0x5b60
    int64_t v10; // 0x5b60
    int64_t v11; // 0x5b60
    int64_t v12; // 0x5b60
    int64_t v13; // 0x5b60
    int64_t v14; // bp-392, 0x5b60
    int64_t v15; // bp-408, 0x5b60
    int64_t v16; // bp-424, 0x5b60
    int64_t v17; // 0x5b60
    int64_t v18; // 0x5c28
    uint64_t v19; // 0x5b9d
    uint64_t v20; // 0x5ba1
    if (n1 == 0) {
        // 0x6009
        v8 = 0;
        if (n0 >= 2) {
            // 0x6150
            v8 = 1;
            if (n0 >= 0x17ded79) {
                // 0x6159
                if (v4 == __readfsqword(40)) {
                    // 0x616c
                    return prime_p_part_0(n0) % 2 != 0;
                }
                // 0x61c2
                return function_2740() % 2 != 0;
            }
        }
        goto lab_0x6015;
    } else {
        // 0x5b90
        v19 = n1 - (int64_t)(n0 == 0);
        v20 = n0 - 1;
        int64_t v21; // 0x5b60
        if (v20 == 0) {
            uint64_t v22 = v19 == 0 ? v17 : llvm_cttz_i64(v19, true); // 0x5ed6
            v1[0] = (0x100000000 * v22 + 0x4000000000) / 0x100000000;
            q = v1;
            v21 = v19 >> v22 % 64;
        } else {
            int64_t v23 = llvm_cttz_i64(v20, true); // 0x5bc4
            v2[0] = v23;
            q = v2;
            v21 = v19 << -v23 % 64 | v20 >> v23;
        }
        // 0x5be4
        v16 = v21;
        unsigned char v24 = *(char *)(n0 / 2 % 128 + (int64_t)"\x01\xab\xcd\xb7\x39\xa3\xc5\xef\xf1\x1b=\xa7)\x13\x35\xdf\xe1\x8b\xad\x97\x19\x83\xa5\xcf\xd1\xfb\x1d\x87\t\xf3\x15\xbf\xc1k\x8dw\xf9\x63\x85\xaf\xb1\xdb\xfdg\xe9\xd3\xf5\x9f\xa1KmW\xd9\x43\x65\x8f\x91\xbb\xddG\xc9\xb3\xd5\x7f\x81+M7\xb9#Eoq\x9b\xbd'\xa9\x93\xb5_a\v-\x17\x99\x03%OQ{\x9d\a\x89s\x95?A\xeb\r\xf7y\xe3\x05/1[}\xe7iSu\x1f!\xcb\xed\xd7Y\xc3\xe5\x0f\x11;]\xc7I3U\xff"); // 0x5c09
        int64_t v25 = v24; // 0x5c09
        int64_t v26 = (2 - v25 * n0) * v25; // 0x5c19
        v18 = (2 - v26 * n0) * v26;
        v5 = n1 != 1;
        v9 = n1 == 1 ? 127 : 63;
        v11 = n1 == 1;
        while (true) {
          lab_0x5c58:
            // 0x5c58
            v10 = v9;
            uint64_t v27 = 2 * v11; // 0x5c5e
            int64_t v28 = v11 >> 63 | 2 * v5; // 0x5c65
            if (v28 > n1) {
                // 0x5c74
                v6 = v28 - n1 + (int64_t)(v27 < n0);
                v12 = v27 - n0;
                goto lab_0x5c7a;
            } else {
                // 0x5c6d
                if (v27 < n0 || v28 != n1) {
                    goto lab_0x5c7a;
                } else {
                    // 0x5c74
                    v6 = v28 - n1 + (int64_t)(v27 < n0);
                    v12 = v27 - n0;
                    goto lab_0x5c7a;
                }
            }
        }
      lab_0x5c80:
        // 0x5c80
        v3[0] = v7;
        na = v3;
        v14 = v13;
        uint64_t v29 = 2 * v13; // 0x5c93
        uint64_t v30 = 2 * v7 | (int64_t)(v29 < v13); // 0x5c96
        v15 = v29;
        if (v30 > n1) {
            // 0x610b
            v15 = v29 - n0;
            goto lab_0x5cb8;
        } else {
            if (v29 < n0 || v30 != n1) {
                goto lab_0x5cb8;
            } else {
                // 0x610b
                v15 = v29 - n0;
                goto lab_0x5cb8;
            }
        }
    }
  lab_0x6015:
    // 0x6015
    if (v4 == __readfsqword(40)) {
        // 0x602c
        return v8 % 2 != 0;
    }
    // 0x61c2
    return function_2740() % 2 != 0;
  lab_0x5f72:;
    // 0x5f72
    int64_t v31; // 0x5b60
    int64_t v32 = v31;
    int64_t v33; // 0x5b60
    int64_t v34 = v33;
    int64_t v35 = v34; // 0x5f76
    int64_t v36; // 0x5b60
    int64_t v37 = v36 - 1; // 0x5f76
    int64_t v38 = v32; // 0x5f76
    if (v36 == 0) {
        // break -> 0x5f78
        goto lab_0x5f78;
    }
    goto lab_0x5f50;
  lab_0x5ea6:;
    // 0x5ea6
    int64_t v45; // 0x5b60
    int64_t v46 = v45;
    int64_t v47; // 0x5b60
    int64_t v48 = v47;
    int64_t v49; // 0x5b60
    int64_t v50 = v49;
    int32_t v51; // 0x5b60
    int32_t v52 = v51;
    int64_t v53; // 0x5b60
    int64_t v54 = v53;
    int64_t v55; // 0x5b60
    int64_t v56 = v55;
    int64_t v57; // 0x5b60
    int64_t v58 = v57;
    int64_t v59; // 0x5b60
    int64_t v60 = v59;
    char * v61; // 0x5b60
    char * v62 = v61;
    int128_t v63; // bp-328, 0x5b60
    uint64_t v64 = *(int64_t *)((int64_t)&v63 + 8 + 8 * v48); // 0x5eab
    int64_t v65; // 0x5b60
    int64_t v66; // 0x5b60
    int64_t v67; // 0x5b60
    int64_t v68; // 0x5b60
    int64_t v69; // 0x5b60
    int64_t v70; // 0x5b60
    int64_t v71; // 0x5b60
    int64_t v72; // bp-360, 0x5b60
    int64_t v73; // 0x5b60
    int64_t v74; // 0x5b60
    int64_t v75; // 0x5b60
    int64_t v76; // 0x5b60
    int64_t v77; // 0x5b60
    int64_t v78; // 0x5b60
    int32_t v79; // 0x5b60
    int32_t v80; // 0x5b60
    int64_t v81; // 0x5b60
    int64_t v82; // 0x5b60
    char * v83; // 0x5b60
    char * v84; // 0x5b60
    if (v64 != 2) {
        unsigned char v85 = *(char *)(v64 / 2 % 128 + (int64_t)"\x01\xab\xcd\xb7\x39\xa3\xc5\xef\xf1\x1b=\xa7)\x13\x35\xdf\xe1\x8b\xad\x97\x19\x83\xa5\xcf\xd1\xfb\x1d\x87\t\xf3\x15\xbf\xc1k\x8dw\xf9\x63\x85\xaf\xb1\xdb\xfdg\xe9\xd3\xf5\x9f\xa1KmW\xd9\x43\x65\x8f\x91\xbb\xddG\xc9\xb3\xd5\x7f\x81+M7\xb9#Eoq\x9b\xbd'\xa9\x93\xb5_a\v-\x17\x99\x03%OQ{\x9d\a\x89s\x95?A\xeb\r\xf7y\xe3\x05/1[}\xe7iSu\x1f!\xcb\xed\xd7Y\xc3\xe5\x0f\x11;]\xc7I3U\xff"); // 0x5e0d
        int64_t v86 = v85; // 0x5e0d
        int64_t v87 = (2 - v64 * v86) * v86; // 0x5e22
        int64_t v88 = (2 - v87 * v64) * v87; // 0x5e31
        int64_t v89 = v88 * v20 * (2 - v88 * v64); // 0x5e43
        v84 = v62;
        v78 = v60;
        v76 = v58;
        v82 = v56;
        v74 = v54;
        v80 = v52;
        v70 = v50;
        v68 = v48;
        v71 = v89;
        v66 = v46;
        if (v19 < v64) {
            goto lab_0x5ff0;
        } else {
            // 0x5e50
            v72 = v89;
            v83 = v62;
            v77 = v60;
            v75 = v58;
            v81 = v56;
            v73 = v54;
            v79 = v52;
            v69 = v50;
            v67 = v48;
            v65 = v46;
            goto lab_0x5e70;
        }
    } else {
        // 0x5eba
        v72 = 0x8000000000000000 * v19 | v20 / 2;
        v83 = v62;
        v77 = v60;
        v75 = v58;
        v81 = v56;
        v73 = v54;
        v79 = v52;
        v69 = v50;
        v67 = v48;
        v65 = v46;
        goto lab_0x5e70;
    }
  lab_0x613e:;
    // 0x613e
    bool v90; // 0x5b60
    bool v91; // 0x5cfe
    if (v90) {
        // 0x6146
        v8 = v91 ? 255 : 0;
        goto lab_0x6015;
    }
    goto lab_0x5f21;
  lab_0x5f21:;
    // 0x5f21
    uint64_t v44; // 0x5b60
    uint64_t v43; // 0x5b60
    while (true) {
      lab_0x5f50:;
        uint64_t v39 = v38;
        v36 = v37;
        int64_t v40 = v35;
        uint64_t v41 = 2 * v39; // 0x5f56
        int64_t v42 = v39 / 0x8000000000000000 | 2 * v40; // 0x5f5d
        if (v42 > v43) {
            // 0x5f6c
            v33 = v42 - v43 + (int64_t)(v41 < v44);
            v31 = v41 - v44;
            goto lab_0x5f72;
        } else {
            // 0x5f65
            v33 = v42;
            v31 = v41;
            if (v41 < v44 || v42 != v43) {
                goto lab_0x5f72;
            } else {
                // 0x5f6c
                v33 = v42 - v43 + (int64_t)(v41 < v44);
                v31 = v41 - v44;
                goto lab_0x5f72;
            }
        }
    }
  lab_0x5f78:
    // 0x5f78
    v15 = v32;
    int64_t v92; // 0x5b60
    int64_t v93; // bp-376, 0x5b60
    int64_t v94; // 0x5c39
    int32_t v95; // 0x5cb8
    bool v96 = millerrabin2(&v93, v94, &v15, &v16, v95, (int64_t *)v92); // 0x5fa0
    v8 = 0;
    if (!v96) {
        goto lab_0x6015;
    }
    // 0x5fa9
    char * v97; // 0x5b60
    char * v98 = (char *)((int64_t)v97 + 1); // 0x5fa9
    char * v99 = v98; // 0x5fc1
    if (v97 != (char *)((int64_t)&g1 - 1)) {
        goto lab_0x5d90;
    } else {
        // 0x5fc7
        function_2700();
        function_2a20();
        function_25f0();
        v84 = v98;
        int64_t v100; // 0x5b60
        v78 = v100;
        int64_t v101; // 0x5b60
        v76 = v101;
        v82 = v92;
        int64_t v102; // 0x5b60
        v74 = v102;
        int32_t v103; // 0x5b60
        v80 = v103;
        v70 = v43;
        v68 = v44;
        v71 = (int32_t)"Lucas prime test failure.  This should not happen" ^ (int32_t)"Lucas prime test failure.  This should not happen";
        uint64_t v104; // 0x5f2a
        v66 = v104;
        goto lab_0x5ff0;
    }
  lab_0x5ff0:
    // 0x5ff0
    v72 = v71;
    v83 = v84;
    v77 = v78;
    v75 = v76;
    v81 = v82;
    v73 = v74;
    v79 = v80;
    v69 = v70;
    v67 = v68;
    v65 = v66;
    goto lab_0x5e70;
  lab_0x5e70:;
    int64_t v105 = v67;
    int32_t v106 = v79;
    int64_t v107 = powm2((int64_t *)v69, &v15, &v72, &v93, v94, (int64_t *)v65); // 0x5e84
    int64_t factors; // bp-336, 0x5b60
    if (v107 == v81) {
        int64_t v108 = na[0]; // 0x5ef8
        v90 = factors != v108;
        if (v106 > (int32_t)v105) {
            // 0x5f0a
            if (factors != v108) {
                goto lab_0x5ea2;
            } else {
                goto lab_0x5f21;
            }
        } else {
            goto lab_0x613e;
        }
    } else {
        if (v106 <= (int32_t)v105) {
            // break -> 0x60f8
            goto lab_0x60f8;
        }
        goto lab_0x5ea2;
    }
  lab_0x5ea2:
    // 0x5ea2
    v61 = v83;
    v59 = v77;
    v57 = v75;
    v55 = v81;
    v53 = v73;
    v51 = v106;
    v49 = v69;
    v47 = v105 + 1;
    v45 = v65;
    goto lab_0x5ea6;
  lab_0x5c7a:
    // 0x5c7a
    v13 = v12;
    v7 = v6;
    v5 = v7;
    v9 = v10 - 1;
    v11 = v13;
    if (v10 == 0) {
        // break -> 0x5c80
        goto lab_0x5c80;
    }
    goto lab_0x5c58;
  lab_0x5cb8:
    // 0x5cb8
    v94 = (2 - v18 * n0) * v18;
    v95 = *(int32_t *)&q;
    v93 = n0;
    v91 = millerrabin2(&v93, v94, &v15, &v16, v95, &v14);
    v8 = 0;
    if (v91) {
        // 0x5d0f
        factor(v19, v20, (int32_t *)&v63);
        int64_t v109 = &factors;
        int32_t v110; // 0x5b60
        int32_t v111 = v110 % 256;
        v99 = "\x01\x02\x02\x04\x02\x04\x02\x04\x06\x02\x06\x04\x02\x04\x06\x06\x02\x06\x04\x02\x06\x04\x06\b\x04\x02\x04\x02\x04\x0e\x04\x06\x02\n\x02\x06\x06\x04\x06\x06\x02\n\x02\x04\x02\f\f\x04\x02\x04\x06\x02\n\x06\x06\x06\x02\x06\x04\x02\n\x0e\x04\x02\x04\x0e\x06\n\x02\x04\x06\b\x06\x06\x04\x06\b\x04\b\n\x02\n\x02\x06\x04\x06\b\x04\x02\x04\f\b\x04\b\x04\x06\f\x02\x12\x06\n\x06\x06\x02\x06\n\x06\x06\x02\x06\x06\x04\x02\f\n\x02\x04\x06\x06\x02\f\x04\x06\b\n\b\n\b\x06\x06\x04\b\x06\x04\b\x04\x0e\n\f\x02\n\x02\x04\x02\n\x0e\x04\x02\x04\x0e\x04\x02\x04\x14\x04\b\n\b\x04\x06\x06\x0e\x04\x06\x06\b\x06\f\x04\x06\x02\n\x02\x06\n\x02\n\x02\x06\x12\x04\x02\x04\x06\x06\b\x06\x06\x16\x02\n\b\n\x06\x06\b\f\x04\x06\x06\x02\x06\f\n\x12\x02\x04\x06\x02\x06\x04\x02\x04\f\x02\x06\"\x06\x06\b\x12\n\x0e\x04\x02\x04\x06\b\x04\x02\x06\f\n\x02\x04\x02\x04\x06\f\f\b\f\x06\x04\x06\b\x04\b\x04\x0e\x04\x06\x02\x04\x06\x02\x06\n\x14\x06\x04\x02\x18\x04\x02\n\f\x02\n\b\x06\x06\x06\x12\x06\x04\x02\f\n\f\b\x10\x0e\x06\x04\x02\x04\x02\n\f\x06\x06\x12\x02\x10\x02\x16\x06\b\x06\x04\x02\x04\b\x06\n\x02\n\x0e\n\x06\f\x02\x04\x02\n\f\x02\x10\x02\x06\x04\x02\n\b\x12\x18\x04\x06\b\x10\x02\x04\b\x10\x02\x04\b\x06\x06\x04\f\x02\x16\x06\x02\x06\x04\x06\x0e\x06\x04\x02\x06\x04\x06\f\x06\x06\x0e\x04\x06\f\b\x06\x04\x1a\x12\n\b\x04\x06\x02\x06\x16\f\x02\x10\b\x04\f\x0e\n\x02\x04\b\x06\x06\x04\x02\x04\x06\b\x04\x02\x06\n\x02\n\b\x04\x0e\n\f\x02\x06\x04\x02\x10\x0e\x04\x06\b\x06\x04\x12\b\n\x06\x06\b\n\f\x0e\x04\x06\x06\x02\x1c\x02\n\b\x04\x0e\x04\b\f\x06\f\x04\x06\x14\n\x02\x10\x1a\x04\x02\f\x06\x04\f\x06\b\x04\b\x16\x02\x04\x02\f\x1c\x02\x06\x06\x06\x04\x06\x02\f\x04\f\x02\n\x02\x10\x02\x10\x06\x14\x10\b\x04\x02\x04\x02\x16\b\f\x06\n\x02\x04\x06\x02\x06\n\x02\f\n\x02\n\x0e\x06\x04\x06\b\x06\x06\x10\f\x02\x04\x0e\x06\x04\b\n\b\x06\x06\x16\x06\x02\n\x0e\x04\x06\x12\x02\n\x0e\x04\x02\n\x0e\x04\b\x12\x04\x06\x02\x04\x06\x02\f\x04\x14\x16\f\x02\x04\x06\x06\x02\x06\x16\x02\x06\x10\x06\f\x02\x06\f\x10\x02\x04\x06\x0e\x04\x02\x12\x18\n\x06\x02\n\x02\n\x02\n\x06\x02\n\x02\n\x06\b\x1e\n\x02\n\b\x06\n\x12\x06\f\f\x02\x12\x06\x04\x06\x06\x12\x02\n\x0e\x06\x04\x02\x04\x18\x02\f\x06\x10\b\x06\x06\x12\x10\x02\x04\x06\x02\x06\x06\n\x06\f\f\x12\x02\x06\x04\x12\b\x18\x04\x02\x04\x06\x02\f\x04\x0e\x1e\n\x06\f\x0e\x06\n\f\x02\x04\x06\b\x06\n\x02\x04\x0e\x06\x06\x00\x00\x00\x00\x00\x00";
        while (true) {
          lab_0x5d90:;
            int64_t v112 = 2;
            int64_t v113 = &v14;
            char * v114 = v99;
            if (v17 != 0) {
                uint64_t v115 = (int64_t)v63; // 0x6040
                unsigned char v116 = *(char *)(v115 / 2 % 128 + (int64_t)"\x01\xab\xcd\xb7\x39\xa3\xc5\xef\xf1\x1b=\xa7)\x13\x35\xdf\xe1\x8b\xad\x97\x19\x83\xa5\xcf\xd1\xfb\x1d\x87\t\xf3\x15\xbf\xc1k\x8dw\xf9\x63\x85\xaf\xb1\xdb\xfdg\xe9\xd3\xf5\x9f\xa1KmW\xd9\x43\x65\x8f\x91\xbb\xddG\xc9\xb3\xd5\x7f\x81+M7\xb9#Eoq\x9b\xbd'\xa9\x93\xb5_a\v-\x17\x99\x03%OQ{\x9d\a\x89s\x95?A\xeb\r\xf7y\xe3\x05/1[}\xe7iSu\x1f!\xcb\xed\xd7Y\xc3\xe5\x0f\x11;]\xc7I3U\xff"); // 0x606f
                int64_t v117 = v116; // 0x606f
                int64_t v118 = (2 - v117 * v115) * v117; // 0x608c
                int64_t v119 = (2 - v118 * v115) * v118; // 0x609b
                v72 = v119 * v20 * (2 - v119 * v115);
                int64_t v120 = powm2(&factors, &v15, &v72, &v93, v94, (int64_t *)v113); // 0x60c6
                int64_t v121 = v14; // 0x60cb
                if (v120 == v121) {
                    int64_t v122 = na[0]; // 0x6195
                    if ((char)v110 == 0) {
                        // 0x61b7
                        v90 = factors != v122;
                        goto lab_0x613e;
                    } else {
                        // 0x619f
                        v61 = v114;
                        v59 = v112;
                        v57 = n1;
                        v55 = v120;
                        v53 = n0;
                        v51 = v111;
                        v49 = v109;
                        v47 = 1;
                        v45 = v113;
                        if (factors == v122) {
                            goto lab_0x5f21;
                        } else {
                            goto lab_0x5ea6;
                        }
                    }
                } else {
                    // 0x60e6
                    v61 = v114;
                    v59 = v112;
                    v57 = n1;
                    v55 = v121;
                    v53 = n0;
                    v51 = v111;
                    v49 = v109;
                    v47 = 1;
                    v45 = v113;
                    if ((char)v110 == 0) {
                        // break -> 0x60f8
                        break;
                    }
                    goto lab_0x5ea6;
                }
            } else {
                if ((char)v110 == 0) {
                    // break -> 0x60f8
                    break;
                }
                // 0x5daa
                v61 = v114;
                v59 = v112;
                v57 = n1;
                v55 = v14;
                v53 = n0;
                v51 = v111;
                v49 = v109;
                v47 = 1;
                v45 = v113;
                goto lab_0x5ea6;
            }
        }
      lab_0x60f8:
        // 0x60f8
        v8 = v91 ? 255 : 0;
    }
    goto lab_0x6015;
}