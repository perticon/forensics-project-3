ulong prime2_p(ulong param_1,ulong param_2)

{
  ulong uVar1;
  byte bVar2;
  uint uVar3;
  ulong uVar4;
  ulong uVar5;
  ulong uVar6;
  undefined8 uVar7;
  long lVar8;
  long lVar9;
  long lVar10;
  ulong uVar11;
  ulong uVar12;
  long in_FS_OFFSET;
  bool bVar13;
  undefined1 *puStack552;
  ulong *puStack472;
  int iStack432;
  ulong uStack424;
  ulong uStack416;
  ulong uStack408;
  ulong uStack400;
  ulong uStack392;
  ulong uStack384;
  ulong uStack376;
  ulong uStack368;
  ulong uStack360;
  ulong uStack352;
  ulong uStack344;
  ulong uStack336;
  ulong uStack328;
  ulong auStack320 [30];
  byte bStack78;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_1 == 0) {
    if (param_2 < 2) goto LAB_00106013;
    if (0x17ded78 < param_2) {
      if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
        uVar6 = prime_p_part_0(param_2);
        return uVar6;
      }
      goto LAB_001061c2;
    }
    uVar6 = 1;
  }
  else {
    uVar4 = param_1 - (param_2 == 0);
    uVar11 = param_2 - 1;
    if (uVar11 == 0) {
      lVar10 = 0;
      if (uVar4 != 0) {
        for (; (uVar4 >> lVar10 & 1) == 0; lVar10 = lVar10 + 1) {
        }
      }
      iStack432 = (int)lVar10 + 0x40;
      uStack424 = uVar4 >> ((byte)lVar10 & 0x3f);
      uStack416 = 0;
    }
    else {
      lVar10 = 0;
      if (uVar11 != 0) {
        for (; (uVar11 >> lVar10 & 1) == 0; lVar10 = lVar10 + 1) {
        }
      }
      iStack432 = (int)lVar10;
      bVar2 = (byte)lVar10;
      uStack416 = uVar4 >> (bVar2 & 0x3f);
      uStack424 = uVar4 << (0x40 - bVar2 & 0x3f) | uVar11 >> (bVar2 & 0x3f);
    }
    lVar8 = 0x3f;
    uVar6 = (ulong)(byte)binvert_table[(uint)(param_2 >> 1) & 0x7f];
    lVar10 = uVar6 * 2 - uVar6 * uVar6 * param_2;
    lVar10 = lVar10 * 2 - lVar10 * lVar10 * param_2;
    lVar10 = lVar10 * 2 - lVar10 * lVar10 * param_2;
    bVar13 = param_1 == 1;
    if (bVar13) {
      lVar8 = 0x7f;
    }
    uStack384 = (ulong)!bVar13;
    uVar6 = (ulong)bVar13;
    do {
      uStack392 = uVar6 * 2;
      uStack384 = uStack384 * 2 | uVar6 >> 0x3f;
      if ((param_1 < uStack384) || ((param_1 == uStack384 && (param_2 <= uStack392)))) {
        bVar13 = uStack392 < param_2;
        uStack392 = uStack392 - param_2;
        uStack384 = (uStack384 - param_1) - (ulong)bVar13;
      }
      bVar13 = lVar8 != 0;
      lVar8 = lVar8 + -1;
      uVar6 = uStack392;
    } while (bVar13);
    uStack408 = uStack392 * 2;
    uStack400 = uStack384 * 2 + (ulong)CARRY8(uStack392,uStack392);
    if ((param_1 < uStack400) || ((uStack400 == param_1 && (param_2 <= uStack408)))) {
      bVar13 = uStack408 < param_2;
      uStack408 = uStack408 - param_2;
      uStack400 = (uStack400 - param_1) - (ulong)bVar13;
    }
    uStack376 = param_2;
    uStack368 = param_1;
    bVar2 = millerrabin2(&uStack376,lVar10,&uStack408,&uStack424,iStack432,&uStack392);
    if (bVar2 == 0) {
LAB_00106013:
      uVar6 = 0;
    }
    else {
      factor(uVar4,uVar11,&uStack328);
      uVar12 = 2;
      puStack552 = primes_diff;
      while (auStack320[0] != 0) {
        uStack352 = 0;
        uVar6 = (ulong)(byte)binvert_table[(uint)(uStack328 >> 1) & 0x7f];
        lVar8 = uVar6 * 2 - uVar6 * uVar6 * uStack328;
        lVar8 = lVar8 * 2 - lVar8 * lVar8 * uStack328;
        uStack360 = uVar11 * (lVar8 * 2 - lVar8 * lVar8 * uStack328);
        uStack344 = powm2(&uStack336,&uStack408,&uStack360,&uStack376,lVar10);
        if (uStack344 != uStack392) break;
        if (bStack78 == 0) {
          bVar13 = uStack336 != uStack384;
LAB_0010613e:
          if (bVar13) {
            uVar6 = (ulong)bVar2;
            goto LAB_00106015;
          }
        }
        else if (uStack336 != uStack384) goto LAB_00105db7;
LAB_00105f21:
        uVar12 = uVar12 + (byte)*puStack552;
        uStack400 = 0;
        if (uVar12 < param_1) {
          uStack400 = uVar12;
        }
        lVar8 = (-(ulong)(uVar12 < param_1) & 0xffffffffffffffc0) + 0x7f;
        uVar6 = 0;
        if (param_1 <= uVar12) {
          uVar6 = uVar12;
        }
        do {
          uStack408 = uVar6 * 2;
          uStack400 = uStack400 * 2 | uVar6 >> 0x3f;
          if ((param_1 < uStack400) || ((param_1 == uStack400 && (param_2 <= uStack408)))) {
            bVar13 = uStack408 < param_2;
            uStack408 = uStack408 - param_2;
            uStack400 = (uStack400 - param_1) - (ulong)bVar13;
          }
          bVar13 = lVar8 != 0;
          lVar8 = lVar8 + -1;
          uVar6 = uStack408;
        } while (bVar13);
        uVar6 = millerrabin2(&uStack376,lVar10,&uStack408,&uStack424,iStack432);
        if ((char)uVar6 == '\0') goto LAB_00106015;
        puStack552 = puStack552 + 1;
        if (puStack552 == primes_diff + 0x29c) {
          uVar7 = dcgettext(0,"Lucas prime test failure.  This should not happen",5);
          error(0,0,uVar7);
                    /* WARNING: Subroutine does not return */
          abort();
        }
      }
      if (bStack78 != 0) {
LAB_00105db7:
        uVar6 = uStack392;
        puStack472 = &uStack336;
        lVar8 = 1;
        uVar3 = (uint)bStack78;
        do {
          uVar1 = auStack320[lVar8];
          uStack360 = uVar4 << 0x3f | uVar11 >> 1;
          uStack352 = uVar4 >> 1;
          if (uVar1 != 2) {
            uVar5 = (ulong)(byte)binvert_table[(uint)(uVar1 >> 1) & 0x7f];
            lVar9 = uVar5 * 2 - uVar5 * uVar5 * uVar1;
            lVar9 = lVar9 * 2 - lVar9 * lVar9 * uVar1;
            lVar9 = lVar9 * 2 - lVar9 * lVar9 * uVar1;
            uStack360 = uVar11 * lVar9;
            if (uVar4 < uVar1) {
              uStack352 = 0;
            }
            else {
              uStack352 = (uVar4 - SUB168(ZEXT816(uStack360) * ZEXT816(uVar1) >> 0x40,0)) * lVar9;
            }
          }
          uStack344 = powm2(puStack472,&uStack408,&uStack360,&uStack376,lVar10,&uStack392);
          if (uStack344 == uVar6) {
            if (uVar3 <= (uint)lVar8) {
              bVar13 = uStack336 != uStack384;
              goto LAB_0010613e;
            }
            if (uStack336 == uStack384) goto LAB_00105f21;
          }
          else if (uVar3 <= (uint)lVar8) break;
          lVar8 = lVar8 + 1;
        } while( true );
      }
      uVar6 = (ulong)bVar2;
    }
  }
LAB_00106015:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar6;
  }
LAB_001061c2:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}