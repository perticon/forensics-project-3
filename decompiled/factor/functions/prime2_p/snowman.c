unsigned char prime2_p(void** rdi, void** rsi) {
    void** r15_3;
    void*** rsp4;
    void** rax5;
    void** v6;
    void** rbx7;
    uint64_t rax8;
    uint64_t v9;
    uint32_t v10;
    int64_t rcx11;
    uint64_t rcx12;
    uint64_t rax13;
    int64_t rax14;
    void* rax15;
    void* rdx16;
    void* rax17;
    void** rdx18;
    void** rbp19;
    uint1_t zf20;
    void** rax21;
    void** rsi22;
    uint1_t cf23;
    int1_t cf24;
    void** v25;
    void** v26;
    void** tmp64_27;
    void** rax28;
    void** r13_29;
    struct s43* r12_30;
    void** r14_31;
    void** rcx32;
    void** r9_33;
    void** v34;
    unsigned char al35;
    unsigned char v36;
    void* v37;
    int64_t v38;
    int64_t v39;
    void** v40;
    void** v41;
    uint32_t eax42;
    void* rax43;
    void** rdi44;
    struct s0* r14_45;
    void** rbp46;
    struct s0** rsp47;
    void** rax48;
    void** v49;
    uint32_t eax50;
    struct s0* v51;
    void* rdx52;
    void** r13_53;
    void* rsp54;
    void** rax55;
    void** v56;
    uint32_t eax57;
    uint64_t r12_58;
    void* rax59;
    void** r13_60;
    void** rdx61;
    int32_t eax62;
    void** rsi63;
    void** r14_64;
    void** rbp65;
    int64_t rax66;
    void*** rdx67;
    void*** rbp68;
    uint64_t rdi69;
    void** rdx70;
    uint64_t rax71;
    uint32_t eax72;
    void** tmp64_73;
    void** rbp74;
    void* rax75;
    void** tmp64_76;
    int1_t zf77;
    int1_t zf78;
    void* rsp79;
    void** r12_80;
    void** rdx81;
    signed char v82;
    void** rbp83;
    uint32_t ebx84;
    signed char v85;
    unsigned char v86;
    void** v87;
    int1_t zf88;
    uint32_t eax89;
    unsigned char v90;
    void** rdi91;
    unsigned char v92;
    int64_t v93;
    void** v94;
    void** v95;
    void* rdx96;
    void** r14_97;
    void** rdi98;
    void** rdx99;
    void** rsp100;
    void** rdi101;
    void** rax102;
    void** rsi103;
    void** r13_104;
    void** rax105;
    int64_t v106;
    void** v107;
    void** v108;
    void** rdi109;
    void** rax110;
    void** r15_111;
    uint64_t rbp112;
    void* rbx113;
    int64_t* v114;
    void** rdi115;
    void** rax116;
    void** rdi117;
    void** v118;
    int1_t zf119;
    void** rdx120;
    void*** v121;
    void** rax122;
    void** v123;
    void** rdi124;
    uint64_t rdi125;
    void** v126;
    uint64_t v127;
    uint32_t v128;
    void** rcx129;
    void** r15_130;
    uint64_t rax131;
    int32_t esi132;
    int64_t rax133;
    void* rax134;
    void* rdx135;
    void* rax136;
    void** rdx137;
    struct s0* r13_138;
    void** rax139;
    uint64_t rdi140;
    uint1_t cf141;
    int64_t rbx142;
    void** r8_143;
    void** r9_144;
    void** rbx145;
    unsigned char al146;
    unsigned char v147;
    void* v148;
    struct s0* v149;
    uint32_t eax150;
    unsigned char v151;
    uint32_t v152;
    int64_t rax153;
    void* v154;
    uint64_t r12_155;
    void** rax156;
    void* rax157;
    uint32_t eax158;
    void** v159;
    void** r12_160;
    void** rcx161;
    int32_t esi162;
    void** rax163;
    void* rdx164;
    void* rdi165;
    uint1_t cf166;
    int64_t rbp167;
    int64_t r15_168;
    int64_t r14_169;
    void** rbp170;
    void** rcx171;
    void** r11_172;
    uint64_t rax173;
    int64_t rax174;
    void* rax175;
    void* rdx176;
    void* rax177;
    void** r8_178;
    void* r10_179;
    void* r9_180;
    void* rax181;
    void** r13_182;
    int64_t rax183;
    int64_t rax184;
    void** rdi185;
    void** rax186;
    int64_t rcx187;
    void* rdi188;
    void* rax189;
    void* r11_190;
    void* rsi191;
    void* rax192;
    void** rbx193;
    void** rsi194;
    void** rax195;
    void** rcx196;
    void** r11_197;
    void** rdx198;
    void** rax199;
    void** r8_200;
    signed char al201;
    void** rdx202;
    void** rsi203;
    void** rcx204;
    int1_t zf205;
    signed char al206;
    int64_t rax207;
    void** r8_208;
    void*** r11_209;
    signed char* r10_210;
    uint32_t r9d211;
    int32_t ebx212;
    struct s44* rcx213;
    struct s44* rax214;
    int64_t rsi215;
    struct s45* rax216;
    uint32_t r9d217;
    uint32_t eax218;
    void** rax219;
    uint64_t rax220;
    void** rax221;
    void** rdx222;
    void** rcx223;
    int32_t esi224;
    void** rax225;
    uint64_t rdi226;
    uint1_t cf227;
    unsigned char al228;
    void* r11_229;
    uint64_t v230;
    void** rax231;
    void** v232;
    uint32_t v233;
    void** v234;
    void** v235;
    void** v236;
    unsigned char al237;
    void** v238;
    void** rdx239;
    void* rax240;
    void** rax241;
    uint64_t rcx242;
    void** rsi243;
    uint1_t cf244;
    int1_t cf245;
    void** v246;
    void** rax247;
    signed char v248;
    void** v249;
    signed char v250;
    unsigned char v251;
    signed char v252;

    r15_3 = rsi;
    rsp4 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1f8);
    rax5 = g28;
    v6 = rax5;
    if (rdi) {
        rbx7 = rdi;
        rax8 = reinterpret_cast<unsigned char>(rdi) - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi) < reinterpret_cast<unsigned char>(static_cast<int64_t>(reinterpret_cast<unsigned char>(static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rsi) < reinterpret_cast<unsigned char>(1))))))));
        v9 = rax8;
        if (!(rsi - 1)) {
            __asm__("bsf rcx, rax");
            v10 = static_cast<uint32_t>(rcx11 + 64);
        } else {
            __asm__("bsf rax, rdx");
            v10 = *reinterpret_cast<uint32_t*>(&rax8);
        }
        *reinterpret_cast<int32_t*>(&rcx12) = 63;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx12) + 4) = 0;
        rax13 = reinterpret_cast<unsigned char>(r15_3) >> 1;
        *reinterpret_cast<uint32_t*>(&rax14) = *reinterpret_cast<uint32_t*>(&rax13) & 0x7f;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax15) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax14);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
        rdx16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax15) + reinterpret_cast<int64_t>(rax15) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax15) * reinterpret_cast<int64_t>(rax15) * reinterpret_cast<unsigned char>(r15_3)));
        rax17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx16) + reinterpret_cast<uint64_t>(rdx16) - reinterpret_cast<uint64_t>(rdx16) * reinterpret_cast<uint64_t>(rdx16) * reinterpret_cast<unsigned char>(r15_3));
        *reinterpret_cast<int32_t*>(&rdx18) = 0;
        *reinterpret_cast<int32_t*>(&rdx18 + 4) = 0;
        rbp19 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax17) + reinterpret_cast<uint64_t>(rax17) - reinterpret_cast<uint64_t>(rax17) * reinterpret_cast<uint64_t>(rax17) * reinterpret_cast<unsigned char>(r15_3));
        zf20 = reinterpret_cast<uint1_t>(rbx7 == 1);
        if (zf20) {
            rcx12 = 0x7f;
        }
        *reinterpret_cast<unsigned char*>(&rdx18) = zf20;
        *reinterpret_cast<uint32_t*>(&rax21) = reinterpret_cast<uint1_t>(!zf20);
        *reinterpret_cast<int32_t*>(&rax21 + 4) = 0;
        do {
            rsi22 = rdx18;
            rdx18 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx18) + reinterpret_cast<unsigned char>(rdx18));
            rax21 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax21) + reinterpret_cast<unsigned char>(rax21)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rsi22) >> 63));
            if (reinterpret_cast<unsigned char>(rbx7) < reinterpret_cast<unsigned char>(rax21) || rbx7 == rax21 && reinterpret_cast<unsigned char>(r15_3) <= reinterpret_cast<unsigned char>(rdx18)) {
                cf23 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx18) < reinterpret_cast<unsigned char>(r15_3));
                rdx18 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx18) - reinterpret_cast<unsigned char>(r15_3));
                rax21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax21) - (reinterpret_cast<unsigned char>(rbx7) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax21) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbx7) + static_cast<uint64_t>(cf23))))));
            }
            cf24 = rcx12 < 1;
            --rcx12;
        } while (!cf24);
        v25 = rax21;
        v26 = rdx18;
        tmp64_27 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx18) + reinterpret_cast<unsigned char>(rdx18));
        rax28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax21) + reinterpret_cast<unsigned char>(rax21) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_27) < reinterpret_cast<unsigned char>(rdx18))));
        if (reinterpret_cast<unsigned char>(rax28) > reinterpret_cast<unsigned char>(rbx7) || rax28 == rbx7 && reinterpret_cast<unsigned char>(tmp64_27) >= reinterpret_cast<unsigned char>(r15_3)) {
        }
        r13_29 = reinterpret_cast<void**>(rsp4 + 0xa0);
        r12_30 = reinterpret_cast<struct s43*>(rsp4 + 0x90);
        r14_31 = reinterpret_cast<void**>(rsp4 + 0xb0);
        rcx32 = reinterpret_cast<void**>(rsp4 + 0x80);
        r9_33 = r13_29;
        rdi = r14_31;
        v34 = rcx32;
        al35 = millerrabin2(rdi, rbp19, r12_30, rcx32, v10, r9_33);
        rsp4 = rsp4 - 8 + 8;
        v36 = al35;
        if (al35) {
            v37 = reinterpret_cast<void*>(rsp4 + 0xe0);
            factor();
            rsp4 = rsp4 - 8 + 8;
            r9_33 = r13_29;
            r13_29 = rbp19;
            rbp19 = r15_3;
            *reinterpret_cast<int32_t*>(&rdi) = 2;
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            r15_3 = reinterpret_cast<void**>(2);
            v38 = v39;
            v40 = reinterpret_cast<void**>(0x10340);
            v41 = reinterpret_cast<void**>(rsp4 + 0xc0);
            goto addr_5d90_15;
        }
    }
    if (reinterpret_cast<unsigned char>(rsi) > reinterpret_cast<unsigned char>(1)) {
        if (reinterpret_cast<unsigned char>(rsi) <= reinterpret_cast<unsigned char>(0x17ded78)) {
            eax42 = 1;
        } else {
            rax43 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v6) - reinterpret_cast<unsigned char>(g28));
            if (rax43) {
                fun_2740();
                goto addr_61d0_21;
            } else {
                rdi44 = rsi;
                r14_45 = reinterpret_cast<struct s0*>(rdi44 + 0xffffffffffffffff);
                rbp46 = rdi44;
                rsp47 = reinterpret_cast<struct s0**>(rsp4 + 0x1f8 + 8 + 8 + 8 + 8 + 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x148);
                rax48 = g28;
                v49 = rax48;
                eax50 = 0;
                v51 = r14_45;
                if (!(*reinterpret_cast<uint32_t*>(&rdi44) & 1)) 
                    goto addr_5703_24; else 
                    goto addr_5478_25;
            }
        }
    } else {
        eax42 = 0;
    }
    addr_6015_27:
    rdx52 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v6) - reinterpret_cast<unsigned char>(g28));
    if (!rdx52) {
        return *reinterpret_cast<unsigned char*>(&eax42);
    }
    addr_61d0_21:
    r13_53 = rdi;
    rsp54 = reinterpret_cast<void*>(rsp4 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x138);
    rax55 = g28;
    v56 = rax55;
    eax57 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi));
    if (*reinterpret_cast<signed char*>(&eax57) == 32) {
        do {
            eax57 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_53 + 1));
            ++r13_53;
        } while (*reinterpret_cast<signed char*>(&eax57) == 32);
    }
    *reinterpret_cast<int32_t*>(&r12_58) = 4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_58) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax59) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax57) == 43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax59) + 4) = 0;
    r13_60 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_53) + reinterpret_cast<uint64_t>(rax59));
    rdx61 = r13_60;
    do {
        eax62 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdx61));
        ++rdx61;
        if (!eax62) 
            break;
        *reinterpret_cast<int32_t*>(&r12_58) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_58) + 4) = 0;
    } while (eax62 - 48 <= 9);
    goto addr_623e_34;
    rsi63 = r13_60;
    *reinterpret_cast<int32_t*>(&r14_64) = 0;
    *reinterpret_cast<int32_t*>(&r14_64 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rbp65) = 0;
    *reinterpret_cast<int32_t*>(&rbp65 + 4) = 0;
    if (*reinterpret_cast<int32_t*>(&r12_58)) {
        addr_623e_34:
        quote();
        fun_2700();
        fun_2a20();
    } else {
        do {
            *reinterpret_cast<int32_t*>(&rax66) = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rsi63));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax66) + 4) = 0;
            ++rsi63;
            if (!*reinterpret_cast<int32_t*>(&rax66)) 
                break;
            *reinterpret_cast<uint32_t*>(&rcx32) = static_cast<uint32_t>(rax66 - 48);
            *reinterpret_cast<int32_t*>(&rcx32 + 4) = 0;
        } while (reinterpret_cast<unsigned char>(rbp65) <= reinterpret_cast<unsigned char>(0x1999999999999999) && (rdx67 = reinterpret_cast<void***>(r14_64 + reinterpret_cast<unsigned char>(r14_64) * 4), rbp68 = reinterpret_cast<void***>(rbp65 + reinterpret_cast<unsigned char>(rbp65) * 4), rdi69 = reinterpret_cast<unsigned char>(r14_64) >> 61, rdx70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdx67) + reinterpret_cast<uint64_t>(rdx67)), rax71 = reinterpret_cast<unsigned char>(r14_64) >> 63, eax72 = *reinterpret_cast<int32_t*>(&rax71) + *reinterpret_cast<int32_t*>(&rdi69) + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx70) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r14_64) + reinterpret_cast<unsigned char>(r14_64))), tmp64_73 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx70) + reinterpret_cast<unsigned char>(rcx32)), rdx61 = tmp64_73, rbp74 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbp68) + reinterpret_cast<uint64_t>(rbp68)), r14_64 = rdx61, *reinterpret_cast<uint32_t*>(&rcx32) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_73) < reinterpret_cast<unsigned char>(rdx70)), *reinterpret_cast<int32_t*>(&rcx32 + 4) = 0, *reinterpret_cast<uint32_t*>(&rax75) = eax72 + *reinterpret_cast<uint32_t*>(&rcx32), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax75) + 4) = 0, tmp64_76 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp74) + reinterpret_cast<uint64_t>(rax75)), rbp65 = tmp64_76, reinterpret_cast<unsigned char>(tmp64_76) >= reinterpret_cast<unsigned char>(rbp74)));
        goto addr_62ff_39;
        if (reinterpret_cast<signed char>(rbp65) < reinterpret_cast<signed char>(0)) {
            addr_62ff_39:
            zf77 = dev_debug == 0;
            if (!zf77) {
                rcx32 = stderr;
                fun_2a80("[using arbitrary-precision arithmetic] ", 1, 39, rcx32, 0x1999999999999999, r9_33);
                rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
                goto addr_630c_42;
            }
        } else {
            zf78 = dev_debug == 0;
            if (!zf78) {
                rcx32 = stderr;
                *reinterpret_cast<int32_t*>(&rdx61) = 36;
                *reinterpret_cast<int32_t*>(&rdx61 + 4) = 0;
                fun_2a80("[using single-precision arithmetic] ", 1, 36, rcx32, 0x1999999999999999, r9_33);
                rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
            }
            if (!rbp65) {
                lbuf_putint(r14_64, 0, rdx61, rcx32, r14_64, 0, rdx61, rcx32);
                rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
            } else {
                print_uintmaxes_part_0(rbp65, r14_64, rdx61, rcx32, rbp65, r14_64, rdx61, rcx32);
                rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
            }
            r12_80 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp79) + 32);
            lbuf_putc(58, 58);
            rdx81 = r12_80;
            factor();
            if (v82) {
                rbp83 = r12_80;
                while (1) {
                    ebx84 = 0;
                    if (!v85) {
                        addr_654c_51:
                        ++rbp83;
                        *reinterpret_cast<uint32_t*>(&rdx81) = v86;
                        *reinterpret_cast<int32_t*>(&rdx81 + 4) = 0;
                        if (*reinterpret_cast<uint32_t*>(&rdx81) > reinterpret_cast<uint32_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&rbp83)) - reinterpret_cast<int32_t>(*reinterpret_cast<void**>(&r12_80)))) 
                            continue; else 
                            break;
                    } else {
                        do {
                            lbuf_putc(32, 32);
                            lbuf_putint(v87, 0, rdx81, rcx32, v87, 0, rdx81, rcx32);
                            zf88 = print_exponents == 0;
                            eax89 = v90;
                            if (zf88) 
                                continue;
                            if (*reinterpret_cast<unsigned char*>(&eax89) > 1) 
                                break;
                            ++ebx84;
                        } while (ebx84 < eax89);
                        goto addr_654c_51;
                    }
                    lbuf_putc(94);
                    *reinterpret_cast<uint32_t*>(&rdi91) = v92;
                    *reinterpret_cast<int32_t*>(&rdi91 + 4) = 0;
                    lbuf_putint(rdi91, 0, rdx81, rcx32, rdi91, 0, rdx81, rcx32);
                    goto addr_654c_51;
                }
            }
            if (v93) {
                lbuf_putc(32, 32);
                if (!v94) {
                    lbuf_putint(v95, 0, rdx81, rcx32, v95, 0, rdx81, rcx32);
                } else {
                    print_uintmaxes_part_0(v94, v95, rdx81, rcx32, v94, v95, rdx81, rcx32);
                }
            }
            lbuf_putc(10, 10);
        }
    }
    addr_626c_62:
    rdx96 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v56) - reinterpret_cast<unsigned char>(g28));
    if (rdx96) {
        fun_2740();
    } else {
        goto v40;
    }
    addr_630c_42:
    r14_97 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp54) + 32);
    fun_2a50(r14_97, r13_60, 10, rcx32);
    rdi98 = stdout;
    rdx99 = r14_97;
    fun_2800(rdi98, 10, rdx99, rcx32);
    rsp100 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp54) - 8 + 8 - 8 + 8);
    rdi101 = stdout;
    rax102 = *reinterpret_cast<void***>(rdi101 + 40);
    if (reinterpret_cast<unsigned char>(rax102) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi101 + 48))) {
        fun_27c0();
        rsp100 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp100 - 8) + 8);
    } else {
        rdx99 = rax102 + 1;
        *reinterpret_cast<void***>(rdi101 + 40) = rdx99;
        *reinterpret_cast<void***>(rax102) = reinterpret_cast<void**>(58);
    }
    rsi103 = rsp100;
    *reinterpret_cast<int32_t*>(&r13_104) = 0;
    *reinterpret_cast<int32_t*>(&r13_104 + 4) = 0;
    mp_factor(r14_97, rsi103, rdx99, rcx32, 0x1999999999999999, r9_33);
    *reinterpret_cast<int32_t*>(&rax105) = 0;
    *reinterpret_cast<int32_t*>(&rax105 + 4) = 0;
    if (!v106) {
        addr_645c_69:
        fun_25e0(v107, rsi103, v107, rsi103);
        fun_25e0(v108, rsi103, v108, rsi103);
        fun_2940(r14_97, rsi103, rdx99, rcx32, 0x1999999999999999, r9_33);
        rdi109 = stdout;
        rax110 = *reinterpret_cast<void***>(rdi109 + 40);
        if (reinterpret_cast<unsigned char>(rax110) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi109 + 48))) {
            *reinterpret_cast<int32_t*>(&rsi103) = 10;
            *reinterpret_cast<int32_t*>(&rsi103 + 4) = 0;
            fun_27c0();
        } else {
            rdx99 = rax110 + 1;
            *reinterpret_cast<void***>(rdi109 + 40) = rdx99;
            *reinterpret_cast<void***>(rax110) = reinterpret_cast<void**>(10);
        }
    } else {
        do {
            *reinterpret_cast<int32_t*>(&r15_111) = 0;
            *reinterpret_cast<int32_t*>(&r15_111 + 4) = 0;
            rbp112 = reinterpret_cast<unsigned char>(rax105) * 8;
            rbx113 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax105) << 4);
            if (v114[reinterpret_cast<unsigned char>(rax105)]) {
                do {
                    rdi115 = stdout;
                    rax116 = *reinterpret_cast<void***>(rdi115 + 40);
                    if (reinterpret_cast<unsigned char>(rax116) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi115 + 48))) {
                        *reinterpret_cast<void***>(rdi115 + 40) = rax116 + 1;
                        *reinterpret_cast<void***>(rax116) = reinterpret_cast<void**>(32);
                    } else {
                        fun_27c0();
                    }
                    rdi117 = stdout;
                    *reinterpret_cast<int32_t*>(&rsi103) = 10;
                    *reinterpret_cast<int32_t*>(&rsi103 + 4) = 0;
                    fun_2800(rdi117, 10, reinterpret_cast<unsigned char>(v118) + reinterpret_cast<uint64_t>(rbx113), rcx32);
                    zf119 = print_exponents == 0;
                    rdx120 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(v121) + rbp112);
                    if (zf119) 
                        continue;
                    if (reinterpret_cast<unsigned char>(rdx120) > reinterpret_cast<unsigned char>(1)) 
                        break;
                    *reinterpret_cast<int32_t*>(&rax122) = static_cast<int32_t>(reinterpret_cast<uint64_t>(r15_111 + 1));
                    *reinterpret_cast<int32_t*>(&rax122 + 4) = 0;
                    r15_111 = rax122;
                } while (reinterpret_cast<unsigned char>(rax122) < reinterpret_cast<unsigned char>(rdx120));
                continue;
            } else {
                continue;
            }
            rsi103 = reinterpret_cast<void**>("^%lu");
            fun_29b0(1, "^%lu", rdx120, rcx32);
            rdx99 = v123;
            *reinterpret_cast<int32_t*>(&rax105) = static_cast<int32_t>(reinterpret_cast<uint64_t>(r13_104 + 1));
            *reinterpret_cast<int32_t*>(&rax105 + 4) = 0;
            r13_104 = rax105;
        } while (reinterpret_cast<unsigned char>(rax105) < reinterpret_cast<unsigned char>(rdx99));
        if (!rdx99) 
            goto addr_645c_69; else 
            goto addr_6435_84;
    }
    rdi124 = stdout;
    fun_2ac0(rdi124, rsi103, rdx99, rcx32);
    goto addr_626c_62;
    addr_6435_84:
    *reinterpret_cast<int32_t*>(&rdi125) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi125) + 4) = 0;
    do {
        fun_2940((rdi125 << 4) + reinterpret_cast<unsigned char>(v126), rsi103, rdx99, rcx32, 0x1999999999999999, r9_33);
        *reinterpret_cast<int32_t*>(&rdi125) = static_cast<int32_t>(r12_58 + 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi125) + 4) = 0;
        r12_58 = rdi125;
    } while (rdi125 < v127);
    goto addr_645c_69;
    addr_5703_24:
    v128 = 0;
    r14_45 = v51;
    addr_5490_87:
    rcx129 = rbp46;
    *reinterpret_cast<int32_t*>(&r15_130) = 0;
    *reinterpret_cast<int32_t*>(&r15_130 + 4) = 0;
    rax131 = reinterpret_cast<unsigned char>(rbp46) >> 1;
    esi132 = 64;
    *reinterpret_cast<uint32_t*>(&rax133) = *reinterpret_cast<uint32_t*>(&rax131) & 0x7f;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax133) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax134) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax133);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax134) + 4) = 0;
    rdx135 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax134) + reinterpret_cast<int64_t>(rax134) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax134) * reinterpret_cast<int64_t>(rax134) * reinterpret_cast<unsigned char>(rbp46)));
    rax136 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx135) + reinterpret_cast<uint64_t>(rdx135) - reinterpret_cast<uint64_t>(rdx135) * reinterpret_cast<uint64_t>(rdx135) * reinterpret_cast<unsigned char>(rbp46));
    *reinterpret_cast<int32_t*>(&rdx137) = 0;
    *reinterpret_cast<int32_t*>(&rdx137 + 4) = 0;
    r13_138 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax136) + reinterpret_cast<uint64_t>(rax136) - reinterpret_cast<uint64_t>(rax136) * reinterpret_cast<uint64_t>(rax136) * reinterpret_cast<unsigned char>(rbp46));
    *reinterpret_cast<int32_t*>(&rax139) = 1;
    *reinterpret_cast<int32_t*>(&rax139 + 4) = 0;
    do {
        rdi140 = reinterpret_cast<unsigned char>(rcx129) << 63;
        rcx129 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx129) >> 1);
        rdx137 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx137) >> 1) | rdi140);
        if (reinterpret_cast<unsigned char>(rcx129) < reinterpret_cast<unsigned char>(rax139) || rcx129 == rax139 && reinterpret_cast<unsigned char>(rdx137) <= reinterpret_cast<unsigned char>(r15_130)) {
            cf141 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_130) < reinterpret_cast<unsigned char>(rdx137));
            r15_130 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_130) - reinterpret_cast<unsigned char>(rdx137));
            rax139 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax139) - (reinterpret_cast<unsigned char>(rcx129) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax139) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx129) + static_cast<uint64_t>(cf141))))));
        }
        --esi132;
    } while (esi132);
    *reinterpret_cast<int32_t*>(&rbx142) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx142) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&r8_143) = v128;
    *reinterpret_cast<int32_t*>(&r8_143 + 4) = 0;
    r9_144 = r15_130;
    *reinterpret_cast<unsigned char*>(&rbx142) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp46) - reinterpret_cast<unsigned char>(r15_130)) > reinterpret_cast<unsigned char>(r15_130));
    rbx145 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbx142) & reinterpret_cast<unsigned char>(rbp46)) + (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r15_130) + reinterpret_cast<unsigned char>(r15_130)) - reinterpret_cast<unsigned char>(rbp46)));
    al146 = millerrabin(rbp46, r13_138, rbx145, r14_45, *reinterpret_cast<uint32_t*>(&r8_143), r9_144);
    v147 = al146;
    if (al146) {
        v148 = reinterpret_cast<void*>(rsp47 - 1 + 1 + 6);
        factor();
        v149 = r14_45;
        r14_45 = r13_138;
        eax150 = v151;
        r13_138 = reinterpret_cast<struct s0*>(0x10340);
        v152 = eax150;
        *reinterpret_cast<uint32_t*>(&rax153) = eax150 - 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax153) + 4) = 0;
        v154 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v148) + rax153 * 8);
        r12_155 = 2;
        rax156 = rbx145;
        rbx145 = r15_130;
        r15_130 = rax156;
        goto addr_55b0_93;
    }
    addr_56fc_94:
    v147 = 0;
    addr_5690_95:
    rax157 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v49) - reinterpret_cast<unsigned char>(g28));
    if (rax157) {
        fun_2740();
    } else {
        eax158 = v147;
        return *reinterpret_cast<unsigned char*>(&eax158);
    }
    addr_5719_98:
    fun_2810("(s1) < (n)", "src/factor.c", 0x4f2, "prime_p", r8_143, r9_144);
    v159 = reinterpret_cast<void**>("src/factor.c");
    if (0) {
        addr_5b07_100:
        fun_2810("(1) < (n)", "src/factor.c", 0x5be, "factor_using_pollard_rho", r8_143, r9_144);
    } else {
        r12_160 = reinterpret_cast<void**>("(s1) < (n)");
        do {
            rcx161 = r12_160;
            esi162 = 64;
            *reinterpret_cast<int32_t*>(&rax163) = 1;
            *reinterpret_cast<int32_t*>(&rax163 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx164) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx164) + 4) = 0;
            rdi165 = reinterpret_cast<void*>(0);
            do {
                r8_143 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx161) << 63);
                rcx161 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx161) >> 1);
                rdx164 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx164) >> 1 | reinterpret_cast<unsigned char>(r8_143));
                if (reinterpret_cast<unsigned char>(rcx161) < reinterpret_cast<unsigned char>(rax163) || rcx161 == rax163 && reinterpret_cast<uint64_t>(rdx164) <= reinterpret_cast<uint64_t>(rdi165)) {
                    cf166 = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi165) < reinterpret_cast<uint64_t>(rdx164));
                    rdi165 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi165) - reinterpret_cast<uint64_t>(rdx164));
                    rax163 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax163) - (reinterpret_cast<unsigned char>(rcx161) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax163) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx161) + static_cast<uint64_t>(cf166))))));
                }
                --esi162;
            } while (esi162);
            *reinterpret_cast<int32_t*>(&rbp167) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp167) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r15_168) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_168) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r14_169) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_169) + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rbp167) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_160) - reinterpret_cast<uint64_t>(rdi165) > reinterpret_cast<uint64_t>(rdi165));
            rbp170 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbp167) & reinterpret_cast<unsigned char>(r12_160)) + (reinterpret_cast<uint64_t>(rdi165) + reinterpret_cast<uint64_t>(rdi165) - reinterpret_cast<unsigned char>(r12_160)));
            rcx171 = rbp170;
            while (reinterpret_cast<unsigned char>(v159) < reinterpret_cast<unsigned char>(r12_160)) {
                r11_172 = rcx171;
                rax173 = reinterpret_cast<unsigned char>(r12_160) >> 1;
                *reinterpret_cast<uint32_t*>(&rax174) = *reinterpret_cast<uint32_t*>(&rax173) & 0x7f;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax174) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax175) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax174);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax175) + 4) = 0;
                rdx176 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax175) + reinterpret_cast<int64_t>(rax175) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax175) * reinterpret_cast<int64_t>(rax175) * reinterpret_cast<unsigned char>(r12_160)));
                rax177 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx176) + reinterpret_cast<uint64_t>(rdx176) - reinterpret_cast<uint64_t>(rdx176) * reinterpret_cast<uint64_t>(rdx176) * reinterpret_cast<unsigned char>(r12_160));
                r8_178 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax177) + reinterpret_cast<uint64_t>(rax177) - reinterpret_cast<uint64_t>(rax177) * reinterpret_cast<uint64_t>(rax177) * reinterpret_cast<unsigned char>(r12_160));
                r10_179 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_160) - reinterpret_cast<unsigned char>(v159));
                r9_180 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v159) - reinterpret_cast<unsigned char>(r12_160));
                while (1) {
                    rax181 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rax181 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax181) + reinterpret_cast<unsigned char>(r12_160));
                    }
                    rbp170 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rbp170) - (reinterpret_cast<unsigned char>(rbp170) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp170) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp170) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax181) < reinterpret_cast<uint64_t>(r10_179))))))) & reinterpret_cast<unsigned char>(r12_160)) + (reinterpret_cast<uint64_t>(rax181) + reinterpret_cast<uint64_t>(r9_180)));
                    r13_182 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        r13_182 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_182) + reinterpret_cast<unsigned char>(r12_160));
                    }
                    rax183 = r14_169;
                    *reinterpret_cast<uint32_t*>(&rax184) = *reinterpret_cast<uint32_t*>(&rax183) & 31;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax184) + 4) = 0;
                    if (rax184 == 1) {
                        rdi185 = r13_182;
                        rax186 = gcd_odd(rdi185, r12_160);
                        if (!reinterpret_cast<int1_t>(rax186 == 1)) 
                            break;
                    }
                    --r14_169;
                    if (r14_169) 
                        continue;
                    rcx187 = r15_168 + r15_168;
                    if (!r15_168) {
                        r15_168 = rcx187;
                        r11_172 = rbp170;
                    } else {
                        do {
                            rdi188 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                            rax189 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi188) + reinterpret_cast<unsigned char>(r12_160));
                            if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                                rdi188 = rax189;
                            }
                            ++r14_169;
                        } while (r15_168 != r14_169);
                        r11_172 = rbp170;
                        r15_168 = rcx187;
                        rbp170 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax189) - (reinterpret_cast<uint64_t>(rax189) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax189) < reinterpret_cast<uint64_t>(rax189) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi188) < reinterpret_cast<uint64_t>(r10_179)))) & reinterpret_cast<unsigned char>(r12_160)) + (reinterpret_cast<uint64_t>(rdi188) + reinterpret_cast<uint64_t>(r9_180)));
                    }
                }
                r9_144 = r8_178;
                r8_143 = r11_172;
                r11_190 = r9_180;
                do {
                    rsi191 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    rax192 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi191) + reinterpret_cast<unsigned char>(r12_160));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rsi191 = rax192;
                    }
                    rbx193 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax192) - (reinterpret_cast<uint64_t>(rax192) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax192) < reinterpret_cast<uint64_t>(rax192) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rsi191) < reinterpret_cast<uint64_t>(r10_179)))) & reinterpret_cast<unsigned char>(r12_160)) + (reinterpret_cast<uint64_t>(rsi191) + reinterpret_cast<uint64_t>(r11_190)));
                    rsi194 = r12_160;
                    rdi185 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rdi185) - (reinterpret_cast<unsigned char>(rdi185) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi185) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rdi185) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_143) < reinterpret_cast<unsigned char>(rbx193))))))) & reinterpret_cast<unsigned char>(r12_160)) + (reinterpret_cast<unsigned char>(r8_143) - reinterpret_cast<unsigned char>(rbx193)));
                    rax195 = gcd_odd(rdi185, rsi194);
                } while (rax195 == 1);
                rcx196 = r8_143;
                r11_197 = rax195;
                if (rax195 == r12_160) 
                    goto addr_5af7_129;
                rdx198 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_160) % reinterpret_cast<unsigned char>(r11_197));
                rax199 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_160) / reinterpret_cast<unsigned char>(r11_197));
                r8_200 = rax199;
                r12_160 = rax199;
                if (reinterpret_cast<unsigned char>(r11_197) > reinterpret_cast<unsigned char>(1) && (reinterpret_cast<unsigned char>(r11_197) <= reinterpret_cast<unsigned char>(0x17ded78) || (al201 = prime_p_part_0(r11_197, rsi194, rdx198, rcx196), r11_197 = r11_197, rcx196 = rcx196, r8_200 = rax199, !!al201))) {
                    *reinterpret_cast<int32_t*>(&rdx202) = 1;
                    *reinterpret_cast<int32_t*>(&rdx202 + 4) = 0;
                    rsi203 = r11_197;
                    factor_insert_multiplicity(0x4f2, rsi203, 1);
                    r8_143 = r8_200;
                    rcx204 = rcx196;
                    zf205 = reinterpret_cast<int1_t>(r8_143 == 1);
                    if (reinterpret_cast<unsigned char>(r8_143) <= reinterpret_cast<unsigned char>(1)) 
                        goto addr_5aca_132; else 
                        goto addr_5a2f_133;
                }
                rdx202 = reinterpret_cast<void**>(0x4f2);
                rsi203 = v159 + 1;
                factor_using_pollard_rho(r11_197, rsi203, 0x4f2, rcx196);
                r8_143 = r8_200;
                rcx204 = rcx196;
                zf205 = reinterpret_cast<int1_t>(r8_143 == 1);
                if (reinterpret_cast<unsigned char>(r8_143) > reinterpret_cast<unsigned char>(1)) {
                    addr_5a2f_133:
                    if (reinterpret_cast<unsigned char>(r8_143) <= reinterpret_cast<unsigned char>(0x17ded78)) 
                        goto addr_5ad7_135;
                    al206 = prime_p_part_0(r8_143, rsi203, rdx202, rcx204);
                    r8_143 = r8_143;
                    if (al206) 
                        goto addr_5ad7_135;
                } else {
                    addr_5aca_132:
                    if (zf205) 
                        goto addr_5b45_137; else 
                        goto addr_5acc_138;
                }
                rbp170 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp170) % reinterpret_cast<unsigned char>(r8_143));
                rcx171 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx204) % reinterpret_cast<unsigned char>(r8_143));
                continue;
                addr_5acc_138:
                *reinterpret_cast<int32_t*>(&rcx171) = 0;
                *reinterpret_cast<int32_t*>(&rcx171 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rbp170) = 0;
                *reinterpret_cast<int32_t*>(&rbp170 + 4) = 0;
            }
            break;
            addr_5af7_129:
            ++v159;
        } while (reinterpret_cast<unsigned char>(r12_160) > reinterpret_cast<unsigned char>(1));
        goto addr_5b07_100;
    }
    fun_2810("a < n", "src/factor.c", 0x5c4, "factor_using_pollard_rho", r8_143, r9_144);
    addr_5b45_137:
    goto v51;
    addr_5ad7_135:
    *reinterpret_cast<uint32_t*>(&rax207) = g5ec;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax207) + 4) = 0;
    r8_208 = r8_143;
    r11_209 = reinterpret_cast<void***>(0x502);
    r10_210 = reinterpret_cast<signed char*>(0x5d2);
    r9d211 = *reinterpret_cast<uint32_t*>(&rax207);
    if (!*reinterpret_cast<uint32_t*>(&rax207)) 
        goto addr_2fa1_142;
    ebx212 = static_cast<int32_t>(rax207 - 1);
    rcx213 = reinterpret_cast<struct s44*>(static_cast<int64_t>(ebx212));
    rax214 = rcx213;
    do {
        *reinterpret_cast<int32_t*>(&rsi215) = *reinterpret_cast<int32_t*>(&rax214);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi215) + 4) = 0;
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(0x4f2 + reinterpret_cast<int64_t>(rax214) * 8 + 16)) <= reinterpret_cast<unsigned char>(r8_208)) 
            break;
        rax214 = reinterpret_cast<struct s44*>(reinterpret_cast<int64_t>(rax214) - 1);
        *reinterpret_cast<int32_t*>(&rsi215) = *reinterpret_cast<int32_t*>(&rax214);
    } while (*reinterpret_cast<int32_t*>(&rax214) != -1);
    goto addr_2f80_146;
    if (*reinterpret_cast<void***>(0x4f2 + reinterpret_cast<int64_t>(rax214) * 8 + 16) == r8_208) {
        rax214->f5d2 = reinterpret_cast<signed char>(rax214->f5d2 + 1);
        goto v51;
    }
    rax216 = reinterpret_cast<struct s45*>(static_cast<int64_t>(static_cast<int32_t>(rsi215 + 1)));
    r11_209 = reinterpret_cast<void***>(0x502 + reinterpret_cast<int64_t>(rax216) * 8);
    r10_210 = &rax216->f5d2;
    if (*reinterpret_cast<int32_t*>(&rsi215) >= ebx212) {
        addr_2fa1_142:
        r9d217 = r9d211 + 1;
        *r11_209 = r8_208;
        *r10_210 = 1;
        g5ec = *reinterpret_cast<unsigned char*>(&r9d217);
        goto v51;
    }
    do {
        addr_2f80_146:
        *reinterpret_cast<int64_t*>(0x4f2 + reinterpret_cast<int64_t>(rcx213) * 8 + 24) = *reinterpret_cast<int64_t*>(0x4f2 + reinterpret_cast<int64_t>(rcx213) * 8 + 16);
        eax218 = *reinterpret_cast<unsigned char*>(0x4f2 + reinterpret_cast<int64_t>(rcx213) + 0xe0);
        *reinterpret_cast<signed char*>(0x4f2 + reinterpret_cast<int64_t>(rcx213) + 0xe1) = *reinterpret_cast<signed char*>(&eax218);
        rcx213 = reinterpret_cast<struct s44*>(reinterpret_cast<int64_t>(rcx213) - 1);
    } while (*reinterpret_cast<int32_t*>(&rsi215) < *reinterpret_cast<int32_t*>(&rcx213));
    goto addr_2fa1_142;
    addr_5478_25:
    do {
        r14_45 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(r14_45) >> 1);
        ++eax50;
    } while (!(*reinterpret_cast<unsigned char*>(&r14_45) & 1));
    v128 = eax50;
    goto addr_5490_87;
    addr_5689_153:
    goto addr_5690_95;
    addr_60f8_154:
    eax42 = v36;
    goto addr_6015_27;
    addr_6146_155:
    eax42 = v36;
    goto addr_6015_27;
    addr_60f4_156:
    goto addr_60f8_154;
    addr_5680_157:
    while (rax219 == rbx145) {
        while (1) {
            *reinterpret_cast<uint32_t*>(&rax220) = r13_138->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax220) + 4) = 0;
            r12_155 = r12_155 + rax220;
            rax221 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx145) * r12_155);
            rdx222 = __intrinsic();
            r15_130 = rax221;
            if (rdx222) {
                if (reinterpret_cast<unsigned char>(rbp46) <= reinterpret_cast<unsigned char>(rdx222)) 
                    goto addr_5719_98;
                rcx223 = rbp46;
                esi224 = 64;
                *reinterpret_cast<int32_t*>(&rax225) = 0;
                *reinterpret_cast<int32_t*>(&rax225 + 4) = 0;
                do {
                    rdi226 = reinterpret_cast<unsigned char>(rcx223) << 63;
                    rcx223 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx223) >> 1);
                    rax225 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax225) >> 1) | rdi226);
                    if (reinterpret_cast<unsigned char>(rcx223) < reinterpret_cast<unsigned char>(rdx222) || rcx223 == rdx222 && reinterpret_cast<unsigned char>(rax225) <= reinterpret_cast<unsigned char>(r15_130)) {
                        cf227 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_130) < reinterpret_cast<unsigned char>(rax225));
                        r15_130 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_130) - reinterpret_cast<unsigned char>(rax225));
                        rdx222 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx222) - (reinterpret_cast<unsigned char>(rcx223) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx222) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx223) + static_cast<uint64_t>(cf227))))));
                    }
                    --esi224;
                } while (esi224);
            } else {
                r15_130 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax221) % reinterpret_cast<unsigned char>(rbp46));
            }
            *reinterpret_cast<uint32_t*>(&r8_143) = v128;
            *reinterpret_cast<int32_t*>(&r8_143 + 4) = 0;
            r9_144 = rbx145;
            al228 = millerrabin(rbp46, r14_45, r15_130, v149, *reinterpret_cast<uint32_t*>(&r8_143), r9_144);
            if (!al228) 
                goto addr_56fc_94;
            r13_138 = reinterpret_cast<struct s0*>(&r13_138->f1);
            if (reinterpret_cast<int1_t>(0x105dc == r13_138)) 
                break;
            addr_55b0_93:
            if (!v152) 
                goto addr_5690_95;
            r11_229 = v148;
            do {
                r8_143 = rbx145;
                rax219 = powm(r15_130, reinterpret_cast<uint64_t>(v51) / v230, rbp46, r14_45, r8_143, r9_144);
                if (v154 == r11_229) 
                    goto addr_5680_157;
                r11_229 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r11_229) + 8);
            } while (rax219 != rbx145);
        }
        fun_2700();
        fun_2a20();
        rax219 = fun_25f0();
    }
    goto addr_5689_153;
    while (1) {
        addr_5fc7_173:
        fun_2700();
        fun_2a20();
        fun_25f0();
        rsp4 = rsp4 - 8 + 8 - 8 + 8 - 8 + 8;
        while (1) {
            while (1) {
                r9_33 = r15_3;
                rcx32 = r14_31;
                rdi = rbx7;
                rax231 = powm2(rdi, r12_30, v41, rcx32, r13_29, r9_33);
                rsp4 = rsp4 - 8 + 8;
                if (rax231 == v232) {
                    if (v233 <= *reinterpret_cast<uint32_t*>(&rbp19)) {
                        r9_33 = r15_3;
                        rbx7 = v234;
                        rbp19 = v235;
                        r15_3 = v236;
                        al237 = reinterpret_cast<uint1_t>(v238 != v25);
                        while (1) {
                            if (al237) 
                                goto addr_6146_155;
                            do {
                                addr_5f21_179:
                                *reinterpret_cast<int32_t*>(&rdx239) = 0;
                                *reinterpret_cast<int32_t*>(&rdx239 + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&rax240) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v40));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax240) + 4) = 0;
                                r15_3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_3) + reinterpret_cast<uint64_t>(rax240));
                                rax241 = reinterpret_cast<void**>(0);
                                if (reinterpret_cast<unsigned char>(rbx7) > reinterpret_cast<unsigned char>(r15_3)) {
                                    rax241 = r15_3;
                                }
                                rcx242 = (reinterpret_cast<unsigned char>(rcx32) - (reinterpret_cast<unsigned char>(rcx32) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx32) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx32) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_3) < reinterpret_cast<unsigned char>(rbx7))))))) & 0xffffffffffffffc0) + 0x7f;
                                if (reinterpret_cast<unsigned char>(rbx7) <= reinterpret_cast<unsigned char>(r15_3)) {
                                    rdx239 = r15_3;
                                }
                                do {
                                    rsi243 = rdx239;
                                    rdx239 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx239) + reinterpret_cast<unsigned char>(rdx239));
                                    rax241 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax241) + reinterpret_cast<unsigned char>(rax241)) | reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rsi243) >> 63));
                                    if (reinterpret_cast<unsigned char>(rbx7) < reinterpret_cast<unsigned char>(rax241) || rbx7 == rax241 && reinterpret_cast<unsigned char>(rbp19) <= reinterpret_cast<unsigned char>(rdx239)) {
                                        cf244 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdx239) < reinterpret_cast<unsigned char>(rbp19));
                                        rdx239 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx239) - reinterpret_cast<unsigned char>(rbp19));
                                        rax241 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax241) - (reinterpret_cast<unsigned char>(rbx7) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax241) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbx7) + static_cast<uint64_t>(cf244))))));
                                    }
                                    cf245 = rcx242 < 1;
                                    --rcx242;
                                } while (!cf245);
                                rcx32 = v34;
                                rdi = r14_31;
                                v232 = r9_33;
                                *reinterpret_cast<unsigned char*>(&eax42) = millerrabin2(rdi, r13_29, r12_30, rcx32, v10, r9_33);
                                rsp4 = rsp4 - 8 + 8;
                                if (!*reinterpret_cast<unsigned char*>(&eax42)) 
                                    goto addr_6015_27;
                                ++v40;
                                rdi = reinterpret_cast<void**>(0x105dc);
                                r9_33 = v232;
                                if (reinterpret_cast<int1_t>(0x105dc == v40)) 
                                    goto addr_5fc7_173;
                                addr_5d90_15:
                                if (!v38) 
                                    goto addr_5d9c_189;
                                rdi = reinterpret_cast<void**>(rsp4 + 0xd8);
                                v246 = rdi;
                                rcx32 = r14_31;
                                rax247 = powm2(rdi, r12_30, v41, rcx32, r13_29, r9_33);
                                rsp4 = rsp4 - 8 + 8;
                                r9_33 = r9_33;
                                if (rax247 != v26) 
                                    goto addr_60e6_191;
                                if (!v248) 
                                    break;
                            } while (v249 == v25);
                            goto addr_61a8_194;
                            al237 = reinterpret_cast<uint1_t>(v249 != v25);
                        }
                    } else {
                        if (v238 == v25) {
                            r9_33 = r15_3;
                            rbx7 = v234;
                            rbp19 = v235;
                            r15_3 = v236;
                            goto addr_5f21_179;
                        }
                    }
                } else {
                    if (v233 <= *reinterpret_cast<uint32_t*>(&rbp19)) 
                        goto addr_60f8_154; else 
                        goto addr_5ea2_199;
                }
                addr_5d9c_189:
                if (!v250) 
                    goto addr_60f8_154;
                v246 = reinterpret_cast<void**>(rsp4 + 0xd8);
                addr_5db7_201:
                v236 = r15_3;
                r15_3 = r9_33;
                v234 = rbx7;
                rbx7 = v246;
                v232 = v26;
                v235 = rbp19;
                rbp19 = reinterpret_cast<void**>(1);
                v233 = v251;
                addr_5ea6_202:
                if (*reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(v37) + reinterpret_cast<unsigned char>(rbp19) * 8 + 8) != 2) {
                    if (v9 < *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(v37) + reinterpret_cast<unsigned char>(rbp19) * 8 + 8)) 
                        break;
                    continue;
                } else {
                    continue;
                }
                addr_60e6_191:
                if (v252) 
                    goto addr_5db7_201; else 
                    goto addr_60f4_156;
                addr_61a8_194:
                goto addr_5db7_201;
                addr_5ea2_199:
                ++rbp19;
                goto addr_5ea6_202;
            }
        }
    }
}