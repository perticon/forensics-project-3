factor_using_squfof (uintmax_t n1, uintmax_t n0, struct factors *factors)
{
  /* Uses algorithm and notation from

     SQUARE FORM FACTORIZATION
     JASON E. GOWER AND SAMUEL S. WAGSTAFF, JR.

     https://homes.cerias.purdue.edu/~ssw/squfof.pdf
   */

  static const unsigned int multipliers_1[] =
    { /* = 1 (mod 4) */
      105, 165, 21, 385, 33, 5, 77, 1, 0
    };
  static const unsigned int multipliers_3[] =
    { /* = 3 (mod 4) */
      1155, 15, 231, 35, 3, 55, 7, 11, 0
    };

  const unsigned int *m;

  struct { uintmax_t Q; uintmax_t P; } queue[QUEUE_SIZE];

  if (n1 >= ((uintmax_t) 1 << (W_TYPE_SIZE - 2)))
    return false;

  uintmax_t sqrt_n = isqrt2 (n1, n0);

  if (n0 == sqrt_n * sqrt_n)
    {
      uintmax_t p1, p0;

      umul_ppmm (p1, p0, sqrt_n, sqrt_n);
      assert (p0 == n0);

      if (n1 == p1)
        {
          if (prime_p (sqrt_n))
            factor_insert_multiplicity (factors, sqrt_n, 2);
          else
            {
              struct factors f;

              f.nfactors = 0;
              if (!factor_using_squfof (0, sqrt_n, &f))
                {
                  /* Try pollard rho instead */
                  factor_using_pollard_rho (sqrt_n, 1, &f);
                }
              /* Duplicate the new factors */
              for (unsigned int i = 0; i < f.nfactors; i++)
                factor_insert_multiplicity (factors, f.p[i], 2 * f.e[i]);
            }
          return true;
        }
    }

  /* Select multipliers so we always get n * mu = 3 (mod 4) */
  for (m = (n0 % 4 == 1) ? multipliers_3 : multipliers_1;
       *m; m++)
    {
      uintmax_t S, Dh, Dl, Q1, Q, P, L, L1, B;
      unsigned int i;
      unsigned int mu = *m;
      unsigned int qpos = 0;

      assert (mu * n0 % 4 == 3);

      /* In the notation of the paper, with mu * n == 3 (mod 4), we
         get \Delta = 4 mu * n, and the paper's \mu is 2 mu.  As far as
         I understand it, the necessary bound is 4 \mu^3 < n, or 32
         mu^3 < n.

         However, this seems insufficient: With n = 37243139 and mu =
         105, we get a trivial factor, from the square 38809 = 197^2,
         without any corresponding Q earlier in the iteration.

         Requiring 64 mu^3 < n seems sufficient.  */
      if (n1 == 0)
        {
          if ((uintmax_t) mu * mu * mu >= n0 / 64)
            continue;
        }
      else
        {
          if (n1 > ((uintmax_t) 1 << (W_TYPE_SIZE - 2)) / mu)
            continue;
        }
      umul_ppmm (Dh, Dl, n0, mu);
      Dh += n1 * mu;

      assert (Dl % 4 != 1);
      assert (Dh < (uintmax_t) 1 << (W_TYPE_SIZE - 2));

      S = isqrt2 (Dh, Dl);

      Q1 = 1;
      P = S;

      /* Square root remainder fits in one word, so ignore high part.  */
      Q = Dl - P * P;
      /* FIXME: When can this differ from floor (sqrt (2 * sqrt (D)))?  */
      L = isqrt (2 * S);
      B = 2 * L;
      L1 = mu * 2 * L;

      /* The form is (+/- Q1, 2P, -/+ Q), of discriminant 4 (P^2 + Q Q1) =
         4 D.  */

      for (i = 0; i <= B; i++)
        {
          uintmax_t q, P1, t, rem;

          div_smallq (q, rem, S + P, Q);
          P1 = S - rem; /* P1 = q*Q - P */

          assert (q > 0 && Q > 0);

# if STAT_SQUFOF
          q_freq[0]++;
          q_freq[MIN (q, Q_FREQ_SIZE)]++;
# endif

          if (Q <= L1)
            {
              uintmax_t g = Q;

              if ((Q & 1) == 0)
                g /= 2;

              g /= gcd_odd (g, mu);

              if (g <= L)
                {
                  if (qpos >= QUEUE_SIZE)
                    die (EXIT_FAILURE, 0, _("squfof queue overflow"));
                  queue[qpos].Q = g;
                  queue[qpos].P = P % g;
                  qpos++;
                }
            }

          /* I think the difference can be either sign, but mod
             2^W_TYPE_SIZE arithmetic should be fine.  */
          t = Q1 + q * (P - P1);
          Q1 = Q;
          Q = t;
          P = P1;

          if ((i & 1) == 0)
            {
              uintmax_t r = is_square (Q);
              if (r)
                {
                  for (unsigned int j = 0; j < qpos; j++)
                    {
                      if (queue[j].Q == r)
                        {
                          if (r == 1)
                            /* Traversed entire cycle.  */
                            goto next_multiplier;

                          /* Need the absolute value for divisibility test.  */
                          if (P >= queue[j].P)
                            t = P - queue[j].P;
                          else
                            t = queue[j].P - P;
                          if (t % r == 0)
                            {
                              /* Delete entries up to and including entry
                                 j, which matched.  */
                              memmove (queue, queue + j + 1,
                                       (qpos - j - 1) * sizeof (queue[0]));
                              qpos -= (j + 1);
                            }
                          goto next_i;
                        }
                    }

                  /* We have found a square form, which should give a
                     factor.  */
                  Q1 = r;
                  assert (S >= P); /* What signs are possible?  */
                  P += r * ((S - P) / r);

                  /* Note: Paper says (N - P*P) / Q1, that seems incorrect
                     for the case D = 2N.  */
                  /* Compute Q = (D - P*P) / Q1, but we need double
                     precision.  */
                  uintmax_t hi, lo;
                  umul_ppmm (hi, lo, P, P);
                  sub_ddmmss (hi, lo, Dh, Dl, hi, lo);
                  udiv_qrnnd (Q, rem, hi, lo, Q1);
                  assert (rem == 0);

                  for (;;)
                    {
                      /* Note: There appears to by a typo in the paper,
                         Step 4a in the algorithm description says q <--
                         floor([S+P]/\hat Q), but looking at the equations
                         in Sec. 3.1, it should be q <-- floor([S+P] / Q).
                         (In this code, \hat Q is Q1).  */
                      div_smallq (q, rem, S + P, Q);
                      P1 = S - rem;     /* P1 = q*Q - P */

# if STAT_SQUFOF
                      q_freq[0]++;
                      q_freq[MIN (q, Q_FREQ_SIZE)]++;
# endif
                      if (P == P1)
                        break;
                      t = Q1 + q * (P - P1);
                      Q1 = Q;
                      Q = t;
                      P = P1;
                    }

                  if ((Q & 1) == 0)
                    Q /= 2;
                  Q /= gcd_odd (Q, mu);

                  assert (Q > 1 && (n1 || Q < n0));

                  if (prime_p (Q))
                    factor_insert (factors, Q);
                  else if (!factor_using_squfof (0, Q, factors))
                    factor_using_pollard_rho (Q, 2, factors);

                  divexact_21 (n1, n0, n1, n0, Q);

                  if (prime2_p (n1, n0))
                    factor_insert_large (factors, n1, n0);
                  else
                    {
                      if (!factor_using_squfof (n1, n0, factors))
                        {
                          if (n1 == 0)
                            factor_using_pollard_rho (n0, 1, factors);
                          else
                            factor_using_pollard_rho2 (n1, n0, 1, factors);
                        }
                    }

                  return true;
                }
            }
        next_i:;
        }
    next_multiplier:;
    }
  return false;
}