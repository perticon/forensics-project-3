bool millerrabin(int64_t n, int64_t ni, int64_t b, int64_t q, uint32_t k, int64_t one) {
    int64_t v1 = powm(b, q, n, ni, one); // 0x304f
    bool v2 = v1 == one | v1 == n - one;
    int64_t v3 = v2; // 0x3067
    if (k < 2 || v2) {
        // 0x30b5
        return v3 != 0;
    }
    int64_t v4 = 1; // 0x30ad
    // 0x30b5
    return v4 != 0;
}