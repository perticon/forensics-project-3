unsigned char millerrabin(void** rdi, struct s0* rsi, void** rdx, struct s0* rcx, uint32_t r8d, void** r9) {
    void** r11_7;
    void** r12_8;
    uint32_t ebp9;
    void** r8_10;
    void** rax11;
    void** rdi12;
    int32_t eax13;
    uint32_t esi14;

    r11_7 = rdi;
    r12_8 = r9;
    ebp9 = r8d;
    r8_10 = r9;
    rax11 = powm(rdx, rcx, r11_7, rsi, r8_10, r9);
    rdi12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r11_7) - reinterpret_cast<unsigned char>(r12_8));
    *reinterpret_cast<unsigned char*>(&r8_10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r12_8 == rax11)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(rax11 == rdi12)));
    if (*reinterpret_cast<unsigned char*>(&r8_10) || ebp9 <= 1) {
        addr_30b5_2:
        eax13 = *reinterpret_cast<int32_t*>(&r8_10);
        return *reinterpret_cast<unsigned char*>(&eax13);
    } else {
        esi14 = 1;
        do {
            rax11 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
            if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                rax11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax11) + reinterpret_cast<unsigned char>(r11_7));
            }
            if (rdi12 == rax11) 
                break;
        } while (r12_8 != rax11 && (++esi14, ebp9 != esi14));
        goto addr_30b5_2;
    }
    *reinterpret_cast<int32_t*>(&r8_10) = 1;
    goto addr_30b5_2;
}