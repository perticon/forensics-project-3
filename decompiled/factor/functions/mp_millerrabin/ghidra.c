mp_millerrabin(undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,
              undefined8 param_5,ulong param_6)

{
  int iVar1;
  ulong uVar2;
  
  __gmpz_powm(param_4,param_3,param_5,param_1);
  iVar1 = __gmpz_cmp_ui(param_4,1);
  if ((iVar1 != 0) && (iVar1 = __gmpz_cmp(param_4,param_2), iVar1 != 0)) {
    uVar2 = 1;
    if (1 < param_6) {
      do {
        __gmpz_powm_ui(param_4,param_4,2,param_1);
        iVar1 = __gmpz_cmp(param_4,param_2);
        if (iVar1 == 0) {
          return 1;
        }
        iVar1 = __gmpz_cmp_ui(param_4,1);
      } while ((iVar1 != 0) && (uVar2 = uVar2 + 1, param_6 != uVar2));
    }
    return 0;
  }
  return 1;
}