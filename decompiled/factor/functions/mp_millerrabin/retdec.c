int64_t mp_millerrabin(int64_t a1, int64_t * a2, int64_t * a3, int64_t * a4, int64_t * a5, uint64_t a6) {
    // 0x3b30
    function_2730();
    if ((int32_t)function_2aa0() == 0 || (int32_t)function_2a00() == 0) {
        // 0x3bc4
        return 1;
    }
    // 0x3b75
    if (a6 < 2) {
        // 0x3bc4
        return 0;
    }
    int64_t v1 = 1; // 0x3b7e
    function_2ad0();
    int64_t result = 1; // 0x3bc2
    while ((int32_t)function_2a00() != 0) {
        // 0x3b88
        result = 0;
        if ((int32_t)function_2aa0() == 0) {
            // break -> 0x3bc4
            break;
        }
        // 0x3b99
        v1++;
        result = 0;
        if (v1 == a6) {
            // break -> 0x3bc4
            break;
        }
        function_2ad0();
        result = 1;
    }
    // 0x3bc4
    return result;
}