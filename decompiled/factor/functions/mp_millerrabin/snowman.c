unsigned char mp_millerrabin(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r14_7;
    void** r13_8;
    void** r12_9;
    void** rbx10;
    int32_t eax11;
    int32_t eax12;
    void** rbp13;
    int32_t eax14;
    int32_t eax15;

    r14_7 = r9;
    r13_8 = rsi;
    r12_9 = rdi;
    rbx10 = rcx;
    fun_2730(rbx10, rdx, r8, rdi, r8, r9);
    eax11 = fun_2aa0(rbx10, 1, r8, rdi, r8, r9);
    if (!eax11 || (eax12 = fun_2a00(rbx10, r13_8, r8, rdi, r8), eax12 == 0)) {
        addr_3bc4_2:
        return 1;
    } else {
        *reinterpret_cast<int32_t*>(&rbp13) = 1;
        *reinterpret_cast<int32_t*>(&rbp13 + 4) = 0;
        if (reinterpret_cast<unsigned char>(r14_7) > reinterpret_cast<unsigned char>(1)) {
            do {
                fun_2ad0(rbx10, rbx10, 2, r12_9);
                eax14 = fun_2a00(rbx10, r13_8, 2, r12_9, r8);
                if (!eax14) 
                    goto addr_3bc4_2;
                eax15 = fun_2aa0(rbx10, 1, 2, r12_9, r8, r9);
            } while (eax15 && (++rbp13, r14_7 != rbp13));
        }
    }
    return 0;
}