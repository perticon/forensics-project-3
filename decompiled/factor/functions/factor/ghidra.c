ulong * factor(ulong *param_1,ulong *param_2,ulong *param_3)

{
  undefined auVar1 [16];
  byte bVar2;
  char cVar3;
  ulong *in_RAX;
  ulong uVar4;
  undefined8 uVar5;
  ulong *puVar6;
  uint uVar7;
  ulong uVar8;
  long lVar9;
  ulong uVar10;
  int iVar11;
  ulong uVar12;
  ulong uVar13;
  ulong uVar14;
  ulong *puVar15;
  ulong *puVar16;
  undefined *puVar17;
  ulong *puVar18;
  undefined1 *puVar19;
  ulong **ppuVar20;
  ulong *puVar21;
  ulong uVar22;
  long in_FS_OFFSET;
  bool bVar23;
  int iStack460;
  byte bStack433;
  undefined auStack432 [250];
  byte bStack182;
  long lStack168;
  long *plStack152;
  ulong *puStack144;
  ulong uStack136;
  ulong uStack128;
  undefined1 *puStack120;
  ulong *puStack112;
  undefined1 *puStack104;
  ulong *puStack96;
  ulong *puStack88;
  ulong *puStack80;
  ulong *puStack72;
  ulong *puStack64;
  
  *(undefined *)((long)param_3 + 0xfa) = 0;
  param_3[1] = 0;
  if ((param_1 == (ulong *)0x0) && (param_2 < (ulong *)0x2)) {
    return in_RAX;
  }
  if (((ulong)param_2 & 1) == 0) {
    if (param_2 == (ulong *)0x0) {
      lVar9 = 0;
      if (param_1 != (ulong *)0x0) {
        for (; ((ulong)param_1 >> lVar9 & 1) == 0; lVar9 = lVar9 + 1) {
        }
      }
      param_2 = (ulong *)((ulong)param_1 >> ((byte)lVar9 & 0x3f));
      uStack136 = 3;
      uVar7 = 0;
      puStack112 = (ulong *)0x104d87;
      puStack96 = param_3;
      factor_insert_multiplicity(param_3,2,(int)lVar9 + 0x40);
      param_1 = (ulong *)0x0;
      puVar16 = (ulong *)0x5555555555555555;
      puVar15 = (ulong *)0xaaaaaaaaaaaaaaab;
      param_3 = puStack96;
      goto LAB_00104ef9;
    }
    lVar9 = 0;
    if (param_2 != (ulong *)0x0) {
      for (; ((ulong)param_2 >> lVar9 & 1) == 0; lVar9 = lVar9 + 1) {
      }
    }
    bVar2 = (byte)lVar9;
    puStack96 = (ulong *)((ulong)param_1 >> (bVar2 & 0x3f));
    param_2 = (ulong *)((ulong)param_2 >> (bVar2 & 0x3f) | (long)param_1 << (0x40 - bVar2 & 0x3f));
    puStack112 = (ulong *)0x104dea;
    puStack104 = (undefined1 *)param_3;
    factor_insert_multiplicity();
    param_1 = puStack96;
    param_3 = (ulong *)puStack104;
  }
  if (param_1 != (ulong *)0x0) {
    plStack152 = (long *)0x1;
    puVar21 = (ulong *)0xaaaaaaaaaaaaaaab;
    uStack136 = 3;
    puStack104 = (undefined1 *)param_3;
    ppuVar20 = (ulong **)(primes_dtab + 8);
    do {
      puVar15 = (ulong *)((long)param_2 * (long)puVar21);
      puVar16 = SUB168(ZEXT816(puVar15) * ZEXT816(uStack136) >> 0x40,0);
      param_3 = (ulong *)puStack104;
      if (puVar16 <= param_1) {
        puVar6 = *ppuVar20;
        do {
          puVar18 = (ulong *)(((long)param_1 - (long)puVar16) * (long)puVar21);
          param_3 = (ulong *)puStack104;
          if (puVar6 <= puVar18 && (long)puVar18 - (long)puVar6 != 0) break;
          puStack112 = (ulong *)0x104e96;
          puStack96 = puVar15;
          puStack88 = puVar6;
          factor_insert_multiplicity(puStack104,uStack136,1);
          puVar15 = (ulong *)((long)puVar21 * (long)puStack96);
          puVar16 = SUB168(ZEXT816(puVar15) * ZEXT816(uStack136) >> 0x40,0);
          param_2 = puStack96;
          param_1 = puVar18;
          param_3 = (ulong *)puStack104;
          puVar6 = puStack88;
        } while (puVar16 <= puVar18);
      }
      puVar16 = (ulong *)(ulong)*(byte *)(plStack152 + 0x22068);
      uStack128 = (ulong)plStack152 & 0xffffffff;
      uVar7 = (uint)uStack128;
      puStack120 = (undefined1 *)(ppuVar20 + 2);
      uStack136 = uStack136 + (long)puVar16;
      if ((param_1 == (ulong *)0x0) || (0x29b < (uint)plStack152)) goto LAB_00104ed0;
      puVar21 = ppuVar20[1];
      plStack152 = (long *)((long)plStack152 + 1);
      puStack104 = (undefined1 *)param_3;
      ppuVar20 = (ulong **)puStack120;
    } while( true );
  }
  uStack136 = 3;
  uVar7 = 0;
  puVar16 = (ulong *)0x5555555555555555;
  puVar15 = (ulong *)0xaaaaaaaaaaaaaaab;
  goto LAB_00104ef9;
LAB_00104ed0:
  if (0x29b < uVar7) {
LAB_00105200:
    if (param_1 == (ulong *)0x0) {
      if (param_2 < 2) {
        return puVar16;
      }
      puStack112 = (ulong *)0x105221;
      puStack104 = (undefined1 *)param_3;
      cVar3 = prime2_p(0,param_2);
      if (cVar3 == '\0') {
        puVar15 = (ulong *)factor_using_pollard_rho(param_2,1,puStack104);
        return puVar15;
      }
      puVar15 = (ulong *)factor_insert_multiplicity(puStack104,param_2,1);
      return puVar15;
    }
    puStack112 = (ulong *)0x10539c;
    puStack104 = (undefined1 *)param_1;
    puStack96 = param_3;
    puVar15 = (ulong *)prime2_p(param_1,param_2);
    if ((char)puVar15 == '\0') {
      puVar15 = (ulong *)factor_using_pollard_rho2(puStack104,param_2,1,puStack96);
      return puVar15;
    }
    if (puStack96[1] == 0) {
      *puStack96 = (ulong)param_2;
      puStack96[1] = (ulong)puStack104;
      return puVar15;
    }
    puStack112 = (ulong *)0x105438;
    factor_insert_large_part_0();
    uVar8 = (long)param_1 - 1;
    lStack168 = *(long *)(in_FS_OFFSET + 0x28);
    iStack460 = 0;
    uVar12 = uVar8;
    if (((ulong)param_1 & 1) == 0) {
      iStack460 = 0;
    }
    else {
      do {
        uVar12 = uVar12 >> 1;
        iStack460 = iStack460 + 1;
      } while ((uVar12 & 1) == 0);
    }
    uVar22 = 0;
    iVar11 = 0x40;
    uVar10 = (ulong)(byte)binvert_table[(uint)((ulong)param_1 >> 1) & 0x7f];
    lVar9 = uVar10 * 2 - uVar10 * uVar10 * (long)param_1;
    lVar9 = lVar9 * 2 - lVar9 * lVar9 * (long)param_1;
    uVar10 = 0;
    lVar9 = lVar9 * 2 - lVar9 * lVar9 * (long)param_1;
    puVar16 = (ulong *)0x1;
    puVar15 = param_1;
    do {
      uVar13 = (long)puVar15 << 0x3f;
      puVar15 = (ulong *)((ulong)puVar15 >> 1);
      uVar10 = uVar10 >> 1 | uVar13;
      if ((puVar15 < puVar16) || ((puVar15 == puVar16 && (uVar10 <= uVar22)))) {
        bVar23 = uVar22 < uVar10;
        uVar22 = uVar22 - uVar10;
        puVar16 = (ulong *)((long)puVar16 + (-(ulong)bVar23 - (long)puVar15));
      }
      iVar11 = iVar11 + -1;
    } while (iVar11 != 0);
    uVar10 = (-(ulong)(uVar22 < (long)param_1 - uVar22) & (ulong)param_1) +
             (uVar22 * 2 - (long)param_1);
    puStack144 = param_2;
    puStack112 = puVar21;
    bStack433 = millerrabin(param_1,lVar9,uVar10,uVar12,iStack460,uVar22);
    if (bStack433 == 0) {
LAB_001056fc:
      bStack433 = 0;
    }
    else {
      factor(0,uVar8,auStack432);
      puVar19 = primes_diff;
      uVar13 = 2;
      while (puVar17 = auStack432, bStack182 != 0) {
        do {
          uVar4 = powm(uVar10,uVar8 / *(ulong *)(puVar17 + 0x10),param_1,lVar9,uVar22);
          if (auStack432 + (ulong)(bStack182 - 1) * 8 == puVar17) {
            if (uVar4 != uVar22) goto LAB_00105690;
            break;
          }
          puVar17 = puVar17 + 8;
        } while (uVar4 != uVar22);
        uVar13 = uVar13 + (byte)*puVar19;
        auVar1 = ZEXT816(uVar22) * ZEXT816(uVar13);
        puVar15 = SUB168(auVar1 >> 0x40,0);
        uVar10 = SUB168(auVar1,0);
        if (puVar15 == (ulong *)0x0) {
          uVar10 = SUB168((ZEXT816(0) << 0x40 | auVar1 & (undefined  [16])0xffffffffffffffff) %
                          ZEXT816(param_1),0);
        }
        else {
          if (param_1 <= puVar15) {
                    /* WARNING: Subroutine does not return */
            __assert_fail("(s1) < (n)","src/factor.c",0x4f2,"prime_p");
          }
          iVar11 = 0x40;
          uVar4 = 0;
          puVar16 = param_1;
          do {
            uVar14 = (long)puVar16 << 0x3f;
            puVar16 = (ulong *)((ulong)puVar16 >> 1);
            uVar4 = uVar4 >> 1 | uVar14;
            if ((puVar16 < puVar15) || ((puVar16 == puVar15 && (uVar4 <= uVar10)))) {
              bVar23 = uVar10 < uVar4;
              uVar10 = uVar10 - uVar4;
              puVar15 = (ulong *)((long)puVar15 + (-(ulong)bVar23 - (long)puVar16));
            }
            iVar11 = iVar11 + -1;
          } while (iVar11 != 0);
        }
        cVar3 = millerrabin(param_1,lVar9,uVar10,uVar12,iStack460,uVar22);
        if (cVar3 == '\0') goto LAB_001056fc;
        puVar19 = puVar19 + 1;
        if (puVar19 == primes_diff + 0x29c) {
          uVar5 = dcgettext(0,"Lucas prime test failure.  This should not happen",5);
          error(0,0,uVar5);
                    /* WARNING: Subroutine does not return */
          abort();
        }
      }
    }
LAB_00105690:
    if (lStack168 == *(long *)(in_FS_OFFSET + 0x28)) {
      return (ulong *)(ulong)bStack433;
    }
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  puVar15 = *(ulong **)(primes_dtab + uStack128 * 0x10);
  puVar16 = *(ulong **)(primes_dtab + uStack128 * 0x10 + 8);
LAB_00104ef9:
  puStack104 = primes_dtab;
  uStack128 = (ulong)(uVar7 + 1);
  puStack120 = primes_diff;
  plStack152 = (long *)(primes_dtab + uStack128 * 0x10);
  do {
    iVar11 = (int)uStack128;
    puVar21 = (ulong *)(ulong)(iVar11 - 1);
    puVar6 = (ulong *)((long)param_2 * (long)puVar15);
    if (puVar16 < puVar6) {
      puVar15 = (ulong *)(*plStack152 * (long)param_2);
      if ((ulong *)plStack152[1] < puVar15) goto LAB_00104f29;
LAB_00105050:
      puStack96 = param_3;
      puStack80 = param_1;
      puStack88 = (ulong *)((byte)primes_diff[uStack128] + uStack136);
      do {
        param_2 = puVar15;
        puStack112 = (ulong *)0x10507d;
        factor_insert_multiplicity(puStack96,puStack88,1);
        puVar15 = (ulong *)(*plStack152 * (long)param_2);
      } while (puVar15 < (ulong *)plStack152[1] || (long)puVar15 - (long)plStack152[1] == 0);
      puVar15 = (ulong *)(plStack152[2] * (long)param_2);
      param_1 = puStack80;
      param_3 = puStack96;
      if (puVar15 < (ulong *)plStack152[3] || (long)puVar15 - (long)plStack152[3] == 0)
      goto LAB_001050b0;
LAB_00104f3b:
      puVar15 = (ulong *)(plStack152[4] * (long)param_2);
      if ((ulong *)plStack152[5] <= puVar15 && (long)puVar15 - (long)plStack152[5] != 0)
      goto LAB_00104f4d;
LAB_00105120:
      puStack96 = param_3;
      puStack80 = param_1;
      puStack88 = (ulong *)((ulong)(byte)primes_diff[iVar11 + 2] +
                            (ulong)(byte)primes_diff[uStack128] + uStack136 +
                           (ulong)(byte)primes_diff[iVar11 + 1]);
      do {
        param_2 = puVar15;
        puStack112 = (ulong *)0x105165;
        factor_insert_multiplicity(puStack96,puStack88,1);
        puVar15 = (ulong *)(plStack152[4] * (long)param_2);
      } while (puVar15 < (ulong *)plStack152[5] || (long)puVar15 - (long)plStack152[5] == 0);
      puVar15 = (ulong *)(plStack152[6] * (long)param_2);
      param_1 = puStack80;
      param_3 = puStack96;
      if (puVar15 < (ulong *)plStack152[7] || (long)puVar15 - (long)plStack152[7] == 0) {
LAB_00105198:
        puStack80._0_4_ = iVar11 + 4;
        uVar8 = uStack128;
        uVar12 = uStack136;
        puStack96 = param_3;
        puStack88 = param_1;
        do {
          do {
            param_2 = puVar15;
            uVar7 = (int)uVar8 + 1;
            uVar12 = uVar12 + (byte)primes_diff[uVar8];
            uVar8 = (ulong)uVar7;
            puVar15 = param_2;
          } while ((uint)puStack80 != uVar7);
          puStack80 = (ulong *)((ulong)puStack80 & 0xffffffff00000000 | (ulong)(uint)puStack80);
          puStack112 = (ulong *)0x1051d4;
          factor_insert_multiplicity(puStack96,uVar12,1);
          puVar15 = (ulong *)(plStack152[6] * (long)param_2);
          uVar8 = uStack128;
          uVar12 = uStack136;
          param_1 = puStack88;
          param_3 = puStack96;
        } while (puVar15 < (ulong *)plStack152[7] || (long)puVar15 - (long)plStack152[7] == 0);
      }
    }
    else {
      do {
        puStack112 = (ulong *)0x105019;
        puStack96 = param_3;
        puStack88 = puVar15;
        puStack80 = puVar16;
        puStack72 = puVar6;
        puStack64 = param_1;
        factor_insert_multiplicity(param_3,uStack136,1);
        puVar6 = (ulong *)((long)puStack72 * (long)puStack88);
        param_1 = puStack64;
        param_3 = puStack96;
        puVar15 = puStack88;
        puVar16 = puStack80;
      } while (puVar6 <= puStack80);
      puVar15 = (ulong *)(*plStack152 * (long)puStack72);
      param_2 = puStack72;
      if (puVar15 <= (ulong *)plStack152[1]) goto LAB_00105050;
LAB_00104f29:
      puVar15 = (ulong *)(plStack152[2] * (long)param_2);
      if ((ulong *)plStack152[3] <= puVar15 && (long)puVar15 - (long)plStack152[3] != 0)
      goto LAB_00104f3b;
LAB_001050b0:
      puStack96 = param_3;
      puStack80 = param_1;
      puStack88 = (ulong *)((ulong)(byte)primes_diff[iVar11 + 1] +
                            (ulong)(byte)primes_diff[uStack128] + uStack136);
      do {
        param_2 = puVar15;
        puStack112 = (ulong *)0x1050e9;
        factor_insert_multiplicity(puStack96,puStack88,1);
        puVar15 = (ulong *)(plStack152[2] * (long)param_2);
      } while (puVar15 < (ulong *)plStack152[3] || (long)puVar15 - (long)plStack152[3] == 0);
      puVar15 = (ulong *)(plStack152[4] * (long)param_2);
      param_1 = puStack80;
      param_3 = puStack96;
      if (puVar15 < (ulong *)plStack152[5] || (long)puVar15 - (long)plStack152[5] == 0)
      goto LAB_00105120;
LAB_00104f4d:
      puVar15 = (ulong *)(plStack152[6] * (long)param_2);
      if (puVar15 < (ulong *)plStack152[7] || (long)puVar15 - (long)plStack152[7] == 0)
      goto LAB_00105198;
    }
    puVar15 = (ulong *)(plStack152[8] * (long)param_2);
    if (puVar15 < (ulong *)plStack152[9] || (long)puVar15 - (long)plStack152[9] == 0) {
      puStack80._0_4_ = iVar11 + 5;
      uVar8 = uStack128;
      uVar12 = uStack136;
      puStack96 = param_3;
      puStack88 = param_1;
      do {
        do {
          param_2 = puVar15;
          uVar7 = (int)uVar8 + 1;
          uVar12 = uVar12 + (byte)primes_diff[uVar8];
          uVar8 = (ulong)uVar7;
          puVar15 = param_2;
        } while (uVar7 != (uint)puStack80);
        puStack80 = (ulong *)((ulong)puStack80 & 0xffffffff00000000 | (ulong)(uint)puStack80);
        puStack112 = (ulong *)0x10535c;
        factor_insert_multiplicity(puStack96,uVar12,1);
        puVar15 = (ulong *)(plStack152[8] * (long)param_2);
        uVar8 = uStack128;
        uVar12 = uStack136;
        param_1 = puStack88;
        param_3 = puStack96;
      } while (puVar15 < (ulong *)plStack152[9] || (long)puVar15 - (long)plStack152[9] == 0);
    }
    puVar15 = (ulong *)(plStack152[10] * (long)param_2);
    if (puVar15 < (ulong *)plStack152[0xb] || (long)puVar15 - (long)plStack152[0xb] == 0) {
      puStack80._0_4_ = iVar11 + 6;
      uVar8 = uStack128;
      uVar12 = uStack136;
      puStack96 = param_3;
      puStack88 = param_1;
      do {
        do {
          param_2 = puVar15;
          uVar7 = (int)uVar8 + 1;
          uVar12 = uVar12 + (byte)primes_diff[uVar8];
          uVar8 = (ulong)uVar7;
          puVar15 = param_2;
        } while (uVar7 != (uint)puStack80);
        puStack80 = (ulong *)((ulong)puStack80 & 0xffffffff00000000 | (ulong)(uint)puStack80);
        puStack112 = (ulong *)0x1052f4;
        factor_insert_multiplicity(puStack96,uVar12,1);
        puVar15 = (ulong *)(plStack152[10] * (long)param_2);
        uVar8 = uStack128;
        uVar12 = uStack136;
        param_1 = puStack88;
        param_3 = puStack96;
      } while (puVar15 < (ulong *)plStack152[0xb] || (long)puVar15 - (long)plStack152[0xb] == 0);
    }
    puVar15 = (ulong *)(plStack152[0xc] * (long)param_2);
    if (puVar15 < (ulong *)plStack152[0xd] || (long)puVar15 - (long)plStack152[0xd] == 0) {
      puStack80._0_4_ = iVar11 + 7;
      uVar8 = uStack128;
      uVar12 = uStack136;
      puStack96 = param_3;
      puStack88 = param_1;
      do {
        do {
          param_2 = puVar15;
          uVar7 = (int)uVar8 + 1;
          uVar12 = uVar12 + (byte)primes_diff[uVar8];
          uVar8 = (ulong)uVar7;
          puVar15 = param_2;
        } while (uVar7 != (uint)puStack80);
        puStack80 = (ulong *)((ulong)puStack80 & 0xffffffff00000000 | (ulong)(uint)puStack80);
        puStack112 = (ulong *)0x10528c;
        factor_insert_multiplicity(puStack96,uVar12,1);
        puVar15 = (ulong *)(plStack152[0xc] * (long)param_2);
        uVar8 = uStack128;
        uVar12 = uStack136;
        param_1 = puStack88;
        param_3 = puStack96;
      } while (puVar15 < (ulong *)plStack152[0xd] || (long)puVar15 - (long)plStack152[0xd] == 0);
    }
    uStack136 = uStack136 + *(byte *)(puVar21 + 0x22010);
    puVar16 = (ulong *)(uStack136 * uStack136);
    if (param_2 < puVar16) goto LAB_00105200;
    puVar16 = (ulong *)(ulong)(iVar11 + 7U);
    plStack152 = plStack152 + 0x10;
    uStack128 = (ulong)(iVar11 + 8);
    if (0x29b < iVar11 + 7U) goto LAB_00105200;
    puVar15 = *(ulong **)(puStack104 + (long)puVar16 * 0x10);
    puVar16 = *(ulong **)((long)(puStack104 + (long)puVar16 * 0x10) + 8);
  } while( true );
}