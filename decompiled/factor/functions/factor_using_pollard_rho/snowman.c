void factor_using_pollard_rho(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** v5;
    void** v6;
    void** r8_7;
    void** r9_8;
    void** r12_9;
    void** rcx10;
    int32_t esi11;
    void** rax12;
    void* rdx13;
    void* rdi14;
    uint1_t cf15;
    int64_t rbp16;
    int64_t r15_17;
    int64_t r14_18;
    void** rbp19;
    void** rcx20;
    void** r11_21;
    uint64_t rax22;
    int64_t rax23;
    void* rax24;
    void* rdx25;
    void* rax26;
    void** r8_27;
    void* r10_28;
    void* r9_29;
    void* rax30;
    void** r13_31;
    int64_t rax32;
    int64_t rax33;
    void** rdi34;
    void** rax35;
    int64_t rcx36;
    void* rdi37;
    void* rax38;
    void* r11_39;
    void* rsi40;
    void* rax41;
    void** rbx42;
    void** rsi43;
    void** rax44;
    void** rcx45;
    void** r11_46;
    void** rdx47;
    void** rax48;
    void** r8_49;
    signed char al50;
    void** rdx51;
    void** rsi52;
    void** rcx53;
    int1_t zf54;
    signed char al55;
    void** rdi56;
    int64_t rax57;
    void** r8_58;
    void*** r11_59;
    signed char* r10_60;
    uint32_t r9d61;
    int32_t ebx62;
    void* rcx63;
    void* rax64;
    int64_t rsi65;
    void* rax66;
    uint32_t r9d67;
    uint32_t eax68;

    v5 = rsi;
    v6 = rdx;
    if (reinterpret_cast<unsigned char>(rdi) <= reinterpret_cast<unsigned char>(1)) {
        addr_5b07_2:
        fun_2810("(1) < (n)", "src/factor.c", 0x5be, "factor_using_pollard_rho", r8_7, r9_8);
    } else {
        r12_9 = rdi;
        do {
            rcx10 = r12_9;
            esi11 = 64;
            *reinterpret_cast<int32_t*>(&rax12) = 1;
            *reinterpret_cast<int32_t*>(&rax12 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx13) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdi14 = reinterpret_cast<void*>(0);
            do {
                r8_7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) << 63);
                rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) >> 1);
                rdx13 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx13) >> 1 | reinterpret_cast<unsigned char>(r8_7));
                if (reinterpret_cast<unsigned char>(rcx10) < reinterpret_cast<unsigned char>(rax12) || rcx10 == rax12 && reinterpret_cast<uint64_t>(rdx13) <= reinterpret_cast<uint64_t>(rdi14)) {
                    cf15 = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi14) < reinterpret_cast<uint64_t>(rdx13));
                    rdi14 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi14) - reinterpret_cast<uint64_t>(rdx13));
                    rax12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax12) - (reinterpret_cast<unsigned char>(rcx10) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax12) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rcx10) + static_cast<uint64_t>(cf15))))));
                }
                --esi11;
            } while (esi11);
            *reinterpret_cast<int32_t*>(&rbp16) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp16) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r15_17) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_17) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r14_18) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_18) + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rbp16) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_9) - reinterpret_cast<uint64_t>(rdi14) > reinterpret_cast<uint64_t>(rdi14));
            rbp19 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(-rbp16) & reinterpret_cast<unsigned char>(r12_9)) + (reinterpret_cast<uint64_t>(rdi14) + reinterpret_cast<uint64_t>(rdi14) - reinterpret_cast<unsigned char>(r12_9)));
            rcx20 = rbp19;
            while (reinterpret_cast<unsigned char>(v5) < reinterpret_cast<unsigned char>(r12_9)) {
                r11_21 = rcx20;
                rax22 = reinterpret_cast<unsigned char>(r12_9) >> 1;
                *reinterpret_cast<uint32_t*>(&rax23) = *reinterpret_cast<uint32_t*>(&rax22) & 0x7f;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax23) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(0xd5c0 + rax23);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax24) + 4) = 0;
                rdx25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax24) + reinterpret_cast<int64_t>(rax24) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax24) * reinterpret_cast<int64_t>(rax24) * reinterpret_cast<unsigned char>(r12_9)));
                rax26 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx25) + reinterpret_cast<uint64_t>(rdx25) - reinterpret_cast<uint64_t>(rdx25) * reinterpret_cast<uint64_t>(rdx25) * reinterpret_cast<unsigned char>(r12_9));
                r8_27 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax26) + reinterpret_cast<uint64_t>(rax26) - reinterpret_cast<uint64_t>(rax26) * reinterpret_cast<uint64_t>(rax26) * reinterpret_cast<unsigned char>(r12_9));
                r10_28 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_9) - reinterpret_cast<unsigned char>(v5));
                r9_29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v5) - reinterpret_cast<unsigned char>(r12_9));
                while (1) {
                    rax30 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rax30 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax30) + reinterpret_cast<unsigned char>(r12_9));
                    }
                    rbp19 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rbp19) - (reinterpret_cast<unsigned char>(rbp19) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp19) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp19) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax30) < reinterpret_cast<uint64_t>(r10_28))))))) & reinterpret_cast<unsigned char>(r12_9)) + (reinterpret_cast<uint64_t>(rax30) + reinterpret_cast<uint64_t>(r9_29)));
                    r13_31 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        r13_31 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_31) + reinterpret_cast<unsigned char>(r12_9));
                    }
                    rax32 = r14_18;
                    *reinterpret_cast<uint32_t*>(&rax33) = *reinterpret_cast<uint32_t*>(&rax32) & 31;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
                    if (rax33 == 1) {
                        rdi34 = r13_31;
                        rax35 = gcd_odd(rdi34, r12_9);
                        if (!reinterpret_cast<int1_t>(rax35 == 1)) 
                            break;
                    }
                    --r14_18;
                    if (r14_18) 
                        continue;
                    rcx36 = r15_17 + r15_17;
                    if (!r15_17) {
                        r15_17 = rcx36;
                        r11_21 = rbp19;
                    } else {
                        do {
                            rdi37 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                            rax38 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi37) + reinterpret_cast<unsigned char>(r12_9));
                            if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                                rdi37 = rax38;
                            }
                            ++r14_18;
                        } while (r15_17 != r14_18);
                        r11_21 = rbp19;
                        r15_17 = rcx36;
                        rbp19 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax38) - (reinterpret_cast<uint64_t>(rax38) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax38) < reinterpret_cast<uint64_t>(rax38) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi37) < reinterpret_cast<uint64_t>(r10_28)))) & reinterpret_cast<unsigned char>(r12_9)) + (reinterpret_cast<uint64_t>(rdi37) + reinterpret_cast<uint64_t>(r9_29)));
                    }
                }
                r9_8 = r8_27;
                r8_7 = r11_21;
                r11_39 = r9_29;
                do {
                    rsi40 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(__intrinsic()) - reinterpret_cast<uint64_t>(__intrinsic()));
                    rax41 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi40) + reinterpret_cast<unsigned char>(r12_9));
                    if (reinterpret_cast<uint64_t>(__intrinsic()) < reinterpret_cast<uint64_t>(__intrinsic())) {
                        rsi40 = rax41;
                    }
                    rbx42 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(rax41) - (reinterpret_cast<uint64_t>(rax41) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax41) < reinterpret_cast<uint64_t>(rax41) + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rsi40) < reinterpret_cast<uint64_t>(r10_28)))) & reinterpret_cast<unsigned char>(r12_9)) + (reinterpret_cast<uint64_t>(rsi40) + reinterpret_cast<uint64_t>(r11_39)));
                    rsi43 = r12_9;
                    rdi34 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rdi34) - (reinterpret_cast<unsigned char>(rdi34) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rdi34) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rdi34) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_7) < reinterpret_cast<unsigned char>(rbx42))))))) & reinterpret_cast<unsigned char>(r12_9)) + (reinterpret_cast<unsigned char>(r8_7) - reinterpret_cast<unsigned char>(rbx42)));
                    rax44 = gcd_odd(rdi34, rsi43);
                } while (rax44 == 1);
                rcx45 = r8_7;
                r11_46 = rax44;
                if (rax44 == r12_9) 
                    goto addr_5af7_31;
                rdx47 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_9) % reinterpret_cast<unsigned char>(r11_46));
                rax48 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_9) / reinterpret_cast<unsigned char>(r11_46));
                r8_49 = rax48;
                r12_9 = rax48;
                if (reinterpret_cast<unsigned char>(r11_46) > reinterpret_cast<unsigned char>(1) && (reinterpret_cast<unsigned char>(r11_46) <= reinterpret_cast<unsigned char>(0x17ded78) || (al50 = prime_p_part_0(r11_46, rsi43, rdx47, rcx45), r11_46 = r11_46, rcx45 = rcx45, r8_49 = rax48, !!al50))) {
                    *reinterpret_cast<int32_t*>(&rdx51) = 1;
                    *reinterpret_cast<int32_t*>(&rdx51 + 4) = 0;
                    rsi52 = r11_46;
                    factor_insert_multiplicity(v6, rsi52, 1);
                    r8_7 = r8_49;
                    rcx53 = rcx45;
                    zf54 = reinterpret_cast<int1_t>(r8_7 == 1);
                    if (reinterpret_cast<unsigned char>(r8_7) <= reinterpret_cast<unsigned char>(1)) 
                        goto addr_5aca_34; else 
                        goto addr_5a2f_35;
                }
                rdx51 = v6;
                rsi52 = v5 + 1;
                factor_using_pollard_rho(r11_46, rsi52, rdx51, rcx45);
                r8_7 = r8_49;
                rcx53 = rcx45;
                zf54 = reinterpret_cast<int1_t>(r8_7 == 1);
                if (reinterpret_cast<unsigned char>(r8_7) > reinterpret_cast<unsigned char>(1)) {
                    addr_5a2f_35:
                    if (reinterpret_cast<unsigned char>(r8_7) <= reinterpret_cast<unsigned char>(0x17ded78)) 
                        goto addr_5ad7_37;
                    al55 = prime_p_part_0(r8_7, rsi52, rdx51, rcx53);
                    r8_7 = r8_7;
                    if (al55) 
                        goto addr_5ad7_37;
                } else {
                    addr_5aca_34:
                    if (zf54) 
                        goto addr_5b45_39; else 
                        goto addr_5acc_40;
                }
                rbp19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp19) % reinterpret_cast<unsigned char>(r8_7));
                rcx20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx53) % reinterpret_cast<unsigned char>(r8_7));
                continue;
                addr_5acc_40:
                *reinterpret_cast<int32_t*>(&rcx20) = 0;
                *reinterpret_cast<int32_t*>(&rcx20 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rbp19) = 0;
                *reinterpret_cast<int32_t*>(&rbp19 + 4) = 0;
            }
            break;
            addr_5af7_31:
            ++v5;
        } while (reinterpret_cast<unsigned char>(r12_9) > reinterpret_cast<unsigned char>(1));
        goto addr_5b07_2;
    }
    fun_2810("a < n", "src/factor.c", 0x5c4, "factor_using_pollard_rho", r8_7, r9_8);
    addr_5b45_39:
    return;
    addr_5ad7_37:
    rdi56 = v6;
    *reinterpret_cast<uint32_t*>(&rax57) = *reinterpret_cast<unsigned char*>(rdi56 + 0xfa);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax57) + 4) = 0;
    r8_58 = r8_7;
    r11_59 = reinterpret_cast<void***>(rdi56 + 16);
    r10_60 = reinterpret_cast<signed char*>(rdi56 + 0xe0);
    r9d61 = *reinterpret_cast<uint32_t*>(&rax57);
    if (!*reinterpret_cast<uint32_t*>(&rax57)) 
        goto addr_2fa1_44;
    ebx62 = static_cast<int32_t>(rax57 - 1);
    rcx63 = reinterpret_cast<void*>(static_cast<int64_t>(ebx62));
    rax64 = rcx63;
    do {
        *reinterpret_cast<int32_t*>(&rsi65) = *reinterpret_cast<int32_t*>(&rax64);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi65) + 4) = 0;
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi56 + reinterpret_cast<uint64_t>(rax64) * 8) + 16)) <= reinterpret_cast<unsigned char>(r8_58)) 
            break;
        rax64 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax64) - 1);
        *reinterpret_cast<int32_t*>(&rsi65) = *reinterpret_cast<int32_t*>(&rax64);
    } while (*reinterpret_cast<int32_t*>(&rax64) != -1);
    goto addr_2f80_48;
    if (*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi56 + reinterpret_cast<uint64_t>(rax64) * 8) + 16) == r8_58) {
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_60) + reinterpret_cast<uint64_t>(rax64)) = reinterpret_cast<signed char>(*reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_60) + reinterpret_cast<uint64_t>(rax64)) + 1);
        return;
    }
    rax66 = reinterpret_cast<void*>(static_cast<int64_t>(static_cast<int32_t>(rsi65 + 1)));
    r11_59 = r11_59 + reinterpret_cast<uint64_t>(rax66) * 8;
    r10_60 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r10_60) + reinterpret_cast<uint64_t>(rax66));
    if (*reinterpret_cast<int32_t*>(&rsi65) >= ebx62) {
        addr_2fa1_44:
        r9d67 = r9d61 + 1;
        *r11_59 = r8_58;
        *r10_60 = 1;
        *reinterpret_cast<unsigned char*>(rdi56 + 0xfa) = *reinterpret_cast<unsigned char*>(&r9d67);
        return;
    }
    do {
        addr_2f80_48:
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi56 + reinterpret_cast<uint64_t>(rcx63) * 8) + 24) = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi56 + reinterpret_cast<uint64_t>(rcx63) * 8) + 16);
        eax68 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi56) + reinterpret_cast<uint64_t>(rcx63) + 0xe0);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi56) + reinterpret_cast<uint64_t>(rcx63) + 0xe1) = *reinterpret_cast<signed char*>(&eax68);
        rcx63 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx63) - 1);
    } while (*reinterpret_cast<int32_t*>(&rsi65) < *reinterpret_cast<int32_t*>(&rcx63));
    goto addr_2fa1_44;
}