void factor_using_pollard_rho(uint64_t n, int64_t a, int32_t * factors) {
    // 0x5740
    int64_t v1; // 0x5740
    uint64_t v2; // 0x5740
    int64_t v3; // 0x5740
    int64_t v4; // 0x5740
    int64_t v5; // 0x5740
    int64_t v6; // 0x5740
    int64_t v7; // 0x5740
    int64_t v8; // 0x5740
    int64_t v9; // 0x5740
    int64_t v10; // 0x5740
    uint64_t v11; // 0x5740
    int64_t v12; // 0x5740
    int64_t v13; // 0x5740
    int64_t v14; // 0x5740
    int32_t v15; // 0x5740
    uint64_t v16; // 0x5740
    uint64_t v17; // 0x578a
    uint64_t v18; // 0x578d
    int64_t v19; // 0x5740
    if (n < 2) {
      lab_0x5b07:
        // 0x5b07
        function_2810();
    } else {
        while (true) {
            // 0x5765
            v7 = 1;
            v9 = n;
            v14 = 0;
            v15 = 64;
            v12 = 0;
            while (true) {
              lab_0x5780:;
                uint64_t v20 = v12;
                int64_t v21 = v7;
                v17 = v9 / 2;
                v18 = v14 / 2 | 0x8000000000000000 * v9;
                if (v21 > v17) {
                    // 0x579c
                    v8 = v21 - v17 + (int64_t)(v20 < v18);
                    v13 = v20 - v18;
                    goto lab_0x57a2;
                } else {
                    // 0x5795
                    v8 = v21;
                    v13 = v20;
                    if (v21 != v17 || v20 < v18) {
                        goto lab_0x57a2;
                    } else {
                        // 0x579c
                        v8 = v21 - v17 + (int64_t)(v20 < v18);
                        v13 = v20 - v18;
                        goto lab_0x57a2;
                    }
                }
            }
          lab_0x57a7:;
            int64_t v22; // 0x5740
            if (v16 >= v22) {
                // break -> 0x5b26
                break;
            }
            // 0x57eb
            int64_t v23; // 0x5740
            int64_t v24 = v22 - v23 > v23 ? v22 : 0; // 0x57cf
            v19 = v16 + 1;
            v5 = 1;
            v3 = 1;
            v1 = v22;
            v10 = v24 + 2 * v23 - v22;
            while (true) {
              lab_0x57eb:
                // 0x57eb
                v11 = v10;
                v2 = v1;
                int64_t v25 = v3; // 0x583d
                int64_t v26 = v5; // 0x583d
                while (true) {
                    // 0x5840
                    v6 = v26;
                    v4 = v25;
                    if (v4 % 32 == 1) {
                        // 0x5920
                        if (gcd_odd(0, v2) != 1) {
                            goto lab_0x5950;
                        }
                    }
                    int64_t v27 = v4 - 1; // 0x58b0
                    while (v27 != 0) {
                        // 0x5840
                        v4 = v27;
                        if (v4 % 32 == 1) {
                            // 0x5920
                            if (gcd_odd(0, v2) != 1) {
                                goto lab_0x5950;
                            }
                        }
                        // 0x58b0
                        v27 = v4 - 1;
                    }
                    int64_t v28 = v27; // 0x58bd
                    int64_t v29 = v27; // 0x58bd
                    if (v6 != 0) {
                        int64_t v30 = v29 + 1; // 0x58f7
                        v28 = v30;
                        while (v6 != v30) {
                            // 0x58d0
                            v30++;
                            v28 = v30;
                        }
                    }
                    // 0x5840
                    v25 = v28;
                    v26 = 2 * v6;
                }
                goto lab_0x59a1;
            }
          lab_0x5af7:
            // 0x5af7
            if (v2 < 2) {
                goto lab_0x5b07;
            }
        }
    }
  lab_0x5b26:
    // 0x5b26
    function_2810();
  lab_0x5950:;
    uint64_t v31 = gcd_odd(0, v2); // 0x5996
    if (v31 == 1) {
        goto lab_0x5950;
    } else {
        goto lab_0x59a1;
    }
  lab_0x59a1:
    if (v31 == v2) {
        // break -> 0x5af7
        goto lab_0x5af7;
    }
    uint64_t v32 = v2 / v31;
    if (v31 < 2) {
        goto lab_0x5a96;
    } else {
        if (v31 < 0x17ded79) {
            goto lab_0x59ff;
        } else {
            // 0x59d1
            if ((char)prime_p_part_0(v31) == 0) {
                goto lab_0x5a96;
            } else {
                goto lab_0x59ff;
            }
        }
    }
  lab_0x5a96:
    // 0x5a96
    factor_using_pollard_rho(v31, v19, factors);
    if (v32 < 2) {
        goto lab_0x5aca;
    } else {
        goto lab_0x5a2f;
    }
  lab_0x5aca:;
    int64_t v33 = 0; // 0x5aca
    if (v32 == 1) {
        // 0x5b45
        return;
    }
    goto lab_0x57e0;
  lab_0x5a2f:
    if (v32 < 0x17ded79 || (char)prime_p_part_0(v32) != 0) {
        // 0x5ad7
        factor_insert_multiplicity(factors, v32, 1);
        return;
    }
    // 0x5a5c
    v33 = v11 % v32;
    goto lab_0x57e0;
  lab_0x59ff:
    // 0x59ff
    factor_insert_multiplicity(factors, v31, 1);
    if (v32 < 2) {
        goto lab_0x5aca;
    } else {
        goto lab_0x5a2f;
    }
  lab_0x57e0:
    // 0x57e0
    v5 = v6;
    v3 = v4;
    v1 = v32;
    v10 = v33;
    if (v16 >= v32) {
        // break (via goto) -> 0x5b26
        goto lab_0x5b26;
    }
    goto lab_0x57eb;
  lab_0x57a2:;
    int32_t v34 = v15 - 1; // 0x57a2
    v7 = v8;
    v9 = v17;
    v14 = v18;
    v15 = v34;
    v12 = v13;
    if (v34 == 0) {
        // break -> 0x57a7
        goto lab_0x57a7;
    }
    goto lab_0x5780;
}