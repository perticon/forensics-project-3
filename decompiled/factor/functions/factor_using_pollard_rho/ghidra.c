void factor_using_pollard_rho(ulong param_1,ulong param_2,undefined8 param_3)

{
  undefined auVar1 [16];
  char cVar2;
  ulong uVar3;
  long lVar4;
  ulong uVar5;
  ulong uVar6;
  long lVar7;
  ulong uVar8;
  ulong uVar9;
  int iVar10;
  ulong uVar11;
  ulong uVar12;
  long lVar13;
  long lVar14;
  long lVar15;
  bool bVar16;
  ulong local_60;
  
  local_60 = param_2;
  if (1 < param_1) {
    do {
      uVar11 = 0;
      iVar10 = 0x40;
      uVar3 = 1;
      uVar6 = 0;
      uVar5 = param_1;
      do {
        uVar12 = uVar5 << 0x3f;
        uVar5 = uVar5 >> 1;
        uVar6 = uVar6 >> 1 | uVar12;
        if ((uVar5 < uVar3) || ((uVar5 == uVar3 && (uVar6 <= uVar11)))) {
          bVar16 = uVar11 < uVar6;
          uVar11 = uVar11 - uVar6;
          uVar3 = (uVar3 - uVar5) - (ulong)bVar16;
        }
        iVar10 = iVar10 + -1;
      } while (iVar10 != 0);
      lVar15 = 1;
      lVar14 = 1;
      uVar6 = (-(ulong)(uVar11 < param_1 - uVar11) & param_1) + (uVar11 * 2 - param_1);
      uVar5 = uVar6;
      uVar3 = uVar6;
      while( true ) {
        if (param_1 <= local_60) {
                    /* WARNING: Subroutine does not return */
          __assert_fail("a < n","src/factor.c",0x5c4,"factor_using_pollard_rho");
        }
        uVar12 = (ulong)(byte)binvert_table[(uint)(param_1 >> 1) & 0x7f];
        lVar7 = uVar12 * 2 - uVar12 * uVar12 * param_1;
        lVar7 = lVar7 * 2 - lVar7 * lVar7 * param_1;
        lVar13 = lVar7 * 2 - lVar7 * lVar7 * param_1;
        uVar12 = param_1 - local_60;
        lVar4 = local_60 - param_1;
        lVar7 = lVar15;
        while( true ) {
          lVar15 = lVar7;
          uVar8 = SUB168(ZEXT816(uVar3) * ZEXT816(uVar3) >> 0x40,0);
          uVar9 = SUB168(ZEXT816((ulong)(SUB168(ZEXT816(uVar3) * ZEXT816(uVar3),0) * lVar13)) *
                         ZEXT816(param_1) >> 0x40,0);
          uVar3 = uVar8 - uVar9;
          if (uVar8 < uVar9) {
            uVar3 = uVar3 + param_1;
          }
          uVar3 = (-(ulong)(uVar3 < uVar12) & param_1) + uVar3 + lVar4;
          auVar1 = ZEXT816(uVar11) * ZEXT816((-(ulong)(uVar6 < uVar3) & param_1) + (uVar6 - uVar3));
          uVar8 = SUB168(auVar1 >> 0x40,0);
          uVar9 = SUB168(ZEXT816((ulong)(SUB168(auVar1,0) * lVar13)) * ZEXT816(param_1) >> 0x40,0);
          uVar11 = uVar8 - uVar9;
          if (uVar8 < uVar9) {
            uVar11 = uVar11 + param_1;
          }
          uVar8 = uVar5;
          if ((((uint)lVar14 & 0x1f) == 1) &&
             (lVar7 = gcd_odd(uVar11,param_1), uVar8 = uVar3, lVar7 != 1)) break;
          lVar14 = lVar14 + -1;
          uVar5 = uVar8;
          lVar7 = lVar15;
          if ((lVar14 == 0) && (uVar5 = uVar3, uVar6 = uVar3, lVar7 = lVar15 * 2, lVar15 != 0)) {
            do {
              uVar3 = SUB168(ZEXT816(uVar5) * ZEXT816(uVar5) >> 0x40,0);
              uVar8 = SUB168(ZEXT816((ulong)(SUB168(ZEXT816(uVar5) * ZEXT816(uVar5),0) * lVar13)) *
                             ZEXT816(param_1) >> 0x40,0);
              uVar5 = uVar3 - uVar8;
              if (uVar3 < uVar8) {
                uVar5 = uVar5 + param_1;
              }
              lVar14 = lVar14 + 1;
              uVar5 = (-(ulong)(uVar5 < uVar12) & param_1) + uVar5 + lVar4;
              uVar3 = uVar5;
            } while (lVar15 != lVar14);
          }
        }
        do {
          uVar8 = SUB168(ZEXT816(uVar5) * ZEXT816(uVar5) >> 0x40,0);
          uVar9 = SUB168(ZEXT816((ulong)(SUB168(ZEXT816(uVar5) * ZEXT816(uVar5),0) * lVar13)) *
                         ZEXT816(param_1) >> 0x40,0);
          uVar5 = uVar8 - uVar9;
          if (uVar8 < uVar9) {
            uVar5 = uVar5 + param_1;
          }
          uVar5 = (-(ulong)(uVar5 < uVar12) & param_1) + uVar5 + lVar4;
          uVar8 = gcd_odd((-(ulong)(uVar6 < uVar5) & param_1) + (uVar6 - uVar5),param_1);
        } while (uVar8 == 1);
        if (uVar8 == param_1) break;
        param_1 = param_1 / uVar8;
        if ((uVar8 < 2) || ((0x17ded78 < uVar8 && (cVar2 = prime_p_part_0(uVar8), cVar2 == '\0'))))
        {
          factor_using_pollard_rho(uVar8,local_60 + 1,param_3);
        }
        else {
          factor_insert_multiplicity(param_3,uVar8,1);
        }
        if (param_1 < 2) {
          if (param_1 == 1) {
            return;
          }
          uVar5 = 0;
          uVar6 = 0;
          uVar3 = 0;
        }
        else {
          if ((param_1 < 0x17ded79) || (cVar2 = prime_p_part_0(param_1), cVar2 != '\0')) {
            factor_insert_multiplicity(param_3,param_1,1);
            return;
          }
          uVar3 = uVar3 % param_1;
          uVar6 = uVar6 % param_1;
          uVar5 = uVar5 % param_1;
        }
      }
      local_60 = local_60 + 1;
    } while (1 < param_1);
  }
                    /* WARNING: Subroutine does not return */
  __assert_fail("(1) < (n)","src/factor.c",0x5be,"factor_using_pollard_rho");
}