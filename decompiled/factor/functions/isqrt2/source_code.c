isqrt2 (uintmax_t nh, uintmax_t nl)
{
  unsigned int shift;
  uintmax_t x;

  /* Ensures the remainder fits in an uintmax_t.  */
  assert (nh < ((uintmax_t) 1 << (W_TYPE_SIZE - 2)));

  if (nh == 0)
    return isqrt (nl);

  count_leading_zeros (shift, nh);
  shift &= ~1;

  /* Make x > sqrt (n).  */
  x = isqrt ((nh << shift) + (nl >> (W_TYPE_SIZE - shift))) + 1;
  x <<= (W_TYPE_SIZE - shift) / 2;

  /* Do we need more than one iteration?  */
  for (;;)
    {
      MAYBE_UNUSED uintmax_t r;
      uintmax_t q, y;
      udiv_qrnnd (q, r, nh, nl, x);
      y = (x + q) / 2;

      if (y >= x)
        {
          uintmax_t hi, lo;
          umul_ppmm (hi, lo, x + 1, x + 1);
          assert (gt2 (hi, lo, nh, nl));

          umul_ppmm (hi, lo, x, x);
          assert (ge2 (nh, nl, hi, lo));
          sub_ddmmss (hi, lo, nh, nl, hi, lo);
          assert (hi == 0);

          return x;
        }

      x = y;
    }
}