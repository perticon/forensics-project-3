void lbuf_putc(char c) {
    int64_t v1 = g26; // 0x38b0
    int64_t v2 = v1 + 1; // 0x38ba
    g26 = v2;
    *(char *)v1 = c;
    if (c != 10) {
        // 0x38cf
        return;
    }
    int32_t v3 = g15; // 0x38e8
    if (g15 == -1) {
        // 0x3950
        if ((int32_t)function_2670() != 0) {
            // 0x395b
            g15 = 1;
            // 0x3965
            lbuf_flush();
            return;
        }
        // 0x3970
        v3 = (int32_t)function_2670() != 0;
        g15 = v3;
    }
    // 0x38ea
    if (v3 != 0) {
        // 0x3965
        lbuf_flush();
        return;
    }
    if (v2 - lbuf < 512) {
        // 0x38cf
        return;
    }
    int64_t v4 = lbuf + 512;
    int64_t v5 = v4 - 1; // 0x390b
    while (*(char *)v5 != 10) {
        // 0x3908
        v4 = v5;
        v5 = v4 - 1;
    }
    // 0x3914
    g26 = v4;
    lbuf_flush();
    g26 = v2 - v4 + function_28d0();
}