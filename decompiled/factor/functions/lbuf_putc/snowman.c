void lbuf_putc(void** dil, ...) {
    int64_t rdi1;
    void** rax2;
    void** rbp3;
    uint32_t eax4;
    void** rbx5;
    int32_t eax6;
    int32_t eax7;
    void** rax8;
    void** r12_9;
    void** rbp10;
    void** rcx11;
    void** r8_12;
    void** rax13;

    *reinterpret_cast<void***>(&rdi1) = dil;
    rax2 = g150f8;
    rbp3 = rax2 + 1;
    g150f8 = rbp3;
    *reinterpret_cast<void***>(rax2) = *reinterpret_cast<void***>(&rdi1);
    if (*reinterpret_cast<void***>(&rdi1) == 10) {
        eax4 = line_buffered_0;
        rbx5 = lbuf;
        if (eax4 == 0xffffffff) {
            eax6 = fun_2670();
            if (!eax6) {
                *reinterpret_cast<int32_t*>(&rdi1) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi1) + 4) = 0;
                eax7 = fun_2670();
                eax4 = reinterpret_cast<uint1_t>(!!eax7);
                line_buffered_0 = eax4;
            } else {
                line_buffered_0 = 1;
                goto addr_3965_6;
            }
        }
        if (eax4) {
            addr_3965_6:
        } else {
            if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp3) - reinterpret_cast<unsigned char>(rbx5)) > 0x1ff) {
                rax8 = rbx5 + 0x200;
                do {
                    r12_9 = rax8;
                    --rax8;
                } while (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax8) == 10));
                g150f8 = r12_9;
                rbp10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp3) - reinterpret_cast<unsigned char>(r12_9));
                lbuf_flush(rdi1);
                rcx11 = lbuf;
                rax13 = fun_28d0(rcx11, r12_9, rbp10, rcx11, r8_12);
                g150f8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax13) + reinterpret_cast<unsigned char>(rbp10));
                return;
            }
        }
    } else {
        return;
    }
}