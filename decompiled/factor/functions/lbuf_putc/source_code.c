lbuf_putc (char c)
{
  *lbuf.end++ = c;

  if (c == '\n')
    {
      size_t buffered = lbuf.end - lbuf.buf;

      /* Provide immediate output for interactive use.  */
      static int line_buffered = -1;
      if (line_buffered == -1)
        line_buffered = isatty (STDIN_FILENO) || isatty (STDOUT_FILENO);
      if (line_buffered)
        lbuf_flush ();
      else if (buffered >= FACTOR_PIPE_BUF)
        {
          /* Write output in <= PIPE_BUF chunks
             so consumers can read atomically.  */
          char const *tend = lbuf.end;

          /* Since a umaxint_t's factors must fit in 512
             we're guaranteed to find a newline here.  */
          char *tlend = lbuf.buf + FACTOR_PIPE_BUF;
          while (*--tlend != '\n');
          tlend++;

          lbuf.end = tlend;
          lbuf_flush ();

          /* Buffer the remainder.  */
          memcpy (lbuf.buf, tlend, tend - tlend);
          lbuf.end = lbuf.buf + (tend - tlend);
        }
    }
}