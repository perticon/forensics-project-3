void lbuf_putc(char param_1)

{
  char *pcVar1;
  char *pcVar2;
  int iVar3;
  char *__src;
  void *pvVar4;
  
  pcVar1 = lbuf._8_8_ + 1;
  pcVar2 = pcVar1;
  *lbuf._8_8_ = param_1;
  lbuf._8_8_ = pcVar2;
  pvVar4 = lbuf._0_8_;
  if (param_1 == '\n') {
    if (line_buffered_0 == 0xffffffff) {
      iVar3 = isatty(0);
      if (iVar3 != 0) {
        line_buffered_0 = 1;
        goto LAB_00103965;
      }
      iVar3 = isatty(1);
      line_buffered_0 = (uint)(iVar3 != 0);
    }
    if (line_buffered_0 != 0) {
LAB_00103965:
      lbuf_flush();
      return;
    }
    if (0x1ff < (ulong)((long)pcVar1 - (long)pvVar4)) {
      pcVar2 = (char *)((long)pvVar4 + 0x200);
      do {
        __src = pcVar2;
        pcVar2 = __src + -1;
      } while (__src[-1] != '\n');
      lbuf._8_8_ = __src;
      lbuf_flush();
      pvVar4 = memcpy(lbuf._0_8_,__src,(long)pcVar1 - (long)__src);
      lbuf._8_8_ = (char *)((long)pvVar4 + ((long)pcVar1 - (long)__src));
      return;
    }
  }
  return;
}