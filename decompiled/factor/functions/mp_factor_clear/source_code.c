mp_factor_clear (struct mp_factors *factors)
{
  for (unsigned int i = 0; i < factors->nfactors; i++)
    mpz_clear (factors->p[i]);

  free (factors->p);
  free (factors->e);
}