uint main(int param_1,undefined8 *param_2)

{
  char cVar1;
  int iVar2;
  uint uVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  long lVar6;
  uint uVar7;
  long in_FS_OFFSET;
  undefined auStack72 [8];
  void *local_40;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  if (lbuf._0_8_ == 0) {
    lbuf._0_8_ = xmalloc(0x400);
    lbuf._8_8_ = lbuf._0_8_;
  }
  atexit(close_stdout);
  atexit(lbuf_flush);
LAB_00102c30:
  do {
    iVar2 = getopt_long(param_1,param_2,"h",long_options,0);
    if (iVar2 == -1) {
      lVar6 = (long)optind;
      if (optind < param_1) {
        uVar7 = 1;
        do {
          cVar1 = print_factors(param_2[lVar6]);
          if (cVar1 == '\0') {
            uVar7 = 0;
          }
          lVar6 = lVar6 + 1;
        } while ((int)lVar6 < param_1);
      }
      else {
        uVar7 = 1;
        init_tokenbuffer(auStack72);
        while( true ) {
          lVar6 = readtoken(stdin,&DAT_0010d1ac,3,auStack72);
          if (lVar6 == -1) break;
          uVar3 = print_factors();
          uVar7 = uVar7 & uVar3;
        }
        free(local_40);
      }
      if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
        return uVar7 ^ 1;
      }
LAB_00102dc0:
                    /* WARNING: Subroutine does not return */
      __stack_chk_fail();
    }
    if (iVar2 == 0x68) {
LAB_00102cf0:
      print_exponents = 1;
      goto LAB_00102c30;
    }
    if (iVar2 < 0x69) {
      if (iVar2 == -0x83) {
        uVar4 = proper_name_utf8("Niels Moller",&DAT_0010d161);
        uVar5 = proper_name_utf8("Torbjorn Granlund",&DAT_0010d17c);
        version_etc(stdout,"factor","GNU coreutils",Version,"Paul Rubin",uVar5,uVar4,0);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar2 == -0x82) {
        usage();
        goto LAB_00102cf0;
      }
LAB_00102db6:
      usage(1);
      goto LAB_00102dc0;
    }
    if (iVar2 != 0x80) goto LAB_00102db6;
    dev_debug = 1;
  } while( true );
}