ulong mulredc2(ulong *param_1,ulong param_2,ulong param_3,ulong param_4,ulong param_5,ulong param_6,
              ulong param_7,long param_8)

{
  undefined auVar1 [16];
  long lVar2;
  ulong uVar3;
  ulong uVar4;
  ulong uVar5;
  ulong uVar6;
  ulong uVar7;
  ulong uVar8;
  ulong uVar9;
  ulong uVar10;
  ulong uVar11;
  bool bVar12;
  
  if ((long)param_2 < 0) {
                    /* WARNING: Subroutine does not return */
    __assert_fail("(a1 >> (W_TYPE_SIZE - 1)) == 0","src/factor.c",0x3e5,__PRETTY_FUNCTION___6);
  }
  if (-1 < (long)param_4) {
    if (-1 < (long)param_6) {
      uVar6 = SUB168(ZEXT816(param_3) * ZEXT816(param_5) >> 0x40,0);
      lVar2 = SUB168(ZEXT816(param_3) * ZEXT816(param_5),0);
      uVar11 = -param_8 * lVar2;
      uVar7 = SUB168(ZEXT816(uVar11) * ZEXT816(param_7) >> 0x40,0);
      auVar1 = ZEXT816(uVar11) * ZEXT816(param_6);
      uVar11 = SUB168(auVar1,0);
      uVar3 = (SUB168(ZEXT816(param_3) * ZEXT816(param_4),0) + 1) - (ulong)(lVar2 == 0);
      uVar4 = uVar3 + uVar7;
      uVar5 = uVar4 + uVar6;
      uVar10 = uVar5 + uVar11;
      uVar8 = SUB168(ZEXT816(param_3) * ZEXT816(param_4) >> 0x40,0) + (ulong)CARRY8(uVar3,uVar7) +
              (ulong)CARRY8(uVar4,uVar6) + SUB168(auVar1 >> 0x40,0) + (ulong)CARRY8(uVar5,uVar11);
      uVar11 = SUB168(ZEXT816(param_2) * ZEXT816(param_5),0);
      lVar2 = uVar11 + uVar10;
      uVar9 = SUB168(ZEXT816(param_2) * ZEXT816(param_5) >> 0x40,0) + (ulong)CARRY8(uVar11,uVar10);
      uVar3 = -param_8 * lVar2;
      uVar11 = SUB168(ZEXT816(param_2) * ZEXT816(param_4),0);
      uVar10 = SUB168(ZEXT816(uVar3) * ZEXT816(param_7) >> 0x40,0);
      auVar1 = ZEXT816(uVar3) * ZEXT816(param_6);
      uVar3 = SUB168(auVar1,0);
      uVar5 = (uVar11 + uVar8 + 1) - (ulong)(lVar2 == 0);
      uVar6 = uVar5 + uVar10;
      uVar7 = uVar6 + uVar9;
      uVar4 = uVar7 + uVar3;
      uVar11 = SUB168(ZEXT816(param_2) * ZEXT816(param_4) >> 0x40,0) + (ulong)CARRY8(uVar11,uVar8) +
               (ulong)CARRY8(uVar5,uVar10) + (ulong)CARRY8(uVar6,uVar9) + SUB168(auVar1 >> 0x40,0) +
               (ulong)CARRY8(uVar7,uVar3);
      if ((param_6 < uVar11) || ((param_6 == uVar11 && (param_7 <= uVar4)))) {
        bVar12 = uVar4 < param_7;
        uVar4 = uVar4 - param_7;
        uVar11 = (uVar11 - param_6) - (ulong)bVar12;
      }
      *param_1 = uVar11;
      return uVar4;
    }
                    /* WARNING: Subroutine does not return */
    __assert_fail("(m1 >> (W_TYPE_SIZE - 1)) == 0","src/factor.c",999,__PRETTY_FUNCTION___6);
  }
                    /* WARNING: Subroutine does not return */
  __assert_fail("(b1 >> (W_TYPE_SIZE - 1)) == 0","src/factor.c",0x3e6,__PRETTY_FUNCTION___6);
}