void** mulredc2(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, void** a8) {
    int64_t rbp9;
    uint64_t r12_10;
    uint64_t rcx11;
    uint64_t rcx12;
    uint64_t tmp64_13;
    uint64_t tmp64_14;
    uint64_t tmp64_15;
    uint64_t r13_16;
    uint64_t tmp64_17;
    void* rcx18;
    void* tmp64_19;
    void** rcx20;
    void** tmp64_21;
    void** tmp64_22;
    void** tmp64_23;
    void** rsi24;
    void** rdx25;
    void** rax26;
    void** r12_27;
    void** rbx28;
    void** rax29;
    void** r8_30;
    void** r10_31;
    void** r14_32;
    void** r13_33;
    void** v34;
    int32_t ebx35;
    void** v36;
    void** rbp37;
    void** rax38;
    void** rax39;
    void** rax40;
    uint64_t rbx41;
    void** rax42;
    void** rax43;
    void** rax44;
    int64_t v45;

    rbp9 = reinterpret_cast<int64_t>(-reinterpret_cast<unsigned char>(a8));
    if (reinterpret_cast<signed char>(rsi) < reinterpret_cast<signed char>(0)) {
        addr_3217_2:
        fun_2810("(a1 >> (W_TYPE_SIZE - 1)) == 0", "src/factor.c", 0x3e5, "mulredc2", r8, r9);
    } else {
        if (reinterpret_cast<signed char>(rcx) < reinterpret_cast<signed char>(0)) {
            addr_31f8_4:
            fun_2810("(b1 >> (W_TYPE_SIZE - 1)) == 0", "src/factor.c", 0x3e6, "mulredc2", r8, r9);
            goto addr_3217_2;
        } else {
            if (reinterpret_cast<signed char>(r9) < reinterpret_cast<signed char>(0)) {
                fun_2810("(m1 >> (W_TYPE_SIZE - 1)) == 0", "src/factor.c", 0x3e7, "mulredc2", r8, r9);
                goto addr_31f8_4;
            } else {
                r12_10 = reinterpret_cast<unsigned char>(rdx) * reinterpret_cast<unsigned char>(r8);
                rcx11 = reinterpret_cast<unsigned char>(rdx) * reinterpret_cast<unsigned char>(rcx);
                rcx12 = rcx11 - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(rcx11 < 1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(r12_10 < 1)))));
                tmp64_13 = rcx12 + __intrinsic();
                tmp64_14 = tmp64_13 + __intrinsic();
                tmp64_15 = tmp64_14 + rbp9 * r12_10 * reinterpret_cast<unsigned char>(r9);
                r13_16 = reinterpret_cast<unsigned char>(rsi) * reinterpret_cast<unsigned char>(r8);
                tmp64_17 = r13_16 + tmp64_15;
                rcx18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rsi) * reinterpret_cast<unsigned char>(rcx));
                tmp64_19 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx18) + (reinterpret_cast<int64_t>(__intrinsic()) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(tmp64_13 < rcx12)) + reinterpret_cast<uint1_t>(tmp64_14 < tmp64_13) + reinterpret_cast<uint64_t>(__intrinsic()) + reinterpret_cast<uint1_t>(tmp64_15 < tmp64_14)));
                rcx20 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(tmp64_19) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(tmp64_19) < 1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(tmp64_17 < 1))))));
                tmp64_21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx20) + reinterpret_cast<uint64_t>(__intrinsic()));
                tmp64_22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_21) + (reinterpret_cast<int64_t>(__intrinsic()) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(tmp64_17 < r13_16))));
                tmp64_23 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_22) + rbp9 * tmp64_17 * reinterpret_cast<unsigned char>(r9));
                rsi24 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__intrinsic()) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(tmp64_19) < reinterpret_cast<uint64_t>(rcx18))) + (reinterpret_cast<unsigned char>(tmp64_21) < reinterpret_cast<unsigned char>(rcx20)) + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_22) < reinterpret_cast<unsigned char>(tmp64_21)) + reinterpret_cast<uint64_t>(__intrinsic()) + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_23) < reinterpret_cast<unsigned char>(tmp64_22)));
                rdx25 = rsi24;
                rax26 = tmp64_23;
                if (reinterpret_cast<unsigned char>(r9) < reinterpret_cast<unsigned char>(rsi24)) 
                    goto addr_31ac_8;
                if (r9 != rsi24) 
                    goto addr_31b8_10; else 
                    goto addr_31d2_11;
            }
        }
    }
    r12_27 = r8;
    rbx28 = *reinterpret_cast<void***>(r9);
    rax29 = *reinterpret_cast<void***>(r9 + 8);
    r8_30 = gd004;
    r10_31 = gd00c;
    r14_32 = __PRETTY_FUNCTION___6;
    r13_33 = gd5a0;
    v34 = rbx28;
    ebx35 = 64;
    v36 = rax29;
    rbp37 = g3e5;
    do {
        if (*reinterpret_cast<unsigned char*>(&rbp37) & 1) {
            rax38 = mulredc2("(a1 >> (W_TYPE_SIZE - 1)) == 0", v36, v34, r10_31, r8_30, r13_33, r14_32, r12_27);
            v34 = rax38;
            rax39 = gd1d0;
            v36 = rax39;
            r8_30 = r8_30;
            r10_31 = r10_31;
        }
        rbp37 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp37) >> 1);
        rax40 = mulredc2("(a1 >> (W_TYPE_SIZE - 1)) == 0", r10_31, r8_30, r10_31, r8_30, r13_33, r14_32, r12_27);
        r10_31 = gd1d0;
        r8_30 = rax40;
        --ebx35;
    } while (ebx35);
    rbx41 = g3ed;
    if (rbx41) {
        do {
            if (*reinterpret_cast<unsigned char*>(&rbx41) & 1) {
                rax42 = mulredc2("(a1 >> (W_TYPE_SIZE - 1)) == 0", v36, v34, r10_31, r8_30, r13_33, r14_32, r12_27);
                v34 = rax42;
                rax43 = gd1d0;
                v36 = rax43;
                r8_30 = r8_30;
                r10_31 = r10_31;
            }
            rax44 = mulredc2("(a1 >> (W_TYPE_SIZE - 1)) == 0", r10_31, r8_30, r10_31, r8_30, r13_33, r14_32, r12_27);
            rbx41 = rbx41 >> 1;
            r10_31 = gd1d0;
            r8_30 = rax44;
        } while (rbx41);
    }
    gd1d0 = v36;
    goto v45;
    addr_31b8_10:
    *reinterpret_cast<void***>(rdi) = rdx25;
    return rax26;
    addr_31d2_11:
    if (reinterpret_cast<unsigned char>(a7) <= reinterpret_cast<unsigned char>(tmp64_23)) {
        addr_31ac_8:
        rdx25 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi24) - (reinterpret_cast<unsigned char>(r9) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rsi24) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r9) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_23) < reinterpret_cast<unsigned char>(a7))))))));
        rax26 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(tmp64_23) - reinterpret_cast<unsigned char>(a7));
        goto addr_31b8_10;
    } else {
        goto addr_31b8_10;
    }
}