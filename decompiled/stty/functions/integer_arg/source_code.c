integer_arg (char const *s, unsigned long int maxval)
{
  return xnumtoumax (s, 0, 0, maxval, "bB", _("invalid integer argument"), 0);
}