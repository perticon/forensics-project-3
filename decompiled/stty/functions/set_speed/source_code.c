set_speed (enum speed_setting type, char const *arg, struct termios *mode)
{
  speed_t baud;

  baud = string_to_baud (arg);
  if (type == input_speed || type == both_speeds)
    cfsetispeed (mode, baud);
  if (type == output_speed || type == both_speeds)
    cfsetospeed (mode, baud);
}