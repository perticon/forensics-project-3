void apply_settings(char param_1,undefined8 param_2,long param_3,int param_4,termios *param_5,
                   undefined *param_6,undefined *param_7)

{
  byte bVar1;
  byte bVar2;
  byte bVar3;
  int iVar4;
  int iVar5;
  int iVar6;
  undefined4 uVar7;
  uint uVar8;
  byte *__s1;
  ulong uVar9;
  int *piVar10;
  long *plVar11;
  undefined8 uVar12;
  undefined8 uVar13;
  termios *ptVar14;
  long lVar15;
  undefined1 *puVar16;
  int iVar17;
  speed_t sVar18;
  long lVar19;
  char **ppcVar20;
  char *pcVar21;
  char *pcVar22;
  long in_FS_OFFSET;
  bool bVar23;
  char cVar24;
  char *local_60;
  undefined8 local_58;
  undefined8 local_50;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_4 < 2) {
LAB_00105cf9:
    if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
      return;
    }
LAB_00106b9e:
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  iVar17 = 1;
LAB_00105b9c:
  do {
    lVar19 = (long)iVar17 * 8;
    pcVar22 = *(char **)(param_3 + (long)iVar17 * 8);
    iVar6 = iVar17;
    if (pcVar22 == (char *)0x0) goto LAB_00105b8f;
    bVar23 = *pcVar22 == '-';
    if (bVar23) {
      pcVar22 = pcVar22 + 1;
    }
    iVar4 = strcmp(pcVar22,"drain");
    if (iVar4 == 0) {
      tcsetattr_options = bVar23 ^ 1;
      goto LAB_00105b8f;
    }
    ppcVar20 = (char **)(mode_info + 0x20);
    iVar4 = 0;
    pcVar21 = "parenb";
    do {
      iVar5 = strcmp(pcVar22,pcVar21);
      if (iVar5 == 0) {
        lVar15 = (long)iVar4;
        if ((mode_info[lVar15 * 0x20 + 0xc] & 0x10) != 0) goto LAB_00105c3a;
        if ((bVar23) && ((mode_info[lVar15 * 0x20 + 0xc] & 4) == 0)) {
          *param_7 = 1;
          goto LAB_00105fa3;
        }
        if (4 < *(uint *)(mode_info + lVar15 * 0x20 + 8)) goto apply_settings_cold;
        switch(*(undefined4 *)(mode_info + lVar15 * 0x20 + 8)) {
        case 0:
          ptVar14 = (termios *)&param_5->c_cflag;
          break;
        case 1:
          ptVar14 = param_5;
          break;
        case 2:
          ptVar14 = (termios *)&param_5->c_oflag;
          break;
        case 3:
          ptVar14 = (termios *)&param_5->c_lflag;
          break;
        case 4:
          iVar4 = strcmp(pcVar21,"evenp");
          if ((iVar4 == 0) || (iVar4 = strcmp(pcVar21,"parity"), iVar4 == 0)) {
            uVar8 = param_5->c_cflag;
            if (bVar23) {
LAB_00105f16:
              param_5->c_cflag = uVar8 & 0xfffffecf | 0x30;
              *param_7 = 1;
            }
            else {
              param_5->c_cflag = uVar8 & 0xfffffccf | 0x120;
              *param_7 = 1;
            }
            goto LAB_00105b8f;
          }
          iVar4 = strcmp(pcVar21,"oddp");
          if (iVar4 == 0) {
            uVar8 = param_5->c_cflag;
            if (bVar23) goto LAB_00105f16;
            param_5->c_cflag = uVar8 & 0xfffffccf | 800;
            *param_7 = 1;
            goto LAB_00105b8f;
          }
          iVar4 = strcmp(pcVar21,"nl");
          if (iVar4 == 0) {
            if (bVar23) {
              param_5->c_oflag = param_5->c_oflag & 0xffffffd3 | 4;
              param_5->c_iflag = param_5->c_iflag & 0xfffffe3f | 0x100;
              *param_7 = 1;
            }
            else {
              *(ulong *)param_5 = *(ulong *)param_5 & 0xfffffffbfffffeff;
              *param_7 = 1;
            }
            goto LAB_00105b8f;
          }
          iVar4 = strcmp(pcVar21,"ek");
          if (iVar4 == 0) {
            *(undefined2 *)(param_5->c_cc + 2) = 0x157f;
            *param_7 = 1;
            goto LAB_00105b8f;
          }
          iVar4 = strcmp(pcVar21,"sane");
          if (iVar4 == 0) {
            plVar11 = (long *)control_info;
            while (*plVar11 != 0) {
              param_5->c_cc[plVar11[2]] = *(cc_t *)(plVar11 + 1);
              plVar11 = plVar11 + 3;
            }
            puVar16 = mode_info;
            do {
              if (*(long *)puVar16 == 0) goto LAB_0010623d;
              bVar1 = *(byte *)((long)puVar16 + 0xc);
              if ((bVar1 & 0x10) == 0) {
                if ((bVar1 & 1) == 0) {
                  if ((bVar1 & 2) != 0) {
                    if (4 < *(uint *)((long)puVar16 + 8)) goto apply_settings_cold;
                    switch(*(undefined4 *)((long)puVar16 + 8)) {
                    case 0:
                      ptVar14 = (termios *)&param_5->c_cflag;
                      break;
                    case 1:
                      ptVar14 = param_5;
                      break;
                    case 2:
                      ptVar14 = (termios *)&param_5->c_oflag;
                      break;
                    case 3:
                      ptVar14 = (termios *)&param_5->c_lflag;
                      break;
                    case 4:
                    /* WARNING: Subroutine does not return */
                      __assert_fail("bitsp","src/stty.c",0x8d2,"sane_mode");
                    }
                    ptVar14->c_iflag =
                         ptVar14->c_iflag &
                         ~((uint)*(long *)((long)puVar16 + 0x10) | *(uint *)((long)puVar16 + 0x18));
                  }
                }
                else {
                  if (4 < *(uint *)((long)puVar16 + 8)) {
apply_settings_cold:
                    /* WARNING: Subroutine does not return */
                    abort();
                  }
                  switch(*(undefined4 *)((long)puVar16 + 8)) {
                  case 0:
                    ptVar14 = (termios *)&param_5->c_cflag;
                    break;
                  case 1:
                    ptVar14 = param_5;
                    break;
                  case 2:
                    ptVar14 = (termios *)&param_5->c_oflag;
                    break;
                  case 3:
                    ptVar14 = (termios *)&param_5->c_lflag;
                    break;
                  case 4:
                    /* WARNING: Subroutine does not return */
                    __assert_fail("bitsp","src/stty.c",0x8cc,"sane_mode");
                  }
                  ptVar14->c_iflag =
                       ~*(uint *)((long)puVar16 + 0x18) & ptVar14->c_iflag |
                       *(uint *)((long)puVar16 + 0x10);
                }
              }
              puVar16 = (undefined1 *)((long)puVar16 + 0x20);
            } while( true );
          }
          iVar4 = strcmp(pcVar21,"cbreak");
          if (iVar4 == 0) {
            uVar8 = param_5->c_lflag;
            if (!bVar23) goto LAB_0010661e;
            param_5->c_lflag = uVar8 | 2;
            *param_7 = 1;
            goto LAB_00105b8f;
          }
          iVar4 = strcmp(pcVar21,"pass8");
          if (iVar4 == 0) {
            uVar8 = param_5->c_cflag & 0xfffffecf;
            if (bVar23) {
              param_5->c_cflag = uVar8 | 0x120;
              param_5->c_iflag = param_5->c_iflag | 0x20;
              *param_7 = 1;
            }
            else {
              param_5->c_cflag = uVar8 | 0x30;
              param_5->c_iflag = param_5->c_iflag & 0xffffffdf;
              *param_7 = 1;
            }
            goto LAB_00105b8f;
          }
          iVar4 = strcmp(pcVar21,"litout");
          if (iVar4 == 0) {
            uVar8 = param_5->c_cflag & 0xfffffecf;
            if (bVar23) {
              *(ulong *)param_5 = *(ulong *)param_5 | 0x100000020;
              param_5->c_cflag = uVar8 | 0x120;
              *param_7 = 1;
            }
            else {
              param_5->c_cflag = uVar8 | 0x30;
              *(ulong *)param_5 = *(ulong *)param_5 & 0xfffffffeffffffdf;
              *param_7 = 1;
            }
            goto LAB_00105b8f;
          }
          iVar4 = strcmp(pcVar21,"raw");
          if ((iVar4 == 0) || (iVar4 = strcmp(pcVar21,"cooked"), iVar4 == 0)) {
            if (*pcVar21 == 'r') {
              if (bVar23) goto LAB_001068fc;
            }
            else if ((*pcVar21 == 'c') && (!bVar23)) {
LAB_001068fc:
              *(ulong *)param_5 = *(ulong *)param_5 | 0x100000526;
              param_5->c_lflag = param_5->c_lflag | 3;
              *param_7 = 1;
              goto LAB_00105b8f;
            }
            param_5->c_oflag = param_5->c_oflag & 0xfffffffe;
            param_5->c_lflag = param_5->c_lflag & 0xfffffff8;
            param_5->c_iflag = 0;
            *(undefined2 *)(param_5->c_cc + 5) = 0x100;
            goto LAB_0010623d;
          }
          iVar4 = strcmp(pcVar21,"decctlq");
          if (iVar4 == 0) {
            if (bVar23) {
              param_5->c_iflag = param_5->c_iflag | 0x800;
              *param_7 = 1;
            }
            else {
              param_5->c_iflag = param_5->c_iflag & 0xfffff7ff;
              *param_7 = 1;
            }
            goto LAB_00105b8f;
          }
          iVar4 = strcmp(pcVar21,"tabs");
          if (iVar4 == 0) {
            if (bVar23) {
              param_5->c_oflag = param_5->c_oflag | 0x1800;
              *param_7 = 1;
            }
            else {
              param_5->c_oflag = param_5->c_oflag & 0xffffe7ff;
              *param_7 = 1;
            }
            goto LAB_00105b8f;
          }
          iVar4 = strcmp(pcVar21,"lcase");
          if ((iVar4 == 0) || (iVar4 = strcmp(pcVar21,"LCASE"), iVar4 == 0)) {
            if (bVar23) {
              *(ulong *)param_5 = *(ulong *)param_5 & 0xfffffffdfffffdff;
              param_5->c_lflag = param_5->c_lflag & 0xfffffffb;
              *param_7 = 1;
            }
            else {
              param_5->c_lflag = param_5->c_lflag | 4;
              *(ulong *)param_5 = *(ulong *)param_5 | 0x200000200;
              *param_7 = 1;
            }
            goto LAB_00105b8f;
          }
          iVar4 = strcmp(pcVar21,"crt");
          if (iVar4 == 0) {
            param_5->c_lflag = param_5->c_lflag | 0xa10;
            *param_7 = 1;
          }
          else {
            iVar4 = strcmp(pcVar21,"dec");
            if (iVar4 != 0) goto LAB_0010623d;
            param_5->c_iflag = param_5->c_iflag & 0xfffff7ff;
            param_5->c_lflag = param_5->c_lflag | 0xa10;
            param_5->c_cc[0] = '\x03';
            *(undefined2 *)(param_5->c_cc + 2) = 0x157f;
            *param_7 = 1;
          }
          goto LAB_00105b8f;
        }
        if (bVar23) {
          ptVar14->c_iflag =
               ~((uint)*(undefined8 *)(mode_info + lVar15 * 0x20 + 0x18) |
                (uint)*(undefined8 *)(mode_info + lVar15 * 0x20 + 0x10)) & ptVar14->c_iflag;
          *param_7 = 1;
        }
        else {
          ptVar14->c_iflag =
               ~(uint)*(undefined8 *)(mode_info + lVar15 * 0x20 + 0x18) & ptVar14->c_iflag |
               (uint)*(undefined8 *)(mode_info + lVar15 * 0x20 + 0x10);
          *param_7 = 1;
        }
        goto LAB_00105b8f;
      }
      pcVar21 = *ppcVar20;
      ppcVar20 = ppcVar20 + 4;
      iVar4 = iVar4 + 1;
    } while (pcVar21 != (char *)0x0);
    if (bVar23) {
LAB_00105fa3:
      __s1 = (byte *)quote(pcVar22 + -1);
      uVar12 = dcgettext(0,"invalid argument %s",5);
      error(0,0,uVar12,__s1);
      usage(1);
LAB_00105fe0:
      uVar13 = 0x105ff3;
      uVar12 = dcgettext(0,"invalid integer argument",5);
      bVar3 = xnumtoumax(__s1,0,0,0xff,&DAT_0010d025,uVar12,0,uVar13);
LAB_00105dcc:
      param_5->c_cc[*(long *)(control_info + (long)iVar4 * 0x18 + 0x10)] = bVar3;
      *param_7 = 1;
      iVar6 = iVar17;
      goto LAB_00105b8f;
    }
    ppcVar20 = (char **)(control_info + 0x18);
    iVar4 = 0;
    pcVar21 = "intr";
    do {
      iVar5 = strcmp(pcVar22,pcVar21);
      if (iVar5 == 0) {
        if ((param_4 + -1 == iVar17) ||
           (__s1 = *(byte **)(param_3 + 8 + lVar19), __s1 == (byte *)0x0)) goto LAB_00106b65;
        iVar17 = iVar17 + 1;
        iVar6 = strcmp(pcVar21,"min");
        if ((iVar6 == 0) || (iVar6 = strcmp(pcVar21,"time"), iVar6 == 0)) goto LAB_00105fe0;
        bVar1 = *__s1;
        bVar3 = bVar1;
        if ((bVar1 == 0) || (bVar2 = __s1[1], bVar2 == 0)) goto LAB_00105dcc;
        iVar6 = strcmp((char *)__s1,"^-");
        bVar3 = 0;
        if ((iVar6 == 0) || (iVar6 = strcmp((char *)__s1,"undef"), iVar6 == 0)) goto LAB_00105dcc;
        if (bVar1 != 0x5e) goto LAB_00105fe0;
        if (bVar2 == 0x3f) {
          bVar3 = 0x7f;
        }
        else {
          bVar3 = bVar2 & 0x9f;
        }
        goto LAB_00105dcc;
      }
      pcVar21 = *ppcVar20;
      ppcVar20 = ppcVar20 + 3;
      iVar4 = iVar4 + 1;
    } while (pcVar21 != (char *)0x0);
LAB_00105c3a:
    iVar4 = strcmp(pcVar22,"ispeed");
    if (iVar4 != 0) {
      iVar4 = strcmp(pcVar22,"ospeed");
      if (iVar4 != 0) {
        iVar4 = strcmp(pcVar22,"rows");
        if (iVar4 == 0) {
          if ((param_4 + -1 == iVar17) || (lVar19 = *(long *)(param_3 + 8 + lVar19), lVar19 == 0)) {
            uVar12 = quote(pcVar22);
            uVar13 = dcgettext(0,"missing argument to %s",5);
            error(0,0,uVar13,uVar12);
            usage(1);
            goto LAB_00106a81;
          }
          iVar6 = iVar17 + 1;
          if (param_1 == '\0') {
            uVar7 = integer_arg(lVar19,0x7fffffff);
            set_window_size(uVar7,0xffffffff,param_2);
          }
        }
        else {
          iVar4 = strcmp(pcVar22,"cols");
          if ((iVar4 == 0) || (iVar4 = strcmp(pcVar22,"columns"), iVar4 == 0)) {
            if ((param_4 + -1 == iVar17) || (lVar19 = *(long *)(param_3 + 8 + lVar19), lVar19 == 0))
            goto LAB_00106aba;
            iVar6 = iVar17 + 1;
            if (param_1 == '\0') {
              uVar7 = integer_arg(lVar19,0x7fffffff);
              set_window_size(0xffffffff,uVar7,param_2);
            }
          }
          else {
            iVar4 = strcmp(pcVar22,"size");
            if (iVar4 == 0) {
              if (param_1 == '\0') {
                max_col = screen_columns();
                current_col = 0;
                display_window_size(0,param_2);
              }
            }
            else {
              iVar4 = strcmp(pcVar22,"line");
              if (iVar4 == 0) {
                if (param_4 + -1 != iVar17) {
                  plVar11 = (long *)(param_3 + 8 + lVar19);
                  lVar19 = *plVar11;
                  if (lVar19 != 0) {
                    iVar17 = iVar17 + 1;
                    uVar9 = integer_arg(lVar19,0xffffffffffffffff);
                    param_5->c_line = (cc_t)uVar9;
                    if ((uVar9 & 0xffffffffffffff00) != 0) {
                      uVar12 = quote(*plVar11);
                      uVar13 = dcgettext(0,"invalid line discipline %s",5);
                      error(0,0,uVar13,uVar12);
                    }
                    goto LAB_0010623d;
                  }
                }
                uVar12 = quote(pcVar22);
                uVar13 = dcgettext(0,"missing argument to %s",5);
                error(0,0,uVar13,uVar12);
                uVar8 = usage(1);
LAB_0010661e:
                param_5->c_lflag = uVar8 & 0xfffffffd;
                *param_7 = 1;
              }
              else {
                iVar4 = strcmp(pcVar22,"speed");
                if (iVar4 == 0) {
                  if (param_1 == '\0') {
                    max_col = screen_columns();
                    display_speed(param_5,0);
                  }
                }
                else {
                  puVar16 = speeds;
                  iVar4 = 0;
                  while (pcVar21 = *(char **)puVar16, pcVar21 != (char *)0x0) {
                    puVar16 = (undefined1 *)((long)puVar16 + 0x18);
                    iVar5 = strcmp(pcVar22,pcVar21);
                    if (iVar5 == 0) {
                      puVar16 = speeds;
                      if (*(int *)(speeds + (long)iVar4 * 0x18 + 8) != -1) {
                        if (param_1 == '\0') goto LAB_00106542;
                        goto LAB_00105b8f;
                      }
                      break;
                    }
                    iVar4 = iVar4 + 1;
                  }
                  piVar10 = __errno_location();
                  lVar19 = 0;
                  pcVar21 = pcVar22;
                  do {
                    *piVar10 = 0;
                    uVar9 = strtoul(pcVar21,&local_60,0x10);
                    if ((((*piVar10 != 0) || (*local_60 != ':')) || (pcVar21 == local_60)) ||
                       (uVar9 != (uVar9 & 0xffffffff))) goto LAB_00106af3;
                    *(int *)((long)&local_58 + lVar19 * 4) = (int)uVar9;
                    lVar19 = lVar19 + 1;
                    pcVar21 = local_60 + 1;
                  } while (lVar19 != 4);
                  lVar19 = 0;
                  cVar24 = ':';
                  *(undefined8 *)param_5 = local_58;
                  *(undefined8 *)&param_5->c_cflag = local_50;
                  while( true ) {
                    *piVar10 = 0;
                    uVar9 = strtoul(pcVar21,&local_60,0x10);
                    if (((*piVar10 != 0) || (*local_60 != cVar24)) ||
                       ((cVar24 = pcVar21 == local_60, (bool)cVar24 ||
                        ((uVar9 & 0xffffffffffffff00) != 0)))) goto LAB_00106af3;
                    pcVar21 = local_60 + 1;
                    param_5->c_cc[lVar19] = (cc_t)uVar9;
                    lVar19 = lVar19 + 1;
                    if (lVar19 == 0x20) break;
                    if (lVar19 != 0x1f) {
                      cVar24 = ':';
                    }
                  }
LAB_0010623d:
                  *param_7 = 1;
                  iVar6 = iVar17;
                }
              }
            }
          }
        }
        goto LAB_00105b8f;
      }
      if ((param_4 + -1 == iVar17) ||
         (pcVar21 = *(char **)(param_3 + 8 + lVar19), pcVar21 == (char *)0x0)) goto LAB_00106b2c;
      iVar17 = iVar17 + 1;
      iVar6 = iVar17;
      if (param_1 != '\0') goto LAB_00105b8f;
      ppcVar20 = (char **)(speeds + 0x18);
      pcVar22 = "0";
      do {
        iVar6 = strcmp(pcVar21,pcVar22);
        if (iVar6 == 0) {
          sVar18 = *(speed_t *)(speeds + (long)iVar4 * 0x18 + 8);
          goto LAB_00105e97;
        }
        pcVar22 = *ppcVar20;
        ppcVar20 = ppcVar20 + 3;
        iVar4 = iVar4 + 1;
      } while (pcVar22 != (char *)0x0);
      sVar18 = 0xffffffff;
      goto LAB_00105e97;
    }
    if ((param_4 + -1 == iVar17) ||
       (pcVar21 = *(char **)(param_3 + 8 + lVar19), pcVar21 == (char *)0x0)) {
LAB_00106a81:
      uVar12 = quote(pcVar22);
      uVar13 = dcgettext(0,"missing argument to %s",5);
      error(0,0,uVar13,uVar12);
      usage(1);
LAB_00106aba:
      uVar12 = quote(pcVar22);
      uVar13 = dcgettext(0,"missing argument to %s",5);
      error(0,0,uVar13,uVar12);
      usage(1);
LAB_00106af3:
      uVar12 = quote(pcVar22);
      uVar13 = dcgettext(0,"invalid argument %s",5);
      error(0,0,uVar13,uVar12);
      usage(1);
LAB_00106b2c:
      uVar12 = quote(pcVar22);
      uVar13 = dcgettext(0,"missing argument to %s",5);
      error(0,0,uVar13,uVar12);
      usage(1);
LAB_00106b65:
      uVar12 = quote(pcVar22);
      uVar13 = dcgettext(0,"missing argument to %s",5);
      error(0,0,uVar13,uVar12);
      usage(1);
      goto LAB_00106b9e;
    }
    iVar6 = iVar17 + 1;
    if (param_1 != '\0') goto LAB_00105b8f;
    ppcVar20 = (char **)(speeds + 0x18);
    pcVar22 = "0";
    do {
      iVar6 = strcmp(pcVar21,pcVar22);
      if (iVar6 == 0) {
        sVar18 = *(speed_t *)(speeds + (long)iVar4 * 0x18 + 8);
        goto LAB_00105cd2;
      }
      pcVar22 = *ppcVar20;
      ppcVar20 = ppcVar20 + 3;
      iVar4 = iVar4 + 1;
    } while (pcVar22 != (char *)0x0);
    sVar18 = 0xffffffff;
LAB_00105cd2:
    iVar17 = iVar17 + 2;
    cfsetispeed(param_5,sVar18);
    *param_6 = 1;
    *param_7 = 1;
  } while (iVar17 < param_4);
  goto LAB_00105cf9;
LAB_00106542:
  pcVar21 = *(char **)puVar16;
  if (pcVar21 == (char *)0x0) goto code_r0x0010654a;
  puVar16 = (undefined1 *)((long)puVar16 + 0x18);
  iVar6 = strcmp(pcVar22,pcVar21);
  if (iVar6 == 0) {
    sVar18 = *(speed_t *)(speeds + (long)iVar5 * 0x18 + 8);
    goto LAB_001065ca;
  }
  iVar5 = iVar5 + 1;
  goto LAB_00106542;
code_r0x0010654a:
  sVar18 = 0xffffffff;
LAB_001065ca:
  cfsetispeed(param_5,sVar18);
LAB_00105e97:
  cfsetospeed(param_5,sVar18);
  *param_6 = 1;
  *param_7 = 1;
  iVar6 = iVar17;
LAB_00105b8f:
  iVar17 = iVar6 + 1;
  if (param_4 <= iVar17) goto LAB_00105cf9;
  goto LAB_00105b9c;
}