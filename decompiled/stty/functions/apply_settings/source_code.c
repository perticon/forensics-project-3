apply_settings (bool checking, char const *device_name,
                char * const *settings, int n_settings,
                struct termios *mode, bool *speed_was_set,
                bool *require_set_attr)
{
#define check_argument(arg)						\
  do									\
    {									\
      if (k == n_settings - 1 || ! settings[k + 1])			\
        {								\
          error (0, 0, _("missing argument to %s"), quote (arg));	\
          usage (EXIT_FAILURE);						\
        }								\
    }									\
  while (0)

  for (int k = 1; k < n_settings; k++)
    {
      char const *arg = settings[k];
      bool match_found = false;
      bool not_set_attr = false;
      bool reversed = false;
      int i;

      if (! arg)
        continue;

      if (arg[0] == '-')
        {
          ++arg;
          reversed = true;
        }
      if (STREQ (arg, "drain"))
        {
          tcsetattr_options = reversed ? TCSANOW : TCSADRAIN;
          continue;
        }
      for (i = 0; mode_info[i].name != NULL; ++i)
        {
          if (STREQ (arg, mode_info[i].name))
            {
              if ((mode_info[i].flags & NO_SETATTR) == 0)
                {
                  match_found = set_mode (&mode_info[i], reversed, mode);
                  *require_set_attr = true;
                }
              else
                match_found = not_set_attr = true;
              break;
            }
        }
      if (!match_found && reversed)
        {
          error (0, 0, _("invalid argument %s"), quote (arg - 1));
          usage (EXIT_FAILURE);
        }
      if (!match_found)
        {
          for (i = 0; control_info[i].name != NULL; ++i)
            {
              if (STREQ (arg, control_info[i].name))
                {
                  check_argument (arg);
                  match_found = true;
                  ++k;
                  set_control_char (&control_info[i], settings[k], mode);
                  *require_set_attr = true;
                  break;
                }
            }
        }
      if (!match_found || not_set_attr)
        {
          if (STREQ (arg, "ispeed"))
            {
              check_argument (arg);
              ++k;
              if (checking)
                continue;
              set_speed (input_speed, settings[k], mode);
              *speed_was_set = true;
              *require_set_attr = true;
            }
          else if (STREQ (arg, "ospeed"))
            {
              check_argument (arg);
              ++k;
              if (checking)
                continue;
              set_speed (output_speed, settings[k], mode);
              *speed_was_set = true;
              *require_set_attr = true;
            }
#ifdef TIOCEXT
          /* This is the BSD interface to "extproc".
            Even though it's an lflag, an ioctl is used to set it.  */
          else if (STREQ (arg, "extproc"))
            {
              int val = ! reversed;

              if (checking)
                continue;

              if (ioctl (STDIN_FILENO, TIOCEXT, &val) != 0)
                {
                  die (EXIT_FAILURE, errno, _("%s: error setting %s"),
                       quotef_n (0, device_name), quote_n (1, arg));
                }
            }
#endif
#ifdef TIOCGWINSZ
          else if (STREQ (arg, "rows"))
            {
              check_argument (arg);
              ++k;
              if (checking)
                continue;
              set_window_size (integer_arg (settings[k], INT_MAX), -1,
                               device_name);
            }
          else if (STREQ (arg, "cols")
                   || STREQ (arg, "columns"))
            {
              check_argument (arg);
              ++k;
              if (checking)
                continue;
              set_window_size (-1, integer_arg (settings[k], INT_MAX),
                               device_name);
            }
          else if (STREQ (arg, "size"))
            {
              if (checking)
                continue;
              max_col = screen_columns ();
              current_col = 0;
              display_window_size (false, device_name);
            }
#endif
#ifdef HAVE_C_LINE
          else if (STREQ (arg, "line"))
            {
              unsigned long int value;
              check_argument (arg);
              ++k;
              mode->c_line = value = integer_arg (settings[k], ULONG_MAX);
              if (mode->c_line != value)
                error (0, 0, _("invalid line discipline %s"),
                       quote (settings[k]));
              *require_set_attr = true;
            }
#endif
          else if (STREQ (arg, "speed"))
            {
              if (checking)
                continue;
              max_col = screen_columns ();
              display_speed (mode, false);
            }
          else if (string_to_baud (arg) != (speed_t) -1)
            {
              if (checking)
                continue;
              set_speed (both_speeds, arg, mode);
              *speed_was_set = true;
              *require_set_attr = true;
            }
          else
            {
              if (! recover_mode (arg, mode))
                {
                  error (0, 0, _("invalid argument %s"), quote (arg));
                  usage (EXIT_FAILURE);
                }
              *require_set_attr = true;
            }
        }
    }
}