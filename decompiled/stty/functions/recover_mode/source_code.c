recover_mode (char const *arg, struct termios *mode)
{
  tcflag_t flag[4];
  char const *s = arg;
  size_t i;
  for (i = 0; i < 4; i++)
    {
      char *p;
      if (strtoul_tcflag_t (s, 16, &p, flag + i, ':') != 0)
        return false;
      s = p + 1;
    }
  mode->c_iflag = flag[0];
  mode->c_oflag = flag[1];
  mode->c_cflag = flag[2];
  mode->c_lflag = flag[3];

  for (i = 0; i < NCCS; ++i)
    {
      char *p;
      char delim = i < NCCS - 1 ? ':' : '\0';
      if (strtoul_cc_t (s, 16, &p, mode->c_cc + i, delim) != 0)
        return false;
      s = p + 1;
    }

  return true;
}