main (int argc, char **argv)
{
  /* Initialize to all zeroes so there is no risk memcmp will report a
     spurious difference in an uninitialized portion of the structure.  */
  static struct termios mode;

  enum output_type output_type;
  int optc;
  int argi = 0;
  int opti = 1;
  bool require_set_attr;
  MAYBE_UNUSED bool speed_was_set;
  bool verbose_output;
  bool recoverable_output;
  bool noargs = true;
  char *file_name = NULL;
  char const *device_name;

  initialize_main (&argc, &argv);
  set_program_name (argv[0]);
  setlocale (LC_ALL, "");
  bindtextdomain (PACKAGE, LOCALEDIR);
  textdomain (PACKAGE);

  atexit (close_stdout);

  output_type = changed;
  verbose_output = false;
  recoverable_output = false;

  /* Don't print error messages for unrecognized options.  */
  opterr = 0;

  /* If any new options are ever added to stty, the short options MUST
     NOT allow any ambiguity with the stty settings.  For example, the
     stty setting "-gagFork" would not be feasible, since it will be
     parsed as "-g -a -g -F ork".  If you change anything about how
     stty parses options, be sure it still works with combinations of
     short and long options, --, POSIXLY_CORRECT, etc.  */

  while ((optc = getopt_long (argc - argi, argv + argi, "-agF:",
                              longopts, NULL))
         != -1)
    {
      switch (optc)
        {
        case 'a':
          verbose_output = true;
          output_type = all;
          break;

        case 'g':
          recoverable_output = true;
          output_type = recoverable;
          break;

        case 'F':
          if (file_name)
            die (EXIT_FAILURE, 0, _("only one device may be specified"));
          file_name = optarg;
          break;

        case_GETOPT_HELP_CHAR;

        case_GETOPT_VERSION_CHAR (PROGRAM_NAME, AUTHORS);

        default:
          /* Consider "drain" as an option rather than a setting,
             to support: alias stty='stty -drain'  etc.  */
          if (! STREQ (argv[argi + opti], "-drain")
              && ! STREQ (argv[argi + opti], "drain"))
            noargs = false;

          /* Skip the argument containing this unrecognized option;
             the 2nd pass will analyze it.  */
          argi += opti;

          /* Restart getopt_long from the first unskipped argument.  */
          opti = 1;
          optind = 0;

          break;
        }

      /* Clear fully-parsed arguments, so they don't confuse the 2nd pass.  */
      while (opti < optind)
        argv[argi + opti++] = NULL;
    }

  /* Specifying both -a and -g gets an error.  */
  if (verbose_output && recoverable_output)
    die (EXIT_FAILURE, 0,
         _("the options for verbose and stty-readable output styles are\n"
           "mutually exclusive"));

  /* Specifying any other arguments with -a or -g gets an error.  */
  if (!noargs && (verbose_output || recoverable_output))
    die (EXIT_FAILURE, 0,
         _("when specifying an output style, modes may not be set"));

  device_name = file_name ? file_name : _("standard input");

  if (!noargs && !verbose_output && !recoverable_output)
    {
      static struct termios check_mode;
      apply_settings (/* checking= */ true, device_name, argv, argc,
                      &check_mode, &speed_was_set, &require_set_attr);
    }

  if (file_name)
    {
      int fdflags;
      if (fd_reopen (STDIN_FILENO, device_name, O_RDONLY | O_NONBLOCK, 0) < 0)
        die (EXIT_FAILURE, errno, "%s", quotef (device_name));
      if ((fdflags = fcntl (STDIN_FILENO, F_GETFL)) == -1
          || fcntl (STDIN_FILENO, F_SETFL, fdflags & ~O_NONBLOCK) < 0)
        die (EXIT_FAILURE, errno, _("%s: couldn't reset non-blocking mode"),
             quotef (device_name));
    }

  if (tcgetattr (STDIN_FILENO, &mode))
    die (EXIT_FAILURE, errno, "%s", quotef (device_name));

  if (verbose_output || recoverable_output || noargs)
    {
      max_col = screen_columns ();
      current_col = 0;
      display_settings (output_type, &mode, device_name);
      return EXIT_SUCCESS;
    }

  speed_was_set = false;
  require_set_attr = false;
  apply_settings (/* checking= */ false, device_name, argv, argc,
                  &mode, &speed_was_set, &require_set_attr);

  if (require_set_attr)
    {
      /* Initialize to all zeroes so there is no risk memcmp will report a
         spurious difference in an uninitialized portion of the structure.  */
      static struct termios new_mode;

      if (tcsetattr (STDIN_FILENO, tcsetattr_options, &mode))
        die (EXIT_FAILURE, errno, "%s", quotef (device_name));

      /* POSIX (according to Zlotnick's book) tcsetattr returns zero if
         it performs *any* of the requested operations.  This means it
         can report 'success' when it has actually failed to perform
         some proper subset of the requested operations.  To detect
         this partial failure, get the current terminal attributes and
         compare them to the requested ones.  */

      if (tcgetattr (STDIN_FILENO, &new_mode))
        die (EXIT_FAILURE, errno, "%s", quotef (device_name));

      /* Normally, one shouldn't use memcmp to compare structures that
         may have 'holes' containing uninitialized data, but we have been
         careful to initialize the storage of these two variables to all
         zeroes.  One might think it more efficient simply to compare the
         modified fields, but that would require enumerating those fields --
         and not all systems have the same fields in this structure.  */

      if (memcmp (&mode, &new_mode, sizeof (mode)) != 0)
        {
#ifdef CIBAUD
          /* SunOS 4.1.3 (at least) has the problem that after this sequence,
             tcgetattr (&m1); tcsetattr (&m1); tcgetattr (&m2);
             sometimes (m1 != m2).  The only difference is in the four bits
             of the c_cflag field corresponding to the baud rate.  To save
             Sun users a little confusion, don't report an error if this
             happens.  But suppress the error only if we haven't tried to
             set the baud rate explicitly -- otherwise we'd never give an
             error for a true failure to set the baud rate.  */

          new_mode.c_cflag &= (~CIBAUD);
          if (speed_was_set || memcmp (&mode, &new_mode, sizeof (mode)) != 0)
#endif
            {
              die (EXIT_FAILURE, 0,
                   _("%s: unable to perform all requested operations"),
                   quotef (device_name));
#ifdef TESTING
              {
                printf ("new_mode: mode\n");
                for (size_t i = 0; i < sizeof (new_mode); i++)
                  printf ("0x%02x: 0x%02x\n",
                          *(((unsigned char *) &new_mode) + i),
                          *(((unsigned char *) &mode) + i));
              }
#endif
            }
        }
    }

  return EXIT_SUCCESS;
}