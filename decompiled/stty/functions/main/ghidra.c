undefined8 main(int param_1,undefined8 *param_2)

{
  char cVar1;
  byte bVar2;
  char *pcVar3;
  bool bVar4;
  bool bVar5;
  bool bVar6;
  int iVar7;
  int iVar8;
  undefined8 uVar9;
  int *piVar10;
  undefined8 uVar11;
  char *pcVar12;
  long lVar13;
  undefined1 *puVar14;
  undefined1 *puVar15;
  uint uVar16;
  _IO_FILE *p_Var17;
  char *in_R9;
  int iVar18;
  long lVar19;
  long in_FS_OFFSET;
  int local_60;
  long local_58;
  char local_42;
  char local_41;
  long local_40;
  
  lVar19 = 1;
  lVar13 = 0;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  puVar15 = (undefined1 *)0x0;
  textdomain("coreutils");
  atexit(close_stdout);
  bVar6 = true;
  opterr = 0;
  local_58 = 0;
  bVar4 = false;
  bVar5 = false;
  local_60 = 0;
  while( true ) {
    uVar9 = getopt_long(param_1 - (int)puVar15,param_2 + lVar13,"-agF:",longopts,0);
    iVar8 = optind;
    iVar7 = (int)uVar9;
    if (iVar7 == -1) break;
    iVar18 = (int)lVar19;
    if (iVar7 == 0x46) {
      if (local_58 == 0) {
        local_58 = optarg;
        goto LAB_00103939;
      }
LAB_0010428d:
      uVar9 = dcgettext(0,"only one device may be specified",5);
      p_Var17 = (_IO_FILE *)0x1;
      error(1,0,uVar9);
      goto LAB_001042b1;
    }
    if (iVar7 < 0x47) {
      if (iVar7 == -0x83) {
        version_etc(stdout,&DAT_0010d05d,"GNU coreutils",Version,"David MacKenzie",0);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar7 == -0x82) {
        usage(0);
        goto LAB_0010428d;
      }
LAB_001039bb:
      uVar16 = (int)puVar15 + iVar18;
      puVar15 = (undefined1 *)(ulong)uVar16;
      lVar13 = (long)(int)uVar16;
      pcVar3 = (char *)param_2[lVar13];
      iVar8 = strcmp(pcVar3,"-drain");
      if ((iVar8 != 0) && (iVar8 = strcmp(pcVar3,"drain"), iVar8 != 0)) {
        bVar6 = false;
      }
      optind = 0;
      lVar19 = 1;
    }
    else {
      if (iVar7 == 0x61) {
        bVar5 = true;
        local_60 = 1;
      }
      else {
        if (iVar7 != 0x67) goto LAB_001039bb;
        bVar4 = true;
        local_60 = 2;
      }
LAB_00103939:
      if (iVar18 < optind) {
        memset(param_2 + lVar19 + lVar13,0,(ulong)(uint)((optind + -1) - iVar18) * 8 + 8);
        lVar19 = (long)iVar8;
      }
    }
  }
  if ((bool)(bVar5 & bVar4)) {
LAB_00104355:
    uVar9 = dcgettext(0,
                      "the options for verbose and stty-readable output styles are\nmutually exclusive"
                      ,5);
    p_Var17 = (_IO_FILE *)0x1;
    error(1,0,uVar9);
LAB_00104379:
    __overflow(p_Var17,10);
LAB_00103eb7:
    iVar8 = 0;
    current_col = 0;
    bVar4 = true;
    for (puVar15 = mode_info; *(long *)puVar15 != 0; puVar15 = (undefined1 *)((long)puVar15 + 0x20))
    {
      bVar2 = *(byte *)((long)puVar15 + 0xc);
      if ((bVar2 & 8) == 0) {
        iVar7 = *(int *)((long)puVar15 + 8);
        if ((iVar7 != iVar8) && (iVar8 = iVar7, !bVar4)) {
          pcVar3 = stdout->_IO_write_ptr;
          if (pcVar3 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar3 + 1;
            *pcVar3 = '\n';
          }
          else {
            __overflow(stdout,10);
          }
          current_col = 0;
          bVar4 = true;
        }
        switch(iVar7) {
        case 0:
          puVar14 = mode_5 + 8;
          break;
        case 1:
          puVar14 = mode_5;
          break;
        case 2:
          puVar14 = mode_5 + 4;
          break;
        case 3:
          puVar14 = mode_5 + 0xc;
          break;
        case 4:
                    /* WARNING: Subroutine does not return */
          __assert_fail("bitsp","src/stty.c",0x7a1,"display_changed");
        default:
          goto main_cold;
        }
        lVar13 = *(long *)((long)puVar15 + 0x18);
        if (lVar13 == 0) {
          lVar13 = *(long *)((long)puVar15 + 0x10);
        }
        if ((ulong)((uint)lVar13 & *(uint *)puVar14) == *(ulong *)((long)puVar15 + 0x10)) {
          if ((bVar2 & 2) != 0) {
            wrapf("%s");
            bVar4 = false;
          }
        }
        else if ((bVar2 & 5) == 5) {
          wrapf(&DAT_0010d334);
          bVar4 = false;
        }
      }
    }
    if (!bVar4) {
LAB_00103ff4:
      pcVar3 = stdout->_IO_write_ptr;
      if (pcVar3 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar3 + 1;
        *pcVar3 = '\n';
      }
      else {
        __overflow(stdout,10);
      }
    }
    current_col = 0;
  }
  else {
    if (!bVar6) {
      if ((!bVar5) && (!bVar4)) {
        if (local_58 == 0) goto LAB_00103d27;
        apply_settings(1,local_58,param_2,param_1,check_mode_6,&local_41,&local_42,uVar9);
        goto LAB_00103aac;
      }
LAB_00104331:
      uVar9 = dcgettext(0,"when specifying an output style, modes may not be set",5);
      error(1,0,uVar9);
      goto LAB_00104355;
    }
    if (local_58 != 0) {
LAB_00103aac:
      iVar8 = fd_reopen(0,local_58,0x800,0);
      if (iVar8 < 0) {
        uVar9 = quotearg_n_style_colon(0,3,local_58);
        piVar10 = __errno_location();
        error(1,*piVar10,"%s",uVar9);
      }
      else {
        uVar16 = rpl_fcntl(0,3);
        if ((uVar16 != 0xffffffff) && (iVar8 = rpl_fcntl(0,4,uVar16 & 0xfffff7ff), -1 < iVar8)) {
          puVar15 = mode_5;
          iVar8 = tcgetattr(0,(termios *)mode_5);
          if (iVar8 == 0) {
            if (((bool)(bVar5 | bVar4)) || (bVar6)) goto LAB_00103bcf;
            do {
              in_R9 = &local_41;
              local_41 = '\0';
              local_42 = '\0';
              apply_settings(0,local_58,param_2,param_1,mode_5,in_R9,&local_42,puVar15);
              if (local_42 == '\0') goto LAB_00103b6d;
              iVar8 = tcsetattr(0,tcsetattr_options,(termios *)mode_5);
              if (iVar8 != 0) {
LAB_00104300:
                uVar9 = quotearg_n_style_colon(0,3,local_58);
                piVar10 = __errno_location();
                error(1,*piVar10,"%s",uVar9);
                goto LAB_00104331;
              }
              iVar8 = tcgetattr(0,(termios *)new_mode_4);
              if (iVar8 != 0) {
                uVar9 = quotearg_n_style_colon(0,3,local_58);
                piVar10 = __errno_location();
                error(1,*piVar10,"%s",uVar9);
                goto LAB_00104300;
              }
              iVar8 = memcmp(mode_5,new_mode_4,0x3c);
              if ((iVar8 == 0) ||
                 ((new_mode_4._8_4_ = new_mode_4._8_4_ & 0xeff0ffff, local_41 == '\0' &&
                  (iVar8 = memcmp(mode_5,new_mode_4,0x3c), iVar8 == 0)))) goto LAB_00103b6d;
              param_2 = (undefined8 *)quotearg_n_style_colon(0,3,local_58);
              uVar9 = dcgettext(0,"%s: unable to perform all requested operations",5);
              error(1,0,uVar9);
LAB_00103d27:
              local_58 = dcgettext(0,"standard input",5);
              apply_settings(1,local_58,param_2,param_1,check_mode_6,&local_41,&local_42,in_R9);
              puVar15 = mode_5;
              iVar8 = tcgetattr(0,(termios *)mode_5);
            } while (iVar8 == 0);
          }
          goto LAB_00103d87;
        }
      }
      uVar9 = quotearg_n_style_colon(0,3,local_58);
      uVar11 = dcgettext(0,"%s: couldn\'t reset non-blocking mode",5);
      piVar10 = __errno_location();
      error(1,*piVar10,uVar11,uVar9);
      goto LAB_0010441a;
    }
    local_58 = dcgettext(0,"standard input",5);
    iVar8 = tcgetattr(0,(termios *)mode_5);
    if (iVar8 != 0) {
LAB_00103d87:
      uVar9 = quotearg_n_style_colon(0,3,local_58);
      piVar10 = __errno_location();
      error(1,*piVar10,"%s",uVar9);
LAB_00103db8:
      display_speed(mode_5,1);
      wrapf("line = %d;",mode_5[16]);
      pcVar3 = stdout->_IO_write_ptr;
      if (pcVar3 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar3 + 1;
        *pcVar3 = '\n';
      }
      else {
        __overflow(stdout,10);
      }
      bVar4 = true;
      puVar15 = control_info;
      current_col = 0;
      while( true ) {
        pcVar3 = *(char **)puVar15;
        iVar8 = strcmp(pcVar3,"min");
        if (iVar8 == 0) break;
        cVar1 = (*(char **)((long)puVar15 + 0x10))[0x115171];
        if ((cVar1 != *(char *)((long)puVar15 + 8)) && (iVar8 = strcmp(pcVar3,"flush"), iVar8 != 0))
        {
          pcVar12 = "<undef>";
          if (cVar1 != '\0') {
            pcVar12 = (char *)visible_part_0(cVar1);
          }
          wrapf("%s = %s;",pcVar3,pcVar12);
          bVar4 = false;
        }
        puVar15 = (undefined1 *)((long)puVar15 + 0x18);
      }
      if ((mode_5._12_4_ & 2) != 0) {
        if (bVar4) goto LAB_00103eb7;
        pcVar3 = stdout->_IO_write_ptr;
        p_Var17 = stdout;
        if (pcVar3 < stdout->_IO_write_end) {
          stdout->_IO_write_ptr = pcVar3 + 1;
          *pcVar3 = '\n';
          goto LAB_00103eb7;
        }
        goto LAB_00104379;
      }
      wrapf("min = %lu; time = %lu;\n",mode_5[23],mode_5[22]);
      goto LAB_00103eb7;
    }
LAB_00103bcf:
    puVar15 = mode_5;
    max_col = screen_columns();
    current_col = 0;
    if (local_60 == 1) {
      display_speed(mode_5,1);
      display_window_size(1,local_58);
      wrapf("line = %d;",mode_5[16]);
      pcVar3 = stdout->_IO_write_ptr;
      p_Var17 = stdout;
      if (pcVar3 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar3 + 1;
        *pcVar3 = '\n';
      }
      else {
LAB_001042b1:
        __overflow(p_Var17,10);
      }
      puVar14 = control_info;
      current_col = 0;
      while( true ) {
        pcVar3 = *(char **)puVar14;
        iVar8 = strcmp(pcVar3,"min");
        if (iVar8 == 0) break;
        iVar8 = strcmp(pcVar3,"flush");
        if (iVar8 != 0) {
          pcVar12 = "<undef>";
          if ((*(char **)((long)puVar14 + 0x10))[(long)(puVar15 + 0x11)] != '\0') {
            pcVar12 = (char *)visible_part_0((*(char **)((long)puVar14 + 0x10))
                                             [(long)(puVar15 + 0x11)]);
          }
          wrapf("%s = %s;",pcVar3,pcVar12);
        }
        puVar14 = (undefined1 *)((long)puVar14 + 0x18);
      }
      wrapf("min = %lu; time = %lu;",mode_5[23],mode_5[22]);
      if (current_col != 0) {
        pcVar3 = stdout->_IO_write_ptr;
        if (pcVar3 < stdout->_IO_write_end) {
          stdout->_IO_write_ptr = pcVar3 + 1;
          *pcVar3 = '\n';
        }
        else {
          __overflow(stdout,10);
        }
      }
      current_col = 0;
      iVar8 = 0;
      for (puVar15 = mode_info; lVar13 = *(long *)puVar15, lVar13 != 0;
          puVar15 = (undefined1 *)((long)puVar15 + 0x20)) {
        bVar2 = *(byte *)((long)puVar15 + 0xc);
        if ((bVar2 & 8) == 0) {
          iVar7 = *(int *)((long)puVar15 + 8);
          if (iVar7 != iVar8) {
            pcVar3 = stdout->_IO_write_ptr;
            if (pcVar3 < stdout->_IO_write_end) {
              stdout->_IO_write_ptr = pcVar3 + 1;
              *pcVar3 = '\n';
            }
            else {
              __overflow(stdout,10);
            }
            current_col = 0;
            iVar8 = iVar7;
          }
          switch(iVar7) {
          case 0:
            puVar14 = mode_5 + 8;
            break;
          case 1:
            puVar14 = mode_5;
            break;
          case 2:
            puVar14 = mode_5 + 4;
            break;
          case 3:
            puVar14 = mode_5 + 0xc;
            break;
          case 4:
                    /* WARNING: Subroutine does not return */
            __assert_fail("bitsp","src/stty.c",0x7f5,"display_all");
          default:
main_cold:
                    /* WARNING: Subroutine does not return */
            abort();
          }
          lVar19 = *(long *)((long)puVar15 + 0x18);
          if (lVar19 == 0) {
            lVar19 = *(long *)((long)puVar15 + 0x10);
          }
          if ((ulong)((uint)lVar19 & *(uint *)puVar14) == *(ulong *)((long)puVar15 + 0x10)) {
            wrapf("%s",lVar13);
          }
          else if ((bVar2 & 4) != 0) {
            wrapf(&DAT_0010d334,lVar13);
          }
        }
      }
      goto LAB_00103ff4;
    }
    if (local_60 != 2) goto LAB_00103db8;
    lVar13 = 0;
    __printf_chk(1,"%lx:%lx:%lx:%lx",mode_5._0_4_,mode_5._4_4_,mode_5._8_4_,mode_5._12_4_);
    do {
      lVar19 = lVar13 + 0x11;
      lVar13 = lVar13 + 1;
      __printf_chk(1,":%lx",mode_5[lVar19]);
    } while (lVar13 != 0x20);
    pcVar3 = stdout->_IO_write_ptr;
    if (pcVar3 < stdout->_IO_write_end) {
      stdout->_IO_write_ptr = pcVar3 + 1;
      *pcVar3 = '\n';
    }
    else {
      __overflow(stdout,10);
    }
  }
LAB_00103b6d:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return 0;
  }
LAB_0010441a:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}