void set_window_size(ulong param_1,ulong param_2,undefined8 param_3)

{
  int iVar1;
  speed_t sVar2;
  speed_t sVar3;
  int *piVar4;
  undefined8 uVar5;
  uint *puVar6;
  long *plVar7;
  char cVar8;
  ulong uVar9;
  undefined8 uVar10;
  termios *__termios_p;
  char *pcVar11;
  long in_FS_OFFSET;
  undefined2 uStack96;
  undefined2 uStack94;
  long lStack88;
  ulong uStack80;
  ulong uStack72;
  undefined8 uStack64;
  ulong local_38;
  long local_30;
  
  uStack72 = param_1 & 0xffffffff;
  uStack80 = param_2 & 0xffffffff;
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  uStack64 = 0x1048b5;
  iVar1 = ioctl(0,0x5413,&local_38);
  if (iVar1 != 0) {
    uStack64 = 0x1048be;
    piVar4 = __errno_location();
    if (*piVar4 != 0x16) {
      uStack64 = 0x104920;
      uVar5 = quotearg_n_style_colon(0,3,param_3);
      uStack64 = 0x104939;
      error(1,*piVar4,"%s",uVar5);
      goto LAB_00104939;
    }
    local_38 = 0;
  }
  if (-1 < (int)uStack72) {
    local_38 = local_38 & 0xffffffffffff0000 | param_1 & 0xffff;
  }
  if (-1 < (int)uStack80) {
    local_38._0_4_ = CONCAT22((short)uStack80,(undefined2)local_38);
    local_38 = local_38 & 0xffffffff00000000 | (ulong)(uint)local_38;
  }
  uStack64 = 0x1048f0;
  iVar1 = ioctl(0,0x5414,&local_38);
  if (iVar1 == 0) {
    if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
      return;
    }
LAB_00104939:
                    /* WARNING: Subroutine does not return */
    uStack64 = 0x10493e;
    __stack_chk_fail();
  }
  uStack64 = 0x10494d;
  uVar5 = quotearg_n_style_colon(0,3,param_3);
  uStack64 = 0x104955;
  puVar6 = (uint *)__errno_location();
  cVar8 = '\x01';
  uVar9 = (ulong)*puVar6;
  uStack64 = 0x10496d;
  error(1,uVar9,"%s",uVar5);
  lStack88 = *(long *)(in_FS_OFFSET + 0x28);
  uStack64 = uVar5;
  iVar1 = ioctl(0,0x5413,&uStack96);
  if (iVar1 == 0) {
    if (cVar8 == '\0') {
      wrapf("%d %d\n",uStack96,uStack94);
      current_col = 0;
    }
    else {
      wrapf("rows %d; columns %d;",uStack96,uStack94);
    }
  }
  else {
    piVar4 = __errno_location();
    if (*piVar4 != 0x16) {
      uVar5 = quotearg_n_style_colon(0,3,uVar9);
      error(1,*piVar4,"%s",uVar5);
      goto LAB_00104a32;
    }
    if (cVar8 == '\0') {
      quotearg_n_style_colon(0,3,uVar9);
      uVar5 = dcgettext(0,"%s: no size information for this device",5);
      cVar8 = '\0';
      __termios_p = (termios *)0x1;
      error(1,0,uVar5);
      sVar2 = cfgetispeed(__termios_p);
      if (sVar2 != 0) {
        sVar2 = cfgetispeed(__termios_p);
        sVar3 = cfgetospeed(__termios_p);
        if (sVar2 != sVar3) {
          sVar3 = cfgetospeed(__termios_p);
          iVar1 = 0;
          sVar2 = 0;
          plVar7 = (long *)(speeds + 0x18);
          while (sVar3 != sVar2) {
            iVar1 = iVar1 + 1;
            if (*plVar7 == 0) {
              uVar5 = 0;
              goto LAB_00104b6c;
            }
            sVar2 = *(speed_t *)(plVar7 + 1);
            plVar7 = plVar7 + 3;
          }
          uVar5 = *(undefined8 *)(speeds + (long)iVar1 * 0x18 + 0x10);
LAB_00104b6c:
          sVar3 = cfgetispeed(__termios_p);
          iVar1 = 0;
          sVar2 = 0;
          plVar7 = (long *)(speeds + 0x18);
          while (sVar3 != sVar2) {
            iVar1 = iVar1 + 1;
            if (*plVar7 == 0) {
              uVar10 = 0;
              goto LAB_00104ba8;
            }
            sVar2 = *(speed_t *)(plVar7 + 1);
            plVar7 = plVar7 + 3;
          }
          uVar10 = *(undefined8 *)(speeds + (long)iVar1 * 0x18 + 0x10);
LAB_00104ba8:
          pcVar11 = "ispeed %lu baud; ospeed %lu baud;";
          if (cVar8 == '\0') {
            pcVar11 = "%lu %lu\n";
          }
          wrapf(pcVar11,uVar10,uVar5);
          goto LAB_00104b07;
        }
      }
      sVar3 = cfgetospeed(__termios_p);
      iVar1 = 0;
      sVar2 = 0;
      plVar7 = (long *)(speeds + 0x18);
      while (sVar3 != sVar2) {
        iVar1 = iVar1 + 1;
        if (*plVar7 == 0) {
          uVar5 = 0;
          goto LAB_00104aec;
        }
        sVar2 = *(speed_t *)(plVar7 + 1);
        plVar7 = plVar7 + 3;
      }
      uVar5 = *(undefined8 *)(speeds + (long)iVar1 * 0x18 + 0x10);
LAB_00104aec:
      pcVar11 = "speed %lu baud;";
      if (cVar8 == '\0') {
        pcVar11 = "%lu\n";
      }
      wrapf(pcVar11,uVar5);
LAB_00104b07:
      if (cVar8 == '\0') {
        current_col = 0;
      }
      return;
    }
  }
  if (lStack88 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
LAB_00104a32:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}