ulong screen_columns(void)

{
  int iVar1;
  char *pcVar2;
  ulong uVar3;
  long in_FS_OFFSET;
  ulong local_20;
  undefined local_18 [2];
  ushort local_16;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  iVar1 = ioctl(1,0x5413,local_18);
  if ((iVar1 != 0) || (uVar3 = (ulong)local_16, local_16 == 0)) {
    pcVar2 = getenv("COLUMNS");
    if (pcVar2 != (char *)0x0) {
      iVar1 = xstrtol(pcVar2,0,0,&local_20,"");
      if ((iVar1 == 0) && (uVar3 = local_20, local_20 - 1 < 0x7fffffff)) goto LAB_0010459a;
    }
    uVar3 = 0x50;
  }
LAB_0010459a:
  if (local_10 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar3;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}