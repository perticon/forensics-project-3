int32_t screen_columns(void) {
    // 0x4520
    int16_t v1; // 0x4520
    uint16_t v2 = v1;
    int64_t v3 = __readfsqword(40); // 0x452e
    int64_t result; // 0x4520
    if ((int32_t)function_35d0() != 0) {
        goto lab_0x4556;
    } else {
        // 0x454c
        result = v2;
        if (v2 != 0) {
            goto lab_0x459a;
        } else {
            goto lab_0x4556;
        }
    }
  lab_0x4556:;
    int64_t v4 = function_3410(); // 0x455d
    if (v4 == 0) {
        // 0x4595
        result = 80;
        goto lab_0x459a;
    } else {
        // 0x456a
        int64_t win; // bp-32, 0x4520
        if (xstrtol((char *)v4, NULL, 0, &win, (char *)&g61) != 0) {
            // 0x4595
            result = 80;
            goto lab_0x459a;
        } else {
            // 0x4583
            result = win;
            if (win < 0x80000000) {
                goto lab_0x459a;
            } else {
                // 0x4595
                result = 80;
                goto lab_0x459a;
            }
        }
    }
  lab_0x459a:
    // 0x459a
    if (v3 != __readfsqword(40)) {
        // 0x45af
        return function_3530();
    }
    // 0x45aa
    return result;
}