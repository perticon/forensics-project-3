void display_speed(struct termios * mode, bool fancy) {
    int64_t v1 = fancy ? 0xffffffff : 0; // 0x4a79
    char v2; // 0x4a70
    if ((int32_t)function_34b0() == 0) {
        goto lab_0x4aa2;
    } else {
        int64_t v3 = function_34b0(); // 0x4a8b
        if ((int32_t)v3 != (int32_t)function_36d0()) {
            int32_t v4 = function_36d0(); // 0x4b55
            if (v4 != 0) {
                int64_t v5 = (int64_t)&g9;
                while (*(int64_t *)v5 != 0) {
                    // 0x4b52
                    if (*(int32_t *)(v5 + 8) == v4) {
                        // break -> 0x4b6c
                        break;
                    }
                    v5 += 24;
                }
            }
            int32_t v6 = function_34b0(); // 0x4b91
            if (v6 != 0) {
                int64_t v7 = (int64_t)&g9;
                while (*(int64_t *)v7 != 0) {
                    // 0x4b8e
                    if (*(int32_t *)(v7 + 8) == v6) {
                        // break -> 0x4ba8
                        break;
                    }
                    v7 += 24;
                }
            }
            char v8 = v1;
            wrapf(v8 == 0 ? "%lu %lu\n" : "ispeed %lu baud; ospeed %lu baud;");
            v2 = v8;
            goto lab_0x4b07;
        } else {
            goto lab_0x4aa2;
        }
    }
  lab_0x4aa2:;
    int32_t v9 = function_36d0(); // 0x4ad5
    if (v9 != 0) {
        int64_t v10 = (int64_t)&g9;
        while (*(int64_t *)v10 != 0) {
            // 0x4ad2
            if (*(int32_t *)(v10 + 8) == v9) {
                // break -> 0x4aec
                break;
            }
            v10 += 24;
        }
    }
    char v11 = v1;
    wrapf(v11 == 0 ? "%lu\n" : "speed %lu baud;");
    v2 = v11;
    goto lab_0x4b07;
  lab_0x4b07:
    // 0x4b07
    if (v2 == 0) {
        // 0x4b0b
        current_col = 0;
    }
}