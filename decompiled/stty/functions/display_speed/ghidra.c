void display_speed(termios *param_1,char param_2)

{
  speed_t sVar1;
  speed_t sVar2;
  int iVar3;
  long *plVar4;
  undefined8 uVar5;
  undefined8 uVar6;
  char *pcVar7;
  
  sVar1 = cfgetispeed(param_1);
  if (sVar1 != 0) {
    sVar1 = cfgetispeed(param_1);
    sVar2 = cfgetospeed(param_1);
    if (sVar1 != sVar2) {
      sVar2 = cfgetospeed(param_1);
      iVar3 = 0;
      sVar1 = 0;
      plVar4 = (long *)(speeds + 0x18);
      while (sVar2 != sVar1) {
        iVar3 = iVar3 + 1;
        if (*plVar4 == 0) {
          uVar5 = 0;
          goto LAB_00104b6c;
        }
        sVar1 = *(speed_t *)(plVar4 + 1);
        plVar4 = plVar4 + 3;
      }
      uVar5 = *(undefined8 *)(speeds + (long)iVar3 * 0x18 + 0x10);
LAB_00104b6c:
      sVar2 = cfgetispeed(param_1);
      iVar3 = 0;
      sVar1 = 0;
      plVar4 = (long *)(speeds + 0x18);
      while (sVar2 != sVar1) {
        iVar3 = iVar3 + 1;
        if (*plVar4 == 0) {
          uVar6 = 0;
          goto LAB_00104ba8;
        }
        sVar1 = *(speed_t *)(plVar4 + 1);
        plVar4 = plVar4 + 3;
      }
      uVar6 = *(undefined8 *)(speeds + (long)iVar3 * 0x18 + 0x10);
LAB_00104ba8:
      pcVar7 = "ispeed %lu baud; ospeed %lu baud;";
      if (param_2 == '\0') {
        pcVar7 = "%lu %lu\n";
      }
      wrapf(pcVar7,uVar6,uVar5);
      goto LAB_00104b07;
    }
  }
  sVar2 = cfgetospeed(param_1);
  iVar3 = 0;
  sVar1 = 0;
  plVar4 = (long *)(speeds + 0x18);
  while (sVar2 != sVar1) {
    iVar3 = iVar3 + 1;
    if (*plVar4 == 0) {
      uVar5 = 0;
      goto LAB_00104aec;
    }
    sVar1 = *(speed_t *)(plVar4 + 1);
    plVar4 = plVar4 + 3;
  }
  uVar5 = *(undefined8 *)(speeds + (long)iVar3 * 0x18 + 0x10);
LAB_00104aec:
  pcVar7 = "speed %lu baud;";
  if (param_2 == '\0') {
    pcVar7 = "%lu\n";
  }
  wrapf(pcVar7,uVar5);
LAB_00104b07:
  if (param_2 == '\0') {
    current_col = 0;
  }
  return;
}