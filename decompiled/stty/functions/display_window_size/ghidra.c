void display_window_size(char param_1,undefined8 param_2)

{
  int iVar1;
  speed_t sVar2;
  speed_t sVar3;
  int *piVar4;
  undefined8 uVar5;
  long *plVar6;
  char cVar7;
  undefined8 uVar8;
  termios *__termios_p;
  char *pcVar9;
  long in_FS_OFFSET;
  undefined2 local_28;
  undefined2 local_26;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  iVar1 = ioctl(0,0x5413,&local_28);
  if (iVar1 == 0) {
    if (param_1 == '\0') {
      wrapf("%d %d\n",local_28,local_26);
      current_col = 0;
    }
    else {
      wrapf("rows %d; columns %d;",local_28,local_26);
    }
  }
  else {
    piVar4 = __errno_location();
    if (*piVar4 != 0x16) {
      uVar5 = quotearg_n_style_colon(0,3,param_2);
      error(1,*piVar4,"%s",uVar5);
      goto LAB_00104a32;
    }
    if (param_1 == '\0') {
      quotearg_n_style_colon(0,3,param_2);
      uVar5 = dcgettext(0,"%s: no size information for this device",5);
      cVar7 = '\0';
      __termios_p = (termios *)0x1;
      error(1,0,uVar5);
      sVar2 = cfgetispeed(__termios_p);
      if (sVar2 != 0) {
        sVar2 = cfgetispeed(__termios_p);
        sVar3 = cfgetospeed(__termios_p);
        if (sVar2 != sVar3) {
          sVar3 = cfgetospeed(__termios_p);
          iVar1 = 0;
          sVar2 = 0;
          plVar6 = (long *)(speeds + 0x18);
          while (sVar3 != sVar2) {
            iVar1 = iVar1 + 1;
            if (*plVar6 == 0) {
              uVar5 = 0;
              goto LAB_00104b6c;
            }
            sVar2 = *(speed_t *)(plVar6 + 1);
            plVar6 = plVar6 + 3;
          }
          uVar5 = *(undefined8 *)(speeds + (long)iVar1 * 0x18 + 0x10);
LAB_00104b6c:
          sVar3 = cfgetispeed(__termios_p);
          iVar1 = 0;
          sVar2 = 0;
          plVar6 = (long *)(speeds + 0x18);
          while (sVar3 != sVar2) {
            iVar1 = iVar1 + 1;
            if (*plVar6 == 0) {
              uVar8 = 0;
              goto LAB_00104ba8;
            }
            sVar2 = *(speed_t *)(plVar6 + 1);
            plVar6 = plVar6 + 3;
          }
          uVar8 = *(undefined8 *)(speeds + (long)iVar1 * 0x18 + 0x10);
LAB_00104ba8:
          pcVar9 = "ispeed %lu baud; ospeed %lu baud;";
          if (cVar7 == '\0') {
            pcVar9 = "%lu %lu\n";
          }
          wrapf(pcVar9,uVar8,uVar5);
          goto LAB_00104b07;
        }
      }
      sVar3 = cfgetospeed(__termios_p);
      iVar1 = 0;
      sVar2 = 0;
      plVar6 = (long *)(speeds + 0x18);
      while (sVar3 != sVar2) {
        iVar1 = iVar1 + 1;
        if (*plVar6 == 0) {
          uVar5 = 0;
          goto LAB_00104aec;
        }
        sVar2 = *(speed_t *)(plVar6 + 1);
        plVar6 = plVar6 + 3;
      }
      uVar5 = *(undefined8 *)(speeds + (long)iVar1 * 0x18 + 0x10);
LAB_00104aec:
      pcVar9 = "speed %lu baud;";
      if (cVar7 == '\0') {
        pcVar9 = "%lu\n";
      }
      wrapf(pcVar9,uVar5);
LAB_00104b07:
      if (cVar7 == '\0') {
        current_col = 0;
      }
      return;
    }
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
LAB_00104a32:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}