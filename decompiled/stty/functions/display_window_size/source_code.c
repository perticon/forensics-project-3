display_window_size (bool fancy, char const *device_name)
{
  struct winsize win;

  if (get_win_size (STDIN_FILENO, &win))
    {
      if (errno != EINVAL)
        die (EXIT_FAILURE, errno, "%s", quotef (device_name));
      if (!fancy)
        die (EXIT_FAILURE, 0,
             _("%s: no size information for this device"),
             quotef (device_name));
    }
  else
    {
      wrapf (fancy ? "rows %d; columns %d;" : "%d %d\n",
             win.ws_row, win.ws_col);
      if (!fancy)
        current_col = 0;
    }
}