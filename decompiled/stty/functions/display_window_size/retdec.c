int64_t display_window_size(int64_t a1, int64_t a2) {
    int64_t v1 = __readfsqword(40); // 0x4984
    if ((int32_t)function_35d0() == 0) {
        // 0x49d0
        int16_t v2; // 0x4970
        int64_t v3 = (uint16_t)v2; // 0x49d0
        int64_t v4 = v2; // 0x49d5
        if ((char)a1 == 0) {
            // 0x49f0
            wrapf("%d %d\n", v4, v3);
            current_col = 0;
        } else {
            // 0x49dd
            wrapf("rows %d; columns %d;", v4, v3);
        }
    } else {
        // 0x49a0
        if (*(int32_t *)function_3450() != 22) {
            // 0x4a0a
            quotearg_n_style_colon();
            function_36f0();
            // 0x4a32
            function_3530();
            // 0x4a37
            quotearg_n_style_colon();
            function_3500();
            return function_36f0();
        }
        if ((char)a1 == 0) {
            // 0x4a37
            quotearg_n_style_colon();
            function_3500();
            return function_36f0();
        }
    }
    int64_t result = v1 - __readfsqword(40); // 0x49ba
    if (result == 0) {
        // 0x49c5
        return result;
    }
    // 0x4a32
    function_3530();
    // 0x4a37
    quotearg_n_style_colon();
    function_3500();
    return function_36f0();
}