
void splitbuf_append_byte(void** rdi, uint32_t esi, ...);

void splitbuf_grow(void** rdi);

/* check_start_new_arg.part.0 */
void check_start_new_arg_part_0(void** rdi, void** rsi, ...) {
    void** ebp3;
    int64_t rdx4;

    splitbuf_append_byte(rdi, 0);
    ebp3 = *reinterpret_cast<void***>(rdi + 8);
    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi + 16)) <= reinterpret_cast<signed char>(static_cast<int64_t>(reinterpret_cast<int32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 24)) + reinterpret_cast<unsigned char>(ebp3) + 1)))) {
        splitbuf_grow(rdi);
    }
    rdx4 = reinterpret_cast<int32_t>(ebp3);
    *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(*reinterpret_cast<void***>(rdi) + rdx4 * 8) + 8) = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi) + rdx4 * 8);
    *reinterpret_cast<void***>(rdi + 8) = ebp3 + 1;
    *reinterpret_cast<signed char*>(rdi + 28) = 0;
    return;
}

void splitbuf_append_byte(void** rdi, uint32_t esi, ...) {
    int64_t rax3;
    void** rdx4;
    uint32_t r12d5;
    void** rbp6;
    void** rax7;

    rax3 = reinterpret_cast<int32_t>(*reinterpret_cast<void***>(rdi + 8));
    rdx4 = *reinterpret_cast<void***>(rdi);
    r12d5 = esi;
    rbp6 = *reinterpret_cast<void***>(rdx4 + rax3 * 8);
    rax7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 16)) << 3);
    if (reinterpret_cast<unsigned char>(rax7) <= reinterpret_cast<unsigned char>(rbp6)) {
        splitbuf_grow(rdi);
        rdx4 = *reinterpret_cast<void***>(rdi);
        rax7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 16)) << 3);
    }
    *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx4) + reinterpret_cast<unsigned char>(rax7)) + reinterpret_cast<unsigned char>(rbp6)) = *reinterpret_cast<signed char*>(&r12d5);
    *reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi) + reinterpret_cast<int32_t>(*reinterpret_cast<void***>(rdi + 8)) * 8) = rbp6 + 1;
    return;
}

int64_t fun_2530();

int64_t fun_2470(void** rdi, ...);

void** quotearg_buffer_restyled(void** rdi, void** rsi, void** rdx, void** rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2530();
    if (r8d > 10) {
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x8f20 + rax11 * 4) + 0x8f20;
    }
}

struct s0 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** g28;

int32_t* fun_2480();

void** slotvec = reinterpret_cast<void**>(0x70);

uint32_t nslots = 1;

void** xpalloc();

void fun_25c0();

struct s1 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void fun_2460(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9);

void** xcharalloc(void** rdi, ...);

void fun_2560();

void** quotearg_n_options(void** rdi, void** rsi, void** rdx, struct s0* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    int32_t* rax8;
    void** r15_9;
    int32_t v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    void** r8_15;
    struct s1* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    int64_t r9_23;
    void** rax24;
    void** rsi25;
    void** rax26;
    uint32_t r8d27;
    int64_t v28;
    int64_t v29;
    void* rax30;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x5ddf;
    rax8 = fun_2480();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xc070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x60f1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x5e6b;
            fun_25c0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        *reinterpret_cast<uint32_t*>(&r8_15) = rcx->f0;
        *reinterpret_cast<int32_t*>(&r8_15 + 4) = 0;
        rbx16 = reinterpret_cast<struct s1*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        *reinterpret_cast<uint32_t*>(&r9_23) = r15d22;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_23) + 4) = 0;
        rax24 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, *reinterpret_cast<uint32_t*>(&r8_15), *reinterpret_cast<uint32_t*>(&r9_23), &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax24)) {
            rsi25 = rax24 + 1;
            rbx16->f0 = rsi25;
            if (r14_19 != 0xc440) {
                fun_2460(r14_19, rsi25, rsi, rdx, r8_15, r9_23);
                rsi25 = rsi25;
            }
            rax26 = xcharalloc(rsi25, rsi25);
            r8d27 = rcx->f0;
            rbx16->f8 = rax26;
            v28 = rcx->f30;
            r14_19 = rax26;
            v29 = rcx->f28;
            quotearg_buffer_restyled(rax26, rsi25, rsi, rdx, r8d27, r15d22, rsi25, v29, v28, 0x5efa);
        }
        *rax8 = v10;
        rax30 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax30) {
            fun_2560();
        } else {
            return r14_19;
        }
    }
}

void fun_2640(void** rdi, ...);

int32_t fun_2450();

void** fun_2520();

void fun_2720();

int32_t fun_24d0(void** rdi, ...);

int32_t fun_27a0(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, int64_t r9);

int32_t sig2str(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9);

void** stderr = reinterpret_cast<void**>(0);

void fun_27d0(void** rdi, void** rsi, void** rdx, ...);

void list_signal_handling(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9) {
    void* rsp7;
    void** rax8;
    void** v9;
    void** rbp10;
    int32_t eax11;
    void* rsp12;
    void** rdi13;
    int32_t r13d14;
    void** rbx15;
    int32_t eax16;
    int64_t rsi17;
    int64_t v18;
    int32_t eax19;
    int64_t r14_20;
    int32_t eax21;
    int1_t zf22;
    void** rcx23;
    void** rdi24;
    void* rax25;
    void** ebp26;
    int64_t rdx27;
    int64_t v28;

    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x158);
    rax8 = g28;
    v9 = rax8;
    rbp10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 16);
    fun_2640(rbp10);
    eax11 = fun_2450();
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8);
    if (eax11) {
        fun_2520();
        fun_2480();
        *reinterpret_cast<int32_t*>(&rdi13) = 0x7d;
        *reinterpret_cast<int32_t*>(&rdi13 + 4) = 0;
        fun_2720();
    } else {
        r13d14 = 1;
        rbx15 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp12) + 0x90);
        while (1) {
            *reinterpret_cast<int32_t*>(&rdi13) = r13d14;
            *reinterpret_cast<int32_t*>(&rdi13 + 4) = 0;
            eax16 = fun_24d0(rdi13);
            rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
            if (eax16) 
                goto addr_3c05_5;
            *reinterpret_cast<int32_t*>(&rsi17) = r13d14;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi17) + 4) = 0;
            rdi13 = rbp10;
            if (v18 != 1) {
                eax19 = fun_27a0(rdi13, rsi17, rbx15, rcx, r8, r9);
                rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
                if (!eax19) {
                    addr_3c05_5:
                    ++r13d14;
                    if (r13d14 == 65) 
                        break; else 
                        continue;
                } else {
                    r14_20 = reinterpret_cast<int64_t>("BLOCK");
                }
            } else {
                eax21 = fun_27a0(rdi13, rsi17, rbx15, rcx, r8, r9);
                rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
                zf22 = eax21 == 0;
                r14_20 = 0x92f1;
                if (!zf22) {
                }
                if (zf22) {
                }
                if (!zf22) {
                    r14_20 = reinterpret_cast<int64_t>("BLOCK");
                }
            }
            rcx23 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp12) + 0x130);
            *reinterpret_cast<int32_t*>(&rdi24) = r13d14;
            *reinterpret_cast<int32_t*>(&rdi24 + 4) = 0;
            sig2str(rdi24, rcx23, rbx15, rcx23, r8, r9);
            rdi13 = stderr;
            rcx = rcx23;
            r9 = r14_20;
            *reinterpret_cast<int32_t*>(&r8) = r13d14;
            *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
            fun_27d0(rdi13, 1, "%-10s (%2d): %s%s%s\n", rdi13, 1, "%-10s (%2d): %s%s%s\n");
            rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 - 8 - 8 + 8 + 8 + 8);
            goto addr_3c05_5;
        }
        rax25 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
        if (!rax25) 
            goto addr_3c83_18;
    }
    fun_2560();
    splitbuf_append_byte(rdi13, 0, rdi13, 0);
    ebp26 = *reinterpret_cast<void***>(rdi13 + 8);
    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi13 + 16)) <= reinterpret_cast<signed char>(static_cast<int64_t>(reinterpret_cast<int32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi13 + 24)) + reinterpret_cast<unsigned char>(ebp26) + 1)))) {
        splitbuf_grow(rdi13);
    }
    rdx27 = reinterpret_cast<int32_t>(ebp26);
    *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(*reinterpret_cast<void***>(rdi13) + rdx27 * 8) + 8) = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi13) + rdx27 * 8);
    *reinterpret_cast<void***>(rdi13 + 8) = ebp26 + 1;
    *reinterpret_cast<signed char*>(rdi13 + 28) = 0;
    goto v28;
    addr_3c83_18:
    return;
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xc228;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s2 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s2* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s2* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0x8ec9);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0x8ec4);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0x8ecd);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0x8ec0);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

void fun_2660(int64_t rdi);

signed char sig_mask_changed = 0;

void** xstrdup(void** rdi);

void** fun_2750();

int32_t operand2sig(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_2810(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, int64_t r9);

void fun_2740(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, int64_t r9);

void** quote(void** rdi, void** rsi, ...);

void usage();

void parse_block_signal_params(void** rdi, int32_t esi, void** rdx, void** rcx, void** r8, int64_t r9, int64_t a7) {
    int32_t r12d8;
    void* rsp9;
    void** rax10;
    void** v11;
    void** rdi12;
    void* rax13;
    int1_t zf14;
    int32_t ebp15;
    void** rax16;
    void** r14_17;
    void* rax18;
    void** r13_19;
    void** r12_20;
    int32_t eax21;
    uint1_t zf22;
    int64_t rsi23;
    int64_t rsi24;
    int64_t rsi25;
    void** rax26;

    r12d8 = esi;
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56);
    rax10 = g28;
    v11 = rax10;
    if (!rdi) {
        if (*reinterpret_cast<signed char*>(&esi)) {
            fun_2660(0xc360);
            rdi12 = reinterpret_cast<void**>(0xc2e0);
        } else {
            fun_2660(0xc2e0);
            rdi12 = reinterpret_cast<void**>(0xc360);
        }
        fun_2640(rdi12);
        sig_mask_changed = 1;
        rax13 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
        if (!rax13) {
            return;
        }
    } else {
        zf14 = sig_mask_changed == 0;
        ebp15 = esi;
        if (zf14) {
            fun_2640(0xc360);
            fun_2640(0xc2e0);
            rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 + 8);
        }
        sig_mask_changed = 1;
        xstrdup(rdi);
        rax16 = fun_2750();
        r14_17 = rax16;
        if (!rax16) 
            goto addr_4498_10; else 
            goto addr_43de_11;
    }
    fun_2560();
    addr_4498_10:
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
    if (!rax18) {
    }
    addr_43de_11:
    r13_19 = reinterpret_cast<void**>(0xc360);
    if (*reinterpret_cast<signed char*>(&r12d8)) {
        r13_19 = reinterpret_cast<void**>(0xc2e0);
    }
    r12_20 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 + 8 + 16);
    do {
        eax21 = operand2sig(r14_17, r12_20, rdx, rcx);
        zf22 = reinterpret_cast<uint1_t>(eax21 == 0);
        if (zf22) 
            break;
        if (reinterpret_cast<uint1_t>(eax21 < 0) | zf22) 
            goto addr_4487_20;
        if (*reinterpret_cast<signed char*>(&ebp15)) {
            *reinterpret_cast<int32_t*>(&rsi23) = eax21;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi23) + 4) = 0;
            fun_2810(0xc360, rsi23, rdx, rcx, r8, r9);
        } else {
            *reinterpret_cast<int32_t*>(&rsi24) = eax21;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi24) + 4) = 0;
            fun_2810(0xc2e0, rsi24, rdx, rcx, r8, r9);
        }
        *reinterpret_cast<int32_t*>(&rsi25) = eax21;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi25) + 4) = 0;
        fun_2740(r13_19, rsi25, rdx, rcx, r8, r9);
        rax26 = fun_2750();
        r14_17 = rax26;
    } while (rax26);
    goto addr_4498_10;
    quote(r14_17, r12_20);
    fun_2520();
    fun_2720();
    addr_4487_20:
    usage();
    goto addr_4498_10;
}

void splitbuf_grow(void** rdi) {
    void** rax2;

    rax2 = xpalloc();
    *reinterpret_cast<void***>(rdi) = rax2;
}

void** signals = reinterpret_cast<void**>(0);

int32_t exit_failure = 1;

void parse_signal_action_params(void** rdi, void** esi, void** rdx, void** rcx, void** r8, int64_t r9, int64_t a7) {
    int64_t rbx8;
    void** rsp9;
    void** rax10;
    void** v11;
    void** rbp12;
    void** r12_13;
    int32_t r12d14;
    int32_t r12d15;
    uint32_t r12d16;
    int32_t r12d17;
    void** rdi18;
    void** rsi19;
    int32_t eax20;
    void** rax21;
    void* rax22;
    void** rax23;
    void** rax24;
    void** r14_25;
    void* rax26;
    int32_t eax27;
    void* rsp28;
    uint1_t zf29;
    void** rax30;
    int32_t r12d31;
    void* rsp32;
    void** rax33;
    void** v34;
    void** rdi35;
    void* rax36;
    int64_t v37;
    int1_t zf38;
    int32_t ebp39;
    void** rax40;
    void** r14_41;
    void* rax42;
    void** r13_43;
    void** r12_44;
    int32_t eax45;
    uint1_t zf46;
    int64_t rsi47;
    int64_t rsi48;
    int64_t rsi49;
    void** rax50;
    void** rax51;
    void** rax52;

    *reinterpret_cast<void***>(&rbx8) = esi;
    rsp9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 32);
    rax10 = g28;
    v11 = rax10;
    if (!rdi) {
        rbp12 = rsp9;
        *reinterpret_cast<void***>(&rbx8) = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx8) + 4) = 0;
        r12_13 = reinterpret_cast<void**>((r12d14 - (r12d15 + reinterpret_cast<uint1_t>(r12d16 < r12d17 + reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&esi) < 1))) & 2) + 2);
        do {
            *reinterpret_cast<int32_t*>(&rdi18) = static_cast<int32_t>(rbx8 + 1);
            *reinterpret_cast<int32_t*>(&rdi18 + 4) = 0;
            rsi19 = rbp12;
            ++rbx8;
            eax20 = sig2str(rdi18, rsi19, rdx, rcx, r8, r9);
            rsp9 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp9 - 8) + 8);
            if (!eax20) {
                rax21 = signals;
                *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rax21) + reinterpret_cast<uint64_t>(rbx8 * 4)) = r12_13;
            }
        } while (rbx8 != 64);
        rax22 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
        if (rax22) 
            goto addr_4361_7;
        return;
    }
    rax23 = xstrdup(rdi);
    rsi19 = reinterpret_cast<void**>(",");
    rdi18 = rax23;
    rax24 = fun_2750();
    rsp9 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp9 - 8) + 8 - 8 + 8);
    r14_25 = rax24;
    if (!rax24) {
        addr_42e0_10:
        rax26 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
        if (rax26) {
            addr_4361_7:
            fun_2560();
        } else {
            goto addr_2460_12;
        }
    } else {
        rbp12 = rsp9;
        *reinterpret_cast<void***>(&rbx8) = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rbx8)) - (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rbx8)) + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rbx8)) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rbx8)) + reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&rbx8) < 1)))) & 2) + 1);
        do {
            rsi19 = rbp12;
            eax27 = operand2sig(r14_25, rsi19, rdx, rcx);
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp9 - 8) + 8);
            zf29 = reinterpret_cast<uint1_t>(eax27 == 0);
            if (zf29) 
                goto addr_429f_15;
            if (reinterpret_cast<uint1_t>(eax27 < 0) | zf29) 
                goto addr_42ce_17;
            rdx = signals;
            rsi19 = reinterpret_cast<void**>(",");
            *reinterpret_cast<int32_t*>(&rdi18) = 0;
            *reinterpret_cast<int32_t*>(&rdi18 + 4) = 0;
            *reinterpret_cast<void***>(rdx + eax27 * 4) = *reinterpret_cast<void***>(&rbx8);
            rax30 = fun_2750();
            rsp9 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp28) - 8 + 8);
            r14_25 = rax30;
        } while (rax30);
        goto addr_42e0_10;
    }
    r12d31 = *reinterpret_cast<int32_t*>(&rsi19);
    rsp32 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp9 - 8) + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 56);
    rax33 = g28;
    v34 = rax33;
    if (!rdi18) {
        if (*reinterpret_cast<signed char*>(&rsi19)) {
            fun_2660(0xc360);
            rdi35 = reinterpret_cast<void**>(0xc2e0);
        } else {
            fun_2660(0xc2e0);
            rdi35 = reinterpret_cast<void**>(0xc360);
        }
        fun_2640(rdi35);
        sig_mask_changed = 1;
        rax36 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v34) - reinterpret_cast<unsigned char>(g28));
        if (!rax36) {
            goto v37;
        }
    }
    zf38 = sig_mask_changed == 0;
    ebp39 = *reinterpret_cast<int32_t*>(&rsi19);
    if (zf38) {
        fun_2640(0xc360);
        fun_2640(0xc2e0);
        rsp32 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp32) - 8 + 8 - 8 + 8);
    }
    sig_mask_changed = 1;
    xstrdup(rdi18);
    rax40 = fun_2750();
    r14_41 = rax40;
    if (rax40) 
        goto addr_43de_28;
    addr_4498_29:
    rax42 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v34) - reinterpret_cast<unsigned char>(g28));
    if (rax42) {
        fun_2560();
    } else {
        goto addr_2460_12;
    }
    addr_2460_12:
    addr_43de_28:
    r13_43 = reinterpret_cast<void**>(0xc360);
    if (*reinterpret_cast<signed char*>(&r12d31)) {
        r13_43 = reinterpret_cast<void**>(0xc2e0);
    }
    r12_44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp32) - 8 + 8 - 8 + 8 + 16);
    do {
        eax45 = operand2sig(r14_41, r12_44, rdx, rcx);
        zf46 = reinterpret_cast<uint1_t>(eax45 == 0);
        if (zf46) 
            break;
        if (reinterpret_cast<uint1_t>(eax45 < 0) | zf46) 
            goto addr_4487_37;
        if (*reinterpret_cast<signed char*>(&ebp39)) {
            *reinterpret_cast<int32_t*>(&rsi47) = eax45;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi47) + 4) = 0;
            fun_2810(0xc360, rsi47, rdx, rcx, r8, r9);
        } else {
            *reinterpret_cast<int32_t*>(&rsi48) = eax45;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi48) + 4) = 0;
            fun_2810(0xc2e0, rsi48, rdx, rcx, r8, r9);
        }
        *reinterpret_cast<int32_t*>(&rsi49) = eax45;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi49) + 4) = 0;
        fun_2740(r13_43, rsi49, rdx, rcx, r8, r9);
        rax50 = fun_2750();
        r14_41 = rax50;
    } while (rax50);
    goto addr_4498_29;
    quote(r14_41, r12_44, r14_41, r12_44);
    fun_2520();
    fun_2720();
    addr_4487_37:
    usage();
    goto addr_4498_29;
    addr_429f_15:
    rax51 = quote(r14_25, rsi19);
    rax52 = fun_2520();
    rcx = rax51;
    *reinterpret_cast<int32_t*>(&rsi19) = 0;
    rdx = rax52;
    fun_2720();
    rsp28 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp28) - 8 + 8 - 8 + 8 - 8 + 8);
    addr_42ce_17:
    *reinterpret_cast<int32_t*>(&rdi18) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi18 + 4) = 0;
    usage();
    rsp9 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp28) - 8 + 8);
    goto addr_42e0_10;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gbdb8 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gbdb8;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2403() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2413() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2423() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2433() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t getenv = 0x2030;

void fun_2443() {
    __asm__("cli ");
    goto getenv;
}

int64_t sigprocmask = 0x2040;

void fun_2453() {
    __asm__("cli ");
    goto sigprocmask;
}

int64_t free = 0x2050;

void fun_2463() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2060;

void fun_2473() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2070;

void fun_2483() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2080;

void fun_2493() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2090;

void fun_24a3() {
    __asm__("cli ");
    goto _exit;
}

int64_t strcpy = 0x20a0;

void fun_24b3() {
    __asm__("cli ");
    goto strcpy;
}

int64_t __fpending = 0x20b0;

void fun_24c3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t sigaction = 0x20c0;

void fun_24d3() {
    __asm__("cli ");
    goto sigaction;
}

int64_t reallocarray = 0x20d0;

void fun_24e3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t textdomain = 0x20e0;

void fun_24f3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20f0;

void fun_2503() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x2100;

void fun_2513() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2110;

void fun_2523() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2120;

void fun_2533() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2130;

void fun_2543() {
    __asm__("cli ");
    goto strlen;
}

int64_t chdir = 0x2140;

void fun_2553() {
    __asm__("cli ");
    goto chdir;
}

int64_t __stack_chk_fail = 0x2150;

void fun_2563() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2160;

void fun_2573() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2170;

void fun_2583() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x2180;

void fun_2593() {
    __asm__("cli ");
    goto strchr;
}

int64_t strrchr = 0x2190;

void fun_25a3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x21a0;

void fun_25b3() {
    __asm__("cli ");
    goto lseek;
}

int64_t memset = 0x21b0;

void fun_25c3() {
    __asm__("cli ");
    goto memset;
}

int64_t strspn = 0x21c0;

void fun_25d3() {
    __asm__("cli ");
    goto strspn;
}

int64_t memcmp = 0x21d0;

void fun_25e3() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21e0;

void fun_25f3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21f0;

void fun_2603() {
    __asm__("cli ");
    goto calloc;
}

int64_t putenv = 0x2200;

void fun_2613() {
    __asm__("cli ");
    goto putenv;
}

int64_t strcmp = 0x2210;

void fun_2623() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2220;

void fun_2633() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t sigemptyset = 0x2230;

void fun_2643() {
    __asm__("cli ");
    goto sigemptyset;
}

int64_t strtol = 0x2240;

void fun_2653() {
    __asm__("cli ");
    goto strtol;
}

int64_t sigfillset = 0x2250;

void fun_2663() {
    __asm__("cli ");
    goto sigfillset;
}

int64_t memcpy = 0x2260;

void fun_2673() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2270;

void fun_2683() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2280;

void fun_2693() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2290;

void fun_26a3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x22a0;

void fun_26b3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t strpbrk = 0x22b0;

void fun_26c3() {
    __asm__("cli ");
    goto strpbrk;
}

int64_t __freading = 0x22c0;

void fun_26d3() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x22d0;

void fun_26e3() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x22e0;

void fun_26f3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22f0;

void fun_2703() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t memmove = 0x2300;

void fun_2713() {
    __asm__("cli ");
    goto memmove;
}

int64_t error = 0x2310;

void fun_2723() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x2320;

void fun_2733() {
    __asm__("cli ");
    goto fseeko;
}

int64_t sigdelset = 0x2330;

void fun_2743() {
    __asm__("cli ");
    goto sigdelset;
}

int64_t strtok = 0x2340;

void fun_2753() {
    __asm__("cli ");
    goto strtok;
}

int64_t unsetenv = 0x2350;

void fun_2763() {
    __asm__("cli ");
    goto unsetenv;
}

int64_t execvp = 0x2360;

void fun_2773() {
    __asm__("cli ");
    goto execvp;
}

int64_t __cxa_atexit = 0x2370;

void fun_2783() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t __libc_current_sigrtmin = 0x2380;

void fun_2793() {
    __asm__("cli ");
    goto __libc_current_sigrtmin;
}

int64_t sigismember = 0x2390;

void fun_27a3() {
    __asm__("cli ");
    goto sigismember;
}

int64_t exit = 0x23a0;

void fun_27b3() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x23b0;

void fun_27c3() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x23c0;

void fun_27d3() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t __libc_current_sigrtmax = 0x23d0;

void fun_27e3() {
    __asm__("cli ");
    goto __libc_current_sigrtmax;
}

int64_t mbsinit = 0x23e0;

void fun_27f3() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x23f0;

void fun_2803() {
    __asm__("cli ");
    goto iswprint;
}

int64_t sigaddset = 0x2400;

void fun_2813() {
    __asm__("cli ");
    goto sigaddset;
}

int64_t __ctype_b_loc = 0x2410;

void fun_2823() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

int64_t __sprintf_chk = 0x2420;

void fun_2833() {
    __asm__("cli ");
    goto __sprintf_chk;
}

void set_program_name(void** rdi);

void** fun_26f0(int64_t rdi, ...);

void fun_2510(int64_t rdi);

void fun_24f0(int64_t rdi);

void atexit(int64_t rdi);

void** xmalloc(int64_t rdi);

uint32_t fun_2570(void** rdi, void** rsi, void** rdx, void** rcx);

uint32_t optind = 0;

int32_t fun_2620(void** rdi, void** rsi, void** rdx, void** rcx);

int64_t usvars_used = 0;

void** usvars = reinterpret_cast<void**>(0);

unsigned char dev_debug = 0;

int32_t fun_2760(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_27c0(void** rdi, void** rsi, void** rdx, void** rcx);

void** fun_2590(void** rdi, void** rsi, void** rdx, void** rcx, ...);

int32_t fun_2610(void** rdi, void** rsi, void** rdx, void** rcx);

signed char report_signal_handling = 0;

void** quotearg_style(int64_t rdi, int64_t rsi, void** rdx, void** rcx, void** r8, int64_t r9);

int32_t fun_2550(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9);

void fun_2770(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9);

int64_t fun_26c0(void** rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8, int64_t r9);

void fun_27b0();

void*** __environ = reinterpret_cast<void***>(0);

void** fun_2670(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9, int64_t a7);

void** stdout = reinterpret_cast<void**>(0);

int64_t Version = 0x8e5c;

void version_etc(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, void** r8, int64_t r9, int64_t a7, int64_t a8);

void fun_2700(int64_t rdi, void** rsi, void** rdx, void** rcx);

int64_t fun_2883(uint32_t edi, void** rsi) {
    void** r15_3;
    void** rbp4;
    int64_t rbx5;
    void** r14_6;
    void** rdi7;
    void** rax8;
    void** v9;
    void** rax10;
    void* rsp11;
    int64_t* rdi12;
    int64_t rcx13;
    int32_t* v14;
    signed char v15;
    void** r8_16;
    void** rcx17;
    void** rsi18;
    void** rdi19;
    void** rdx20;
    uint32_t eax21;
    void** r12_22;
    void** r13_23;
    int32_t eax24;
    int1_t less_or_equal25;
    int64_t r13_26;
    int1_t zf27;
    void** rdi28;
    int32_t eax29;
    int1_t less30;
    int1_t zf31;
    void* rsp32;
    void** rax33;
    int1_t zf34;
    void** rdi35;
    int64_t rax36;
    int32_t eax37;
    int64_t rax38;
    void** v39;
    void* rbx40;
    void** rax41;
    int32_t eax42;
    void** r12_43;
    int32_t eax44;
    void** rax45;
    int1_t zf46;
    int64_t r9_47;
    int1_t zf48;
    void** rax49;
    void** v50;
    int32_t* rax51;
    void* rsp52;
    int1_t zf53;
    void** rdx54;
    int32_t eax55;
    void* rsp56;
    uint32_t v57;
    void** v58;
    int64_t rsi59;
    int32_t eax60;
    void* rsp61;
    int64_t rsi62;
    int64_t rbx63;
    int32_t eax64;
    int64_t rsi65;
    int1_t zf66;
    void** r15_67;
    void** rdi68;
    int1_t zf69;
    void** rdi70;
    int32_t eax71;
    int1_t zf72;
    uint32_t eax73;
    void** rax74;
    void** rdi75;
    int32_t eax76;
    int1_t zf77;
    int64_t rax78;
    void** rdi79;
    int1_t zf80;
    void** rdi81;
    void** rax82;
    void** rdi83;
    int64_t rax84;
    void** rax85;
    void** rdi86;
    void* rsp87;
    int64_t rax88;
    void** rdi89;
    void** rax90;
    void** rcx91;
    int64_t rax92;
    void** rdi93;
    int64_t rax94;
    void* rsp95;
    int64_t rax96;
    void** rdi97;
    void** rax98;
    void* rsp99;
    void** rax100;
    void** rsi101;
    void** r14_102;
    signed char v103;
    int64_t rax104;
    uint32_t eax105;
    int32_t edx106;
    uint32_t eax107;
    void** v108;
    uint32_t v109;
    void** v110;
    int64_t v111;
    int64_t rdx112;
    void** r12_113;
    void** rax114;
    void** rsi115;
    int1_t zf116;
    void** rdi117;
    int64_t rax118;
    void** rsi119;
    int64_t v120;
    void** v121;
    void** rax122;
    void** rdi123;
    int1_t zf124;
    void** rdi125;
    void** rax126;
    void** rdi127;
    int64_t rax128;
    int1_t zf129;
    void** rdi130;
    void** rax131;
    void** rdi132;
    void** rdi133;
    int64_t rcx134;
    void*** rbx135;
    void** rdx136;
    void** rcx137;
    void* rax138;
    void** rdi139;
    void** rax140;
    void** rax141;
    int64_t rax142;
    void** rdi143;
    int64_t rdx144;
    int64_t rax145;

    __asm__("cli ");
    r15_3 = rsi;
    rbp4 = reinterpret_cast<void**>("coreutils");
    *reinterpret_cast<uint32_t*>(&rbx5) = edi;
    *reinterpret_cast<uint32_t*>(&r14_6) = *reinterpret_cast<uint32_t*>(&rbx5);
    rdi7 = *reinterpret_cast<void***>(rsi);
    rax8 = g28;
    v9 = rax8;
    set_program_name(rdi7);
    fun_26f0(6, 6);
    fun_2510("coreutils");
    fun_24f0("coreutils");
    exit_failure = 0x7d;
    atexit(0x4750);
    rax10 = xmalloc(0x104);
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x128 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    signals = rax10;
    rdi12 = reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rax10 + 8) & 0xfffffffffffffff8);
    *reinterpret_cast<void***>(rax10) = reinterpret_cast<void**>(0);
    *reinterpret_cast<int64_t*>(rax10 + 0xfc) = 0;
    *reinterpret_cast<uint32_t*>(&rcx13) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(rax10) - reinterpret_cast<uint64_t>(rdi12) + 0x104) >> 3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx13) + 4) = 0;
    while (rcx13) {
        --rcx13;
        *rdi12 = 0;
        ++rdi12;
    }
    *reinterpret_cast<unsigned char*>(&v14) = 0;
    v15 = 0;
    while (1) {
        *reinterpret_cast<uint32_t*>(&r8_16) = 0;
        *reinterpret_cast<int32_t*>(&r8_16 + 4) = 0;
        rcx17 = reinterpret_cast<void**>(0xb9c0);
        rsi18 = r15_3;
        *reinterpret_cast<uint32_t*>(&rdi19) = *reinterpret_cast<uint32_t*>(&r14_6);
        *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
        rdx20 = reinterpret_cast<void**>(0x8e30);
        eax21 = fun_2570(rdi19, rsi18, 0x8e30, 0xb9c0);
        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
        *reinterpret_cast<uint32_t*>(&r12_22) = eax21;
        if (eax21 == 0xffffffff) {
            *reinterpret_cast<uint32_t*>(&r13_23) = optind;
            *reinterpret_cast<uint32_t*>(&rbx5) = *reinterpret_cast<uint32_t*>(&r14_6);
            if (*reinterpret_cast<int32_t*>(&r13_23) < *reinterpret_cast<int32_t*>(&r14_6) && (rsi18 = reinterpret_cast<void**>("-"), rdi19 = *reinterpret_cast<void***>(r15_3 + *reinterpret_cast<int32_t*>(&r13_23) * 8), eax24 = fun_2620(rdi19, "-", 0x8e30, 0xb9c0), rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8), eax24 == 0)) {
                optind = *reinterpret_cast<uint32_t*>(&r13_23) + 1;
                goto addr_2f14_8;
            }
            if (!v15) {
                less_or_equal25 = usvars_used <= 0;
                if (!less_or_equal25) {
                    r14_6 = usvars;
                    *reinterpret_cast<int32_t*>(&r13_26) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_26) + 4) = 0;
                    r12_22 = reinterpret_cast<void**>("unset:    %s\n");
                    do {
                        zf27 = dev_debug == 0;
                        if (!zf27) {
                            rcx17 = *reinterpret_cast<void***>(r14_6 + r13_26 * 8);
                            rdi28 = stderr;
                            rdx20 = reinterpret_cast<void**>("unset:    %s\n");
                            *reinterpret_cast<int32_t*>(&rsi18) = 1;
                            *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
                            fun_27d0(rdi28, 1, "unset:    %s\n", rdi28, 1, "unset:    %s\n");
                            rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                        }
                        r14_6 = usvars;
                        rbp4 = r14_6 + r13_26 * 8;
                        rdi19 = *reinterpret_cast<void***>(rbp4);
                        eax29 = fun_2760(rdi19, rsi18, rdx20, rcx17);
                        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                        if (eax29) 
                            goto addr_38c4_15;
                        ++r13_26;
                        less30 = r13_26 < usvars_used;
                    } while (less30);
                    *reinterpret_cast<uint32_t*>(&r13_23) = optind;
                }
            } else {
                addr_2f14_8:
                zf31 = dev_debug == 0;
                if (!zf31) {
                    rcx17 = stderr;
                    *reinterpret_cast<int32_t*>(&rdx20) = 17;
                    *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rsi18) = 1;
                    *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
                    rdi19 = reinterpret_cast<void**>("cleaning environ\n");
                    fun_27c0("cleaning environ\n", 1, 17, rcx17);
                    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                    goto addr_2f21_19;
                }
            }
        } else {
            if (*reinterpret_cast<int32_t*>(&r12_22) > reinterpret_cast<int32_t>(0x83)) {
                addr_34d0_21:
                usage();
                rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                goto addr_34da_22;
            } else {
                if (*reinterpret_cast<int32_t*>(&r12_22) <= reinterpret_cast<int32_t>(66)) {
                    if (*reinterpret_cast<int32_t*>(&r12_22) > reinterpret_cast<int32_t>(13)) {
                        addr_2df0_25:
                        if (*reinterpret_cast<uint32_t*>(&r12_22) == 32) {
                            addr_380e_26:
                            fun_2520();
                            fun_2720();
                            rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8);
                            goto addr_34bb_27;
                        } else {
                            if (*reinterpret_cast<uint32_t*>(&r12_22) == 48) {
                                *reinterpret_cast<unsigned char*>(&v14) = 1;
                                continue;
                            }
                        }
                    } else {
                        if (*reinterpret_cast<int32_t*>(&r12_22) > reinterpret_cast<int32_t>(8)) 
                            goto addr_380e_26;
                        if (*reinterpret_cast<uint32_t*>(&r12_22) != 0xffffff7d) 
                            goto addr_2e0e_32; else 
                            goto addr_2da9_33;
                    }
                } else {
                    *reinterpret_cast<uint32_t*>(&r12_22) = *reinterpret_cast<uint32_t*>(&r12_22) - 67;
                    *reinterpret_cast<int32_t*>(&r12_22 + 4) = 0;
                    if (*reinterpret_cast<uint32_t*>(&r12_22) > 64) 
                        goto addr_34d0_21; else 
                        goto addr_299d_35;
                }
            }
        }
        addr_2f36_36:
        if (*reinterpret_cast<int32_t*>(&rbx5) <= *reinterpret_cast<int32_t*>(&r13_23)) {
            addr_2fc7_37:
            if (1) {
                if (*reinterpret_cast<int32_t*>(&r13_23) < *reinterpret_cast<int32_t*>(&rbx5)) 
                    goto addr_2fe8_39; else 
                    break;
            }
        } else {
            r12_22 = reinterpret_cast<void**>("setenv:   %s\n");
            do {
                *reinterpret_cast<int32_t*>(&rsi18) = 61;
                *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
                r14_6 = *reinterpret_cast<void***>(r15_3 + *reinterpret_cast<int32_t*>(&r13_23) * 8);
                rdi19 = r14_6;
                rax33 = fun_2590(rdi19, 61, rdx20, rcx17);
                rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                rbp4 = rax33;
                if (!rax33) 
                    goto addr_2fbe_42;
                zf34 = dev_debug == 0;
                if (!zf34) {
                    rdi35 = stderr;
                    rcx17 = r14_6;
                    rdx20 = reinterpret_cast<void**>("setenv:   %s\n");
                    *reinterpret_cast<int32_t*>(&rsi18) = 1;
                    *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
                    fun_27d0(rdi35, 1, "setenv:   %s\n", rdi35, 1, "setenv:   %s\n");
                    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                }
                rax36 = reinterpret_cast<int32_t>(optind);
                rdi19 = *reinterpret_cast<void***>(r15_3 + rax36 * 8);
                eax37 = fun_2610(rdi19, rsi18, rdx20, rcx17);
                rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                if (eax37) 
                    goto addr_3961_46;
                *reinterpret_cast<uint32_t*>(&rax38) = optind;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax38) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r13_23) = static_cast<uint32_t>(rax38 + 1);
                optind = *reinterpret_cast<uint32_t*>(&r13_23);
            } while (*reinterpret_cast<int32_t*>(&r13_23) < *reinterpret_cast<int32_t*>(&rbx5));
            goto addr_2fc7_37;
        }
        *reinterpret_cast<int32_t*>(&rdx20) = 5;
        *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
        rsi18 = reinterpret_cast<void**>("must specify command with --chdir (-C)");
        if (*reinterpret_cast<int32_t*>(&r13_23) >= *reinterpret_cast<int32_t*>(&rbx5)) {
            addr_34bb_27:
            fun_2520();
            fun_2720();
            rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8);
            goto addr_34d0_21;
        } else {
            addr_2fe8_39:
            *reinterpret_cast<uint32_t*>(&v14) = *reinterpret_cast<uint32_t*>(&rbx5);
            *reinterpret_cast<uint32_t*>(&r13_23) = 1;
            v39 = r15_3;
            rbx40 = reinterpret_cast<void*>(4);
        }
        while (1) {
            rax41 = signals;
            *reinterpret_cast<uint32_t*>(&rbp4) = *reinterpret_cast<uint32_t*>(reinterpret_cast<unsigned char>(rax41) + reinterpret_cast<uint64_t>(rbx40));
            *reinterpret_cast<int32_t*>(&rbp4 + 4) = 0;
            if (!*reinterpret_cast<uint32_t*>(&rbp4)) 
                goto addr_3022_50;
            r15_3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp11) + 96);
            *reinterpret_cast<int32_t*>(&rsi18) = 0;
            *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_6) = static_cast<uint32_t>(reinterpret_cast<uint64_t>(rbp4 + 0xffffffffffffffff));
            *reinterpret_cast<uint32_t*>(&rdi19) = *reinterpret_cast<uint32_t*>(&r13_23);
            *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
            rdx20 = r15_3;
            eax42 = fun_24d0(rdi19);
            rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
            *reinterpret_cast<uint32_t*>(&rbp4) = *reinterpret_cast<uint32_t*>(&rbp4) - 2 & 0xfffffffd;
            *reinterpret_cast<int32_t*>(&rbp4 + 4) = 0;
            *reinterpret_cast<int32_t*>(&r12_43) = eax42;
            *reinterpret_cast<unsigned char*>(&rbp4) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<uint32_t*>(&rbp4));
            if (eax42) {
                if (*reinterpret_cast<unsigned char*>(&rbp4)) 
                    goto addr_3871_53;
                if (!eax42) 
                    goto addr_3069_55;
            } else {
                addr_3069_55:
                rsi18 = r15_3;
                *reinterpret_cast<uint32_t*>(&rdi19) = *reinterpret_cast<uint32_t*>(&r13_23);
                *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdx20) = 0;
                *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
                eax44 = fun_24d0(rdi19, rdi19);
                rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                *reinterpret_cast<int32_t*>(&r12_43) = eax44;
                if (eax44 && *reinterpret_cast<unsigned char*>(&rbp4)) {
                    rax45 = fun_2520();
                    r12_43 = rax45;
                    fun_2480();
                    *reinterpret_cast<uint32_t*>(&rcx17) = *reinterpret_cast<uint32_t*>(&r13_23);
                    *reinterpret_cast<int32_t*>(&rcx17 + 4) = 0;
                    rdx20 = r12_43;
                    fun_2720();
                    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8 - 8 + 8);
                    goto addr_30c0_57;
                }
            }
            zf46 = dev_debug == 0;
            if (!zf46) {
                addr_30c0_57:
                *reinterpret_cast<uint32_t*>(&rdi19) = *reinterpret_cast<uint32_t*>(&r13_23);
                *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
                rsi18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp11) + 0x100);
                sig2str(rdi19, rsi18, rdx20, rcx17, r8_16, r9_47);
                rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                zf48 = dev_debug == 0;
                if (!zf48) {
                    rax49 = reinterpret_cast<void**>(0x92f1);
                    v50 = rdx20;
                    if (*reinterpret_cast<int32_t*>(&r12_43)) {
                        rax49 = reinterpret_cast<void**>(" (failure ignored)");
                    }
                    r9_47 = reinterpret_cast<int64_t>("IGNORE");
                    rdi19 = stderr;
                    *reinterpret_cast<uint32_t*>(&r8_16) = *reinterpret_cast<uint32_t*>(&r13_23);
                    *reinterpret_cast<int32_t*>(&r8_16 + 4) = 0;
                    if (*reinterpret_cast<uint32_t*>(&r14_6) < 2) {
                        r9_47 = reinterpret_cast<int64_t>("DEFAULT");
                    }
                    rdx20 = reinterpret_cast<void**>("Reset signal %s (%d) to %s%s\n");
                    fun_27d0(rdi19, 1, "Reset signal %s (%d) to %s%s\n", rdi19, 1, "Reset signal %s (%d) to %s%s\n");
                    rcx17 = rax49;
                    rsi18 = v50;
                    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 - 8 - 8 + 8 + 8 + 8);
                    goto addr_3022_50;
                }
            } else {
                addr_3022_50:
                *reinterpret_cast<uint32_t*>(&r13_23) = *reinterpret_cast<uint32_t*>(&r13_23) + 1;
                rbx40 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx40) + 4);
                if (*reinterpret_cast<uint32_t*>(&r13_23) == 65) 
                    break;
            }
        }
        rax51 = fun_2480();
        rsp52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
        zf53 = sig_mask_changed == 0;
        *reinterpret_cast<uint32_t*>(&rbx5) = *reinterpret_cast<uint32_t*>(&v14);
        r15_3 = v39;
        v14 = rax51;
        if (!zf53) {
            r12_22 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp52) + 96);
            *reinterpret_cast<uint32_t*>(&r13_23) = 1;
            rbp4 = reinterpret_cast<void**>(0xc360);
            r14_6 = reinterpret_cast<void**>(0xc2e0);
            fun_2640(r12_22, r12_22);
            rdx54 = r12_22;
            eax55 = fun_2450();
            rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp52) - 8 + 8 - 8 + 8);
            if (eax55) 
                goto addr_3848_66;
            v57 = *reinterpret_cast<uint32_t*>(&rbx5);
            v58 = r15_3;
            while (1) {
                *reinterpret_cast<uint32_t*>(&rsi59) = *reinterpret_cast<uint32_t*>(&r13_23);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi59) + 4) = 0;
                eax60 = fun_27a0(0xc360, rsi59, rdx54, rcx17, r8_16, r9_47);
                rsp61 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
                *reinterpret_cast<uint32_t*>(&rsi62) = *reinterpret_cast<uint32_t*>(&r13_23);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi62) + 4) = 0;
                if (eax60) {
                    rbx63 = reinterpret_cast<int64_t>("BLOCK");
                    fun_2810(r12_22, rsi62, rdx54, rcx17, r8_16, r9_47);
                    rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) - 8 + 8);
                } else {
                    eax64 = fun_27a0(0xc2e0, rsi62, rdx54, rcx17, r8_16, r9_47);
                    rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) - 8 + 8);
                    if (!eax64) {
                        addr_3748_71:
                        *reinterpret_cast<uint32_t*>(&r13_23) = *reinterpret_cast<uint32_t*>(&r13_23) + 1;
                        if (*reinterpret_cast<uint32_t*>(&r13_23) == 65) 
                            break; else 
                            continue;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rsi65) = *reinterpret_cast<uint32_t*>(&r13_23);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi65) + 4) = 0;
                        rbx63 = reinterpret_cast<int64_t>("UNBLOCK");
                        fun_2740(r12_22, rsi65, rdx54, rcx17, r8_16, r9_47);
                        rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
                    }
                }
                zf66 = dev_debug == 0;
                if (!zf66 && (r15_67 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp56) + 0x100), *reinterpret_cast<uint32_t*>(&rdi68) = *reinterpret_cast<uint32_t*>(&r13_23), *reinterpret_cast<int32_t*>(&rdi68 + 4) = 0, sig2str(rdi68, r15_67, rdx54, rcx17, r8_16, r9_47), rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8), zf69 = dev_debug == 0, !zf69)) {
                    rdi70 = stderr;
                    r9_47 = rbx63;
                    *reinterpret_cast<uint32_t*>(&r8_16) = *reinterpret_cast<uint32_t*>(&r13_23);
                    *reinterpret_cast<int32_t*>(&r8_16 + 4) = 0;
                    rcx17 = r15_67;
                    rdx54 = reinterpret_cast<void**>("signal %s (%d) mask set to %s\n");
                    fun_27d0(rdi70, 1, "signal %s (%d) mask set to %s\n", rdi70, 1, "signal %s (%d) mask set to %s\n");
                    rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
                    goto addr_3748_71;
                }
            }
            *reinterpret_cast<int32_t*>(&rdx20) = 0;
            *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
            rsi18 = r12_22;
            *reinterpret_cast<uint32_t*>(&rdi19) = 2;
            *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rbx5) = v57;
            r15_3 = v58;
            eax71 = fun_2450();
            rsp52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
            if (!eax71) 
                goto addr_33c8_76;
        } else {
            addr_33c8_76:
            zf72 = report_signal_handling == 0;
            if (!zf72) {
                list_signal_handling(rdi19, rsi18, rdx20, rcx17, r8_16, r9_47);
                rsp52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp52) - 8 + 8);
                goto addr_33d5_78;
            }
        }
        fun_2520();
        fun_2720();
        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp52) - 8 + 8 - 8 + 8);
        goto addr_380e_26;
        addr_33d5_78:
        eax73 = dev_debug;
        if (!1) {
            if (*reinterpret_cast<signed char*>(&eax73)) {
                rax74 = quotearg_style(4, 0, rdx20, rcx17, r8_16, r9_47);
                rdi75 = stderr;
                *reinterpret_cast<int32_t*>(&rsi18) = 1;
                *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
                rdx20 = reinterpret_cast<void**>("chdir:    %s\n");
                rcx17 = rax74;
                fun_27d0(rdi75, 1, "chdir:    %s\n", rdi75, 1, "chdir:    %s\n");
                rsp52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp52) - 8 + 8 - 8 + 8);
            }
            eax76 = fun_2550(0, rsi18, rdx20, rcx17, r8_16, r9_47);
            rsp52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp52) - 8 + 8);
            if (eax76) 
                goto addr_38ff_83;
        }
        zf77 = dev_debug == 0;
        if (!zf77) {
            rax78 = reinterpret_cast<int32_t>(optind);
            rdi79 = stderr;
            rdx20 = reinterpret_cast<void**>("executing: %s\n");
            rcx17 = *reinterpret_cast<void***>(r15_3 + rax78 * 8);
            fun_27d0(rdi79, 1, "executing: %s\n", rdi79, 1, "executing: %s\n");
            rsp52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp52) - 8 + 8);
            rbp4 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(optind)));
            while (*reinterpret_cast<int32_t*>(&rbx5) > *reinterpret_cast<int32_t*>(&rbp4)) {
                zf80 = dev_debug == 0;
                if (!zf80) {
                    rdi81 = *reinterpret_cast<void***>(r15_3 + reinterpret_cast<unsigned char>(rbp4) * 8);
                    rax82 = quote(rdi81, 1, rdi81, 1);
                    rdx20 = reinterpret_cast<void**>("   arg[%d]= %s\n");
                    rdi83 = stderr;
                    r8_16 = rax82;
                    *reinterpret_cast<uint32_t*>(&rcx17) = *reinterpret_cast<uint32_t*>(&rbp4) - optind;
                    *reinterpret_cast<int32_t*>(&rcx17 + 4) = 0;
                    fun_27d0(rdi83, 1, "   arg[%d]= %s\n", rdi83, 1, "   arg[%d]= %s\n");
                    rsp52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp52) - 8 + 8 - 8 + 8);
                }
                ++rbp4;
            }
        }
        rax84 = reinterpret_cast<int32_t>(optind);
        rax85 = r15_3 + rax84 * 8;
        rdi86 = *reinterpret_cast<void***>(rax85);
        fun_2770(rdi86, rax85, rdx20, rcx17, r8_16, r9_47);
        rsp87 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp52) - 8 + 8);
        if (*v14 == 2) {
            rax88 = reinterpret_cast<int32_t>(optind);
            rdi89 = *reinterpret_cast<void***>(r15_3 + rax88 * 8);
            rax90 = quote(rdi89, rax85, rdi89, rax85);
            rcx91 = rax90;
            fun_2720();
            rax92 = reinterpret_cast<int32_t>(optind);
            rdi93 = *reinterpret_cast<void***>(r15_3 + rax92 * 8);
            rax94 = fun_26c0(rdi93, 0x8117, "%s", rcx91, r8_16, r9_47);
            rsp95 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp87) - 8 + 8 - 8 + 8 - 8 + 8);
            if (rax94) {
                fun_2520();
                fun_2720();
                rsp95 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8 - 8 + 8);
            }
        } else {
            rax96 = reinterpret_cast<int32_t>(optind);
            rdi97 = *reinterpret_cast<void***>(r15_3 + rax96 * 8);
            rax98 = quote(rdi97, rax85, rdi97, rax85);
            rcx91 = rax98;
            fun_2720();
            rsp95 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp87) - 8 + 8 - 8 + 8);
        }
        fun_27b0();
        rsp99 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8);
        goto addr_3463_96;
        addr_2fbe_42:
        if (*reinterpret_cast<int32_t*>(&r13_23) < *reinterpret_cast<int32_t*>(&rbx5) && *reinterpret_cast<unsigned char*>(&v14)) {
            goto addr_34bb_27;
        }
        addr_2f21_19:
        *reinterpret_cast<uint32_t*>(&r13_23) = optind;
        __environ = reinterpret_cast<void***>(0xc2c0);
        goto addr_2f36_36;
        do {
            addr_34da_22:
            rax100 = fun_2520();
            *reinterpret_cast<uint32_t*>(&rsi101) = 0;
            *reinterpret_cast<int32_t*>(&rsi101 + 4) = 0;
            fun_2720();
            rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8 - 8 + 8);
            r14_102 = rax100;
            *reinterpret_cast<uint32_t*>(&r13_23) = 12;
            while (1) {
                r15_3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp32) + 64);
                if (v103) {
                    check_start_new_arg_part_0(r15_3, rsi101);
                    rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                }
                *reinterpret_cast<uint32_t*>(&rsi101) = *reinterpret_cast<uint32_t*>(&r13_23);
                *reinterpret_cast<int32_t*>(&rsi101 + 4) = 0;
                splitbuf_append_byte(r15_3, *reinterpret_cast<uint32_t*>(&rsi101));
                rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                ++r14_102;
                *reinterpret_cast<uint32_t*>(&rax104) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_102));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax104) + 4) = 0;
                if (!*reinterpret_cast<signed char*>(&rax104)) 
                    goto addr_2b1f_103;
                if (*reinterpret_cast<signed char*>(&rax104) > 39) {
                    if (*reinterpret_cast<signed char*>(&rax104) == 92) {
                        eax105 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_102 + 1));
                        if (!*reinterpret_cast<signed char*>(&r12_22)) 
                            break;
                        if (*reinterpret_cast<signed char*>(&eax105) == 92) 
                            break;
                        *reinterpret_cast<uint32_t*>(&r13_23) = 92;
                        if (*reinterpret_cast<signed char*>(&eax105) != 39) 
                            continue; else 
                            goto addr_2d54_109;
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rax104) <= 8) 
                        goto addr_2af0_111;
                    edx106 = static_cast<int32_t>(rax104 - 9);
                    if (*reinterpret_cast<unsigned char*>(&edx106) <= 30) 
                        goto addr_2acc_113;
                }
                addr_2af0_111:
                *reinterpret_cast<uint32_t*>(&r13_23) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(&rax104)));
            }
            addr_2d58_114:
            if (!*reinterpret_cast<signed char*>(&eax105)) 
                goto addr_38a0_115; else 
                continue;
            addr_2d54_109:
            goto addr_2d58_114;
            eax107 = eax105 - 34;
        } while (*reinterpret_cast<unsigned char*>(&eax107) > 84);
        goto addr_2d73_117;
        addr_2b1f_103:
        r15_3 = v108;
        if (*reinterpret_cast<signed char*>(&r12_22)) 
            goto addr_393d_118;
        if (*reinterpret_cast<signed char*>(&rbx5)) 
            goto addr_393d_118;
        splitbuf_append_byte(reinterpret_cast<int64_t>(rsp32) + 64, 0);
        rsp99 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
        *reinterpret_cast<uint32_t*>(&r14_6) = v109;
        *reinterpret_cast<int32_t*>(&r14_6 + 4) = 0;
        rbp4 = v110;
        rcx91 = reinterpret_cast<void**>(v111 * 8);
        if (*reinterpret_cast<int32_t*>(&r14_6) <= reinterpret_cast<int32_t>(1)) {
            addr_3463_96:
            *reinterpret_cast<void***>(rbp4) = *reinterpret_cast<void***>(r15_3);
        } else {
            *reinterpret_cast<int32_t*>(&rdx112) = static_cast<int32_t>(reinterpret_cast<uint64_t>(r14_6 + 0xfffffffffffffffe));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx112) + 4) = 0;
            r12_113 = rbp4 + 16;
            rax114 = rbp4 + 8;
            rsi115 = r12_113 + rdx112 * 8;
            do {
                rax114 = rax114 + 8;
                *reinterpret_cast<void**>(rax114 + 0xfffffffffffffff8) = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax114)) + reinterpret_cast<unsigned char>(rcx91)) + reinterpret_cast<unsigned char>(rbp4));
            } while (rsi115 != rax114);
            zf116 = dev_debug == 0;
            *reinterpret_cast<void***>(rbp4) = *reinterpret_cast<void***>(r15_3);
            if (!zf116) 
                goto addr_322c_124;
        }
        addr_2ba2_125:
        *reinterpret_cast<uint32_t*>(&rbx5) = *reinterpret_cast<uint32_t*>(&v58);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx5) + 4) = 0;
        rdi117 = rbp4 + *reinterpret_cast<int32_t*>(&r14_6) * 8;
        rax118 = reinterpret_cast<int32_t>(optind);
        *reinterpret_cast<uint32_t*>(&r14_6) = *reinterpret_cast<uint32_t*>(&r14_6) + *reinterpret_cast<uint32_t*>(&rbx5);
        rsi119 = r15_3 + rax118 * 8;
        r15_3 = rbp4;
        fun_2670(rdi117, rsi119, reinterpret_cast<uint64_t>(static_cast<int64_t>(static_cast<int32_t>(rbx5 + 1))) << 3, rcx91, r8_16, r9_47, v120);
        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp99) - 8 + 8);
        optind = 0;
        continue;
        addr_322c_124:
        rax122 = quote(v121, rsi115);
        rdi123 = stderr;
        rcx91 = rax122;
        fun_27d0(rdi123, 1, "split -S:  %s\n", rdi123, 1, "split -S:  %s\n");
        rsp99 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp99) - 8 + 8 - 8 + 8);
        zf124 = dev_debug == 0;
        if (!zf124) {
            rdi125 = *reinterpret_cast<void***>(rbp4 + 8);
            rax126 = quote(rdi125, 1, rdi125, 1);
            rdi127 = stderr;
            rcx91 = rax126;
            fun_27d0(rdi127, 1, " into:    %s\n", rdi127, 1, " into:    %s\n");
            rsp99 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp99) - 8 + 8 - 8 + 8);
        }
        if (*reinterpret_cast<uint32_t*>(&r14_6) != 2) {
            *reinterpret_cast<int32_t*>(&rax128) = static_cast<int32_t>(reinterpret_cast<uint64_t>(r14_6 + 0xfffffffffffffffd));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax128) + 4) = 0;
            r13_23 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbp4 + rax128 * 8) + 24);
            do {
                zf129 = dev_debug == 0;
                if (!zf129) {
                    rdi130 = *reinterpret_cast<void***>(r12_113);
                    rax131 = quote(rdi130, 1, rdi130, 1);
                    rdi132 = stderr;
                    rcx91 = rax131;
                    fun_27d0(rdi132, 1, "     &    %s\n", rdi132, 1, "     &    %s\n");
                    rsp99 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp99) - 8 + 8 - 8 + 8);
                }
                r12_113 = r12_113 + 8;
            } while (r13_23 != r12_113);
            goto addr_2ba2_125;
        }
        addr_2e0e_32:
        if (*reinterpret_cast<uint32_t*>(&r12_22) == 0xffffff7e) {
            usage();
            rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
            v15 = 1;
            continue;
        }
        addr_2da9_33:
        rdi133 = stdout;
        r9_47 = reinterpret_cast<int64_t>("David MacKenzie");
        rcx134 = Version;
        r8_16 = reinterpret_cast<void**>("Richard Mlynarik");
        version_etc(rdi133, "env", "GNU coreutils", rcx134, "Richard Mlynarik", "David MacKenzie", "Assaf Gordon", 0);
        fun_27b0();
        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 - 8 - 8 + 8 - 8 + 8);
        goto addr_2df0_25;
    }
    rbx135 = __environ;
    rdx136 = *rbx135;
    if (rdx136) {
        *reinterpret_cast<uint32_t*>(&rbp4) = *reinterpret_cast<uint32_t*>(&rbp4) - (*reinterpret_cast<uint32_t*>(&rbp4) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rbp4) < *reinterpret_cast<uint32_t*>(&rbp4) + reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&v14) < 1))) & 10;
        *reinterpret_cast<int32_t*>(&rbp4 + 4) = 0;
        do {
            rbx135 = rbx135 + 8;
            *reinterpret_cast<uint32_t*>(&rcx137) = *reinterpret_cast<uint32_t*>(&rbp4);
            *reinterpret_cast<int32_t*>(&rcx137 + 4) = 0;
            fun_2700(1, "%s%c", rdx136, rcx137);
            rdx136 = *rbx135;
        } while (rdx136);
    }
    rax138 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
    if (rax138) {
        fun_2560();
    } else {
        return 0;
    }
    addr_3848_66:
    fun_2520();
    fun_2720();
    addr_3871_53:
    fun_2520();
    fun_2480();
    fun_2720();
    addr_38a0_115:
    fun_2520();
    *reinterpret_cast<int32_t*>(&rsi18) = 0;
    *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
    fun_2720();
    addr_38c4_15:
    rdi139 = *reinterpret_cast<void***>(rbp4);
    rax140 = quote(rdi139, rsi18, rdi139, rsi18);
    rax141 = fun_2520();
    fun_2480();
    rcx17 = rax140;
    rdx20 = rax141;
    fun_2720();
    addr_38ff_83:
    quotearg_style(4, 0, rdx20, rcx17, r8_16, r9_47);
    fun_2520();
    fun_2720();
    addr_393d_118:
    fun_2520();
    *reinterpret_cast<int32_t*>(&rsi18) = 0;
    *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
    fun_2720();
    addr_3961_46:
    *reinterpret_cast<void***>(rbp4) = reinterpret_cast<void**>(0);
    rax142 = reinterpret_cast<int32_t>(optind);
    rdi143 = *reinterpret_cast<void***>(r15_3 + rax142 * 8);
    quote(rdi143, rsi18, rdi143, rsi18);
    fun_2520();
    fun_2480();
    fun_2720();
    addr_2acc_113:
    *reinterpret_cast<uint32_t*>(&rdx144) = *reinterpret_cast<unsigned char*>(&edx106);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx144) + 4) = 0;
    goto static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rbp4) + reinterpret_cast<uint64_t>(rdx144 * 4)))) + reinterpret_cast<unsigned char>(rbp4);
    addr_2d73_117:
    *reinterpret_cast<uint32_t*>(&rax145) = *reinterpret_cast<unsigned char*>(&eax107);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax145) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x8cd0 + rax145 * 4) + 0x8cd0;
    addr_299d_35:
    goto *reinterpret_cast<int32_t*>(0x8b50 + reinterpret_cast<unsigned char>(r12_22) * 4) + 0x8b50;
}

int64_t __libc_start_main = 0;

void fun_39b3() {
    __asm__("cli ");
    __libc_start_main(0x2880, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xc008;

void fun_2430(int64_t rdi);

int64_t fun_3a53() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2430(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_3a93() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_25f0(void** rdi, void** rsi, void** rdx, void** rcx);

int32_t fun_2490(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

void fun_3d33(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** rcx6;
    void** r12_7;
    void** rax8;
    void** r12_9;
    void** rax10;
    void** r12_11;
    void** rax12;
    void** r12_13;
    void** rax14;
    void** r12_15;
    void** rax16;
    void** r12_17;
    void** rax18;
    void** r12_19;
    void** rax20;
    void** r12_21;
    void** rax22;
    void** r12_23;
    void** rax24;
    void** r12_25;
    void** rax26;
    void** r12_27;
    void** rax28;
    void** r12_29;
    void** rax30;
    void** r12_31;
    void** rax32;
    void** r12_33;
    void** rax34;
    int32_t eax35;
    void** r13_36;
    void** rax37;
    void** rax38;
    int32_t eax39;
    void** rax40;
    void** rax41;
    void** rax42;
    int32_t eax43;
    void** rax44;
    void** r15_45;
    void** rax46;
    void** rax47;
    void** rax48;
    void** rdi49;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2520();
            fun_2700(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_2520();
            fun_25f0(rax8, r12_7, 5, rcx6);
            r12_9 = stdout;
            rax10 = fun_2520();
            fun_25f0(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_2520();
            fun_25f0(rax12, r12_11, 5, rcx6);
            r12_13 = stdout;
            rax14 = fun_2520();
            fun_25f0(rax14, r12_13, 5, rcx6);
            r12_15 = stdout;
            rax16 = fun_2520();
            fun_25f0(rax16, r12_15, 5, rcx6);
            r12_17 = stdout;
            rax18 = fun_2520();
            fun_25f0(rax18, r12_17, 5, rcx6);
            r12_19 = stdout;
            rax20 = fun_2520();
            fun_25f0(rax20, r12_19, 5, rcx6);
            r12_21 = stdout;
            rax22 = fun_2520();
            fun_25f0(rax22, r12_21, 5, rcx6);
            r12_23 = stdout;
            rax24 = fun_2520();
            fun_25f0(rax24, r12_23, 5, rcx6);
            r12_25 = stdout;
            rax26 = fun_2520();
            fun_25f0(rax26, r12_25, 5, rcx6);
            r12_27 = stdout;
            rax28 = fun_2520();
            fun_25f0(rax28, r12_27, 5, rcx6);
            r12_29 = stdout;
            rax30 = fun_2520();
            fun_25f0(rax30, r12_29, 5, rcx6);
            r12_31 = stdout;
            rax32 = fun_2520();
            fun_25f0(rax32, r12_31, 5, rcx6);
            r12_33 = stdout;
            rax34 = fun_2520();
            fun_25f0(rax34, r12_33, 5, rcx6);
            do {
                if (1) 
                    break;
                eax35 = fun_2620("env", 0, 5, "sha512sum");
            } while (eax35);
            r13_36 = v4;
            if (!r13_36) {
                rax37 = fun_2520();
                fun_2700(1, rax37, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax38 = fun_26f0(5);
                if (!rax38 || (eax39 = fun_2490(rax38, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax39)) {
                    rax40 = fun_2520();
                    r13_36 = reinterpret_cast<void**>("env");
                    fun_2700(1, rax40, "https://www.gnu.org/software/coreutils/", "env");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_36 = reinterpret_cast<void**>("env");
                    goto addr_41d8_9;
                }
            } else {
                rax41 = fun_2520();
                fun_2700(1, rax41, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax42 = fun_26f0(5);
                if (!rax42 || (eax43 = fun_2490(rax42, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax43)) {
                    addr_40de_11:
                    rax44 = fun_2520();
                    fun_2700(1, rax44, "https://www.gnu.org/software/coreutils/", "env");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_36 == "env")) {
                        r12_2 = reinterpret_cast<void**>(0x92f1);
                    }
                } else {
                    addr_41d8_9:
                    r15_45 = stdout;
                    rax46 = fun_2520();
                    fun_25f0(rax46, r15_45, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_40de_11;
                }
            }
            rax47 = fun_2520();
            rcx6 = r12_2;
            fun_2700(1, rax47, r13_36, rcx6);
            addr_3d8e_14:
            fun_27b0();
        }
    } else {
        rax48 = fun_2520();
        rdi49 = stderr;
        rcx6 = r12_2;
        fun_27d0(rdi49, 1, rax48, rdi49, 1, rax48);
        goto addr_3d8e_14;
    }
}

int32_t str2sig(void** rdi, void** rsi);

void** fun_2650(void** rdi, void** rsi, int64_t rdx);

int64_t fun_4553(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9) {
    void** r13_7;
    void** rbp8;
    void* rsp9;
    void** rax10;
    void** v11;
    void** rax12;
    void* rsp13;
    uint32_t ebx14;
    void** r14_15;
    void** r15_16;
    void** rsi17;
    void** rax18;
    uint32_t ebx19;
    void** r12_20;
    void** rsi21;
    int32_t eax22;
    int32_t* rax23;
    void** rax24;
    void** v25;
    int32_t eax26;
    uint32_t r12d27;
    uint32_t v28;
    void** rdi29;
    int32_t eax30;
    int64_t rax31;
    uint32_t v32;
    void* rdx33;
    uint32_t r12d34;

    __asm__("cli ");
    r13_7 = rsi;
    rbp8 = rdi;
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 40);
    rax10 = g28;
    v11 = rax10;
    if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdi) - 48) > 9) {
        rax12 = xstrdup(rdi);
        rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        ebx14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax12));
        r14_15 = rax12;
        r15_16 = rax12;
        if (*reinterpret_cast<signed char*>(&ebx14)) {
            do {
                *reinterpret_cast<int32_t*>(&rsi17) = *reinterpret_cast<signed char*>(&ebx14);
                *reinterpret_cast<int32_t*>(&rsi17 + 4) = 0;
                rax18 = fun_2590("abcdefghijklmnopqrstuvwxyz", rsi17, rdx, rcx);
                rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8);
                if (rax18) {
                    ebx19 = ebx14 - 32;
                    *reinterpret_cast<void***>(r15_16) = *reinterpret_cast<void***>(&ebx19);
                }
                ebx14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_16 + 1));
                ++r15_16;
            } while (*reinterpret_cast<signed char*>(&ebx14));
        }
        r12_20 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp13) + 12);
        rsi21 = r12_20;
        eax22 = str2sig(r14_15, rsi21);
        if (!eax22) 
            goto addr_462f_8;
    } else {
        rax23 = fun_2480();
        rsi21 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 + 16);
        *rax23 = 0;
        rax24 = fun_2650(rbp8, rsi21, 10);
        if (v25 == rbp8) 
            goto addr_45c8_10;
        if (*reinterpret_cast<void***>(v25)) 
            goto addr_45c8_10;
        if (*rax23) 
            goto addr_45c8_10;
        rdx = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax24)));
        if (rdx == rax24) 
            goto addr_46c0_14; else 
            goto addr_45c4_15;
    }
    if (*reinterpret_cast<void***>(r14_15) != 83 || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r14_15 + 1) == 73) || (*reinterpret_cast<signed char*>(r14_15 + 2) != 71 || (rsi21 = r12_20, eax26 = str2sig(r14_15 + 3, rsi21), !!eax26)))) {
        fun_2460(r14_15, rsi21, rdx, rcx, r8, r9);
        goto addr_4645_18;
    }
    addr_462f_8:
    r12d27 = v28;
    fun_2460(r14_15, rsi21, rdx, rcx, r8, r9);
    if (reinterpret_cast<int32_t>(r12d27) < reinterpret_cast<int32_t>(0)) 
        goto addr_4645_18;
    addr_46e6_20:
    rsi21 = r13_7;
    *reinterpret_cast<uint32_t*>(&rdi29) = r12d27;
    *reinterpret_cast<int32_t*>(&rdi29 + 4) = 0;
    eax30 = sig2str(rdi29, rsi21, rdx, rcx, r8, r9);
    if (eax30) {
        addr_4645_18:
        quote(rbp8, rsi21);
        fun_2520();
        fun_2720();
        *reinterpret_cast<uint32_t*>(&rax31) = 0xffffffff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax31) + 4) = 0;
    } else {
        *reinterpret_cast<uint32_t*>(&rax31) = v32;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax31) + 4) = 0;
    }
    rdx33 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
    if (rdx33) {
        fun_2560();
    } else {
        return rax31;
    }
    addr_46c0_14:
    if (*reinterpret_cast<uint32_t*>(&rdx) == 0xffffffff) {
        addr_45c8_10:
        goto addr_4645_18;
    } else {
        r12d34 = 0xff;
        if (*reinterpret_cast<int32_t*>(&rdx) <= reinterpret_cast<int32_t>(0xfe)) {
            r12d34 = 0x7f;
        }
        r12d27 = r12d34 & *reinterpret_cast<uint32_t*>(&rdx);
        v32 = r12d27;
        goto addr_46e6_20;
    }
    addr_45c4_15:
    goto addr_45c8_10;
}

int64_t file_name = 0;

void fun_4733(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_4743(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

void** fun_24a0(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_4753() {
    void** rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2480(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2520();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_47e3_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2720();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_24a0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_47e3_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2720();
    }
}

struct s3 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s3* fun_25a0();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_4803(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s3* rax4;
    void** r12_5;
    void** rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_27c0("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2470("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_25a0();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_2490(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<signed char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_5fa3(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2480();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xc540;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_5fe3(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xc540);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_6003(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xc540);
    }
    *rdi = esi;
    return 0xc540;
}

int64_t fun_6023(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xc540);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s4 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_6063(struct s4* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s4*>(0xc540);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s5 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s5* fun_6083(struct s5* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s5*>(0xc540);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x284a;
    if (!rdx) 
        goto 0x284a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xc540;
}

struct s6 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_60c3(void** rdi, void** rsi, void** rdx, void** rcx, struct s6* r8) {
    struct s6* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s6*>(0xc540);
    }
    rax7 = fun_2480();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x60f6);
    *rax7 = r15d8;
    return rax13;
}

struct s7 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_6143(void** rdi, void** rsi, void*** rdx, struct s7* rcx) {
    struct s7* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s7*>(0xc540);
    }
    rax6 = fun_2480();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x6171);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x61cc);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_6233() {
    __asm__("cli ");
}

void** gc078 = reinterpret_cast<void**>(64);

int64_t slotvec0 = 0x100;

void fun_6243() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rsi7;
    void** rdx8;
    void** rcx9;
    void** r8_10;
    int64_t r9_11;
    void** rdi12;
    void** rsi13;
    void** rdx14;
    void** rcx15;
    void** r8_16;
    int64_t r9_17;
    void** rsi18;
    void** rdx19;
    void** rcx20;
    void** r8_21;
    int64_t r9_22;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2460(rdi6, rsi7, rdx8, rcx9, r8_10, r9_11);
        } while (rbx4 != rbp5);
    }
    rdi12 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi12 != 0xc440) {
        fun_2460(rdi12, rsi13, rdx14, rcx15, r8_16, r9_17);
        gc078 = reinterpret_cast<void**>(0xc440);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xc070) {
        fun_2460(r12_2, rsi18, rdx19, rcx20, r8_21, r9_22);
        slotvec = reinterpret_cast<void**>(0xc070);
    }
    nslots = 1;
    return;
}

void fun_62e3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6303() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6313(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6333(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_6353(void** rdi, int32_t esi, void** rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2850;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2560();
    } else {
        return rax6;
    }
}

void** fun_63e3(void** rdi, int32_t esi, void** rdx, void** rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2855;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2560();
    } else {
        return rax7;
    }
}

void** fun_6473(int32_t edi, void** rsi) {
    void** rax3;
    struct s0* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x285a;
    rcx4 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, 0xffffffffffffffff, rcx4, 0, rsi, 0xffffffffffffffff, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2560();
    } else {
        return rax5;
    }
}

void** fun_6503(int32_t edi, void** rsi, void** rdx) {
    void** rax4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x285f;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2560();
    } else {
        return rax6;
    }
}

void** fun_6593(void** rdi, void** rsi, uint32_t edx) {
    struct s0* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x5fa0]");
    __asm__("movdqa xmm1, [rip+0x5fa8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x5f91]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2560();
    } else {
        return rax10;
    }
}

void** fun_6633(void** rdi, uint32_t esi) {
    struct s0* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x5f00]");
    __asm__("movdqa xmm1, [rip+0x5f08]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x5ef1]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, 0xffffffffffffffff, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2560();
    } else {
        return rax9;
    }
}

void** fun_66d3(void** rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5e60]");
    __asm__("movdqa xmm1, [rip+0x5e68]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x5e49]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, 0xffffffffffffffff, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2560();
    } else {
        return rax3;
    }
}

void** fun_6763(void** rdi, void** rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5dd0]");
    __asm__("movdqa xmm1, [rip+0x5dd8]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x5dc6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2560();
    } else {
        return rax4;
    }
}

void** fun_67f3(void** rdi, int32_t esi, void** rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2864;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2560();
    } else {
        return rax6;
    }
}

void** fun_6893(void** rdi, int64_t rsi, int64_t rdx, void** rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5c9a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x5c92]");
    __asm__("movdqa xmm2, [rip+0x5c9a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2869;
    if (!rdx) 
        goto 0x2869;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, 0xffffffffffffffff, rcx6, rdi, rcx, 0xffffffffffffffff, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2560();
    } else {
        return rax7;
    }
}

void** fun_6933(int32_t edi, int64_t rsi, int64_t rdx, void** rcx, void** r8) {
    void** rcx6;
    struct s0* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5bfa]");
    __asm__("movdqa xmm1, [rip+0x5c02]");
    __asm__("movdqa xmm2, [rip+0x5c0a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x286e;
    if (!rdx) 
        goto 0x286e;
    rcx7 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2560();
    } else {
        return rax9;
    }
}

void** fun_69e3(int64_t rdi, int64_t rsi, void** rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5b4a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x5b42]");
    __asm__("movdqa xmm2, [rip+0x5b4a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2873;
    if (!rsi) 
        goto 0x2873;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, 0xffffffffffffffff, rcx5, 0, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2560();
    } else {
        return rax6;
    }
}

void** fun_6a83(int64_t rdi, int64_t rsi, void** rdx, void** rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5aaa]");
    __asm__("movdqa xmm1, [rip+0x5ab2]");
    __asm__("movdqa xmm2, [rip+0x5aba]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2878;
    if (!rsi) 
        goto 0x2878;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2560();
    } else {
        return rax7;
    }
}

void fun_6b23() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6b33(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6b53() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6b73(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

int32_t fun_2790(void** rdi, void** rsi);

int32_t fun_27e0(void** rdi, void** rsi);

int64_t fun_6b93(void** rdi, int32_t* rsi, void** rdx, void** rcx) {
    int32_t* r13_5;
    void** r12_6;
    void** rbp7;
    int64_t rbx8;
    void** rsp9;
    void** rax10;
    void** v11;
    void** rdi12;
    int32_t eax13;
    void** rax14;
    void** v15;
    int32_t r8d16;
    void* rax17;
    int64_t rax18;
    int32_t eax19;
    int64_t rbp20;
    int32_t eax21;
    int32_t eax22;
    int32_t eax23;
    void** rax24;
    void** v25;
    void** rax26;
    void** v27;

    __asm__("cli ");
    r13_5 = rsi;
    r12_6 = rdi;
    rbp7 = reinterpret_cast<void**>("HUP");
    *reinterpret_cast<int32_t*>(&rbx8) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx8) + 4) = 0;
    rsp9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24);
    rax10 = g28;
    v11 = rax10;
    if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdi) - 48) > 9) {
        do {
            rdi12 = rbp7;
            eax13 = fun_2620(rdi12, r12_6, rdx, rcx);
            rsp9 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp9 - 8) + 8);
            if (!eax13) 
                break;
            *reinterpret_cast<int32_t*>(&rbx8) = *reinterpret_cast<int32_t*>(&rbx8) + 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx8) + 4) = 0;
            rbp7 = rbp7 + 12;
        } while (*reinterpret_cast<int32_t*>(&rbx8) != 35);
        goto addr_6c60_4;
    } else {
        rax14 = fun_2650(rdi, rsp9, 10);
        if (*reinterpret_cast<void***>(v15)) 
            goto addr_6c4c_7;
        if (reinterpret_cast<signed char>(rax14) <= reinterpret_cast<signed char>(64)) 
            goto addr_6bfd_9; else 
            goto addr_6c4c_7;
    }
    *reinterpret_cast<int32_t*>(&rax14) = *reinterpret_cast<int32_t*>(0xc080 + (rbx8 + rbx8 * 2) * 4);
    addr_6bfd_9:
    r8d16 = *reinterpret_cast<int32_t*>(&rax14) >> 31;
    addr_6c04_11:
    *r13_5 = *reinterpret_cast<int32_t*>(&rax14);
    rax17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
    if (rax17) {
        fun_2560();
    } else {
        *reinterpret_cast<int32_t*>(&rax18) = r8d16;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
        return rax18;
    }
    addr_6c60_4:
    eax19 = fun_2790(rdi12, r12_6);
    *reinterpret_cast<int32_t*>(&rbp20) = eax19;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp20) + 4) = 0;
    eax21 = fun_27e0(rdi12, r12_6);
    rsp9 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp9 - 8) + 8 - 8 + 8);
    if (reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbp20) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbp20) == 0) || (eax22 = fun_2490(r12_6, "RTMIN", 5, rcx), rsp9 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp9 - 8) + 8), !!eax22)) {
        if (reinterpret_cast<uint1_t>(eax21 < 0) | reinterpret_cast<uint1_t>(eax21 == 0) || ((eax23 = fun_2490(r12_6, "RTMAX", 5, rcx), rsp9 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp9 - 8) + 8), !!eax23) || ((rax24 = fun_2650(r12_6 + 5, rsp9, 10), !!*reinterpret_cast<void***>(v25)) || (reinterpret_cast<signed char>(rax24) < reinterpret_cast<signed char>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbp20) - eax21)) || !reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rax24) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rax24 == 0)))))) {
            addr_6c4c_7:
            *reinterpret_cast<int32_t*>(&rax14) = -1;
            r8d16 = -1;
            goto addr_6c04_11;
        } else {
            *reinterpret_cast<int32_t*>(&rax14) = *reinterpret_cast<int32_t*>(&rax24) + eax21;
            goto addr_6bfd_9;
        }
    } else {
        rax26 = fun_2650(r12_6 + 5, rsp9, 10);
        if (*reinterpret_cast<void***>(v27)) 
            goto addr_6c4c_7;
        if (reinterpret_cast<signed char>(rax26) < reinterpret_cast<signed char>(0)) 
            goto addr_6c4c_7;
        if (reinterpret_cast<signed char>(rax26) > reinterpret_cast<signed char>(static_cast<int64_t>(eax21 - *reinterpret_cast<int32_t*>(&rbp20)))) 
            goto addr_6c4c_7;
        *reinterpret_cast<int32_t*>(&rax14) = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbp20 + reinterpret_cast<unsigned char>(rax26)));
        goto addr_6bfd_9;
    }
}

void fun_24b0(void** rdi, int64_t rsi);

void fun_2830(uint64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8);

int64_t fun_6d33(void** rdi, void** rsi) {
    int64_t rdx3;
    int32_t* rcx4;
    void** rbp5;
    int32_t ebx6;
    int64_t rax7;
    int32_t eax8;
    int32_t r12d9;
    int32_t eax10;
    int64_t r8_11;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rdx3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    rcx4 = reinterpret_cast<int32_t*>(0xc080);
    rbp5 = rsi;
    ebx6 = *reinterpret_cast<int32_t*>(&rdi);
    do {
        if (*rcx4 == ebx6) 
            break;
        *reinterpret_cast<int32_t*>(&rdx3) = *reinterpret_cast<int32_t*>(&rdx3) + 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
        rcx4 = rcx4 + 3;
    } while (*reinterpret_cast<int32_t*>(&rdx3) != 35);
    goto addr_6d80_4;
    fun_24b0(rbp5, 0xc080 + (rdx3 + rdx3 * 2) * 4 + 4);
    *reinterpret_cast<int32_t*>(&rax7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    addr_6d73_6:
    return rax7;
    addr_6d80_4:
    eax8 = fun_2790(rdi, rsi);
    r12d9 = eax8;
    eax10 = fun_27e0(rdi, rsi);
    if (r12d9 > ebx6 || eax10 < ebx6) {
        *reinterpret_cast<int32_t*>(&rax7) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        goto addr_6d73_6;
    } else {
        if ((eax10 - r12d9 >> 1) + r12d9 >= ebx6) {
            *reinterpret_cast<void***>(rbp5) = reinterpret_cast<void**>(0x494d5452);
            *reinterpret_cast<int16_t*>(rbp5 + 4) = 78;
        } else {
            *reinterpret_cast<void***>(rbp5) = reinterpret_cast<void**>(0x414d5452);
            r12d9 = eax10;
            *reinterpret_cast<int16_t*>(rbp5 + 4) = 88;
        }
        *reinterpret_cast<int32_t*>(&rax7) = ebx6 - r12d9;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rax7)) {
            *reinterpret_cast<int32_t*>(&r8_11) = *reinterpret_cast<int32_t*>(&rax7);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_11) + 4) = 0;
            fun_2830(rbp5 + 5, 1, -1, "%+d", r8_11);
            return 0;
        }
    }
}

void fun_2630(int64_t rdi, void** rsi, void** rdx, void** rcx, int64_t r8, uint64_t r9);

void fun_6e13(void** rdi, int64_t rsi, int64_t rdx, uint64_t rcx, int64_t r8, uint64_t r9) {
    uint64_t r12_7;
    void** rax8;
    void** rax9;
    void** rax10;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_27d0(rdi, 1, "%s %s\n", rdi, 1, "%s %s\n");
    } else {
        r9 = rcx;
        fun_27d0(rdi, 1, "%s (%s) %s\n", rdi, 1, "%s (%s) %s\n");
    }
    rax8 = fun_2520();
    fun_27d0(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rdi, 1, "Copyright %s %d Free Software Foundation, Inc.");
    fun_2630(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax8, 0x7e6, r9);
    rax9 = fun_2520();
    fun_27d0(rdi, 1, rax9, rdi, 1, rax9);
    fun_2630(10, rdi, rax9, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (r12_7 > 9) {
        rax10 = fun_2520();
        fun_27d0(rdi, 1, rax10, rdi, 1, rax10);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x9598 + r12_7 * 4) + 0x9598;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_7283() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s8 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_72a3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s8* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s8* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2560();
    } else {
        return;
    }
}

void fun_7343(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_73e6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_73f0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2560();
    } else {
        return;
    }
    addr_73e6_5:
    goto addr_73f0_7;
}

void fun_7423() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    int64_t r8_4;
    uint64_t r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2630(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2520();
    fun_2700(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2520();
    fun_2700(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2520();
    goto fun_2700;
}

int64_t fun_24e0();

void xalloc_die();

void fun_74c3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_24e0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_2690(void** rdi);

void fun_7503(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2690(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7523(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2690(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7543(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2690(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_26e0();

void fun_7563(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_26e0();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7593() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_26e0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_75c3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_24e0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7603() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_24e0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7643(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_24e0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7673(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_24e0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_76c3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_24e0();
            if (rax5) 
                break;
            addr_770d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_770d_5;
        rax8 = fun_24e0();
        if (rax8) 
            goto addr_76f6_9;
        if (rbx4) 
            goto addr_770d_5;
        addr_76f6_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_7753(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_24e0();
            if (rax8) 
                break;
            addr_779a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_779a_5;
        rax11 = fun_24e0();
        if (rax11) 
            goto addr_7782_9;
        if (!rbx6) 
            goto addr_7782_9;
        if (r12_4) 
            goto addr_779a_5;
        addr_7782_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_77e3(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_788d_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_78a0_10:
                *r12_8 = 0;
            }
            addr_7840_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_7866_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_78b4_14;
            if (rcx10 <= rsi9) 
                goto addr_785d_16;
            if (rsi9 >= 0) 
                goto addr_78b4_14;
            addr_785d_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_78b4_14;
            addr_7866_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_26e0();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_78b4_14;
            if (!rbp13) 
                break;
            addr_78b4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_788d_9;
        } else {
            if (!r13_6) 
                goto addr_78a0_10;
            goto addr_7840_11;
        }
    }
}

int64_t fun_2600();

void fun_78e3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2600();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7913() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2600();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7943() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2600();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7963() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2600();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7983(int64_t rdi, void** rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2690(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2670;
    }
}

void fun_79c3(int64_t rdi, void** rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2690(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2670;
    }
}

struct s9 {
    signed char[1] pad1;
    void** f1;
};

void fun_7a03(int64_t rdi, struct s9* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2690(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_2670;
    }
}

void** fun_2540(void** rdi, ...);

void fun_7a43(void** rdi) {
    void** rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_2540(rdi);
    rax3 = fun_2690(rax2 + 1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2670;
    }
}

void fun_7a83() {
    void** rdi1;

    __asm__("cli ");
    fun_2520();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_2720();
    fun_2470(rdi1);
}

int64_t fun_24c0();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_7ac3(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    int32_t* rax5;
    int32_t* rax6;

    __asm__("cli ");
    rax2 = fun_24c0();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_7b1e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2480();
            *rax5 = 0;
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_7b1e_3;
            rax6 = fun_2480();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax6 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s10 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_2680(struct s10* rdi);

int32_t fun_26d0(struct s10* rdi);

int64_t fun_25b0(int64_t rdi, ...);

int32_t rpl_fflush(struct s10* rdi);

int64_t fun_2500(struct s10* rdi);

int64_t fun_7b33(struct s10* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2680(rdi);
    if (eax2 >= 0) {
        eax3 = fun_26d0(rdi);
        if (!(eax3 && (eax4 = fun_2680(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_25b0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2480();
            r12d9 = *rax8;
            rax10 = fun_2500(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2500;
}

void rpl_fseeko(struct s10* rdi);

void fun_7bc3(struct s10* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_26d0(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_7c13(struct s10* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2680(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_25b0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_26b0(int64_t rdi);

signed char* fun_7c93() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_26b0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2580(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_7cd3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2580(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2560();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_7d63() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2560();
    } else {
        return rax3;
    }
}

int64_t fun_7de3(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9) {
    void** rax7;
    int32_t r13d8;
    void** rax9;
    int64_t v10;
    int64_t v11;
    int64_t rax12;

    __asm__("cli ");
    rax7 = fun_26f0(rdi);
    if (!rax7) {
        r13d8 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax9 = fun_2540(rax7);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax9)) {
            fun_2670(rsi, rax7, rax9 + 1, rcx, r8, r9, v10);
            return 0;
        } else {
            r13d8 = 34;
            if (rdx) {
                fun_2670(rsi, rax7, rdx + 0xffffffffffffffff, rcx, r8, r9, v11);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax12) = r13d8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
    return rax12;
}

void fun_7e93() {
    __asm__("cli ");
    goto fun_26f0;
}

void fun_7ea3() {
    __asm__("cli ");
}

void fun_7eb7() {
    __asm__("cli ");
    return;
}

void fun_29ae() {
    report_signal_handling = 1;
    goto 0x2950;
}

void** optarg = reinterpret_cast<void**>(0);

struct s11 {
    signed char[8] pad8;
    int64_t f8;
};

struct s11* xnmalloc(int64_t rdi, int64_t rsi);

void fun_2a43() {
    int64_t r14_1;
    int32_t r14d2;
    void** rbp3;
    struct s11* rax4;
    uint32_t eax5;

    *reinterpret_cast<uint32_t*>(&r14_1) = r14d2 - optind;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_1) + 4) = 0;
    rbp3 = optarg;
    rax4 = xnmalloc(static_cast<int64_t>(static_cast<int32_t>(r14_1 + 2)), 16);
    rax4->f8 = 0;
    eax5 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp3));
    if (!*reinterpret_cast<signed char*>(&eax5)) 
        goto 0x2b35;
}

int64_t fun_25d0(int64_t rdi, int64_t rsi);

void fun_2ae0() {
    int32_t ecx1;
    int32_t r12d2;
    unsigned char bl3;
    int64_t r14_4;

    ecx1 = r12d2;
    if (!(*reinterpret_cast<unsigned char*>(&ecx1) | bl3)) {
        fun_25d0(r14_4, 0x8117);
        goto 0x2b17;
    }
}

struct s12 {
    signed char[1] pad1;
    signed char f1;
};

struct s13 {
    signed char[2] pad2;
    unsigned char f2;
};

struct s14 {
    signed char[3] pad3;
    unsigned char f3;
};

void** vnlen = reinterpret_cast<void**>(0);

void** varname = reinterpret_cast<void**>(0);

void** xrealloc(void** rdi);

struct s15 {
    signed char[2] pad2;
    void** f2;
};

void** fun_2440(void** rdi, void** rsi, void** rdx);

void fun_2c40() {
    void* rsp1;
    int64_t v2;
    signed char r12b3;
    struct s12* r14_4;
    signed char v5;
    int64_t rax6;
    struct s13* r14_7;
    int32_t edx8;
    unsigned char* rdx9;
    struct s14* r14_10;
    int64_t rax11;
    int32_t ecx12;
    uint32_t eax13;
    void* rdx14;
    uint64_t r14_15;
    void** r15_16;
    int1_t less17;
    void** rdi18;
    void** rax19;
    void** rcx20;
    void** rsi21;
    struct s15* r14_22;
    void** rdx23;
    void** r8_24;
    int64_t r9_25;
    void** rax26;
    void** rax27;
    void* rsp28;
    void** rcx29;
    void** r13_30;
    signed char v31;
    void** rdi32;
    int1_t zf33;
    void** rdi34;
    uint32_t eax35;
    void** r15_36;
    uint32_t esi37;
    int1_t zf38;
    void** rdi39;
    void** r14_40;

    rsp1 = __zero_stack_offset();
    v2 = reinterpret_cast<int64_t>(__return_address());
    if (r12b3) 
        goto 0x2af4;
    if (r14_4->f1 != 0x7b) {
        addr_2c8e_3:
        fun_2520();
        fun_2720();
        if (v5) {
            goto 0x2b35;
        } else {
            goto 0x2b08;
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rax6) = r14_7->f2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rax6) > 90) {
            edx8 = static_cast<int32_t>(rax6 - 97);
            if (*reinterpret_cast<unsigned char*>(&edx8) <= 25 || *reinterpret_cast<signed char*>(&rax6) == 95) {
                addr_2c67_8:
                rdx9 = &r14_10->f3;
            } else {
                goto addr_2c8e_3;
            }
        } else {
            if (*reinterpret_cast<signed char*>(&rax6) <= 64) 
                goto addr_2c8e_3; else 
                goto addr_2c67_8;
        }
    }
    while (1) {
        *reinterpret_cast<uint32_t*>(&rax11) = *rdx9;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rax11) > 90) {
            ecx12 = static_cast<int32_t>(rax11 - 97);
            if (*reinterpret_cast<unsigned char*>(&ecx12) <= 25) 
                goto addr_2ec0_13;
            if (*reinterpret_cast<signed char*>(&rax11) != 95) 
                break;
        } else {
            if (*reinterpret_cast<signed char*>(&rax11) > 64) 
                goto addr_2ec0_13;
            eax13 = *reinterpret_cast<uint32_t*>(&rax11) - 48;
            if (*reinterpret_cast<unsigned char*>(&eax13) > 9) 
                goto addr_2c8e_3;
        }
        addr_2ec0_13:
        ++rdx9;
    }
    if (*reinterpret_cast<signed char*>(&rax11) == 0x7d) {
        rdx14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdx9) - r14_15);
        r15_16 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdx14) + 0xfffffffffffffffe);
        less17 = reinterpret_cast<signed char>(r15_16) < reinterpret_cast<signed char>(vnlen);
        if (!less17) {
            rdi18 = varname;
            vnlen = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdx14) + 0xffffffffffffffff);
            rax19 = xrealloc(rdi18);
            rsp1 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp1) - 8 + 8);
            varname = rax19;
        }
        rcx20 = varname;
        rsi21 = reinterpret_cast<void**>(&r14_22->f2);
        rdx23 = r15_16;
        rax26 = fun_2670(rcx20, rsi21, rdx23, rcx20, r8_24, r9_25, v2);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax26) + reinterpret_cast<uint64_t>(rdx14) + 0xfffffffffffffffe) = 0;
        rax27 = fun_2440(rax26, rsi21, rdx23);
        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp1) - 8 + 8 - 8 + 8);
        rcx29 = rax26;
        r13_30 = rax27;
        if (rax27) {
            if (v31) {
                rdi32 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp28) + 64);
                check_start_new_arg_part_0(rdi32, rsi21, rdi32, rsi21);
                rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
                rcx29 = rcx29;
            }
            zf33 = dev_debug == 0;
            if (!zf33) {
                quote(r13_30, rsi21, r13_30, rsi21);
                rcx29 = rcx29;
                rdi34 = stderr;
                rdx23 = reinterpret_cast<void**>("expanding ${%s} into %s\n");
                fun_27d0(rdi34, 1, "expanding ${%s} into %s\n", rdi34, 1, "expanding ${%s} into %s\n");
                rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8 - 8 + 8);
            }
            eax35 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_30));
            r15_36 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp28) + 64);
            if (*reinterpret_cast<signed char*>(&eax35)) {
                do {
                    esi37 = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(&eax35)));
                    ++r13_30;
                    splitbuf_append_byte(r15_36, esi37, r15_36, esi37);
                    eax35 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_30));
                } while (*reinterpret_cast<signed char*>(&eax35));
            }
        } else {
            zf38 = dev_debug == 0;
            if (!zf38) {
                rdi39 = stderr;
                rdx23 = reinterpret_cast<void**>("replacing ${%s} with null string\n");
                fun_27d0(rdi39, 1, "replacing ${%s} with null string\n");
            }
        }
        fun_2590(r14_40, 0x7d, rdx23, rcx29, r14_40, 0x7d, rdx23, rcx29);
        goto 0x2b17;
    }
}

void fun_2cd8() {
    signed char bl1;
    signed char v2;
    void** rsi3;

    if (bl1) 
        goto 0x2af4;
    if (!v2) 
        goto 0x2b13;
    check_start_new_arg_part_0(reinterpret_cast<int64_t>(__zero_stack_offset()) + 64, rsi3);
    goto 0x2b13;
}

void fun_2d18() {
    goto 0x2950;
}

void fun_2e2c() {
    goto 0x2af4;
}

void fun_2e7d() {
    goto 0x2af4;
}

void fun_2ec9() {
    goto 0x2af4;
}

uint32_t fun_25e0(void** rdi, void** rsi, void** rdx, void** rcx);

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_2800(int64_t rdi, void** rsi);

uint32_t fun_27f0(void** rdi, void** rsi);

void** fun_2820(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_4a35() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2520();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2520();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2540(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_4d33_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_4d33_22; else 
                            goto addr_512d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_51ed_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_5540_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_4d30_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_4d30_30; else 
                                goto addr_5559_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2540(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_5540_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_25e0(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_5540_28; else 
                            goto addr_4bdc_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_56a0_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_5520_40:
                        if (r11_27 == 1) {
                            addr_50ad_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_5668_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_4ce7_44;
                            }
                        } else {
                            goto addr_5530_46;
                        }
                    } else {
                        addr_56af_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_50ad_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_4d33_22:
                                if (v47 != 1) {
                                    addr_5289_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2540(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_52d4_54;
                                    }
                                } else {
                                    goto addr_4d40_56;
                                }
                            } else {
                                addr_4ce5_57:
                                ebp36 = 0;
                                goto addr_4ce7_44;
                            }
                        } else {
                            addr_5514_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_56af_47; else 
                                goto addr_551e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_50ad_41;
                        if (v47 == 1) 
                            goto addr_4d40_56; else 
                            goto addr_5289_52;
                    }
                }
                addr_4da1_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_4c38_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_4c5d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_4f60_65;
                    } else {
                        addr_4dc9_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_5618_67;
                    }
                } else {
                    goto addr_4dc0_69;
                }
                addr_4c71_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_4cbc_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_5618_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_4cbc_81;
                }
                addr_4dc0_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_4c5d_64; else 
                    goto addr_4dc9_66;
                addr_4ce7_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_4d9f_91;
                if (v22) 
                    goto addr_4cff_93;
                addr_4d9f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4da1_62;
                addr_52d4_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_5a5b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_5acb_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_58cf_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2800(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_27f0(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_53ce_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_4d8c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_53d8_112;
                    }
                } else {
                    addr_53d8_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_54a9_114;
                }
                addr_4d98_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_4d9f_91;
                while (1) {
                    addr_54a9_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_59b7_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_5416_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_59c5_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_5497_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_5497_128;
                        }
                    }
                    addr_5445_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_5497_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_5416_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_5445_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4cbc_81;
                addr_59c5_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_5618_67;
                addr_5a5b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_53ce_109;
                addr_5acb_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_53ce_109;
                addr_4d40_56:
                rax93 = fun_2820(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_4d8c_110;
                addr_551e_59:
                goto addr_5520_40;
                addr_51ed_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_4d33_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_4d98_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4ce5_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_4d33_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_5232_160;
                if (!v22) 
                    goto addr_5607_162; else 
                    goto addr_5813_163;
                addr_5232_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_5607_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_5618_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_50db_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_4f43_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_4c71_70; else 
                    goto addr_4f57_169;
                addr_50db_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_4c38_63;
                goto addr_4dc0_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_5514_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_564f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4d30_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4c28_178; else 
                        goto addr_55d2_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5514_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4d33_22;
                }
                addr_564f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_4d30_30:
                    r8d42 = 0;
                    goto addr_4d33_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_4da1_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_5668_42;
                    }
                }
                addr_4c28_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4c38_63;
                addr_55d2_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_5530_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_4da1_62;
                } else {
                    addr_55e2_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_4d33_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_5d92_188;
                if (v28) 
                    goto addr_5607_162;
                addr_5d92_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_4f43_168;
                addr_4bdc_37:
                if (v22) 
                    goto addr_5bd3_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_4bf3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_56a0_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_572b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4d33_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4c28_178; else 
                        goto addr_5707_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5514_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4d33_22;
                }
                addr_572b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_4d33_22;
                }
                addr_5707_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_5530_46;
                goto addr_55e2_186;
                addr_4bf3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_4d33_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_4d33_22; else 
                    goto addr_4c04_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_5cde_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_5b64_210;
            if (1) 
                goto addr_5b62_212;
            if (!v29) 
                goto addr_579e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2530();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_5cd1_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_4f60_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_4d1b_219; else 
            goto addr_4f7a_220;
        addr_4cff_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_4d13_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_4f7a_220; else 
            goto addr_4d1b_219;
        addr_58cf_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_4f7a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2530();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_58ed_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2530();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_5d60_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_57c6_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_59b7_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4d13_221;
        addr_5813_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4d13_221;
        addr_4f57_169:
        goto addr_4f60_65;
        addr_5cde_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_4f7a_220;
        goto addr_58ed_222;
        addr_5b64_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_5bbe_236;
        fun_2560();
        rsp25 = rsp25 - 8 + 8;
        goto addr_5d60_225;
        addr_5b62_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_5b64_210;
        addr_579e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_5b64_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_57c6_226;
        }
        addr_5cd1_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_512d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x904c + rax113 * 4) + 0x904c;
    addr_5559_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x914c + rax114 * 4) + 0x914c;
    addr_5bd3_190:
    addr_4d1b_219:
    goto 0x4a00;
    addr_4c04_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x8f4c + rax115 * 4) + 0x8f4c;
    addr_5bbe_236:
    goto v116;
}

void fun_4c20() {
}

void fun_4dd8() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x4ad2;
}

void fun_4e31() {
    goto 0x4ad2;
}

void fun_4f1e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x4da1;
    }
    if (v2) 
        goto 0x5813;
    if (!r10_3) 
        goto addr_597e_5;
    if (!v4) 
        goto addr_584e_7;
    addr_597e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_584e_7:
    goto 0x4c54;
}

void fun_4f3c() {
}

void fun_4fe7() {
    signed char v1;

    if (v1) {
        goto 0x4f6f;
    } else {
        goto 0x4caa;
    }
}

void fun_5001() {
    signed char v1;

    if (!v1) 
        goto 0x4ffa; else 
        goto "???";
}

void fun_5028() {
    goto 0x4f43;
}

void fun_50a8() {
}

void fun_50c0() {
}

void fun_50ef() {
    goto 0x4f43;
}

void fun_5141() {
    goto 0x50d0;
}

void fun_5170() {
    goto 0x50d0;
}

void fun_51a3() {
    goto 0x50d0;
}

void fun_5570() {
    goto 0x4c28;
}

void fun_586e() {
    signed char v1;

    if (v1) 
        goto 0x5813;
    goto 0x4c54;
}

void fun_5915() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x4c54;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x4c38;
        goto 0x4c54;
    }
}

void fun_5d32() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x4fa0;
    } else {
        goto 0x4ad2;
    }
}

void fun_6ee8() {
    fun_2520();
}

void fun_29b7() {
    void** rdi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    int64_t r9_5;

    rdi1 = optarg;
    parse_block_signal_params(rdi1, 1, rdx2, rcx3, r8_4, r9_5, __return_address());
    goto 0x2950;
}

void fun_2e3a() {
    signed char bl1;
    signed char bl2;

    if (!bl1) 
        goto 0x2b35;
    fun_2520();
    fun_2720();
    if (!bl2) {
        goto 0x2b17;
    } else {
        goto 0x2af4;
    }
}

void fun_2e8b() {
    goto 0x2af4;
}

void fun_4e5e() {
    goto 0x4ad2;
}

void fun_5034() {
    goto 0x4fec;
}

void fun_50fb() {
    goto 0x4c28;
}

void fun_514d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x50d0;
    goto 0x4cff;
}

void fun_517f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x50db;
        goto 0x4b00;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x4f7a;
        goto 0x4d1b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x5918;
    if (r10_8 > r15_9) 
        goto addr_5065_9;
    addr_506a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x5923;
    goto 0x4c54;
    addr_5065_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_506a_10;
}

void fun_51b2() {
    goto 0x4ce7;
}

void fun_5580() {
    goto 0x4ce7;
}

void fun_5d1f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x4e3c;
    } else {
        goto 0x4fa0;
    }
}

void fun_6fa0() {
}

void fun_29ca() {
    void** rdi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    int64_t r9_5;

    rdi1 = optarg;
    parse_signal_action_params(rdi1, 0, rdx2, rcx3, r8_4, r9_5, __return_address());
    goto 0x2950;
}

void fun_2c08() {
    signed char r12b1;
    signed char v2;

    if (r12b1) 
        goto 0x2af4;
    if (!v2) 
        goto 0x2b13; else 
        goto "???";
}

void fun_2e99() {
    goto 0x2af4;
}

void fun_51bc() {
    goto 0x5157;
}

void fun_558a() {
    goto 0x50ad;
}

void fun_7000() {
    fun_2520();
    goto fun_27d0;
}

void fun_29dd() {
    void** rdi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    int64_t r9_5;
    void** rdi6;
    void** rdx7;
    void** rcx8;
    void** r8_9;
    int64_t r9_10;

    rdi1 = optarg;
    parse_signal_action_params(rdi1, 1, rdx2, rcx3, r8_4, r9_5, __return_address());
    rdi6 = optarg;
    parse_block_signal_params(rdi6, 0, rdx7, rcx8, r8_9, r9_10, __return_address());
    goto 0x2950;
}

void fun_4e8d() {
    goto 0x4ad2;
}

void fun_51c8() {
    goto 0x5157;
}

void fun_5597() {
    goto 0x50fe;
}

void fun_7040() {
    fun_2520();
    goto fun_27d0;
}

void fun_2a01() {
    dev_debug = 1;
    goto 0x2950;
}

void fun_4eba() {
    goto 0x4ad2;
}

void fun_51d4() {
    goto 0x50d0;
}

void fun_7080() {
    fun_2520();
    goto fun_27d0;
}

int64_t usvars_alloc = 0;

void** x2nrealloc(void** rdi, int64_t rsi, int64_t rdx);

void fun_2a0d() {
    int64_t rax1;
    int1_t zf2;
    void** rbp3;
    void** rdi4;
    void** rax5;
    void** rdx6;

    rax1 = usvars_used;
    zf2 = rax1 == usvars_alloc;
    rbp3 = optarg;
    if (zf2) {
        rdi4 = usvars;
        rax5 = x2nrealloc(rdi4, 0xc408, 8);
        usvars = rax5;
        rax1 = usvars_used;
    }
    usvars_used = rax1 + 1;
    rdx6 = usvars;
    *reinterpret_cast<void***>(rdx6 + rax1 * 8) = rbp3;
    goto 0x2950;
}

void fun_4edc() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x5870;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x4da1;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x4da1;
    }
    if (v11) 
        goto 0x5bd3;
    if (r10_12 > r15_13) 
        goto addr_5c23_8;
    addr_5c28_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x5961;
    addr_5c23_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_5c28_9;
}

void fun_70d0() {
    void** rax1;
    void** rbp2;
    int64_t v3;

    rax1 = fun_2520();
    fun_27d0(rbp2, 1, rax1, rbp2, 1, rax1);
    goto v3;
}

void fun_7128() {
    fun_2520();
    goto 0x70f9;
}

void fun_7160() {
    void** rax1;
    void** rbp2;
    int64_t v3;

    rax1 = fun_2520();
    fun_27d0(rbp2, 1, rax1, rbp2, 1, rax1);
    goto v3;
}

void fun_71d8() {
    fun_2520();
    goto 0x719b;
}
