splitbuf_finishup (struct splitbuf *ss)
{
  int argc = ss->argc;
  char **argv = ss->argv;
  char *stringbase = (char *) (ss->argv + ss->half_alloc);
  for (int i = 1; i < argc; i++)
    argv[i] = stringbase + (intptr_t) argv[i];
  return argv;
}