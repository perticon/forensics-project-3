scan_varname (char const *str)
{
  if (str[1] == '{' && (c_isalpha (str[2]) || str[2] == '_'))
    {
      char const *end = str + 3;
      while (c_isalnum (*end) || *end == '_')
        ++end;
      if (*end == '}')
        return end;
    }

  return NULL;
}