parse_split_string (char const *str, int *orig_optind,
                    int *orig_argc, char ***orig_argv)
{
  int extra_argc = *orig_argc - *orig_optind, newargc;
  char **newargv = build_argv (str, extra_argc, &newargc);

  /* Restore argv[0] - the 'env' executable name.  */
  *newargv = (*orig_argv)[0];

  /* Print parsed arguments.  */
  if (dev_debug && 1 < newargc)
    {
      devmsg ("split -S:  %s\n", quote (str));
      devmsg (" into:    %s\n", quote (newargv[1]));
      for (int i = 2; i < newargc; i++)
        devmsg ("     &    %s\n", quote (newargv[i]));
    }

  /* Add remaining arguments and terminating null from the original
     command line.  */
  memcpy (newargv + newargc, *orig_argv + *orig_optind,
          (extra_argc + 1) * sizeof *newargv);

  /* Set new values for original getopt variables.  */
  *orig_argc = newargc + extra_argc;
  *orig_argv = newargv;
  *orig_optind = 0; /* Tell getopt to restart from first argument.  */
}