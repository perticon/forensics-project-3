check_start_new_arg (struct splitbuf *ss)
{
  if (ss->sep)
    {
      splitbuf_append_byte (ss, '\0');
      int argc = ss->argc;
      if (ss->half_alloc <= argc + ss->extra_argc + 1)
        splitbuf_grow (ss);
      ss->argv[argc + 1] = ss->argv[argc];
      ss->argc = argc + 1;
      ss->sep = false;
    }
}