void splitbuf_grow(void** rdi) {
    void** rax2;

    rax2 = xpalloc();
    *reinterpret_cast<void***>(rdi) = rax2;
}