void splitbuf_grow(long *param_1)

{
  long lVar1;
  undefined8 uVar2;
  long lVar3;
  
  lVar1 = param_1[2];
  uVar2 = *(undefined8 *)(*param_1 + (long)*(int *)(param_1 + 1) * 8);
  lVar3 = xpalloc(*param_1,param_1 + 2,1,0x7fffffff,0x10);
  *param_1 = lVar3;
  (*(code *)PTR_memmove_0010bf28)(lVar3 + param_1[2] * 8,lVar3 + lVar1 * 8,uVar2);
  return;
}