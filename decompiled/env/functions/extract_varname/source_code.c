extract_varname (char const *str)
{
  idx_t i;
  char const *p;

  p = scan_varname (str);
  if (!p)
    return NULL;

  /* -2 and +2 (below) account for the '${' prefix.  */
  i = p - str - 2;

  if (i >= vnlen)
    {
      vnlen = i + 1;
      varname = xrealloc (varname, vnlen);
    }

  memcpy (varname, str + 2, i);
  varname[i] = 0;

  return varname;
}