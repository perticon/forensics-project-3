set_signal_proc_mask (void)
{
  /* Get the existing signal mask */
  sigset_t set;
  char const *debug_act;

  sigemptyset (&set);

  if (sigprocmask (0, NULL, &set))
    die (EXIT_CANCELED, errno, _("failed to get signal process mask"));

  for (int i = 1; i <= SIGNUM_BOUND; i++)
    {
      if (sigismember (&block_signals, i))
        {
          sigaddset (&set, i);
          debug_act = "BLOCK";
        }
      else if (sigismember (&unblock_signals, i))
        {
          sigdelset (&set, i);
          debug_act = "UNBLOCK";
        }
      else
        {
          debug_act = NULL;
        }

      if (dev_debug && debug_act)
        {
          char signame[SIG2STR_MAX];
          sig2str (i, signame);
          devmsg ("signal %s (%d) mask set to %s\n",
                  signame, i, debug_act);
        }
    }

  if (sigprocmask (SIG_SETMASK, &set, NULL))
    die (EXIT_CANCELED, errno, _("failed to set signal process mask"));
}