int64_t parse_signal_action_params(int64_t a1, int32_t a2) {
    int64_t v1 = __readfsqword(40); // 0x421e
    int64_t v2; // bp-72, 0x4210
    if (a1 == 0) {
        int64_t v3 = 0; // 0x431c
        int64_t v4 = v3 + 1; // 0x4326
        if (sig2str((int32_t)v3 + 1, (char *)&v2) == 0) {
            // 0x4333
            *(int32_t *)(4 * v4 + (int64_t)signals) = (char)a2 == 0 ? 4 : 2;
        }
        // 0x433e
        v3 = v4;
        while (v4 != 64) {
            // 0x4320
            v4 = v3 + 1;
            if (sig2str((int32_t)v3 + 1, (char *)&v2) == 0) {
                // 0x4333
                *(int32_t *)(4 * v4 + (int64_t)signals) = (char)a2 == 0 ? 4 : 2;
            }
            // 0x433e
            v3 = v4;
        }
        int64_t result = v1 - __readfsqword(40); // 0x4349
        if (result == 0) {
            // 0x4354
            return result;
        }
        // 0x4361
        return function_2560();
    }
    // 0x4237
    xstrdup((char *)a1);
    int64_t v5 = function_2750(); // 0x424c
    if (v5 != 0) {
        int32_t v6 = (char)a2 == 0 ? 3 : 1;
        char * v7 = (char *)v5;
        int32_t v8 = operand2sig(v7, (char *)&v2); // 0x4296
        while (v8 != 0) {
            if (v8 < 1) {
                goto lab_0x42ce;
            }
            // 0x4272
            *(int32_t *)((0x100000000 * (int64_t)v8 >> 30) + (int64_t)signals) = v6;
            int64_t v9 = function_2750(); // 0x4283
            if (v9 == 0) {
                goto lab_0x42e0;
            }
            v7 = (char *)v9;
            v8 = operand2sig(v7, (char *)&v2);
        }
        // 0x429f
        quote(v7);
        function_2520();
        function_2720();
      lab_0x42ce:
        // 0x42ce
        usage(g18);
    }
  lab_0x42e0:
    // 0x42e0
    if (v1 != __readfsqword(40)) {
        // 0x4361
        return function_2560();
    }
    // 0x42f0
    return function_2460();
}