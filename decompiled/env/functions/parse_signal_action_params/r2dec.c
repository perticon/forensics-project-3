int64_t parse_signal_action_params (int64_t arg1, int64_t arg2) {
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    if (rdi == 0) {
        goto label_0;
    }
    rax = xstrdup (rdi);
    r12 = 0x0000800b;
    rdi = rax;
    r13 = rax;
    rax = strtok (rdi, r12);
    r14 = rax;
    if (rax == 0) {
        goto label_1;
    }
    ebx -= ebx;
    ebx &= 2;
    ebx++;
    while (eax != 0) {
        if (ebx <= 0) {
            goto label_2;
        }
        rdx = signals;
        rax = (int64_t) eax;
        *((rdx + rax*4)) = ebx;
        rax = strtok (0, r12);
        r14 = rax;
        if (rax == 0) {
            goto label_1;
        }
        eax = operand2sig (r14, rbp);
    }
    rax = quote (r14, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "%s: invalid signal");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
label_2:
    rax = usage (*(obj.exit_failure));
label_1:
    rax = *((rsp + 0x18));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_3;
    }
    rdi = r13;
    void (*0x2460)() ();
label_0:
    r12d -= r12d;
    ebx = 0;
    r12d &= 2;
    r12d += 2;
    do {
        rbx++;
        eax = sig2str (rbx + 1, rbp, rdx, rcx);
        if (eax == 0) {
            rax = signals;
            *((rax + rbx*4)) = r12d;
        }
    } while (rbx != 0x40);
    rax = *((rsp + 0x18));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
label_3:
    return stack_chk_fail ();
}