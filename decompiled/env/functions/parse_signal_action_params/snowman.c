void parse_signal_action_params(void** rdi, void** esi, void** rdx, void** rcx, void** r8, int64_t r9, int64_t a7) {
    int64_t rbx8;
    void** rsp9;
    void** rax10;
    void** v11;
    void** rbp12;
    void** r12_13;
    int32_t r12d14;
    int32_t r12d15;
    uint32_t r12d16;
    int32_t r12d17;
    void** rdi18;
    void** rsi19;
    int32_t eax20;
    void** rax21;
    void* rax22;
    void** rax23;
    void** rax24;
    void** r14_25;
    void* rax26;
    int32_t eax27;
    void* rsp28;
    uint1_t zf29;
    void** rax30;
    int32_t r12d31;
    void* rsp32;
    void** rax33;
    void** v34;
    void** rdi35;
    void* rax36;
    int64_t v37;
    int1_t zf38;
    int32_t ebp39;
    void** rax40;
    void** r14_41;
    void* rax42;
    void** r13_43;
    void** r12_44;
    int32_t eax45;
    uint1_t zf46;
    int64_t rsi47;
    int64_t rsi48;
    int64_t rsi49;
    void** rax50;
    void** rax51;
    void** rax52;

    *reinterpret_cast<void***>(&rbx8) = esi;
    rsp9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 32);
    rax10 = g28;
    v11 = rax10;
    if (!rdi) {
        rbp12 = rsp9;
        *reinterpret_cast<void***>(&rbx8) = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx8) + 4) = 0;
        r12_13 = reinterpret_cast<void**>((r12d14 - (r12d15 + reinterpret_cast<uint1_t>(r12d16 < r12d17 + reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&esi) < 1))) & 2) + 2);
        do {
            *reinterpret_cast<int32_t*>(&rdi18) = static_cast<int32_t>(rbx8 + 1);
            *reinterpret_cast<int32_t*>(&rdi18 + 4) = 0;
            rsi19 = rbp12;
            ++rbx8;
            eax20 = sig2str(rdi18, rsi19, rdx, rcx, r8, r9);
            rsp9 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp9 - 8) + 8);
            if (!eax20) {
                rax21 = signals;
                *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rax21) + reinterpret_cast<uint64_t>(rbx8 * 4)) = r12_13;
            }
        } while (rbx8 != 64);
        rax22 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
        if (rax22) 
            goto addr_4361_7;
        return;
    }
    rax23 = xstrdup(rdi);
    rsi19 = reinterpret_cast<void**>(",");
    rdi18 = rax23;
    rax24 = fun_2750();
    rsp9 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp9 - 8) + 8 - 8 + 8);
    r14_25 = rax24;
    if (!rax24) {
        addr_42e0_10:
        rax26 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
        if (rax26) {
            addr_4361_7:
            fun_2560();
        } else {
            goto addr_2460_12;
        }
    } else {
        rbp12 = rsp9;
        *reinterpret_cast<void***>(&rbx8) = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rbx8)) - (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rbx8)) + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rbx8)) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rbx8)) + reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&rbx8) < 1)))) & 2) + 1);
        do {
            rsi19 = rbp12;
            eax27 = operand2sig(r14_25, rsi19, rdx, rcx);
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp9 - 8) + 8);
            zf29 = reinterpret_cast<uint1_t>(eax27 == 0);
            if (zf29) 
                goto addr_429f_15;
            if (reinterpret_cast<uint1_t>(eax27 < 0) | zf29) 
                goto addr_42ce_17;
            rdx = signals;
            rsi19 = reinterpret_cast<void**>(",");
            *reinterpret_cast<int32_t*>(&rdi18) = 0;
            *reinterpret_cast<int32_t*>(&rdi18 + 4) = 0;
            *reinterpret_cast<void***>(rdx + eax27 * 4) = *reinterpret_cast<void***>(&rbx8);
            rax30 = fun_2750();
            rsp9 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp28) - 8 + 8);
            r14_25 = rax30;
        } while (rax30);
        goto addr_42e0_10;
    }
    r12d31 = *reinterpret_cast<int32_t*>(&rsi19);
    rsp32 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp9 - 8) + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 56);
    rax33 = g28;
    v34 = rax33;
    if (!rdi18) {
        if (*reinterpret_cast<signed char*>(&rsi19)) {
            fun_2660(0xc360);
            rdi35 = reinterpret_cast<void**>(0xc2e0);
        } else {
            fun_2660(0xc2e0);
            rdi35 = reinterpret_cast<void**>(0xc360);
        }
        fun_2640(rdi35);
        sig_mask_changed = 1;
        rax36 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v34) - reinterpret_cast<unsigned char>(g28));
        if (!rax36) {
            goto v37;
        }
    }
    zf38 = sig_mask_changed == 0;
    ebp39 = *reinterpret_cast<int32_t*>(&rsi19);
    if (zf38) {
        fun_2640(0xc360);
        fun_2640(0xc2e0);
        rsp32 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp32) - 8 + 8 - 8 + 8);
    }
    sig_mask_changed = 1;
    xstrdup(rdi18);
    rax40 = fun_2750();
    r14_41 = rax40;
    if (rax40) 
        goto addr_43de_28;
    addr_4498_29:
    rax42 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v34) - reinterpret_cast<unsigned char>(g28));
    if (rax42) {
        fun_2560();
    } else {
        goto addr_2460_12;
    }
    addr_2460_12:
    addr_43de_28:
    r13_43 = reinterpret_cast<void**>(0xc360);
    if (*reinterpret_cast<signed char*>(&r12d31)) {
        r13_43 = reinterpret_cast<void**>(0xc2e0);
    }
    r12_44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp32) - 8 + 8 - 8 + 8 + 16);
    do {
        eax45 = operand2sig(r14_41, r12_44, rdx, rcx);
        zf46 = reinterpret_cast<uint1_t>(eax45 == 0);
        if (zf46) 
            break;
        if (reinterpret_cast<uint1_t>(eax45 < 0) | zf46) 
            goto addr_4487_37;
        if (*reinterpret_cast<signed char*>(&ebp39)) {
            *reinterpret_cast<int32_t*>(&rsi47) = eax45;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi47) + 4) = 0;
            fun_2810(0xc360, rsi47, rdx, rcx, r8, r9);
        } else {
            *reinterpret_cast<int32_t*>(&rsi48) = eax45;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi48) + 4) = 0;
            fun_2810(0xc2e0, rsi48, rdx, rcx, r8, r9);
        }
        *reinterpret_cast<int32_t*>(&rsi49) = eax45;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi49) + 4) = 0;
        fun_2740(r13_43, rsi49, rdx, rcx, r8, r9);
        rax50 = fun_2750();
        r14_41 = rax50;
    } while (rax50);
    goto addr_4498_29;
    quote(r14_41, r12_44, r14_41, r12_44);
    fun_2520();
    fun_2720();
    addr_4487_37:
    usage();
    goto addr_4498_29;
    addr_429f_15:
    rax51 = quote(r14_25, rsi19);
    rax52 = fun_2520();
    rcx = rax51;
    *reinterpret_cast<int32_t*>(&rsi19) = 0;
    rdx = rax52;
    fun_2720();
    rsp28 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp28) - 8 + 8 - 8 + 8 - 8 + 8);
    addr_42ce_17:
    *reinterpret_cast<int32_t*>(&rdi18) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi18 + 4) = 0;
    usage();
    rsp9 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp28) - 8 + 8);
    goto addr_42e0_10;
}