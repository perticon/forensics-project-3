void parse_signal_action_params(long param_1,char param_2)

{
  int iVar1;
  char *__s;
  char *pcVar2;
  undefined8 uVar3;
  undefined8 uVar4;
  long lVar5;
  long in_FS_OFFSET;
  undefined auStack72 [24];
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_1 == 0) {
    lVar5 = 0;
    do {
      iVar1 = (int)lVar5;
      lVar5 = lVar5 + 1;
      iVar1 = sig2str(iVar1 + 1,auStack72);
      if (iVar1 == 0) {
        *(uint *)(signals + lVar5 * 4) = (-(uint)(param_2 == '\0') & 2) + 2;
      }
    } while (lVar5 != 0x40);
    if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
      return;
    }
  }
  else {
    __s = (char *)xstrdup();
    pcVar2 = strtok(__s,",");
    if (pcVar2 != (char *)0x0) {
      do {
        iVar1 = operand2sig(pcVar2,auStack72);
        if (iVar1 == 0) {
          uVar3 = quote(pcVar2);
          uVar4 = dcgettext(0,"%s: invalid signal",5);
          error(0,0,uVar4,uVar3);
LAB_001042ce:
          usage(exit_failure);
          break;
        }
        if (iVar1 < 1) goto LAB_001042ce;
        *(uint *)(signals + (long)iVar1 * 4) = (-(uint)(param_2 == '\0') & 2) + 1;
        pcVar2 = strtok((char *)0x0,",");
      } while (pcVar2 != (char *)0x0);
    }
    if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
      free(__s);
      return;
    }
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}