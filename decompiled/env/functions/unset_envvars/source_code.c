unset_envvars (void)
{
  for (idx_t i = 0; i < usvars_used; ++i)
    {
      devmsg ("unset:    %s\n", usvars[i]);

      if (unsetenv (usvars[i]))
        die (EXIT_CANCELED, errno, _("cannot unset %s"),
             quote (usvars[i]));
    }
}