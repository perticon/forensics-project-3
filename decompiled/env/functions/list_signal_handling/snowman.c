void list_signal_handling(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9) {
    void* rsp7;
    void** rax8;
    void** v9;
    void** rbp10;
    int32_t eax11;
    void* rsp12;
    void** rdi13;
    int32_t r13d14;
    void** rbx15;
    int32_t eax16;
    int64_t rsi17;
    int64_t v18;
    int32_t eax19;
    int64_t r14_20;
    int32_t eax21;
    int1_t zf22;
    void** rcx23;
    void** rdi24;
    void* rax25;
    void** ebp26;
    int64_t rdx27;
    int64_t v28;

    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x158);
    rax8 = g28;
    v9 = rax8;
    rbp10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 16);
    fun_2640(rbp10);
    eax11 = fun_2450();
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8);
    if (eax11) {
        fun_2520();
        fun_2480();
        *reinterpret_cast<int32_t*>(&rdi13) = 0x7d;
        *reinterpret_cast<int32_t*>(&rdi13 + 4) = 0;
        fun_2720();
    } else {
        r13d14 = 1;
        rbx15 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp12) + 0x90);
        while (1) {
            *reinterpret_cast<int32_t*>(&rdi13) = r13d14;
            *reinterpret_cast<int32_t*>(&rdi13 + 4) = 0;
            eax16 = fun_24d0(rdi13);
            rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
            if (eax16) 
                goto addr_3c05_5;
            *reinterpret_cast<int32_t*>(&rsi17) = r13d14;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi17) + 4) = 0;
            rdi13 = rbp10;
            if (v18 != 1) {
                eax19 = fun_27a0(rdi13, rsi17, rbx15, rcx, r8, r9);
                rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
                if (!eax19) {
                    addr_3c05_5:
                    ++r13d14;
                    if (r13d14 == 65) 
                        break; else 
                        continue;
                } else {
                    r14_20 = reinterpret_cast<int64_t>("BLOCK");
                }
            } else {
                eax21 = fun_27a0(rdi13, rsi17, rbx15, rcx, r8, r9);
                rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
                zf22 = eax21 == 0;
                r14_20 = 0x92f1;
                if (!zf22) {
                }
                if (zf22) {
                }
                if (!zf22) {
                    r14_20 = reinterpret_cast<int64_t>("BLOCK");
                }
            }
            rcx23 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp12) + 0x130);
            *reinterpret_cast<int32_t*>(&rdi24) = r13d14;
            *reinterpret_cast<int32_t*>(&rdi24 + 4) = 0;
            sig2str(rdi24, rcx23, rbx15, rcx23, r8, r9);
            rdi13 = stderr;
            rcx = rcx23;
            r9 = r14_20;
            *reinterpret_cast<int32_t*>(&r8) = r13d14;
            *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
            fun_27d0(rdi13, 1, "%-10s (%2d): %s%s%s\n", rdi13, 1, "%-10s (%2d): %s%s%s\n");
            rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 - 8 - 8 + 8 + 8 + 8);
            goto addr_3c05_5;
        }
        rax25 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
        if (!rax25) 
            goto addr_3c83_18;
    }
    fun_2560();
    splitbuf_append_byte(rdi13, 0, rdi13, 0);
    ebp26 = *reinterpret_cast<void***>(rdi13 + 8);
    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi13 + 16)) <= reinterpret_cast<signed char>(static_cast<int64_t>(reinterpret_cast<int32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi13 + 24)) + reinterpret_cast<unsigned char>(ebp26) + 1)))) {
        splitbuf_grow(rdi13);
    }
    rdx27 = reinterpret_cast<int32_t>(ebp26);
    *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(*reinterpret_cast<void***>(rdi13) + rdx27 * 8) + 8) = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi13) + rdx27 * 8);
    *reinterpret_cast<void***>(rdi13 + 8) = ebp26 + 1;
    *reinterpret_cast<signed char*>(rdi13 + 28) = 0;
    goto v28;
    addr_3c83_18:
    return;
}