list_signal_handling (void)
{
  sigset_t set;
  char signame[SIG2STR_MAX];

  sigemptyset (&set);
  if (sigprocmask (0, NULL, &set))
    die (EXIT_CANCELED, errno, _("failed to get signal process mask"));

  for (int i = 1; i <= SIGNUM_BOUND; i++)
    {
      struct sigaction act;
      if (sigaction (i, NULL, &act))
        continue;

      char const *ignored = act.sa_handler == SIG_IGN ? "IGNORE" : "";
      char const *blocked = sigismember (&set, i) ? "BLOCK" : "";
      char const *connect = *ignored && *blocked ? "," : "";

      if (! *ignored && ! *blocked)
        continue;

      sig2str (i, signame);
      fprintf (stderr, "%-10s (%2d): %s%s%s\n", signame, i,
               blocked, connect, ignored);
    }
}