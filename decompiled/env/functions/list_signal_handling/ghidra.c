void list_signal_handling(void)

{
  int iVar1;
  int iVar2;
  undefined8 uVar3;
  int *piVar4;
  char *pcVar5;
  char *pcVar6;
  char *pcVar7;
  long in_FS_OFFSET;
  sigset_t local_178;
  sigaction local_f8;
  undefined local_58 [24];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  sigemptyset(&local_178);
  iVar1 = sigprocmask(0,(sigset_t *)0x0,&local_178);
  if (iVar1 == 0) {
    iVar1 = 1;
    do {
      iVar2 = sigaction(iVar1,(sigaction *)0x0,&local_f8);
      if (iVar2 == 0) {
        if (local_f8.__sigaction_handler == 1) {
          iVar2 = sigismember(&local_178,iVar1);
          pcVar7 = "";
          pcVar6 = "";
          if (iVar2 != 0) {
            pcVar7 = ",";
          }
          pcVar5 = "IGNORE";
          if (iVar2 != 0) {
            pcVar6 = "BLOCK";
          }
        }
        else {
          iVar2 = sigismember(&local_178,iVar1);
          if (iVar2 == 0) goto LAB_00103c05;
          pcVar5 = "";
          pcVar6 = "BLOCK";
          pcVar7 = "";
        }
        sig2str(iVar1,local_58);
        __fprintf_chk(stderr,1,"%-10s (%2d): %s%s%s\n",local_58,iVar1,pcVar6,pcVar7,pcVar5);
      }
LAB_00103c05:
      iVar1 = iVar1 + 1;
    } while (iVar1 != 0x41);
    if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
      return;
    }
  }
  else {
    uVar3 = dcgettext(0,"failed to get signal process mask",5);
    piVar4 = __errno_location();
    error(0x7d,*piVar4,uVar3);
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}