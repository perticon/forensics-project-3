void splitbuf_append_byte(int32_t * ss, char c) {
    int64_t v1 = (int64_t)ss;
    int32_t * v2 = (int32_t *)(v1 + 8); // 0x3af0
    uint64_t v3 = *(int64_t *)(8 * (int64_t)*v2 + v1); // 0x3afe
    int64_t * v4 = (int64_t *)(v1 + 16); // 0x3b05
    int64_t v5 = 8 * *v4; // 0x3b09
    int64_t v6 = v5; // 0x3b10
    if (v5 <= v3) {
        // 0x3b30
        splitbuf_grow(ss);
        v6 = 8 * *v4;
    }
    // 0x3b12
    *(char *)(v3 + v1 + v6) = c;
    *(int64_t *)(8 * (int64_t)*v2 + v1) = v3 + 1;
}