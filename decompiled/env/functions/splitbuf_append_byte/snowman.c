void splitbuf_append_byte(void** rdi, uint32_t esi, ...) {
    int64_t rax3;
    void** rdx4;
    uint32_t r12d5;
    void** rbp6;
    void** rax7;

    rax3 = reinterpret_cast<int32_t>(*reinterpret_cast<void***>(rdi + 8));
    rdx4 = *reinterpret_cast<void***>(rdi);
    r12d5 = esi;
    rbp6 = *reinterpret_cast<void***>(rdx4 + rax3 * 8);
    rax7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 16)) << 3);
    if (reinterpret_cast<unsigned char>(rax7) <= reinterpret_cast<unsigned char>(rbp6)) {
        splitbuf_grow(rdi);
        rdx4 = *reinterpret_cast<void***>(rdi);
        rax7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 16)) << 3);
    }
    *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx4) + reinterpret_cast<unsigned char>(rax7)) + reinterpret_cast<unsigned char>(rbp6)) = *reinterpret_cast<signed char*>(&r12d5);
    *reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi) + reinterpret_cast<int32_t>(*reinterpret_cast<void***>(rdi + 8)) * 8) = rbp6 + 1;
    return;
}