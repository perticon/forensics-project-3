void splitbuf_append_byte(long *param_1,undefined param_2)

{
  ulong uVar1;
  ulong uVar2;
  long lVar3;
  
  lVar3 = *param_1;
  uVar1 = *(ulong *)(lVar3 + (long)*(int *)(param_1 + 1) * 8);
  uVar2 = param_1[2] << 3;
  if (uVar2 <= uVar1) {
    splitbuf_grow();
    lVar3 = *param_1;
    uVar2 = param_1[2] << 3;
  }
  *(undefined *)(lVar3 + uVar2 + uVar1) = param_2;
  *(ulong *)(*param_1 + (long)*(int *)(param_1 + 1) * 8) = uVar1 + 1;
  return;
}