splitbuf_append_byte (struct splitbuf *ss, char c)
{
  idx_t string_bytes = (intptr_t) ss->argv[ss->argc];
  if (ss->half_alloc * sizeof *ss->argv <= string_bytes)
    splitbuf_grow (ss);
  ((char *) (ss->argv + ss->half_alloc))[string_bytes] = c;
  ss->argv[ss->argc] = (char *) (intptr_t) (string_bytes + 1);
}