build_argv (char const *str, int extra_argc, int *argc)
{
  bool dq = false, sq = false;
  struct splitbuf ss;
  ss.argv = xnmalloc (extra_argc + 2, 2 * sizeof *ss.argv);
  ss.argc = 1;
  ss.half_alloc = extra_argc + 2;
  ss.extra_argc = extra_argc;
  ss.sep = true;
  ss.argv[ss.argc] = 0;

  /* In the following loop,
     'break' causes the character 'newc' to be added to *dest,
     'continue' skips the character.  */
  while (*str)
    {
      char newc = *str; /* Default: add the next character.  */

      switch (*str)
        {
        case '\'':
          if (dq)
            break;
          sq = !sq;
          check_start_new_arg (&ss);
          ++str;
          continue;

        case '"':
          if (sq)
            break;
          dq = !dq;
          check_start_new_arg (&ss);
          ++str;
          continue;

        case ' ': case '\t': case '\n': case '\v': case '\f': case '\r':
          /* Start a new argument if outside quotes.  */
          if (sq || dq)
            break;
          ss.sep = true;
          str += strspn (str, C_ISSPACE_CHARS);
          continue;

        case '#':
          if (!ss.sep)
            break;
          goto eos; /* '#' as first char terminates the string.  */

        case '\\':
          /* Backslash inside single-quotes is not special, except \\
             and \'.  */
          if (sq && str[1] != '\\' && str[1] != '\'')
            break;

          /* Skip the backslash and examine the next character.  */
          newc = *++str;
          switch (newc)
            {
            case '"': case '#': case '$': case '\'': case '\\':
              /* Pass escaped character as-is.  */
              break;

            case '_':
              if (!dq)
                {
                  ++str;  /* '\_' outside double-quotes is arg separator.  */
                  ss.sep = true;
                  continue;
                }
              newc = ' ';  /* '\_' inside double-quotes is space.  */
              break;

            case 'c':
              if (dq)
                die (EXIT_CANCELED, 0,
                     _("'\\c' must not appear in double-quoted -S string"));
              goto eos; /* '\c' terminates the string.  */

            case 'f': newc = '\f'; break;
            case 'n': newc = '\n'; break;
            case 'r': newc = '\r'; break;
            case 't': newc = '\t'; break;
            case 'v': newc = '\v'; break;

            case '\0':
              die (EXIT_CANCELED, 0,
                   _("invalid backslash at end of string in -S"));

            default:
              die (EXIT_CANCELED, 0, _("invalid sequence '\\%c' in -S"), newc);
            }
          break;

        case '$':
          /* ${VARNAME} are not expanded inside single-quotes.  */
          if (sq)
            break;

          /* Store the ${VARNAME} value.  */
          {
            char *n = extract_varname (str);
            if (!n)
              die (EXIT_CANCELED, 0,
                   _("only ${VARNAME} expansion is supported, error at: %s"),
                   str);

            char *v = getenv (n);
            if (v)
              {
                check_start_new_arg (&ss);
                devmsg ("expanding ${%s} into %s\n", n, quote (v));
                for (; *v; v++)
                  splitbuf_append_byte (&ss, *v);
              }
            else
              devmsg ("replacing ${%s} with null string\n", n);

            str = strchr (str, '}') + 1;
            continue;
          }
        }

      check_start_new_arg (&ss);
      splitbuf_append_byte (&ss, newc);
      ++str;
    }

  if (dq || sq)
    die (EXIT_CANCELED, 0, _("no terminating quote in -S string"));

 eos:
  splitbuf_append_byte (&ss, '\0');
  *argc = ss.argc;
  return splitbuf_finishup (&ss);
}