void parse_block_signal_params(char * optarg, bool block) {
    int64_t v1 = __readfsqword(40); // 0x4381
    if (optarg == NULL) {
        // 0x44e8
        function_2660();
        function_2640();
        *(char *)&sig_mask_changed = 1;
        if (v1 == __readfsqword(40)) {
            // 0x451c
            return;
        }
        // 0x4545
        function_2560();
        return;
    }
    // 0x439a
    if (*(char *)&sig_mask_changed == 0) {
        // 0x44c8
        function_2640();
        function_2640();
    }
    // 0x43ac
    *(char *)&sig_mask_changed = 1;
    xstrdup(optarg);
    int64_t v2 = function_2750(); // 0x43cd
    if (v2 != 0) {
        char * v3 = (char *)v2;
        int64_t v4; // bp-88, 0x4370
        int32_t v5 = operand2sig(v3, (char *)&v4); // 0x4431
        while (v5 != 0) {
            if (v5 < 1) {
                goto lab_0x4487;
            }
            // 0x443f
            function_2810();
            function_2740();
            int64_t v6 = function_2750(); // 0x441e
            if (v6 == 0) {
                goto lab_0x4498;
            }
            v3 = (char *)v6;
            v5 = operand2sig(v3, (char *)&v4);
        }
        // 0x4458
        quote(v3);
        function_2520();
        function_2720();
      lab_0x4487:
        // 0x4487
        usage(g18);
    }
  lab_0x4498:
    // 0x4498
    if (v1 == __readfsqword(40)) {
        // 0x44ac
        function_2460();
        return;
    }
    // 0x4545
    function_2560();
}