void parse_block_signal_params(void** rdi, int32_t esi, void** rdx, void** rcx, void** r8, int64_t r9, int64_t a7) {
    int32_t r12d8;
    void* rsp9;
    void** rax10;
    void** v11;
    void** rdi12;
    void* rax13;
    int1_t zf14;
    int32_t ebp15;
    void** rax16;
    void** r14_17;
    void* rax18;
    void** r13_19;
    void** r12_20;
    int32_t eax21;
    uint1_t zf22;
    int64_t rsi23;
    int64_t rsi24;
    int64_t rsi25;
    void** rax26;

    r12d8 = esi;
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56);
    rax10 = g28;
    v11 = rax10;
    if (!rdi) {
        if (*reinterpret_cast<signed char*>(&esi)) {
            fun_2660(0xc360);
            rdi12 = reinterpret_cast<void**>(0xc2e0);
        } else {
            fun_2660(0xc2e0);
            rdi12 = reinterpret_cast<void**>(0xc360);
        }
        fun_2640(rdi12);
        sig_mask_changed = 1;
        rax13 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
        if (!rax13) {
            return;
        }
    } else {
        zf14 = sig_mask_changed == 0;
        ebp15 = esi;
        if (zf14) {
            fun_2640(0xc360);
            fun_2640(0xc2e0);
            rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 + 8);
        }
        sig_mask_changed = 1;
        xstrdup(rdi);
        rax16 = fun_2750();
        r14_17 = rax16;
        if (!rax16) 
            goto addr_4498_10; else 
            goto addr_43de_11;
    }
    fun_2560();
    addr_4498_10:
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
    if (!rax18) {
    }
    addr_43de_11:
    r13_19 = reinterpret_cast<void**>(0xc360);
    if (*reinterpret_cast<signed char*>(&r12d8)) {
        r13_19 = reinterpret_cast<void**>(0xc2e0);
    }
    r12_20 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 + 8 + 16);
    do {
        eax21 = operand2sig(r14_17, r12_20, rdx, rcx);
        zf22 = reinterpret_cast<uint1_t>(eax21 == 0);
        if (zf22) 
            break;
        if (reinterpret_cast<uint1_t>(eax21 < 0) | zf22) 
            goto addr_4487_20;
        if (*reinterpret_cast<signed char*>(&ebp15)) {
            *reinterpret_cast<int32_t*>(&rsi23) = eax21;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi23) + 4) = 0;
            fun_2810(0xc360, rsi23, rdx, rcx, r8, r9);
        } else {
            *reinterpret_cast<int32_t*>(&rsi24) = eax21;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi24) + 4) = 0;
            fun_2810(0xc2e0, rsi24, rdx, rcx, r8, r9);
        }
        *reinterpret_cast<int32_t*>(&rsi25) = eax21;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi25) + 4) = 0;
        fun_2740(r13_19, rsi25, rdx, rcx, r8, r9);
        rax26 = fun_2750();
        r14_17 = rax26;
    } while (rax26);
    goto addr_4498_10;
    quote(r14_17, r12_20);
    fun_2520();
    fun_2720();
    addr_4487_20:
    usage();
    goto addr_4498_10;
}