void parse_block_signal_params(long param_1,char param_2)

{
  long lVar1;
  int __signo;
  char *__s;
  char *pcVar2;
  undefined8 uVar3;
  undefined8 uVar4;
  undefined1 *puVar5;
  long in_FS_OFFSET;
  
  lVar1 = *(long *)(in_FS_OFFSET + 0x28);
  if (param_1 == 0) {
    if (param_2 == '\0') {
      sigfillset((sigset_t *)unblock_signals);
      puVar5 = block_signals;
    }
    else {
      sigfillset((sigset_t *)block_signals);
      puVar5 = unblock_signals;
    }
    sigemptyset((sigset_t *)puVar5);
    sig_mask_changed = 1;
    if (lVar1 == *(long *)(in_FS_OFFSET + 0x28)) {
      sig_mask_changed = 1;
      return;
    }
  }
  else {
    if (sig_mask_changed == '\0') {
      sigemptyset((sigset_t *)block_signals);
      sigemptyset((sigset_t *)unblock_signals);
    }
    sig_mask_changed = 1;
    __s = (char *)xstrdup(param_1);
    pcVar2 = strtok(__s,",");
    if (pcVar2 != (char *)0x0) {
      puVar5 = block_signals;
      if (param_2 != '\0') {
        puVar5 = unblock_signals;
      }
      do {
        __signo = operand2sig(pcVar2);
        if (__signo == 0) {
          uVar3 = quote(pcVar2);
          uVar4 = dcgettext(0,"%s: invalid signal",5);
          error(0,0,uVar4,uVar3);
LAB_00104487:
          usage(exit_failure);
          break;
        }
        if (__signo < 1) goto LAB_00104487;
        if (param_2 == '\0') {
          sigaddset((sigset_t *)unblock_signals,__signo);
        }
        else {
          sigaddset((sigset_t *)block_signals,__signo);
        }
        sigdelset((sigset_t *)puVar5,__signo);
        pcVar2 = strtok((char *)0x0,",");
      } while (pcVar2 != (char *)0x0);
    }
    if (lVar1 == *(long *)(in_FS_OFFSET + 0x28)) {
      free(__s);
      return;
    }
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}