#include <stdint.h>

/* /tmp/tmpvkjt68yd @ 0x3070 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (dbg.main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmpvkjt68yd @ 0x3f00 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x000165a7;
        rdx = 0x00016598;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x0001659f;
        rdx = 0x000165a1;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x000165a3;
    rdx = 0x0001659c;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x154e0 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = "ASCII";
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = "ASCII";
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x2770 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmpvkjt68yd @ 0x3fe0 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x2920)() ();
    }
    rdx = 0x00016600;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0x16600 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x000165ab;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x000165a1;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x0001662c;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0x1662c */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x0001659f;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x000165a1;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x0001659f;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x000165a1;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x0001672c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0x1672c */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x0001682c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0x1682c */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x000165a1;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x0001659f;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x0001659f;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x000165a1;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmpvkjt68yd @ 0x2920 */
 
void quotearg_buffer_restyled_cold (void) {
    /* [16] -r-x section size 77938 named .text */
    return abort ();
}

/* /tmp/tmpvkjt68yd @ 0x5400 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x2925)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                *((rsp + 0x10)) = rsi;
                free (r14);
                rsi = *((rsp + 0x10));
            }
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc (*((rsp + 0x10)));
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x2925 */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmpvkjt68yd @ 0x292a */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmpvkjt68yd @ 0x24e0 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmpvkjt68yd @ 0x2930 */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmpvkjt68yd @ 0x2935 */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpvkjt68yd @ 0x293a */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmpvkjt68yd @ 0x293f */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpvkjt68yd @ 0x2944 */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmpvkjt68yd @ 0x2949 */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpvkjt68yd @ 0x294e */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpvkjt68yd @ 0x2953 */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpvkjt68yd @ 0x2958 */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpvkjt68yd @ 0x61e0 */
 
int32_t dbg_re_node_set_compare (re_node_set const * set1, re_node_set const * set2) {
    rdi = set1;
    rsi = set2;
    /* _Bool re_node_set_compare(re_node_set const * set1,re_node_set const * set2); */
    al = (rdi == 0) ? 1 : 0;
    dl = (rsi == 0) ? 1 : 0;
    al |= dl;
    if (al != 0) {
        goto label_1;
    }
    rdx = *((rdi + 8));
    if (rdx == *((rsi + 8))) {
        goto label_2;
    }
    do {
        return al;
label_0:
        rcx = *((rsi + 0x10));
        r8 = *((rdi + 0x10));
        rcx = *((rcx + rdx*8));
    } while (*((r8 + rdx*8)) != rcx);
label_2:
    rdx--;
    if (rdx >= 0) {
        goto label_0;
    }
    eax = 1;
    return eax;
label_1:
    eax = 0;
    return eax;
}

/* /tmp/tmpvkjt68yd @ 0x6230 */
 
uint64_t dbg_re_node_set_contains (void * arg1, Idx elem) {
    rdi = arg1;
    rsi = elem;
    /* Idx re_node_set_contains(re_node_set const * set,Idx elem); */
    rdx = *((rdi + 8));
    eax = 0;
    if (rdx <= 0) {
        goto label_1;
    }
    rdi = *((rdi + 0x10));
    rdx--;
    ecx = 0;
    while (rcx < rdx) {
        rax = rcx + rdx;
        rax >>= 1;
        if (*((rdi + rax*8)) < rsi) {
            goto label_2;
        }
        rdx = rax;
label_0:
    }
    eax = 0;
    if (*((rdi + rcx*8)) != rsi) {
label_1:
        return rax;
label_2:
        rcx = rax + 1;
        goto label_0;
    }
    rax = rcx + 1;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x6290 */
 
int64_t dbg_peek_token_bracket (int64_t arg1, int64_t arg3, re_string_t * input) {
    rdi = arg1;
    rdx = arg3;
    rsi = input;
    /* int peek_token_bracket(re_token_t * token,re_string_t * input,reg_syntax_t syntax); */
    rcx = *((rsi + 0x48));
    rax = rdi;
    if (*((rsi + 0x68)) <= rcx) {
        goto label_2;
    }
    r8 = *((rsi + 8));
    edi = *((r8 + rcx));
    *(rax) = dil;
    if (*((rsi + 0x90)) > 1) {
        if (rcx == *((rsi + 0x30))) {
            goto label_3;
        }
        r9 = *((rsi + 0x10));
        if (*((r9 + rcx*4)) == 0xffffffff) {
            goto label_0;
        }
    }
label_3:
    if (dil != 0x5c) {
        if (dil == 0x5b) {
            goto label_4;
        }
        if (dil == 0x5d) {
            goto label_5;
        }
        if (dil != 0x5e) {
            goto label_6;
        }
        *((rax + 8)) = 0x19;
        eax = 1;
        return rax;
    }
    edx &= 1;
    if (edx == 0) {
        goto label_0;
    }
    rdx = rcx + 1;
    if (rdx < *((rsi + 0x58))) {
        goto label_7;
    }
    do {
label_0:
        *((rax + 8)) = 1;
        eax = 1;
        return rax;
label_6:
    } while (dil != 0x2d);
    rdx = rcx + 2;
    if (rdx >= *((rsi + 0x58))) {
        goto label_8;
    }
    while (*((r8 + rcx + 2)) != 0x2d) {
label_8:
        *((rax + 8)) = 0x16;
        eax = 1;
        return rax;
label_2:
        *((rdi + 8)) = 2;
        eax = 0;
        return rax;
label_4:
        rdi = rcx + 1;
        if (rdi < *((rsi + 0x58))) {
            ecx = *((r8 + rcx + 1));
            *(rax) = cl;
            if (cl == 0x3a) {
                goto label_9;
            }
            if (cl == 0x3d) {
                goto label_10;
            }
            if (cl == 0x2e) {
                goto label_11;
            }
        }
label_1:
        *((rax + 8)) = 1;
        *(rax) = 0x5b;
        eax = 1;
        return rax;
label_5:
        *((rax + 8)) = 0x15;
        eax = 1;
        return rax;
label_7:
        *((rsi + 0x48)) = rdx;
        edx = *((r8 + rcx + 1));
        *((rax + 8)) = 1;
        *(rax) = dl;
        eax = 1;
        return rax;
label_11:
        *((rax + 8)) = 0x1a;
        eax = 2;
        return rax;
label_10:
        *((rax + 8)) = 0x1c;
        eax = 2;
        return rax;
    }
    *((rsi + 0x48)) = rdx;
    goto label_0;
label_9:
    edx &= 4;
    if (edx == 0) {
        goto label_1;
    }
    *((rax + 8)) = 0x1e;
    eax = 2;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x6400 */
 
int64_t dbg_check_dst_limits_calc_pos_1 (int64_t arg1, int64_t arg2, uint32_t arg3, int64_t arg4, uint32_t arg5) {
    uint32_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    uint32_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_34h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* int check_dst_limits_calc_pos_1(re_match_context_t const * mctx,int boundaries,Idx subexp_idx,Idx from_node,Idx bkref_idx); */
label_1:
    r9 = rcx;
    rcx *= 3;
    r10 = *((rdi + 0x98));
    rax = *((r10 + 0x30));
    rax = rax + rcx*8;
    r13 = *((rax + 8));
    if (r13 <= 0) {
        goto label_6;
    }
    rcx = r8 * 3;
    r11d = 1;
    r15 = *((rax + 0x10));
    eax = esi;
    rcx <<= 4;
    eax &= 1;
    *((rsp + 8)) = r9;
    r14 = *(r10);
    *((rsp + 0x38)) = rcx;
    ecx = edx;
    ebx = 0;
    r11 <<= cl;
    *((rsp + 0x30)) = eax;
    *((rsp + 0x10)) = r11;
    r11 = ~r11;
    *((rsp + 0x18)) = r11;
    r11d = esi;
    r11d &= 2;
    while (cl != 9) {
        if (cl == 4) {
            goto label_7;
        }
label_0:
        rbx++;
        if (rbx == r13) {
            goto label_6;
        }
label_5:
        rbp = *((r15 + rbx*8));
        rax = *((r15 + rbx*8));
        rax <<= 4;
        rax += r14;
        ecx = *((rax + 8));
        if (cl == 8) {
            goto label_8;
        }
    }
    if (r11d == 0) {
        goto label_0;
    }
    if (*(rax) != rdx) {
        goto label_0;
    }
label_3:
    eax = 0;
    return rax;
label_8:
    ecx = *((rsp + 0x30));
    if (ecx == 0) {
        goto label_0;
    }
    if (*(rax) != rdx) {
        goto label_0;
    }
label_2:
    eax = 0xffffffff;
    return rax;
label_7:
    if (r8 == -1) {
        goto label_0;
    }
    r9 = rbp + rbp*2;
    *((rsp + 0x40)) = rbx;
    r12 = *((rsp + 0x38));
    rbx = rdx;
    *((rsp + 0x48)) = r13;
    r9 <<= 3;
    r13 = r10;
    r12 += *((rdi + 0xd8));
    *((rsp + 0x50)) = r15;
    r15d = esi;
    *((rsp + 0x58)) = r14;
    r14 = rdi;
label_4:
    if (*(r12) != rbp) {
        goto label_9;
    }
    if (rbx <= 0x3f) {
        rax = *((rsp + 0x10));
        rax &= *((r12 + 0x20));
        if (rax == 0) {
            goto label_9;
        }
    }
    rax = *((r13 + 0x28));
    rax = *((rax + r9 + 0x10));
    rcx = *(rax);
    if (*((rsp + 8)) == rcx) {
        goto label_10;
    }
    *((rsp + 0x34)) = r11d;
    *((rsp + 0x28)) = r9;
    *((rsp + 0x20)) = r8;
    eax = check_dst_limits_calc_pos_1 (r14, r15d, rbx, rcx, r8);
    goto label_1;
    r8 = *((rsp + 0x20));
    r9 = *((rsp + 0x28));
    r11d = *((rsp + 0x34));
    if (eax == 0xffffffff) {
        goto label_2;
    }
    if (eax != 0) {
        goto label_11;
    }
    if (r11d != 0) {
        goto label_3;
    }
label_11:
    if (rbx <= 0x3f) {
        rax = *((rsp + 0x18));
        *((r12 + 0x20)) &= rax;
    }
label_9:
    r12 += 0x30;
    if (*((r12 - 8)) != 0) {
        goto label_4;
    }
    rdx = rbx;
    rbx = *((rsp + 0x40));
    r10 = r13;
    r13 = *((rsp + 0x48));
    rdi = r14;
    esi = r15d;
    r14 = *((rsp + 0x58));
    r15 = *((rsp + 0x50));
    rbx++;
    if (rbx != r13) {
        goto label_5;
    }
label_6:
    eax = esi;
    eax >>= 1;
    return rax;
label_10:
    esi = r15d;
    esi &= 1;
    eax = esi;
    eax = -eax;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x6630 */
 
uint64_t dbg_search_cur_bkref_entry (re_match_context_t const * mctx, Idx str_idx) {
    rdi = mctx;
    rsi = str_idx;
    /* Idx search_cur_bkref_entry(re_match_context_t const * mctx,Idx str_idx); */
    r9 = *((rdi + 0xc8));
    r8d = 0;
    rdx = r9;
    while (rdx > r8) {
        rax = rdx + r8;
        rcx = rax;
        rax &= 0xfffffffffffffffe;
        rcx >>= 1;
        rax += rcx;
        rax <<= 4;
        rax += *((rdi + 0xd8));
        if (*((rax + 8)) < rsi) {
            goto label_1;
        }
        rdx = rcx;
label_0:
    }
    if (r9 <= r8) {
        goto label_2;
    }
    rax = r8 * 3;
    rax <<= 4;
    rax += *((rdi + 0xd8));
    rax = 0xffffffffffffffff;
    r8 = rax;
    while (1) {
        rax = r8;
        return rax;
label_1:
        r8 = rcx + 1;
        goto label_0;
label_2:
        r8 = 0xffffffffffffffff;
    }
}

/* /tmp/tmpvkjt68yd @ 0x66b0 */
 
int64_t dbg_re_string_realloc_buffers (uint32_t arg2, void ** ptr) {
    rsi = arg2;
    rdi = ptr;
    /* reg_errcode_t re_string_realloc_buffers(re_string_t * pstr,Idx new_buf_len); */
    rbx = rdi;
    if (*((rdi + 0x90)) > 1) {
        rax = rsi;
        rax >>= 0x3d;
        if (rax != 0) {
            goto label_0;
        }
        rax = realloc (*((rdi + 0x10)), rsi*4);
        if (rax == 0) {
            goto label_0;
        }
        rdi = *((rbx + 0x18));
        *((rbx + 0x10)) = rax;
        if (rdi == 0) {
            goto label_1;
        }
        rax = realloc (rdi, rbp*8);
        if (rax == 0) {
            goto label_0;
        }
        *((rbx + 0x18)) = rax;
    }
label_1:
    while (1) {
        *((rbx + 0x40)) = rbp;
        eax = 0;
        return rax;
        rax = realloc (*((rbx + 8)), rbp);
        if (rax == 0) {
            goto label_0;
        }
        *((rbx + 8)) = rax;
    }
label_0:
    eax = 0xc;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x6750 */
 
uint64_t dbg_re_node_set_insert_last (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* _Bool re_node_set_insert_last(re_node_set * set,Idx elem); */
    rbx = rdi;
    rdx = *((rdi + 8));
    rdi = *((rdi + 0x10));
    while (1) {
        rax = rdx + 1;
        *((rbx + 8)) = rax;
        eax = 1;
        *((rdi + rdx*8)) = rbp;
label_0:
        return rax;
        rax = rsi + rsi;
        rsi <<= 4;
        *(rbx) = rax;
        rax = realloc (rdi, rdx + 1);
        rdi = rax;
        if (rax == 0) {
            goto label_1;
        }
        *((rbx + 0x10)) = rax;
        rdx = *((rbx + 8));
    }
label_1:
    eax = 0;
    goto label_0;
}

/* /tmp/tmpvkjt68yd @ 0x67c0 */
 
int64_t create_token_tree (int64_t arg1, void ** arg2, int64_t arg3, size_t arg4) {
    void ** var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rcx;
    r12 = rdx;
    rbx = rdi;
    rdx = *((rdi + 0x80));
    if (edx == 0xf) {
        goto label_1;
    }
    r8 = rdx;
    rax = *((rdi + 0x70));
    ecx = rdx + 1;
    r8 <<= 6;
    r8 += 8;
    do {
        rdx <<= 6;
        r8 += rax;
        *((rbx + 0x80)) = ecx;
        rax += rdx;
        *((rax + 8)) = 0;
        *((rax + 0x10)) = rsi;
        *((rax + 0x18)) = r12;
        __asm ("movdqu xmm0, xmmword [r13]");
        *((rax + 0x20)) = 0;
        __asm ("movups xmmword [rax + 0x30], xmm0");
        *((rax + 0x28)) = 0;
        *((rax + 0x3a)) &= 0xf3;
        *((rax + 0x40)) = 0xffffffffffffffff;
        if (rsi != 0) {
            *(rsi) = r8;
        }
        if (r12 != 0) {
            *(r12) = r8;
        }
label_0:
        rax = r8;
        return rax;
label_1:
        *((rsp + 8)) = rsi;
        rax = malloc (0x3c8);
        if (rax == 0) {
            goto label_2;
        }
        rdx = *((rbx + 0x70));
        rsi = *((rsp + 8));
        *((rbx + 0x70)) = rax;
        r8d = 8;
        ecx = 1;
        *(rax) = rdx;
        edx = 0;
    } while (1);
label_2:
    r8d = 0;
    goto label_0;
}

/* /tmp/tmpvkjt68yd @ 0x68a0 */
 
int64_t dbg_lower_subexp (int64_t arg_28h, int64_t arg_32h, int64_t arg1, int64_t arg2, int64_t arg3) {
    re_token_t t;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* bin_tree_t * lower_subexp(reg_errcode_t * err,regex_t * preg,bin_tree_t * node); */
    rbx = rdx;
    r15 = *(rsi);
    r12 = *((rdx + 8));
    *((rsp + 8)) = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    if ((*((rsi + 0x38)) & 0x10) == 0) {
        goto label_3;
    }
    if (r12 == 0) {
        goto label_4;
    }
    rax = *((rdx + 0x28));
    if (rax > 0x3f) {
        goto label_2;
    }
    rdx = *((r15 + 0xa0));
    if (((rdx >> rax) & 1) >= 0) {
        goto label_2;
    }
    r13 = rsp + 0x10;
    xmm0 = 0;
    edx = 0;
    esi = 0;
    rcx = r13;
    rdi = r15;
    *((rsp + 0x10)) = xmm0;
    *((rsp + 0x18)) = 8;
    rax = create_token_tree ();
    xmm0 = 0;
    rcx = r13;
    edx = 0;
    esi = 0;
    rdi = r15;
    *((rsp + 0x10)) = xmm0;
    *((rsp + 0x18)) = 9;
    rax = create_token_tree ();
    r14 = rax;
label_0:
    xmm0 = 0;
    rdx = r14;
    rcx = r13;
    rsi = r12;
    rdi = r15;
    *((rsp + 0x10)) = xmm0;
    *((rsp + 0x18)) = 0x10;
    rax = create_token_tree ();
    rdx = rax;
    do {
label_1:
        xmm0 = 0;
        rcx = r13;
        rsi = rbp;
        rdi = r15;
        *((rsp + 0x10)) = xmm0;
        *(rsp) = rdx;
        *((rsp + 0x18)) = 0x10;
        rax = create_token_tree ();
        r12 = rax;
        if (rax == 0) {
            goto label_5;
        }
        rdx = *(rsp);
        if (rdx == 0) {
            goto label_5;
        }
        if (rbp == 0) {
            goto label_5;
        }
        if (r14 == 0) {
            goto label_5;
        }
        rax = *((rbx + 0x28));
        edx = *((rbx + 0x32));
        *((r14 + 0x28)) = rax;
        edx &= 8;
        *((rbp + 0x28)) = rax;
        eax = *((r14 + 0x32));
        eax &= 0xfffffff7;
        eax |= edx;
        *((r14 + 0x32)) = al;
        eax = *((rbp + 0x32));
        eax &= 0xfffffff7;
        eax |= edx;
        *((rbp + 0x32)) = al;
label_2:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_6;
        }
        rax = r12;
        return rax;
label_4:
        r13 = rsp + 0x10;
        xmm0 = 0;
        edx = 0;
        esi = 0;
        rcx = r13;
        rdi = r15;
        *((rsp + 0x10)) = xmm0;
        *((rsp + 0x18)) = 8;
        rax = create_token_tree ();
        xmm0 = 0;
        edx = 0;
        rcx = r13;
        esi = 0;
        rdi = r15;
        *((rsp + 0x10)) = xmm0;
        *((rsp + 0x18)) = 9;
        rax = create_token_tree ();
        rdx = rax;
        r14 = rax;
    } while (1);
label_3:
    r13 = rsp + 0x10;
    xmm0 = 0;
    edx = 0;
    esi = 0;
    rcx = r13;
    rdi = r15;
    *((rsp + 0x10)) = xmm0;
    *((rsp + 0x18)) = 8;
    rax = create_token_tree ();
    xmm0 = 0;
    edx = 0;
    rcx = r13;
    esi = 0;
    rdi = r15;
    *((rsp + 0x10)) = xmm0;
    *((rsp + 0x18)) = 9;
    rax = create_token_tree ();
    r14 = rax;
    rdx = rax;
    if (r12 != 0) {
        goto label_0;
    }
    goto label_1;
label_5:
    rax = *((rsp + 8));
    r12d = 0;
    *(rax) = 0xc;
    goto label_2;
label_6:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x6ad0 */
 
int64_t dbg_re_dfa_add_node (int64_t arg2, int64_t arg3, void ** ptr) {
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rdi = ptr;
    /* Idx re_dfa_add_node(re_dfa_t * dfa,re_token_t token); */
    r13 = rsi;
    rbx = rdi;
    rsi = *((rdi + 0x10));
    r12 = *((rdi + 8));
    while (1) {
        rax = rsi;
        edx = ebp;
        edx &= 0xfffc00ff;
        rax <<= 4;
        rax += *(rbx);
        *((rax + 8)) = rbp;
        *(rax) = r13;
        *((rax + 8)) = edx;
        dl = (bpl == 6) ? 1 : 0;
        if (bpl == 5) {
            dl = (*((rbx + 0xb4)) > 1) ? 1 : 0;
        }
        ecx = edx;
        edx = *((rax + 0xa));
        xmm0 = 0;
        ecx <<= 4;
        edx &= 0xffffffef;
        edx |= ecx;
        *((rax + 0xa)) = dl;
        rax = *((rbx + 0x18));
        *((rax + rsi*8)) = 0xffffffffffffffff;
        rax = *((rbx + 0x10));
        rdx = rax * 3;
        rax = *((rbx + 0x28));
        rax = rax + rdx*8;
        *((rax + 0x10)) = 0;
        __asm ("movups xmmword [rax], xmm0");
        rax = *((rbx + 0x10));
        rdx = rax * 3;
        rax = *((rbx + 0x30));
        rax = rax + rdx*8;
        *((rax + 0x10)) = 0;
        __asm ("movups xmmword [rax], xmm0");
        rax = *((rbx + 0x10));
        rdx = rax + 1;
        *((rbx + 0x10)) = rdx;
label_0:
        return rax;
        rax = 0xaaaaaaaaaaaaaaa;
        r14 = r12 + r12;
        if (r14 > rax) {
            goto label_1;
        }
        rsi <<= 5;
        rax = realloc (*(rdi), r12);
        if (rax == 0) {
            goto label_1;
        }
        *((rbx + 8)) = r14;
        r15 = r12;
        *(rbx) = rax;
        r15 <<= 4;
        rax = realloc (*((rbx + 0x18)), r15);
        r14 = rax;
        if (rax != 0) {
            *((rbx + 0x18)) = rax;
        }
        rax = realloc (*((rbx + 0x20)), r15);
        r15 = rax;
        if (rax != 0) {
            *((rbx + 0x20)) = rax;
        }
        rsi <<= 4;
        *((rsp + 8)) = rsi;
        rax = realloc (*((rbx + 0x28)), r12 + r12*2);
        rsi = *((rsp + 8));
        r12 = rax;
        if (rax != 0) {
            *((rbx + 0x28)) = rax;
        }
        rax = realloc (*((rbx + 0x30)), rsi);
        if (rax == 0) {
            goto label_1;
        }
        *((rbx + 0x30)) = rax;
        if (r14 == 0) {
            goto label_1;
        }
        if (r15 == 0) {
            goto label_1;
        }
        if (r12 == 0) {
            goto label_1;
        }
        rsi = *((rbx + 0x10));
    }
label_1:
    rax = 0xffffffffffffffff;
    goto label_0;
}

/* /tmp/tmpvkjt68yd @ 0x6c70 */
 
int64_t dbg_duplicate_node (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* Idx duplicate_node(re_dfa_t * dfa,Idx org_idx,unsigned int constraint); */
    r13 = rsi;
    r13 <<= 4;
    r12 = rdi;
    ebx = edx;
    rax = *(rdi);
    rax = re_dfa_add_node (rdi, *((rax + r13)), *((rax + r13 + 8)));
    r8 = rax;
    if (rax != -1) {
        rsi = *(r12);
        rdx = rax;
        ecx = ebx;
        rdx <<= 4;
        ecx &= 0x3ff;
        rdx += rsi;
        ecx <<= 8;
        eax = *((rdx + 8));
        eax &= 0xfffc00ff;
        eax |= ecx;
        *((rdx + 8)) = eax;
        bx |= *((rsi + r13 + 9));
        eax &= 0xfff800ff;
        ebx &= 0x3ff;
        ebx <<= 8;
        ebx |= 0x40000;
        ebx |= eax;
        rax = *((r12 + 0x20));
        *((rdx + 8)) = ebx;
        *((rax + r8*8)) = rbp;
    }
    rax = r8;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x6d00 */
 
int64_t dbg_build_wcs_buffer (int64_t arg_1h, int64_t arg1) {
    wchar_t wc;
    unsigned char[64] buf;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_58h;
    rdi = arg1;
    /* void build_wcs_buffer(re_string_t * pstr); */
    rbx = rdi;
    r12 = *((rdi + 0x40));
    rbp = *((rdi + 0x30));
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    rax = *((rdi + 0x58));
    if (r12 > rax) {
        r12 = rax;
    }
    if (r12 <= rbp) {
        goto label_5;
    }
    r15 = rdi + 0x20;
    r14 = rsp + 0xc;
    while (rsi < rbp) {
        r8 = rax*4 - 4;
        memset (rdx + rdi + 4, 0xff, r8);
        if (rbp >= r12) {
            goto label_5;
        }
label_0:
        rcx = *((rbx + 0x78));
        r8 = r12;
        r13 = *((rbx + 0x20));
        r8 -= rbp;
        if (rcx != 0) {
            goto label_7;
        }
        rsi += rbp;
        rsi += *(rbx);
label_2:
        rax = rpl_mbrtowc (r14, *((rbx + 0x28)), r8, r15);
        rdx = rax - 1;
        if (rdx > 0xfffffffffffffffd) {
            goto label_4;
        }
        ecx = *((rsp + 0xc));
        if (rax == 0xfffffffffffffffe) {
            goto label_8;
        }
label_1:
        rdx = *((rbx + 0x10));
        rsi = rbp + 1;
        rdi = rbp*4;
        *((rdx + rbp*4)) = ecx;
        rbp += rax;
    }
    if (rbp < r12) {
        goto label_0;
    }
label_5:
    *((rbx + 0x30)) = rbp;
    *((rbx + 0x38)) = rbp;
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_9;
    }
    return rax;
label_4:
    rax = *(rbx);
    rdx = *((rbx + 0x78));
    rax += rbp;
    rax += *((rbx + 0x28));
    ecx = *(rax);
    *((rsp + 0xc)) = ecx;
    if (rdx != 0) {
        goto label_10;
    }
label_6:
    *((rbx + 0x20)) = r13;
    eax = 1;
    goto label_1;
label_7:
    eax = *((rbx + 0x90));
    rsi = rsp + 0x10;
    if (eax <= 0) {
        goto label_2;
    }
    eax = 0;
    rsi = rsp + 0x10;
    goto label_11;
label_3:
    rax++;
    if (rax == r8) {
        goto label_2;
    }
    rcx = *((rbx + 0x78));
label_11:
    rdx = *(rbx);
    rdx += rax;
    rdx += rbp;
    rdx += *((rbx + 0x28));
    edx = *(rdx);
    edx = *((rcx + rdx));
    rcx = *((rbx + 8));
    rcx += rax;
    *((rcx + rbp)) = dl;
    *((rsi + rax)) = dl;
    edx = rax + 1;
    if (*((rbx + 0x90)) > edx) {
        goto label_3;
    }
    goto label_2;
label_8:
    rax = *((rbx + 0x58));
    if (*((rbx + 0x40)) >= rax) {
        goto label_4;
    }
    *((rbx + 0x20)) = r13;
    goto label_5;
label_10:
    ecx = *((rdx + rcx));
    goto label_6;
label_9:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x6ed0 */
 
int64_t dbg_re_node_set_add_intersect (int64_t arg_8h, int64_t arg_10h, int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* reg_errcode_t re_node_set_add_intersect(re_node_set * dest,re_node_set const * src1,re_node_set const * src2); */
    rcx = *((rsi + 8));
    eax = 0;
    if (rcx == 0) {
        goto label_8;
    }
    r12 = rdx;
    rsi = *((rdx + 8));
    if (rsi == 0) {
        goto label_7;
    }
    rax = *((rdi + 8));
    r8 = *(rdi);
    rdx = rcx + rsi;
    rbx = rdi;
    rdi = *((rdi + 0x10));
    r9 = rdx + rax;
    if (r9 > r8) {
        goto label_9;
    }
label_6:
    r9 = rax + rcx;
    r11 = *((rbp + 0x10));
    rcx--;
    rax--;
    r10 = *((r12 + 0x10));
    r9 += rsi;
    rsi--;
    rdx = *((r11 + rcx*8));
    r8 = *((r10 + rsi*8));
label_1:
    if (rdx == r8) {
        goto label_10;
    }
    do {
        if (rdx >= r8) {
            goto label_11;
        }
        rsi--;
        if (rsi < 0) {
            goto label_12;
        }
label_0:
        r8 = *((r10 + rsi*8));
    } while (rdx != r8);
label_10:
    if (rax >= 0) {
        goto label_13;
    }
    goto label_14;
    do {
        rax--;
        if (rax < 0) {
            goto label_14;
        }
label_13:
    } while (*((rdi + rax*8)) > rdx);
    if (*((rdi + rax*8)) != rdx) {
label_14:
        r9--;
        *((rdi + r9*8)) = rdx;
    }
    rcx--;
    if (rcx < 0) {
        goto label_12;
    }
    rsi--;
    if (rsi < 0) {
        goto label_12;
    }
    rdx = *((r11 + rcx*8));
    goto label_0;
label_3:
    *(r10) = rcx;
    rax--;
    if (rax >= 0) {
        goto label_4;
    }
    do {
label_2:
        rdx <<= 3;
label_5:
        eax = memcpy (rdi, rdi + r9*8, rdx);
        eax = 0;
label_7:
        return rax;
label_11:
        rcx--;
        if (rcx >= 0) {
            rdx = *((r11 + rcx*8));
            goto label_1;
        }
label_12:
        rcx = *((rbx + 8));
        rdx = *((rbp + 8));
        rdx += rcx;
        rdx += *((r12 + 8));
        rax = rcx - 1;
        r8 = rdx - 1;
        rdx -= r9;
        rcx += rdx;
        *((rbx + 8)) = rcx;
    } while (rdx <= 0);
    if (rax < 0) {
        goto label_2;
    }
label_4:
    rsi = *((rdi + r8*8));
    rcx = *((rdi + rax*8));
    r10 = rax + rdx;
    r10 = rdi + r10*8;
    if (rsi <= rcx) {
        goto label_3;
    }
    *(r10) = rsi;
    r8--;
    rdx--;
    if (rdx != 0) {
        goto label_4;
    }
    edx = 0;
    goto label_5;
label_8:
    return rax;
label_9:
    r13 = rdx + r8;
    rax = realloc (rdi, r13*8);
    rdi = rax;
    if (rax != 0) {
        rcx = *((rbp + 8));
        rsi = *((r12 + 8));
        *((rbx + 0x10)) = rax;
        *(rbx) = r13;
        rax = *((rbx + 8));
        goto label_6;
    }
    eax = 0xc;
    goto label_7;
}

/* /tmp/tmpvkjt68yd @ 0x7070 */
 
uint64_t dbg_re_node_set_init_copy (void * s2, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* reg_errcode_t re_node_set_init_copy(re_node_set * dest,re_node_set const * src); */
    rdx = *((rsi + 8));
    rbx = rdi;
    *((rdi + 8)) = rdx;
    if (rdx <= 0) {
        goto label_0;
    }
    *(rdi) = rdx;
    r12 = rdx*8;
    rax = malloc (r12);
    *((rbx + 0x10)) = rax;
    rdi = rax;
    if (rax == 0) {
        goto label_1;
    }
    eax = memcpy (rdi, *((rbp + 0x10)), r12);
    eax = 0;
    do {
        return rax;
label_0:
        xmm0 = 0;
        *((rdi + 0x10)) = 0;
        eax = 0;
        __asm ("movups xmmword [rdi], xmm0");
        return rax;
label_1:
        *((rbx + 8)) = 0;
        eax = 0xc;
        *(rbx) = 0;
    } while (1);
}

/* /tmp/tmpvkjt68yd @ 0x2740 */
 
void malloc (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmpvkjt68yd @ 0x2700 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmpvkjt68yd @ 0x7100 */
 
uint64_t dbg_re_node_set_init_union (int64_t arg_10h, int64_t arg1, int64_t arg2, signed int64_t arg3) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* reg_errcode_t re_node_set_init_union(re_node_set * dest,re_node_set const * src1,re_node_set const * src2); */
    r13 = rdi;
    if (rsi == 0) {
        goto label_3;
    }
    rbx = *((rsi + 8));
    if (rdx == 0) {
        goto label_4;
    }
    if (rbx <= 0) {
        goto label_3;
    }
    r14 = *((rdx + 8));
    *(rsp) = rdx;
    if (r14 <= 0) {
        goto label_5;
    }
    *((rsp + 8)) = rsi;
    *(r13) = rdi;
    rdi <<= 3;
    rax = malloc (rbx + r14);
    rsi = *((rsp + 8));
    rdx = *(rsp);
    *((r13 + 0x10)) = rax;
    rdi = rax;
    if (rax == 0) {
        goto label_6;
    }
    r8 = *((rdx + 0x10));
    r9 = *((rsi + 0x10));
    ebp = 0;
    eax = 0;
    edx = 0;
    while (rcx <= rsi) {
        sil = (rax == 0) ? 1 : 0;
        rdx++;
        esi = (int32_t) sil;
        rax += rsi;
label_0:
        *((rdi + rbp*8 - 8)) = rcx;
        if (rbx <= rdx) {
            goto label_7;
        }
        if (r14 <= rax) {
            goto label_8;
        }
        rcx = *((r9 + rdx*8));
        rsi = *((r8 + rax*8));
        rbp++;
    }
    rax++;
    rcx = rsi;
    goto label_0;
label_4:
    if (rbx <= 0) {
label_3:
        if (rdx != 0) {
            if (*((rdx + 8)) > 0) {
                goto label_9;
            }
        }
        *((r13 + 0x10)) = 0;
        xmm0 = 0;
        eax = 0;
        __asm ("movups xmmword [r13], xmm0");
label_2:
        return rax;
label_9:
        rsi = rdx;
    }
label_5:
    rdi = r13;
    void (*0x7070)() ();
label_7:
    while (1) {
label_1:
        *((r13 + 8)) = rbp;
        eax = 0;
        return rax;
        r14 -= rax;
        rbp += r14;
        memcpy (rdi + rbp*8, r8 + rax*8, r14*8);
    }
label_8:
    rbx -= rdx;
    r8 = rbx*8;
    rbp += rbx;
    memcpy (rdi + rbp*8, r9 + rdx*8, r8);
    goto label_1;
label_6:
    eax = 0xc;
    goto label_2;
}

/* /tmp/tmpvkjt68yd @ 0x7270 */
 
int64_t dbg_build_wcs_upper_buffer (uint32_t arg1) {
    mbstate_t prev_st;
    char[64] buf;
    mbstate_t * var_8h;
    mbstate_t * var_10h;
    void * s2;
    void * var_24h;
    int64_t var_28h;
    wint_t wc;
    mbstate_t * ps;
    char * s;
    int64_t var_88h;
    rdi = arg1;
    /* reg_errcode_t build_wcs_upper_buffer(re_string_t * pstr); */
    r15 = rdi;
    rbx = *((rdi + 0x40));
    r13 = *((rdi + 0x30));
    rax = *(fs:0x28);
    *((rsp + 0x88)) = rax;
    eax = 0;
    rax = *((rdi + 0x58));
    if (rbx > rax) {
        rbx = rax;
    }
    if (*((rdi + 0x8a)) == 0) {
        if (*((rdi + 0x78)) == 0) {
            goto label_16;
        }
    }
label_0:
    r12 = *((r15 + 0x38));
    do {
label_7:
        if (r13 >= rbx) {
label_9:
            *((r15 + 0x30)) = r13;
            eax = 0;
            *((r15 + 0x38)) = r12;
label_5:
            rdx = *((rsp + 0x88));
            rdx -= *(fs:0x28);
            if (rdx != 0) {
                goto label_17;
            }
            return rax;
        }
        rax = r15 + 0x20;
        r14 = rbx;
        *((rsp + 0x28)) = rax;
        rax = rsp + 0x34;
        r14 -= r13;
        *((rsp + 0x10)) = rax;
label_4:
        rax = *((r15 + 0x20));
        rdx = *((r15 + 0x78));
        *((rsp + 0x38)) = rax;
        if (rdx != 0) {
            goto label_18;
        }
        rbp = *((r15 + 0x28));
        rbp += r12;
        rbp += *(r15);
label_3:
        rax = rpl_mbrtowc (*((rsp + 0x10)), rbp, r14, *((rsp + 0x28)));
        r14 = rax;
        rax = rax - 1;
        *(rsp) = rax;
        if (rax > 0xfffffffffffffffc) {
            goto label_19;
        }
        edx = *((rsp + 0x34));
        *((rsp + 8)) = edx;
        eax = towupper (*((rsp + 0x34)));
        edx = *((rsp + 8));
        r8d = eax;
        if (edx == eax) {
            goto label_10;
        }
        r10 = rsp + 0x40;
        esi = eax;
        *((rsp + 0x10)) = eax;
        rdi = r10;
        *((rsp + 8)) = r10;
        rax = wcrtomb (rdi, esi, rsp + 0x38);
        r10 = *((rsp + 8));
        r8d = *((rsp + 0x10));
        rdx = rax;
        if (r14 != rax) {
            goto label_20;
        }
        *((rsp + 8)) = r8d;
        rdi += r13;
        memcpy (*((r15 + 8)), r10, r14);
        r8d = *((rsp + 8));
label_6:
        rax = r14 + r12;
        if (*((r15 + 0x8c)) != 0) {
            goto label_21;
        }
label_8:
        r12 = rax;
        rax = *((r15 + 0x10));
        rdx = r13 + 1;
        rcx = r13*4;
        *((rax + r13*4)) = r8d;
        r13 += r14;
        if (rdx >= r13) {
            goto label_22;
        }
        rdx <<= 2;
        memset (rax + rcx + 4, 0xff, *(rsp));
    } while (1);
label_16:
    if (*((rdi + 0x8c)) != 0) {
        goto label_0;
    }
    if (r13 >= rbx) {
        goto label_23;
    }
    rax = rdi + 0x20;
    *((rsp + 8)) = rax;
    *((rsp + 0x28)) = rax;
    while (r12d != eax) {
        r10 = rsp + 0x40;
        esi = eax;
        *((rsp + 0x24)) = eax;
        rdi = r10;
        *((rsp + 0x18)) = r10;
        rax = wcrtomb (rdi, esi, rsp + 0x38);
        if (rbp != rax) {
            goto label_24;
        }
        rdi += r13;
        memcpy (*((r15 + 8)), *((rsp + 0x18)), rbp);
        ecx = *((rsp + 0x24));
label_1:
        rax = *((r15 + 0x10));
        rdx = r13 + 1;
        rsi = r13*4;
        *((rax + r13*4)) = ecx;
        r13 += rbp;
        if (rdx >= r13) {
            goto label_25;
        }
        rdx <<= 2;
        memset (rax + rsi + 4, 0xff, *(rsp));
label_2:
        if (r13 >= rbx) {
            goto label_23;
        }
        rbp = *((r15 + 0x28));
        *((rsp + 0x18)) = r13;
        rbp += r13;
        rbp += *(r15);
        r14d = *(rbp);
        *((rsp + 0x24)) = r14d;
        r12d = r14d;
        if ((r14b & 0x80) == 0) {
            eax = mbsinit (*((rsp + 8)));
            if (eax == 0) {
                goto label_26;
            }
            eax = towupper (r14d);
            if ((eax & 0xffffff80) == 0) {
                goto label_27;
            }
        }
label_26:
        rax = *((r15 + 0x20));
        r14 = rbx;
        r14 -= r13;
        *((rsp + 0x38)) = rax;
        rax = rsp + 0x34;
        rdi = rax;
        *((rsp + 0x10)) = rax;
        rax = rpl_mbrtowc (rdi, rbp, r14, *((rsp + 8)));
        rax = rax - 1;
        *(rsp) = rax;
        if (rax > 0xfffffffffffffffc) {
            goto label_28;
        }
        r12d = *((rsp + 0x34));
        eax = towupper (*((rsp + 0x34)));
    }
    *((rsp + 0x10)) = eax;
    rdi += r13;
    rsi += r13;
    rsi += *(r15);
    al = memcpy (*((r15 + 8)), *((r15 + 0x28)), rbp);
    ecx = *((rsp + 0x10));
    goto label_1;
label_27:
    rdx = *((r15 + 8));
    *((rdx + r13)) = al;
    rdx = *((r15 + 0x10));
    *((rdx + r13*4)) = eax;
    r13++;
    goto label_2;
label_28:
    if (*(rsp) == 0xfffffffffffffffd) {
        goto label_29;
    }
    rax = *((r15 + 8));
    rcx = *((rsp + 0x18));
    esi = *((rsp + 0x24));
    *((rax + r13)) = r12b;
    rax = *((r15 + 0x10));
    r13++;
    *((rax + rcx*4)) = esi;
    if (rbp != -1) {
        goto label_2;
    }
    rax = *((rsp + 0x38));
    *((r15 + 0x20)) = rax;
    goto label_2;
label_25:
    r13 = rdx;
    goto label_2;
label_29:
    rax = *((r15 + 0x58));
    if (*((r15 + 0x40)) < rax) {
        goto label_30;
    }
    rax = *((r15 + 8));
    rcx = *((rsp + 0x18));
    esi = *((rsp + 0x24));
    *((rax + r13)) = r12b;
    rax = *((r15 + 0x10));
    r13++;
    *((rax + rcx*4)) = esi;
    goto label_2;
label_18:
    eax = *((r15 + 0x90));
    if (eax <= 0) {
        goto label_31;
    }
    rsi = *((r15 + 0x28));
    edi = rax - 1;
    r10 = rsp + 0x40;
    eax = 0;
    rsi += r12;
    rsi += *(r15);
    while (rdi != rax) {
        rax++;
        if (r14 == rax) {
            goto label_13;
        }
        ecx = *((rsi + rax));
        ecx = *((rdx + rcx));
        *((r10 + rax)) = cl;
    }
label_13:
    goto label_3;
label_24:
    r12 = r13;
    goto label_4;
label_30:
    rax = *((rsp + 0x38));
    *((r15 + 0x20)) = rax;
label_23:
    *((r15 + 0x30)) = r13;
    eax = 0;
    *((r15 + 0x38)) = r13;
    goto label_5;
label_10:
    *((rsp + 8)) = r8d;
    rdi += r13;
    memcpy (*((r15 + 8)), rbp, r14);
    r8d = *((rsp + 8));
    goto label_6;
label_19:
    if (*(rsp) == 0xfffffffffffffffd) {
        rax = *((r15 + 0x58));
        if (*((r15 + 0x40)) < rax) {
            goto label_11;
        }
    }
    rax = *(r15);
    rdx = *((r15 + 0x78));
    rax += r12;
    rax += *((r15 + 0x28));
    eax = *(rax);
    if (rdx != 0) {
        goto label_32;
    }
label_15:
    rdx = *((r15 + 8));
    rcx = r13;
    *((rdx + r13)) = al;
    if (*((r15 + 0x8c)) != 0) {
        goto label_33;
    }
label_14:
    rdx = *((r15 + 0x10));
    r12++;
    r13++;
    *((rdx + rcx*4)) = eax;
    if (r14 != -1) {
        goto label_7;
    }
    rax = *((rsp + 0x38));
    *((r15 + 0x20)) = rax;
    goto label_7;
label_21:
    rdx = *((r15 + 0x18));
    rcx = r13;
    rcx -= r12;
    rdx = rdx + rcx*8;
    do {
        *((rdx + r12*8)) = r12;
        r12++;
    } while (r12 != rax);
    goto label_8;
label_22:
    r13 = rdx;
    goto label_7;
label_11:
    rax = *((rsp + 0x38));
    *((r15 + 0x20)) = rax;
    goto label_9;
label_20:
    if (rax == -1) {
        goto label_10;
    }
    rbp = rax + r13;
    rax = *((r15 + 0x40));
    if (rbp > rax) {
        goto label_11;
    }
    if (*((r15 + 0x18)) == 0) {
        goto label_34;
    }
label_12:
    if (*((r15 + 0x8c)) != 0) {
        goto label_35;
    }
    if (r13 == 0) {
        goto label_36;
    }
    rcx = *((r15 + 0x18));
    eax = 0;
    do {
        *((rcx + rax*8)) = rax;
        rax++;
    } while (rax != r13);
label_36:
    *((r15 + 0x8c)) = 1;
label_35:
    *((rsp + 0x10)) = r8d;
    *((rsp + 8)) = rdx;
    rdi += r13;
    memcpy (*((r15 + 8)), r10, rdx);
    rax = *((r15 + 0x10));
    r8d = *((rsp + 0x10));
    rdx = *((rsp + 8));
    rdi = rax + r13*4;
    rax = *((r15 + 0x18));
    *(rdi) = r8d;
    rsi = rax + r13*8;
    eax = 1;
    *(rsi) = r12;
    if (rdx <= 1) {
        goto label_37;
    }
    do {
        rcx = *(rsp);
        if (rax < r14) {
            rcx = rax;
        }
        rcx += r12;
        *((rsi + rax*8)) = rcx;
        *((rdi + rax*4)) = 0xffffffff;
        rax++;
    } while (rax != rdx);
label_37:
    rbx = *((r15 + 0x58));
    rdx -= r14;
    rbx += rdx;
    *((r15 + 0x58)) = rbx;
    if (*((r15 + 0x60)) > r12) {
        *((r15 + 0x68)) += rdx;
    }
    rax = *((r15 + 0x40));
    r13 = rbp;
    if (rbx > rax) {
        rbx = rax;
    }
    r12 += r14;
    goto label_7;
label_34:
    *((rsp + 0x18)) = r10;
    *((rsp + 0x10)) = rdx;
    *((rsp + 8)) = r8d;
    rax = malloc (rax*8);
    r8d = *((rsp + 8));
    rdx = *((rsp + 0x10));
    *((r15 + 0x18)) = rax;
    r10 = *((rsp + 0x18));
    if (rax != 0) {
        goto label_12;
    }
    eax = 0xc;
    goto label_5;
label_31:
    r10 = rsp + 0x40;
    goto label_13;
label_17:
    stack_chk_fail ();
label_33:
    rdx = *((r15 + 0x18));
    *((rdx + r13*8)) = r12;
    goto label_14;
label_32:
    eax = *((rdx + rax));
    goto label_15;
}

/* /tmp/tmpvkjt68yd @ 0x78a0 */
 
int64_t dbg_build_upper_buffer (int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_78h, int64_t arg1) {
    rdi = arg1;
    /* void build_upper_buffer(re_string_t * pstr); */
    rax = *((rdi + 0x58));
    r12 = *((rdi + 0x40));
    rbx = *((rdi + 0x30));
    if (r12 > rax) {
        r12 = rax;
    }
    if (r12 <= rbx) {
        goto label_1;
    }
    rax = ctype_toupper_loc ();
    do {
        rdx = *(rbp);
        rcx = *((rbp + 0x78));
        rdx += rbx;
        rdx += *((rbp + 0x28));
        edx = *(rdx);
        if (rcx != 0) {
            goto label_2;
        }
label_0:
        rsi = *(rax);
        rcx = *((rbp + 8));
        edx = *((rsi + rdx*4));
        *((rcx + rbx)) = dl;
        rbx++;
    } while (r12 != rbx);
    *((rbp + 0x30)) = r12;
    *((rbp + 0x38)) = r12;
    return rax;
label_2:
    edx = *((rcx + rdx));
    goto label_0;
label_1:
    r12 = rbx;
    *((rbp + 0x30)) = r12;
    *((rbp + 0x38)) = r12;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x7920 */
 
int64_t dbg_free_state (int64_t arg_8h, void * arg_18h, void * arg_48h, void * arg_50h, void * arg_58h, void * arg_60h, void ** ptr) {
    rdi = ptr;
    /* void free_state(re_dfastate_t * state); */
    free (*((rdi + 0x30)));
    free (*((rbp + 0x48)));
    rax = *((rbp + 0x50));
    rdx = rbp + 8;
    if (rax != rdx) {
        free (*((rax + 0x10)));
        free (*((rbp + 0x50)));
    }
    free (*((rbp + 0x18)));
    free (*((rbp + 0x60)));
    free (*((rbp + 0x58)));
    rdi = rbp;
    return free ();
}

/* /tmp/tmpvkjt68yd @ 0x24d0 */
 
void free (void) {
    __asm ("bnd jmp qword [reloc.free]");
}

/* /tmp/tmpvkjt68yd @ 0x7980 */
 
int64_t dbg_match_ctx_clean (void * ptr, signed int64_t arg1) {
    rdi = arg1;
    /* void match_ctx_clean(re_match_context_t * mctx); */
    r14 = rdi;
    if (*((rdi + 0xe8)) <= 0) {
        goto label_1;
    }
    r13d = 0;
label_0:
    rax = *((r14 + 0xf8));
    r12 = *((rax + r13*8));
    if (*((r12 + 0x20)) <= 0) {
        goto label_2;
    }
    ebx = 0;
    do {
        rax = *((r12 + 0x28));
        rbp = *((rax + rbx*8));
        rbx++;
        free (*((rbp + 0x20)));
        free (rbp);
    } while (*((r12 + 0x20)) > rbx);
label_2:
    free (*((r12 + 0x28)));
    rax = *((r12 + 0x10));
    if (rax != 0) {
        free (*((rax + 0x10)));
        free (*((r12 + 0x10)));
    }
    r13++;
    free (r12);
    if (*((r14 + 0xe8)) > r13) {
        goto label_0;
    }
label_1:
    *((r14 + 0xe8)) = 0;
    *((r14 + 0xc8)) = 0;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x7a40 */
 
int64_t dbg_re_node_set_merge (int64_t arg_8h, int64_t arg_10h, int64_t arg2, void ** ptr) {
    rsi = arg2;
    rdi = ptr;
    /* reg_errcode_t re_node_set_merge(re_node_set * dest,re_node_set const * src); */
    if (rsi == 0) {
        goto label_6;
    }
    r12 = rsi;
    rcx = *((rsi + 8));
    rsi = *((rsi + 8));
    if (rcx == 0) {
        goto label_4;
    }
    rax = *((rdi + 8));
    rdx = *(rdi);
    rbx = rax + rcx*2;
    if (rdx < rbx) {
        goto label_7;
    }
    if (rax == 0) {
        goto label_8;
    }
label_3:
    rdx = rcx - 1;
    rax--;
    rcx = -rcx;
    if (rax >= 0) {
        goto label_9;
    }
    if (rax < 0) {
        goto label_9;
    }
    rsi = *((rbp + 0x10));
    rdi = *((r12 + 0x10));
    if (rax >= 0) {
        while (*((rsi + rax*8)) != rcx) {
            goto label_10;
        }
        rbx--;
        rdx--;
        *((rsi + rbx*8)) = rcx;
label_1:
        rcx = rax;
        rcx |= rdx;
        if (rcx < 0) {
            goto label_9;
        }
label_0:
        rcx = *((rdi + rdx*8));
    }
    rax--;
    rdx--;
    rcx = rax;
    rcx |= rdx;
    if (rcx >= 0) {
        goto label_0;
    }
label_9:
    if (rdx >= 0) {
        rdx++;
        rax = *((rbp + 0x10));
        rbx -= rdx;
        rdx <<= 3;
        memcpy (rax + rbx*8, *((r12 + 0x10)), rdx);
    }
    rcx = *((rbp + 8));
    rax = *((r12 + 8));
    r8 = rcx + rax*2 - 1;
    rdx = r8;
    rdx -= rbx;
    rdx++;
    if (rdx == 0) {
        goto label_4;
    }
    rax = rcx - 1;
    rcx += rdx;
    rdi = *((rbp + 0x10));
    *((rbp + 8)) = rcx;
    do {
label_2:
        rsi = *((rdi + r8*8));
        rcx = *((rdi + rax*8));
        r9 = rax + rdx;
        r9 = rdi + r9*8;
        if (rsi <= rcx) {
            goto label_11;
        }
        *(r9) = rsi;
        r8--;
        rdx--;
    } while (rdx != 0);
label_4:
    eax = 0;
label_5:
    return rax;
label_10:
    rax--;
    goto label_1;
label_11:
    *(r9) = rcx;
    rax--;
    if (rax >= 0) {
        goto label_2;
    }
    rdx <<= 3;
    eax = memcpy (rdi, rdi + rbx*8, rdx);
    eax = 0;
    return rax;
label_7:
    rbx = rsi + rsi;
    rsi <<= 4;
    rax = realloc (*((rdi + 0x10)), rcx + rdx);
    rdi = rax;
    if (rax == 0) {
        goto label_12;
    }
    *((rbp + 0x10)) = rax;
    rax = *((rbp + 8));
    *(rbp) = rbx;
    if (rax == 0) {
        goto label_13;
    }
    rcx = *((r12 + 8));
    rbx = rax + rcx*2;
    goto label_3;
label_6:
    eax = 0;
    return rax;
label_8:
    do {
        *((rbp + 8)) = rsi;
        memcpy (*((rdi + 0x10)), *((r12 + 0x10)), rsi*8);
        goto label_4;
label_12:
        eax = 0xc;
        goto label_5;
label_13:
        rsi = *((r12 + 8));
    } while (1);
}

/* /tmp/tmpvkjt68yd @ 0x7c00 */
 
int64_t dbg_re_string_context_at (int64_t arg_70h, uint32_t arg_8dh, uint32_t arg_8eh, int32_t arg1, int64_t arg2, uint32_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* unsigned int re_string_context_at(re_string_t const * input,Idx idx,int eflags); */
    if (rsi < 0) {
        goto label_2;
    }
    if (*((rdi + 0x58)) == rsi) {
        goto label_3;
    }
    if (*((rdi + 0x90)) <= 1) {
        goto label_4;
    }
    rax = *((rdi + 0x10));
    while (ebx == 0xffffffff) {
        rsi--;
        if (rsi < 0) {
            goto label_2;
        }
        ebx = *((rax + rsi*4));
    }
    if (*((rbp + 0x8e)) != 0) {
        goto label_5;
    }
label_1:
    eax = 0;
    if (ebx == 0xa) {
        goto label_6;
    }
    do {
label_0:
        return rax;
label_4:
        rax = *((rdi + 8));
        rdx = *((rdi + 0x80));
        eax = *((rax + rsi));
        rcx = rax;
        rax >>= 6;
        rax = *((rdx + rax*8));
        rax >>= cl;
        rdx = rax;
        eax = 1;
        edx &= 1;
    } while (edx != 0);
    eax = 0;
    if (cl != 0xa) {
        goto label_0;
    }
label_6:
    eax = 0;
    al = (*((rbp + 0x8d)) != 0) ? 1 : 0;
    eax += eax;
    return rax;
label_2:
    eax = *((rbp + 0x70));
    return rax;
label_3:
    edx &= 2;
    eax -= eax;
    eax &= 2;
    eax += 8;
    goto label_0;
label_5:
    eax = iswalnum (ebx);
    if (eax != 0) {
        goto label_7;
    }
    if (ebx != 0x5f) {
        goto label_1;
    }
label_7:
    eax = 1;
    goto label_0;
}

/* /tmp/tmpvkjt68yd @ 0x7ce0 */
 
int64_t dbg_check_node_accept (uint32_t arg1, uint32_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* _Bool check_node_accept(re_match_context_t const * mctx,re_token_t const * node,Idx idx); */
    rax = *((rdi + 8));
    r8 = rdx;
    ecx = *((rax + rdx));
    eax = *((rsi + 8));
    if (al == 5) {
        goto label_3;
    }
    if (al > 5) {
        goto label_4;
    }
    if (al == 1) {
        goto label_5;
    }
    if (al != 3) {
        goto label_6;
    }
    rdx = *(rsi);
    eax = (int32_t) cl;
    rax >>= 6;
    rax = *((rdx + rax*8));
    rax >>= cl;
    eax &= 1;
    if (eax == 0) {
        goto label_7;
    }
    do {
label_0:
        ebx = *((rsi + 8));
        eax = 1;
        if ((ebx & 0x3ff00) != 0) {
            ebx >>= 8;
            eax = re_string_context_at (rdi, r8, *((rdi + 0xa0)), rcx, r8, r9);
            ecx = ebx;
            edx = eax;
            cx &= 0x3ff;
            if ((bl & 4) != 0) {
                goto label_8;
            }
            ebx &= 8;
            if (ebx != 0) {
                if ((al & 1) != 0) {
                    goto label_9;
                }
            }
label_2:
            if ((cl & 0x20) != 0) {
                eax = 0;
                if ((dl & 2) == 0) {
                    goto label_1;
                }
            }
            edx >>= 3;
            eax = 1;
            edx &= 1;
            ecx &= 0x80;
            if (ecx == 0) {
                eax = edx;
                goto label_1;
            }
        }
label_1:
        return rax;
label_4:
        eax = 0;
        if (al != 7) {
            goto label_10;
        }
        if (cl < 0) {
            goto label_11;
        }
label_3:
        if (cl == 0xa) {
            goto label_12;
        }
    } while (cl != 0);
    rdx = *((rdi + 0x98));
    eax = 0;
    if ((*((rdx + 0xd8)) & 0x80) == 0) {
        goto label_0;
    }
label_11:
    return rax;
label_10:
    return rax;
label_5:
    eax = 0;
    if (*(rsi) == cl) {
        goto label_0;
    }
    return rax;
label_8:
    eax = 0;
    if ((dl & 1) == 0) {
        goto label_1;
    }
    ebx &= 8;
    if (ebx == 0) {
        goto label_2;
    }
    return rax;
label_7:
    return rax;
label_12:
    rdx = *((rdi + 0x98));
    eax = 0;
    if ((*((rdx + 0xd8)) & 0x40) != 0) {
        goto label_0;
    }
    return rax;
label_9:
    eax = 0;
    return rax;
label_6:
    eax = 0;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x7e20 */
 
int64_t dbg_check_subexp_matching_top (signed int64_t arg_8h, int64_t arg_10h, int64_t arg1, uint32_t arg2, int64_t arg3) {
    void ** var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* reg_errcode_t check_subexp_matching_top(re_match_context_t * mctx,re_node_set * cur_nodes,Idx str_idx); */
    r14 = *((rdi + 0x98));
    if (*((rsi + 8)) <= 0) {
        goto label_4;
    }
    r15 = rdi;
    r13 = rdx;
    ebx = 0;
    while (*((rax + 8)) != 8) {
label_0:
        rbx++;
        if (*((rbp + 8)) <= rbx) {
            goto label_4;
        }
label_1:
        rax = *((rbp + 0x10));
        r12 = *((rax + rbx*8));
        rax = *((rax + rbx*8));
        rax <<= 4;
        rax += *(r14);
    }
    rax = *(rax);
    if (rax > 0x3f) {
        goto label_0;
    }
    rdx = *((r14 + 0xa0));
    if (((rdx >> rax) & 1) >= 0) {
        goto label_0;
    }
    r8 = *((r15 + 0xf8));
    rdx = *((r15 + 0xe8));
    if (*((r15 + 0xf0)) == rdx) {
        goto label_5;
    }
label_3:
    *((rsp + 8)) = rdx;
    *(rsp) = r8;
    rax = calloc (1, 0x30);
    r8 = *(rsp);
    rdx = *((rsp + 8));
    *((r8 + rdx*8)) = rax;
    if (rax == 0) {
        goto label_2;
    }
    rdx++;
    *((rax + 8)) = r12;
    rbx++;
    *((r15 + 0xe8)) = rdx;
    *(rax) = r13;
    if (*((rbp + 8)) > rbx) {
        goto label_1;
    }
label_4:
    eax = 0;
    do {
        return rax;
label_2:
        eax = 0xc;
    } while (1);
label_5:
    rax = rdx + rdx;
    rsi <<= 4;
    *(rsp) = rax;
    rax = realloc (r8, rdx);
    r8 = rax;
    if (rax == 0) {
        goto label_2;
    }
    *((r15 + 0xf8)) = rax;
    rax = *(rsp);
    rdx = *((r15 + 0xe8));
    *((r15 + 0xf0)) = rax;
    goto label_3;
}

/* /tmp/tmpvkjt68yd @ 0x7f50 */
 
int64_t dbg_pop_fail_stack (size_t * n, int64_t arg1, void * arg2, size_t arg3, int64_t arg5, int64_t arg6, void ** s1) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r8 = arg5;
    r9 = arg6;
    rcx = s1;
    /* Idx pop_fail_stack(re_fail_stack_t * fs,Idx * pidx,Idx nregs,regmatch_t * regs,regmatch_t * prevregs,re_node_set * eps_via_nodes); */
    if (rdi == 0) {
        goto label_0;
    }
    rax = *(rdi);
    if (rax == 0) {
        goto label_0;
    }
    rax--;
    r12 = rdx;
    r13 = r9;
    *(rbp) = rax;
    rbx = rax * 3;
    rax = *((rbp + 0x10));
    r12 <<= 4;
    rbx <<= 4;
    r14 = r8;
    rax += rbx;
    rdx = *(rax);
    *(rsi) = rdx;
    memcpy (rcx, *((rax + 0x10)), r12);
    rax = *((rbp + 0x10));
    rsi += r12;
    memcpy (r14, *((rax + rbx + 0x10)), r12);
    free (*((r13 + 0x10)));
    rax = *((rbp + 0x10));
    free (*((rax + rbx + 0x10)));
    rbx += *((rbp + 0x10));
    __asm ("movdqu xmm0, xmmword [rbx + 0x18]");
    __asm ("movups xmmword [r13], xmm0");
    rax = *((rbx + 0x28));
    *((r13 + 0x10)) = rax;
    rax = *((rbx + 8));
    do {
        return rax;
label_0:
        rax = 0xffffffffffffffff;
    } while (1);
}

/* /tmp/tmpvkjt68yd @ 0x8010 */
 
uint64_t dbg_build_charclass (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg6, char * s1) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r9 = arg6;
    r8 = s1;
    /* reg_errcode_t build_charclass(unsigned char * trans,bitset_word_t * sbcset,re_charset_t * mbcset,Idx * char_class_alloc,char const * class_name,reg_syntax_t syntax); */
    r14 = rcx;
    r13 = rdx;
    r12 = r8;
    rbx = rsi;
    if ((r9d & 0x400000) != 0) {
        eax = strcmp (r8, "upper");
        if (eax != 0) {
            goto label_3;
        }
        r12 = 0x00016968;
    }
    rdx = *((r13 + 0x48));
    rdi = *((r13 + 0x18));
    if (*(r14) == rdx) {
        goto label_4;
    }
label_1:
    rax = rdx + 1;
    *((r13 + 0x48)) = rax;
    r13 = rdi + rdx*8;
    rax = wctype (r12);
    *(r13) = rax;
    eax = strcmp (r12, "alnum");
    if (eax == 0) {
        goto label_5;
    }
    eax = strcmp (r12, "cntrl");
    if (eax != 0) {
        goto label_6;
    }
    rax = ctype_b_loc ();
    ecx = 0;
    edi = 1;
    rdx = *(rax);
    if (rbp != 0) {
        goto label_7;
    }
    do {
        if ((*((rdx + rcx*2)) & 2) != 0) {
            rax = rcx;
            rsi = rdi;
            rax >>= 6;
            rsi <<= cl;
            *((rbx + rax*8)) |= rsi;
        }
        rcx++;
    } while (rcx != 0x100);
label_0:
    eax = 0;
label_2:
    return rax;
label_5:
    rax = ctype_b_loc ();
    ecx = 0;
    edi = 1;
    rdx = *(rax);
    if (rbp != 0) {
        goto label_8;
    }
    do {
        if ((*((rdx + rcx*2)) & 8) != 0) {
            rax = rcx;
            rsi = rdi;
            rax >>= 6;
            rsi <<= cl;
            *((rbx + rax*8)) |= rsi;
        }
        rcx++;
    } while (rcx != 0x100);
    goto label_0;
label_3:
    eax = strcmp (r12, "lower");
    rdx = *((r13 + 0x48));
    rdi = *((r13 + 0x18));
    rax = 0x00016968;
    if (eax == 0) {
        r12 = rax;
    }
    if (*(r14) != rdx) {
        goto label_1;
    }
label_4:
    r15 = rdx + rdx + 1;
    rax = realloc (rdi, r15*8);
    rdi = rax;
    if (rax == 0) {
        goto label_9;
    }
    *((r13 + 0x18)) = rax;
    *(r14) = r15;
    rdx = *((r13 + 0x48));
    goto label_1;
label_6:
    eax = strcmp (r12, "lower");
    if (eax == 0) {
        goto label_10;
    }
    eax = strcmp (r12, "space");
    if (eax != 0) {
        goto label_11;
    }
    rax = ctype_b_loc ();
    ecx = 0;
    edi = 1;
    rdx = *(rax);
    if (rbp != 0) {
        goto label_12;
    }
    do {
        if ((*((rdx + rcx*2 + 1)) & 0x20) != 0) {
            rax = rcx;
            rsi = rdi;
            rax >>= 6;
            rsi <<= cl;
            *((rbx + rax*8)) |= rsi;
        }
        rcx++;
    } while (rcx != 0x100);
    goto label_0;
label_10:
    rax = ctype_b_loc ();
    ecx = 0;
    edi = 1;
    rdx = *(rax);
    if (rbp != 0) {
        goto label_13;
    }
    do {
        if ((*((rdx + rcx*2 + 1)) & 2) != 0) {
            rax = rcx;
            rsi = rdi;
            rax >>= 6;
            rsi <<= cl;
            *((rbx + rax*8)) |= rsi;
        }
        rcx++;
    } while (rcx != 0x100);
    goto label_0;
label_11:
    eax = strcmp (r12, 0x00016968);
    if (eax == 0) {
        goto label_14;
    }
    eax = strcmp (r12, 0x000169a5);
    if (eax != 0) {
        goto label_15;
    }
    rax = ctype_b_loc ();
    ecx = 0;
    edi = 1;
    rdx = *(rax);
    if (rbp != 0) {
        goto label_16;
    }
    do {
        if ((*((rdx + rcx*2 + 1)) & 8) != 0) {
            rax = rcx;
            rsi = rdi;
            rax >>= 6;
            rsi <<= cl;
            *((rbx + rax*8)) |= rsi;
        }
        rcx++;
    } while (rcx != 0x100);
    goto label_0;
label_14:
    rax = ctype_b_loc ();
    ecx = 0;
    edi = 1;
    rdx = *(rax);
    if (rbp != 0) {
        goto label_17;
    }
    do {
        if ((*((rdx + rcx*2 + 1)) & 4) != 0) {
            rax = rcx;
            rsi = rdi;
            rax >>= 6;
            rsi <<= cl;
            *((rbx + rax*8)) |= rsi;
        }
        rcx++;
    } while (rcx != 0x100);
    goto label_0;
label_8:
    eax = 0;
    do {
        if ((*((rdx + rax*2)) & 8) != 0) {
            ecx = *((rbp + rax));
            r10 = rdi;
            rsi = rcx;
            r10 <<= cl;
            rsi >>= 3;
            esi &= 0x18;
            *((rbx + rsi)) |= r10;
        }
        rax++;
    } while (rax != 0x100);
    goto label_0;
label_7:
    eax = 0;
    do {
        if ((*((rdx + rax*2)) & 2) != 0) {
            ecx = *((rbp + rax));
            r11 = rdi;
            rsi = rcx;
            r11 <<= cl;
            rsi >>= 3;
            esi &= 0x18;
            *((rbx + rsi)) |= r11;
        }
        rax++;
    } while (rax != 0x100);
    goto label_0;
label_13:
    eax = 0;
    do {
        if ((*((rdx + rax*2 + 1)) & 2) != 0) {
            ecx = *((rbp + rax));
            r15 = rdi;
            rsi = rcx;
            r15 <<= cl;
            rsi >>= 3;
            esi &= 0x18;
            *((rbx + rsi)) |= r15;
        }
        rax++;
    } while (rax != 0x100);
    goto label_0;
label_15:
    eax = strcmp (r12, "print");
    if (eax != 0) {
        goto label_18;
    }
    rax = ctype_b_loc ();
    ecx = 0;
    edi = 1;
    rdx = *(rax);
    if (rbp != 0) {
        goto label_19;
    }
    do {
        if ((*((rdx + rcx*2 + 1)) & 0x40) != 0) {
            rax = rcx;
            rsi = rdi;
            rax >>= 6;
            rsi <<= cl;
            *((rbx + rax*8)) |= rsi;
        }
        rcx++;
    } while (rcx != 0x100);
    goto label_0;
label_18:
    eax = strcmp (r12, "upper");
    if (eax != 0) {
        goto label_20;
    }
    rax = ctype_b_loc ();
    ecx = 0;
    edi = 1;
    rax = *(rax);
    if (rbp != 0) {
        goto label_21;
    }
    do {
        if ((*((rax + rcx*2 + 1)) & 1) != 0) {
            rdx = rcx;
            rsi = rdi;
            rdx >>= 6;
            rsi <<= cl;
            *((rbx + rdx*8)) |= rsi;
        }
        rcx++;
    } while (rcx != 0x100);
    goto label_0;
label_9:
    eax = 0xc;
    goto label_2;
label_20:
    eax = strcmp (r12, "blank");
    if (eax != 0) {
        goto label_22;
    }
    rax = ctype_b_loc ();
    ecx = 0;
    esi = 1;
    rdx = *(rax);
    if (rbp != 0) {
        goto label_23;
    }
    do {
        if ((*((rdx + rcx*2)) & 1) != 0) {
            rax = rcx;
            rdi = rsi;
            rax >>= 6;
            rdi <<= cl;
            *((rbx + rax*8)) |= rdi;
        }
        rcx++;
    } while (rcx != 0x100);
    goto label_0;
label_17:
    eax = 0;
    do {
        if ((*((rdx + rax*2 + 1)) & 4) != 0) {
            ecx = *((rbp + rax));
            r14 = rdi;
            rsi = rcx;
            r14 <<= cl;
            rsi >>= 3;
            esi &= 0x18;
            *((rbx + rsi)) |= r14;
        }
        rax++;
    } while (rax != 0x100);
    goto label_0;
label_12:
    eax = 0;
    do {
        if ((*((rdx + rax*2 + 1)) & 0x20) != 0) {
            ecx = *((rbp + rax));
            r15 = rdi;
            rsi = rcx;
            r15 <<= cl;
            rsi >>= 3;
            esi &= 0x18;
            *((rbx + rsi)) |= r15;
        }
        rax++;
    } while (rax != 0x100);
    goto label_0;
label_16:
    eax = 0;
    do {
        if ((*((rdx + rax*2 + 1)) & 8) != 0) {
            ecx = *((rbp + rax));
            r14 = rdi;
            rsi = rcx;
            r14 <<= cl;
            rsi >>= 3;
            esi &= 0x18;
            *((rbx + rsi)) |= r14;
        }
        rax++;
    } while (rax != 0x100);
    goto label_0;
label_22:
    eax = strcmp (r12, "graph");
    if (eax != 0) {
        goto label_24;
    }
    rax = ctype_b_loc ();
    ecx = 0;
    edx = 1;
    rsi = *(rax);
    if (rbp != 0) {
        goto label_25;
    }
    do {
        if (*((rsi + rcx*2)) < 0) {
            rax = rcx;
            rdi = rdx;
            rax >>= 6;
            rdi <<= cl;
            *((rbx + rax*8)) |= rdi;
        }
        rcx++;
    } while (rcx != 0x100);
    goto label_0;
label_25:
    eax = 0;
    edi = 1;
    do {
        if (*((rsi + rax*2)) < 0) {
            ecx = *((rbp + rax));
            r14 = rdi;
            rdx = rcx;
            r14 <<= cl;
            rdx >>= 3;
            edx &= 0x18;
            *((rbx + rdx)) |= r14;
        }
        rax++;
    } while (rax != 0x100);
    goto label_0;
label_24:
    eax = strcmp (r12, "punct");
    if (eax != 0) {
        goto label_26;
    }
    rax = ctype_b_loc ();
    ecx = 0;
    edx = 1;
    rsi = *(rax);
    if (rbp != 0) {
        goto label_27;
    }
    do {
        if ((*((rsi + rcx*2)) & 4) != 0) {
            rax = rcx;
            rdi = rdx;
            rax >>= 6;
            rdi <<= cl;
            *((rbx + rax*8)) |= rdi;
        }
        rcx++;
    } while (rcx != 0x100);
    goto label_0;
label_27:
    eax = 0;
    edi = 1;
    do {
        if ((*((rsi + rax*2)) & 4) != 0) {
            ecx = *((rbp + rax));
            r8 = rdi;
            rdx = rcx;
            r8 <<= cl;
            rdx >>= 3;
            edx &= 0x18;
            *((rbx + rdx)) |= r8;
        }
        rax++;
    } while (rax != 0x100);
    goto label_0;
label_26:
    eax = strcmp (r12, "xdigit");
    if (eax != 0) {
        goto label_28;
    }
    rax = ctype_b_loc ();
    ecx = 0;
    edx = 1;
    rsi = *(rax);
    if (rbp != 0) {
        goto label_29;
    }
    do {
        if ((*((rsi + rcx*2 + 1)) & 0x10) != 0) {
            rax = rcx;
            rdi = rdx;
            rax >>= 6;
            rdi <<= cl;
            *((rbx + rax*8)) |= rdi;
        }
        rcx++;
    } while (rcx != 0x100);
    goto label_0;
label_29:
    eax = 0;
    edi = 1;
    do {
        if ((*((rsi + rax*2 + 1)) & 0x10) != 0) {
            ecx = *((rbp + rax));
            r9 = rdi;
            rdx = rcx;
            r9 <<= cl;
            rdx >>= 3;
            edx &= 0x18;
            *((rbx + rdx)) |= r9;
        }
        rax++;
    } while (rax != 0x100);
    goto label_0;
label_28:
    eax = 4;
    goto label_2;
label_19:
    eax = 0;
    do {
        if ((*((rdx + rax*2 + 1)) & 0x40) != 0) {
            ecx = *((rbp + rax));
            r14 = rdi;
            rsi = rcx;
            r14 <<= cl;
            rsi >>= 3;
            esi &= 0x18;
            *((rbx + rsi)) |= r14;
        }
        rax++;
    } while (rax != 0x100);
    goto label_0;
label_23:
    eax = 0;
    edi = 1;
    do {
        if ((*((rdx + rax*2)) & 1) != 0) {
            ecx = *((rbp + rax));
            r15 = rdi;
            rsi = rcx;
            r15 <<= cl;
            rsi >>= 3;
            esi &= 0x18;
            *((rbx + rsi)) |= r15;
        }
        rax++;
    } while (rax != 0x100);
    goto label_0;
label_21:
    edx = 0;
    do {
        if ((*((rax + rdx*2 + 1)) & 1) != 0) {
            ecx = *((rbp + rdx));
            r14 = rdi;
            rsi = rcx;
            r14 <<= cl;
            rsi >>= 3;
            esi &= 0x18;
            *((rbx + rsi)) |= r14;
        }
        rdx++;
    } while (rdx != 0x100);
    goto label_0;
}

/* /tmp/tmpvkjt68yd @ 0x87a0 */
 
int64_t dbg_re_string_reconstruct (signed int64_t arg1, signed int64_t arg2, int64_t arg3) {
    wchar_t wc2;
    mbstate_t cur_state;
    unsigned char[6] buf;
    int64_t var_1h;
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_24h;
    int64_t var_28h;
    int64_t var_32h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* reg_errcode_t re_string_reconstruct(re_string_t * pstr,Idx idx,int eflags); */
    r8d = edx;
    r12 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    rax = *((rdi + 0x28));
    rbp -= rax;
    if (rax <= rsi) {
        goto label_18;
    }
    if (*((rdi + 0x90)) > 1) {
        *((rdi + 0x20)) = 0;
    }
    rax = *((rbx + 0x50));
    *((rbx + 0x30)) = 0;
    *((rbx + 0x28)) = 0;
    *((rbx + 0x58)) = rax;
    rax = *((rbx + 0x60));
    *((rbx + 0x38)) = 0;
    *((rbx + 0x68)) = rax;
    eax = r8d;
    eax &= 1;
    *((rbx + 0x8c)) = 0;
    eax -= eax;
    eax &= 2;
    eax += 4;
    *((rbx + 0x70)) = eax;
    while (1) {
label_18:
        if (rbp != 0) {
            rax = *((rbx + 0x38));
            edx = *((rbx + 0x8c));
            if (rax <= rbp) {
                goto label_19;
            }
            if (dl != 0) {
                goto label_20;
            }
            eax = re_string_context_at (rbx, rbp - 1, r8d, rcx, r8, r9);
            *((rbx + 0x70)) = eax;
            if (*((rbx + 0x90)) > 1) {
                goto label_21;
            }
label_0:
            if (*((rbx + 0x8b)) != 0) {
                goto label_22;
            }
label_4:
            *((rbx + 0x30)) -= rbp;
            *((rbx + 0x38)) -= rbp;
label_2:
            if (*((rbx + 0x8b)) != 0) {
                goto label_23;
            }
            *((rbx + 8)) += rbp;
        }
label_23:
        rax = *((rbx + 0x58));
        *((rbx + 0x68)) -= rbp;
        *((rbx + 0x28)) = r12;
        rax -= rbp;
        *((rbx + 0x58)) = rax;
        if (*((rbx + 0x90)) <= 1) {
            goto label_24;
        }
        rdi = rbx;
        if (*((rbx + 0x88)) == 0) {
            goto label_25;
        }
        eax = build_wcs_upper_buffer (rdi);
        if (eax == 0) {
label_1:
            *((rbx + 0x48)) = 0;
            eax = 0;
        }
        rdx = *((rsp + 0x38));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_26;
        }
        return rax;
        rax = *(rbx);
        *((rbx + 8)) = rax;
    }
label_21:
    rdx -= rbp;
    rdx <<= 2;
    memmove (*((rbx + 0x10)), rdi + rbp*4, *((rbx + 0x30)));
    goto label_0;
label_25:
    rax = build_wcs_buffer (rdi, rsi);
    goto label_1;
label_24:
    if (*((rbx + 0x8b)) != 0) {
        goto label_27;
    }
    *((rbx + 0x30)) = rax;
    goto label_1;
label_19:
    rcx = *((rbx + 0x30));
    *((rsp + 8)) = rcx;
    if (dl != 0) {
        goto label_28;
    }
label_13:
    rdx = *((rbx + 0x90));
    rsi = *((rbx + 0x28));
    *((rbx + 0x30)) = 0;
    if (edx <= 1) {
        goto label_29;
    }
    if (*((rbx + 0x89)) != 0) {
        goto label_30;
    }
label_6:
    r15 = rax + rsi;
    if (r12 <= r15) {
        goto label_31;
    }
    rax = rbx + 0x20;
    r13 = rsp + 0x24;
    *((rsp + 0x10)) = rax;
    do {
        r14 = *((rbx + 0x50));
        *((rsp + 4)) = r8d;
        rax = *((rbx + 0x20));
        r14 -= r15;
        rsi += r15;
        *((rsp + 0x18)) = rax;
        rax = rpl_mbrtowc (r13, *(rbx), r14, *((rsp + 0x10)));
        ecx = *((rsp + 0x24));
        r8d = *((rsp + 4));
        rsi = rax - 1;
        if (rsi > 0xfffffffffffffffc) {
            goto label_32;
        }
label_14:
        r15 += rax;
    } while (r12 > r15);
    rax = r15;
    rax -= r12;
    *((rbx + 0x30)) = rax;
    if (ecx == 0xffffffff) {
        goto label_33;
    }
label_11:
    if (*((rbx + 0x8e)) != 0) {
        goto label_34;
    }
label_15:
    eax = 0;
    if (ecx == 0xa) {
        eax = 0;
        al = (*((rbx + 0x8d)) != 0) ? 1 : 0;
        eax += eax;
    }
label_10:
    r13 = *((rbx + 0x30));
    *((rbx + 0x70)) = eax;
    if (r13 != 0) {
        if (r13 > 0) {
            memset (*((rbx + 0x10)), 0xff, r13*4);
        }
        if (*((rbx + 0x8b)) != 0) {
            goto label_35;
        }
    }
label_5:
    *((rbx + 0x38)) = r13;
    goto label_2;
label_20:
    r15 = *((rbx + 0x30));
    r14 = *((rbx + 0x18));
    esi = 0;
    rcx = r15;
    if (*((rbx + 0x8b)) >= 0) {
        while (rdx <= rbp) {
            goto label_36;
        }
        rsi = rax + 1;
label_3:
        if (rsi >= rcx) {
            goto label_37;
        }
        rdx = rsi + rcx;
        rax = rdx;
        rax >>= 0x3f;
        rax += rdx;
        rax >>= 1;
        rdx = *((r14 + rax*8));
        r9 = rax;
    }
    rcx = rax;
    goto label_3;
label_22:
    rdx -= rbp;
    memmove (*((rbx + 8)), rdi + rbp, *((rbx + 0x30)));
    goto label_4;
label_37:
    r13 = rax + 1;
    if (rdx >= rbp) {
        goto label_38;
    }
label_8:
    eax = re_string_context_at (rbx, r9, r8d, rcx, r8, r9);
    *((rbx + 0x70)) = eax;
    if (rbp != r13) {
        goto label_39;
    }
    if (rbp >= r15) {
        goto label_39;
    }
    if (*((r14 + r13*8)) != rbp) {
        goto label_39;
    }
    r15 -= rbp;
    memmove (*((rbx + 0x10)), rdi + rbp*4, r15*4);
    rdx -= rbp;
    memmove (*((rbx + 8)), rdi + rbp, *((rbx + 0x30)));
    rax = *((rbx + 0x30));
    *((rbx + 0x38)) -= rbp;
    rax -= rbp;
    *((rbx + 0x30)) = rax;
    if (rax <= 0) {
        goto label_2;
    }
    rcx = *((rbx + 0x18));
    eax = 0;
    rsi = rcx + rbp*8;
    do {
        rdx = *((rsi + rax*8));
        rdx -= rbp;
        *((rcx + rax*8)) = rdx;
        rax++;
    } while (*((rbx + 0x30)) > rax);
    goto label_2;
label_39:
    rax = *((rbx + 0x50));
    *((rbx + 0x8c)) = 0;
    rax += rbp;
    rax -= r12;
    *((rbx + 0x58)) = rax;
    rax = *((rbx + 0x60));
    rax += rbp;
    rax -= r12;
    *((rbx + 0x68)) = rax;
    if (r13 > 0) {
        goto label_40;
    }
    goto label_41;
    do {
        r13--;
        if (r13 == 0) {
            goto label_41;
        }
label_40:
    } while (*((r14 + r13*8 - 8)) == rbp);
label_41:
    if (r15 <= r13) {
        goto label_42;
    }
    rax = *((rbx + 0x10));
    while (*((rax + r13*4)) == 0xffffffff) {
        r13++;
        if (r15 == r13) {
            goto label_43;
        }
    }
label_9:
    r13 = *((r14 + r13*8));
    r13 -= rbp;
    *((rbx + 0x30)) = r13;
    if (r13 == 0) {
        goto label_5;
    }
    if (r13 > 0) {
        memset (*((rbx + 0x10)), 0xff, r13*4);
    }
label_35:
    memset (*((rbx + 8)), 0xff, r13);
    r13 = *((rbx + 0x30));
    goto label_5;
label_30:
    rcx = *(rbx);
    rdi = rbp;
    rdi -= rdx;
    r15 = rcx + rsi;
    rdx = r15 + rdi;
    r14 = r15 + rbp - 1;
    if (rcx < rdx) {
        rcx = rdx;
    }
    if (r14 >= rcx) {
        goto label_44;
    }
    goto label_6;
label_7:
    r14--;
    if (r14 < rcx) {
        goto label_6;
    }
label_44:
    edx = *(r14);
    edx &= 0xffffffc0;
    if (dl == 0x80) {
        goto label_7;
    }
    rdx = *((rbx + 0x58));
    rcx = *((rbx + 0x78));
    rsi = r14;
    rdx += r15;
    rdx -= r14;
    if (rcx != 0) {
        goto label_45;
    }
label_17:
    r13 = rsp + 0x24;
    r15 += rbp;
    *((rsp + 4)) = r8d;
    *((rsp + 0x28)) = 0;
    r15 -= r14;
    rax = rpl_mbrtowc (r13, rsi, rdx, rsp + 0x28);
    r8d = *((rsp + 4));
    if (rax >= r15) {
        if (rax <= 0xfffffffffffffffd) {
            goto label_46;
        }
    }
label_12:
    rsi = *((rbx + 0x28));
    rax = *((rbx + 0x38));
    goto label_6;
label_38:
    r13 = rax;
    r9 = rax - 1;
    goto label_8;
label_27:
    if (*((rbx + 0x88)) != 0) {
        goto label_47;
    }
    rcx = *((rbx + 0x78));
    if (rcx == 0) {
        goto label_1;
    }
    rdx = *((rbx + 0x40));
    if (rax > rdx) {
        rax = rdx;
    }
    rdx = *((rbx + 0x30));
    if (rax > rdx) {
        goto label_48;
    }
    goto label_49;
    do {
        r12 = *((rbx + 0x28));
        rcx = *((rbx + 0x78));
label_48:
        rsi = *(rbx);
        rsi += rdx;
        esi = *((rsi + r12));
        esi = *((rcx + rsi));
        rcx = *((rbx + 8));
        *((rcx + rdx)) = sil;
        rdx++;
    } while (rax != rdx);
label_16:
    *((rbx + 0x30)) = rax;
    *((rbx + 0x38)) = rax;
    goto label_1;
label_29:
    rax = *(rbx);
    rax += rbp;
    ecx = *((rsi + rax - 1));
    rax = *((rbx + 0x78));
    *((rbx + 0x38)) = 0;
    if (rax != 0) {
        ecx = *((rax + rcx));
    }
    rdx = *((rbx + 0x80));
    eax = (int32_t) cl;
    rax >>= 6;
    rax = *((rdx + rax*8));
    edx = 1;
    rax >>= cl;
    if ((al & 1) == 0) {
        edx = 0;
        if (cl != 0xa) {
            goto label_50;
        }
        edx = 0;
        dl = (*((rbx + 0x8d)) != 0) ? 1 : 0;
        edx += edx;
    }
label_50:
    *((rbx + 0x70)) = edx;
    goto label_2;
label_36:
    r13 = rax;
    r9--;
    goto label_8;
    if (r9 != 0) {
label_42:
        goto label_9;
    }
label_43:
    *((rbx + 0x30)) = 0;
    r13d = 0;
    goto label_5;
label_31:
    rax = r15;
    rax -= r12;
    *((rbx + 0x30)) = rax;
label_33:
    rsi--;
    rax = re_string_context_at (rbx, *((rsp + 8)), r8d, rcx, r8, r9);
    goto label_10;
label_46:
    ecx = *((rsp + 0x24));
    rax -= r15;
    *((rbx + 0x20)) = 0;
    *((rbx + 0x30)) = rax;
    if (ecx != 0xffffffff) {
        goto label_11;
    }
    goto label_12;
label_47:
    rax = build_upper_buffer (rbx, rsi, rdx, rcx, r8, r9);
    goto label_1;
label_28:
    rdx = *((rbx + 0x50));
    *((rbx + 0x8c)) = 0;
    rdx += rbp;
    rdx -= r12;
    *((rbx + 0x58)) = rdx;
    rdx = *((rbx + 0x60));
    rdx += rbp;
    rdx -= r12;
    *((rbx + 0x68)) = rdx;
    goto label_13;
label_32:
    if (rax == 0) {
        goto label_51;
    }
    if (r14 == 0) {
        goto label_51;
    }
    rax = *(rbx);
    ecx = *((rax + r15));
    do {
        rax = *((rsp + 0x18));
        *((rbx + 0x20)) = rax;
        eax = 1;
        goto label_14;
label_51:
        ecx = 0;
    } while (1);
label_34:
    edi = ecx;
    *((rsp + 4)) = ecx;
    eax = iswalnum (edi);
    ecx = *((rsp + 4));
    if (ecx == 0x5f) {
        goto label_52;
    }
    if (eax == 0) {
        goto label_15;
    }
label_52:
    eax = 1;
    goto label_10;
label_49:
    rax = rdx;
    goto label_16;
label_45:
    eax = 6;
    rsi = rsp + 0x32;
    if (rdx <= rax) {
        rax = rdx;
    }
    edi = rax - 1;
    rax = (int64_t) edi;
    if (edi < 0) {
        goto label_17;
    }
    do {
        edi = *((r14 + rax));
        edi = *((rcx + rdi));
        *((rsi + rax)) = dil;
        rax--;
    } while (eax >= 0);
    goto label_17;
label_26:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x8f00 */
 
int64_t dbg_re_compile_fastmap_iter (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7) {
    unsigned char c;
    mbstate_t state;
    char[256] buf;
    int64_t var_8h;
    char ** var_10h;
    char ** s;
    int64_t var_20h;
    int64_t var_28h;
    wint_t wc;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_41h;
    int64_t var_148h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    /* void re_compile_fastmap_iter(regex_t * bufp,re_dfastate_t const * init_state,char * fastmap); */
    r15 = rdx;
    ebp = 0;
    rbx = *(rdi);
    *((rsp + 0x20)) = rdi;
    *((rsp + 8)) = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x148)) = rax;
    eax = 0;
    if (*((rbx + 0xb4)) == 1) {
        rax = *((rdi + 0x18));
        r9 = *((rdi + 0x18));
        *(rsp) = rax;
        r9 >>= 0x16;
        ebp &= 1;
    }
    rax = *((rsp + 8));
    *(rsp) = 0;
    if (*((rax + 0x10)) <= 0) {
        goto label_0;
    }
    do {
        rax = *((rsp + 8));
        rsi = *(rsp);
        rdx = *(rbx);
        rax = *((rax + 0x18));
        r13 = *((rax + rsi*8));
        rax = *((rax + rsi*8));
        rax <<= 4;
        rcx = rdx + rax;
        *((rsp + 0x18)) = rax;
        esi = *((rcx + 8));
        if (sil != 1) {
            if (sil > 7) {
                goto label_1;
            }
            rdi = 0x000169c0;
            rax = *((rdi + rsi*4));
            rax += rdi;
            /* switch table (8 cases) at 0x169c0 */
            void (*rax)() ();
        }
        edx = *(rcx);
        *((r15 + rdx)) = 1;
        *((rsp + 0x10)) = rdx;
        if (ebp != 0) {
            rax = ctype_tolower_loc ();
            rdx = *((rsp + 0x10));
            rax = *(rax);
            rax = *((rax + rdx*4));
            *((r15 + rax)) = 1;
        }
        rax = *((rsp + 0x20));
        if ((*((rax + 0x1a)) & 0x40) != 0) {
            if (*((rbx + 0xb4)) > 1) {
                goto label_5;
            }
        }
label_1:
        rcx = *((rsp + 8));
        rax = *(rsp);
    } while (*((rcx + 0x10)) > rax);
    do {
label_0:
        rax = *((rsp + 0x148));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_6;
        }
        return rax;
        __asm ("movdqa xmm0, xmmword [0x00017000]");
        __asm ("movups xmmword [r15], xmm0");
        __asm ("movups xmmword [r15 + 0x10], xmm0");
        __asm ("movups xmmword [r15 + 0x20], xmm0");
        __asm ("movups xmmword [r15 + 0x30], xmm0");
        __asm ("movups xmmword [r15 + 0x40], xmm0");
        __asm ("movups xmmword [r15 + 0x50], xmm0");
        __asm ("movups xmmword [r15 + 0x60], xmm0");
        __asm ("movups xmmword [r15 + 0x70], xmm0");
        __asm ("movups xmmword [r15 + 0x80], xmm0");
        __asm ("movups xmmword [r15 + 0x90], xmm0");
        __asm ("movups xmmword [r15 + 0xa0], xmm0");
        __asm ("movups xmmword [r15 + 0xb0], xmm0");
        __asm ("movups xmmword [r15 + 0xc0], xmm0");
        __asm ("movups xmmword [r15 + 0xd0], xmm0");
        __asm ("movups xmmword [r15 + 0xe0], xmm0");
        __asm ("movups xmmword [r15 + 0xf0], xmm0");
    } while (esi != 2);
    goto label_7;
    __asm ("movdqa xmm0, xmmword [0x00017000]");
    __asm ("movups xmmword [r15], xmm0");
    __asm ("movups xmmword [r15 + 0x10], xmm0");
    __asm ("movups xmmword [r15 + 0x20], xmm0");
    __asm ("movups xmmword [r15 + 0x30], xmm0");
    __asm ("movups xmmword [r15 + 0x40], xmm0");
    __asm ("movups xmmword [r15 + 0x50], xmm0");
    __asm ("movups xmmword [r15 + 0x60], xmm0");
    __asm ("movups xmmword [r15 + 0x70], xmm0");
    __asm ("movups xmmword [r15 + 0x80], xmm0");
    __asm ("movups xmmword [r15 + 0x90], xmm0");
    __asm ("movups xmmword [r15 + 0xa0], xmm0");
    __asm ("movups xmmword [r15 + 0xb0], xmm0");
    __asm ("movups xmmword [r15 + 0xc0], xmm0");
    __asm ("movups xmmword [r15 + 0xd0], xmm0");
    __asm ("movups xmmword [r15 + 0xe0], xmm0");
    __asm ("movups xmmword [r15 + 0xf0], xmm0");
label_7:
    rax = *((rsp + 0x20));
    *((rax + 0x38)) |= 1;
    goto label_0;
    r14 = *(rcx);
    if (*((rbx + 0xb4)) > 1) {
        if (*((r14 + 0x48)) != 0) {
            goto label_8;
        }
        if ((*((r14 + 0x20)) & 1) != 0) {
            goto label_8;
        }
        if (*((r14 + 0x40)) != 0) {
            goto label_8;
        }
    }
    ecx = 0;
    rax = rsp + 0x40;
    *((rsp + 0x18)) = rax;
    r13 = rsp + 0x38;
    if (*((r14 + 0x28)) > 0) {
        goto label_9;
    }
    goto label_1;
label_2:
    rcx++;
    if (*((r14 + 0x28)) <= rcx) {
        goto label_1;
    }
label_9:
    *(r13) = 0;
    rax = *(r14);
    r12 = rcx*4;
    *((rsp + 0x10)) = rcx;
    rax = wcrtomb (*((rsp + 0x18)), *((rax + rcx*4)), r13);
    rcx = *((rsp + 0x10));
    if (rax != -1) {
        edx = *((rsp + 0x40));
        *((r15 + rdx)) = 1;
        *((rsp + 0x10)) = rdx;
        if (ebp == 0) {
            goto label_10;
        }
        *((rsp + 0x28)) = rcx;
        rax = ctype_tolower_loc ();
        rdx = *((rsp + 0x10));
        rcx = *((rsp + 0x28));
        rax = *(rax);
        rax = *((rax + rdx*4));
        *((r15 + rax)) = 1;
    }
label_10:
    rax = *((rsp + 0x20));
    if ((*((rax + 0x1a)) & 0x40) == 0) {
        goto label_2;
    }
    if (*((rbx + 0xb4)) <= 1) {
        goto label_2;
    }
    rax = *(r14);
    *((rsp + 0x10)) = rcx;
    eax = towlower (*((rax + r12)));
    rax = wcrtomb (*((rsp + 0x18)), eax, r13);
    rcx = *((rsp + 0x10));
    if (rax == -1) {
        goto label_2;
    }
    eax = *((rsp + 0x40));
    *((r15 + rax)) = 1;
    goto label_2;
    *((rsp + 0x10)) = 0;
label_3:
    rax = *((rsp + 0x18));
    rsi = *((rsp + 0x10));
    r14d = 0;
    rax = *((rdx + rax));
    r13 = rsi*8;
    r12 = *((rax + rsi));
    do {
        if (((r12 >> r14) & 1) < 0) {
            *((r15 + r13)) = 1;
            rax = r15 + r13;
            if (ebp == 0) {
                goto label_11;
            }
            edi = r13 + 0x80;
            if (edi <= 0x17f) {
                rax = ctype_tolower_loc ();
                rax = *(rax);
                rax = *((rax + r13*4));
                rax += r15;
            }
            *(rax) = 1;
        }
label_11:
        r14d++;
        r13++;
    } while (r14d != 0x40);
    rax = *((rsp + 0x10));
    if (rax == 0x20) {
        goto label_1;
    }
    rdx = *(rbx);
    goto label_3;
label_8:
    *((rsp + 0x34)) = 0;
    r13 = rsp + 0x38;
    r14 = rsp + 0x34;
    do {
        *(r13) = 0;
        rax = rpl_mbrtowc (0, r14, 1, r13);
        if (rax == 0xfffffffffffffffe) {
            eax = *((rsp + 0x34));
            *((r15 + rax)) = 1;
        }
    } while (rax != 0xfffffffffffffffe);
    goto label_1;
label_5:
    rax = *(rbx);
    rcx = *((rsp + 0x18));
    r13++;
    eax = *((rax + rcx));
    rcx = rsp + 0x41;
    *((rsp + 0x40)) = al;
    if (r13 < *((rbx + 0x10))) {
        goto label_12;
    }
    goto label_13;
    do {
        eax = *(rax);
        rcx++;
        r13++;
        *((rcx - 1)) = al;
        if (*((rbx + 0x10)) <= r13) {
            goto label_14;
        }
label_12:
        rax = r13;
        rax <<= 4;
        rax += *(rbx);
        edx = *((rax + 8));
        edx &= 0x2000ff;
    } while (edx == 0x200001);
label_14:
    rax = rsp + 0x40;
    *((rsp + 0x18)) = rax;
    rcx -= rax;
label_4:
    r13 = rsp + 0x38;
    r14 = rsp + 0x34;
    *((rsp + 0x10)) = rdx;
    *((rsp + 0x38)) = 0;
    rax = rpl_mbrtowc (r14, *((rsp + 0x18)), rcx, r13);
    rdx = *((rsp + 0x10));
    if (rax != rdx) {
        goto label_1;
    }
    eax = towlower (*((rsp + 0x34)));
    rax = wcrtomb (*((rsp + 0x18)), eax, r13);
    if (rax == -1) {
        goto label_1;
    }
    eax = *((rsp + 0x40));
    *((r15 + rax)) = 1;
    goto label_1;
label_13:
    rax = rsp + 0x40;
    edx = 1;
    *((rsp + 0x18)) = rax;
    goto label_4;
label_6:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x9400 */
 
int64_t dbg_peek_token (int64_t arg_8h, int64_t arg_ah, uint32_t arg1, signed int64_t arg2, int64_t arg3) {
    re_token_t next;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int peek_token(re_token_t * token,re_string_t * input,reg_syntax_t syntax); */
label_4:
    r13 = *((rsi + 0x48));
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    if (*((rsi + 0x68)) <= r13) {
        goto label_14;
    }
    r15 = *((rsi + 8));
    eax = *((rsi + 0x90));
    rbx = rsi;
    r14 = rdx;
    r12d = *((r15 + r13));
    *((rdi + 0xa)) &= 0x9f;
    *(rdi) = r12b;
    if (eax <= 1) {
        goto label_15;
    }
    if (r13 != *((rsi + 0x30))) {
        rdx = *((rsi + 0x10));
        if (*((rdx + r13*4)) == 0xffffffff) {
            goto label_16;
        }
    }
    if (r12b == 0x5c) {
        goto label_17;
    }
    rax = *((rbx + 0x10));
    *((rbp + 8)) = 1;
    edx = *((rax + r13*4));
    *((rsp + 0xc)) = edx;
    eax = iswalnum (*((rax + r13*4)));
    edx = *((rsp + 0xc));
    al = (eax != 0) ? 1 : 0;
    dl = (edx == 0x5f) ? 1 : 0;
    eax |= edx;
    do {
        edx = *((rbp + 0xa));
        eax <<= 6;
        edx &= 0xffffffbf;
        eax |= edx;
        *((rbp + 0xa)) = al;
        if (r12b > 0x3f) {
            goto label_18;
        }
        if (r12b <= 9) {
            goto label_0;
        }
        r12d -= 0xa;
        if (r12b > 0x35) {
            goto label_0;
        }
        rdx = 0x000169e0;
        r12d = (int32_t) r12b;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (54 cases) at 0x169e0 */
        void (*rax)() ();
label_15:
        if (r12b == 0x5c) {
            goto label_17;
        }
        *((rbp + 8)) = 1;
        rax = ctype_b_loc ();
        r8 = rax;
        eax = (int32_t) r12b;
        rdx = *(r8);
        eax = *((rdx + rax*2));
        ax >>= 3;
        eax &= 1;
        dl = (r12b == 0x5f) ? 1 : 0;
        eax |= edx;
    } while (1);
label_2:
    if (*((r15 + r13 - 1)) == 0xa) {
        if ((r14d & 0x800) != 0) {
            goto label_19;
        }
    }
label_0:
    eax = 1;
    do {
label_1:
        rdx = *((rsp + 0x28));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_20;
        }
        return rax;
label_17:
        rcx = r13 + 1;
        if (rcx < *((rbx + 0x58))) {
            goto label_21;
        }
        *((rbp + 8)) = 0x24;
        eax = 1;
    } while (1);
label_18:
    r12d -= 0x5b;
    if (r12b > 0x22) {
        goto label_0;
    }
    rdx = 0x00016ab8;
    r12d = (int32_t) r12b;
    rax = *((rdx + r12*4));
    rax += rdx;
    /* switch table (35 cases) at 0x16ab8 */
    eax = void (*rax)() ();
label_14:
    *((rdi + 8)) = 2;
    goto label_1;
label_21:
    if (*((rbx + 0x8b)) != 0) {
        goto label_22;
    }
label_12:
    esi = *((r15 + r13 + 1));
    *((rsp + 0xc)) = esi;
    r12d = esi;
label_11:
    *(rbp) = r12b;
    *((rbp + 8)) = 1;
    if (eax <= 1) {
        goto label_23;
    }
    rax = *((rbx + 0x10));
    rsi = rax + rcx*4;
label_13:
    ebx = *(rsi);
    eax = iswalnum (*(rsi));
    al = (eax != 0) ? 1 : 0;
    cl = (ebx == 0x5f) ? 1 : 0;
    eax |= ecx;
    do {
        ecx = *((rbp + 0xa));
        eax <<= 6;
        edx = r12 - 0x27;
        ecx &= 0xffffffbf;
        eax |= ecx;
        *((rbp + 0xa)) = al;
        if (dl > 0x56) {
            goto label_24;
        }
        rcx = 0x00016b44;
        edx = (int32_t) dl;
        rax = *((rcx + rdx*4));
        rax += rcx;
        /* switch table (87 cases) at 0x16b44 */
        void (*rax)() ();
label_16:
        eax = *((rdi + 8));
        eax &= 0xffdfff00;
        eax |= 0x200001;
        *((rdi + 8)) = eax;
        eax = 1;
        goto label_1;
label_23:
        rax = ctype_b_loc ();
        r8 = rax;
        eax = (int32_t) r12b;
        rcx = *(r8);
        eax = *((rcx + rax*2));
        ax >>= 3;
        eax &= 1;
        cl = (r12b == 0x5f) ? 1 : 0;
        eax |= ecx;
    } while (1);
label_24:
    eax = 2;
    goto label_1;
    r14d &= 0x8400;
    if (r14 != 0x8000) {
        goto label_0;
    }
label_3:
    *((rbp + 8)) = 0xa;
    eax = 1;
    goto label_1;
    r14d &= 0x1200;
    eax = 1;
    if (r14 != 0x1200) {
        goto label_1;
    }
label_5:
    *((rbp + 8)) = 0x18;
    goto label_1;
    if ((r14d & 0x800008) != 0) {
        goto label_19;
    }
    if (r13 != 0) {
        goto label_2;
    }
label_19:
    *((rbp + 8)) = 0xc;
    eax = 1;
    *(rbp) = 0x10;
    goto label_1;
    r14d &= 0x1200;
    eax = 1;
    if (r14 != 0x1200) {
        goto label_1;
    }
label_9:
    *((rbp + 8)) = 0x17;
    goto label_1;
    *((rbp + 8)) = 0x14;
    eax = 1;
    goto label_1;
    *((rbp + 8)) = 5;
    eax = 1;
    goto label_1;
    eax = 1;
    if ((r14d & 0x402) != 0) {
        goto label_1;
    }
label_10:
    *((rbp + 8)) = 0x13;
    goto label_1;
    eax = 1;
    if ((r14d & 0x800) == 0) {
        goto label_1;
    }
    goto label_3;
    if ((r14b & 8) != 0) {
        goto label_25;
    }
    r13++;
    if (r13 == *((rbx + 0x58))) {
        goto label_25;
    }
    *((rbx + 0x48)) = r13;
    peek_token (rsp + 0x10, rbx, r14, rcx, r8);
    goto label_4;
    eax = *((rsp + 0x18));
    *((rbx + 0x48))--;
    edx = rax - 9;
    eax = 1;
    if (dl > 1) {
        goto label_1;
    }
label_25:
    *((rbp + 8)) = 0xc;
    eax = 1;
    *(rbp) = 0x20;
    goto label_1;
    eax = 1;
    if ((r14d & sym._init) == 0) {
        goto label_1;
    }
label_8:
    *((rbp + 8)) = 8;
    goto label_1;
    eax = 1;
    if ((r14d & sym._init) == 0) {
        goto label_1;
    }
label_7:
    *((rbp + 8)) = 9;
    goto label_1;
    *((rbp + 8)) = 0xb;
    eax = 1;
    goto label_1;
    eax = 1;
    if ((r14d & 0x402) != 0) {
        goto label_1;
    }
label_6:
    *((rbp + 8)) = 0x12;
    goto label_1;
    eax = 2;
    if ((r14d & 0x4000) != 0) {
        goto label_1;
    }
    edx = *((rsp + 0xc));
    *((rbp + 8)) = 4;
    edx -= 0x31;
    rdx = (int64_t) edx;
    *(rbp) = rdx;
    goto label_1;
    eax = 2;
    if ((r14d & 0x80000) != 0) {
        goto label_1;
    }
    *((rbp + 8)) = 0xc;
    *(rbp) = 0x80;
    goto label_1;
    r14d &= 0x1200;
    eax = 2;
    if (r14 != 0x200) {
        goto label_1;
    }
    goto label_5;
    r14d &= 0x402;
    eax = 2;
    if (r14 != 2) {
        goto label_1;
    }
    goto label_6;
    eax = 2;
    if ((r14d & 0x80000) != 0) {
        goto label_1;
    }
    *((rbp + 8)) = 0xc;
    *(rbp) = 9;
    goto label_1;
    eax = 2;
    if ((r14d & 0x80000) != 0) {
        goto label_1;
    }
    *((rbp + 8)) = 0xc;
    *(rbp) = 6;
    goto label_1;
    eax = 2;
    if ((r14d & sym._init) != 0) {
        goto label_1;
    }
    goto label_7;
    eax = 2;
    if ((r14d & sym._init) != 0) {
        goto label_1;
    }
    goto label_8;
    eax = 2;
    if ((r14d & 0x8400) != 0) {
        goto label_1;
    }
    *((rbp + 8)) = 0xa;
    goto label_1;
    r14d &= 0x1200;
    eax = 2;
    if (r14 != 0x200) {
        goto label_1;
    }
    goto label_9;
    eax = 2;
    if ((r14d & 0x80000) != 0) {
        goto label_1;
    }
    *((rbp + 8)) = 0x20;
    goto label_1;
    eax = 2;
    if ((r14d & 0x80000) != 0) {
        goto label_1;
    }
    *((rbp + 8)) = 0x22;
    goto label_1;
    eax = 2;
    if ((r14d & 0x80000) != 0) {
        goto label_1;
    }
    *((rbp + 8)) = 0xc;
    *(rbp) = 0x100;
    goto label_1;
    eax = 2;
    if ((r14d & 0x80000) != 0) {
        goto label_1;
    }
    *((rbp + 8)) = 0xc;
    *(rbp) = 0x40;
    goto label_1;
    eax = 2;
    if ((r14d & 0x80000) != 0) {
        goto label_1;
    }
    *((rbp + 8)) = 0x21;
    goto label_1;
    eax = 2;
    if ((r14d & 0x80000) != 0) {
        goto label_1;
    }
    *((rbp + 8)) = 0x23;
    goto label_1;
    eax = 2;
    if ((r14d & 0x80000) != 0) {
        goto label_1;
    }
    *((rbp + 8)) = 0xc;
    *(rbp) = 0x200;
    goto label_1;
    r14d &= 0x402;
    eax = 2;
    if (r14 != 2) {
        goto label_1;
    }
    goto label_10;
label_22:
    if (eax > 1) {
        rdi = *((rbx + 0x10));
        rdx = rcx*4;
        rsi = rdi + rdx;
        if (*(rsi) == 0xffffffff) {
            goto label_26;
        }
        r8 = r13 + 2;
        if (*((rbx + 0x30)) == r8) {
            goto label_27;
        }
        if (*((rdi + rdx + 4)) == 0xffffffff) {
            goto label_26;
        }
    }
label_27:
    rdx = *(rbx);
    rsi = *((rbx + 0x28));
    if (*((rbx + 0x8c)) == 0) {
        goto label_28;
    }
    rdi = *((rbx + 0x18));
    rdx += rsi;
    rdx += *((rdi + rcx*8));
    esi = *(rdx);
    *((rsp + 0xc)) = esi;
    r12d = esi;
    if ((sil & 0x80) == 0) {
        goto label_11;
    }
    goto label_12;
label_28:
    rdx += rcx;
    esi = *((rdx + rsi));
    *((rsp + 0xc)) = esi;
    r12d = esi;
    goto label_11;
label_26:
    eax = *((r15 + r13 + 1));
    *((rbp + 8)) = 1;
    *(rbp) = al;
    r12d = eax;
    *((rsp + 0xc)) = eax;
    goto label_13;
label_20:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x9ad0 */
 
uint64_t dbg_fetch_number (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* Idx fetch_number(re_string_t * input,re_token_t * token,reg_syntax_t syntax); */
    r13 = rdi;
    r12 = 0xffffffffffffffff;
    rbx = rsi;
    do {
label_1:
        rax = peek_token (rbx, r13, rbp, rcx, r8);
        edx = *(rbx);
        rax = (int64_t) eax;
        *((r13 + 0x48)) += rax;
        eax = *((rbx + 8));
        if (al == 2) {
            goto label_2;
        }
        if (dl == 0x2c) {
            goto label_3;
        }
        if (al == 0x18) {
            goto label_3;
        }
        if (al == 1) {
            goto label_4;
        }
label_0:
        r12 = 0xfffffffffffffffe;
    } while (1);
label_4:
    eax = rdx - 0x30;
    if (al > 9) {
        goto label_0;
    }
    if (r12 == 0xfffffffffffffffe) {
        goto label_0;
    }
    if (r12 != -1) {
        rax = r12 * 5;
        r12 = rdx + rax*2;
        eax = 0x8030;
        if (r12 > rax) {
            r12 = rax;
        }
        r12 -= 0x30;
        goto label_1;
label_2:
        r12 = 0xfffffffffffffffe;
label_3:
        rax = r12;
        return rax;
    }
    edx -= 0x30;
    r12 = (int64_t) edx;
    goto label_1;
}

/* /tmp/tmpvkjt68yd @ 0x9b80 */
 
uint64_t dbg_check_halt_state_context (int64_t arg_10h, int64_t arg_18h, int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* Idx check_halt_state_context(re_match_context_t const * mctx,re_dfastate_t const * state,Idx idx); */
    rbx = rdi;
    eax = re_string_context_at (rdi, rdx, *((rdi + 0xa0)), rcx, r8, r9);
    r8 = *((rbp + 0x10));
    if (r8 <= 0) {
        goto label_2;
    }
    rdx = *((rbx + 0x98));
    r11d = eax;
    ebx = eax;
    r9 = *((rbp + 0x18));
    eax &= 1;
    ecx = 0;
    ebx &= 8;
    r11d &= 2;
    rsi = *(rdx);
    edi = eax;
label_1:
    rax = *((r9 + rcx*8));
    rdx = *((r9 + rcx*8));
    rdx <<= 4;
    rdx += rsi;
    if (*((rdx + 8)) != 2) {
        goto label_3;
    }
    edx = *((rdx + 8));
    edx >>= 8;
    r10d = edx;
    r10w &= 0x3ff;
    if (r10w == 0) {
        goto label_4;
    }
    if ((dl & 4) == 0) {
        goto label_5;
    }
    if (edi == 0) {
        goto label_3;
    }
    edx &= 8;
    if (edx != 0) {
        goto label_3;
    }
    do {
label_0:
        if ((r10b & 0x20) != 0) {
            if (r11d == 0) {
                goto label_3;
            }
        }
        r10d &= 0x80;
        if (r10d != 0) {
            if (ebx == 0) {
                goto label_3;
            }
        }
label_4:
        return rax;
label_5:
        edx &= 8;
    } while (edx == 0);
    if (edi == 0) {
        goto label_0;
    }
label_3:
    rcx++;
    if (rcx != r8) {
        goto label_1;
    }
label_2:
    eax = 0;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x9c50 */
 
int64_t parse_bracket_element_constprop_0 (int64_t arg1, uint32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    uint32_t var_8h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r10 = rdi;
    r11 = rdx;
    rcx = *((rsi + 0x48));
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    if (*((rsi + 0x90)) == 1) {
        goto label_7;
    }
    rdi = *((rsi + 0x30));
    rax = rcx + 1;
    rdx = *((rsi + 0x10));
    if (rdi > rax) {
        goto label_8;
    }
    goto label_7;
    do {
        rax++;
        if (rdi == rax) {
            goto label_9;
        }
label_8:
        ebx = eax;
        ebx -= ecx;
    } while (*((rdx + rax*4)) == 0xffffffff);
    if (ebx != 1) {
        goto label_9;
    }
label_7:
    edi = *((r11 + 8));
    rdx = (int64_t) ebp;
    rdx += rcx;
    eax = edi;
    *((rsi + 0x48)) = rdx;
    eax &= 0xfffffffb;
    if (al == 0x1a) {
        goto label_10;
    }
    if (dil == 0x1c) {
        goto label_10;
    }
    if (dil == 0x16) {
        if (r9b == 0) {
            goto label_11;
        }
    }
label_6:
    eax = *(r11);
    *(r10) = 0;
    *((r10 + 8)) = al;
    eax = 0;
    do {
label_5:
        rdx = *((rsp + 0x18));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_12;
        }
        return rax;
label_10:
        r8 = *((rsi + 0x68));
        r9d = *(r11);
        eax = 0;
        if (rdx < r8) {
            goto label_13;
        }
label_0:
        eax = 7;
    } while (1);
label_2:
    rdx = *((rsi + 8));
    if (*((rdx + rcx)) == 0x5d) {
        goto label_14;
    }
label_1:
    rdx = *((r10 + 8));
    *((rdx + rax)) = dil;
    rax++;
    if (rax == 0x20) {
        goto label_0;
    }
    edi = *((r11 + 8));
    rdx = *((rsi + 0x48));
    r8 = *((rsi + 0x68));
label_13:
    rbx = (int64_t) eax;
    rcx = rdx + 1;
    if (dil == 0x1e) {
        goto label_15;
    }
label_3:
    rdi = *((rsi + 8));
    *((rsi + 0x48)) = rcx;
    edi = *((rdi + rdx));
label_4:
    if (r8 <= rcx) {
        goto label_0;
    }
    if (r9b != dil) {
        goto label_1;
    }
    goto label_2;
label_15:
    if (*((rsi + 0x8b)) == 0) {
        goto label_3;
    }
    if (*((rsi + 0x8c)) == 0) {
        goto label_16;
    }
    rbp = *((rsi + 0x30));
    if (rbp == rdx) {
        goto label_17;
    }
    rdi = *((rsi + 0x10));
    if (*((rdi + rdx*4)) == 0xffffffff) {
        goto label_3;
    }
label_17:
    rdi = *((rsi + 0x18));
    rdi = *((rdi + rdx*8));
    rdi += *(rsi);
    rdi += *((rsi + 0x28));
    edi = *(rdi);
    if ((dil & 0x80) != 0) {
        goto label_3;
    }
    if (*((rsi + 0x90)) == 1) {
        goto label_18;
    }
    r13 = *((rsi + 0x10));
    if (rbp > rcx) {
        goto label_19;
    }
    goto label_18;
    do {
        rcx++;
        if (rcx == rbp) {
            goto label_18;
        }
label_19:
        r12d = ecx;
        r12d -= edx;
    } while (*((r13 + rcx*4)) == 0xffffffff);
    r12 = (int64_t) r12d;
    rcx = r12 + rdx;
label_18:
    *((rsi + 0x48)) = rcx;
    goto label_4;
label_9:
    *(r10) = 1;
    edx = *((rdx + rcx*4));
    *((r10 + 8)) = edx;
    *((rsi + 0x48)) = rax;
    eax = 0;
    goto label_5;
label_14:
    rax = *((r10 + 8));
    rcx++;
    *((rsi + 0x48)) = rcx;
    *((rax + rbx)) = 0;
    edx = *((r11 + 8));
    if (dl == 0x1c) {
        goto label_20;
    }
    if (dl == 0x1e) {
        goto label_21;
    }
    eax = 0;
    if (dl != 0x1a) {
        goto label_5;
    }
    *(r10) = 3;
    goto label_5;
label_16:
    rdx += *(rsi);
    rdx += *((rsi + 0x28));
    *((rsi + 0x48)) = rcx;
    edi = *(rdx);
    goto label_4;
label_11:
    rdx = r8;
    peek_token_bracket (rsp, rsi);
    eax = 0xb;
    if (*((rsp + 8)) == 0x15) {
        goto label_6;
    }
    goto label_5;
label_21:
    *(r10) = 4;
    eax = 0;
    goto label_5;
label_20:
    *(r10) = 2;
    eax = 0;
    goto label_5;
label_12:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x9ec0 */
 
uint64_t dbg_check_dst_limits (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* _Bool check_dst_limits(re_match_context_t const * mctx,re_node_set const * limits,Idx dst_node,Idx dst_idx,Idx src_node,Idx src_idx); */
    r13 = rsi;
    rsi = rcx;
    r12 = r9;
    rbx = rcx;
    r11 = *((rdi + 0x98));
    *((rsp + 0x10)) = rdx;
    *((rsp + 0x18)) = r8;
    rax = search_cur_bkref_entry ();
    rsi = r12;
    *(rsp) = rax;
    rax = search_cur_bkref_entry ();
    *((rsp + 8)) = rax;
    if (*((r13 + 8)) <= 0) {
        goto label_5;
    }
    r9 = *((r13 + 0x10));
    r11 = *(r11);
    *((rsp + 0x20)) = r13;
    r14d = 0;
    r10 = *((rdi + 0xd8));
    r15 = *((rdi + 0xd8));
label_1:
    rax = *((r9 + r14*8));
    rax *= 3;
    rax <<= 4;
    rax += r15;
    rdx = *(rax);
    rdx <<= 4;
    r13 = *((r11 + rdx));
    rdx = *((rax + 0x10));
    if (rbx < rdx) {
        goto label_6;
    }
    rax = *((rax + 0x18));
    if (rbx > rax) {
        goto label_7;
    }
    ecx = 0;
    cl = (rbx == rdx) ? 1 : 0;
    ebp = 0;
    bpl = (rbx == rax) ? 1 : 0;
    ebp += ebp;
    ebp |= ecx;
    if (ebp != 0) {
        goto label_8;
    }
label_3:
    while (1) {
        esi = 1;
        if (r12 <= rax) {
            dl = (r12 == rdx) ? 1 : 0;
            esi = 0;
            sil = (r12 == rax) ? 1 : 0;
            edx = (int32_t) dl;
            esi += esi;
            esi |= edx;
            if (esi != 0) {
                goto label_9;
            }
        }
label_4:
        if (esi == ebp) {
            goto label_10;
        }
label_0:
        eax = 1;
label_2:
        return rax;
label_8:
        *((rsp + 0x38)) = r11;
        *((rsp + 0x30)) = r9;
        *((rsp + 0x28)) = rdi;
        eax = check_dst_limits_calc_pos_1 (rdi, ebp, r13, *((rsp + 0x10)), *(rsp));
        r9 = *((rsp + 0x30));
        rdi = *((rsp + 0x28));
        r11 = *((rsp + 0x38));
        rax = *((r9 + r14*8));
        rax *= 3;
        rax <<= 4;
        rax += r15;
        rdx = *((rax + 0x10));
        if (r12 < rdx) {
            goto label_11;
        }
        rax = *((rax + 0x18));
    }
label_6:
    if (r12 < rdx) {
        goto label_10;
    }
    rax = *((rax + 0x18));
    if (rax < r12) {
        goto label_0;
    }
    dl = (r12 == rdx) ? 1 : 0;
    esi = 0;
    sil = (rax == r12) ? 1 : 0;
    edx = (int32_t) dl;
    esi += esi;
    esi |= edx;
    if (esi == 0) {
        goto label_0;
    }
label_9:
    *((rsp + 0x38)) = r11;
    *((rsp + 0x30)) = r9;
    *((rsp + 0x28)) = rdi;
    eax = check_dst_limits_calc_pos_1 (rdi, rsi, r13, *((rsp + 0x18)), *((rsp + 8)));
    rdi = *((rsp + 0x28));
    r9 = *((rsp + 0x30));
    esi = eax;
    r11 = *((rsp + 0x38));
    if (esi != ebp) {
        goto label_0;
    }
label_10:
    rax = *((rsp + 0x20));
    r14++;
    if (*((rax + 8)) > r14) {
        goto label_1;
    }
label_5:
    eax = 0;
    goto label_2;
label_7:
    goto label_3;
label_11:
    esi = 0xffffffff;
    goto label_4;
}

/* /tmp/tmpvkjt68yd @ 0xa0c0 */
 
uint64_t dbg_check_node_accept_bytes (int64_t arg1, int64_t arg2, void * arg3, void * arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* int check_node_accept_bytes(re_dfa_t const * dfa,Idx node_idx,re_string_t const * input,Idx str_idx); */
    r9 = rdi;
    rsi <<= 4;
    rdi = rdx;
    r10 = rcx;
    rsi += *(r9);
    r8d = *((rsi + 8));
    if (r8b == 7) {
        goto label_9;
    }
    if (*((rdx + 0x90)) == 1) {
        goto label_1;
    }
    rcx = *((rdx + 0x10));
    rdx = *((rdx + 0x30));
    rax = r10 + 1;
    if (rax >= rdx) {
        goto label_1;
    }
    r12d = 1;
    while (*((rcx + rax*4)) == 0xffffffff) {
        rax++;
        r12d++;
        if (rax == rdx) {
            goto label_10;
        }
    }
    if (r8b == 5) {
        goto label_11;
    }
label_0:
    if (r8b != 6) {
        goto label_1;
    }
    if (r12d == 1) {
        goto label_1;
    }
    r13 = *(rsi);
    rsi = *((r13 + 0x40));
    rdx = *((r13 + 0x28));
    if (rsi == 0) {
        rax = rdx;
        rax |= *((r13 + 0x48));
        if (rax == 0) {
            goto label_12;
        }
    }
    ebp = *((rcx + r10*4));
    if (rdx <= 0) {
        goto label_13;
    }
    rcx = *(r13);
    eax = 0;
    while (*((rcx + rax*4)) != ebp) {
        rax++;
        if (rax == rdx) {
            goto label_13;
        }
    }
label_6:
    if ((*((r13 + 0x20)) & 1) != 0) {
label_1:
        r12d = 0;
    }
label_2:
    eax = r12d;
    return rax;
label_10:
    if (r8b != 5) {
        goto label_0;
    }
    rax = *((r9 + 0xd8));
    if ((al & 0x40) != 0) {
        goto label_3;
    }
label_4:
    rdx = *((rdi + 8));
    if (*((rdx + r10)) == 0xa) {
        goto label_1;
    }
label_3:
    if ((al & 0x80) == 0) {
        goto label_2;
    }
    rax = *((rdi + 8));
    if (*((rax + r10)) == 0) {
        goto label_1;
    }
    goto label_2;
label_11:
    if (r12d == 1) {
        goto label_1;
    }
    rax = *((r9 + 0xd8));
    if ((al & 0x40) != 0) {
        goto label_3;
    }
    goto label_4;
label_9:
    rdx = *((rdx + 8));
    eax = *((rdx + rcx));
    if (al <= 0xc1) {
        goto label_1;
    }
    rdi = *((rdi + 0x58));
    rcx = rcx + 1;
    if (rcx >= rdi) {
        goto label_1;
    }
    rcx += rdx;
    esi = *(rcx);
    if (al <= 0xdf) {
        goto label_14;
    }
    if (al > 0xef) {
        goto label_15;
    }
    if (al != 0xe0) {
        goto label_16;
    }
    if (sil <= 0x9f) {
        goto label_1;
    }
label_16:
    esi = 3;
    r12d = 3;
label_8:
    rax = r10 + rsi;
    if (rdi < rax) {
        goto label_1;
    }
    rax = rdx + r10;
    rax += rsi;
    goto label_17;
label_5:
    rcx++;
    if (rax == rcx) {
        goto label_2;
    }
label_17:
    ebx = *(rcx);
    edx = rbx - 0x80;
    if (dl <= 0x3f) {
        goto label_5;
    }
    goto label_1;
label_13:
    rax = *((r13 + 0x48));
    if (rax <= 0) {
        goto label_18;
    }
    ebx = 0;
    while (eax == 0) {
        rbx++;
        if (*((r13 + 0x48)) <= rbx) {
            goto label_19;
        }
        rax = *((r13 + 0x18));
        eax = iswctype (ebp, *((rax + rbx*8)));
    }
    goto label_6;
label_19:
    rsi = *((r13 + 0x40));
label_18:
    if (rsi <= 0) {
        goto label_12;
    }
    rdx = *((r13 + 8));
    eax = 0;
label_7:
    if (*((rdx + rax*4)) > ebp) {
        goto label_20;
    }
    rcx = *((r13 + 0x10));
    if (*((rcx + rax*4)) >= ebp) {
        goto label_6;
    }
label_20:
    rax++;
    if (rax != rsi) {
        goto label_7;
    }
label_12:
    if ((*((r13 + 0x20)) & 1) != 0) {
        goto label_2;
    }
    goto label_1;
label_15:
    if (al > 0xf7) {
        goto label_21;
    }
    if (al != 0xf0) {
        goto label_22;
    }
    if (sil <= 0x8f) {
        goto label_1;
    }
label_22:
    esi = 4;
    r12d = 4;
    goto label_8;
label_14:
    esi += 0xffffff80;
    r12d = 0;
    r12b = (sil <= 0x3f) ? 1 : 0;
    r12d += r12d;
    goto label_2;
label_21:
    if (al > 0xfb) {
        goto label_23;
    }
    if (al != 0xf8) {
        goto label_24;
    }
    if (sil <= 0x87) {
        goto label_1;
    }
label_24:
    esi = 5;
    r12d = 5;
    goto label_8;
label_23:
    if (al > 0xfd) {
        goto label_1;
    }
    if (al != 0xfc) {
        goto label_25;
    }
    if (sil <= 0x83) {
        goto label_1;
    }
label_25:
    esi = 6;
    r12d = 6;
    goto label_8;
}

/* /tmp/tmpvkjt68yd @ 0xa390 */
 
uint64_t dbg_register_state (signed int64_t arg_10h, int64_t arg_18h, void * arg_30h, int64_t arg1, int64_t arg3, size_t * size) {
    rdi = arg1;
    rdx = arg3;
    rsi = size;
    /* reg_errcode_t register_state(re_dfa_t const * dfa,re_dfastate_t * newstate,re_hashval_t hash); */
    r14 = rdi;
    r13 = rsi + 0x20;
    r12 = rdx;
    r15 = *((rsi + 0x10));
    *(rsi) = rdx;
    *((rsi + 0x28)) = 0;
    *((rsi + 0x20)) = r15;
    rax = malloc (r15*8);
    *((rbp + 0x30)) = rax;
    if (rax == 0) {
        goto label_2;
    }
    ebx = 0;
    if (r15 > 0) {
        goto label_3;
    }
    goto label_4;
    do {
label_0:
        rbx++;
        if (*((rbp + 0x10)) <= rbx) {
            goto label_4;
        }
label_3:
        rax = *((rbp + 0x18));
        rsi = *((rax + rbx*8));
        rax = *((rax + rbx*8));
        rax <<= 4;
        rax += *(r14);
    } while ((*((rax + 8)) & 8) != 0);
    al = re_node_set_insert_last (r13, rsi);
    if (al != 0) {
        goto label_0;
    }
    do {
label_2:
        eax = 0xc;
        return rax;
label_4:
        rax = *((r14 + 0x40));
        r12 &= *((r14 + 0x88));
        rdx = r12 * 3;
        rbx = rax + rdx*8;
        rdx = *(rbx);
        rdi = *((rbx + 0x10));
        if (*((rbx + 8)) > rdx) {
label_1:
            rax = rdx + 1;
            *(rbx) = rax;
            eax = 0;
            *((rdi + rdx*8)) = rbp;
            return rax;
        }
        rdx++;
        r12 = rdx + rdx;
        rdx <<= 4;
        rax = realloc (rdi, rdx);
        rdi = rax;
    } while (rax == 0);
    *((rbx + 0x10)) = rax;
    rdx = *(rbx);
    *((rbx + 8)) = r12;
    goto label_1;
}

/* /tmp/tmpvkjt68yd @ 0xa490 */
 
int64_t dbg_re_acquire_state_context (int64_t arg_40h, int64_t arg_88h, size_t * arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    size_t * var_10h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* re_dfastate_t * re_acquire_state_context(reg_errcode_t * err,re_dfa_t const * dfa,re_node_set const * nodes,unsigned int context); */
    r13 = rdx;
    rdx = *((rdx + 8));
    *(rdi) = 0;
    *((rsp + 0x10)) = rdi;
    if (rdx == 0) {
        goto label_9;
    }
    r14d = ecx;
    r9d = ecx;
    r14 += rdx;
    if (rdx <= 0) {
        goto label_10;
    }
    rax = *((r13 + 0x10));
    rdx = rax + rdx*8;
    do {
        r14 += *(rax);
        rax += 8;
    } while (rdx != rax);
label_10:
    rax = *((rbp + 0x88));
    rax &= r14;
    rdx = rax * 3;
    rax = *((rbp + 0x40));
    rax = rax + rdx*8;
    r11 = *(rax);
    if (r11 <= 0) {
        goto label_11;
    }
    rbx = *((rax + 0x10));
    r10d = 0;
    while (*(r12) != r14) {
label_0:
        r10++;
        if (r10 == r11) {
            goto label_11;
        }
        r12 = *((rbx + r10*8));
    }
    eax = *((r12 + 0x68));
    eax &= 0xf;
    if (eax != r9d) {
        goto label_0;
    }
    rdi = *((r12 + 0x50));
    rsi = r13;
    al = re_node_set_compare ();
    if (al == 0) {
        goto label_0;
    }
label_2:
    rax = r12;
    return rax;
label_11:
    *((rsp + 4)) = r9d;
    rax = calloc (0x70, 1);
    r12 = rax;
    if (rax == 0) {
        goto label_8;
    }
    r10 = rax + 8;
    rdi = r10;
    *((rsp + 8)) = r10;
    eax = re_node_set_init_copy (rdi, r13, rdx);
    r10 = *((rsp + 8));
    r9d = *((rsp + 4));
    if (eax != 0) {
        goto label_12;
    }
    eax = *((r12 + 0x68));
    edx = r9d;
    *((r12 + 0x50)) = r10;
    edx &= 0xf;
    eax &= 0xfffffff0;
    eax |= edx;
    *((r12 + 0x68)) = al;
    if (*((r13 + 8)) <= 0) {
        goto label_13;
    }
    eax = r9d;
    *((rsp + 0x18)) = r14;
    r11d = 0;
    ebx = 0;
    eax &= 4;
    r15 = r10;
    r14 = r13;
    *((rsp + 8)) = eax;
label_1:
    rax = *((r14 + 0x10));
    rax = *((rax + rbx*8));
    rax <<= 4;
    rax += *(rbp);
    edx = *((rax + 8));
    edi = *((rax + 8));
    edx >>= 8;
    r13d = edx;
    edx &= 0x3ff;
    r13w &= 0x3ff;
    if (edi == 1) {
        if (edx == 0) {
            goto label_14;
        }
    }
    ecx = *((r12 + 0x68));
    eax = *((rax + 0xa));
    r8d = ecx;
    al >>= 4;
    ecx &= 0xffffffdf;
    r8b >>= 5;
    eax |= r8d;
    eax &= 1;
    eax <<= 5;
    ecx |= eax;
    *((r12 + 0x68)) = cl;
    if (edi == 2) {
        goto label_15;
    }
    if (edi == 4) {
        ecx |= 0x40;
        *((r12 + 0x68)) = cl;
    }
label_3:
    if (edx == 0) {
        goto label_14;
    }
    if (r15 == *((r12 + 0x50))) {
        goto label_16;
    }
label_7:
    if ((r13b & 1) == 0) {
        goto label_17;
    }
    if ((r9b & 1) != 0) {
        if ((r13b & 2) != 0) {
            goto label_5;
        }
label_4:
        if ((r13b & 0x10) != 0) {
            if ((r9b & 2) == 0) {
                goto label_5;
            }
        }
        r13d &= 0x40;
        if (r13d == 0) {
            goto label_14;
        }
        eax = *((rsp + 8));
        if (eax != 0) {
            goto label_14;
        }
    }
label_5:
    rax = rbx;
    rax -= r11;
    if (rax < 0) {
        goto label_18;
    }
    rdx = *((r12 + 0x10));
    if (rax >= rdx) {
        goto label_18;
    }
    rdx--;
    *((r12 + 0x10)) = rdx;
    if (rax >= rdx) {
        goto label_18;
    }
    rdx = *((r12 + 0x18));
    do {
        rcx = *((rdx + rax*8 + 8));
        *((rdx + rax*8)) = rcx;
        rax++;
    } while (rax < *((r12 + 0x10)));
label_18:
    r11++;
label_14:
    rbx++;
    if (rbx < *((r14 + 8))) {
        goto label_1;
    }
    r14 = *((rsp + 0x18));
label_13:
    eax = register_state (rbp, r12, *((rsp + 0x18)), rcx, r8, r9);
    if (eax == 0) {
        goto label_2;
    }
label_6:
    free_state (r12, rsi, rdx, rcx, r8, r9);
label_8:
    rax = *((rsp + 0x10));
    r12d = 0;
    *(rax) = 0xc;
    goto label_2;
label_15:
    ecx |= 0x10;
    *((r12 + 0x68)) = cl;
    goto label_3;
label_17:
    if ((r13b & 2) == 0) {
        goto label_4;
    }
    if ((r9b & 1) == 0) {
        goto label_4;
    }
    goto label_5;
label_16:
    *((rsp + 4)) = r9d;
    rax = malloc (0x18);
    r9d = *((rsp + 4));
    rdi = rax;
    if (rax == 0) {
        goto label_6;
    }
    *((r12 + 0x50)) = rax;
    *((rsp + 4)) = r9d;
    eax = re_node_set_init_copy (rdi, r14, rdx);
    r9d = *((rsp + 4));
    if (eax != 0) {
        goto label_6;
    }
    *((r12 + 0x68)) |= 0x80;
    r11d = 0;
    goto label_7;
label_9:
    r12d = 0;
    goto label_2;
label_12:
    free (r12);
    goto label_8;
}

/* /tmp/tmpvkjt68yd @ 0xa7c0 */
 
int64_t dbg_re_acquire_state (int64_t arg_40h, int64_t arg_88h, int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* re_dfastate_t * re_acquire_state(reg_errcode_t * err,re_dfa_t const * dfa,re_node_set const * nodes); */
    r13 = *((rdx + 8));
    *(rdi) = 0;
    if (r13 == 0) {
        goto label_4;
    }
    rbx = rdi;
    r14 = rdx;
    if (r13 <= 0) {
        goto label_5;
    }
    rax = *((rdx + 0x10));
    rdx = rax + r13*8;
    do {
        r13 += *(rax);
        rax += 8;
    } while (rdx != rax);
label_5:
    rax = *((rbp + 0x88));
    rax &= r13;
    rdx = rax * 3;
    rax = *((rbp + 0x40));
    rax = rax + rdx*8;
    r10 = *(rax);
    if (r10 <= 0) {
        goto label_6;
    }
    r11 = *((rax + 0x10));
    r9d = 0;
    do {
        r12 = *((r11 + r9*8));
        if (*(r12) == r13) {
            rdi = r12 + 8;
            rsi = r14;
            al = re_node_set_compare ();
            if (al != 0) {
                goto label_3;
            }
        }
        r9++;
    } while (r9 != r10);
label_6:
    rax = calloc (0x70, 1);
    r12 = rax;
    if (rax == 0) {
        goto label_7;
    }
    r15 = rax + 8;
    eax = re_node_set_init_copy (r15, r14, rdx);
    if (eax != 0) {
        goto label_8;
    }
    rax = *((r14 + 8));
    *((r12 + 0x50)) = r15;
    if (rax <= 0) {
        goto label_9;
    }
    rsi = *((r14 + 0x10));
    r9 = *(rbp);
    r8 = rsi + rax*8;
    while (edi != 1) {
        ecx = *((r12 + 0x68));
        edx = *((rax + 0xa));
        r10d = ecx;
        dl >>= 4;
        ecx &= 0xffffffdf;
        r10b >>= 5;
        edx |= r10d;
        edx &= 1;
        edx <<= 5;
        ecx |= edx;
        *((r12 + 0x68)) = cl;
        if (edi == 2) {
            goto label_10;
        }
        if (edi != 4) {
            goto label_11;
        }
        ecx |= 0x40;
        *((r12 + 0x68)) = cl;
label_0:
        rsi += 8;
        if (r8 == rsi) {
            goto label_9;
        }
label_1:
        rax = *(rsi);
        rax <<= 4;
        rax += r9;
        edi = *((rax + 8));
    }
    if ((*((rax + 8)) & 0x3ff00) == 0) {
        goto label_0;
    }
    edx = *((r12 + 0x68));
    eax = *((rax + 0xa));
    ecx = edx;
    al >>= 4;
    edx &= 0xffffffdf;
    cl >>= 5;
    eax |= ecx;
    eax &= 1;
    eax <<= 5;
    edx |= eax;
    *((r12 + 0x68)) = dl;
label_2:
    rsi += 8;
    *((r12 + 0x68)) |= 0x80;
    if (r8 != rsi) {
        goto label_1;
    }
label_9:
    eax = register_state (rbp, r12, r13, rcx, r8, r9);
    if (eax != 0) {
        goto label_12;
    }
label_3:
    rax = r12;
    return rax;
label_10:
    ecx |= 0x10;
    *((r12 + 0x68)) = cl;
    goto label_0;
label_11:
    if (edi == 0xc) {
        goto label_2;
    }
    if ((*((rax + 8)) & 0x3ff00) == 0) {
        goto label_0;
    }
    goto label_2;
label_8:
    free (r12);
    do {
label_7:
        *(rbx) = 0xc;
        r12d = 0;
        goto label_3;
label_4:
        r12d = 0;
        goto label_3;
label_12:
        free_state (r12, rsi, rdx, rcx, r8, r9);
    } while (1);
}

/* /tmp/tmpvkjt68yd @ 0xa9c0 */
 
int64_t dbg_merge_state_array (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    reg_errcode_t err;
    re_node_set merged_set;
    int64_t var_ch;
    int64_t var_10h;
    void * ptr;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* reg_errcode_t merge_state_array(re_dfa_t const * dfa,re_dfastate_t ** dst,re_dfastate_t ** src,Idx num); */
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    if (rcx <= 0) {
        goto label_1;
    }
    r15 = rdi;
    r13 = rdx;
    r12 = rcx;
    ebx = 0;
    r14 = rsp + 0x10;
    while (rsi != 0) {
        if (rdx != 0) {
            rdx += 8;
            rsi += 8;
            eax = re_node_set_init_union (r14, rsi, rdx, rcx);
            *((rsp + 0xc)) = eax;
            if (eax != 0) {
                goto label_2;
            }
            rax = re_acquire_state (rsp + 0xc, r15, r14, rcx, r8);
            *((rbp + rbx*8)) = rax;
            free (*((rsp + 0x20)));
            eax = *((rsp + 0xc));
            if (eax != 0) {
                goto label_2;
            }
        }
        rbx++;
        if (r12 == rbx) {
            goto label_1;
        }
label_0:
        rsi = *((rbp + rbx*8));
        rdx = *((r13 + rbx*8));
    }
    *((rbp + rbx*8)) = rdx;
    rbx++;
    if (r12 != rbx) {
        goto label_0;
    }
label_1:
    eax = 0;
label_2:
    rdx = *((rsp + 0x28));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0xaa90 */
 
int64_t dbg_sub_epsilon_src_nodes (int64_t arg_28h, int64_t arg_38h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    re_node_set except_nodes;
    int64_t canary;
    int64_t var_10h;
    void * ptr;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* reg_errcode_t sub_epsilon_src_nodes(re_dfa_t const * dfa,Idx node,re_node_set * dest_nodes,re_node_set const * candidates); */
    xmm0 = 0;
    r12 = rdx;
    rdx = rsi * 3;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = *((rdi + 0x38));
    *((rsp + 0x10)) = xmm0;
    *((rsp + 0x20)) = 0;
    r15 = rax + rdx*8;
    if (*((r15 + 8)) <= 0) {
        goto label_7;
    }
    rax = rsp + 0x10;
    r13 = rsi;
    r14 = rcx;
    *((rsp + 8)) = rax;
    ebx = 0;
    goto label_8;
label_1:
    if (rax != 0) {
        goto label_9;
    }
    rax = re_node_set_contains (r12);
    if (rax == 0) {
        goto label_9;
    }
label_2:
    rdx += r8;
    eax = re_node_set_add_intersect (*((rsp + 8)), r14, *((rbp + 0x38)), rcx, r8);
    if (eax != 0) {
        goto label_10;
    }
    do {
label_0:
        rax = *((r15 + 8));
        rbx++;
        if (rax <= rbx) {
            goto label_11;
        }
label_8:
        rax = *((r15 + 0x10));
        rax = *((rax + rbx*8));
    } while (r13 == rax);
    rdx = rax;
    rdx <<= 4;
    rdx += *(rbp);
    if ((*((rdx + 8)) & 8) == 0) {
        goto label_0;
    }
    r8 = rax * 3;
    r10 = *((rbp + 0x28));
    r8 <<= 3;
    r10 += r8;
    r9 = *((r10 + 0x10));
    rsi = *(r9);
    rax = re_node_set_contains (r15);
    if (*((r10 + 8)) > 1) {
        goto label_1;
    }
label_3:
    if (rax != 0) {
        goto label_0;
    }
    rax = re_node_set_contains (r12);
    if (rax == 0) {
        goto label_0;
    }
    goto label_2;
label_9:
    rsi = *((r9 + 8));
    if (rsi <= 0) {
        goto label_0;
    }
    rax = re_node_set_contains (r15);
    goto label_3;
label_11:
    r10 = *((rsp + 0x20));
    if (rax <= 0) {
        goto label_6;
    }
    r11 = *((r15 + 0x10));
    r8d = 0;
    r9 = rsp + 0x10;
    while (rax != 0) {
label_4:
        r8++;
        if (*((r15 + 8)) <= r8) {
            goto label_6;
        }
label_5:
        rsi = *((r11 + r8*8));
        rax = re_node_set_contains (r9);
    }
    rax = re_node_set_contains (r12);
    rdx = rax;
    rdx--;
    if (rdx < 0) {
        goto label_4;
    }
    rsi = *((r12 + 8));
    if (rdx >= rsi) {
        goto label_4;
    }
    rdi = rsi - 1;
    *((r12 + 8)) = rdi;
    if (rax >= rsi) {
        goto label_4;
    }
    rax = *((r12 + 0x10));
    do {
        rsi = *((rax + rdx*8 + 8));
        *((rax + rdx*8)) = rsi;
        rdx++;
    } while (rdx < *((r12 + 8)));
    r8++;
    if (*((r15 + 8)) > r8) {
        goto label_5;
    }
label_6:
    eax = free (r10);
    eax = 0;
    do {
        rdx = *((rsp + 0x28));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_12;
        }
        return rax;
label_10:
        *((rsp + 8)) = eax;
        free (*((rsp + 0x20)));
        eax = *((rsp + 8));
    } while (1);
label_7:
    r10d = 0;
    goto label_6;
label_12:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0xac90 */
 
int64_t dbg_re_node_set_insert (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* _Bool re_node_set_insert(re_node_set * set,Idx elem); */
    rax = *(rdi);
    rbx = rdi;
    if (rax == 0) {
        goto label_3;
    }
    rsi = *((rdi + 8));
    r12 = *((rdi + 0x10));
    if (rsi != 0) {
        goto label_4;
    }
    *(r12) = rbp;
    eax = 1;
    *((rdi + 8))++;
    do {
label_1:
        r12 = rbx;
        return rax;
label_3:
        *(rdi) = 1;
        *((rdi + 8)) = 1;
        rax = malloc (8);
        *((rbx + 0x10)) = rax;
        if (rax == 0) {
            goto label_5;
        }
        *(rax) = rbp;
        eax = 1;
        return rax;
label_4:
        if (rax == rsi) {
            goto label_6;
        }
label_2:
        if (*(r12) <= rbp) {
            goto label_7;
        }
        if (rsi > 0) {
            memmove (r12 + 8, r12, rsi*8);
            esi = 0;
        }
label_0:
        *((r12 + rsi*8)) = rbp;
        eax = 1;
        *((rbx + 8))++;
    } while (1);
label_7:
    rax = *((r12 + rsi*8 - 8));
    if (rbp >= rax) {
        goto label_0;
    }
    do {
        *((r12 + rsi*8)) = rax;
        rsi--;
        rax = *((r12 + rsi*8 - 8));
    } while (rax > rbp);
    goto label_0;
label_5:
    *((rbx + 8)) = 0;
    *(rbx) = 0;
    goto label_1;
label_6:
    rax = rsi + rsi;
    rsi <<= 4;
    *(rdi) = rax;
    rax = realloc (r12, rsi);
    r12 = rax;
    if (rax != 0) {
        *((rbx + 0x10)) = rax;
        rsi = *((rbx + 8));
        goto label_2;
    }
    eax = 0;
    goto label_1;
}

/* /tmp/tmpvkjt68yd @ 0x2810 */
 
void memmove (void) {
    __asm ("bnd jmp qword [reloc.memmove]");
}

/* /tmp/tmpvkjt68yd @ 0x27b0 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmpvkjt68yd @ 0xadb0 */
 
int64_t dbg_duplicate_node_closure (int64_t arg1, int64_t arg2, int64_t arg3, uint32_t arg4, int64_t arg5) {
    uint32_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* reg_errcode_t duplicate_node_closure(re_dfa_t * dfa,Idx top_org_node,Idx top_clone_node,Idx root_node,unsigned int init_constraint); */
label_4:
    r9 = rsi;
    r14 = rdi;
    r12 = rdx;
    *((rsp + 8)) = rcx;
label_1:
    rdi = *(r14);
    rax = r9;
    r8 = *((r14 + 0x28));
    r15 = r12;
    rax <<= 4;
    rax += rdi;
    if (*((rax + 8)) == 4) {
        goto label_7;
    }
    r15 = r9 * 3;
    r15 <<= 3;
    rsi = r8 + r15;
    rcx = *((rsi + 8));
    if (rcx == 0) {
        goto label_8;
    }
    rbx = r12 * 3;
    rsi = *((rsi + 0x10));
    rbx <<= 3;
    r8 += rbx;
    r13 = *(rsi);
    *((r8 + 8)) = 0;
    if (rcx == 1) {
        goto label_9;
    }
    rdx = *((r14 + 0x10));
    rsi = rdx - 1;
    rax = rsi;
    rax <<= 4;
    rax += rdi;
    if ((*((rax + 0xa)) & 4) == 0) {
        goto label_10;
    }
    if (rsi <= 0) {
        goto label_10;
    }
    rdx <<= 4;
    rdi -= rax;
    rcx = *((r14 + 0x20));
    rdi = rdi + rdx - 0x20;
    while (*((rcx + rsi*8)) != r13) {
label_0:
        rax += rdi;
        rsi--;
        if ((*((rax + 0xa)) & 4) == 0) {
            goto label_10;
        }
        if (rsi <= 0) {
            goto label_10;
        }
    }
    edx = *((rax + 8));
    edx >>= 8;
    edx &= 0x3ff;
    if (ebp != edx) {
        goto label_0;
    }
    al = re_node_set_insert (r8, rsi);
    if (al == 0) {
        goto label_2;
    }
label_5:
    rax = *((r14 + 0x28));
    rax = *((rax + r15 + 0x10));
    r9 = *((rax + 8));
    *(rsp) = r9;
    rax = duplicate_node (r14, *((rax + 8)), ebp);
    r12 = rax;
    if (rax == -1) {
        goto label_2;
    }
    rbx += *((r14 + 0x28));
label_3:
    al = re_node_set_insert (rbx, r12);
    r9 = *(rsp);
    if (al != 0) {
        goto label_1;
    }
    do {
label_2:
        eax = 0xc;
label_6:
        return rax;
label_9:
        if (r9 == *((rsp + 8))) {
            if (r9 != r12) {
                goto label_11;
            }
        }
        eax = *((rax + 8));
        eax >>= 8;
        eax &= 0x3ff;
        ebp |= eax;
        rax = duplicate_node (r14, r13, ebp);
        r12 = rax;
    } while (rax == -1);
    rbx += *((r14 + 0x28));
    al = re_node_set_insert (rbx, rax);
    if (al == 0) {
        goto label_2;
    }
    r9 = r13;
    goto label_1;
label_7:
    rax = *((r14 + 0x18));
    r13 = r12 * 3;
    r13 <<= 3;
    rbx = r9*8;
    r9 = *((rax + r9*8));
    *((r8 + r13 + 8)) = 0;
    rsi = r9;
    *(rsp) = r9;
    rax = duplicate_node (r14, rsi, ebp);
    r9 = *(rsp);
    r12 = rax;
    if (rax == -1) {
        goto label_2;
    }
    rax = *((r14 + 0x18));
    rdi = *((r14 + 0x28));
    *(rsp) = r9;
    rcx = *((rax + rbx));
    rdi += r13;
    *((rax + r15*8)) = rcx;
    goto label_3;
label_10:
    rax = duplicate_node (r14, r13, ebp);
    r12 = rax;
    if (rax == -1) {
        goto label_2;
    }
    rdi += rbx;
    al = re_node_set_insert (*((r14 + 0x28)), r12);
    if (al == 0) {
        goto label_2;
    }
    eax = duplicate_node_closure (r14, r13, r12, *((rsp + 8)), ebp);
    goto label_4;
    if (eax == 0) {
        goto label_5;
    }
    goto label_6;
label_8:
    rax = *((r14 + 0x18));
    rcx = *((rax + r9*8));
    *((rax + r12*8)) = rcx;
    eax = 0;
    goto label_6;
label_11:
    eax = re_node_set_insert (r8, r13);
    r8d = eax;
    eax = 0;
    if (r8b != 0) {
        goto label_6;
    }
    goto label_2;
}

/* /tmp/tmpvkjt68yd @ 0xb040 */
 
int64_t dbg_calc_eclosure_iter (int64_t arg_10h, int64_t arg1, int64_t arg2, int64_t arg3, uint32_t arg4) {
    re_node_set eclosure;
    re_node_set eclosure_elem;
    uint32_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    void * var_20h;
    int64_t var_30h;
    void * ptr;
    int64_t var_48h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* reg_errcode_t calc_eclosure_iter(re_node_set * new_set,re_dfa_t * dfa,Idx node,_Bool root); */
label_0:
    r14 = rsi;
    r13 = rdx;
    rbx = rdx * 3;
    rbx <<= 3;
    r12 = *((rsi + 0x28));
    *((rsp + 4)) = ecx;
    r12 += rbx;
    rax = *(fs:0x28);
    *((rsp + 0x48)) = rax;
    eax = 0;
    rax = *((r12 + 8));
    *((rsp + 0x10)) = rdi;
    rdi <<= 3;
    rax = malloc (rax + 1);
    *((rsp + 0x20)) = rax;
    if (rax == 0) {
        goto label_8;
    }
    rsi = *(r14);
    r15 = r13;
    *(rax) = r13;
    r15 <<= 4;
    rax = *((r14 + 0x30));
    *((rsp + 0x18)) = 1;
    rcx = rsi + r15;
    *((rax + rbx + 8)) = 0xffffffffffffffff;
    r8d = *((rcx + 8));
    if ((r8d & 0x3ff00) != 0) {
        if (*((r12 + 8)) == 0) {
            goto label_4;
        }
        rdx = *((r12 + 0x10));
        rdx = *(rdx);
        rdx <<= 4;
        if ((*((rsi + rdx + 0xa)) & 4) == 0) {
            goto label_9;
        }
    }
label_7:
    if ((*((rcx + 8)) & 8) == 0) {
        goto label_4;
    }
    rdi = *((r14 + 0x28));
    rdx = rdi + rbx;
    if (*((rdx + 8)) <= 0) {
        goto label_4;
    }
    r13d = 0;
    r12d = 0;
    while (rsi != 0) {
        rax = *((rcx + 0x10));
        __asm ("movdqu xmm1, xmmword [rcx]");
        r9 = rsp + 0x30;
        *((rsp + 0x40)) = rax;
        *((rsp + 0x30)) = xmm1;
label_1:
        eax = re_node_set_merge (rsp + 0x10, r9, rdx, rcx);
        if (eax != 0) {
            goto label_6;
        }
        rax = *((r14 + 0x30));
        if (*((rax + r15 + 8)) == 0) {
            goto label_10;
        }
label_2:
        rdi = *((r14 + 0x28));
        r12++;
        rdx = rdi + rbx;
        if (*((rdx + 8)) <= r12) {
            goto label_11;
        }
label_3:
        rdx = *((rdx + 0x10));
        rdx = *((rdx + r12*8));
        r15 = rdx * 3;
        r15 <<= 3;
        rcx = rax + r15;
        rsi = *((rcx + 8));
        if (rsi == -1) {
            goto label_12;
        }
    }
    r9 = rsp + 0x30;
    rdi = r9;
    *((rsp + 8)) = r9;
    eax = calc_eclosure_iter (rdi, r14, rdx, 0, r8);
    goto label_0;
    r9 = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    goto label_6;
label_4:
    rdx = *((rsp + 0x20));
    __asm ("movdqa xmm0, xmmword [rsp + 0x10]");
    *((rax + rbx + 0x10)) = rdx;
    __asm ("movups xmmword [rax + rbx], xmm0");
label_5:
    *((rbp + 0x10)) = rdx;
    eax = 0;
    __asm ("movups xmmword [rbp], xmm0");
label_6:
    rdx = *((rsp + 0x48));
    rdx -= *(fs:0x28);
    if (rdx != 0) {
        goto label_13;
    }
    return rax;
label_10:
    r13d = 1;
    free (*((rsp + 0x40)));
    rax = *((r14 + 0x30));
    goto label_2;
label_12:
    rdx = rdi + rbx;
    r13d = 1;
    r12++;
    if (*((rdx + 8)) > r12) {
        goto label_3;
    }
label_11:
    rdx = rax + rbx;
    if (*((rsp + 4)) == 1) {
        goto label_4;
    }
    if (r13b == 0) {
        goto label_4;
    }
    *((rdx + 8)) = 0;
    __asm ("movdqa xmm0, xmmword [rsp + 0x10]");
    rdx = *((rsp + 0x20));
    goto label_5;
label_9:
    r8d >>= 8;
    rcx = r13;
    rdx = r13;
    r8d &= 0x3ff;
    eax = duplicate_node_closure (r14, r13, rdx, rcx, r8);
    if (eax != 0) {
        goto label_6;
    }
    rcx = *(r14);
    rax = *((r14 + 0x30));
    rcx += r15;
    goto label_7;
label_8:
    eax = 0xc;
    goto label_6;
label_13:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0xb2c0 */
 
uint64_t dbg_check_arrival_expand_ecl_sub (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* reg_errcode_t check_arrival_expand_ecl_sub(re_dfa_t const * dfa,re_node_set * dst_nodes,Idx target,Idx ex_subexp,int type); */
label_0:
    r14 = rsi;
    r13 = rcx;
    r12d = r8d;
    rsi = rbp;
    rbx = rdi;
    rax = re_node_set_contains (r14);
    if (rax != 0) {
        goto label_2;
    }
    do {
        rax = rbp;
        rax <<= 4;
        rax += *(rbx);
        edx = *((rax + 8));
        if (edx == r12d) {
            if (*(rax) == r13) {
                goto label_3;
            }
        }
        al = re_node_set_insert (r14, rbp);
        if (al == 0) {
            goto label_4;
        }
        rax = *((rbx + 0x28));
        rbp = rbp + rbp*2;
        rbp <<= 3;
        rax += rbp;
        rdx = *((rax + 8));
        if (rdx == 0) {
            goto label_2;
        }
        rcx = *((rax + 0x10));
        if (rdx == 2) {
            goto label_5;
        }
label_1:
        rax = *((rax + 0x10));
        rbp = *(rax);
        rsi = *(rax);
        rax = re_node_set_contains (r14);
    } while (rax == 0);
label_2:
    eax = 0;
    do {
        return rax;
label_5:
        eax = check_arrival_expand_ecl_sub (rbx, r14, *((rcx + 8)), r13, r12d);
        goto label_0;
    } while (eax != 0);
    rax = *((rbx + 0x28));
    rax += rbp;
    goto label_1;
label_3:
    if (r12d != 9) {
        goto label_2;
    }
    al = re_node_set_insert (r14, rbp);
    if (al != 0) {
        goto label_2;
    }
label_4:
    eax = 0xc;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0xb3b0 */
 
int64_t dbg_check_arrival_expand_ecl (int64_t arg1, int64_t arg3, int64_t arg4, size_t * size) {
    re_node_set new_nodes;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    void * ptr;
    int64_t var_28h;
    rdi = arg1;
    rdx = arg3;
    rcx = arg4;
    rsi = size;
    /* reg_errcode_t check_arrival_expand_ecl(re_dfa_t const * dfa,re_node_set * cur_nodes,Idx ex_subexp,int type); */
    r15d = ecx;
    r14 = rsi;
    r13 = rdi;
    rbx = rdx;
    rbp = *((rsi + 8));
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x10)) = rbp;
    rax = malloc (rbp*8);
    *((rsp + 0x20)) = rax;
    if (rax == 0) {
        goto label_3;
    }
    if (rbp <= 0) {
        goto label_4;
    }
    r12d = 0;
    rbp = rsp + 0x10;
label_1:
    rax = *((r14 + 0x10));
    r9 = *((rax + r12*8));
    rax = *((r13 + 0x30));
    rdx = r9 * 3;
    r10 = rax + rdx*8;
    rsi = *((r10 + 8));
    if (rsi <= 0) {
        goto label_5;
    }
    r8 = *((r10 + 0x10));
    rdi = *(r13);
    edx = 0;
    while (r15d != r11d) {
label_0:
        rdx++;
        if (rsi == rdx) {
            goto label_5;
        }
        rcx = *((r8 + rdx*8));
        rax = *((r8 + rdx*8));
        rax <<= 4;
        rax += rdi;
        r11d = *((rax + 8));
    }
    if (rbx != *(rax)) {
        goto label_0;
    }
    if (rcx == -1) {
        goto label_5;
    }
    eax = check_arrival_expand_ecl_sub (r13, rbp, r9, rbx, r15d);
    if (eax != 0) {
        goto label_6;
    }
label_2:
    r12++;
    if (*((r14 + 8)) > r12) {
        goto label_1;
    }
label_4:
    free (*((r14 + 0x10)));
    rax = *((rsp + 0x20));
    __asm ("movdqa xmm0, xmmword [rsp + 0x10]");
    *((r14 + 0x10)) = rax;
    eax = 0;
    __asm ("movups xmmword [r14], xmm0");
    goto label_7;
label_5:
    eax = re_node_set_merge (rbp, r10, rdx, rcx);
    if (eax == 0) {
        goto label_2;
    }
label_6:
    *((rsp + 0xc)) = eax;
    free (*((rsp + 0x20)));
    eax = *((rsp + 0xc));
    do {
label_7:
        rdx = *((rsp + 0x28));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_8;
        }
        return rax;
label_3:
        eax = 0xc;
    } while (1);
label_8:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0xb510 */
 
int64_t dbg_expand_bkref_cache (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    reg_errcode_t err;
    re_node_set union_set;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_3ch;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    void * ptr;
    int64_t var_68h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* reg_errcode_t expand_bkref_cache(re_match_context_t * mctx,re_node_set * cur_nodes,Idx cur_str,Idx subexp_num,int type); */
    rsi = rdx;
    r13 = *((rdi + 0x98));
    *((rsp + 8)) = rdx;
    *((rsp + 0x30)) = rcx;
    *((rsp + 0x3c)) = r8d;
    rax = *(fs:0x28);
    *((rsp + 0x68)) = rax;
    eax = 0;
    rax = search_cur_bkref_entry ();
    if (rax == -1) {
        goto label_6;
    }
    rax *= 3;
    r12 = rdi;
    rax <<= 4;
    *((rsp + 0x28)) = rax;
    rax = rsp + 0x4c;
    *((rsp + 0x20)) = rax;
label_2:
    rbx = *((rsp + 0x28));
    rbx += *((r12 + 0xd8));
    goto label_7;
label_0:
    rax = *((r13 + 0x18));
    r11 = *((r12 + 0xb8));
    r10 = *((rax + rsi*8));
    rax = r14*8;
    r11 += rax;
    *((rsp + 0x10)) = rax;
    rax = *(r11);
    if (rax == 0) {
        goto label_8;
    }
    r11 = rax + 8;
    rsi = r10;
    *((rsp + 0x18)) = r10;
    rax = re_node_set_contains (r11);
    if (rax != 0) {
        goto label_1;
    }
    r15 = rsp + 0x50;
    eax = re_node_set_init_copy (r15, r11, rdx);
    *((rsp + 0x4c)) = eax;
    al = re_node_set_insert (r15, *((rsp + 0x18)));
    edx = *((rsp + 0x4c));
    if (edx != 0) {
        goto label_9;
    }
    if (al != 1) {
        goto label_9;
    }
    r11 = *((rsp + 0x10));
    r11 += *((r12 + 0xb8));
label_3:
    *((rsp + 0x10)) = r11;
    rax = re_acquire_state (*((rsp + 0x20)), r13, r15, rcx, r8);
    r11 = *((rsp + 0x10));
    *(r11) = rax;
    free (*((rsp + 0x60)));
    rax = *((r12 + 0xb8));
    if (*((rax + r14*8)) == 0) {
        goto label_10;
    }
    do {
label_1:
        rbx += 0x30;
        if (*((rbx - 8)) == 0) {
            goto label_6;
        }
label_7:
        rsi = *(rbx);
        rax = re_node_set_contains (rbp);
    } while (rax == 0);
    rax = *((rsp + 8));
    r8 = *((rbx + 0x18));
    r8 += rax;
    r14 = r8;
    r14 -= *((rbx + 0x10));
    if (rax != r14) {
        goto label_0;
    }
    rcx = *((r13 + 0x28));
    rax = rsi * 3;
    rax = rcx + rax*8;
    rax = *((rax + 0x10));
    r14 = *(rax);
    rsi = *(rax);
    rax = re_node_set_contains (rbp);
    if (rax != 0) {
        goto label_1;
    }
    *((rsp + 0x50)) = 1;
    *((rsp + 0x58)) = 1;
    rax = malloc (8);
    *((rsp + 0x60)) = rax;
    if (rax == 0) {
        goto label_11;
    }
    *(rax) = r14;
    eax = 0;
label_5:
    *((rsp + 0x4c)) = eax;
    r15 = rsp + 0x50;
    eax = check_arrival_expand_ecl (r13, r15, *((rsp + 0x30)), *((rsp + 0x3c)));
    r14d = eax;
    eax = re_node_set_merge (rbp, r15, rdx, rcx);
    ebx = eax;
    free (*((rsp + 0x60)));
    eax = *((rsp + 0x4c));
    edx = ebx;
    edx |= r14d;
    edx |= eax;
    if (edx == 0) {
        goto label_2;
    }
    if (eax == 0) {
        eax = ebx;
        if (r14d != 0) {
            eax = r14d;
        }
        goto label_4;
label_8:
        *((rsp + 0x18)) = r10;
        *((rsp + 0x10)) = r11;
        *((rsp + 0x50)) = 1;
        *((rsp + 0x58)) = 1;
        rax = malloc (8);
        *((rsp + 0x60)) = rax;
        if (rax == 0) {
            goto label_12;
        }
        r10 = *((rsp + 0x18));
        r11 = *((rsp + 0x10));
        r15 = rsp + 0x50;
        *((rsp + 0x4c)) = 0;
        *(rax) = r10;
        goto label_3;
label_6:
        eax = 0;
    }
label_4:
    rdx = *((rsp + 0x68));
    rdx -= *(fs:0x28);
    if (rdx != 0) {
        goto label_13;
    }
    return rax;
label_10:
    eax = *((rsp + 0x4c));
    if (eax == 0) {
        goto label_1;
    }
    goto label_4;
label_9:
    free (*((rsp + 0x60)));
    eax = *((rsp + 0x4c));
    if (eax != 0) {
        goto label_4;
    }
label_12:
    eax = 0xc;
    goto label_4;
label_11:
    *((rsp + 0x58)) = 0;
    eax = 0xc;
    *((rsp + 0x50)) = 0;
    goto label_5;
label_13:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0xb810 */
 
int64_t dbg_sift_states_backward (int64_t arg1, int64_t arg2) {
    re_node_set cur_dest;
    int64_t var_8h;
    size_t n;
    int64_t var_18h;
    signed int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_3ch;
    int64_t var_40h;
    int64_t var_48h;
    void * ptr;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    /* reg_errcode_t sift_states_backward(re_match_context_t const * mctx,re_sift_context_t * sctx); */
    r15 = rsi;
    r12 = rdi;
    rbp = *((rsi + 0x18));
    rbx = *((rsi + 0x10));
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    *((rsp + 0x40)) = 1;
    *((rsp + 0x48)) = 1;
    rax = malloc (8);
    *((rsp + 0x50)) = rax;
    if (rax == 0) {
        goto label_7;
    }
    *(rax) = rbx;
    *((rsp + 8)) = rcx;
    eax = update_cur_sifted_state (r12, r15, rbp, rsp + 0x40);
    if (eax != 0) {
        goto label_8;
    }
    rax = r15 + 0x20;
    *((rsp + 0x3c)) = 0;
    *((rsp + 0x30)) = rax;
    *(rsp) = r15;
label_2:
    if (rbp <= 0) {
        goto label_9;
    }
    rax = *(rsp);
    rdi = *(rax);
    rax = rbp*8;
    *((rsp + 0x10)) = rax;
    if (*((rdi + rbp*8)) == 0) {
        goto label_10;
    }
    *((rsp + 0x3c)) = 0;
    eax = *((rsp + 0x3c));
    if (*((r12 + 0xe0)) < eax) {
        goto label_11;
    }
label_4:
    rax = *((r12 + 0xb8));
    rdx = *((rsp + 0x10));
    rbp--;
    *((rsp + 0x48)) = 0;
    r13 = *((rax + rdx - 8));
    if (r13 == 0) {
        goto label_12;
    }
    r14 = *((r12 + 0x98));
    if (*((r13 + 0x28)) <= 0) {
        goto label_12;
    }
    ebx = 0;
    while ((*((rsi + 0xa)) & 0x10) == 0) {
label_1:
        al = check_node_accept (r12, rsi, rbp);
        if (al != 0) {
            rax = *(rsp);
            rcx = *((rsp + 0x10));
            rax = *(rax);
            rdi = *((rax + rcx));
            if (rdi == 0) {
                goto label_13;
            }
            rax = *((r14 + 0x18));
            rdi += 8;
            rsi = *((rax + r15*8));
            rax = re_node_set_contains (rdi);
            if (rax == 0) {
                goto label_13;
            }
            r9d = 1;
label_0:
            rax = *(rsp);
            if (*((rax + 0x28)) != 0) {
                rax = *((r14 + 0x18));
                rcx = (int64_t) r9d;
                rcx += rbp;
                al = check_dst_limits (r12, *((rsp + 0x30)), *((rax + r15*8)), rcx, r15, rbp);
                if (al != 0) {
                    goto label_13;
                }
            }
            al = re_node_set_insert (*((rsp + 8)), r15);
            if (al == 0) {
                goto label_14;
            }
        }
label_13:
        rbx++;
        if (rbx >= *((r13 + 0x28))) {
            goto label_12;
        }
        rax = *((r13 + 0x30));
        rsi = *(r14);
        r15 = *((rax + rbx*8));
        r10 = *((rax + rbx*8));
        r10 <<= 4;
        rsi += r10;
    }
    rax = *(rsp);
    *((rsp + 0x18)) = r10;
    r11 = *((r12 + 0x98));
    rcx = *((rax + 0x18));
    rdi = r11;
    *((rsp + 0x28)) = r11;
    *((rsp + 0x20)) = rcx;
    eax = check_node_accept_bytes (rdi, r15, r12, rbp);
    r10 = *((rsp + 0x18));
    r9d = eax;
    if (eax <= 0) {
        goto label_15;
    }
    rax = (int64_t) eax;
    rax += rbp;
    if (*((rsp + 0x20)) < rax) {
        goto label_0;
    }
    rdx = *(rsp);
    r11 = *((rsp + 0x28));
    rdx = *(rdx);
    rdi = *((rdx + rax*8));
    if (rdi == 0) {
        goto label_3;
    }
    rax = *((r11 + 0x18));
    rdi += 8;
    rsi = *((rax + r15*8));
    rax = re_node_set_contains (rdi);
    if (rax != 0) {
        goto label_0;
    }
label_3:
    rsi = *(r14);
    rsi += r10;
    goto label_1;
label_12:
    eax = update_cur_sifted_state (r12, *(rsp), rbp, *((rsp + 8)));
    if (eax == 0) {
        goto label_2;
    }
label_8:
    r13d = eax;
    goto label_6;
    if (eax == 0) {
label_15:
        goto label_3;
    }
    goto label_0;
label_14:
    r13d = 0xc;
label_6:
    free (*((rsp + 0x50)));
label_5:
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_16;
    }
    eax = r13d;
    return rax;
label_10:
    eax = *((rsp + 0x3c));
    if (*((r12 + 0xe0)) >= eax) {
        goto label_4;
    }
label_11:
    r13d = 0;
    memset (rdi, 0, *((rsp + 0x10)));
    free (*((rsp + 0x50)));
    goto label_5;
label_9:
    r13d = 0;
    goto label_6;
label_7:
    r13d = 0xc;
    goto label_5;
label_16:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0xbb20 */
 
int64_t dbg_update_cur_sifted_state (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_bp_30h;
    int64_t var_bp_38h;
    re_sift_context_t local_sctx;
    void ** var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    uint32_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    void ** var_6ch;
    uint32_t var_70h;
    int64_t var_80h;
    int64_t var_88h;
    int64_t var_90h;
    signed int64_t var_98h;
    void * ptr;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* reg_errcode_t update_cur_sifted_state(re_match_context_t const * mctx,re_sift_context_t * sctx,Idx str_idx,re_node_set * dest_nodes); */
    r14 = rdx;
    r12 = rcx;
    rbx = rsi;
    rbp = *((rdi + 0x98));
    *((rsp + 0x10)) = rdi;
    rax = *(fs:0x28);
    *((rsp + 0xa8)) = rax;
    eax = 0;
    rax = rdx*8;
    rdx = *((r12 + 8));
    *((rsp + 0x6c)) = 0;
    *((rsp + 0x20)) = rax;
    rax += *((rdi + 0xb8));
    rcx = *(rax);
    *((rsp + 0x18)) = rcx;
    if (rcx == 0) {
        goto label_17;
    }
    if (rdx != 0) {
        goto label_18;
    }
    rdx = *(rsi);
    *((rdx + r14*8)) = 0;
    rdx = *(rax);
    eax = 0;
    if ((*((rdx + 0x68)) & 0x40) != 0) {
        goto label_19;
    }
    do {
label_0:
        rdx = *((rsp + 0xa8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_20;
        }
        return rax;
label_18:
        *((rsp + 0x70)) = 0;
        rax = re_acquire_state (rsp + 0x70, rbp, r12, rcx, r8);
        rdx = rax;
        eax = *((rsp + 0x70));
    } while (eax != 0);
    r13 = *((rdx + 0x38));
    r15 = rdx + 0x38;
    if (r13 != 0) {
        goto label_21;
    }
    *((rdx + 0x40)) = 0;
    *((rsp + 8)) = rdx;
    *((rdx + 0x38)) = rdi;
    rdi <<= 3;
    rax = malloc (*((r12 + 8)));
    rdx = *((rsp + 8));
    *((rdx + 0x48)) = rax;
    if (rax == 0) {
        goto label_22;
    }
    *((rsp + 0x70)) = 0;
    if (*((r12 + 8)) > 0) {
        goto label_23;
    }
    goto label_21;
    do {
        r13++;
        if (r13 >= *((r12 + 8))) {
            goto label_21;
        }
label_23:
        rax = *((r12 + 0x10));
        rax = *((rax + r13*8));
        rax = *((rbp + 0x38));
        eax = re_node_set_merge (r15, rax + rdx*8, rax + rax*2, rcx);
        *((rsp + 0x70)) = eax;
    } while (eax == 0);
label_22:
    eax = 0xc;
    goto label_0;
label_21:
    rax = *((rsp + 0x18));
    rax += 8;
    rsi = rax;
    *((rsp + 0x30)) = rax;
    eax = re_node_set_add_intersect (r12, rsi, r15, rcx, r8);
    *((rsp + 0x6c)) = eax;
    if (eax != 0) {
        goto label_0;
    }
    if (*((rbx + 0x28)) != 0) {
        goto label_24;
    }
label_9:
    r13 = *((rsp + 0x20));
    r13 += *(rbx);
    rax = re_acquire_state (rsp + 0x6c, rbp, r12, rcx, r8);
    *(r13) = rax;
    eax = *((rsp + 0x6c));
    if (eax != 0) {
        goto label_0;
    }
    rsi = *((rsp + 0x10));
    rax = *((rsp + 0x20));
    rax += *((rsi + 0xb8));
    rdx = *(rax);
    eax = 0;
    if ((*((rdx + 0x68)) & 0x40) == 0) {
        goto label_0;
    }
label_19:
    rdi = *((rsp + 0x10));
    rsi = r14;
    rax = search_cur_bkref_entry ();
    r15 = *((rdi + 0x98));
    *((rsp + 0x50)) = rax;
    if (rax == -1) {
        goto label_25;
    }
    *((rsp + 0x70)) = 0;
    rax = *((rsp + 0x18));
    if (*((rax + 0x10)) <= 0) {
        goto label_25;
    }
    rax = *((rsp + 0x50));
    r13 = rbx;
    *((rsp + 0x40)) = 0;
    *((rsp + 8)) = 0;
    rax *= 3;
    rax <<= 4;
    *((rsp + 0x58)) = rax;
    rax = rbx + 0x20;
    *((rsp + 0x38)) = rax;
    while (r12 != *((r13 + 0x10))) {
        if (al == 4) {
            goto label_26;
        }
label_1:
        rbx = *((rsp + 0x18));
        rax = *((rsp + 8));
        if (rax >= *((rbx + 0x10))) {
            goto label_27;
        }
        rax = *((rsp + 0x18));
        rbx = *((rsp + 8));
        rax = *((rax + 0x18));
        r12 = *((rax + rbx*8));
        rax = *((rax + rbx*8));
        rax <<= 4;
        rax += *(r15);
        eax = *((rax + 8));
    }
    if (r14 == *((r13 + 0x18))) {
        goto label_1;
    }
    if (al != 4) {
        goto label_1;
    }
label_26:
    rax = *((rsp + 0x10));
    rbx = *((rsp + 0x58));
    rbp = *((rsp + 0x50));
    rbx += *((rax + 0xd8));
    rax = r12 * 3;
    rax <<= 3;
    *((rsp + 0x30)) = rax;
    rax = r12*8;
    *((rsp + 0x28)) = rax;
    rax = r13;
    r13 = rbp;
    rbx = rax;
label_2:
    if (r12 != *(rbp)) {
        goto label_28;
    }
    rax = *((rbp + 0x18));
    rax -= *((rbp + 0x10));
    r9 = r14 + rax;
    if (rax == 0) {
        goto label_29;
    }
    rax = *((r15 + 0x18));
    rcx = *((rsp + 0x28));
    r8 = *((rax + rcx));
label_4:
    if (r9 > *((rbx + 0x18))) {
        goto label_28;
    }
    rax = *(rbx);
    rdi = *((rax + r9*8));
    if (rdi == 0) {
        goto label_28;
    }
    rdi += 8;
    rsi = r8;
    rax = re_node_set_contains (rdi);
    if (rax == 0) {
        goto label_28;
    }
    al = check_dst_limits (*((rsp + 0x10)), *((rsp + 0x38)), r12, r14, r8, r9);
    if (al != 0) {
        goto label_28;
    }
    if (*((rsp + 0x40)) == 0) {
        goto label_30;
    }
    rbp = rsp + 0x90;
label_5:
    r8 = rsp + 0x70;
    *((rsp + 0x80)) = r12;
    *((rsp + 0x40)) = r8;
    *((rsp + 0x88)) = r14;
    al = re_node_set_insert (rbp, r13);
    r8 = *((rsp + 0x40));
    if (al == 0) {
        goto label_31;
    }
    rsi = *((rsp + 0x20));
    rax = *((rsp + 0x70));
    rax = *((rax + rsi));
    *((rsp + 0x48)) = rax;
    eax = sift_states_backward (*((rsp + 0x10)), r8);
    if (eax != 0) {
        goto label_15;
    }
    rsi = *((rbx + 8));
    rdx = *((rsp + 0x70));
    if (rsi != 0) {
        eax = merge_state_array (r15, rsi, rdx, r14 + 1);
        if (eax != 0) {
            goto label_15;
        }
        rdx = *((rsp + 0x70));
    }
    rcx = *((rsp + 0x20));
    rsi = *((rsp + 0x48));
    *((rsp + 0x40)) = rdx;
    *((rdx + rcx)) = rsi;
    rsi = r13;
    rax = re_node_set_contains (rbp);
    rdx = rax;
    rdx--;
    if (rdx < 0) {
        goto label_32;
    }
    rcx = *((rsp + 0x98));
    if (rdx >= rcx) {
        goto label_32;
    }
    rsi = rcx - 1;
    *((rsp + 0x98)) = rsi;
    if (rax >= rcx) {
        goto label_32;
    }
    rax = *((rsp + 0xa0));
    do {
        rcx = *((rax + rdx*8 + 8));
        *((rax + rdx*8)) = rcx;
        rdx++;
    } while (rdx < *((rsp + 0x98)));
label_32:
    rcx = *((rsp + 0x10));
    rax = r13 * 3;
    rax <<= 4;
    rax += *((rcx + 0xd8));
label_3:
    r13++;
    rbp = rax + 0x30;
    if (*((rax + 0x28)) != 0) {
        goto label_2;
    }
    r13 = rbx;
    goto label_1;
label_17:
    rax = *((rsp + 0x20));
    rax += *(rsi);
    rbx = rax;
    if (rdx == 0) {
        *(rax) = 0;
        eax = 0;
        goto label_0;
    }
    rax = re_acquire_state (rsp + 0x6c, rbp, r12, rcx, r8);
    *(rbx) = rax;
    eax = *((rsp + 0x6c));
    goto label_0;
label_27:
    if (*((rsp + 0x40)) != 0) {
        goto label_33;
    }
label_25:
    eax = 0;
    goto label_0;
label_28:
    rax = rbp;
    goto label_3;
label_29:
    rax = *((r15 + 0x28));
    rcx = *((rsp + 0x30));
    rax = *((rax + rcx + 0x10));
    r8 = *(rax);
    goto label_4;
label_30:
    __asm ("movdqu xmm0, xmmword [rbx]");
    rax = *((rbx + 0x30));
    rbp = rsp + 0x90;
    __asm ("movdqu xmm1, xmmword [rbx + 0x10]");
    __asm ("movdqu xmm2, xmmword [rbx + 0x20]");
    *((rsp + 0xa0)) = rax;
    *((rsp + 0x70)) = xmm0;
    *((rsp + 0x80)) = xmm1;
    *((rsp + 0x90)) = xmm2;
    eax = re_node_set_init_copy (rbp, *((rsp + 0x38)), rdx);
    if (eax == 0) {
        goto label_5;
    }
label_15:
    if (*((rsp + 0x70)) == 0) {
        goto label_0;
    }
label_16:
    *((rsp + 8)) = eax;
    free (*((rsp + 0xa0)));
    eax = *((rsp + 8));
    goto label_0;
label_24:
    rax = *((rsp + 0x10));
    rax = *((rax + 0xd8));
    *((rsp + 0x28)) = rax;
    if (*((rsp + 0x70)) <= 0) {
        goto label_34;
    }
    *((rsp + 8)) = r14;
    r13d = 0;
label_8:
    rax = *((rbx + 0x30));
    rdi = *((rsp + 8));
    rax = *((rax + r13*8));
    rdx = rax * 3;
    rdx <<= 4;
    rdx += *((rsp + 0x28));
    if (rdi <= *((rdx + 0x10))) {
        goto label_10;
    }
    if (rdi > *((rdx + 8))) {
        goto label_10;
    }
    rcx = *(rdx);
    rax = *(rbp);
    rsi = *((r12 + 8));
    rcx <<= 4;
    r15 = *((rax + rcx));
    if (rdi == *((rdx + 0x18))) {
        goto label_35;
    }
    r14d = 0;
    if (rsi <= 0) {
        goto label_10;
    }
    rsi = r14;
    r14 = rbp;
    r15 = rsi;
    while (edx > 1) {
label_6:
        r15++;
        if (r15 >= *((r12 + 8))) {
            goto label_36;
        }
label_7:
        rax = *(r14);
        rdx = *((r12 + 0x10));
        rsi = *((rdx + r15*8));
        rdx = *((rdx + r15*8));
        rdx <<= 4;
        rax += rdx;
        edx = *((rax + 8));
        edx -= 8;
    }
    if (rbp != *(rax)) {
        goto label_6;
    }
    eax = sub_epsilon_src_nodes (r14, rsi, r12, *((rsp + 0x30)), r8, r9);
    if (eax != 0) {
        goto label_0;
    }
    r15++;
    if (r15 < *((r12 + 8))) {
        goto label_7;
    }
label_36:
label_10:
    r13++;
    if (r13 < *((rbx + 0x28))) {
        goto label_8;
    }
    r14 = *((rsp + 8));
label_34:
    *((rsp + 0x6c)) = 0;
    goto label_9;
label_35:
    if (rsi <= 0) {
        goto label_10;
    }
    r10 = *((r12 + 0x10));
    r14 = 0xffffffffffffffff;
    r9 = 0xffffffffffffffff;
    rcx = r10;
    r11 = r10 + rsi*8;
    while (esi != 8) {
        if (esi == 9) {
            if (r15 != *(rdx)) {
                r14 = rdi;
                goto label_37;
            }
        }
label_37:
        rcx += 8;
        if (r11 == rcx) {
            goto label_38;
        }
label_11:
        rdi = *(rcx);
        rdx = *(rcx);
        rdx <<= 4;
        rdx += rax;
        esi = *((rdx + 8));
    }
    if (r15 == *(rdx)) {
        r9 = rdi;
    }
    rcx += 8;
    if (r11 != rcx) {
        goto label_11;
    }
label_38:
    if (r9 >= 0) {
        goto label_39;
    }
    if (r14 < 0) {
        goto label_10;
    }
label_14:
    r15d = 0;
    *((rsp + 0x38)) = rbx;
    rbx = r15;
    r15 = *((rsp + 0x30));
    while (rax != 0) {
label_12:
        rbx++;
label_13:
        if (*((r12 + 8)) <= rbx) {
            goto label_40;
        }
        r10 = *((r12 + 0x10));
        r10 = *((r10 + rbx*8));
        rsi = r14;
        r8 = r10 * 3;
        r8 <<= 3;
        rdi += r8;
        rax = re_node_set_contains (*((rbp + 0x38)));
    }
    rdi += r8;
    rax = re_node_set_contains (*((rbp + 0x30)));
    if (rax != 0) {
        goto label_12;
    }
    eax = sub_epsilon_src_nodes (rbp, r10, r12, r15, r8, r9);
    if (eax == 0) {
        goto label_13;
    }
    goto label_0;
label_39:
    eax = sub_epsilon_src_nodes (rbp, r9, r12, *((rsp + 0x30)), r8, r9);
    if (eax != 0) {
        goto label_0;
    }
    if (r14 < 0) {
        goto label_10;
    }
    if (*((r12 + 8)) <= 0) {
        goto label_10;
    }
    r10 = *((r12 + 0x10));
    goto label_14;
label_31:
    eax = 0xc;
    goto label_15;
label_40:
    rbx = *((rsp + 0x38));
    goto label_10;
label_20:
    eax = stack_chk_fail ();
label_33:
    eax = 0;
    goto label_16;
}

/* /tmp/tmpvkjt68yd @ 0xc300 */
 
int64_t dbg_extend_buffers (int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_58h, int64_t arg_78h, uint32_t arg_88h, int64_t arg_90h, void * arg_b8h, int64_t arg1, uint32_t arg2) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_24h;
    int64_t var_28h;
    int64_t var_34h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_88h;
    rdi = arg1;
    rsi = arg2;
    /* reg_errcode_t extend_buffers(re_match_context_t * mctx,int min_len); */
    rdx = 0xffffffffffffffe;
    rax = *((rdi + 0x40));
    if (rax > rdx) {
        goto label_1;
    }
    rdx = *((rdi + 0x58));
    rax += rax;
    rsi = (int64_t) esi;
    if (rax > rdx) {
        rax = rdx;
    }
    if (rax >= rsi) {
        rsi = rax;
    }
    eax = re_string_realloc_buffers (rdi, rsi);
    r12d = eax;
    if (eax != 0) {
        goto label_0;
    }
    rdi = *((rbp + 0xb8));
    if (rdi != 0) {
        rax = *((rbp + 0x40));
        rax = realloc (rdi, rax*8 + 8);
        if (rax == 0) {
            goto label_1;
        }
        *((rbp + 0xb8)) = rax;
    }
    eax = *((rbp + 0x90));
    if (*((rbp + 0x88)) == 0) {
        goto label_2;
    }
    rdi = rbp;
    if (eax <= 1) {
        goto label_3;
    }
    void (*0x7270)() ();
    do {
        build_wcs_buffer (rbp, rsi);
label_0:
        eax = r12d;
        return rax;
label_2:
    } while (eax > 1);
    rcx = *((rbp + 0x78));
    if (rcx == 0) {
        goto label_0;
    }
    rax = *((rbp + 0x58));
    rsi = *((rbp + 0x40));
    if (rsi > rax) {
        rsi = rax;
    }
    rax = *((rbp + 0x30));
    if (rsi > rax) {
        goto label_4;
    }
    goto label_5;
    do {
        rcx = *((rbp + 0x78));
label_4:
        rdx = *(rbp);
        rdx += rax;
        rdx += *((rbp + 0x28));
        edx = *(rdx);
        ecx = *((rcx + rdx));
        rdx = *((rbp + 8));
        *((rdx + rax)) = cl;
        rax++;
    } while (rsi != rax);
    do {
        *((rbp + 0x30)) = rsi;
        eax = r12d;
        *((rbp + 0x38)) = rsi;
        return rax;
label_3:
        build_upper_buffer (rdi, rsi, rdx, rcx, r8, r9);
        eax = r12d;
        return rax;
label_1:
        r12d = 0xc;
        eax = r12d;
        return rax;
label_5:
        rsi = rax;
    } while (1);
}

/* /tmp/tmpvkjt68yd @ 0xc450 */
 
int64_t dbg_clean_state_log_if_needed (int64_t arg_1h, signed int64_t arg1, signed int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* reg_errcode_t clean_state_log_if_needed(re_match_context_t * mctx,Idx next_state_log_idx); */
    rax = *((rdi + 0x40));
    rbx = rdi;
    r12 = *((rdi + 0xc0));
    if (rax > rsi) {
        goto label_3;
    }
    if (rax >= *((rdi + 0x58))) {
        goto label_3;
    }
    eax = extend_buffers (rbx, rbp + 1, rdx, rcx, r8, r9);
    while (rax > rbp) {
label_0:
        if (r12 < rbp) {
            goto label_4;
        }
label_2:
        eax = 0;
label_1:
        return rax;
label_3:
        rax = *((rbx + 0x30));
    }
    if (rax >= *((rbx + 0x58))) {
        goto label_0;
    }
    eax = extend_buffers (rbx, rbp + 1, rdx, rcx, r8, r9);
    if (eax == 0) {
        goto label_0;
    }
    goto label_1;
label_4:
    rax = *((rbx + 0xb8));
    rdx -= r12;
    rdx <<= 3;
    memset (rax + r12*8 + 8, 0, rbp);
    *((rbx + 0xc0)) = rbp;
    goto label_2;
}

/* /tmp/tmpvkjt68yd @ 0xc4e0 */
 
int64_t dbg_free_dfa_content (void * ptr, void * arg_10h, void * arg_18h, uint32_t arg1) {
    rdi = arg1;
    /* void free_dfa_content(re_dfa_t * dfa); */
    r12 = rdi;
    rdx = *(rdi);
    if (rdx == 0) {
        goto label_3;
    }
    if (*((rdi + 0x10)) == 0) {
        goto label_3;
    }
    ebx = 0;
    while (eax != 6) {
        if (eax == 3) {
            goto label_4;
        }
label_2:
        rbx++;
        if (*((r12 + 0x10)) <= rbx) {
            goto label_3;
        }
label_0:
        rdx = *(r12);
        rax = rbx;
        rax <<= 4;
        rdx += rax;
        eax = *((rdx + 8));
        eax &= 0x400ff;
    }
    rbp = *(rdx);
    rbx++;
    free (*(rbp));
    free (*((rbp + 8)));
    free (*((rbp + 0x10)));
    free (*((rbp + 0x18)));
    rax = free (rbp);
    if (*((r12 + 0x10)) > rbx) {
        goto label_0;
    }
label_3:
    ebx = 0;
    ebp = 0;
    free (*((r12 + 0x18)));
    if (*((r12 + 0x10)) == 0) {
        goto label_5;
    }
    do {
        rax = *((r12 + 0x30));
        if (rax != 0) {
            free (*((rax + rbx + 0x10)));
        }
        rax = *((r12 + 0x38));
        if (rax != 0) {
            free (*((rax + rbx + 0x10)));
        }
        rax = *((r12 + 0x28));
        if (rax != 0) {
            free (*((rax + rbx + 0x10)));
        }
        rbp++;
        rbx += 0x18;
    } while (*((r12 + 0x10)) > rbp);
label_5:
    r13d = 0;
    free (*((r12 + 0x28)));
    free (*((r12 + 0x30)));
    free (*((r12 + 0x38)));
    free (*(r12));
    rdi = *((r12 + 0x40));
    if (rdi == 0) {
        goto label_6;
    }
label_1:
    rax = r13 * 3;
    ebx = 0;
    rbp = rdi + rax*8;
    if (*(rbp) <= 0) {
        goto label_7;
    }
    do {
        rax = *((rbp + 0x10));
        rbx++;
        free_state (*((rax + rbx*8)), rsi, rdx, rcx, r8, r9);
    } while (*(rbp) > rbx);
label_7:
    r13++;
    free (*((rbp + 0x10)));
    if (*((r12 + 0x88)) >= r13) {
        rdi = *((r12 + 0x40));
        goto label_1;
    }
label_6:
    free (*((r12 + 0x40)));
    rdi = *((r12 + 0x78));
    rax = obj_utf8_sb_map;
    if (rdi != rax) {
        free (rdi);
    }
    free (*((r12 + 0xe0)));
    rdi = r12;
    void (*0x24d0)() ();
label_4:
    free (*(rdx));
    goto label_2;
}

/* /tmp/tmpvkjt68yd @ 0xc6b0 */
 
int32_t dbg_free_tree (void * arg_8h, void * arg_10h, void * arg_18h, void ** ptr) {
    rsi = ptr;
    /* reg_errcode_t free_tree(void * extra,bin_tree_t * node); */
    eax = *((rsi + 0x30));
    eax &= 0x400ff;
    if (eax != 6) {
        if (eax == 3) {
            goto label_0;
        }
        eax = 0;
        return eax;
    }
    rbp = *((rsi + 0x28));
    free (*(rbp));
    free (*((rbp + 8)));
    free (*((rbp + 0x10)));
    free (*((rbp + 0x18)));
    eax = free (rbp);
    eax = 0;
    return eax;
label_0:
    eax = free (*((rsi + 0x28)));
    eax = 0;
    return eax;
}

/* /tmp/tmpvkjt68yd @ 0xc720 */
 
int64_t dbg_check_arrival (int64_t arg_48h, int64_t arg_98h, int64_t arg_a0h, int64_t arg_b8h, int64_t arg_e0h, int64_t arg_110h, int64_t arg1, void * arg2, int64_t arg3, int64_t arg4, int64_t arg5, void ** arg6) {
    int32_t type;
    reg_errcode_t err;
    re_node_set next_nodes;
    re_node_set union_set;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    signed int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    void ** var_50h;
    int64_t var_58h;
    void * s;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_78h;
    int64_t var_88h;
    int64_t var_8ch;
    int64_t var_90h;
    uint32_t var_98h;
    void * var_a0h;
    int64_t var_b0h;
    int64_t var_b8h;
    void * ptr;
    int64_t var_c8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* reg_errcode_t check_arrival(re_match_context_t * mctx,state_array_t * path,Idx top_node,Idx top_str,Idx last_node,Idx last_str,int type); */
    r14 = rdx;
    rbx = rcx;
    rcx = *((rdi + 0x98));
    r13 = *((rsi + 8));
    *((rsp + 0x60)) = rsi;
    *((rsp + 0x78)) = r8;
    *((rsp + 0x50)) = r9;
    rax = *(fs:0x28);
    *((rsp + 0xc8)) = rax;
    eax = 0;
    rax = rdx;
    *((rsp + 0x38)) = rcx;
    *((rsp + 0x88)) = 0;
    rax <<= 4;
    rax += *(rcx);
    rax = *(rax);
    *((rsp + 0x58)) = rax;
    rax = *((rdi + 0xe0));
    rax += r9;
    if (rax >= r13) {
        goto label_16;
    }
label_12:
    rdi = *((rbp + 0xb8));
    rax = *((rsp + 0x60));
    edx = *((rbp + 0xa0));
    r15 = *(rax);
    r13 = *((rax + 0x10));
    *((rsp + 0x68)) = rdi;
    rdi = *((rbp + 0x48));
    *((rbp + 0xb8)) = r13;
    *((rsp + 0x70)) = rdi;
    if (r15 == 0) {
        goto label_17;
    }
    *((rbp + 0x48)) = r15;
    eax = re_string_context_at (rbp, r15 - 1, rdx, rcx, r8, r9);
    r12d = eax;
    if (r15 == rbx) {
        goto label_18;
    }
    r14 = *((r13 + r15*8));
    if (r14 != 0) {
        if ((*((r14 + 0x68)) & 0x40) != 0) {
            goto label_19;
        }
    }
    *((rsp + 0xa0)) = 0;
    xmm0 = 0;
    *((rsp + 0x90)) = xmm0;
label_2:
    if (r15 >= *((rsp + 0x50))) {
        goto label_13;
    }
    *((rsp + 0x28)) = 0;
    rax = r15 + 1;
    *((rsp + 8)) = rax;
    rax = rsp + 0x90;
    *(rsp) = rax;
label_1:
    rax = *((rsp + 8));
    rax--;
    *((rsp + 0x20)) = rax;
    rax = *((rbp + 0xe0));
    if (*((rsp + 0x28)) > rax) {
        goto label_20;
    }
    rdx = *((rsp + 8));
    *((rsp + 0x98)) = 0;
    rax = rdx*8;
    *((rsp + 0x30)) = rax;
    rax = *((rbp + 0xb8));
    rsi = *((rax + rdx*8));
    if (rsi == 0) {
        goto label_21;
    }
    rsi += 8;
    eax = re_node_set_merge (*(rsp), rsi, rdx, rcx);
    *((rsp + 0x88)) = eax;
    if (eax != 0) {
        goto label_11;
    }
    if (r14 == 0) {
        goto label_8;
    }
label_5:
    xmm0 = 0;
    rax = rsp + 0xb0;
    *((rsp + 0x8c)) = 0;
    *((rsp + 0x18)) = rax;
    r13 = *((rbp + 0x98));
    *((rsp + 0xc0)) = 0;
    *((rsp + 0xb0)) = xmm0;
    if (*((r14 + 0x28)) <= 0) {
        goto label_22;
    }
    r15 = *((rsp + 0x20));
    ebx = 0;
    while ((*((rsi + 0xa)) & 0x10) == 0) {
label_4:
        al = check_node_accept (rbp, rsi, r15);
        if (al != 0) {
label_3:
            rcx = r12*8;
label_0:
            rax = *((r13 + 0x18));
            al = re_node_set_insert (*(rsp), *((rax + rcx)));
            if (al == 0) {
                goto label_23;
            }
        }
        rbx++;
        if (rbx >= *((r14 + 0x28))) {
            goto label_24;
        }
        rax = *((r14 + 0x30));
        rsi = *(r13);
        r12 = *((rax + rbx*8));
        r8 = *((rax + rbx*8));
        r8 <<= 4;
        rsi += r8;
    }
    *((rsp + 0x10)) = r8;
    eax = check_node_accept_bytes (r13, r12, rbp, r15);
    r8 = *((rsp + 0x10));
    if (eax <= 1) {
        goto label_25;
    }
    rdx = *((r13 + 0x18));
    rax = (int64_t) eax;
    rcx = r12*8;
    r8 = *((rdx + r12*8));
    r12 = rax + r15;
    rax = r12*8;
    *((rsp + 0x40)) = rax;
    rax = *((rbp + 0xb8));
    rsi = *((rax + r12*8));
    *((rsp + 0xb8)) = 0;
    if (rsi != 0) {
        rsi += 8;
        *((rsp + 0x48)) = rcx;
        *((rsp + 0x10)) = r8;
        eax = re_node_set_merge (*((rsp + 0x18)), rsi, rdx, rcx);
        r8 = *((rsp + 0x10));
        rcx = *((rsp + 0x48));
        *((rsp + 0x8c)) = eax;
        if (eax != 0) {
            goto label_26;
        }
    }
    *((rsp + 0x10)) = rcx;
    al = re_node_set_insert (*((rsp + 0x18)), r8);
    rcx = *((rsp + 0x10));
    if (al == 0) {
        goto label_23;
    }
    *((rsp + 0x48)) = rcx;
    r8 += *((rbp + 0xb8));
    *((rsp + 0x10)) = r8;
    rax = re_acquire_state (rsp + 0x8c, r13, *((rsp + 0x18)), rcx, *((rsp + 0x40)));
    r8 = *((rsp + 0x10));
    rcx = *((rsp + 0x48));
    *(r8) = rax;
    rax = *((rbp + 0xb8));
    if (*((rax + r12*8)) != 0) {
        goto label_0;
    }
    edx = *((rsp + 0x8c));
    if (edx == 0) {
        goto label_0;
    }
label_26:
    free (*((rsp + 0xc0)));
    eax = *((rsp + 0x8c));
    *((rsp + 0x88)) = eax;
    if (eax != 0) {
        goto label_11;
    }
label_8:
    rbx = *((rsp + 8));
    if (*((rsp + 0x98)) != 0) {
        eax = check_arrival_expand_ecl (*((rsp + 0x38)), *(rsp), *((rsp + 0x58)), *((rsp + 0x110)));
        *((rsp + 0x88)) = eax;
        if (eax != 0) {
            goto label_11;
        }
        eax = expand_bkref_cache (rbp, *(rsp), *((rsp + 8)), *((rsp + 0x58)), *((rsp + 0x110)));
        *((rsp + 0x88)) = eax;
        if (eax != 0) {
            goto label_11;
        }
    }
label_6:
    eax = re_string_context_at (rbp, *((rsp + 0x20)), *((rbp + 0xa0)), rcx, r8, r9);
    rax = re_acquire_state_context (rsp + 0x88, *((rsp + 0x38)), *(rsp), eax, r8, r9);
    r14 = rax;
    if (rax == 0) {
        goto label_27;
    }
    rax = *((rbp + 0xb8));
    rcx = *((rsp + 0x30));
    *((rsp + 0x28)) = 0;
    *((rax + rcx)) = r14;
label_9:
    if (rbx < *((rsp + 0x50))) {
        goto label_1;
    }
    r15 = rbx;
label_13:
    free (*((rsp + 0xa0)));
    rax = *((rbp + 0xb8));
    rbx = *((rsp + 0x50));
    rax = *((rax + rbx*8));
    if (rax == 0) {
        goto label_28;
    }
    rbx = *((rsp + 0x60));
    rsi = *((rsp + 0x78));
    *(rbx) = r15;
    rbx = *((rsp + 0x68));
    *((rbp + 0xb8)) = rbx;
    rbx = *((rsp + 0x70));
    *((rbp + 0x48)) = rbx;
    rax = re_node_set_contains (rax + 8);
    al = (rax == 0) ? 1 : 0;
    eax = (int32_t) al;
    do {
label_7:
        rdx = *((rsp + 0xc8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_29;
        }
        return rax;
label_19:
        r13 = rsp + 0x90;
        eax = re_node_set_init_copy (r13, r14 + 8, rdx);
        *((rsp + 0x88)) = eax;
    } while (eax != 0);
    if ((*((r14 + 0x68)) & 0x40) == 0) {
        goto label_2;
    }
label_10:
    if (*((rsp + 0x98)) != 0) {
        eax = expand_bkref_cache (rbp, r13, r15, *((rsp + 0x58)), *((rsp + 0x110)));
        *((rsp + 0x88)) = eax;
        if (eax != 0) {
            goto label_11;
        }
    }
    rax = re_acquire_state_context (rsp + 0x88, *((rsp + 0x38)), r13, r12d, r8, r9);
    r14 = rax;
    if (rax == 0) {
        goto label_30;
    }
label_14:
    rax = *((rbp + 0xb8));
    *((rax + r15*8)) = r14;
    goto label_2;
label_25:
    if (eax != 0) {
        goto label_3;
    }
    rsi = *(r13);
    rsi += r8;
    goto label_4;
label_21:
    if (r14 != 0) {
        goto label_5;
    }
    rbx = *((rsp + 8));
    goto label_6;
label_23:
    free (*((rsp + 0xc0)));
    *((rsp + 0x88)) = 0xc;
    do {
label_11:
        free (*((rsp + 0xa0)));
        eax = *((rsp + 0x88));
        goto label_7;
label_24:
label_15:
        free (*((rsp + 0xc0)));
        *((rsp + 0x88)) = 0;
        goto label_8;
label_27:
        eax = *((rsp + 0x88));
    } while (eax != 0);
    rax = *((rbp + 0xb8));
    rdi = *((rsp + 0x30));
    *((rax + rdi)) = 0;
    goto label_9;
label_17:
    *((rbp + 0x48)) = rbx;
    eax = re_string_context_at (rbp, rbx - 1, rdx, rcx, r8, r9);
    r12d = eax;
label_18:
    *((rsp + 0x90)) = 1;
    *((rsp + 0x98)) = 1;
    rax = malloc (8);
    *((rsp + 0xa0)) = rax;
    if (rax == 0) {
        goto label_31;
    }
    *(rax) = r14;
    r15 = rbx;
    r13 = rsp + 0x90;
    *((rsp + 0x88)) = 0;
    eax = check_arrival_expand_ecl (*((rsp + 0x38)), r13, *((rsp + 0x58)), *((rsp + 0x110)));
    *((rsp + 0x88)) = eax;
    if (eax == 0) {
        goto label_10;
    }
    goto label_11;
label_16:
    r12 = rax + 1;
    rax = 0x7fffffffffffffff;
    rax -= r13;
    if (rax < r12) {
        goto label_31;
    }
    r15 = r13 + r12;
    rax = r15;
    rax >>= 0x3d;
    if (rax != 0) {
        goto label_31;
    }
    rax = *((rsp + 0x60));
    rax = realloc (*((rax + 0x10)), r15*8);
    if (rax == 0) {
        goto label_31;
    }
    rdi = *((rsp + 0x60));
    *((rdi + 8)) = r15;
    *((rdi + 0x10)) = rax;
    memset (rax + r13*8, 0, r12*8);
    goto label_12;
label_20:
    r15 = *((rsp + 0x20));
    goto label_13;
label_30:
    ecx = *((rsp + 0x88));
    if (ecx == 0) {
        goto label_14;
    }
    goto label_11;
label_31:
    eax = 0xc;
    goto label_7;
label_28:
    rax = *((rsp + 0x60));
    *(rax) = r15;
    rax = *((rsp + 0x68));
    *((rbp + 0xb8)) = rax;
    rax = *((rsp + 0x70));
    *((rbp + 0x48)) = rax;
    eax = 1;
    goto label_7;
label_22:
    edi = 0;
    goto label_15;
label_29:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0xcea0 */
 
uint64_t get_subexp_sub (int64_t arg_1h, int64_t c, size_t n, void * ptr, int64_t arg_e0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, void ** arg5) {
    void * var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = r8;
    r14 = rcx;
    r13 = rsi;
    r12 = rdx;
    rbx = r8;
    eax = check_arrival (rdi, rdx + 0x10, *(rdx), *((rdx + 8)), r14, r9);
    if (eax != 0) {
label_1:
        return eax;
    }
    r15 = *((r12 + 8));
    rcx = *(r13);
    rdx = *((rbp + 0xc8));
    rax = *((rbp + 0xd0));
    rdi = *((rbp + 0xd8));
    r8 = rcx;
    rsi = r15;
    if (rdx >= rax) {
        goto label_2;
    }
label_0:
    rax = rdx * 3;
    rax <<= 4;
    if (rdx <= 0) {
        goto label_3;
    }
    r9 = rdi + rax - 0x30;
    while (1) {
label_3:
        rax += rdi;
        edi = 0;
        dil = (r15 == rcx) ? 1 : 0;
        rdx++;
        *(rax) = r14;
        rdi = -rdi;
        *((rax + 8)) = rbx;
        *((rax + 0x10)) = rcx;
        *((rax + 0x18)) = r15;
        *((rax + 0x20)) = rdi;
        *((rbp + 0xc8)) = rdx;
        *((rax + 0x28)) = 0;
        rax = r15;
        rdx = *((rbp + 0xe0));
        rax -= rcx;
        if (rdx < rax) {
            r15d -= ecx;
            *((rbp + 0xe0)) = r15d;
        }
        rsi += rbx;
        rdi = rbp;
        rsi -= r8;
        void (*0xc450)() ();
        *((r9 + 0x28)) = 1;
    }
label_2:
    *((rsp + 8)) = rcx;
    rsi <<= 5;
    rax = realloc (rdi, rax + rax*2);
    rcx = *((rsp + 8));
    if (rax != 0) {
        rsi = *((rbp + 0xc8));
        rdx = *((rbp + 0xd0));
        *((rsp + 8)) = rcx;
        *((rbp + 0xd8)) = rax;
        rsi *= 3;
        rsi <<= 4;
        rdx <<= 4;
        memset (rax + rsi, 0, rdx + rdx*2);
        rsi = *((r12 + 8));
        r8 = *(r13);
        *((rbp + 0xd0)) <<= 1;
        rdx = *((rbp + 0xc8));
        rdi = *((rbp + 0xd8));
        rcx = *((rsp + 8));
        goto label_0;
    }
    free (*((rbp + 0xd8)));
    eax = 0xc;
    goto label_1;
}

/* /tmp/tmpvkjt68yd @ 0xd050 */
 
int64_t dbg_transit_state_bkref (int64_t arg1, uint32_t arg2) {
    re_node_set dest_nodes;
    int64_t var_1h;
    int64_t var_8h;
    uint32_t var_10h;
    int64_t var_18h;
    void ** var_20h;
    uint32_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    void ** canary;
    void ** var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_7ch;
    int64_t var_80h;
    void * ptr;
    int64_t var_98h;
    rdi = arg1;
    rsi = arg2;
    /* reg_errcode_t transit_state_bkref(re_match_context_t * mctx,re_node_set const * nodes); */
label_9:
    r14 = rdi;
    r15 = *((rdi + 0x48));
    *((rsp + 0x10)) = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x98)) = rax;
    rax = *((rdi + 0x98));
    *((rsp + 8)) = 0;
    *((rsp + 0x38)) = rax;
    rax = r15*8;
    *((rsp + 0x50)) = rax;
    if (*((rsi + 8)) > 0) {
        goto label_21;
    }
    goto label_22;
label_0:
    if ((al & 1) == 0) {
        goto label_2;
    }
    edx &= 8;
    if (edx != 0) {
        goto label_2;
    }
label_1:
    if ((cl & 0x20) != 0) {
        if ((al & 2) == 0) {
            goto label_2;
        }
    }
    ecx &= 0x80;
    if (ecx == 0) {
        goto label_23;
    }
    if ((al & 8) != 0) {
        goto label_23;
    }
    do {
label_2:
        rdi = *((rsp + 0x10));
        rax = *((rsp + 8));
        if (*((rdi + 8)) <= rax) {
            goto label_22;
        }
label_21:
        rax = *((rsp + 0x10));
        rdi = *((rsp + 8));
        rsi = *((rsp + 0x38));
        rax = *((rax + 0x10));
        r13 = *((rax + rdi*8));
        rax = *(rsi);
        rbp <<= 4;
        rax += rbp;
    } while (*((rax + 8)) != 4);
    ebx = *((rax + 8));
    if ((ebx & 0x3ff00) == 0) {
        goto label_23;
    }
    ebx >>= 8;
    al = re_string_context_at (r14, r15, *((r14 + 0xa0)), rcx, r8, r9);
    ecx = ebx;
    edx = ebx;
    cx &= 0x3ff;
    if ((bl & 4) != 0) {
        goto label_0;
    }
    edx &= 8;
    if (edx == 0) {
        goto label_1;
    }
    if ((al & 1) == 0) {
        goto label_1;
    }
    goto label_2;
label_23:
    rax = *((r14 + 0x98));
    rsi = r15;
    rdi = r14;
    rbx = *((r14 + 0xc8));
    r10 = *((r14 + 8));
    *((rsp + 0x58)) = rax;
    rax = search_cur_bkref_entry ();
    if (rax == -1) {
        goto label_24;
    }
    rax *= 3;
    rax <<= 4;
    rax += *((r14 + 0xd8));
    while (r13 != *(rax)) {
        rax += 0x30;
        if (*((rax - 8)) == 0) {
            goto label_24;
        }
    }
label_7:
    rax = r13*8;
    *((rsp + 0x7c)) = 0;
    r12 = *((rsp + 0x38));
    *((rsp + 0x58)) = rax;
    rax = r13 * 3;
    rax <<= 3;
    *((rsp + 0x60)) = rax;
    if (rbx < *((r14 + 0xc8))) {
        goto label_25;
    }
    goto label_2;
label_4:
    rsi = *((rsp + 0x60));
    rcx = *((r12 + 0x28));
    rcx = *((rcx + rsi + 0x10));
    rcx = *(rcx);
    rsi = rdx + rcx*8;
    *((rsp + 0x20)) = rsi;
label_5:
    rbp += r15;
    rbp -= rax;
    eax = re_string_context_at (r14, rbp - 1, *((r14 + 0xa0)), rcx + rcx*2, r8, r9);
    rdi = *((rsp + 0x50));
    r11 = rbp*8;
    *((rsp + 0x18)) = 0;
    ecx = eax;
    rax = *((r14 + 0xb8));
    r10 = rax + r11;
    rax = *((rax + rdi));
    rdx = *(r10);
    if (rax != 0) {
        rax = *((rax + 0x10));
        *((rsp + 0x18)) = rax;
    }
    if (rdx == 0) {
        goto label_26;
    }
    *((rsp + 0x48)) = ecx;
    rdi = r10;
    *((rsp + 0x40)) = r11;
    *((rsp + 0x30)) = r10;
    eax = re_node_set_init_union (rdi, *((rdx + 0x50)), *((rsp + 0x20)), rsp + 0x80);
    r10 = *((rsp + 0x30));
    r11 = *((rsp + 0x40));
    *((rsp + 0x7c)) = eax;
    ecx = *((rsp + 0x48));
    if (eax != 0) {
        goto label_27;
    }
    r11 += *((r14 + 0xb8));
    *((rsp + 0x30)) = r11;
    rax = re_acquire_state_context (rsp + 0x7c, r12, r10, rcx, r8, r9);
    r11 = *((rsp + 0x30));
    *(r11) = rax;
    free (*((rsp + 0x90)));
    rdx = *((r14 + 0xb8));
    if (*((rdx + rbp*8)) == 0) {
        goto label_28;
    }
label_6:
    if (*((rsp + 0x28)) == 0) {
        rax = *((rsp + 0x50));
        rdi = *((rsp + 0x18));
        rax = *((rdx + rax));
        if (*((rax + 0x10)) > rdi) {
            goto label_29;
        }
    }
label_3:
    rbx++;
    if (*((r14 + 0xc8)) <= rbx) {
        goto label_2;
    }
label_25:
    rax = rbx * 3;
    rax <<= 4;
    rax += *((r14 + 0xd8));
    if (*(rax) != r13) {
        goto label_3;
    }
    if (*((rax + 8)) != r15) {
        goto label_3;
    }
    rbp = *((rax + 0x18));
    rax = *((rax + 0x10));
    rdx = *((r12 + 0x30));
    rsi = rbp;
    rsi -= rax;
    *((rsp + 0x28)) = rsi;
    if (rsi == 0) {
        goto label_4;
    }
    rcx = *((r12 + 0x18));
    rdi = *((rsp + 0x58));
    rcx = *((rcx + rdi));
    rcx *= 3;
    rsi = rdx + rcx*8;
    *((rsp + 0x20)) = rsi;
    goto label_5;
label_26:
    *((rsp + 0x30)) = r10;
    rax = re_acquire_state_context (rsp + 0x7c, r12, *((rsp + 0x20)), rcx, r8, r9);
    r10 = *((rsp + 0x30));
    rdx = *((r14 + 0xb8));
    *(r10) = rax;
    if (*((rdx + rbp*8)) != 0) {
        goto label_6;
    }
label_28:
    eax = *((rsp + 0x7c));
    if (eax == 0) {
        goto label_6;
    }
label_8:
    rdx = *((rsp + 0x98));
    rdx -= *(fs:0x28);
    if (rdx != 0) {
        goto label_30;
    }
    return rax;
label_24:
    rax = *((rsp + 0x58));
    rdx = *(rax);
    rax = *((rdx + rbp));
    *((rsp + 0x60)) = rax;
    if (*((r14 + 0xe8)) <= 0) {
        goto label_7;
    }
    *((rsp + 0x18)) = 0;
    *((rsp + 0x68)) = rbx;
    *((rsp + 0x40)) = r15;
    *((rsp + 0x48)) = r13;
    do {
        rax = *((r14 + 0xf8));
        rsi = *((rsp + 0x18));
        rdi = *((rsp + 0x60));
        rbx = *((rax + rsi*8));
        rax = *((rbx + 8));
        rax <<= 4;
        if (rdi == *((rdx + rax))) {
            goto label_31;
        }
label_11:
        rax = *((rsp + 0x18));
        if (rax >= *((r14 + 0xe8))) {
            goto label_32;
        }
        rax = *((rsp + 0x58));
        rdx = *(rax);
    } while (1);
label_31:
    r15 = *(rbx);
    if (*((rbx + 0x20)) <= 0) {
        goto label_33;
    }
    rbp = *((rsp + 0x40));
    r12d = 0;
    r9 = r10;
    while (eax <= 1) {
        r12++;
        if (r12 >= *((rbx + 0x20))) {
            goto label_34;
        }
        rax = *((rbx + 0x28));
        rcx = r15;
        r8 = rbp;
        r13 = *((rax + r12*8));
        r15 = *((r13 + 8));
        rdx = *((r13 + 8));
        rdx -= rcx;
        rbp += rdx;
        if (rdx > 0) {
            if (*((r14 + 0x30)) < rbp) {
                goto label_35;
            }
label_10:
            *((rsp + 0x30)) = rcx;
            *((rsp + 0x28)) = r8;
            *((rsp + 0x20)) = r9;
            eax = memcmp (r9 + r8, r9 + rcx, rdx);
            r9 = *((rsp + 0x20));
            r8 = *((rsp + 0x28));
            rcx = *((rsp + 0x30));
            if (eax != 0) {
                goto label_36;
            }
        }
        r8 = *((rsp + 0x40));
        rcx = *((rsp + 0x48));
        rdx = r13;
        rsi = rbx;
        rdi = r14;
        eax = get_subexp_sub ();
        r9 = *((r14 + 8));
    }
    goto label_8;
label_29:
    rbp = *((rsp + 0x20));
    eax = check_subexp_matching_top (r14, rbp, r15, rcx, r8);
    *((rsp + 0x7c)) = eax;
    if (eax != 0) {
        goto label_8;
    }
    eax = transit_state_bkref (r14, rbp);
    goto label_9;
    *((rsp + 0x7c)) = eax;
    if (eax == 0) {
        goto label_3;
    }
    goto label_8;
label_35:
    *((rsp + 0x20)) = rdx;
    if (*((r14 + 0x58)) < rbp) {
        goto label_36;
    }
    *((rsp + 0x30)) = r8;
    *((rsp + 0x28)) = rcx;
    eax = clean_state_log_if_needed (r14, rbp, rdx);
    if (eax != 0) {
        goto label_8;
    }
    r9 = *((r14 + 8));
    rdx = *((rsp + 0x20));
    r8 = *((rsp + 0x30));
    rcx = *((rsp + 0x28));
    goto label_10;
label_27:
    free (*((rsp + 0x90)));
    eax = *((rsp + 0x7c));
    goto label_8;
label_22:
    eax = 0;
    goto label_8;
label_34:
    r10 = r9;
label_16:
    r13 = r15 + 1;
    r11 = rbp;
label_15:
    if (*((rsp + 0x40)) < r13) {
        goto label_11;
    }
    rbp = *((rsp + 0x60));
    r12 = r11;
    r15 = r14;
label_13:
    if (r13 > *(rbx)) {
        if (*((r15 + 0x30)) <= r12) {
            goto label_37;
        }
label_18:
        ecx = *((r10 + r13 - 1));
        rax = r12 + 1;
        if (*((r10 + r12)) != cl) {
            goto label_17;
        }
        r12 = rax;
    }
    rax = *((r15 + 0xb8));
    rax = *((rax + r13*8));
    if (rax == 0) {
        goto label_38;
    }
    rcx = *((rax + 0x10));
    if (rcx <= 0) {
        goto label_38;
    }
    rdi = *((rax + 0x18));
    rax = *((rsp + 0x58));
    edx = 0;
    rsi = *(rax);
    while (*((rax + 8)) != 9) {
label_12:
        rdx++;
        if (rcx == rdx) {
            goto label_38;
        }
        r14 = *((rdi + rdx*8));
        rax = *((rdi + rdx*8));
        rax <<= 4;
        rax += rsi;
    }
    if (rbp != *(rax)) {
        goto label_12;
    }
    if (r14 == -1) {
        goto label_38;
    }
    rsi = *((rbx + 0x10));
    rcx = *(rbx);
    if (rsi == 0) {
        goto label_39;
    }
label_14:
    *((rsp + 0x20)) = r10;
    eax = check_arrival (r15, rsi, *((rbx + 8)), rcx, r14, r13);
    r10 = *((rsp + 0x20));
    if (eax == 1) {
        goto label_38;
    }
    if (eax != 0) {
        goto label_8;
    }
    rax = *((rbx + 0x18));
    if (*((rbx + 0x20)) == rax) {
        goto label_40;
    }
label_20:
    rax = calloc (1, 0x28);
    rdx = rax;
    if (rax == 0) {
        goto label_19;
    }
    rax = *((rbx + 0x20));
    rcx = *((rbx + 0x28));
    rsi = rbx;
    rdi = r15;
    r8 = *((rsp + 0x40));
    *((rcx + rax*8)) = rdx;
    rax++;
    rcx = *((rsp + 0x48));
    *(rdx) = r14;
    *((rdx + 8)) = r13;
    *((rbx + 0x20)) = rax;
    eax = get_subexp_sub ();
    r10 = *((r15 + 8));
    if (eax > 1) {
        goto label_8;
    }
label_38:
    r13++;
    if (*((rsp + 0x40)) >= r13) {
        goto label_13;
    }
label_17:
    r14 = r15;
    goto label_11;
label_39:
    *((rsp + 0x28)) = r10;
    rsi -= rcx;
    *((rsp + 0x20)) = rcx;
    rsi++;
    rax = calloc (0x18, r13);
    rcx = *((rsp + 0x20));
    r10 = *((rsp + 0x28));
    *((rbx + 0x10)) = rax;
    rsi = rax;
    if (rax != 0) {
        goto label_14;
    }
label_19:
    eax = 0xc;
    goto label_8;
label_36:
    r10 = r9;
    r13 = rcx;
    r11 = r8;
    if (r12 < *((rbx + 0x20))) {
        goto label_11;
    }
    if (r12 == 0) {
        goto label_15;
    }
    r15 = rcx;
    goto label_16;
label_32:
    rbx = *((rsp + 0x68));
    r15 = *((rsp + 0x40));
    r13 = *((rsp + 0x48));
    goto label_7;
label_37:
    if (*((r15 + 0x58)) <= r12) {
        goto label_17;
    }
    eax = extend_buffers (r15, r12 + 1, rdx, rcx, r8, r9);
    if (eax != 0) {
        goto label_8;
    }
    r10 = *((r15 + 8));
    goto label_18;
label_40:
    rdx = rax + rax + 1;
    *((rsp + 0x20)) = rdx;
    rax = realloc (*((rbx + 0x28)), rdx*8);
    if (rax == 0) {
        goto label_19;
    }
    rdx = *((rsp + 0x20));
    *((rbx + 0x28)) = rax;
    *((rbx + 0x18)) = rdx;
    goto label_20;
label_33:
    r11 = *((rsp + 0x40));
    r13 = r15;
    goto label_15;
label_30:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0xd860 */
 
int64_t dbg_merge_state_with_log (int64_t arg1, signed int64_t arg2, int64_t arg3, int64_t arg7) {
    re_node_set next_nodes;
    int64_t var_1h;
    int64_t var_8h;
    int64_t var_10h;
    void * ptr;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    /* re_dfastate_t * merge_state_with_log(reg_errcode_t * err,re_match_context_t * mctx,re_dfastate_t * next_state); */
    r13 = rdi;
    rbx = rsi;
    rbp = *((rsi + 0x48));
    r14 = *((rsi + 0xb8));
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    r15 = *((rsi + 0x98));
    r12 = rbp*8;
    r14 += r12;
    if (*((rsi + 0xc0)) >= rbp) {
        goto label_2;
    }
    *(r14) = rdx;
    r12 = rdx;
    *((rsi + 0xc0)) = rbp;
    do {
label_0:
        if (*((r15 + 0x98)) != 0) {
            goto label_3;
        }
label_1:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_4;
        }
        rax = r12;
        return rax;
label_2:
        rax = *(r14);
        if (rax == 0) {
            goto label_5;
        }
        r10 = *((rax + 0x50));
        if (rdx == 0) {
            goto label_6;
        }
        rax = *((rdx + 0x50));
        r14 = rsp + 0x10;
        rsi = rax;
        *(rsp) = rax;
        eax = re_node_set_init_union (r14, rsi, r10, rcx);
        *(r13) = eax;
        if (eax != 0) {
            goto label_7;
        }
        rax = *((rbx + 0x48));
        eax = re_string_context_at (rbx, rax - 1, *((rbx + 0xa0)), rcx, r8, r9);
        r8 += r12;
        *((rsp + 8)) = r8;
        rax = re_acquire_state_context (r13, r15, r14, eax, *((rbx + 0xb8)), r9);
        r8 = *((rsp + 8));
        r12 = rax;
        *(r8) = rax;
    } while (*(rsp) == 0);
    free (*((rsp + 0x20)));
    goto label_0;
label_6:
    __asm ("movdqu xmm0, xmmword [r10]");
    *((rsp + 0x10)) = xmm0;
    rax = *((r10 + 0x10));
    *((rsp + 0x20)) = rax;
    eax = re_string_context_at (rbx, rbp - 1, *((rsi + 0xa0)), rcx, r8, r9);
    rax = re_acquire_state_context (r13, r15, rsp + 0x10, eax, r8, r9);
    *(r14) = rax;
    r12 = rax;
    goto label_0;
label_5:
    *(r14) = rdx;
    r12 = rdx;
    goto label_0;
label_3:
    if (r12 == 0) {
        goto label_7;
    }
    r14 = r12 + 8;
    eax = check_subexp_matching_top (rbx, r14, rbp, rcx, r8);
    *(r13) = eax;
    if (eax != 0) {
        goto label_7;
    }
    if ((*((r12 + 0x68)) & 0x40) == 0) {
        goto label_1;
    }
    eax = transit_state_bkref (rbx, r14);
    *(r13) = eax;
    if (eax == 0) {
        rax = *((rbx + 0xb8));
        r12 = *((rax + rbp*8));
        goto label_1;
    }
label_7:
    r12d = 0;
    goto label_1;
label_4:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0xda50 */
 
int64_t dbg_build_trtable (int64_t arg_18a0h, int64_t arg_18a8h, int64_t arg_18b0h, int64_t arg_30a0h, uint32_t arg1, int64_t arg10, int64_t arg11, void ** arg2, int64_t arg8, int64_t arg9) {
    reg_errcode_t err;
    re_node_set follows;
    bitset_t acceptable;
    bitset_t accepts;
    bitset_t intersec;
    bitset_t remains;
    re_node_set[256] dests_node;
    bitset_word_t[256][4] dests_ch;
    int64_t var_8h;
    uint32_t var_10h;
    uint32_t var_18h;
    size_t * var_20h;
    uint32_t var_28h;
    int64_t var_30h;
    size_t size;
    void ** var_40h;
    size_t var_48h;
    int64_t var_5ch;
    int64_t var_60h;
    int64_t var_68h;
    uint32_t var_70h;
    int64_t var_80h;
    int64_t var_81h;
    int64_t var_90h;
    int64_t var_a0h;
    int64_t var_b0h;
    int64_t var_c0h;
    int64_t var_8a0h;
    int64_t var_8b0h;
    int64_t var_10a0h;
    int64_t var_10b0h;
    rdi = arg1;
    xmm3 = arg10;
    xmm4 = arg11;
    rsi = arg2;
    xmm1 = arg8;
    xmm2 = arg9;
    /* _Bool build_trtable(re_dfa_t const * dfa,re_dfastate_t * state); */
    r11 = rsp - 0x5000;
    do {
    } while (rsp != r11);
    xmm0 = 0;
    *((rsp + 0x40)) = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x50a8)) = rax;
    eax = 0;
    *((rsi + 0x58)) = 0;
    *((rsi + 0x60)) = 0;
    *((rsp + 0xa0)) = xmm0;
    *((rsp + 0xb0)) = xmm0;
    if (*((rsi + 0x10)) <= 0) {
        goto label_28;
    }
    rax = rsp + 0x18a0;
    *((rsp + 0x28)) = rdi;
    r12d = 0;
    r9 = rsi;
    *((rsp + 8)) = 0;
    r8 = r12;
    *((rsp + 0x38)) = rax;
    do {
        rdi = *((rsp + 8));
        rax = rdi*8;
        *((rsp + 0x20)) = rax;
        rax = *((r9 + 0x18));
        r15 = *((rax + rdi*8));
        rax = *((rsp + 0x28));
        r15 <<= 4;
        r15 += *(rax);
        eax = *((r15 + 8));
        r12d = *((r15 + 8));
        eax >>= 8;
        ax &= 0x3ff;
        if (r12d == 1) {
            goto label_29;
        }
        if (r12d == 3) {
            goto label_30;
        }
        if (r12d == 5) {
            goto label_31;
        }
        if (r12d == 7) {
            goto label_32;
        }
label_2:
        rax = *((rsp + 8));
    } while (rax < *((r9 + 0x10)));
    r15 = *((rsp + 0x28));
    r12 = r8;
    if (r8 <= 0) {
        goto label_33;
    }
    *((rsp + 0x68)) = 0;
    rax = r8 + 1;
    *((rsp + 0x60)) = rax;
    rax = malloc (rax*8);
    *((rsp + 0x70)) = rax;
    if (rax == 0) {
        goto label_22;
    }
    *((rsp + 0x28)) = 0;
    xmm0 = 0;
    ebp = 0;
    rbx = rsp + 0x60;
    rax = rsp + 0x18a8;
    *((rsp + 0x10)) = r12;
    r14 = r15;
    *((rsp + 8)) = rax;
    rax = rsp + sym_deregister_tm_clones;
    *((rsp + 0x30)) = rax;
    *((rsp + 0x18)) = rax;
    rax = rsp + 0x5c;
    *((rsp + 0x5c)) = 0;
    *((rsp + 0x20)) = rax;
    *((rsp + 0x80)) = xmm0;
    *((rsp + 0x90)) = xmm0;
label_0:
    *((rsp + 0x68)) = 0;
    rax = *((rsp + 8));
    r13 = *(rax);
    if (r13 <= 0) {
        goto label_34;
    }
    rax = *((rsp + 8));
    r15d = 0;
    r12 = *((rax + 8));
    do {
        rdx = *((r12 + r15*8));
        rax = *((r14 + 0x18));
        rax = *((rax + rdx*8));
        if (rax != -1) {
            rax = *((r14 + 0x30));
            eax = re_node_set_merge (rbx, rax + rdx*8, rax + rax*2, rcx);
            *((rsp + 0x5c)) = eax;
            if (eax != 0) {
                goto label_16;
            }
        }
        r15++;
    } while (r15 != r13);
label_34:
    rax = re_acquire_state_context (*((rsp + 0x20)), r14, rbx, 0, r8, r9);
    r10 = rsp + 0xa0;
    *((r10 + rbp*8)) = rax;
    r9 = rax;
    if (rax == 0) {
        goto label_35;
    }
label_13:
    if (*((r9 + 0x68)) < 0) {
        goto label_36;
    }
    *((rsp + rbp*8 + 0x8a0)) = r9;
    *((rsp + rbp*8 + 0x10a0)) = r9;
label_15:
    r9 = rsp + 0x80;
    rdx = *((rsp + 0x18));
    rax = r9;
    do {
        rcx = *(rdx);
        *(rax) |= rcx;
        rax += 8;
        rdx += 8;
    } while (r10 != rax);
    rbp++;
    if (rbp != *((rsp + 0x10))) {
        goto label_0;
    }
    *((rsp + 8)) = r9;
    r15 = r14;
    r12 = *((rsp + 0x10));
    rbp = *((rsp + 0x70));
    if (*((rsp + 0x28)) != 0) {
        goto label_37;
    }
    rax = calloc (8, 0x100);
    r10 = rax;
    rax = *((rsp + 0x40));
    *((rax + 0x58)) = r10;
    if (r10 == 0) {
        goto label_22;
    }
    r8 = *((rsp + 0x30));
    r9 = *((rsp + 8));
    r11d = 0;
label_1:
    rsi = r11;
    rax = *((r9 + r11*8));
    edx = 1;
    rsi <<= 9;
    rsi += r10;
    if (rax == 0) {
        goto label_38;
    }
    do {
        if ((al & 1) != 0) {
            goto label_39;
        }
label_18:
        rdx += rdx;
        rsi += 8;
        rax >>= 1;
    } while (rax != 0);
label_38:
    r11++;
    r8 += 8;
    if (r11 != 4) {
        goto label_1;
    }
label_24:
    if ((*((rsp + 0x81)) & 4) == 0) {
        goto label_26;
    }
    rdx = *((rsp + 0x30));
    eax = 0;
    do {
        if ((*(rdx) & 0x400) != 0) {
            goto label_40;
        }
        rax++;
        rdx += 0x20;
    } while (rax != r12);
label_26:
    rbx = rsp + 0x18b0;
    free (rbp);
    rax = r12 * 3;
    rbp = rbx + rax*8;
    do {
        rbx += 0x18;
        free (*(rbx));
    } while (rbp != rbx);
    eax = 1;
    goto label_19;
label_29:
    ecx = *(r15);
    rbp = rsp + 0xa0;
    esi = 1;
    rdx = rcx;
    rsi <<= cl;
    rdx >>= 3;
    edx &= 0x18;
    *((rbp + rdx)) |= rsi;
label_5:
    if (ax == 0) {
        goto label_9;
    }
    if ((al & 0x20) != 0) {
        goto label_41;
    }
label_4:
    if ((al & 0x80) != 0) {
        goto label_11;
    }
    if ((al & 4) == 0) {
        goto label_6;
    }
    if (r12d == 1) {
        goto label_42;
    }
label_12:
    r10 = *((rsp + 0x28));
    if (*((r10 + 0xb4)) <= 1) {
        goto label_43;
    }
    rdi = *((r10 + 0x78));
    esi = 0;
    ecx = 0;
    do {
        rdx = *((rdi + rcx*8));
        rdx = ~rdx;
        rdx |= *((r10 + rcx*8 + 0xb8));
        rdx &= *((rbp + rcx*8));
        *((rbp + rcx*8)) = rdx;
        rcx++;
        rsi |= rdx;
    } while (rcx != 4);
    if (rsi == 0) {
        goto label_2;
    }
label_6:
    if ((al & 8) == 0) {
        goto label_9;
    }
    if (r12d == 1) {
        goto label_44;
    }
label_10:
    rdi = *((rsp + 0x28));
    if (*((rdi + 0xb4)) <= 1) {
        goto label_45;
    }
    rsi = *((rdi + 0x78));
    ecx = 0;
    edx = 0;
    do {
        rax = *((rdi + rdx*8 + 0xb8));
        rax &= *((rsi + rdx*8));
        rax = ~rax;
        rax &= *((rbp + rdx*8));
        *((rbp + rdx*8)) = rax;
        rdx++;
        rcx |= rax;
    } while (rdx != 4);
    if (rcx == 0) {
        goto label_2;
    }
label_9:
    r14 = *((rsp + 0x38));
    rbx = rsp + sym_deregister_tm_clones;
    r13d = 0;
    if (r8 <= 0) {
        goto label_46;
    }
    *((rsp + 0x10)) = r15;
    r15 = r8;
    *((rsp + 0x30)) = r9;
    while (r12d == 1) {
        rax = *((rsp + 0x10));
        ecx = *(rax);
        rax = rcx;
        rax >>= 3;
        eax &= 0x18;
        rax += 0x50b0;
        rax += rsp;
        rax = *((rdx + rax - 0x2010));
        rax >>= cl;
        if ((al & 1) != 0) {
            goto label_47;
        }
label_3:
        r13++;
        rbx += 0x20;
        r14 += 0x18;
        if (r13 >= r15) {
            goto label_48;
        }
        rdx = r13;
        rdx <<= 5;
    }
label_47:
    ecx = 0;
    eax = 0;
    rsi = rsp + 0x8a0;
    do {
        rdx = *((rbp + rax*8));
        rdx &= *((rbx + rax*8));
        *((rsi + rax*8)) = rdx;
        rax++;
        rcx |= rdx;
    } while (rax != 4);
    if (rcx == 0) {
        goto label_3;
    }
    r10d = 0;
    edi = 0;
    r11 = rsp + 0x10a0;
    edx = 0;
    do {
        rsi = *((rbp + rdx*8));
        rax = *((rbx + rdx*8));
        rcx = rsi;
        rcx = ~rcx;
        rcx &= rax;
        rax = ~rax;
        rax &= rsi;
        *((r11 + rdx*8)) = rcx;
        rdi |= rcx;
        *((rbp + rdx*8)) = rax;
        rdx++;
        r10 |= rax;
    } while (rdx != 4);
    if (rdi != 0) {
        rax = r15;
        rdi = *((rsp + 0x38));
        *((rsp + 0x18)) = r10;
        __asm ("movdqa xmm1, xmmword [rsp + 0x10a0]");
        rax <<= 5;
        __asm ("movdqa xmm2, xmmword [rsp + 0x10b0]");
        __asm ("movdqa xmm3, xmmword [rsp + 0x8a0]");
        __asm ("movdqa xmm4, xmmword [rsp + 0x8b0]");
        *((rsp + rax + sym.deregister_tm_clones)) = xmm1;
        *((rsp + rax + 0x30b0)) = xmm2;
        rax = r15 * 3;
        *(rbx) = xmm3;
        *((rbx + 0x10)) = xmm4;
        eax = re_node_set_init_copy (rdi + rax*8, r14, rdx);
        if (eax != 0) {
            goto label_49;
        }
        r10 = *((rsp + 0x18));
        r15++;
    }
    rax = *((rsp + 0x30));
    rdi = *((rsp + 0x20));
    *((rsp + 0x18)) = r10;
    rax = *((rax + 0x18));
    al = re_node_set_insert (r14, *((rax + rdi)));
    if (al == 0) {
        goto label_49;
    }
    r10 = *((rsp + 0x18));
    if (r10 != 0) {
        goto label_3;
    }
label_48:
    r9 = *((rsp + 0x30));
    r8 = r15;
label_46:
    if (r8 != r13) {
        goto label_2;
    }
    rax = r8;
    rdi = *((rsp + 0x20));
    *((rsp + 0x18)) = r9;
    __asm ("movdqa xmm5, xmmword [rsp + 0xa0]");
    rax <<= 5;
    *((rsp + 0x10)) = r8;
    __asm ("movdqa xmm6, xmmword [rsp + 0xb0]");
    *((rsp + rax + sym.deregister_tm_clones)) = xmm5;
    *((rsp + rax + 0x30b0)) = xmm6;
    rax = *((r9 + 0x18));
    rbx = *((rax + rdi));
    rdi = *((rsp + 0x38));
    rax = r8 * 3;
    r13 = rdi + rax*8;
    *(r13) = 1;
    *((r13 + 8)) = 1;
    rax = malloc (8);
    r8 = *((rsp + 0x10));
    r9 = *((rsp + 0x18));
    *((r13 + 0x10)) = rax;
    if (rax == 0) {
        goto label_50;
    }
    *(rax) = rbx;
    r8++;
label_11:
    xmm0 = 0;
    *(rbp) = xmm0;
    *((rbp + 0x10)) = xmm0;
    goto label_2;
label_49:
    r12 = r15;
label_17:
    ebx = 0;
    rbp = rsp + 0x18b0;
    do {
        rax = rbx * 3;
        rbx++;
        eax = free (*((rbp + rax*8)));
    } while (r12 > rbx);
label_14:
    eax = 0;
label_19:
    rdx = *((rsp + 0x50a8));
    rdx -= *(fs:0x28);
    if (rdx != 0) {
        goto label_51;
    }
    return rax;
label_41:
    rdx = *((rsp + 0xa0));
    xmm0 = 0;
    *(rbp) = xmm0;
    *((rbp + 0x10)) = xmm0;
    dh &= 4;
    if (dh == 0) {
        goto label_2;
    }
    *((rsp + 0xa0)) = 0x400;
    goto label_4;
label_31:
    rdi = *((rsp + 0x28));
    rbp = rsp + 0xa0;
    if (*((rdi + 0xb4)) <= 1) {
        goto label_52;
    }
    rcx = *((rdi + 0x78));
    rdx = rbp;
    rdi = rsp + 0xc0;
    do {
        rsi = *(rcx);
        *(rdx) |= rsi;
        rdx += 8;
        rcx += 8;
    } while (rdi != rdx);
label_7:
    rdi = *((rsp + 0x28));
    rdx = *((rdi + 0xd8));
    if ((dl & 0x40) == 0) {
    }
label_8:
    edx &= 0x80;
    if (edx == 0) {
        goto label_5;
    }
    goto label_5;
label_30:
    rbp = rsp + 0xa0;
    rcx = *(r15);
    rdi = rsp + 0xc0;
    rdx = rbp;
    do {
        rsi = *(rcx);
        *(rdx) |= rsi;
        rdx += 8;
        rcx += 8;
    } while (rdi != rdx);
    goto label_5;
label_43:
    rdi = *((rsp + 0x28));
    esi = 0;
    edx = 0;
    do {
        rcx = *((rbp + rdx*8));
        rcx &= *((rdi + rdx*8 + 0xb8));
        *((rbp + rdx*8)) = rcx;
        rdx++;
        rsi |= rcx;
    } while (rdx != 4);
    if (rsi != 0) {
        goto label_6;
    }
    goto label_2;
label_52:
    __asm ("pcmpeqd xmm0, xmm0");
    *((rsp + 0xa0)) = xmm0;
    *((rsp + 0xb0)) = xmm0;
    goto label_7;
label_32:
    rdi = *((rsp + 0x28));
    __asm ("pcmpeqd xmm0, xmm0");
    rbp = rsp + 0xa0;
    *((rsp + 0xa0)) = xmm0;
    rdx = *((rdi + 0xd8));
    if ((dl & 0x40) != 0) {
        goto label_8;
    }
    *((rsp + 0xa0)) = 0xfffffffffffffbff;
    goto label_8;
label_45:
    rsi = *((rsp + 0x28));
    ecx = 0;
    edx = 0;
    do {
        rax = *((rsi + rdx*8 + 0xb8));
        rax = ~rax;
        rax &= *((rbp + rdx*8));
        *((rbp + rdx*8)) = rax;
        rdx++;
        rcx |= rax;
    } while (rdx != 4);
    if (rcx == 0) {
        goto label_2;
    }
    goto label_9;
label_44:
    if ((*((r15 + 0xa)) & 0x40) == 0) {
        goto label_10;
    }
    goto label_11;
label_42:
    if ((*((r15 + 0xa)) & 0x40) != 0) {
        goto label_12;
    }
    goto label_11;
label_35:
    ecx = *((rsp + 0x5c));
    if (ecx == 0) {
        goto label_13;
    }
label_16:
    r12 = *((rsp + 0x10));
    rbp = *((rsp + 0x70));
label_22:
    rbx = rsp + 0x18b0;
    free (*((rsp + 0x70)));
    rax = r12 * 3;
    rbp = rbx + rax*8;
    do {
        rbx += 0x18;
        free (*(rbx));
    } while (rbp != rbx);
    goto label_14;
label_36:
    *((rsp + 0x48)) = r10;
    *((rsp + 0x38)) = r9;
    rax = re_acquire_state_context (*((rsp + 0x20)), r14, rbx, 1, r8, r9);
    r9 = *((rsp + 0x38));
    r10 = *((rsp + 0x48));
    *((rsp + rbp*8 + 0x8a0)) = rax;
    if (rax == 0) {
        goto label_53;
    }
    if (r9 != rax) {
label_20:
        edi = *((rsp + 0x28));
        eax = 1;
        if (*((r14 + 0xb4)) >= 2) {
            edi = eax;
        }
        *((rsp + 0x28)) = dil;
    }
    *((rsp + 0x38)) = r10;
    rax = re_acquire_state_context (*((rsp + 0x20)), r14, rbx, 2, r8, r9);
    r10 = *((rsp + 0x38));
    *((rsp + rbp*8 + 0x10a0)) = rax;
    if (rax != 0) {
        goto label_15;
    }
    eax = *((rsp + 0x5c));
    if (eax == 0) {
        goto label_15;
    }
    goto label_16;
label_50:
    *((r13 + 8)) = 0;
    r12 = r8;
    *(r13) = 0;
    if (r8 == 0) {
        goto label_14;
    }
    goto label_17;
label_39:
    rcx = rdx;
    rcx &= *(r8);
    if (rcx != 0) {
        goto label_54;
    }
    do {
        rcx++;
        rbx = rdx;
        rdi = rcx;
        rdi <<= 5;
        rbx &= *((r8 + rdi));
    } while (rbx == 0);
label_21:
    rdi = rdx;
    rdi &= *((r15 + r11*8 + 0xb8));
    if (rdi == 0) {
        goto label_55;
    }
    rcx = *((rsp + rcx*8 + 0x8a0));
    *(rsi) = rcx;
    goto label_18;
    if (rdi != 0) {
label_33:
        goto label_14;
    }
label_28:
    rax = calloc (8, 0x100);
    rdi = *((rsp + 0x40));
    *((rdi + 0x58)) = rax;
    al = (rax != 0) ? 1 : 0;
    goto label_19;
label_55:
    rcx = *((rsp + rcx*8 + 0xa0));
    *(rsi) = rcx;
    goto label_18;
label_53:
    edx = *((rsp + 0x5c));
    if (edx == 0) {
        goto label_20;
    }
    goto label_16;
label_54:
    ecx = 0;
    goto label_21;
label_37:
    rax = calloc (8, 0x200);
    r10 = rax;
    rax = *((rsp + 0x40));
    *((rax + 0x60)) = r10;
    if (r10 == 0) {
        goto label_22;
    }
    r8 = *((rsp + 0x30));
    r9 = *((rsp + 8));
    esi = 0;
label_23:
    rax = rsi;
    rcx = *((r9 + rsi*8));
    edi = 1;
    rax <<= 9;
    rax += r10;
    if (rcx == 0) {
        goto label_56;
    }
    do {
        if ((cl & 1) != 0) {
            goto label_57;
        }
label_25:
        rdi += rdi;
        rax += 8;
        rcx >>= 1;
    } while (rcx != 0);
label_56:
    rsi++;
    r8 += 8;
    if (rsi != 4) {
        goto label_23;
    }
    goto label_24;
label_57:
    rdx = rdi;
    rdx &= *(r8);
    if (rdx != 0) {
        goto label_58;
    }
    do {
        rdx++;
        rbx = rdi;
        r11 = rdx;
        r11 <<= 5;
        rbx &= *((r8 + r11));
    } while (rbx == 0);
label_27:
    r11 = *((rsp + rdx*8 + 0xa0));
    rdx = *((rsp + rdx*8 + 0x8a0));
    *(rax) = r11;
    *((rax + 0x800)) = rdx;
    goto label_25;
label_40:
    rax = *((rsp + rax*8 + 0x10a0));
    *((r10 + 0x50)) = rax;
    if (*((rsp + 0x28)) == 0) {
        goto label_26;
    }
    *((r10 + 0x850)) = rax;
    goto label_26;
label_58:
    edx = 0;
    goto label_27;
label_51:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0xe560 */
 
int64_t dbg_duplicate_tree (int64_t arg_8h, int64_t arg_10h, int64_t arg_32h, int64_t arg1, int64_t arg2, int64_t arg7) {
    bin_tree_t * dup_root;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    /* bin_tree_t * duplicate_tree(bin_tree_t const * root,re_dfa_t * dfa); */
    r13 = rsi;
    rbx = rdi;
    rbp = *(rdi);
    rdx = *((rsi + 0x80));
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    r12 = rsp;
    while (rax != 0) {
        r12 = rbp + 8;
        rbx = rax;
label_1:
        rdx = (int64_t) ecx;
        if (edx == 0xf) {
            goto label_2;
        }
        rax = *((r13 + 0x70));
        ecx = rdx + 1;
        rdx <<= 6;
        *((r13 + 0x80)) = ecx;
        r8 = rax + rdx + 8;
        rax += rdx;
        *((rax + 8)) = 0;
        *((rax + 0x10)) = 0;
        *((rax + 0x18)) = 0;
        __asm ("movdqu xmm0, xmmword [rbx + 0x28]");
        *((rax + 0x20)) = 0;
        __asm ("movups xmmword [rax + 0x30], xmm0");
        *((rax + 0x28)) = 0;
        *((rax + 0x3a)) &= 0xf3;
        *((rax + 0x40)) = 0xffffffffffffffff;
        *(r12) = r8;
        if (r8 == 0) {
            goto label_3;
        }
label_0:
        *((rax + 8)) = rbp;
        rbp = *(r12);
        *((rbp + 0x32)) |= 4;
        rax = *((rbx + 8));
    }
    while (rdx != 0) {
        rbx = rdx;
        rdx = *((rbx + 0x10));
        if (rdx != 0) {
            if (rdx != rax) {
                goto label_4;
            }
        }
        rdx = *(rbx);
        rbp = *(rbp);
        rax = rbx;
    }
    r8 = *(rsp);
    do {
label_3:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_5;
        }
        rax = r8;
        return rax;
label_2:
        rax = malloc (0x3c8);
        if (rax != 0) {
            rdx = *((r13 + 0x70));
            *((rax + 8)) = 0;
            ecx = 1;
            *((rax + 0x10)) = 0;
            *((rax + 0x18)) = 0;
            __asm ("movdqu xmm1, xmmword [rbx + 0x28]");
            *(rax) = rdx;
            rdx = rax + 8;
            __asm ("movups xmmword [rax + 0x30], xmm1");
            *((r13 + 0x70)) = rax;
            *((rax + 0x3a)) &= 0xf3;
            *((r13 + 0x80)) = 1;
            *((rax + 0x20)) = 0;
            *((rax + 0x28)) = 0;
            *((rax + 0x40)) = 0xffffffffffffffff;
            *(r12) = rdx;
            goto label_0;
label_4:
            r12 = rbp + 0x10;
            rbx = rdx;
            goto label_1;
        }
        *(r12) = 0;
        r8d = 0;
    } while (1);
label_5:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0xe710 */
 
int64_t dbg_build_charclass_op (int64_t arg_78h, int64_t arg_b0h, signed int64_t arg_b4h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    Idx alloc;
    re_token_t br_token;
    re_token_t t;
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_48h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* bin_tree_t * build_charclass_op(re_dfa_t * dfa,unsigned char * trans,char const * class_name,char const * extra,_Bool non_match,reg_errcode_t * err); */
    r13 = rsi;
    r12d = r8d;
    rbx = rcx;
    *(rsp) = rdx;
    *((rsp + 8)) = r9;
    rax = *(fs:0x28);
    *((rsp + 0x48)) = rax;
    eax = 0;
    *((rsp + 0x18)) = 0;
    rax = calloc (0x20, 1);
    if (rax == 0) {
        goto label_2;
    }
    r15 = rax;
    rax = calloc (0x50, 1);
    r14 = rax;
    if (rax == 0) {
        goto label_3;
    }
    eax = *((rax + 0x20));
    edx = r12d;
    edx &= 1;
    eax &= 0xfffffffe;
    eax |= edx;
    *((r14 + 0x20)) = al;
    eax = build_charclass (r13, r15, r14, rsp + 0x18, *(rsp), 0);
    if (eax != 0) {
        goto label_4;
    }
    ecx = *(rbx);
    esi = 1;
    if (cl == 0) {
        goto label_5;
    }
    do {
        rdx = (int64_t) cl;
        rax = rdx + 0x3f;
        __asm ("cmovns rax, rdx");
        rdx = rsi;
        rbx++;
        rdx <<= cl;
        ecx = *(rbx);
        rax >>= 6;
        *((r15 + rax*8)) |= rdx;
    } while (cl != 0);
label_5:
    if (r12b == 0) {
        goto label_6;
    }
    rax = r15;
    rdx = r15 + 0x20;
    do {
        rax = ~rax;
        rax += 8;
    } while (rdx != rax);
label_6:
    if (*((rbp + 0xb4)) <= 1) {
        edx = 0;
        esi = 0;
        rcx = rsp + 0x20;
        rdi = rbp;
        *((rsp + 0x20)) = r15;
        *((rsp + 0x28)) = 3;
        rax = create_token_tree ();
        if (rax != 0) {
            goto label_7;
        }
label_0:
        free (r15);
        free (*(r14));
        free (*((r14 + 8)));
        free (*((r14 + 0x10)));
        free (*((r14 + 0x18)));
        free (r14);
        rax = *((rsp + 8));
        *(rax) = 0xc;
        eax = 0;
label_1:
        rdx = *((rsp + 0x48));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_8;
        }
        return rax;
    }
    rcx = *((rbp + 0x78));
    eax = 0;
    do {
        rdx = *((rcx + rax));
        *((r15 + rax)) &= rdx;
        rax += 8;
    } while (rax != 0x20);
    r13 = rsp + 0x20;
    edx = 0;
    esi = 0;
    rdi = rbp;
    rcx = r13;
    *((rsp + 0x20)) = r15;
    *((rsp + 0x28)) = 3;
    rax = create_token_tree ();
    r12 = rax;
    if (rax == 0) {
        goto label_0;
    }
    *((rbp + 0xb0)) |= 2;
    edx = 0;
    rcx = r13;
    esi = 0;
    rdi = rbp;
    *((rsp + 0x28)) = 6;
    *((rsp + 0x20)) = r14;
    rax = create_token_tree ();
    rdx = rax;
    if (rax == 0) {
        goto label_0;
    }
    xmm0 = 0;
    rcx = rsp + 0x30;
    rsi = r12;
    rdi = rbp;
    *((rsp + 0x30)) = xmm0;
    *((rsp + 0x38)) = 0xa;
    create_token_tree ();
    goto label_1;
label_3:
    free (r15);
label_2:
    rax = *((rsp + 8));
    *(rax) = 0xc;
    eax = 0;
    goto label_1;
label_7:
    *(rsp) = rax;
    free (*(r14));
    free (*((r14 + 8)));
    free (*((r14 + 0x10)));
    free (*((r14 + 0x18)));
    free (r14);
    rax = *(rsp);
    goto label_1;
label_4:
    *(rsp) = eax;
    free (r15);
    free (*(r14));
    free (*((r14 + 8)));
    free (*((r14 + 0x10)));
    free (*((r14 + 0x18)));
    free (r14);
    eax = *(rsp);
    rbx = *((rsp + 8));
    *(rbx) = eax;
    eax = 0;
    goto label_1;
label_8:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0xe9e0 */
 
int64_t dbg_re_search_internal (size_t n, int64_t arg_348h, int64_t arg_350h, size_t arg1, uint32_t arg2, int32_t arg3, int64_t arg4, uint32_t arg5, int64_t arg6) {
    int64_t var_1h;
    regmatch_t * pmatch;
    int32_t eflags;
    Idx match_first;
    Idx idx;
    reg_errcode_t err;
    re_sift_context_t sctx;
    regmatch_list prev_match;
    re_match_context_t mctx;
    uint32_t var_8h;
    void * var_10h;
    void * var_18h;
    size_t var_20h;
    uint32_t var_28h;
    void * var_30h;
    int64_t var_38h;
    size_t size;
    uint32_t var_48h;
    int64_t var_50h;
    int64_t var_5ch;
    int64_t var_60h;
    uint32_t var_68h;
    uint32_t var_70h;
    uint32_t var_78h;
    uint32_t var_80h;
    int32_t var_88h;
    int32_t var_90h;
    uint32_t var_98h;
    uint32_t var_9ch;
    size_t var_a0h;
    void * var_a8h;
    size_t s2;
    int64_t var_b8h;
    uint32_t var_c0h;
    int64_t var_c6h;
    int64_t var_c7h;
    uint32_t var_c8h;
    uint32_t var_d0h;
    int32_t var_d8h;
    void * var_e0h;
    uint32_t var_e8h;
    int64_t var_f0h;
    int64_t var_f8h;
    void * var_100h;
    int64_t var_110h;
    uint32_t var_118h;
    void * ptr;
    int64_t var_128h;
    int64_t var_130h;
    void * var_140h;
    uint32_t canary;
    int64_t var_158h;
    void * s1;
    int64_t var_168h;
    int64_t var_1f0h;
    uint32_t var_1f8h;
    void * var_200h;
    void * var_208h;
    int64_t var_218h;
    uint32_t var_220h;
    void * var_228h;
    int64_t var_230h;
    int64_t var_238h;
    int64_t var_240h;
    signed int64_t var_248h;
    int64_t var_250h;
    int32_t var_258h;
    int64_t var_260h;
    int64_t var_268h;
    int64_t var_270h;
    int64_t var_278h;
    int64_t var_279h;
    int64_t var_27ah;
    uint32_t var_27bh;
    int64_t var_27ch;
    int64_t var_27dh;
    int64_t var_27eh;
    int64_t var_280h;
    int64_t var_288h;
    int64_t var_290h;
    int32_t var_298h;
    uint32_t var_2a0h;
    void * var_2a8h;
    int64_t var_2b0h;
    int64_t var_2b8h;
    int64_t var_2c0h;
    void * var_2c8h;
    int64_t var_2d0h;
    int64_t var_2e0h;
    void * var_2e8h;
    int64_t var_2f8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* reg_errcode_t re_search_internal(regex_t const * preg,char const * string,Idx length,Idx start,Idx last_start,Idx stop,size_t nmatch,regmatch_t * pmatch,int eflags); */
    r12 = r9;
    rbx = rcx;
    ecx = 0x20;
    rax = *((rsp + 0x348));
    *((rsp + 0x40)) = rdi;
    r14 = rsp + 0x1f0;
    *((rsp + 0x80)) = rsi;
    rsi = rdi;
    *((rsp + 0x90)) = rdx;
    *((rsp + 0xb0)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0x2f8)) = rax;
    eax = 0;
    rax = *(rdi);
    rdi = r14;
    rdx = rax;
    *((rsp + 0x38)) = rax;
    eax = 0;
    do {
        *(rdi) = rax;
        rcx--;
        rdi += 8;
    } while (rcx != 0);
    rcx = *((rsi + 0x20));
    *((rsp + 0x288)) = rdx;
    *((rsp + 0x68)) = rcx;
    if (rcx != 0) {
        edx = *((rsi + 0x38));
        if ((dl & 8) == 0) {
            goto label_74;
        }
        if (rbx == r8) {
            goto label_74;
        }
        edx &= 1;
        if (edx == 0) {
            rax = rcx;
        }
        *((rsp + 0x68)) = rax;
    }
label_2:
    rax = *((rsp + 0x40));
    *((rsp + 0xa0)) = 0;
    rcx = *((rax + 0x28));
    rax = *((rax + 0x30));
    *((rsp + 0x78)) = rcx;
    if (rax < *((rsp + 0x340))) {
        rsi = *((rsp + 0x340));
        rdx = rsi - 1;
        rcx = rdx;
        rcx -= rax;
        rax -= rdx;
        *((rsp + 0xa0)) = rcx;
    }
    rsi = *((rsp + 0x40));
    if (*((rsi + 0x10)) == 0) {
        goto label_75;
    }
    rdi = *((rsp + 0x38));
    rax = *((rdi + 0x48));
    if (rax == 0) {
        goto label_75;
    }
    rdx = *((rdi + 0x50));
    if (rdx == 0) {
        goto label_75;
    }
    rcx = *((rdi + 0x58));
    if (rcx == 0) {
        goto label_75;
    }
    if (*((rdi + 0x60)) == 0) {
        goto label_75;
    }
    if (*((rax + 0x10)) == 0) {
        if (*((rdx + 0x10)) != 0) {
            goto label_76;
        }
        if (*((rcx + 0x10)) != 0) {
            if (*((rsi + 0x38)) < 0) {
                goto label_76;
            }
        }
        if (rbx != 0) {
            *((rsp + 0x5c)) = 1;
            if (rbp != 0) {
                goto label_35;
            }
        }
        ebp = 0;
        ebx = 0;
    }
label_76:
    *((rsp + 0x48)) = 1;
    if (*((rsp + 0x340)) == 0) {
        goto label_77;
    }
label_1:
    rax = *((rsp + 0x40));
    r11 = *((rsp + 0x38));
    rdi = *((rsp + 0x90));
    rax = *((rax + 0x18));
    rcx = *((r11 + 0xb4));
    *((rsp + 8)) = rax;
    rax = *((r11 + 0x10));
    rdx = rcx;
    *((rsp + 0x248)) = rdi;
    *((rsp + 0x10)) = rax;
    rax++;
    *((rsp + 0x240)) = rdi;
    if (rax < rcx) {
        rax = rcx;
    }
    rcx = rdi + 1;
    *((rsp + 0x280)) = edx;
    if (rax <= rcx) {
        rcx = rax;
    }
    rax = *((rsp + 0x80));
    *((rsp + 0x1f0)) = rax;
    rax = *((rsp + 8));
    rsi = rcx;
    rcx = rdi;
    rdi = *((rsp + 0x78));
    *((rsp + 0x258)) = rcx;
    eax &= 0x400000;
    *((rsp + 0x250)) = rcx;
    rsp + 0x278 = (eax != 0) ? 1 : 0;
    rax |= rdi;
    eax = *((r11 + 0xb0));
    rsp + 0x27b = (rax != 0) ? 1 : 0;
    edx = eax;
    al >>= 3;
    *((rsp + 0x268)) = rdi;
    dl >>= 2;
    eax &= 1;
    edx &= 1;
    *((rsp + 0x27a)) = al;
    *((rsp + 0x279)) = dl;
    eax = re_string_realloc_buffers (r14, rsi);
    *((rsp + 0x5c)) = eax;
    if (eax != 0) {
        goto label_78;
    }
    rcx = *((rsp + 0x38));
    rax = rcx + 0xb8;
    *((rsp + 0x270)) = rax;
    eax = *((rcx + 0xb0));
    *((rsp + 8)) = al;
    al >>= 4;
    eax &= 1;
    *((rsp + 0x27e)) = al;
    if (*((rsp + 0x27b)) == 0) {
        rsi = *((rsp + 0x38));
        rax = *((rsp + 0x80));
        *((rsp + 0x1f8)) = rax;
        rax = *((rsp + 0x90));
        if (*((rsi + 0xb4)) <= 1) {
            goto label_79;
        }
    }
    eax = 0;
label_79:
    *((rsp + 0x220)) = rax;
    *((rsp + 0x228)) = rax;
    rax = *((rsp + 0x40));
    *((rsp + 0x258)) = r12;
    eax = *((rax + 0x38));
    *((rsp + 0x250)) = r12;
    *((rsp + 8)) = al;
    al >>= 7;
    *((rsp + 0x27d)) = al;
    rax = *((rsp + 0x38));
    r13 = *((rax + 0x98));
    eax = *((rsp + 0x350));
    *((rsp + 0x298)) = 0xffffffffffffffff;
    r12 = r13 + r13;
    *((rsp + 0x290)) = eax;
    if (r12 > 0) {
        rax = 0x555555555555555;
        if (r12 > rax) {
            goto label_51;
        }
        rdi <<= 4;
        rax = malloc (r12 + r13*4);
        rdi <<= 4;
        *((rsp + 0x2c8)) = rax;
        r15 = rax;
        rax = malloc (r13);
        *((rsp + 0x2e8)) = rax;
        if (rax == 0) {
            goto label_51;
        }
        if (r15 == 0) {
            goto label_51;
        }
    }
    *((rsp + 0x2c0)) = r12;
    *((rsp + 0x2d0)) = 1;
    *((rsp + 0x2e0)) = r12;
    if (*((rsp + 0x340)) <= 1) {
        goto label_80;
    }
label_32:
    rax = *((rsp + 0x230));
    rdx = 0x1ffffffffffffffe;
    if (rax > rdx) {
        goto label_51;
    }
    rax = malloc (rax*8 + 8);
    *((rsp + 0x2a8)) = rax;
    r15 = rax;
    if (rax == 0) {
        goto label_42;
    }
label_33:
    eax = *((rsp + 0x350));
    *((rsp + 0xe0)) = rbx;
    *((rsp + 0x98)) = 8;
    eax &= 1;
    eax -= eax;
    eax &= 2;
    eax += 4;
    *((rsp + 0x260)) = eax;
    eax = 0;
    al = (rbx <= rbp) ? 1 : 0;
    eax = rax + rax - 1;
    *((rsp + 0x9c)) = eax;
    rax = rbp;
    if (rbx <= rbp) {
        rax = rbx;
    }
    *((rsp + 0x88)) = rax;
    rax = rbp;
    if (rbx >= rbp) {
        rax = rbx;
    }
    *((rsp + 0x70)) = rax;
    rax = *((rsp + 0x38));
    eax = *((rax + 0xb4));
    *((rsp + 0xc0)) = eax;
    if (*((rsp + 0x68)) != 0) {
        edx = 4;
        if (*((rsp + 0xc0)) != 1) {
            rax = *((rsp + 0x40));
            edx = 0;
            rax = *((rax + 0x18));
            *((rsp + 8)) = rax;
            eax &= 0x400000;
            rax |= *((rsp + 0x78));
            dl = (rax == 0) ? 1 : 0;
            edx <<= 2;
        }
        eax = 0;
        al = (rbx <= rbp) ? 1 : 0;
        ecx = 0;
        eax += eax;
        cl = (*((rsp + 0x78)) != 0) ? 1 : 0;
        eax |= ecx;
        eax |= edx;
        *((rsp + 0x98)) = eax;
    }
    eax = 0;
    rdx = rsp + 0xe0;
    if (rbx <= rbp) {
        rax = rdx;
    }
    *((rsp + 0xb8)) = rax;
    rsp + 0xc7 = (rax != 0) ? 1 : 0;
label_20:
    eax = *((rsp + 0x98));
    if (eax == 7) {
        goto label_81;
    }
    if (eax == 8) {
        goto label_28;
    }
    if (eax > 5) {
        goto label_82;
    }
    if (eax > 3) {
        goto label_83;
    }
label_3:
    rbp = *((rsp + 0x68));
    r12d = *((rsp + 0x9c));
    r13 = *((rsp + 0x88));
    r15 = *((rsp + 0x70));
    while (*((rsp + 0x228)) > rax) {
label_0:
        rdx = rbp;
        if (*((rsp + 0x220)) > rax) {
            rdx = *((rsp + 0x1f8));
            edx = *((rdx + rax));
            rdx += rbp;
        }
        if (*(rdx) != 0) {
            goto label_28;
        }
        rax = (int64_t) r12d;
        rbx += rax;
        *((rsp + 0xe0)) = rbx;
        if (rbx < r13) {
            goto label_27;
        }
        if (rbx > r15) {
            goto label_27;
        }
        rax = rbx;
        rax -= *((rsp + 0x218));
    }
    eax = re_string_reconstruct (r14, rbx, *((rsp + 0x350)));
    if (eax != 0) {
        goto label_84;
    }
    rax = rbx;
    rax -= *((rsp + 0x218));
    goto label_0;
label_77:
    rax = *((rsp + 0x38));
    al = (*((rax + 0x98)) != 0) ? 1 : 0;
    eax = (int32_t) al;
    *((rsp + 0x48)) = eax;
    goto label_1;
label_74:
    *((rsp + 0x68)) = 0;
    goto label_2;
label_82:
    if (*((rsp + 0x98)) != 6) {
        goto label_3;
    }
    rax = rbx;
    ecx = 0;
    if (*((rsp + 0x70)) <= rbx) {
        goto label_29;
    }
    rsi = *((rsp + 0x68));
    r8 = *((rsp + 0x70));
    r9 = *((rsp + 0xd0));
    rdi = *((rsp + 0x80));
    while (rax != r8) {
        r9 = rax;
        edx = *((rdi + rax));
        if (*((rsi + rdx)) != 0) {
            goto label_85;
        }
        rax++;
        ecx = 1;
    }
    *((rsp + 0xd0)) = r9;
    *((rsp + 0xe0)) = rax;
    goto label_86;
label_85:
    *((rsp + 0xd0)) = r9;
    if (cl != 0) {
        *((rsp + 0xe0)) = r9;
        rbx = r9;
label_29:
        if (*((rsp + 0x70)) == rbx) {
            goto label_87;
        }
    }
label_28:
    eax = re_string_reconstruct (r14, rbx, *((rsp + 0x350)));
    *((rsp + 0x5c)) = eax;
    if (eax != 0) {
        goto label_78;
    }
    if (*((rsp + 0xc0)) != 1) {
        if (*((rsp + 0x220)) == 0) {
            goto label_88;
        }
        rax = *((rsp + 0x200));
        if (*(rax) == 0xffffffff) {
            goto label_89;
        }
    }
label_88:
    eax = *((rsp + 0x48));
    rbx = *((rsp + 0x288));
    *((rsp + 0x2d0)) = 0;
    *((rsp + 0x2b8)) = 0;
    eax &= 1;
    r13 = *((rbx + 0x48));
    *((rsp + 0x2b0)) = 0;
    *((rsp + 0xc6)) = al;
    rax = *((rsp + 0x238));
    *((rsp + 0xe8)) = 0;
    *((rsp + 8)) = rax;
    if (*((r13 + 0x68)) < 0) {
        al = re_string_context_at (r14, rax - 1, *((rsp + 0x290)), rcx, r8, r9);
        if ((al & 1) != 0) {
            goto label_90;
        }
        if (eax == 0) {
            goto label_25;
        }
        edx = eax;
        edx &= 6;
        if (edx == 6) {
            goto label_91;
        }
        if ((al & 2) != 0) {
            goto label_92;
        }
        if ((al & 4) != 0) {
            goto label_93;
        }
    }
label_25:
    rax = *((rsp + 0x2a8));
    if (rax != 0) {
        rdi = *((rsp + 8));
        *((rax + rdi*8)) = r13;
        if (*((rbx + 0x98)) != 0) {
            goto label_94;
        }
    }
    eax = *((rsp + 0xc7));
    *((rsp + 0x18)) = al;
    eax = *((r13 + 0x68));
label_41:
    *((rsp + 0x28)) = 0xffffffffffffffff;
    *((rsp + 0x30)) = 0;
    if ((al & 0x10) != 0) {
        goto label_95;
    }
label_39:
    rax = *((rsp + 0xb8));
    rbp = *((rsp + 0x238));
    *((rsp + 0x60)) = rax;
    if (*((rsp + 0x258)) <= rbp) {
        goto label_7;
    }
    rax = rsp + 0xe8;
    *((rsp + 0x20)) = rax;
    while (r12 != 0) {
label_6:
        rbp = *((rsp + 0x238));
        al = (r13 == r12) ? 1 : 0;
        eax = *((r12 + 0x68));
        if (r13 == r12) {
            rbx = *((rsp + 8));
        }
        *((rsp + 8)) = rbx;
        if ((al & 0x10) != 0) {
            if (al < 0) {
                goto label_96;
            }
label_9:
            esi = *((rsp + 0x48));
            *((rsp + 0x28)) = rbp;
            if (esi == 0) {
                goto label_57;
            }
            *((rsp + 0x30)) = 1;
            *((rsp + 0x60)) = 0;
        }
label_8:
        if (*((rsp + 0x258)) <= rbp) {
            goto label_7;
        }
        r13 = r12;
        rax = *((rsp + 0x230));
        rbx = rbp + 1;
        if (rbx >= rax) {
            goto label_97;
        }
label_10:
        rax = *((rsp + 0x220));
        if (rbx >= rax) {
            goto label_98;
        }
label_18:
        if ((*((r13 + 0x68)) & 0x20) != 0) {
            goto label_99;
        }
label_11:
        rax = rbp + 1;
        *((rsp + 0x238)) = rax;
        rax = *((rsp + 0x1f8));
        ebp = *((rax + rbp));
label_22:
        rax = *((r13 + 0x58));
        if (rax == 0) {
            goto label_100;
        }
        r12 = *((rax + rbp*8));
label_17:
        if (*((rsp + 0x2a8)) != 0) {
label_21:
            rax = merge_state_with_log (*((rsp + 0x20)), r14, r12, rcx);
            r12 = rax;
        }
    }
    r8d = *((rsp + 0xe8));
    rcx = *((rsp + 0x2a8));
    if (r8d != 0) {
        goto label_101;
    }
    if (rcx == 0) {
        goto label_7;
    }
    eax = *((rsp + 0xc6));
    eax ^= 1;
    if ((*((rsp + 0x30)) & al) != 0) {
        goto label_7;
    }
    rbp = *((rsp + 0x20));
label_5:
    rdi = *((rsp + 0x2b0));
    rax = *((rsp + 0x238));
    edx = 0;
    while (rdi >= rax) {
        edx = 1;
        if (*((rcx + rax*8)) != 0) {
            goto label_102;
        }
        rsi = rax;
        rax++;
    }
    if (dl != 0) {
        *((rsp + 0x238)) = rsi;
    }
label_7:
    rax = *((rsp + 0x60));
    if (rax != 0) {
        rsi = *((rsp + 8));
        *(rax) += rsi;
    }
label_57:
    rax = *((rsp + 0x28));
    if (rax == -1) {
        goto label_37;
    }
    r15 = *((rsp + 0x2a8));
    if (rax == 0xfffffffffffffffe) {
        goto label_42;
    }
    *((rsp + 0x298)) = rax;
    rax = *((rsp + 0x40));
    eax = *((rax + 0x38));
    if ((al & 0x10) != 0) {
        goto label_103;
    }
    if (*((rsp + 0x340)) <= 1) {
        goto label_104;
    }
    rax = check_halt_state_context (r14, *((r15 + rdx*8)), *((rsp + 0x28)), rcx, r8);
    *((rsp + 0x2a0)) = rax;
    rax = *((rsp + 0x38));
    if ((*((rax + 0xb0)) & 1) != 0) {
        goto label_105;
    }
    rax = *((rsp + 0x38));
    if (*((rax + 0x98)) != 0) {
        goto label_105;
    }
label_26:
    if (*((rsp + 0x340)) == 1) {
        goto label_106;
    }
    rdx = *((rsp + 0x340));
    rsi = *((rsp + 0xb0));
    rdx <<= 4;
    rax = rsi + 0x10;
    rdx += rsi;
    do {
        *((rax + 8)) = 0xffffffffffffffff;
        rax += 0x10;
        *((rax - 0x10)) = 0xffffffffffffffff;
    } while (rdx != rax);
label_106:
    rax = *((rsp + 0x40));
    eax = *((rax + 0x38));
label_31:
    rdi = *((rsp + 0xb0));
    rdx = *((rsp + 0x298));
    *(rdi) = 0;
    *((rdi + 8)) = rdx;
    if ((al & 0x10) != 0) {
        goto label_107;
    }
    if (*((rsp + 0x340)) == 1) {
        goto label_107;
    }
    rax = *((rsp + 0x40));
    rax = *(rax);
    *((rsp + 0x30)) = rax;
    rax = *((rsp + 0x38));
    if ((*((rax + 0xb0)) & 1) == 0) {
        goto label_108;
    }
    if (*((rax + 0x98)) <= 0) {
        goto label_108;
    }
    rax = rsp + 0x168;
    *((rsp + 0x110)) = 0;
    *((rsp + 0x118)) = 2;
    *((rsp + 0x150)) = 0;
    *((rsp + 0x158)) = 8;
    *((rsp + 8)) = rax;
    *((rsp + 0x160)) = rax;
    rax = malloc (0x60);
    *((rsp + 0x120)) = rax;
    if (rax == 0) {
        goto label_42;
    }
    r13 = rsp + 0x110;
label_63:
    rax = *((rsp + 0x30));
    xmm0 = 0;
    r15 = *((rax + 0x90));
    *((rsp + 0xf0)) = xmm0;
    *((rsp + 0x100)) = 0;
    al = gl_dynarray_resize (rsp + 0x150, *((rsp + 0x340)), *((rsp + 8)), 0x10, r8, r9);
    if (al == 0) {
        goto label_109;
    }
    rax = *((rsp + 0x340));
    rbp = *((rsp + 0xb0));
    rax <<= 4;
    rdx = rax;
    *((rsp + 0x18)) = rdi;
    *((rsp + 0x20)) = rax;
    memcpy (*((rsp + 0x160)), rbp, rdx);
    rbx = *(rbp);
    *((rsp + 0x10)) = r14;
    rax = *((rsp + 0x340));
    *((rsp + 0xe8)) = rbx;
    r12 = rbx;
    rax <<= 5;
    *((rsp + 0x40)) = rax;
    rax = rsp + 0xf0;
    r14 = rax;
label_4:
    rdx = *((rbp + 8));
    if (rdx < r12) {
        goto label_110;
    }
    rax = *((rsp + 0x30));
    rbx = r15;
    *((rsp + 0x28)) = r15;
    rbx <<= 4;
    rcx = *(rax);
    rcx += rbx;
    eax = *((rcx + 8));
    if (eax == 8) {
        goto label_111;
    }
    if (eax == 9) {
        goto label_112;
    }
label_47:
    if (r12 == rdx) {
        goto label_113;
    }
label_48:
    if (r13 != 0) {
        rsi = r15;
        rax = re_node_set_contains (r14);
        if (rax != 0) {
            goto label_114;
        }
    }
label_50:
    r12 = *((rsp + 0x288));
    rsi = *(r12);
    rsi += rbx;
    eax = *((rsi + 8));
    if ((al & 8) == 0) {
        goto label_115;
    }
    rdx = *((rsp + 0xe8));
    rax = *((rsp + 0x2a8));
    rsi = r15;
    rbx = *((rax + rdx*8));
    rdx = *((r12 + 0x28));
    rax = r15 * 3;
    rbx += 8;
    r12 = rdx + rax*8;
    rax = re_node_set_contains (r14);
    if (rax == 0) {
        goto label_116;
    }
label_54:
    r11 = *((r12 + 8));
    if (r11 <= 0) {
        goto label_61;
    }
    r10 = *((r12 + 0x10));
    r15 = 0xffffffffffffffff;
    r9d = 0;
    do {
        r12 = *((r10 + r9*8));
        rsi = r12;
        rax = re_node_set_contains (rbx);
        if (rax != 0) {
            if (r15 != -1) {
                goto label_117;
            }
            r15 = r12;
        }
        r9++;
    } while (r9 != r11);
label_46:
    if (r15 < 0) {
        goto label_118;
    }
label_60:
    r12 = *((rsp + 0xe8));
    goto label_4;
label_102:
    *((rsp + 0x238)) = rax;
    rax = merge_state_with_log (rbp, r14, 0, rcx);
    edi = *((rsp + 0xe8));
    if (edi == 0) {
        if (rax != 0) {
            goto label_119;
        }
        rcx = *((rsp + 0x2a8));
        goto label_5;
    }
    r12 = rax;
    if (rax != 0) {
        goto label_6;
    }
    goto label_7;
label_96:
    rax = check_halt_state_context (r14, r12, rbp, rcx, r8);
    if (rax == 0) {
        goto label_8;
    }
    goto label_9;
label_97:
    if (rax >= *((rsp + 0x248))) {
        goto label_10;
    }
label_19:
    eax = extend_buffers (r14, rbx + 1, rdx, rcx, r8, r9);
    *((rsp + 0xe8)) = eax;
    if (eax != 0) {
        goto label_51;
    }
    rbp = *((rsp + 0x238));
    if ((*((r13 + 0x68)) & 0x20) == 0) {
        goto label_11;
    }
label_99:
    r12 = *((rsp + 0x288));
    if (*((r13 + 0x10)) <= 0) {
        goto label_120;
    }
    r15d = 0;
    rax = rsp + 0x110;
    *((rsp + 0xa8)) = rbx;
    rcx = rbp;
    *((rsp + 0x50)) = rax;
    rbx = r13;
    goto label_121;
label_13:
    if ((al & 1) == 0) {
        goto label_15;
    }
    r13d &= 8;
    if (r13d != 0) {
        goto label_15;
    }
label_14:
    if ((dl & 0x20) != 0) {
        if ((al & 2) == 0) {
            goto label_15;
        }
    }
    edx &= 0x80;
    if (edx != 0) {
        if ((al & 8) == 0) {
            goto label_15;
        }
    }
label_12:
    eax = check_node_accept_bytes (r12, r15, r14, rcx);
    if (eax != 0) {
        edx = *((rsp + 0x2d0));
        r13 = (int64_t) eax;
        rdi = r14;
        r13 += *((rsp + 0x238));
        rsi = r13;
        if (edx < eax) {
            edx = eax;
        }
        *((rsp + 0x2d0)) = edx;
        eax = clean_state_log_if_needed (rdi, rsi, rdx);
        *((rsp + 0xf0)) = eax;
        if (eax != 0) {
            goto label_122;
        }
        rax = *((r12 + 0x18));
        r10 = *((rsp + 0x2a8));
        rax = *((rax + r15*8));
        r15 = r13*8;
        r10 += r15;
        rdx = rax * 3;
        rax = *((r12 + 0x30));
        *((rsp + 0x10)) = r10;
        rdx = rax + rdx*8;
        rax = *(r10);
        if (rax == 0) {
            goto label_123;
        }
        eax = re_node_set_init_union (*((rsp + 0x50)), *((rax + 0x50)), rdx, rcx);
        *((rsp + 0xf0)) = eax;
        if (eax != 0) {
            goto label_122;
        }
        eax = re_string_context_at (r14, r13 - 1, *((rsp + 0x290)), rcx, r8, r9);
        r15 += *((rsp + 0x2a8));
        rax = re_acquire_state_context (rsp + 0xf0, r12, *((rsp + 0x50)), eax, r8, r9);
        *(r15) = rax;
        free (*((rsp + 0x120)));
        rdx = *((rsp + 0x2a8));
        if (*((rdx + r13*8)) == 0) {
            goto label_124;
        }
    }
label_16:
    rcx = *((rsp + 0x238));
    do {
label_15:
        rbp++;
        if (rbp >= *((rbx + 0x10))) {
            goto label_125;
        }
label_121:
        rax = *((rbx + 0x18));
        r15 = *((rax + rbp*8));
        rax = *((rax + rbp*8));
        rax <<= 4;
        rax += *(r12);
    } while ((*((rax + 0xa)) & 0x10) == 0);
    r13d = *((rax + 8));
    if ((r13d & 0x3ff00) == 0) {
        goto label_12;
    }
    rsi = rcx;
    r13d >>= 8;
    *((rsp + 0x10)) = rcx;
    al = re_string_context_at (r14, rsi, *((rsp + 0x290)), rcx, r8, r9);
    edx = r13d;
    rcx = *((rsp + 0x10));
    dx &= 0x3ff;
    if ((r13b & 4) != 0) {
        goto label_13;
    }
    r13d &= 8;
    if (r13d == 0) {
        goto label_14;
    }
    if ((al & 1) == 0) {
        goto label_14;
    }
    goto label_15;
label_123:
    __asm ("movdqu xmm1, xmmword [rdx]");
    *((rsp + 0x110)) = xmm1;
    rax = *((rdx + 0x10));
    *((rsp + 0x120)) = rax;
    eax = re_string_context_at (r14, r13 - 1, *((rsp + 0x290)), rcx, r8, r9);
    rax = re_acquire_state_context (rsp + 0xf0, r12, *((rsp + 0x50)), eax, r8, r9);
    r10 = *((rsp + 0x10));
    rdx = *((rsp + 0x2a8));
    *(r10) = rax;
    if (*((rdx + r13*8)) != 0) {
        goto label_16;
    }
label_124:
    eax = *((rsp + 0xf0));
    if (eax == 0) {
        goto label_16;
    }
    r13 = rbx;
    rbx = *((rsp + 0xa8));
    goto label_126;
label_100:
    r12 = *((r13 + 0x60));
    if (r12 == 0) {
        goto label_127;
    }
    rax = *((rsp + 0x238));
    al = re_string_context_at (r14, rax - 1, *((rsp + 0x290)), rcx, r8, r9);
    if ((al & 1) == 0) {
        goto label_128;
    }
    r12 = *((r12 + rbp*8 + 0x800));
    goto label_17;
label_98:
    if (rax >= *((rsp + 0x248))) {
        goto label_18;
    }
    goto label_19;
label_103:
    rax = *((rsp + 0x38));
    if (*((rax + 0x98)) == 0) {
        goto label_129;
    }
label_30:
    rax = check_halt_state_context (r14, *((r15 + rdx*8)), *((rsp + 0x28)), rcx, r8);
    *((rsp + 0x2a0)) = rax;
label_105:
    rax = *((rsp + 0x288));
    *((rsp + 0x18)) = rax;
    rax = *((rsp + 0x2a0));
    *((rsp + 8)) = rax;
    rax = 0x1ffffffffffffffe;
    if (*((rsp + 0x28)) > rax) {
        goto label_42;
    }
    rax = *((rsp + 0x28));
    rbx = rax + 1;
    rbp = rbx*8;
    rax = malloc (rbp);
    r13 = rax;
    if (rax == 0) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    if (*((rax + 0x98)) == 0) {
        goto label_130;
    }
    rax = malloc (rbp);
    r12 = rax;
    if (rax == 0) {
        goto label_131;
    }
    rax = rsp + 0x130;
    *((rsp + 0x30)) = rbp;
    rbp = *((rsp + 0x28));
    r15 = rsp + 0x110;
    *((rsp + 0x20)) = rax;
label_36:
    memset (r12, 0, rbx*8);
    rax = *((rsp + 8));
    *((rsp + 0x110)) = r13;
    xmm0 = 0;
    *((rsp + 0x120)) = rax;
    rax = *((rsp + 0x20));
    *((rsp + 0x118)) = r12;
    *((rsp + 0x128)) = rbp;
    *((rax + 0x10)) = 0;
    *(rax) = xmm0;
    eax = sift_states_backward (r14, r15);
    *((rsp + 0x10)) = eax;
    free (*((rsp + 0x140)));
    ecx = *((rsp + 0x10));
    if (ecx != 0) {
        goto label_132;
    }
    if (*(r13) != 0) {
        goto label_133;
    }
    if (*(r12) != 0) {
        goto label_133;
    }
    rbp--;
    if (rbp < 0) {
        goto label_134;
    }
    rax = *((rsp + 0x2a8));
    do {
        rsi = *((rax + rbp*8));
        if (rsi != 0) {
            if ((*((rsi + 0x68)) & 0x10) != 0) {
                goto label_135;
            }
        }
        rbp--;
    } while (rbp >= 0);
label_134:
    free (r13);
    free (r12);
label_37:
    match_ctx_clean (r14, rsi);
    rbx = *((rsp + 0xe0));
label_89:
    rax = *((rsp + 0x9c));
    rbx += rax;
    *((rsp + 0xe0)) = rbx;
    if (rbx < *((rsp + 0x88))) {
        goto label_27;
    }
    if (rbx <= *((rsp + 0x70))) {
        goto label_20;
    }
label_27:
    *((rsp + 0x5c)) = 1;
    r15 = *((rsp + 0x2a8));
label_24:
    free (r15);
    rax = *((rsp + 0x38));
    if (*((rax + 0x98)) != 0) {
        goto label_136;
    }
label_34:
    free (*((rsp + 0x200)));
    free (*((rsp + 0x208)));
    if (*((rsp + 0x27b)) != 0) {
        goto label_137;
    }
    do {
label_35:
        rax = *((rsp + 0x2f8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_138;
        }
        eax = *((rsp + 0x5c));
        return rax;
label_122:
        r13 = rbx;
        rdx = *((rsp + 0x2a8));
        rbx = *((rsp + 0xa8));
label_126:
        *((rsp + 0xe8)) = eax;
        if (rdx == 0) {
            goto label_139;
        }
label_23:
        r12d = 0;
        goto label_21;
label_75:
        *((rsp + 0x5c)) = 1;
    } while (1);
label_127:
    al = build_trtable (*((rsp + 0x288)), r13, rdx, rcx, r8, r9);
    if (al != 0) {
        goto label_22;
    }
    *((rsp + 0xe8)) = 0xc;
    if (*((rsp + 0x2a8)) != 0) {
        goto label_23;
    }
label_139:
    r15d = 0;
label_42:
    *((rsp + 0x5c)) = 0xc;
    goto label_24;
label_125:
    r13 = rbx;
    rbx = *((rsp + 0xa8));
label_120:
    *((rsp + 0xe8)) = 0;
    goto label_11;
label_128:
    r12 = *((r12 + rbp*8));
    goto label_17;
label_90:
    r13 = *((rbx + 0x50));
label_38:
    if (r13 != 0) {
        goto label_25;
    }
label_51:
    *((rsp + 0x5c)) = 0xc;
    r15 = *((rsp + 0x2a8));
    goto label_24;
label_130:
    rax = *((rsp + 8));
    r15 = rsp + 0x110;
    xmm0 = 0;
    *((rsp + 0x110)) = r13;
    *((rsp + 0x120)) = rax;
    rax = *((rsp + 0x28));
    *((rsp + 0x118)) = 0;
    *((rsp + 0x128)) = rax;
    *((rsp + 0x140)) = 0;
    *((rsp + 0x130)) = xmm0;
    eax = sift_states_backward (r14, r15);
    ebx = eax;
    *((rsp + 0x10)) = eax;
    free (*((rsp + 0x140)));
    if (ebx != 0) {
        goto label_140;
    }
    if (*(r13) == 0) {
        goto label_141;
    }
label_43:
    r15 = r13;
    free (*((rsp + 0x2a8)));
    rax = *((rsp + 8));
    *((rsp + 0x2a8)) = r13;
    *((rsp + 0x2a0)) = rax;
    rax = *((rsp + 0x28));
    *((rsp + 0x298)) = rax;
label_129:
    if (*((rsp + 0x340)) == 0) {
        goto label_24;
    }
    goto label_26;
label_83:
    if (*((rsp + 0x88)) > rbx) {
        goto label_27;
    }
    rdi = *((rsp + 0x68));
    rsi = *((rsp + 0x78));
    rax = rbx;
    ecx = 0;
    r9 = *((rsp + 0x88));
    r10 = *((rsp + 0xd8));
    r11 = *((rsp + 0x80));
    r8 = *((rsp + 0x90));
    while (*((rdi + rdx)) == 0) {
        rax--;
        ecx = 1;
        if (rax < r9) {
            goto label_142;
        }
        r10 = rax;
        edx = 0;
        if (rax < r8) {
            edx = *((r11 + rax));
        }
        if (rsi != 0) {
            edx = *((rsi + rdx));
        }
    }
    *((rsp + 0xd8)) = r10;
    if (cl == 0) {
        goto label_28;
    }
    *((rsp + 0xe0)) = r10;
    rbx = r10;
    goto label_28;
label_81:
    rax = rbx;
    ecx = 0;
    if (*((rsp + 0x70)) <= rbx) {
        goto label_29;
    }
    rsi = *((rsp + 0x68));
    rdi = *((rsp + 0x78));
    r9 = *((rsp + 0x70));
    r10 = *((rsp + 0xc8));
    r8 = *((rsp + 0x80));
    while (rax != r9) {
        r10 = rax;
        edx = *((r8 + rax));
        edx = *((rdi + rdx));
        if (*((rsi + rdx)) != 0) {
            goto label_143;
        }
        rax++;
        ecx = 1;
    }
    *((rsp + 0xc8)) = r10;
    *((rsp + 0xe0)) = rax;
label_86:
    rbx = rax;
label_87:
    eax = 0;
    if (*((rsp + 0x90)) > rbx) {
        rax = *((rsp + 0x80));
        eax = *((rax + rbx));
    }
    rcx = *((rsp + 0x78));
    if (rcx != 0) {
        eax = *((rcx + rax));
    }
    rdi = *((rsp + 0x68));
    if (*((rdi + rax)) != 0) {
        goto label_28;
    }
    goto label_27;
label_104:
    rcx = *((rsp + 0x38));
    if (*((rcx + 0x98)) != 0) {
        goto label_30;
    }
    if (*((rsp + 0x340)) == 0) {
        goto label_24;
    }
    goto label_31;
label_78:
    r15 = *((rsp + 0x2a8));
    goto label_24;
label_80:
    rax = *((rsp + 0x38));
    if ((*((rax + 0xb0)) & 2) != 0) {
        goto label_32;
    }
    goto label_33;
label_136:
    match_ctx_clean (r14, rsi);
    free (*((rsp + 0x2e8)));
    free (*((rsp + 0x2c8)));
    goto label_34;
label_137:
    free (*((rsp + 0x1f8)));
    goto label_35;
label_135:
    rbx = rbp + 1;
    rax = check_halt_state_context (r14, rsi, rbp, rcx, r8);
    *((rsp + 8)) = rax;
    goto label_36;
label_142:
    *((rsp + 0xe0)) = rax;
    goto label_27;
label_143:
    *((rsp + 0xc8)) = r10;
    if (cl == 0) {
        goto label_28;
    }
    *((rsp + 0xe0)) = r10;
    rbx = r10;
    goto label_29;
label_95:
    if (*((r13 + 0x68)) >= 0) {
label_40:
        rax = *((rsp + 8));
        r9d = *((rsp + 0x48));
        *((rsp + 0x30)) = 1;
        *((rax + rax)) = rax;
        *(rax) += al;
        *(rax) += al;
        *(rax) += al;
        *(rax) += al;
        *(rax) += al;
        *(rax) += al;
label_119:
        r12 = rax;
        void (*0x100f1b8)() ();
label_92:
        *(rax) += al;
        *(rax) += al;
        *(rax) += al;
        *(rax) += al;
label_141:
        *(rax) += al;
        *(rax) -= eax;
        *(rax) += al;
        goto label_37;
label_93:
        rax = re_acquire_state_context (rsp + 0xe8, rbx, *((r13 + 0x50)), eax, r8, r9);
        r13 = rax;
        goto label_38;
    }
    rax = check_halt_state_context (r14, r13, *((rsp + 8)), rcx, r8);
    *((rsp + 0x30)) = rax;
    if (rax == 0) {
        goto label_39;
    }
    goto label_40;
label_94:
    rbp = r13 + 8;
    eax = check_subexp_matching_top (r14, rbp, 0, rcx, r8);
    *((rsp + 0xe8)) = eax;
    if (eax != 0) {
        goto label_144;
    }
    eax = *((r13 + 0x68));
    if ((al & 0x40) != 0) {
        goto label_145;
    }
label_45:
    *((rsp + 0x18)) = 0;
    goto label_41;
label_91:
    r13 = *((rbx + 0x60));
    goto label_38;
label_101:
    r15 = rcx;
    goto label_42;
label_132:
    r12 = *((rsp + 0x30));
label_44:
    free (r13);
    free (r12);
    if (*((rsp + 0x10)) == 1) {
        goto label_37;
    }
    eax = *((rsp + 0x10));
    r15 = *((rsp + 0x2a8));
    *((rsp + 0x5c)) = eax;
    goto label_24;
label_133:
    *((rsp + 0x28)) = rbp;
    eax = merge_state_array (*((rsp + 0x18)), r13, r12, rbx);
    ebx = eax;
    *((rsp + 0x10)) = eax;
    eax = free (r12);
    if (ebx == 0) {
        goto label_43;
    }
label_140:
    r12d = 0;
    goto label_44;
label_84:
    *((rsp + 0x5c)) = eax;
    r15 = *((rsp + 0x2a8));
    goto label_24;
label_131:
    free (r13);
    *((rsp + 0x5c)) = 0xc;
    goto label_24;
label_145:
    eax = transit_state_bkref (r14, rbp);
    *((rsp + 0xe8)) = eax;
    if (eax != 0) {
        goto label_144;
    }
    eax = *((r13 + 0x68));
    goto label_45;
label_115:
    if ((*((rsi + 0xa)) & 0x10) != 0) {
        goto label_146;
    }
    if (al == 4) {
        goto label_147;
    }
label_53:
    *((rsp + 0x48)) = rdx;
    al = check_node_accept (*((rsp + 0x10)), rsi, *((rsp + 0xe8)));
    if (al == 0) {
        goto label_61;
    }
    rax = *((r12 + 0x18));
    rcx = *((rsp + 0x28));
    rdx = *((rsp + 0x48));
    r15 = *((rax + rcx*8));
    rdx++;
label_52:
    *((rsp + 0xe8)) = rdx;
    if (r13 != 0) {
        if (rdx > *((rsp + 0x298))) {
            goto label_61;
        }
        rax = *((rsp + 0x2a8));
        rax = *((rax + rdx*8));
        if (rax == 0) {
            goto label_61;
        }
        rsi = r15;
        rax = re_node_set_contains (rax + 8);
        if (rax == 0) {
            goto label_61;
        }
    }
    *((rsp + 0xf8)) = 0;
    goto label_46;
label_111:
    rax = *(rcx);
    rax++;
    if (*((rsp + 0x340)) <= rax) {
        goto label_47;
    }
    rax <<= 4;
    rax += rbp;
    *((rax + 8)) = 0xffffffffffffffff;
    rdx = *((rbp + 8));
    *(rax) = r12;
    goto label_47;
label_113:
    if (r15 != *((rsp + 0x2a0))) {
        goto label_48;
    }
    if (r13 == 0) {
        goto label_148;
    }
label_114:
    rcx = *((rsp + 0x340));
    rax = rbp;
    edx = 0;
    do {
        if (*(rax) >= 0) {
            if (*((rax + 8)) == -1) {
                goto label_149;
            }
        }
        rdx++;
        rax += 0x10;
    } while (rcx != rdx);
label_49:
    r14 = *((rsp + 0x10));
    free (*((rsp + 0x100)));
    rdi = *((rsp + 0x160));
    if (rdi == *((rsp + 8))) {
        goto label_150;
    }
    free (rdi);
    rax = *((rsp + 8));
    *((rsp + 0x150)) = 0;
    *((rsp + 0x158)) = 8;
    *((rsp + 0x160)) = rax;
    if (r13 == 0) {
        goto label_69;
    }
label_66:
    ebx = 0;
    if (*(r13) <= 0) {
        goto label_70;
    }
    do {
        rax = *((r13 + 0x10));
        rbp = rbx * 3;
        rbx++;
        rbp <<= 4;
        free (*((rax + rbp + 0x28)));
        rax = *((r13 + 0x10));
        free (*((rax + rbp + 0x10)));
    } while (rbx < *(r13));
label_70:
label_72:
    free (*((r13 + 0x10)));
label_69:
    rax = *((rsp + 0xb0));
    r15 = *((rsp + 0x2a8));
    rax = *(rax);
    goto label_151;
label_149:
    rax = pop_fail_stack (r13, rsp + 0xe8, *((rsp + 0x340)), rbp, *((rsp + 0x18)), r14);
    r15 = rax;
    if (rax < 0) {
        goto label_49;
    }
    rbx = rax;
    *((rsp + 0x28)) = rax;
    rbx <<= 4;
    goto label_50;
label_117:
    rsi = r15;
    rax = re_node_set_contains (r14);
    if (rax != 0) {
        goto label_152;
    }
    if (r13 == 0) {
        goto label_46;
    }
    r9 = *((rsp + 0xe8));
    rdx = *(r13);
    rdi = *((r13 + 0x10));
    if (rdx == *((r13 + 8))) {
        goto label_153;
    }
label_68:
    rax = rdx * 3;
    *((rsp + 0x48)) = rdx;
    rax <<= 4;
    rbx = rdi + rax;
    *((rsp + 0x28)) = rax;
    *(rbx) = r9;
    *((rbx + 8)) = r12;
    rax = malloc (*((rsp + 0x40)));
    *((rbx + 0x10)) = rax;
    rdi = rax;
    if (rax != 0) {
    }
label_67:
    r14 = *((rsp + 0x10));
    free (*((rsp + 0x100)));
    rdi = *((rsp + 0x160));
    if (rdi == *((rsp + 8))) {
        goto label_154;
    }
label_55:
    free (rdi);
label_56:
    rax = *((rsp + 8));
    *((rsp + 0x150)) = 0;
    *((rsp + 0x158)) = 8;
    *((rsp + 0x160)) = rax;
    if (r13 == 0) {
        goto label_51;
    }
label_73:
    ebx = 0;
    if (*(r13) > 0) {
    }
label_71:
    free (*((r13 + 0x10)));
    goto label_51;
label_112:
    rax = *(rcx);
    rax++;
    if (*((rsp + 0x340)) <= rax) {
        goto label_47;
    }
    rax <<= 4;
    rdx = rbp + rax;
    if (*(rdx) < r12) {
        goto label_155;
    }
    if ((*((rcx + 0xa)) & 8) == 0) {
        goto label_156;
    }
    rsi = *((rsp + 0x18));
    if (*((rsi + rax)) == -1) {
        goto label_156;
    }
    memcpy (rbp, rsi, *((rsp + 0x20)));
    rdx = *((rbp + 8));
    goto label_47;
label_147:
    rax = *(rsi);
    rax++;
    if (*((rsp + 0x340)) <= rax) {
        goto label_157;
    }
    rax <<= 4;
    rax += rbp;
    rsi = *((rax + 8));
    rcx = *(rax);
    rdx = rsi;
    rdx -= rcx;
    if (r13 == 0) {
        goto label_158;
    }
    if (rcx == -1) {
        goto label_61;
    }
    if (rsi == -1) {
        goto label_61;
    }
    if (rdx == 0) {
        goto label_64;
    }
    rbx = *((rsp + 0xe8));
    rsi = *((rsp + 0x220));
    rax = *((rsp + 0x1f8));
    rsi -= rbx;
    if (rdx > rsi) {
        goto label_61;
    }
    *((rsp + 0x28)) = rdx;
    eax = memcmp (rax + rcx, rax + rbx, rdx);
    rdx = *((rsp + 0x28));
    if (eax != 0) {
        goto label_61;
    }
label_65:
    rax = *((r12 + 0x18));
    rdx += rbx;
    r15 = *((rax + r15*8));
    goto label_52;
label_146:
    eax = check_node_accept_bytes (r12, r15, *((rsp + 0x10)), *((rsp + 0xe8)));
    rdx = (int64_t) eax;
    if (rdx != 0) {
        goto label_159;
    }
    rsi = *(r12);
    rdx = *((rsp + 0xe8));
    rsi += rbx;
    goto label_53;
label_116:
    al = re_node_set_insert (r14, rsi);
    if (al != 0) {
        goto label_54;
    }
label_59:
    r14 = *((rsp + 0x10));
    rax = free (*((rsp + 0x100)));
    rdi = *((rsp + 0x160));
    if (rdi != *((rsp + 8))) {
        goto label_55;
    }
    goto label_56;
label_144:
    rax = (int64_t) eax;
    *((rsp + 0x28)) = rax;
    goto label_57;
label_107:
    eax = 0;
label_151:
    ebx = *((rsp + 0x27c));
    r11 = *((rsp + 0x220));
    esi = 0;
    r10 = *((rsp + 0x208));
    r8 = *((rsp + 0x228));
    rdi = *((rsp + 0xe0));
    rdx = *((rsp + 0xb0));
    while (rsi != *((rsp + 0x340))) {
        rax = *(rdx);
        if (rax != -1) {
            rcx = *((rdx + 8));
            if (bl != 0) {
                goto label_160;
            }
label_58:
            rax += rdi;
            rcx += rdi;
            *(rdx) = rax;
            *((rdx + 8)) = rcx;
        }
        rsi++;
        rdx += 0x10;
    }
    if (*((rsp + 0xa0)) <= 0) {
        goto label_161;
    }
    rax = *((rsp + 0x340));
    rcx = *((rsp + 0xb0));
    rdx = *((rsp + 0xa0));
    rdx += *((rsp + 0x340));
    rax <<= 4;
    rdx <<= 4;
    rax += rcx;
    rdx += rcx;
    do {
        *(rax) = 0xffffffffffffffff;
        rax += 0x10;
        *((rax - 8)) = 0xffffffffffffffff;
    } while (rdx != rax);
label_161:
    rax = *((rsp + 0x38));
    rsi = *((rax + 0xe0));
    if (rsi == 0) {
        goto label_24;
    }
    if (*((rsp + 0x340)) <= 1) {
        goto label_24;
    }
    rax = *((rsp + 0x340));
    r8 = *((rsp + 0xb0));
    rdi = rax - 1;
    rdx = r8 + 0x10;
    eax = 0;
    do {
        rcx = *((rsi + rax*8));
        if (rcx != rax) {
            rcx <<= 4;
            rcx = *((rcx + r8 + 0x10));
            *(rdx) = rcx;
            rcx = *((rsi + rax*8));
            rcx <<= 4;
            rcx = *((rcx + r8 + 0x18));
            *((rdx + 8)) = rcx;
        }
        rax++;
        rdx += 0x10;
    } while (rdi != rax);
    goto label_24;
label_160:
    if (r11 == rax) {
        goto label_162;
    }
    rax = *((r10 + rax*8));
label_62:
    *(rdx) = rax;
    if (r11 == rcx) {
        goto label_163;
    }
    rcx = *((r10 + rcx*8));
    goto label_58;
label_118:
    if (r15 == 0xfffffffffffffffe) {
        goto label_59;
    }
label_61:
    rax = pop_fail_stack (r13, rsp + 0xe8, *((rsp + 0x340)), rbp, *((rsp + 0x18)), r14);
    r15 = rax;
    if (rax >= 0) {
        goto label_60;
    }
    r14 = *((rsp + 0x10));
    free (*((rsp + 0x100)));
    rdi = *((rsp + 0x160));
    if (rdi != *((rsp + 8))) {
        free (rdi);
    }
    rax = *((rsp + 8));
    ebx = 0;
    *((rsp + 0x150)) = 0;
    *((rsp + 0x158)) = 8;
    *((rsp + 0x160)) = rax;
    if (r13 != 0) {
        void (*0x10837)() ();
    }
label_157:
    if (r13 != 0) {
        goto label_61;
    }
label_64:
    al = re_node_set_insert (r14, r15);
    if (al == 0) {
        goto label_59;
    }
    rdx = *((r12 + 0x28));
    rax = r15 * 3;
    rax = rdx + rax*8;
    rdx = *((rsp + 0xe8));
    rax = *((rax + 0x10));
    *((rsp + 0x48)) = rdx;
    r15 = *(rax);
    rax = *((rsp + 0x2a8));
    rsi = r15;
    rdi += 8;
    rax = re_node_set_contains (*((rax + rdx*8)));
    if (rax != 0) {
        goto label_46;
    }
    rsi = *(r12);
    rdx = *((rsp + 0x48));
    rsi += rbx;
    goto label_53;
label_163:
    rcx = r8;
    goto label_58;
label_162:
    rax = r8;
    goto label_62;
label_108:
    rax = rsp + 0x168;
    r13d = 0;
    *((rsp + 0x110)) = 0;
    *((rsp + 0x118)) = 2;
    *((rsp + 0x120)) = 0;
    *((rsp + 0x150)) = 0;
    *((rsp + 0x158)) = 8;
    *((rsp + 8)) = rax;
    *((rsp + 0x160)) = rax;
    goto label_63;
label_158:
    if (rdx == 0) {
        goto label_64;
    }
label_159:
    rbx = *((rsp + 0xe8));
    goto label_65;
label_156:
    *((rdx + 8)) = r12;
    rdx = *((rbp + 8));
    goto label_47;
label_155:
    *((rdx + 8)) = r12;
    memcpy (*((rsp + 0x18)), rbp, *((rsp + 0x20)));
    rdx = *((rbp + 8));
    r12 = *((rsp + 0xe8));
    goto label_47;
label_152:
    r15 = r12;
    goto label_46;
label_150:
    *((rsp + 0x150)) = 0;
    *((rsp + 0x158)) = 8;
    goto label_66;
label_138:
    stack_chk_fail ();
label_153:
    *((rsp + 0x48)) = r9;
    rsi <<= 5;
    *((rsp + 0x28)) = rdx;
    rax = realloc (rdi, rdx + rdx*2);
    rdi = rax;
    if (rax == 0) {
        goto label_67;
    }
    r9 = *((rsp + 0x48));
    rdx = *((rsp + 0x28));
    *((r13 + 0x10)) = rax;
    *((r13 + 8)) <<= 1;
    goto label_68;
label_110:
    r14 = *((rsp + 0x10));
    free (*((rsp + 0x100)));
    rdi = *((rsp + 0x160));
    if (rdi != *((rsp + 8))) {
        free (rdi);
    }
    rax = *((rsp + 8));
    *((rsp + 0x150)) = 0;
    *((rsp + 0x158)) = 8;
    *((rsp + 0x160)) = rax;
    if (r13 == 0) {
        goto label_69;
    }
    ebx = 0;
    if (*(r13) <= 0) {
        goto label_70;
    }
    do {
        rax = *((r13 + 0x10));
        rbp = rbx * 3;
        rbx++;
        rbp <<= 4;
        free (*((rax + rbp + 0x28)));
        rax = *((r13 + 0x10));
        free (*((rax + rbp + 0x10)));
    } while (rbx < *(r13));
    goto label_70;
label_109:
    rdi = *((rsp + 0x160));
    if (rdi != *((rsp + 8))) {
        free (rdi);
    }
    rax = *((rsp + 8));
    *((rsp + 0x150)) = 0;
    *((rsp + 0x158)) = 8;
    *((rsp + 0x160)) = rax;
    if (r13 == 0) {
        goto label_51;
    }
    ebx = 0;
    if (*(r13) <= 0) {
        goto label_71;
    }
    do {
        rax = *((r13 + 0x10));
        rbp = rbx * 3;
        rbx++;
        rbp <<= 4;
        free (*((rax + rbp + 0x28)));
        rax = *((r13 + 0x10));
        free (*((rax + rbp + 0x10)));
    } while (rbx < *(r13));
    goto label_71;
label_148:
    r14 = *((rsp + 0x10));
    free (*((rsp + 0x100)));
    rdi = *((rsp + 0x160));
    if (rdi != *((rsp + 8))) {
        goto label_72;
    }
    goto label_69;
label_154:
    *((rsp + 0x150)) = 0;
    *((rsp + 0x158)) = 8;
    goto label_73;
}

/* /tmp/tmpvkjt68yd @ 0x10b50 */
 
uint8_t rotate_right8 (uint8_t value, uint32_t count) {
    const uint8_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value >> count) | (value << (-count & mask));
}
 
int64_t dbg_parse_expression (size_t arg1, uint32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_78h;
    char * s;
    int64_t var_a0h;
    char * var_a8h;
    size_t var_b4h;
    int64_t var_c0h;
    int64_t var_c8h;
    int64_t var_e8h;
    Idx char_class_alloc;
    bracket_elem_t start_elem;
    bracket_elem_t end_elem;
    re_token_t t;
    unsigned char[32] start_name_buf;
    unsigned char[32] end_name_buf;
    int64_t var_8h;
    size_t var_10h;
    size_t var_18h;
    void ** ptr;
    size_t var_28h;
    int64_t canary;
    int64_t var_38h;
    uint32_t var_40h;
    void * var_48h;
    size_t var_4ch;
    uint32_t var_50h;
    size_t size;
    size_t var_60h;
    int64_t var_68h;
    void * var_70h;
    uint32_t var_7ah;
    uint32_t var_7bh;
    int64_t var_7ch;
    int64_t var_80h;
    void * var_88h;
    void * var_90h;
    size_t var_b0h;
    signed int64_t var_b8h;
    int64_t var_d0h;
    int64_t var_d8h;
    int64_t var_e0h;
    int64_t var_f8h;
    int64_t var_108h;
    int64_t var_109h;
    int64_t var_10ah;
    uint32_t var_10bh;
    int64_t var_110h;
    int64_t var_118h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* bin_tree_t * parse_expression(re_string_t * regexp,regex_t * preg,re_token_t * token,reg_syntax_t syntax,Idx nest,reg_errcode_t * err); */
    r13 = r8;
    r12 = rsi;
    rbx = 0x00016ca0;
    rbp = *(rsi);
    ecx &= 0x1000000;
    r14 = rcx;
    rax = *(fs:0x28);
    eax = 0;
label_0:
    rax = *(rsp);
    eax = *((rax + 8));
    rdx = rax;
    rax = *((rbx + rax*4));
    rax += rbx;
    /* switch table (72 cases) at 0x16ca0 */
    void (*rax)(uint64_t, uint64_t, uint64_t, uint64_t, uint64_t) (rbx, rbp, r12, r13, r14);
    do {
        if (dl == 9) {
            goto label_107;
        }
label_29:
        rax = *(rsp);
        edx = 0;
        esi = 0;
        rdi = rbp;
        *((rax + 8)) = 1;
        rcx = rax;
        rax = create_token_tree ();
        *((rsp + 0x10)) = rax;
        if (rax != 0) {
            goto label_7;
        }
label_6:
        rax = *((rsp + 0x30));
        *(rax) = 0xc;
label_1:
        *((rsp + 0x10)) = 0;
label_2:
        rax = *((rsp + 0x108));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_108;
        }
        rax = *((rsp + 0x10));
        return rax;
        if (r14 != 0) {
            goto label_109;
        }
        rax = *((rsp + 8));
        if ((al & 0x20) != 0) {
            goto label_109;
        }
    } while ((al & 0x10) == 0);
    r15 = *((rsp + 0x18));
    rax = peek_token (*(rsp), r15, rax, rcx, r8);
    rax = (int64_t) eax;
    *((r15 + 0x48)) += rax;
    goto label_0;
    rax = *((rsp + 0x18));
    rbx = *((rsp + 0x30));
    r8b = (dl == 0x21) ? 1 : 0;
    r8d = (int32_t) r8b;
label_5:
    rax = build_charclass_op (rbp, *((rax + 0x78)), "alnum", 0x000160be, r8, rbx);
    edi = *(rbx);
    *((rsp + 0x10)) = rax;
    if (edi == 0) {
        goto label_7;
    }
    if (rax == 0) {
        goto label_1;
    }
label_7:
    rbx = *((rsp + 0x18));
    r15 = *(rsp);
    rax = peek_token (r15, rbx, *((rsp + 8)), rcx, r8);
    rax = (int64_t) eax;
    rax += *((rbx + 0x48));
    *((rbx + 0x48)) = rax;
    r14 = rax;
    eax = *((r15 + 8));
    if (al > 0x17) {
        goto label_2;
    }
    rdx = 0xffffffffff73f7ff;
    bl = (al == 0x12) ? 1 : 0;
    if (((rdx >> rax) & 1) < 0) {
        goto label_2;
    }
    rdi = *(rsp);
    r13 = rsp + 0xb0;
    rcx = r14;
    __asm ("movdqu xmm0, xmmword [rdi]");
    if (al == 0x17) {
        goto label_110;
    }
label_3:
    r15 = *((rsp + 0x18));
    r12d = 0;
    r12b = (al == 0x13) ? 1 : 0;
    r12 = r12 + r12 - 1;
    rax = peek_token (*(rsp), r15, *((rsp + 8)), rcx, r8);
    rax = (int64_t) eax;
    *((r15 + 0x48)) += rax;
    if (*((rsp + 0x10)) == 0) {
        goto label_17;
    }
    r14 = *((rsp + 0x10));
    if (bl != 0) {
        goto label_111;
    }
    *((rsp + 0x20)) = 0;
    ebx = 0;
label_24:
    if (*((r14 + 0x30)) == 0x11) {
        goto label_112;
    }
label_14:
    xmm0 = 0;
    rcx = r13;
    rsi = r14;
    al = (r12 == -1) ? 1 : 0;
    rdi = rbp;
    edx = 0;
    *((rsp + 0xb0)) = xmm0;
    eax += 0xa;
    *((rsp + 0xb8)) = al;
    rax = create_token_tree ();
    r15 = rax;
    if (rax == 0) {
        goto label_25;
    }
    rbx += 2;
    if (r12 < rbx) {
        goto label_113;
    }
    do {
        rax = duplicate_tree (r14, rbp, rdx, rcx, r8, r9);
        xmm0 = 0;
        rsi = r15;
        rcx = r13;
        rdx = rax;
        rdi = rbp;
        *((rsp + 0xb0)) = xmm0;
        r14 = rax;
        *((rsp + 0xb8)) = 0x10;
        rax = create_token_tree ();
        rsi = rax;
        if (r14 == 0) {
            goto label_25;
        }
        if (rax == 0) {
            goto label_25;
        }
        xmm0 = 0;
        rcx = r13;
        edx = 0;
        rdi = rbp;
        *((rsp + 0xb0)) = xmm0;
        *((rsp + 0xb8)) = 0xa;
        rax = create_token_tree ();
        r15 = rax;
        if (rax == 0) {
            goto label_25;
        }
        rbx++;
    } while (r12 >= rbx);
label_113:
    rsi = *((rsp + 0x20));
    if (rsi == 0) {
        goto label_114;
    }
    xmm0 = 0;
    rcx = r13;
    rdx = r15;
    rdi = rbp;
    *((rsp + 0xb0)) = xmm0;
    *((rsp + 0xb8)) = 0x10;
    rax = create_token_tree ();
    rbx = *((rsp + 0x30));
    ecx = *(rbx);
    if (ecx != 0) {
        goto label_115;
    }
label_4:
    *((rsp + 0x10)) = rax;
label_16:
    rax = *(rsp);
    eax = *((rax + 8));
    if ((*((rsp + 8)) & 0x1000000) != 0) {
        if (al == 0xb) {
            goto label_116;
        }
        if (al == 0x17) {
            goto label_116;
        }
    }
    if (al > 0x17) {
        goto label_2;
    }
    rdx = 0xffffffffff73f7ff;
    bl = (al == 0x12) ? 1 : 0;
    if (((rdx >> rax) & 1) < 0) {
        goto label_2;
    }
    rdi = *((rsp + 0x18));
    rcx = *((rdi + 0x48));
    rdi = *(rsp);
    __asm ("movdqu xmm0, xmmword [rdi]");
    if (al != 0x17) {
        goto label_3;
    }
label_110:
    *((rsp + 0x38)) = rcx;
    *((rsp + 0x20)) = xmm0;
    rax = fetch_number (*((rsp + 0x18)), rdi, *((rsp + 8)));
    __asm ("movdqa xmm0, xmmword [rsp + 0x20]");
    rcx = *((rsp + 0x38));
    rbx = rax;
    if (rax == -1) {
        goto label_117;
    }
    if (rax != 0xfffffffffffffffe) {
        rax = *(rsp);
        eax = *((rax + 8));
        if (al == 0x18) {
            goto label_118;
        }
        if (al == 1) {
            goto label_119;
        }
    }
label_10:
    if ((*((rsp + 8)) & 0x200000) == 0) {
        goto label_120;
    }
label_23:
    rax = *((rsp + 0x18));
    rbx = *((rsp + 0x30));
    *((rax + 0x48)) = rcx;
    rax = *(rsp);
    __asm ("movups xmmword [rax], xmm0");
    *((rax + 8)) = 1;
    ecx = *(rbx);
    rax = *((rsp + 0x10));
    if (ecx == 0) {
        goto label_4;
    }
label_115:
    if (rax != 0) {
        goto label_4;
    }
    goto label_28;
    rax = *((rsp + 0x18));
    rbx = *((rsp + 0x30));
    rcx = 0x00017069;
    r8b = (dl == 0x23) ? 1 : 0;
    rdx = "space";
    rsi = *((rax + 0x78));
    r9 = rbx;
    r8d = (int32_t) r8b;
    goto label_5;
    rcx = *(rsp);
    edx = 0;
    esi = 0;
    rdi = rbp;
    rax = create_token_tree ();
    *((rsp + 0x10)) = rax;
    if (rax == 0) {
        goto label_6;
    }
    if (*((rbp + 0xb4)) <= 1) {
        goto label_7;
    }
    *((rbp + 0xb0)) |= 2;
    goto label_7;
    *((rsp + 0x88)) = 0;
    rax = calloc (0x20, 1);
    r13 = rax;
    rax = calloc (0x50, 1);
    rbx = rax;
    *((rsp + 0x20)) = rax;
    al = (r13 == 0) ? 1 : 0;
    dl = (rbx == 0) ? 1 : 0;
    al |= dl;
    if (al != 0) {
        goto label_121;
    }
    rbx = *(rsp);
    rdx = *((rsp + 8));
    eax = peek_token_bracket (rbx, *((rsp + 0x18)));
    ecx = eax;
    eax = *((rbx + 8));
    if (al == 2) {
        goto label_122;
    }
    *((rsp + 0x7a)) = 0;
    if (al == 0x19) {
        goto label_123;
    }
label_37:
    if (al == 0x15) {
        goto label_124;
    }
label_35:
    rax = rsp + 0xc0;
    r14 = *(rsp);
    r12d = ecx;
    rbx = *((rsp + 8));
    *((rsp + 0x40)) = rax;
    r15 = *((rsp + 0x20));
    rax = rsp + 0x90;
    r9d = 1;
    *((rsp + 0x38)) = rax;
    rax = rsp + 0x88;
    *((rsp + 0x70)) = rax;
    rax = rbx;
    rax >>= 0x10;
    *((rsp + 0x10)) = rbp;
    rbp = *((rsp + 0x18));
    *((rsp + 0x58)) = 0;
    eax &= 1;
    *((rsp + 0x50)) = 0;
    *((rsp + 0x7b)) = al;
label_9:
    rax = *((rsp + 0x40));
    rdi = *((rsp + 0x38));
    r8 = rbx;
    ecx = r12d;
    r9d &= 1;
    rdx = r14;
    rsi = rbp;
    *((rsp + 0x90)) = 3;
    *((rsp + 0x98)) = rax;
    eax = parse_bracket_element_constprop_0 ();
    if (eax != 0) {
        goto label_40;
    }
    rdx = rbx;
    eax = peek_token_bracket (r14, rbp);
    r10d = *((rsp + 0x90));
    r12d = eax;
    eax = r10 - 2;
    eax &= 0xfffffffd;
    if (eax != 0) {
        eax = *((r14 + 8));
        if (al == 2) {
            goto label_125;
        }
        if (al == 0x16) {
            goto label_126;
        }
    }
label_42:
    rcx = 0x00016d34;
    rax = *((rcx + r10*4));
    rax += rcx;
    /* switch table (22 cases) at 0x16d34 */
    void (*rax)() ();
    *((rsp + 0x10)) = 0;
    rax = *((rsp + 0x30));
    *(rax) = 5;
    goto label_2;
    rcx = *(rsp);
    edx = 0;
    esi = 0;
    rdi = rbp;
    rax = create_token_tree ();
    *((rsp + 0x10)) = rax;
    if (rax == 0) {
        goto label_6;
    }
    if (*((rbp + 0xb4)) <= 1) {
        goto label_7;
    }
    rax = *((rsp + 0x18));
    r15 = *((rsp + 0x10));
    r12 = rsp + 0xb0;
    r14 = *(rsp);
    rbx = *((rax + 0x68));
    r13 = *((rax + 0x48));
    *((rsp + 0x20)) = rbx;
    rbx = rax;
    goto label_127;
label_8:
    if (rax == 0) {
        goto label_6;
    }
label_127:
    if (r13 >= *((rsp + 0x20))) {
        goto label_128;
    }
    if (*((rbx + 0x30)) == r13) {
        goto label_128;
    }
    rax = *((rbx + 0x10));
    if (*((rax + r13*4)) != 0xffffffff) {
        goto label_128;
    }
    eax = peek_token (r14, rbx, *((rsp + 8)), rcx, r8);
    rcx = r14;
    edx = 0;
    esi = 0;
    r13 = (int64_t) eax;
    r13 += *((rbx + 0x48));
    rdi = rbp;
    *((rbx + 0x48)) = r13;
    rax = create_token_tree ();
    xmm0 = 0;
    rsi = r15;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    *((rsp + 0x10)) = rax;
    *((rsp + 0xb0)) = xmm0;
    *((rsp + 0xb8)) = 0x10;
    rax = create_token_tree ();
    rdx = *((rsp + 0x10));
    r15 = rax;
    if (rdx != 0) {
        goto label_8;
    }
    goto label_6;
    rax = *(rsp);
    rcx = *(rax);
    eax = 1;
    eax <<= cl;
    rax = (int64_t) eax;
    rdx = rax;
    rdx &= *((rbp + 0xa8));
    if (rdx == 0) {
        goto label_129;
    }
    *((rbp + 0xa0)) |= rax;
    rcx = *(rsp);
    edx = 0;
    esi = 0;
    rdi = rbp;
    rax = create_token_tree ();
    *((rsp + 0x10)) = rax;
    if (rax == 0) {
        goto label_6;
    }
    *((rbp + 0x98))++;
    *((rbp + 0xb0)) |= 2;
    goto label_7;
    rax = *(rsp);
    r12d = *(rax);
    if ((r12d & 0x30f) != 0) {
        eax = *((rbp + 0xb0));
        if ((al & 0x10) != 0) {
            goto label_59;
        }
        eax |= 0x10;
        *((rbp + 0xb0)) = al;
        if ((al & 8) != 0) {
            goto label_130;
        }
        rbx = 0x3ff000000000000;
        *((rbp + 0xb8)) = rbx;
        rbx = 0x7fffffe87fffffe;
        *((rbp + 0xc0)) = rbx;
        if ((al & 4) == 0) {
            goto label_131;
        }
        rax = *(rsp);
        xmm0 = 0;
        __asm ("movups xmmword [rbp + 0xc8], xmm0");
        r12d = *(rax);
    }
label_59:
    eax = r12 - 0x100;
    if ((eax & 0xfffffeff) != 0) {
        goto label_132;
    }
    rax = *(rsp);
    if (r12d == 0x100) {
        goto label_133;
    }
    *(rax) = 5;
    rcx = rax;
    edx = 0;
    esi = 0;
    rdi = rbp;
    rax = create_token_tree ();
    rbx = rax;
    eax = 0xa;
label_36:
    rcx = *(rsp);
    edx = 0;
    esi = 0;
    rdi = rbp;
    *(rcx) = eax;
    rax = create_token_tree ();
    xmm0 = 0;
    rsi = rbx;
    rdi = rbp;
    rcx = rsp + 0xb0;
    rdx = rax;
    r12 = rax;
    *((rsp + 0xb0)) = xmm0;
    *((rsp + 0xb8)) = 0xa;
    rax = create_token_tree ();
    *((rsp + 0x10)) = rax;
    rcx = rax;
    al = (rbx == 0) ? 1 : 0;
    dl = (r12 == 0) ? 1 : 0;
    al |= dl;
    if (al != 0) {
        goto label_6;
    }
    if (rcx == 0) {
        goto label_6;
    }
label_38:
    rbx = *((rsp + 0x18));
    rax = peek_token (*(rsp), rbx, *((rsp + 8)), rcx, r8);
    rax = (int64_t) eax;
    *((rbx + 0x48)) += rax;
    goto label_2;
    rbx = *((r12 + 0x30));
    r15 = *((rsp + 8));
    r14 = *((rsp + 0x18));
    rax = rbx + 1;
    *((r12 + 0x30)) = rax;
    rdx |= 0x800000;
    rax = peek_token (*(rsp), r14, r15, rcx, r8);
    rdx = *(rsp);
    esi = 0;
    r11 = r14;
    rax = (int64_t) eax;
    *((r14 + 0x48)) += rax;
    if (*((rdx + 8)) != 9) {
        goto label_134;
    }
label_30:
    if (rbx <= 8) {
        goto label_135;
    }
label_33:
    xmm0 = 0;
    rcx = rsp + 0xb0;
    edx = 0;
    rdi = rbp;
    *((rsp + 0xb0)) = xmm0;
    *((rsp + 0xb8)) = 0x11;
    rax = create_token_tree ();
    *((rsp + 0x10)) = rax;
    if (rax == 0) {
        goto label_6;
    }
    *((rax + 0x28)) = rbx;
    goto label_7;
    rdx = *((rsp + 0x98));
    *((rsp + 0x48)) = rdx;
    rax = strlen (*((rsp + 0x98)));
    if (rax != 1) {
        goto label_51;
    }
    rdx = *((rsp + 0x48));
    ecx = *(rdx);
    rdx = rcx;
    rax <<= cl;
    rdx >>= 3;
    edx &= 0x18;
    *((r13 + rdx)) |= rax;
label_41:
    rax = *((rsp + 0x30));
    *(rax) = 0;
label_34:
    eax = *((r14 + 8));
    if (al == 2) {
        goto label_125;
    }
    if (al == 0x15) {
        goto label_136;
    }
    r9d = 0;
    goto label_9;
label_117:
    rax = *(rsp);
    if (*((rax + 8)) != 1) {
        goto label_43;
    }
    rax = *(rsp);
    if (*(rax) != 0x2c) {
        goto label_43;
    }
    ebx = 0;
label_22:
    *((rsp + 0x38)) = rcx;
    *((rsp + 0x20)) = xmm0;
    rax = fetch_number (*((rsp + 0x18)), *(rsp), *((rsp + 8)));
    __asm ("movdqa xmm0, xmmword [rsp + 0x20]");
    rcx = *((rsp + 0x38));
    r12 = rax;
    if (rax == 0xfffffffffffffffe) {
        goto label_10;
    }
    if (rax != -1) {
        if (rbx > rax) {
            goto label_43;
        }
    }
    rax = *(rsp);
    if (*((rax + 8)) != 0x18) {
        goto label_43;
    }
    if (r12 != -1) {
        goto label_137;
    }
    if (rbx > 0x7fff) {
        goto label_138;
    }
    r15 = *((rsp + 0x18));
    rax = peek_token (*(rsp), r15, *((rsp + 8)), rcx, r8);
    rax = (int64_t) eax;
    *((r15 + 0x48)) += rax;
    if (*((rsp + 0x10)) == 0) {
        goto label_17;
    }
label_18:
    r14 = *((rsp + 0x10));
    if (rbx <= 0) {
        goto label_139;
    }
    *((rsp + 0x20)) = r14;
    if (rbx == 1) {
        goto label_140;
    }
    r15d = 2;
    while (r14 != 0) {
        if (rax == 0) {
            goto label_25;
        }
        r15++;
        if (r15 > rbx) {
            goto label_140;
        }
        rax = duplicate_tree (r14, rbp, rdx, rcx, r8, r9);
        rsi = *((rsp + 0x20));
        rcx = r13;
        rdi = rbp;
        xmm0 = 0;
        rdx = rax;
        r14 = rax;
        *((rsp + 0xb0)) = xmm0;
        *((rsp + 0xb8)) = 0x10;
        rax = create_token_tree ();
        *((rsp + 0x20)) = rax;
    }
label_25:
    rax = *((rsp + 0x30));
    *(rax) = 0xc;
    do {
label_11:
        rax = *((rsp + 0x10));
        rbx = *((rsp + 0x10));
        rax = *((rax + 8));
        *((rsp + 0x10)) = rax;
    } while (rax != 0);
    rax = *((rbx + 0x10));
    *((rsp + 0x10)) = rax;
    if (rax != 0) {
        goto label_11;
    }
    goto label_141;
label_12:
    rax = *(rbx);
    if (rax == 0) {
        goto label_1;
    }
    rcx = *((rax + 0x10));
    *((rsp + 0x10)) = rcx;
    if (rcx == 0) {
        goto label_142;
    }
    if (rbx != rcx) {
        goto label_11;
    }
label_142:
    rbx = rax;
label_141:
    eax = free_tree (0, rbx, rdx, rcx);
    if (eax == 0) {
        goto label_12;
    }
    goto label_1;
label_112:
    rsi = *((r14 + 0x28));
    rax = r14;
    do {
label_13:
        rdx = rax;
        rax = *((rax + 8));
    } while (rax != 0);
    rax = *((rdx + 0x10));
    if (rax != 0) {
        goto label_13;
    }
    goto label_143;
label_15:
    rcx = *(rdx);
    if (rcx == 0) {
        goto label_14;
    }
    rax = *((rcx + 0x10));
    if (rdx == rax) {
        goto label_144;
    }
    if (rax != 0) {
        goto label_13;
    }
label_144:
    rdx = rcx;
label_143:
    if (*((rdx + 0x30)) != 0x11) {
        goto label_15;
    }
    if (rsi != *((rdx + 0x28))) {
        goto label_15;
    }
    *((rdx + 0x32)) |= 8;
    goto label_15;
label_17:
    rax = *((rsp + 0x30));
    esi = *(rax);
    if (esi != 0) {
        goto label_1;
    }
label_21:
    *((rsp + 0x10)) = 0;
    goto label_16;
label_118:
    r12 = rbx;
label_137:
    if (r12 > 0x7fff) {
        goto label_138;
    }
    r15 = *((rsp + 0x18));
    rax = peek_token (*(rsp), r15, *((rsp + 8)), rcx, r8);
    rax = (int64_t) eax;
    *((r15 + 0x48)) += rax;
    rax = *((rsp + 0x10));
    if (rax == 0) {
        goto label_17;
    }
    rcx = rbx;
    rcx |= r12;
    if (rcx != 0) {
        goto label_18;
    }
    do {
label_19:
        rbx = rax;
        rax = *((rax + 8));
    } while (rax != 0);
    rax = *((rbx + 0x10));
    if (rax != 0) {
        goto label_19;
    }
    goto label_145;
label_20:
    rdx = *(rbx);
    if (rdx == 0) {
        goto label_146;
    }
    rax = *((rdx + 0x10));
    if (rax == 0) {
        goto label_147;
    }
    if (rbx != rax) {
        goto label_19;
    }
label_147:
    rbx = rdx;
label_145:
    eax = free_tree (0, rbx, rdx, rcx);
    if (eax == 0) {
        goto label_20;
    }
label_146:
    rax = *((rsp + 0x30));
    edx = *(rax);
    if (edx == 0) {
        goto label_21;
    }
    goto label_11;
label_119:
    rax = *(rsp);
    if (*(rax) == 0x2c) {
        goto label_22;
    }
    if ((*((rsp + 8)) & 0x200000) != 0) {
        goto label_23;
    }
label_43:
    rax = *((rsp + 0x30));
    *(rax) = 0xa;
label_28:
    if (*((rsp + 0x10)) == 0) {
        goto label_1;
    }
    goto label_11;
    do {
        r15 = *((rsp + 0x20));
label_114:
        *((rsp + 0x10)) = r15;
        goto label_16;
label_111:
        *((rsp + 0x20)) = r14;
        ebx = 1;
label_140:
    } while (rbx == r12);
    rax = duplicate_tree (r14, rbp, rdx, rcx, r8, r9);
    r14 = rax;
    if (rax != 0) {
        goto label_24;
    }
    goto label_25;
label_116:
    rax = *((rsp + 0x10));
    if (rax == 0) {
        goto label_109;
    }
    do {
label_26:
        rbx = rax;
        rax = *((rax + 8));
    } while (rax != 0);
    rax = *((rbx + 0x10));
    if (rax != 0) {
        goto label_26;
    }
    goto label_148;
label_27:
    rdx = *(rbx);
    if (rdx == 0) {
        goto label_109;
    }
    rax = *((rdx + 0x10));
    if (rax == 0) {
        goto label_149;
    }
    if (rbx != rax) {
        goto label_26;
    }
label_149:
    rbx = rdx;
label_148:
    eax = free_tree (0, rbx, rdx, rcx);
    if (eax == 0) {
        goto label_27;
    }
label_109:
    *((rsp + 0x10)) = 0;
    rax = *((rsp + 0x30));
    *(rax) = 0xd;
    goto label_2;
label_138:
    rax = *((rsp + 0x30));
    *(rax) = 0xf;
    goto label_28;
label_107:
    if ((*((rsp + 8)) & 0x20000) != 0) {
        goto label_29;
    }
    *((rsp + 0x10)) = 0;
    rax = *((rsp + 0x30));
    *(rax) = 0x10;
    goto label_2;
label_134:
    r14 = *((rsp + 0x30));
    rcx = r15;
    rax = parse_reg_exp (r11, r12, rdx, esi, r13 + 1, r14);
    r12d = *(r14);
    rsi = rax;
    if (r12d != 0) {
        goto label_1;
    }
    rax = *(rsp);
    if (*((rax + 8)) == 9) {
        goto label_30;
    }
    if (rsi == 0) {
        goto label_150;
    }
    do {
label_31:
        rbx = rsi;
        rsi = *((rsi + 8));
    } while (rsi != 0);
    rsi = *((rbx + 0x10));
    if (rsi != 0) {
        goto label_31;
    }
    goto label_151;
label_32:
    rax = *(rbx);
    if (rax == 0) {
        goto label_150;
    }
    rsi = *((rax + 0x10));
    if (ebx == esi) {
        goto label_152;
    }
    if (rsi != 0) {
        goto label_31;
    }
label_152:
    rbx = rax;
label_151:
    eax = free_tree (0, rbx, rdx, rcx);
    if (eax == 0) {
        goto label_32;
    }
label_150:
    rax = *((rsp + 0x30));
    *(rax) = 8;
    goto label_1;
label_135:
    eax = 1;
    ecx = ebx;
    eax <<= cl;
    rax = (int64_t) eax;
    *((rbp + 0xa8)) |= rax;
    goto label_33;
    rax = *((r15 + 0x28));
    rdi = *(r15);
    if (rax == *((rsp + 0x50))) {
        goto label_153;
    }
label_46:
    rdx = rax + 1;
    *((r15 + 0x28)) = rdx;
    edx = *((rsp + 0x98));
    *((rdi + rax*4)) = edx;
    goto label_34;
    ecx = *((rsp + 0x98));
    edx = 1;
    rax = rcx;
    rdx <<= cl;
    rax >>= 3;
    eax &= 0x18;
    *((r13 + rax)) |= rdx;
    goto label_34;
    eax = build_charclass (*((rbp + 0x78)), r13, r15, *((rsp + 0x70)), *((rsp + 0x98)), rbx);
    rcx = *((rsp + 0x30));
    *(rcx) = eax;
    if (eax == 0) {
        goto label_34;
    }
    rbp = *((rsp + 0x10));
label_39:
    free (r13);
    rbx = *((rsp + 0x20));
    free (*(rbx));
    free (*((rbx + 8)));
    free (*((rbx + 0x10)));
    free (*((rbx + 0x18)));
    free (rbx);
    rax = *((rsp + 0x30));
    r8d = *(rax);
    if (r8d != 0) {
        goto label_1;
    }
    *((rsp + 0x10)) = 0;
    goto label_7;
label_124:
    rax = *(rsp);
    *((rax + 8)) = 1;
    goto label_35;
label_133:
    *(rax) = 6;
    rcx = rax;
    edx = 0;
    esi = 0;
    rdi = rbp;
    rax = create_token_tree ();
    rbx = rax;
    eax = 9;
    goto label_36;
label_123:
    rax = *((rsp + 0x20));
    *((rax + 0x20)) |= 1;
    if ((*((rsp + 8)) & 0x100) != 0) {
        *(r13) |= 0x400;
    }
    rbx = *((rsp + 0x18));
    rax = (int64_t) ecx;
    rdx = *((rsp + 8));
    *((rbx + 0x48)) += rax;
    rbx = *(rsp);
    eax = peek_token_bracket (*(rsp), rbx);
    ecx = eax;
    eax = *((rbx + 8));
    if (al == 2) {
        goto label_122;
    }
    *((rsp + 0x7a)) = 1;
    goto label_37;
label_132:
    rcx = *(rsp);
    edx = 0;
    esi = 0;
    rdi = rbp;
    rax = create_token_tree ();
    *((rsp + 0x10)) = rax;
    if (rax != 0) {
        goto label_38;
    }
    goto label_6;
    do {
label_125:
        rax = *((rsp + 0x30));
        rbp = *((rsp + 0x10));
        *(rax) = 7;
        goto label_39;
label_128:
        *((rsp + 0x10)) = r15;
        goto label_7;
label_40:
        rbx = *((rsp + 0x30));
        rbp = *((rsp + 0x10));
        *(rbx) = eax;
        goto label_39;
label_126:
        rax = (int64_t) r12d;
        *((rbp + 0x48)) += rax;
        r11 = rsp + 0xb0;
        rdx = rbx;
        eax = peek_token_bracket (r11, rsi);
        ecx = eax;
        eax = *((rsp + 0xb8));
    } while (al == 2);
    if (al == 0x15) {
        goto label_154;
    }
    rax = rsp + 0xe0;
    r9d = 1;
    r8 = rbx;
    rdx = r11;
    rdi = rsp + 0xa0;
    *((rsp + 0xa8)) = rax;
    *((rsp + 0xa0)) = 3;
    eax = parse_bracket_element_constprop_0 ();
    if (eax != 0) {
        goto label_40;
    }
    rdx = rbx;
    eax = peek_token_bracket (r14, rbp);
    r12d = eax;
    eax = *((rsp + 0x90));
    *((rsp + 0x48)) = eax;
    eax -= 2;
    eax &= 0xfffffffd;
    if (eax == 0) {
        goto label_155;
    }
    edx = *((rsp + 0xa0));
    eax = rdx - 2;
    eax &= 0xfffffffd;
    if (eax == 0) {
        goto label_155;
    }
    rsp + 0x60 = (*((rsp + 0x48)) == 3) ? 1 : 0;
    if (*((rsp + 0x48)) == 3) {
        goto label_156;
    }
    if (edx == 3) {
        goto label_157;
    }
    eax = *((rsp + 0x48));
    if (eax != 0) {
        goto label_158;
    }
label_63:
    r9d = *((rsp + 0x98));
label_53:
    if (edx == 0) {
        goto label_61;
    }
    if (edx != 3) {
        goto label_159;
    }
    rdi = *((rsp + 0xa8));
label_62:
    r10d = *(rdi);
label_44:
    r11d = *((rsp + 0x48));
    if (r11d == 0) {
        goto label_160;
    }
    if (*((rsp + 0x60)) != 0) {
        goto label_160;
    }
    r9d = *((rsp + 0x98));
label_45:
    if (edx == 0) {
        goto label_161;
    }
    if (edx == 3) {
        goto label_161;
    }
label_56:
    r10d = *((rsp + 0xa8));
label_50:
    if (r9d == 0xffffffff) {
        goto label_51;
    }
    if (r10d == 0xffffffff) {
        goto label_51;
    }
label_55:
    if (*((rsp + 0x7b)) != 0) {
        if (r10d < r9d) {
            goto label_155;
        }
    }
    rax = *((rsp + 0x10));
    if (*((rax + 0xb4)) > 1) {
        rax = *((r15 + 0x40));
        rdi = *((r15 + 8));
        if (rax == *((rsp + 0x58))) {
            goto label_162;
        }
label_54:
        rdx = *((r15 + 0x10));
        rcx = rax + 1;
        *((rdi + rax*4)) = r9d;
        *((r15 + 0x40)) = rcx;
        *((rdx + rax*4)) = r10d;
    }
    eax = 0;
    esi = 1;
    do {
        ecx = eax;
        if (r9d <= eax) {
            if (r10d < eax) {
                goto label_163;
            }
            rdx = rax;
            rdi = rsi;
            rdx >>= 6;
            rdi <<= cl;
            *((r13 + rdx*8)) |= rdi;
        }
label_163:
        rax++;
    } while (rax != 0x100);
    goto label_41;
label_154:
    eax = r12d;
    eax = -eax;
    rax = (int64_t) eax;
    *((rbp + 0x48)) += rax;
    *((r14 + 8)) = 1;
    goto label_42;
label_121:
    free (r13);
    free (*((rsp + 0x20)));
    goto label_6;
label_129:
    *((rsp + 0x10)) = 0;
    rax = *((rsp + 0x30));
    *(rax) = 6;
    goto label_2;
label_52:
    *((rsp + 0x7c)) = edx;
    rax = strlen (*((rsp + 0xa8)));
    edx = *((rsp + 0x7c));
    if (rax <= 1) {
        goto label_164;
    }
label_51:
    rax = *((rsp + 0x30));
    rbp = *((rsp + 0x10));
    *(rax) = 3;
    goto label_39;
label_122:
    rax = *((rsp + 0x30));
    *(rax) = 2;
    goto label_39;
label_120:
    rax = *(rsp);
    if (*((rax + 8)) != 2) {
        goto label_43;
    }
    rax = *((rsp + 0x30));
    *(rax) = 9;
    goto label_28;
label_136:
    rbx = *((rsp + 0x18));
    rax = (int64_t) r12d;
    rbp = *((rsp + 0x10));
    *((rbx + 0x48)) += rax;
    if (*((rsp + 0x7a)) == 0) {
        goto label_165;
    }
    rax = r13;
    rdx = r13 + 0x20;
    do {
        rax = ~rax;
        rax += 8;
    } while (rdx != rax);
label_165:
    edx = *((rbp + 0xb4));
    if (edx > 1) {
        goto label_166;
    }
label_49:
    rax = *((rsp + 0x20));
    if (*((rax + 0x28)) != 0) {
        goto label_167;
    }
    rax = *((rsp + 0x20));
    if (*((rax + 0x30)) != 0) {
        goto label_167;
    }
    if (*((rax + 0x38)) != 0) {
        goto label_167;
    }
    if (*((rax + 0x40)) != 0) {
        goto label_167;
    }
    edx--;
    if (edx > 0) {
        if (*((rax + 0x48)) != 0) {
            goto label_167;
        }
        if ((*((rax + 0x20)) & 1) != 0) {
            goto label_167;
        }
    }
    rbx = *((rsp + 0x20));
    free (*(rbx));
    free (*((rbx + 8)));
    free (*((rbx + 0x10)));
    free (*((rbx + 0x18)));
    free (rbx);
    *((rsp + 0xa8)) = 3;
    edx = 0;
    esi = 0;
    *((rsp + 0xa0)) = r13;
    rcx = rsp + 0xa0;
label_48:
    rdi = rbp;
    rax = create_token_tree ();
    *((rsp + 0x10)) = rax;
    if (rax != 0) {
        goto label_7;
    }
    do {
label_47:
        rax = *((rsp + 0x30));
        *(rax) = 0xc;
        goto label_39;
label_61:
        r10d = *((rsp + 0xa8));
        goto label_44;
label_160:
        rax = *((rsp + 0x10));
        if (*((rax + 0xb4)) <= 1) {
            goto label_168;
        }
        *((rsp + 0x48)) = r10d;
        eax = btowc (r9d);
        edx = *((rsp + 0xa0));
        r10d = *((rsp + 0x48));
        r9d = eax;
        goto label_45;
label_153:
        rax = *((rsp + 0x50));
        rax = rax + rax + 1;
        *((rsp + 0x50)) = rax;
        rax = realloc (rdi, rax*4);
        rdi = rax;
        if (rax == 0) {
            goto label_169;
        }
        *(r15) = rax;
        rax = *((r15 + 0x28));
        goto label_46;
label_167:
        *((rbp + 0xb0)) |= 2;
        rax = *((rsp + 0x20));
        edx = 0;
        esi = 0;
        r12 = rsp + 0xa0;
        rdi = rbp;
        *((rsp + 0xa8)) = 6;
        rcx = r12;
        *((rsp + 0xa0)) = rax;
        rax = create_token_tree ();
        rdx = r13 + 0x20;
        rbx = rax;
        *((rsp + 0x10)) = rax;
        rax = r13;
    } while (rbx == 0);
    do {
        if (*(rax) != 0) {
            goto label_170;
        }
        rax += 8;
    } while (rdx != rax);
    free (r13);
    goto label_7;
label_170:
    esi = 0;
    rcx = r12;
    edx = 0;
    rdi = rbp;
    *((rsp + 0xa8)) = 3;
    *((rsp + 0xa0)) = r13;
    rax = create_token_tree ();
    rsi = rax;
    if (rax == 0) {
        goto label_47;
    }
    xmm0 = 0;
    rdx = *((rsp + 0x10));
    rcx = rsp + 0xb0;
    *((rsp + 0xb0)) = xmm0;
    *((rsp + 0xb8)) = 0xa;
    goto label_48;
label_166:
    rcx = *((rbp + 0x78));
    eax = 0;
    do {
        rsi = *((rcx + rax));
        *((r13 + rax)) &= rsi;
        rax += 8;
    } while (rax != 0x20);
    goto label_49;
label_161:
    rax = *((rsp + 0x10));
    if (*((rax + 0xb4)) <= 1) {
        goto label_50;
    }
    *((rsp + 0x48)) = r9d;
    eax = btowc (r10d);
    r9d = *((rsp + 0x48));
    r10d = eax;
    goto label_50;
label_156:
    rax = *((rsp + 0x98));
    *((rsp + 0x7c)) = edx;
    rdi = rax;
    *((rsp + 0x68)) = rax;
    rax = strlen (rdi);
    edx = *((rsp + 0x7c));
    if (rax > 1) {
        goto label_51;
    }
    if (edx == 3) {
        goto label_52;
    }
label_164:
    rax = *((rsp + 0x68));
    r9d = *(rax);
    goto label_53;
label_155:
    rax = *((rsp + 0x30));
    rbp = *((rsp + 0x10));
    *(rax) = 0xb;
    goto label_39;
label_162:
    rax = *((rsp + 0x58));
    *((rsp + 0x7c)) = r9d;
    *((rsp + 0x68)) = r10d;
    rax += rax;
    rax++;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x60)) = rsi;
    rax = realloc (rdi, rax*4);
    *((rsp + 0x48)) = rax;
    rax = realloc (*((r15 + 0x10)), *((rsp + 0x60)));
    r8 = *((rsp + 0x48));
    if (r8 == 0) {
        goto label_171;
    }
    r10d = *((rsp + 0x68));
    r9d = *((rsp + 0x7c));
    if (rax == 0) {
        goto label_171;
    }
    *((r15 + 0x10)) = rax;
    rdi = r8;
    rax = *((r15 + 0x40));
    *((r15 + 8)) = r8;
    goto label_54;
label_168:
    if (edx == 0) {
        goto label_55;
    }
    if (edx != 3) {
        goto label_56;
    }
    goto label_55;
label_171:
    rbx = rax;
    rbp = *((rsp + 0x10));
    free (r8);
    free (rbx);
    rax = *((rsp + 0x30));
    *(rax) = 0xc;
    goto label_39;
label_108:
    stack_chk_fail ();
label_139:
    *((rsp + 0x20)) = 0;
    goto label_24;
label_131:
    ebx = 0x80;
    r13d = 2;
label_60:
    rax = ctype_b_loc ();
    r9d = rbx + 0x100;
    r8 = rax;
    rax = (int64_t) ebx;
    rdx = *(r8);
    r8d = 1;
    rdi = rdx + rax*2;
    rax = (int64_t) r13d;
    r13d <<= 6;
    rsi = rbp + rax*8;
    r9d -= r13d;
label_58:
    eax = 0;
    while (edx != 0x5f) {
label_57:
        rax++;
        if (rax == 0x40) {
            goto label_172;
        }
        ecx = eax;
        if ((*((rdi + rax*2)) & 8) != 0) {
            goto label_173;
        }
        edx = rbx + rax;
    }
label_173:
    rdx = r8;
    rdx <<= cl;
    *((rsi + 0xb8)) |= rdx;
    goto label_57;
label_172:
    ebx += 0x40;
    rdi -= 0xffffffffffffff80;
    rsi += 8;
    if (r9d != ebx) {
        goto label_58;
    }
    goto label_59;
label_130:
    ebx = 0;
    r13d = 0;
    goto label_60;
label_158:
    r9d = 0;
    if (edx == 0) {
        goto label_61;
    }
label_159:
    r10d = 0;
    goto label_44;
label_157:
    *((rsp + 0x7c)) = edx;
    *((rsp + 0x68)) = rdi;
    rax = strlen (*((rsp + 0xa8)));
    if (rax > 1) {
        goto label_51;
    }
    r9d = 0;
    rdi = *((rsp + 0x68));
    edx = *((rsp + 0x7c));
    if (*((rsp + 0x48)) != 0) {
        goto label_62;
    }
    goto label_63;
label_169:
    rbp = *((rsp + 0x10));
    goto label_47;
label_83:
    if (*(rbp) == 0) {
        goto label_174;
    }
    if (*((rbp + 0x40)) == 0) {
        goto label_174;
    }
    rax = rbp + 0xe8;
    esi = 0;
    *((rsp + 0x4c)) = 0;
    rdi = rax;
    *((rsp + 0x30)) = rax;
    eax = pthread_mutex_init ();
    if (eax != 0) {
        goto label_174;
    }
    esi = *((rsp + 0x4c));
    if (esi != 0) {
        goto label_73;
    }
    rax = *((rsp + 8));
    rdx = rsp + 0x88;
    ecx = 0x12;
    esi = *((rbp + 0xb4));
    rdi = rdx;
    r12 = rsp + 0x80;
    rbx = *((rax + 0x28));
    eax = 0;
    *(rdi) = rax;
    rcx--;
    rdi += 8;
    rax = *((rsp + 0x10));
    *((rsp + 0xd8)) = r13;
    *((rsp + 0xf8)) = rbx;
    *((rsp + 0x80)) = rax;
    rax = *((rsp + 0x18));
    *((rsp + 0xd0)) = r13;
    eax &= 0x400000;
    *((rsp + 0x110)) = esi;
    *((rsp + 0x20)) = rax;
    rsp + 0x108 = (eax != 0) ? 1 : 0;
    rbx |= rax;
    eax = *((rbp + 0xb0));
    cl = (rbx != 0) ? 1 : 0;
    *((rsp + 0xe8)) = r13;
    edx = eax;
    al >>= 3;
    *((rsp + 0x10b)) = cl;
    dl >>= 2;
    eax &= 1;
    *((rsp + 0xe0)) = r13;
    edx &= 1;
    *((rsp + 0x10a)) = al;
    *((rsp + 0x109)) = dl;
    if (r13 != 0) {
        goto label_175;
    }
label_72:
    rax = *((rsp + 0x10));
    if (cl != 0) {
        rax = *((rsp + 0x88));
    }
    *((rsp + 0x10)) = rax;
    *((rsp + 0x88)) = rax;
    if (*((rsp + 0x20)) == 0) {
        goto label_176;
    }
    if (esi <= 1) {
        goto label_177;
    }
    do {
        eax = build_wcs_upper_buffer (r12);
        if (eax != 0) {
            goto label_71;
        }
        if (r13 <= *((rsp + 0xb8))) {
            goto label_95;
        }
        rsi = *((rsp + 0xc0));
        rax = *((rbp + 0xb4));
        rax += *((rsp + 0xb0));
        if (rsi > rax) {
            goto label_95;
        }
        rsi += rsi;
        eax = re_string_realloc_buffers (r12, rsi);
    } while (eax == 0);
label_71:
    *((rsp + 0x4c)) = eax;
    rax = *((rsp + 8));
    r15 = *(rax);
    goto label_87;
label_176:
    if (esi > 1) {
        goto label_178;
    }
    rcx = *((rsp + 0xc0));
    if (rbx == 0) {
        goto label_106;
    }
    rax = *((rsp + 0xd8));
    if (rcx > rax) {
        rcx = rax;
    }
    rax = *((rsp + 0xb0));
    if (rcx <= rax) {
        goto label_179;
    }
    rdi = *((rsp + 0x10));
    while (rcx != rax) {
        rdi = *((rsp + 0x88));
        rdx = *((rsp + 0x80));
        rsi = *((rsp + 0xf8));
        rdx += rax;
        rdx += *((rsp + 0xa8));
        edx = *(rdx);
        edx = *((rsi + rdx));
        *((rdi + rax)) = dl;
        rax++;
    }
label_106:
    *((rsp + 0xb0)) = rcx;
    *((rsp + 0xb8)) = rcx;
label_95:
    r15 = *((rsp + 8));
    rbx = *((rsp + 0x18));
    r13 = rsp + 0x50;
    *((rsp + 0x4c)) = 0;
    r14 = *(r15);
    *((r15 + 0x30)) = 0;
    rdx = rbx;
    rdx |= 0x800000;
    *((r14 + 0xd8)) = rbx;
    rax = peek_token (r13, r12, rdx, rcx, r8);
    rax = (int64_t) eax;
    rax = parse_reg_exp (r12, r15, r13, rbx, 0, rsp + 0x4c);
    ecx = *((rsp + 0x4c));
    r15 = rax;
    if (ecx != 0) {
        goto label_180;
    }
    r12 = rsp + 0x60;
    xmm0 = 0;
    edx = 0;
    esi = 0;
    rcx = r12;
    rdi = r14;
    *((rsp + 0x60)) = xmm0;
    *((rsp + 0x68)) = 2;
    rax = create_token_tree ();
    rbx = rax;
    if (r15 != 0) {
label_101:
        xmm0 = 0;
        rdx = rbx;
        rcx = r12;
        rsi = r15;
        rdi = r14;
        *((rsp + 0x60)) = xmm0;
        *((rsp + 0x68)) = 0x10;
        rax = create_token_tree ();
        r8 = rax;
        rax = rbx;
        rbx = r8;
    }
    rcx = *((rsp + 8));
    al = (rax == 0) ? 1 : 0;
    dl = (rbx == 0) ? 1 : 0;
    r15 = *(rcx);
    al |= dl;
    if (al != 0) {
        goto label_181;
    }
    r14 = *((r15 + 8));
    *((rbp + 0x68)) = rbx;
    r14 *= 3;
    *((rsp + 0x10)) = rdi;
    r14 <<= 3;
    rax = malloc (r14*8);
    *((r15 + 0x18)) = rax;
    rbx = rax;
    rax = malloc (*((rsp + 0x10)));
    *((r15 + 0x20)) = rax;
    *((rsp + 0x18)) = rax;
    rax = malloc (r14);
    *((r15 + 0x28)) = rax;
    *((rsp + 0x10)) = rax;
    rax = malloc (r14);
    rcx = *((rsp + 0x10));
    rdx = *((rsp + 0x18));
    *((r15 + 0x30)) = rax;
    if (rbx == 0) {
        goto label_182;
    }
    if (rdx == 0) {
        goto label_182;
    }
    if (rcx == 0) {
        goto label_182;
    }
    if (rax == 0) {
        goto label_182;
    }
    rax = *((rsp + 8));
    r14 = *((rax + 0x30));
    rax = malloc (r14*8);
    *((r15 + 0xe0)) = rax;
    rdi = rax;
    if (rax == 0) {
        goto label_183;
    }
    eax = 0;
    if (r14 == 0) {
        goto label_184;
    }
    do {
        *((rdi + rax*8)) = rax;
        rax++;
    } while (rax != r14);
label_184:
    rbx = *((r15 + 0x68));
    r9d = 1;
    esi = 1;
    ecx = *((rbx + 0x30));
    rax = rbx;
label_70:
    rdx = *((rax + 8));
    if (cl == 4) {
        goto label_185;
    }
    do {
        if (cl == 0x11) {
            goto label_186;
        }
label_96:
        if (rdx == 0) {
            goto label_69;
        }
label_64:
        ecx = *((rdx + 0x30));
        rax = rdx;
        rdx = *((rax + 8));
    } while (cl != 4);
label_185:
    rcx = *((rax + 0x28));
    r10d = esi;
    rcx = *((rdi + rcx*8));
    r10d <<= cl;
    *((rax + 0x28)) = rcx;
    rcx = (int64_t) r10d;
    *((r15 + 0xa0)) |= rcx;
    if (rdx != 0) {
        goto label_64;
    }
label_69:
    ecx = 0;
    goto label_187;
label_65:
    rax = rdx;
label_187:
    rdx = *((rax + 0x10));
    if (rdx == 0) {
        goto label_188;
    }
    if (rdx != rcx) {
        goto label_64;
    }
label_188:
    rdx = *(rax);
    rcx = rax;
    if (rdx != 0) {
        goto label_65;
    }
    eax = 0;
    if (r14 == 0) {
        goto label_189;
    }
    do {
        if (*((rdi + rax*8)) != rax) {
            goto label_190;
        }
        rax++;
    } while (rax != r14);
label_189:
    free (rdi);
    *((r15 + 0xe0)) = 0;
label_183:
    rbx = *((r15 + 0x68));
label_190:
    rdx = *((r15 + 0x68));
    do {
label_66:
        r14 = rdx;
        rdx = *((rdx + 8));
    } while (rdx != 0);
    rdx = *((r14 + 0x10));
    if (rdx != 0) {
        goto label_66;
    }
    goto label_191;
label_67:
    rdx = *((r14 + 0x10));
    if (rdx != 0) {
        if (*((rdx + 0x30)) == 0x11) {
            goto label_192;
        }
label_68:
        eax = *((rsp + 0x60));
        if (eax != 0) {
            goto label_93;
        }
    }
    rax = *(r14);
    if (rax == 0) {
        goto label_74;
    }
    rdx = *((rax + 0x10));
    if (r14 == rdx) {
        goto label_193;
    }
    if (rdx != 0) {
        goto label_66;
    }
label_193:
    rdx = *((rax + 8));
    r14 = rax;
label_191:
    *((rsp + 0x60)) = 0;
    if (rdx == 0) {
        goto label_67;
    }
    if (*((rdx + 0x30)) != 0x11) {
        goto label_67;
    }
    rax = lower_subexp (r12, *((rsp + 8)), rdx, rcx, r8);
    *((r14 + 8)) = rax;
    if (rax != 0) {
        *(rax) = r14;
    }
    rdx = *((r14 + 0x10));
    if (rdx == 0) {
        goto label_68;
    }
    if (*((rdx + 0x30)) != 0x11) {
        goto label_68;
    }
label_192:
    rax = lower_subexp (r12, *((rsp + 8)), rdx, rcx, r8);
    *((r14 + 0x10)) = rax;
    if (rax == 0) {
        goto label_68;
    }
    *(rax) = r14;
    goto label_68;
label_186:
    if (rdx == 0) {
        goto label_69;
    }
    ecx = *((rdx + 0x30));
    r8 = *((rdx + 8));
    if (cl == 0x11) {
        goto label_194;
    }
    rax = rdx;
    goto label_70;
label_182:
    *((rsp + 0x4c)) = 0xc;
label_87:
    rbx = *((r15 + 0x70));
    if (rbx == 0) {
        goto label_195;
    }
    do {
        rdi = rbx;
        rbx = *(rbx);
        free (rdi);
    } while (rbx != 0);
label_195:
    *((r15 + 0x70)) = 0;
    *((r15 + 0x80)) = 0xf;
    *((r15 + 0x68)) = 0;
    free (*((r15 + 0x20)));
    *((r15 + 0x20)) = 0;
    free (*((rsp + 0x90)));
    free (*((rsp + 0x98)));
    if (*((rsp + 0x10b)) != 0) {
        goto label_196;
    }
label_84:
    rdi = *((rsp + 0x30));
    pthread_mutex_destroy ();
label_73:
    free_dfa_content (rbp, rsi, rdx, rcx);
    rax = *((rsp + 8));
    *(rax) = 0;
    *((rax + 8)) = 0;
    eax = *((rsp + 0x4c));
label_92:
    rdx = *((rsp + 0x118));
    rdx -= *(fs:0x28);
    if (rdx != 0) {
        goto label_197;
    }
    return rax;
label_175:
    eax = re_string_realloc_buffers (r12, *((rsp + 0x28)));
    if (eax != 0) {
        goto label_71;
    }
    ecx = *((rsp + 0x10b));
    esi = *((rbp + 0xb4));
    goto label_72;
label_174:
    *((rsp + 0x4c)) = 0xc;
    goto label_73;
    do {
label_74:
        r14 = rbx;
        rbx = *((rbx + 8));
    } while (rbx != 0);
    rbx = *((r14 + 0x10));
    if (rbx != 0) {
        goto label_74;
    }
    goto label_198;
label_75:
    rax = *((r14 + 8));
    rdx = *((rax + 0x18));
    rax = *((rax + 0x38));
    *((r14 + 0x18)) = rdx;
    *((r14 + 0x38)) = rax;
label_76:
    rax = *(r14);
    if (rax == 0) {
        goto label_199;
    }
label_77:
    rbx = *((rax + 0x10));
    if (rbx == 0) {
        goto label_200;
    }
    if (r14 != rbx) {
        goto label_74;
    }
label_200:
    r14 = rax;
label_198:
    if (*((r14 + 0x30)) == 0x10) {
        goto label_75;
    }
    *((r14 + 0x18)) = r14;
    rax = re_dfa_add_node (r15, *((r14 + 0x28)), *((r14 + 0x30)));
    *((r14 + 0x38)) = rax;
    if (rax == -1) {
        goto label_105;
    }
    if (*((r14 + 0x30)) != 0xc) {
        goto label_76;
    }
    ecx = *((r14 + 0x28));
    rax <<= 4;
    rax += *(r15);
    edx = *((rax + 8));
    ecx &= 0x3ff;
    ecx <<= 8;
    edx &= 0xfffc00ff;
    edx |= ecx;
    *((rax + 8)) = edx;
    rax = *(r14);
    if (rax != 0) {
        goto label_77;
    }
label_199:
    r14 = *((r15 + 0x68));
    rax = *((r15 + 0x68));
    while (cl != 0x10) {
        if (rdx != 0) {
            rcx = *((rax + 0x20));
            *((rdx + 0x20)) = rcx;
        }
        rsi = *((rax + 0x10));
        if (rsi != 0) {
            rcx = *((rax + 0x20));
            *((rsi + 0x20)) = rcx;
        }
        if (rdx == 0) {
            goto label_201;
        }
label_78:
        rax = rdx;
        ecx = *((rax + 0x30));
        rdx = *((rax + 8));
        if (cl == 0xb) {
            goto label_202;
        }
    }
    rcx = *((rax + 0x10));
    rsi = *((rcx + 0x18));
    *((rdx + 0x20)) = rsi;
    rax = *((rax + 0x20));
    *((rcx + 0x20)) = rax;
    goto label_78;
label_82:
    r14d = rbx*8;
    r15d = 0;
    while (eax == 0xffffffff) {
        if (edx == 0) {
label_80:
            *((rbp + 0xb0)) |= 8;
        }
label_79:
        r15d++;
        r14d++;
        if (r15d == 0x40) {
            goto label_203;
        }
label_81:
        eax = btowc (r14d);
        edx = r14d;
        edx &= 0xffffff80;
    }
    rsi = *((rbp + 0x78));
    rdi = r12;
    ecx = r15d;
    rdi <<= cl;
    __asm ("out 0x48, eax");
    esi += ebx;
    *(rsi) |= rdi;
    if (edx != 0) {
        goto label_79;
    }
    if (eax != r14d) {
        goto label_80;
    }
    r15d++;
    r14d++;
    if (r15d != 0x40) {
        goto label_81;
    }
label_203:
    rbx += 8;
    if (rbx != 0x20) {
        goto label_82;
    }
    goto label_83;
label_196:
    rax = free (*((rsp + 0x88)));
    goto label_84;
    do {
        rsi = *((rcx + 0x10));
        rdx = rax;
        rax = rcx;
label_201:
        if (rsi != rdx) {
            if (rsi != 0) {
                goto label_204;
            }
        }
        rcx = *(rax);
    } while (rcx != 0);
    *((rsp + 0x10)) = rbp;
    rcx = 0x00016d48;
    *((rsp + 0x18)) = r13;
    *((rsp + 0x28)) = r12;
    do {
        rdx = *((r14 + 0x38));
        if (*((r14 + 0x30)) <= 0x10) {
            eax = *((r14 + 0x30));
            eax = *((rsi + 0x30));
            rax = *((rcx + rax*4));
            rax += rcx;
            /* switch table (17 cases) at 0x16d34 */
            void (*rax)() ();
        }
        rax = *((r14 + 0x20));
        rsi = *((rax + 0x38));
        rax = *((r15 + 0x18));
        *((rax + rdx*8)) = rsi;
        rbx = *((r14 + 8));
label_86:
        if (rbx == 0) {
            goto label_205;
        }
label_100:
        r14 = rbx;
    } while (1);
    do {
        r14 = rdx;
label_205:
        rdx = *((r14 + 0x10));
        al = (rdx == 0) ? 1 : 0;
        sil = (rdx == rbx) ? 1 : 0;
        al |= sil;
        if (al == 0) {
            goto label_206;
        }
        rdx = *(r14);
        rbx = r14;
    } while (rdx != 0);
    rbp = *((rsp + 0x10));
    r12 = *((rsp + 0x28));
    *((rsp + 0x10)) = al;
    ebx = 0;
    r14d = 0;
    while (*((r15 + 0x10)) != r14) {
        r8 = r14 * 3;
        r13 = r8*8;
label_85:
        rax = *((r15 + 0x30));
        rax = *((rax + r13 + 8));
        if (rax == 0) {
            eax = calc_eclosure_iter (r12, r15, r14, 1, r8);
            if (eax != 0) {
                goto label_207;
            }
            rax = *((r15 + 0x30));
            if (*((rax + r13 + 8)) == 0) {
                goto label_208;
            }
        }
        r14++;
    }
    if (bl != 0) {
        *(rax) += eax;
        *(rcx) += dh;
        ebx = 0;
        r13d = 0;
        r14d = 0;
        goto label_85;
        rcx = 0x00016d48;
        *((r12 + 0x10)) = rax;
        al &= 0x10;
        if (al == 0) {
            goto label_209;
        }
        *(rax) = rbx;
        rbx = *((r14 + 8));
        goto label_86;
        *(rax) += al;
        if (*(rax) >= 0) {
            goto label_210;
        }
        *(rax) = r12;
        *((rax + 8)) = r13;
        goto label_86;
label_202:
        *((rdx + 0x20)) = rax;
        goto label_78;
label_208:
        r14++;
        free (*((rsp + 0x70)));
        if (*((r15 + 0x10)) == r14) {
            goto label_211;
        }
        r8 = r14 * 3;
        ebx = *((rsp + 0x10));
        r13 = r8*8;
        goto label_85;
    }
    rax = *((rsp + 8));
    r13 = *((rsp + 0x18));
    *((rsp + 0x10)) = bl;
    if ((*((rax + 0x38)) & 0x10) == 0) {
        if (*((rax + 0x30)) == 0) {
            goto label_212;
        }
        if ((*((r15 + 0xb0)) & 1) != 0) {
            goto label_213;
        }
    }
label_212:
    if (*((r15 + 0x98)) == 0) {
        goto label_214;
    }
label_213:
    rdi <<= 3;
    rax = malloc (r14 + r14*2);
    *((r15 + 0x38)) = rax;
    if (rax == 0) {
        goto label_215;
    }
    ecx = 0;
    if (r14 != 0) {
        goto label_216;
    }
    goto label_214;
    do {
        rax = *((r15 + 0x38));
label_216:
        rdx = rcx * 3;
        xmm0 = 0;
        rcx++;
        rax = rax + rdx*8;
        *((rax + 0x10)) = 0;
        __asm ("movups xmmword [rax], xmm0");
        rax = *((r15 + 0x10));
    } while (rax > rcx);
    if (rax == 0) {
        goto label_214;
    }
    *((rsp + 0x18)) = r13;
    ebx = 0;
    rax = *((r15 + 0x30));
    *((rsp + 0x28)) = r12;
    r12 = rbx;
label_88:
    rbx = r12 * 3;
    r14d = 0;
    rbx <<= 3;
    rdx = rax + rbx;
    r13 = *((rdx + 0x10));
    if (*((rdx + 8)) > 0) {
        goto label_217;
    }
    goto label_218;
    do {
        rax = *((r15 + 0x30));
        r14++;
        if (r14 >= *((rax + rbx + 8))) {
            goto label_218;
        }
label_217:
        rax = *((r13 + r14*8));
        rdx = rax * 3;
        rax = *((r15 + 0x38));
        al = re_node_set_insert_last (rax + rdx*8, r12);
    } while (al != 0);
label_105:
    rax = *((rsp + 8));
    r15 = *(rax);
    eax = 0xc;
label_93:
    *((rsp + 0x4c)) = eax;
    goto label_87;
label_218:
    r12++;
    if (*((r15 + 0x10)) > r12) {
        goto label_88;
    }
    r13 = *((rsp + 0x18));
    r12 = *((rsp + 0x28));
label_214:
    eax = *((rbp + 0xb0));
    *((rsp + 0x4c)) = 0;
    al >>= 2;
    dl = (*((rsp + 0x20)) == 0) ? 1 : 0;
    al &= dl;
    if (al != 0) {
        goto label_219;
    }
label_97:
    rax = *((rbp + 0x68));
    rax = *((rax + 0x18));
    rax = *((rax + 0x38));
    *((rbp + 0x90)) = rax;
    rax = *((rbp + 0x30));
    eax = re_node_set_init_copy (r12, rax + rdx*8, rax + rax*2);
    *((rsp + 0x50)) = eax;
    ebx = eax;
    if (eax != 0) {
        goto label_99;
    }
    if (*((rbp + 0x98)) <= 0) {
        goto label_220;
    }
    r8 = *((rsp + 0x68));
    if (r8 <= 0) {
        goto label_220;
    }
    r9d = 0;
    while (*((rdi + 8)) != 4) {
label_89:
        r9++;
        if (r8 <= r9) {
            goto label_220;
        }
label_91:
        rcx = *((rsp + 0x70));
        rsi = *(rbp);
        r10 = *((rcx + r9*8));
        rdi = *((rcx + r9*8));
        rdi <<= 4;
        rdi += rsi;
    }
    edx = 0;
    goto label_221;
label_90:
    rdx++;
    if (r8 == rdx) {
        goto label_89;
    }
label_221:
    rax = *((rcx + rdx*8));
    rax <<= 4;
    rax += rsi;
    if (*((rax + 8)) != 9) {
        goto label_90;
    }
    r11 = *(rdi);
    if (*(rax) != r11) {
        goto label_90;
    }
    rdx = *((rbp + 0x28));
    rax = r10 * 3;
    rax = rdx + rax*8;
    rax = *((rax + 0x10));
    rsi = *(rax);
    rax = re_node_set_contains (r12);
    if (rax != 0) {
        goto label_89;
    }
    rax = *((rbp + 0x30));
    eax = re_node_set_merge (r12, rax + rdx*8, rsi + rsi*2, rcx);
    if (eax != 0) {
        goto label_222;
    }
    r8 = *((rsp + 0x68));
    r9d = 1;
    if (r8 > r9) {
        goto label_91;
    }
label_220:
    rax = re_acquire_state_context (r13, rbp, r12, 0, r8, r9);
    *((rbp + 0x48)) = rax;
    if (rax == 0) {
        goto label_223;
    }
    if (*((rax + 0x68)) < 0) {
        goto label_224;
    }
    *((rbp + 0x60)) = rax;
    *((rbp + 0x58)) = rax;
    *((rbp + 0x50)) = rax;
label_98:
    free (*((rsp + 0x70)));
label_99:
    rax = *((rsp + 8));
    *((rsp + 0x4c)) = ebx;
    r12 = *(rax);
    rbx = *((r12 + 0x70));
    if (rbx == 0) {
        goto label_225;
    }
    do {
        rdi = rbx;
        rbx = *(rbx);
        free (rdi);
    } while (rbx != 0);
label_225:
    *((r12 + 0x70)) = 0;
    *((r12 + 0x80)) = 0xf;
    *((r12 + 0x68)) = 0;
    free (*((r12 + 0x20)));
    *((r12 + 0x20)) = 0;
    free (*((rsp + 0x90)));
    free (*((rsp + 0x98)));
    if (*((rsp + 0x10b)) != 0) {
        goto label_226;
    }
label_94:
    eax = *((rsp + 0x4c));
    if (eax == 0) {
        goto label_92;
    }
    goto label_84;
label_211:
    ebx = 0;
    r14d = 0;
    r13d = 0;
    goto label_85;
label_207:
    rbx = *((rsp + 8));
    r15 = *(rbx);
    goto label_93;
label_226:
    free (*((rsp + 0x88)));
    goto label_94;
label_178:
    rax = build_wcs_buffer (r12, rsi);
    goto label_95;
label_194:
    rcx = *((rdx + 0x28));
    *((rax + 8)) = r8;
    if (r8 != 0) {
        *(r8) = rax;
    }
    rdx = *((rax + 0x28));
    rdx = *((rdi + rdx*8));
    *((rdi + rcx*8)) = rdx;
    rdx = r8;
    if (rcx > 0x3f) {
        goto label_96;
    }
    r11 = r9;
    r11 <<= cl;
    rcx = r11;
    rcx = ~rcx;
    *((r15 + 0xa0)) &= rcx;
    goto label_96;
label_219:
    rbx = *((rsp + 8));
    if (*((rbx + 0x28)) != 0) {
        goto label_97;
    }
    r10 = *((rbp + 0x10));
    if (r10 == 0) {
        goto label_227;
    }
    rdi = *(rbp);
    r8d = 0;
    ecx = 0;
    r9 = 0x00016d8c;
    r11 = 0x1000000010001;
    rdx = rdi;
label_102:
    if (*((rdx + 8)) > 0xc) {
        void (*0x295d)() ();
    }
    esi = *((rdx + 8));
    rsi = *((r9 + rsi*4));
    rsi += r9;
    /* switch table (13 cases) at 0x16d8c */
    void (*rsi)() ();
label_224:
    rax = re_acquire_state_context (r13, rbp, r12, 1, r8, r9);
    *((rbp + 0x50)) = rax;
    rax = re_acquire_state_context (r13, rbp, r12, 2, r8, r9);
    *((rbp + 0x58)) = rax;
    rax = re_acquire_state_context (r13, rbp, r12, 6, r8, r9);
    *((rbp + 0x60)) = rax;
    if (*((rbp + 0x50)) == 0) {
        goto label_223;
    }
    if (*((rbp + 0x58)) >= 0) {
        goto label_228;
    }
    *((rcx + rcx + 0x48)) += dh;
    if (*((rbp + 0x58)) == 0) {
        *(rdi) = rotate_right8 (*(rdi), 0x85);
    }
    goto label_223;
    if (rax != 0) {
        goto label_98;
    }
label_223:
    ebx = *((rsp + 0x50));
    goto label_99;
label_206:
    rbx = rdx;
    goto label_100;
label_204:
    rdx = rsi;
    goto label_78;
label_210:
    *(rax) = r13;
    *((rax + 8)) = r12;
    goto label_86;
label_228:
    __asm ("stc");
label_177:
    rdi = r12;
    __asm ("out 0xe8, eax");
    if (rax > 0) {
        goto label_95;
        __asm ("int3");
        __asm ("out dx, eax");
    }
label_181:
    *((rsp + 0x4c)) = 0xc;
    *((rbp + 0x68)) = 0;
    goto label_87;
label_180:
    if (rax == 0) {
        goto label_229;
    }
    r12 = rsp + 0x60;
    xmm0 = 0;
    edx = 0;
    esi = 0;
    rcx = r12;
    rdi = r14;
    *((rsp + 0x60)) = xmm0;
    *((rsp + 0x68)) = 2;
    rax = create_token_tree ();
    rbx = rax;
    goto label_101;
    __asm ("cmovs r8d, eax");
label_104:
    rcx++;
    rdx += 0x10;
    if (rcx != r10) {
        goto label_102;
    }
    rax = rcx;
    rax <<= 4;
    rax += rdi;
    r8b |= *((rsp + 0x10));
    if (r8b != 0) {
        goto label_230;
    }
    goto label_231;
    do {
        if (dl == 5) {
            *((rdi + 8)) = 7;
        }
label_103:
        rdi += 0x10;
        if (rax == rdi) {
            goto label_232;
        }
label_230:
        edx = *((rdi + 8));
    } while (dl != 1);
    if (*(rdi) >= 0) {
        goto label_103;
    }
    *((rdi + 0xa)) &= 0xdf;
    goto label_103;
    rsi = *(rdx);
    if (*((rsi + 0x10)) != 0) {
        goto label_97;
    }
    if (*((rsi + 0x18)) == 0) {
        goto label_104;
    }
    goto label_97;
    esi = *(rdx);
    if (esi > 0x40) {
        goto label_233;
    }
    if (esi <= 0xf) {
        goto label_97;
    }
    esi -= 0x10;
    if (((r11 >> rsi) & 1) >= 0) {
        goto label_97;
    }
    goto label_104;
    *((rsp + 0x10)) = al;
    goto label_104;
label_209:
    *((r12 + 8)) = 0;
    rbp = *((rsp + 0x10));
    *(r12) = 0;
    goto label_105;
label_222:
    ebx = eax;
    goto label_99;
label_233:
    if (esi == 0x80) {
        goto label_104;
    }
    goto label_97;
label_179:
    rcx = rax;
    goto label_106;
label_215:
    rax = *((rsp + 8));
    *((rsp + 0x4c)) = 0xc;
    r15 = *(rax);
    goto label_87;
label_229:
    rax = *((rsp + 8));
    *((rbp + 0x68)) = 0;
    r15 = *(rax);
    goto label_87;
label_232:
    r8d = *((rsp + 0x10));
    do {
label_231:
        edx = *((rbp + 0xb0));
        *((rbp + 0xb4)) = 1;
        al = (*((rbp + 0x98)) > 0) ? 1 : 0;
        eax |= r8d;
        edx &= 0xfffffff9;
        eax += eax;
        eax &= 6;
        eax |= edx;
        *((rbp + 0xb0)) = al;
        goto label_97;
label_227:
        r8d = 0;
    } while (1);
label_197:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x12430 */
 
int64_t dbg_parse_reg_exp (int64_t arg_48h, int64_t arg1, int64_t arg2, int64_t arg3, uint32_t arg4, uint32_t arg5, int64_t arg6) {
    re_token_t t;
    uint32_t var_8h;
    int64_t var_10h;
    uint32_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_48h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* bin_tree_t * parse_reg_exp(re_string_t * regexp,regex_t * preg,re_token_t * token,reg_syntax_t syntax,Idx nest,reg_errcode_t * err); */
    r13 = r9;
    rbx = rdx;
    r12 = *(rsi);
    *((rsp + 0x28)) = rsi;
    *((rsp + 0x18)) = rcx;
    *((rsp + 8)) = r8;
    rax = *(fs:0x28);
    *((rsp + 0x48)) = rax;
    eax = 0;
    rax = *((r12 + 0xa8));
    *((rsp + 0x20)) = rax;
    rax = parse_branch (rdi, rsi, rdx, rcx, r8, r9);
    edx = *(r13);
    r15 = rax;
    if (edx != 0) {
        if (rax == 0) {
            goto label_4;
        }
    }
    r14 = *((rsp + 0x18));
    r14 |= 0x800000;
    rax = r14;
    r14 = r15;
    r15 = rax;
    while (dl != 2) {
        if (al == 9) {
            if (*((rsp + 8)) != 0) {
                goto label_5;
            }
        }
        rax = *((r12 + 0xa8));
        *((rsp + 0x10)) = rax;
        rax = *((rsp + 0x20));
        *((r12 + 0xa8)) = rax;
        rax = parse_branch (rbp, *((rsp + 0x28)), rbx, *((rsp + 0x18)), *((rsp + 8)), r13);
        rdx = rax;
        eax = *(r13);
        if (eax != 0) {
            if (rdx == 0) {
                goto label_6;
            }
        }
        rax = *((rsp + 0x10));
        *((r12 + 0xa8)) |= rax;
label_0:
        xmm0 = 0;
        rsi = r14;
        rcx = rsp + 0x30;
        rdi = r12;
        *((rsp + 0x30)) = xmm0;
        *((rsp + 0x38)) = 0xa;
        rax = create_token_tree ();
        r14 = rax;
        if (rax == 0) {
            goto label_7;
        }
        if (*((rbx + 8)) != 0xa) {
            goto label_8;
        }
        rax = peek_token (rbx, rbp, r15, rcx, r8);
        rax = (int64_t) eax;
        *((rbp + 0x48)) += rax;
        eax = *((rbx + 8));
        edx = eax;
        edx &= 0xfffffff7;
    }
label_5:
    edx = 0;
    goto label_0;
label_7:
    *(r13) = 0xc;
    r15 = rax;
    do {
label_3:
        rax = *((rsp + 0x48));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_9;
        }
        rax = r15;
        return rax;
label_8:
        r15 = r14;
    } while (1);
label_6:
    r15 = r14;
    if (r14 == 0) {
        goto label_4;
    }
    do {
label_1:
        rbx = r15;
        r15 = *((r15 + 8));
    } while (r15 != 0);
    r15 = *((rbx + 0x10));
    if (r15 != 0) {
        goto label_1;
    }
    goto label_10;
label_2:
    rax = *(rbx);
    if (rax == 0) {
        goto label_4;
    }
    r15 = *((rax + 0x10));
    if (rbx == r15) {
        goto label_11;
    }
    if (r15 != 0) {
        goto label_1;
    }
label_11:
    rbx = rax;
label_10:
    eax = free_tree (0, rbx, rdx, rcx);
    if (eax == 0) {
        goto label_2;
    }
label_4:
    r15d = 0;
    goto label_3;
label_9:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x121d0 */
 
int64_t dbg_parse_branch (int64_t arg1, int64_t arg2, int64_t arg3, uint32_t arg4, uint32_t arg5, int64_t arg6) {
    re_token_t t;
    int64_t var_8h;
    int64_t var_10h;
    uint32_t var_1fh;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_48h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* bin_tree_t * parse_branch(re_string_t * regexp,regex_t * preg,re_token_t * token,reg_syntax_t syntax,Idx nest,reg_errcode_t * err); */
    r14 = r9;
    r13 = rcx;
    r12 = rdx;
    rbx = rsi;
    *((rsp + 8)) = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x48)) = rax;
    rax = *(rsi);
    *((rsp + 0x20)) = rax;
    rax = parse_expression (rdi, rsi, rdx, rcx, r8, r9);
    edx = *(r14);
    r15 = rax;
    if (edx != 0) {
        if (rax == 0) {
            goto label_7;
        }
    }
    eax = *((r12 + 8));
    edx = eax;
    edx &= 0xfffffff7;
    if (dl == 2) {
        goto label_3;
    }
    rcx = rsp + 0x30;
    *((rsp + 0x28)) = rcx;
    rcx = r14;
    r14 = r15;
    r15 = r12;
    rsp + 0x1f = (rbp == 0) ? 1 : 0;
    r12 = rbp;
    rbx = rcx;
    while (r14 != 0) {
        if (rdx == 0) {
            goto label_8;
        }
        rcx = *((rsp + 0x28));
        rdi = *((rsp + 0x20));
        xmm0 = 0;
        rsi = r14;
        *((rsp + 0x10)) = rdx;
        *((rsp + 0x30)) = xmm0;
        *((rsp + 0x38)) = 0x10;
        rax = create_token_tree ();
        rdx = *((rsp + 0x10));
        if (rax == 0) {
            goto label_9;
        }
        r14 = rax;
        eax = *((r15 + 8));
        edx = eax;
        edx &= 0xfffffff7;
        if (dl == 2) {
            goto label_10;
        }
label_0:
        if (al == 9) {
            if (*((rsp + 0x1f)) == 0) {
                goto label_10;
            }
        }
        rax = parse_expression (*((rsp + 8)), rbp, r15, r13, r12, rbx);
        rdx = rax;
        eax = *(rbx);
        if (eax != 0) {
            if (rdx == 0) {
                goto label_11;
            }
        }
    }
label_8:
    eax = *((r15 + 8));
    if (r14 == 0) {
        r14 = rdx;
    }
    edx = eax;
    edx &= 0xfffffff7;
    if (dl != 2) {
        goto label_0;
    }
label_10:
    r15 = r14;
label_3:
    rax = *((rsp + 0x48));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_12;
    }
    rax = r15;
    return rax;
label_11:
    r15 = r14;
    if (r14 == 0) {
        goto label_7;
    }
    do {
label_1:
        rbx = r15;
        r15 = *((r15 + 8));
    } while (r15 != 0);
    r15 = *((rbx + 0x10));
    if (r15 != 0) {
        goto label_1;
    }
    goto label_13;
label_2:
    rax = *(rbx);
    if (rax == 0) {
        goto label_7;
    }
    r15 = *((rax + 0x10));
    if (r15 == 0) {
        goto label_14;
    }
    if (rbx != r15) {
        goto label_1;
    }
label_14:
    rbx = rax;
label_13:
    eax = free_tree (0, rbx, rdx, rcx);
    if (eax == 0) {
        goto label_2;
    }
label_7:
    r15d = 0;
    goto label_3;
label_9:
    r15 = r14;
    r14 = rbx;
    do {
label_4:
        rbx = rdx;
        rdx = *((rdx + 8));
    } while (rdx != 0);
    rdx = *((rbx + 0x10));
    if (rdx != 0) {
        goto label_4;
    }
    goto label_15;
label_5:
    rax = *(rbx);
    if (rax == 0) {
        goto label_6;
    }
    rdx = *((rax + 0x10));
    if (rdx == 0) {
        goto label_16;
    }
    if (rbx != rdx) {
        goto label_4;
    }
label_16:
    rbx = rax;
label_15:
    eax = free_tree (0, rbx, rdx, rcx);
    if (eax == 0) {
        goto label_5;
    }
    do {
label_6:
        rbx = r15;
        r15 = *((r15 + 8));
    } while (r15 != 0);
    r15 = *((rbx + 0x10));
    if (r15 != 0) {
        goto label_6;
    }
    while (eax == 0) {
        rax = *(rbx);
        if (rax == 0) {
            goto label_17;
        }
        rdx = *((rax + 0x10));
        if (rbx != rdx) {
            if (rdx != 0) {
                goto label_18;
            }
        }
        rbx = rax;
        eax = free_tree (0, rbx, rdx, rcx);
    }
label_17:
    *(r14) = 0xc;
    goto label_3;
label_18:
    r15 = rdx;
    goto label_6;
label_12:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x12610 */
 
int64_t re_compile_internal (int64_t arg_8h, int64_t arg_10h, int64_t arg_28h, int64_t arg_30h, void * arg_40h, int64_t arg_48h, int64_t arg_50h, int64_t arg_58h, int64_t arg_60h, int64_t arg_68h, void * arg_78h, int64_t arg_80h, int64_t arg_88h, int64_t arg_90h, int64_t arg_98h, int64_t arg_b0h, uint32_t arg_b4h, int64_t arg_e8h, int64_t arg_108h, uint32_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    uint32_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_10h_2;
    int64_t var_28h;
    int64_t var_20h;
    int64_t var_28h_2;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_4ch;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_4ch_2;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_7ah;
    int64_t var_7bh;
    int64_t var_7ch;
    int64_t var_80h;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_a0h;
    int64_t var_a8h;
    int64_t var_b0h;
    int64_t var_b8h;
    int64_t var_c0h;
    int64_t var_c8h;
    int64_t var_d0h;
    int64_t var_d8h;
    int64_t var_e0h;
    int64_t var_e8h;
    int64_t var_f8h;
    int64_t var_118h;
    int64_t var_109h;
    int64_t var_10ah;
    int64_t var_10bh;
    int64_t var_110h;
    int64_t var_118h_2;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    rbp = *(rdi);
    *((rsp + 8)) = rdi;
    *((rsp + 0x10)) = rsi;
    *((rsp + 0x18)) = rcx;
    rax = *(fs:0x28);
    *((rsp + 0x118)) = rax;
    eax = 0;
    *((rdi + 0x38)) &= 0x90;
    *((rsp + 0x4c)) = 0;
    *((rdi + 0x18)) = rcx;
    *((rdi + 0x10)) = 0;
    *((rdi + 0x30)) = 0;
    if (*((rdi + 8)) <= 0x10f) {
        goto label_2;
    }
label_0:
    rdi = rbp + 8;
    rcx = rbp;
    rax = *((rsp + 8));
    rdi &= 0xfffffffffffffff8;
    rcx -= rdi;
    *((rax + 0x10)) = 0x110;
    eax = 0;
    ecx += 0x110;
    *(rbp) = 0;
    *((rbp + 0x108)) = 0;
    ecx >>= 3;
    do {
        *(rdi) = rax;
        rcx--;
        rdi += 8;
    } while (rcx != 0);
    rax = 0x555555555555554;
    *((rbp + 0x80)) = 0xf;
    if (r13 > rax) {
        void (*0x12dff)() ();
    }
    rax = r13 + 1;
    *((rbp + 8)) = rax;
    rdi = rax;
    rdi <<= 4;
    *((rsp + 0x28)) = rax;
    rax = malloc (rdi);
    *(rbp) = rax;
    if (r13 == 0) {
        goto label_3;
    }
    esi = 1;
    do {
        rsi += rsi;
    } while (r13 >= rsi);
    rbx = rsi - 1;
label_1:
    rax = calloc (0x18, rsi);
    *((rbp + 0x88)) = rbx;
    *((rbp + 0x40)) = rax;
    eax = ctype_get_mb_cur_max ();
    *((rbp + 0xb4)) = eax;
    rax = nl_langinfo (0xe);
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_4;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_4;
        }
        edx = 0;
        dl = (*((rax + 3)) == 0x2d) ? 1 : 0;
        rax = rax + rdx + 3;
        if (*(rax) != 0x38) {
            goto label_4;
        }
        if (*((rax + 1)) != 0) {
            goto label_4;
        }
        *((rbp + 0xb0)) |= 4;
    }
label_4:
    eax = *((rbp + 0xb0));
    edx = eax;
    edx &= 0xfffffff7;
    *((rbp + 0xb0)) = dl;
    if (*((rbp + 0xb4)) <= 1) {
        void (*0x127ae)() ();
    }
    if ((al & 4) != 0) {
        rax = obj_utf8_sb_map;
        *((rbp + 0x78)) = rax;
    }
    rax = calloc (0x20, 1);
    *((rbp + 0x78)) = rax;
    if (rax == 0) {
        void (*0x12dff)() ();
    }
    ebx = 0;
    r12d = 1;
label_2:
    rax = realloc (rbp, 0x110);
    if (rax != 0) {
        __asm ("int3");
        *(rax) += eax;
        rax = *((rsp + 8));
        *((rax + 8)) = 0x110;
        *(rax) = rbp;
        goto label_0;
label_3:
        ebx = 0;
        esi = 1;
        goto label_1;
        __asm ("out dx, eax");
    }
    eax = 0xc;
    return void (*0x12dae)() ();
}

/* /tmp/tmpvkjt68yd @ 0x295d */
 
void re_compile_internal_cold (void) {
    return abort ();
}

/* /tmp/tmpvkjt68yd @ 0x13a60 */
 
int64_t dbg_re_search_stub (void ** arg_70h, int64_t arg_78h, int64_t arg1, void * arg2, signed int64_t arg3, signed int64_t arg4, int64_t arg5, size_t * arg6) {
    _Bool ret_len;
    void * var_8h;
    size_t * size;
    signed int64_t var_18h;
    int64_t var_20h;
    uint32_t var_28h;
    int64_t var_2ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* regoff_t re_search_stub(re_pattern_buffer * bufp,char const * string,Idx length,Idx start,regoff_t range,Idx stop,re_registers * regs,_Bool ret_len); */
    r15 = rcx + r8;
    r12 = *(rdi);
    ebx = *((rsp + 0x78));
    *((rsp + 8)) = rsi;
    *((rsp + 0x10)) = r9;
    *((rsp + 0x28)) = ebx;
    if (rcx < 0) {
        goto label_12;
    }
    r13 = rdx;
    if (rcx > rdx) {
        goto label_12;
    }
    r14 = rdi;
    if (r15 > rdx) {
        goto label_13;
    }
    rax = r8;
    if (r8 >= 0) {
        if (rcx > r15) {
            goto label_13;
        }
    }
    if (r15 < 0) {
        goto label_14;
    }
    if (rax < 0) {
        if (rcx <= r15) {
            goto label_14;
        }
    }
label_6:
    r12 += 0xe8;
    *((rsp + 0x18)) = rcx;
    rdi = r12;
    pthread_mutex_lock ();
    eax = *((r14 + 0x38));
    rcx = *((rsp + 0x18));
    esi = eax;
    sil >>= 5;
    ebx = esi;
    ebx &= 3;
    *((rsp + 0x2c)) = ebx;
    if (r15 > rcx) {
        if (*((r14 + 0x20)) == 0) {
            goto label_5;
        }
        if ((al & 8) == 0) {
            goto label_15;
        }
    }
label_5:
    if ((al & 0x10) != 0) {
        goto label_16;
    }
    if (*((rsp + 0x70)) == 0) {
        goto label_16;
    }
    eax &= 6;
    rdi = *((r14 + 0x30));
    if (al == 4) {
        goto label_17;
    }
label_2:
    rsi = rdi + 1;
    rbx = rsi;
label_3:
    rdi = rsi;
    rdi <<= 4;
label_4:
    *((rsp + 0x20)) = rcx;
    *((rsp + 0x18)) = rsi;
    rax = malloc (rdi);
    if (rax == 0) {
        goto label_18;
    }
    eax = *((rsp + 0x34));
    rsi = *((rsp + 0x30));
    eax = re_search_internal (r14, *((rsp + 0x28)), r13, *((rsp + 0x40)), r15, *((rsp + 0x30)));
    if (eax != 0) {
        r13 = 0xffffffffffffffff;
        if (eax != 1) {
label_0:
            r13 = 0xfffffffffffffffe;
        }
label_1:
        free (rbp);
label_9:
        rdi = r12;
        pthread_mutex_unlock ();
label_8:
        rax = r13;
        return rax;
    }
    if (*((rsp + 0x70)) == 0) {
        goto label_19;
    }
    eax = *((r14 + 0x38));
    r15 = rbx + 1;
    al >>= 1;
    eax &= 3;
    if (eax == 0) {
        goto label_20;
    }
    r13d = 2;
    if (eax == 1) {
        goto label_21;
    }
label_7:
    if (rbx <= 0) {
        goto label_22;
    }
    rax = *((rsp + 0x70));
    rdx = rbx;
    rsi = rbx*8;
    r8 = *((rax + 8));
    rdi = *((rax + 0x10));
    eax = 0;
    do {
        rcx = *((rbp + rax*2));
        *((r8 + rax)) = rcx;
        rcx = *((rbp + rax*2 + 8));
        *((rdi + rax)) = rcx;
        rax += 8;
    } while (rax != rsi);
label_10:
    rax = *((rsp + 0x70));
    if (*(rax) <= rdx) {
        goto label_11;
    }
    rcx = *((rax + 0x10));
    rax = *((rax + 8));
    do {
        rsi = *((rsp + 0x70));
        rbx++;
        *((rcx + rdx*8)) = 0xffffffffffffffff;
        *((rax + rdx*8)) = 0xffffffffffffffff;
        rdx = rbx;
    } while (*(rsi) > rbx);
label_11:
    eax = r13 + r13;
    r13d = *((r14 + 0x38));
    r13d &= 0xfffffff9;
    r13d |= eax;
    *((r14 + 0x38)) = r13b;
    r13d &= 6;
    if (r13d == 0) {
        goto label_0;
    }
label_19:
    r13 = *(rbp);
    if (*((rsp + 0x28)) == 0) {
        goto label_1;
    }
    rax = *((rbp + 8));
    rax -= r13;
    r13 = rax;
    goto label_1;
label_17:
    rax = *((rsp + 0x70));
    rsi = *(rax);
    if (rsi > rdi) {
        goto label_2;
    }
    rbx = rsi;
    if (rsi > 0) {
        goto label_3;
    }
label_16:
    edi = 0x10;
    esi = 1;
    ebx = 1;
    *((rsp + 0x70)) = 0;
    goto label_4;
label_15:
    rpl_re_compile_fastmap (r14, rsi, rdx, rcx, r8);
    eax = *((r14 + 0x38));
    rcx = *((rsp + 0x18));
    goto label_5;
label_13:
    r15 = r13;
    goto label_6;
label_20:
    *((rsp + 8)) = rdi;
    rax = malloc (r15*8);
    rdi = *((rsp + 8));
    r13 = rax;
    rax = *((rsp + 0x70));
    *((rax + 8)) = r13;
    if (r13 == 0) {
        goto label_23;
    }
    rax = malloc (rdi);
    rdx = *((rsp + 0x70));
    *((rdx + 0x10)) = rax;
    if (rax == 0) {
        goto label_24;
    }
    rax = *((rsp + 0x70));
    r13d = 1;
    *(rax) = r15;
    goto label_7;
label_14:
    r12 += 0xe8;
    *((rsp + 0x18)) = rcx;
    r15d = 0;
    rdi = r12;
    pthread_mutex_lock ();
    eax = *((r14 + 0x38));
    rcx = *((rsp + 0x18));
    esi = eax;
    sil >>= 5;
    edx = esi;
    edx &= 3;
    *((rsp + 0x2c)) = edx;
    goto label_5;
label_12:
    r13 = 0xffffffffffffffff;
    goto label_8;
label_18:
    r13 = 0xfffffffffffffffe;
    goto label_9;
label_21:
    rax = *((rsp + 0x70));
    r13d = 1;
    if (*(rax) >= r15) {
        goto label_7;
    }
    *((rsp + 0x10)) = rsi;
    rax = realloc (*((rax + 8)), r15*8);
    *((rsp + 8)) = rax;
    if (rax != 0) {
        rax = *((rsp + 0x70));
        rax = realloc (*((rax + 0x10)), *((rsp + 0x10)));
        r8 = *((rsp + 8));
        if (rax == 0) {
            goto label_25;
        }
        rdx = *((rsp + 0x70));
        *((rdx + 8)) = r8;
        *((rdx + 0x10)) = rax;
        *(rdx) = r15;
        goto label_7;
label_22:
        edx = 0;
        ebx = 0;
        goto label_10;
label_24:
        free (r13);
    }
label_23:
    r13d = 0;
    goto label_11;
label_25:
    r13d = 0;
    free (r8);
    goto label_11;
}

/* /tmp/tmpvkjt68yd @ 0x2962 */
 
void rpl_regerror_cold (void) {
    return abort ();
}

/* /tmp/tmpvkjt68yd @ 0x3160 */
 
void dbg_output (int64_t arg1, uint32_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void output(char const * start,char const * past_end); */
    rsi -= rdi;
    ebx = _init;
    rdx = *(0x0001e0e0);
    rbx -= rdx;
    if (rdi == 0) {
        goto label_1;
    }
    r12 = rsi;
    r13 = 0x0001c0e0;
    if (rsi < rbx) {
        goto label_2;
    }
    do {
        r12 -= rbx;
        memcpy (r13 + rdx, rbp, rbx);
        edx = _init;
        rdi = r13;
        rbp += rbx;
        rcx = stdout;
        esi = 1;
        ebx = _init;
        fwrite_unlocked ();
        edx = 0;
        *(0x0001e0e0) = 0;
    } while (r12 > 0x1fff);
    rbx = r12;
label_0:
    memcpy (0x0001c0e0, rbp, r12);
    do {
        *(0x0001e0e0) = rbx;
        return;
label_1:
        rcx = stdout;
        esi = 1;
        rdi = 0x0001c0e0;
        ebx = 0;
        fwrite_unlocked ();
    } while (1);
label_2:
    rdi = rdx + r13;
    rbx = rdx + rsi;
    goto label_0;
}

/* /tmp/tmpvkjt68yd @ 0x27a0 */
 
void fwrite_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fwrite_unlocked]");
}

/* /tmp/tmpvkjt68yd @ 0x3250 */
 
int64_t dbg_tac_seekable (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_10h_2;
    int64_t var_18h_2;
    int64_t var_20h_2;
    int64_t var_28h_2;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* _Bool tac_seekable(int input_fd,char const * file,off_t file_pos); */
    rax = rdx;
    r12 = read_size;
    r15 = separator;
    *(rsp) = rdx;
    edx = 0;
    rcx = match_length;
    rax = rdx:rax / r12;
    rdx = rdx:rax % r12;
    ebx = *(r15);
    *((rsp + 0xc)) = edi;
    *((rsp + 0x20)) = rsi;
    *((rsp + 0x10)) = rcx;
    r15++;
    r13 = rcx - 1;
    if (rdx != 0) {
        goto label_12;
    }
label_11:
    *((rsp + 0x28)) = r13;
    ebp = *((rsp + 0xc));
    r13 = r12;
    r14 = "%s: seek failed";
    *((rsp + 0x18)) = r15;
    r12 = *(rsp);
    while (rax >= 0) {
label_0:
        r12 -= r13;
        rax = safe_read (ebp, *(obj.G_buffer), r13);
        if (rax != 0) {
            goto label_13;
        }
        r13 = read_size;
        if (r12 == 0) {
            goto label_14;
        }
        rsi = r13;
        edx = 1;
        edi = ebp;
        rsi = -rsi;
        rax = lseek ();
    }
    rdx = *((rsp + 0x20));
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    edx = 5;
    r15 = rax;
    rax = dcgettext (0, r14);
    r13 = rax;
    rax = errno_location ();
    rcx = r15;
    eax = 0;
    rax = error (0, *(rax), r13);
    r13 = read_size;
    goto label_0;
label_13:
    *(rsp) = r12;
    rcx = r12;
    r15 = *((rsp + 0x18));
    r12 = read_size;
    r13 = *((rsp + 0x28));
    if (rax != r12) {
        goto label_15;
    }
    rax = r12;
    r14d = *((rsp + 0xc));
    r12 = rcx;
    while (rax != 0) {
        if (rax == -1) {
            goto label_16;
        }
        r12 += rax;
        if (*(obj.read_size) != rax) {
            goto label_17;
        }
label_10:
        rdx = rax;
        rax = safe_read (r14d, *(obj.G_buffer), rdx);
    }
    *(rsp) = r12;
label_15:
    if (rbp == -1) {
        goto label_16;
    }
label_9:
    r12 = G_buffer;
    rax = sentinel_length;
    *((rsp + 0x18)) = 1;
    r14 = r15;
    rbp += r12;
    rcx = rbp;
    rcx -= *((rsp + 0x10));
    rdx = rbp;
    rcx++;
    if (rax != 0) {
        rdx = rcx;
    }
    rcx = r12;
    r15 = r12;
    r12 = rdx;
label_5:
    if (rax != 0) {
        goto label_2;
    }
    do {
label_4:
        r12 -= r15;
        r8d = 1;
        r8 -= r12;
        if (r8 > 1) {
            goto label_18;
        }
        if (r8 == 1) {
            goto label_19;
        }
        rcx = r12 - 1;
        r9 = obj_regs;
        rdx = r12;
        rax = rpl_re_search (obj.compiled_separator, r15);
        if (rax == -1) {
            goto label_20;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_21;
        }
        rax = Scrt1.o;
        r15 = G_buffer;
        rdx = *(rax);
        rax = Scrt1.o;
        rax = *(rax);
        r12 = r15 + rdx;
        rax -= rdx;
        *(obj.match_length) = rax;
label_1:
        if (r12 >= r15) {
            goto label_22;
        }
        if (*(rsp) == 0) {
            goto label_23;
        }
        rax = read_size;
        rbp -= r15;
        if (rax < rbp) {
            rcx = sentinel_length;
            edx = 1;
            rsi = rax + rax;
            rdi = G_buffer_size;
            *(obj.read_size) = rsi;
            rsi = rcx + rax*4 + 2;
            if (rcx != 0) {
                rdx = rcx;
            }
            *(obj.G_buffer_size) = rsi;
            r12 = rdx;
            if (rsi < rdi) {
                goto label_24;
            }
            rdi = r15;
            rdi -= rdx;
            rax = xrealloc ();
            rax += r12;
            *(obj.G_buffer) = rax;
            rax = read_size;
        }
        rcx = *(rsp);
        if (rcx < rax) {
            goto label_25;
        }
        rcx -= rax;
        *(rsp) = rcx;
label_6:
        rsi = *(rsp);
        edi = *((rsp + 0xc));
        edx = 0;
        rax = lseek ();
        if (rax < 0) {
            goto label_26;
        }
label_8:
        r15 = G_buffer;
        r8 = read_size;
        r12 = r15 + r8;
        *((rsp + 0x10)) = r8;
        memmove (r12, r15, rbp);
        r8 = *((rsp + 0x10));
        edi = *((rsp + 0xc));
        rsi = r15;
        rbp += r8;
        rdx = r8;
        rbp += r15;
        if (*(obj.sentinel_length) == 0) {
            r12 = rbp;
        }
        rax = safe_read (rdi, rsi, rdx);
        if (rax != *(obj.read_size)) {
            goto label_16;
        }
        rax = sentinel_length;
        r15 = G_buffer;
    } while (rax == 0);
    do {
label_2:
        rdi = r12;
        r12--;
    } while (*(r12) != bl);
label_3:
    if (r13 == 0) {
        goto label_1;
    }
    eax = strncmp (rdi, r14, r13);
    if (eax == 0) {
        goto label_1;
    }
    rdi = r12;
    r12--;
    if (*(r12) != bl) {
        goto label_2;
    }
    goto label_3;
label_22:
    if (*(obj.separator_ends_record) == 0) {
        goto label_27;
    }
    rax = match_length;
    esi = *((rsp + 0x18));
    rax += r12;
    esi ^= 1;
    dl = (rbp != rax) ? 1 : 0;
    sil |= dl;
    *((rsp + 0x18)) = sil;
    if (sil != 0) {
        goto label_28;
    }
label_7:
    rax = sentinel_length;
    if (rax == 0) {
        goto label_4;
    }
    edx = 1;
    rdx -= *(obj.match_length);
    r12 += rdx;
    goto label_5;
label_20:
    r15 = G_buffer;
label_19:
    r12 = r15 - 1;
    goto label_1;
label_25:
    rax = *(rsp);
    *(rsp) = 0;
    *(obj.read_size) = rax;
    goto label_6;
label_27:
    rdi = r12;
    output (rdi, rbp);
    r15 = G_buffer;
    goto label_7;
label_26:
    rdx = *((rsp + 0x20));
    edi = 0;
    esi = 3;
    rax = quotearg_n_style_colon ();
    edx = 5;
    r15 = rax;
    rax = dcgettext (0, "%s: seek failed");
    r12 = rax;
    rax = errno_location ();
    rcx = r15;
    eax = 0;
    rax = error (0, *(rax), r12);
    goto label_8;
label_28:
    rdi = rax;
    *((rsp + 0x10)) = rax;
    output (rdi, rbp);
    *((rsp + 0x18)) = 0;
    rbp = *((rsp + 0x10));
    r15 = G_buffer;
    goto label_7;
label_16:
    rdx = *((rsp + 0x20));
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "%s: read error");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    eax = error (0, *(rax), r12);
    eax = 0;
    return rax;
label_14:
    *(rsp) = r12;
    r12 = r13;
    r15 = *((rsp + 0x18));
    r13 = *((rsp + 0x28));
    if (r12 != 0) {
        goto label_9;
    }
    rax = r12;
    r14d = *((rsp + 0xc));
    r12 = *(rsp);
    goto label_10;
label_23:
    output (r15, rbp);
    eax = 1;
    return rax;
label_12:
    rax = *(rsp);
    edx = 0;
    rsi = rax;
    rax = lseek ();
    if (rax >= 0) {
        goto label_11;
    }
    rdx = *((rsp + 0x20));
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    edx = 5;
    r14 = rax;
    rax = dcgettext (0, "%s: seek failed");
    r12 = rax;
    rax = errno_location ();
    rcx = r14;
    eax = 0;
    rax = error (0, *(rax), r12);
    r12 = read_size;
    goto label_11;
label_17:
    *(rsp) = r12;
    goto label_9;
label_18:
    edx = 5;
    rax = dcgettext (0, "record too large");
    eax = 0;
    error (1, 0, rax);
label_24:
    xalloc_die ();
label_21:
    edx = 5;
    rax = dcgettext (0, "error in regular expression search");
    eax = 0;
    rax = error (1, 0, rax);
}

/* /tmp/tmpvkjt68yd @ 0x30a0 */
 
uint64_t deregister_tm_clones (void) {
    rdi = obj___progname;
    rax = obj___progname;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x30d0 */
 
int64_t register_tm_clones (void) {
    rdi = obj___progname;
    rsi = obj___progname;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x3110 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_000024a0 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmpvkjt68yd @ 0x24a0 */
 
void fcn_000024a0 (void) {
    /* [14] -r-x section size 16 named .plt.got */
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmpvkjt68yd @ 0x3150 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmpvkjt68yd @ 0x15980 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmpvkjt68yd @ 0x5950 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpvkjt68yd @ 0x15230 */
 
uint64_t dbg_base_len (int64_t arg1) {
    rdi = arg1;
    /* size_t base_len(char const * name); */
    rbx = rdi;
    rax = strlen (rdi);
    do {
        if (rax > 1) {
            rdx = rax - 1;
            if (*((rbx + rax - 1)) == 0x2f) {
                goto label_0;
            }
        }
        return rax;
label_0:
        rax = rdx;
    } while (1);
}

/* /tmp/tmpvkjt68yd @ 0x5c80 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0001e3d0]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x0001e3e0]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x25f0 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmpvkjt68yd @ 0x143c0 */
 
uint32_t dbg_rpl_re_set_registers (re_pattern_buffer * bufp, regoff_t * ends, __re_size_t num_regs, re_registers * regs, regoff_t * starts) {
    rdi = bufp;
    r8 = ends;
    rdx = num_regs;
    rsi = regs;
    rcx = starts;
    /* void rpl_re_set_registers(re_pattern_buffer * bufp,re_registers * regs,__re_size_t num_regs,regoff_t * starts,regoff_t * ends); */
    if (rdx != 0) {
        eax = *((rdi + 0x38));
        eax &= 0xfffffff9;
        eax |= 2;
        *((rdi + 0x38)) = al;
        *(rsi) = rdx;
        *((rsi + 8)) = rcx;
        *((rsi + 0x10)) = r8;
        return eax;
    }
    *((rdi + 0x38)) &= 0xf9;
    *(rsi) = 0;
    *((rsi + 0x10)) = 0;
    *((rsi + 8)) = 0;
    return eax;
}

/* /tmp/tmpvkjt68yd @ 0x14a50 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x141c0 */
 
void dbg_rpl_re_match_2 (int64_t arg_58h_2, int64_t arg_58h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    re_registers * regs;
    Idx stop;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* regoff_t rpl_re_match_2(re_pattern_buffer * bufp,char const * string1,Idx length1,char const * string2,Idx length2,Idx start,re_registers * regs,Idx stop); */
    rax = *((rsp + 0x58));
    rax |= r8;
    rax |= rdx;
    if (rax < 0) {
        goto label_2;
    }
    r10 = rdx;
    rbx = rdx;
    r12 = r8;
    r10 += r8;
    if (r10 overflow 0) {
        goto label_2;
    }
    r13 = rdi;
    r14 = r9;
    if (r8 == 0) {
        goto label_3;
    }
    *(rsp) = rsi;
    r15d = 0;
    while (1) {
label_1:
        rax = re_search_stub (r13, rbp, r10, r14, 0, *((rsp + 0x68)));
        r12 = rax;
        free (r15);
label_0:
        rax = r12;
        return;
        rdi = r10;
        *((rsp + 8)) = r10;
        rax = malloc (rdi);
        r15 = rax;
        if (rax == 0) {
            goto label_2;
        }
        memcpy (rax, *(rsp), rbx);
        memcpy (r15 + rbx, rbp, r12);
        r10 = *((rsp + 8));
    }
label_2:
    r12 = 0xfffffffffffffffe;
    goto label_0;
label_3:
    r15d = 0;
    goto label_1;
}

/* /tmp/tmpvkjt68yd @ 0x5790 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc (rdi);
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x24f0 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmpvkjt68yd @ 0x14c50 */
 
uint64_t dbg_xcharalloc (size_t size) {
    rdi = size;
    /* char * xcharalloc(size_t n); */
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpvkjt68yd @ 0x15190 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x00016550);
    return abort ();
}

/* /tmp/tmpvkjt68yd @ 0x25c0 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmpvkjt68yd @ 0x2820 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmpvkjt68yd @ 0x2550 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmpvkjt68yd @ 0x56b0 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x15520 */
 
int64_t dbg_rpl_mbrtowc (char ** arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x59a0 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x2930)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x5710 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x3e50 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite ("A NULL argv[0] was passed through an exec system call.\n", 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmpvkjt68yd @ 0x2630 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmpvkjt68yd @ 0x2510 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmpvkjt68yd @ 0x2890 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmpvkjt68yd @ 0x5ee0 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x0001e3d0]");
    __asm ("movdqa xmm2, xmmword [0x0001e3e0]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x2949)() ();
    }
    if (rdx == 0) {
        void (*0x2949)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x14480 */
 
uint32_t dbg_mkstemp_safer (char * template) {
    rdi = template;
    /* int mkstemp_safer(char * templ); */
    eax = mkstemp (rdi);
    edi = eax;
    return void (*0x144c0)() ();
}

/* /tmp/tmpvkjt68yd @ 0x2780 */
 
void mkstemp (void) {
    __asm ("bnd jmp qword [reloc.mkstemp]");
}

/* /tmp/tmpvkjt68yd @ 0x155b0 */
 
int32_t dbg_dup_safer (int64_t arg_80h, int64_t arg1, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* int dup_safer(int fd); */
    edx = 3;
    esi = 0;
    eax = 0;
    return void (*0x155d0)() ();
}

/* /tmp/tmpvkjt68yd @ 0x2680 */
 
void close (void) {
    __asm ("bnd jmp qword [reloc.close]");
}

/* /tmp/tmpvkjt68yd @ 0x5f80 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0001e3d0]");
    __asm ("movdqa xmm2, xmmword [0x0001e3e0]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x294e)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x294e)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x5ac0 */
 
int64_t quotearg_style (uint32_t arg1, int64_t arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x293a)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x5e40 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x2944)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x15994 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmpvkjt68yd @ 0x14cd0 */
 
uint64_t xreallocarray (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpvkjt68yd @ 0x14180 */
 
void dbg_rpl_re_match (int64_t arg3, int64_t arg5) {
    rdx = arg3;
    r8 = arg5;
    /* regoff_t rpl_re_match(re_pattern_buffer * bufp,char const * string,Idx length,Idx start,re_registers * regs); */
    re_search_stub (rdi, rsi, rdx, rcx, 0, rdx);
}

/* /tmp/tmpvkjt68yd @ 0x14dd0 */
 
int64_t dbg_x2realloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x142c0 */
 
void dbg_rpl_re_search_2 (int64_t arg_60h_2, int64_t arg_60h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    regoff_t range;
    re_registers * regs;
    Idx stop;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* regoff_t rpl_re_search_2(re_pattern_buffer * bufp,char const * string1,Idx length1,char const * string2,Idx length2,Idx start,regoff_t range,re_registers * regs,Idx stop); */
    rax = *((rsp + 0x60));
    rax |= r8;
    rax |= rdx;
    if (rax < 0) {
        goto label_2;
    }
    r10 = rdx;
    rbx = rdx;
    r12 = r8;
    r10 += r8;
    if (r10 overflow 0) {
        goto label_2;
    }
    r13 = rdi;
    r14 = r9;
    if (r8 == 0) {
        goto label_3;
    }
    *(rsp) = rsi;
    r15d = 0;
    while (1) {
label_1:
        rax = re_search_stub (r13, rbp, r10, r14, *((rsp + 0x60)), *((rsp + 0x70)));
        r12 = rax;
        free (r15);
label_0:
        rax = r12;
        return;
        rdi = r10;
        *((rsp + 8)) = r10;
        rax = malloc (rdi);
        r15 = rax;
        if (rax == 0) {
            goto label_2;
        }
        memcpy (rax, *(rsp), rbx);
        memcpy (r15 + rbx, rbp, r12);
        r10 = *((rsp + 8));
    }
label_2:
    r12 = 0xfffffffffffffffe;
    goto label_0;
label_3:
    r15d = 0;
    goto label_1;
}

/* /tmp/tmpvkjt68yd @ 0x13e80 */
 
uint64_t dbg_rpl_regcomp (void * ptr, int64_t arg_28h, int64_t arg_38h, int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_regcomp(regex_t * restrict preg,char const * restrict pattern,int cflags); */
    eax = edx;
    r14 = rsi;
    eax &= 1;
    r13 -= r13;
    r13d &= 0xfd4fca;
    ebx = edx;
    *(rdi) = 0;
    r13 += 0x3b2fc;
    *((rdi + 8)) = 0;
    *((rdi + 0x10)) = 0;
    rax = malloc (0x100);
    *((rbp + 0x20)) = rax;
    if (rax == 0) {
        goto label_1;
    }
    r12 = rbx;
    eax = 0;
    r12 <<= 0x15;
    r12d &= 0x400000;
    r12 |= r13;
    while (1) {
        ebx >>= 3;
        eax <<= 7;
        *((rbp + 0x28)) = 0;
        ebx &= 1;
        ebx <<= 4;
        ebx |= eax;
        eax = *((rbp + 0x38));
        eax &= 0x6f;
        ebx |= eax;
        *((rbp + 0x38)) = bl;
        rax = strlen (r14);
        rcx = r12;
        rsi = r14;
        rdi = rbp;
        rdx = rax;
        eax = re_compile_internal ();
        r12d = eax;
        if (eax == 0x10) {
            goto label_2;
        }
        if (eax != 0) {
            goto label_3;
        }
        rpl_re_compile_fastmap (rbp, rsi, rdx, rcx, r8);
label_0:
        eax = r12d;
        return rax;
        r12 &= 0xffffffffffffffbf;
        eax = 1;
        r12 |= 0x100;
    }
label_2:
    r12d = 8;
label_3:
    free (*((rbp + 0x20)));
    *((rbp + 0x20)) = 0;
    goto label_0;
label_1:
    r12d = 0xc;
    eax = r12d;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x13920 */
 
int64_t dbg_rpl_re_compile_pattern (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char const * rpl_re_compile_pattern(char const * pattern,size_t length,re_pattern_buffer * bufp); */
    rcx = rpl_re_syntax_options;
    r8 = rdi;
    rdi = rdx;
    edx = *((rdx + 0x38));
    rax = rcx;
    rax >>= 0x19;
    edx &= 0x6f;
    eax &= 1;
    eax <<= 4;
    eax |= 0xffffff80;
    eax |= edx;
    rdx = rsi;
    rsi = r8;
    *((rdi + 0x38)) = al;
    eax = re_compile_internal ();
    if (eax != 0) {
        rdx = obj___re_error_msgid_idx;
        rax = (int64_t) eax;
        rsi = "Success";
        edi = 0;
        rsi += *((rdx + rax*8));
        edx = 5;
        void (*0x25c0)() ();
    }
    eax = 0;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x14c70 */
 
uint64_t xrealloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmpvkjt68yd @ 0x3cd0 */
 
uint64_t dbg_mfile_name_concat (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_7h;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * mfile_name_concat(char const * dir,char const * base,char ** base_in_result); */
    r12 = rsi;
    *((rsp + 8)) = rdx;
    rax = last_component ();
    r13 = rax;
    rax = base_len (rax);
    r13 -= rbp;
    r14 = r13 + rax;
    rbx = rax;
    rax = strlen (r12);
    r13 = rax;
    if (rbx == 0) {
        goto label_1;
    }
    if (*((rbp + r14 - 1)) == 0x2f) {
        goto label_2;
    }
    ebx = 0;
    r15d = 0;
    eax = 0x2f;
    if (*(r12) == 0x2f) {
        eax = r15d;
    }
    bl = (*(r12) != 0x2f) ? 1 : 0;
    *((rsp + 7)) = al;
    do {
label_0:
        rdi += rbx;
        rax = malloc (r14 + r13 + 1);
        r15 = rax;
        if (rax != 0) {
            rdi = rax;
            rdx = r14;
            rsi = rbp;
            mempcpy ();
            ecx = *((rsp + 7));
            rdi = rax + rbx;
            *(rax) = cl;
            rax = *((rsp + 8));
            if (rax != 0) {
                *(rax) = rdi;
            }
            rdx = r13;
            rsi = r12;
            mempcpy ();
            *(rax) = 0;
        }
        rax = r15;
        return rax;
label_1:
        ebx = 0;
        r15d = 0;
        eax = 0x2e;
        if (*(r12) != 0x2f) {
            eax = r15d;
        }
        bl = (*(r12) == 0x2f) ? 1 : 0;
        *((rsp + 7)) = al;
    } while (1);
label_2:
    *((rsp + 7)) = 0;
    ebx = 0;
    goto label_0;
}

/* /tmp/tmpvkjt68yd @ 0x15490 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x2760)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmpvkjt68yd @ 0x140a0 */
 
uint64_t dbg_rpl_regexec (int64_t arg_38h, int64_t arg1, int64_t arg3, int64_t arg4, int64_t arg5, char * s) {
    size_t var_8h;
    rdi = arg1;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    rsi = s;
    /* int rpl_regexec(regex_t const * restrict preg,char const * restrict string,size_t nmatch,regmatch_t * pmatch,int eflags); */
    if ((r8d & 0xfffffff8) != 0) {
        goto label_1;
    }
    r14 = rdx;
    r13 = rcx;
    r12 = rsi;
    ebx = r8d;
    if ((r8b & 4) != 0) {
        goto label_2;
    }
    rax = strlen (rsi);
    ecx = 0;
    rdx = rax;
    do {
        rax = *(rbp);
        *((rsp + 8)) = rdx;
        *(rsp) = rcx;
        r15 = rax + 0xe8;
        rdi = r15;
        pthread_mutex_lock ();
        rcx = *(rsp);
        rdx = *((rsp + 8));
        if ((*((rbp + 0x38)) & 0x10) == 0) {
            goto label_3;
        }
label_0:
        r9 = rdx;
        eax = re_search_internal (rbp, r12, rdx, rcx, rdx, r9);
        rdi = r15;
        ebx = eax;
        eax = pthread_mutex_unlock ();
        eax = 0;
        al = (ebx != 0) ? 1 : 0;
        return rax;
label_2:
        rcx = *(rcx);
        rdx = *((r13 + 8));
    } while (1);
label_3:
    goto label_0;
label_1:
    eax = 2;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x14c10 */
 
uint64_t xmalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpvkjt68yd @ 0x14030 */
 
void dbg_rpl_regfree (int64_t arg_e8h, int64_t arg1) {
    rdi = arg1;
    /* void rpl_regfree(regex_t * preg); */
    rbx = rdi;
    rbp = *(rdi);
    if (rbp != 0) {
        rdi = rbp + 0xe8;
        pthread_mutex_destroy ();
        free_dfa_content (rbp, rsi, rdx, rcx);
    }
    *(rbx) = 0;
    *((rbx + 8)) = 0;
    free (*((rbx + 0x20)));
    *((rbx + 0x20)) = 0;
    free (*((rbx + 0x28)));
    *((rbx + 0x28)) = 0;
}

/* /tmp/tmpvkjt68yd @ 0x2620 */
 
void pthread_mutex_destroy (void) {
    __asm ("bnd jmp qword [reloc.pthread_mutex_destroy]");
}

/* /tmp/tmpvkjt68yd @ 0x5980 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpvkjt68yd @ 0x15150 */
 
uint64_t dbg_xstrdup (int64_t arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rax = malloc (r12);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2700)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpvkjt68yd @ 0x25e0 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmpvkjt68yd @ 0x14c30 */
 
uint64_t ximalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpvkjt68yd @ 0x14990 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0x14520)() ();
}

/* /tmp/tmpvkjt68yd @ 0x3c20 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, 0x00016198);
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x00016550);
    } while (1);
}

/* /tmp/tmpvkjt68yd @ 0x151d0 */
 
int64_t dbg_last_component (char const * name) {
    rdi = name;
    /* char * last_component(char const * name); */
    edx = *(rdi);
    rax = rdi;
    if (dl != 0x2f) {
        goto label_1;
    }
    do {
        edx = *((rax + 1));
        rax++;
    } while (dl == 0x2f);
label_1:
    if (dl == 0) {
        goto label_2;
    }
    rcx = rax;
    esi = 0;
    while (dl != 0x2f) {
        if (sil != 0) {
            rax = rcx;
            esi = 0;
        }
        edx = *((rcx + 1));
        rcx++;
        if (dl == 0) {
            goto label_2;
        }
label_0:
    }
    edx = *((rcx + 1));
    rcx++;
    esi = 1;
    if (dl != 0) {
        goto label_0;
    }
label_2:
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x158c0 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmpvkjt68yd @ 0x14bd0 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpvkjt68yd @ 0x15840 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x6030 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x0001e3d0]");
    __asm ("movdqa xmm2, xmmword [0x0001e3e0]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x2953)() ();
    }
    if (rax == 0) {
        void (*0x2953)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x5db0 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0001e3d0]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x0001e3e0]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x15020 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpvkjt68yd @ 0x26d0 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmpvkjt68yd @ 0x5650 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x6170 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpvkjt68yd @ 0x55f0 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x15090 */
 
uint64_t xmemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2700)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpvkjt68yd @ 0x5890 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rbx += 0x10;
        free (*(rbx));
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        free (rdi);
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        free (r12);
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x144a0 */
 
uint32_t dbg_mkostemp_safer (int64_t arg2) {
    rsi = arg2;
    /* int mkostemp_safer(char * templ,int flags); */
    eax = mkostemp ();
    esi = ebp;
    edi = eax;
    return void (*0x15410)() ();
}

/* /tmp/tmpvkjt68yd @ 0x2650 */
 
void mkostemp (void) {
    __asm ("bnd jmp qword [reloc.mkostemp]");
}

/* /tmp/tmpvkjt68yd @ 0x15470 */
 
int32_t dbg_dup_safer_flag (int64_t arg_80h, int64_t arg1, int64_t arg2, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* int dup_safer_flag(int fd,int flag); */
    esi &= 0x80000;
    eax = 0x406;
    edx = 3;
    if (esi != 0) {
        esi = eax;
    }
    eax = 0;
    return void (*0x155d0)() ();
}

/* /tmp/tmpvkjt68yd @ 0x3c10 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmpvkjt68yd @ 0x14ca0 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpvkjt68yd @ 0x14e60 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x152d0 */
 
int64_t dbg_gl_dynarray_resize (int64_t arg_8h, void * arg_10h, uint32_t arg1, uint32_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* _Bool gl_dynarray_resize(dynarray_header * list,size_t size,void * scratch,size_t element_size); */
    rbx = rsi;
    if (*((rdi + 8)) >= rsi) {
        goto label_2;
    }
    rax = rbx;
    rsi = rdx;
    r12 = rcx;
    rdx:rax = rax * rcx;
    if (*((rdi + 8)) overflow rsi) {
        goto label_3;
    }
    r13 = *((rdi + 0x10));
    if (r13 == rsi) {
        goto label_4;
    }
    rax = realloc (r13, rax);
    rcx = rax;
    if (rax == 0) {
        goto label_5;
    }
    do {
label_0:
        *((rbp + 0x10)) = rcx;
        *((rbp + 8)) = rbx;
label_2:
        *(rbp) = rbx;
        eax = 1;
label_1:
        return rax;
label_4:
        rax = malloc (rax);
        rcx = rax;
        if (rax == 0) {
            goto label_5;
        }
    } while (r13 == 0);
    rdx *= r12;
    rax = memcpy (rax, r13, *(rbp));
    rcx = rax;
    goto label_0;
label_5:
    eax = 0;
    goto label_1;
label_3:
    errno_location ();
    *(rax) = 0xc;
    eax = 0;
    goto label_1;
}

/* /tmp/tmpvkjt68yd @ 0x15380 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x25a0)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpvkjt68yd @ 0x14d80 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmpvkjt68yd @ 0x141a0 */
 
void dbg_rpl_re_search (int64_t arg3, int64_t arg6) {
    rdx = arg3;
    r9 = arg6;
    /* regoff_t rpl_re_search(re_pattern_buffer * bufp,char const * string,Idx length,Idx start,regoff_t range,re_registers * regs); */
    re_search_stub (rdi, rsi, rdx, rcx, r8, rdx);
}

/* /tmp/tmpvkjt68yd @ 0x6180 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpvkjt68yd @ 0x60d0 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0001e3d0]");
    __asm ("movdqa xmm2, xmmword [0x0001e3e0]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x2958)() ();
    }
    if (rax == 0) {
        void (*0x2958)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x150d0 */
 
uint64_t dbg_ximemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2700)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpvkjt68yd @ 0x5880 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0x5790)() ();
}

/* /tmp/tmpvkjt68yd @ 0x13990 */
 
int64_t dbg_rpl_re_set_syntax (reg_syntax_t syntax) {
    rdi = syntax;
    /* reg_syntax_t rpl_re_set_syntax(reg_syntax_t syntax); */
    rax = rpl_re_syntax_options;
    *(obj.rpl_re_syntax_options) = rdi;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x14410 */
 
uint64_t dbg_safe_read (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* size_t safe_read(int fd,void * buf,size_t count); */
    r13d = edi;
    rbx = rdx;
    do {
label_0:
        rax = read (r13d, rbp, rbx);
        r12 = rax;
        if (rax >= 0) {
            goto label_1;
        }
        rax = errno_location ();
        eax = *(rax);
    } while (eax == 4);
    if (rbx > 0x7ff00000) {
        if (eax != 0x16) {
            goto label_1;
        }
        ebx = 0x7ff00000;
        goto label_0;
    }
label_1:
    rax = r12;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x5960 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpvkjt68yd @ 0x2970 */
 
uint64_t dbg_main (int32_t argc, char ** argv) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t status;
    rdi = argc;
    rsi = argv;
    /* int main(int argc,char ** argv); */
    r13 = obj_longopts;
    r12 = 0x000160b2;
    rbx = rsi;
    set_program_name (*(rsi), rsi, rdx);
    setlocale (6, 0x00017069);
    bindtextdomain (r12, "/usr/local/share/locale");
    r12 = "brs:";
    textdomain (r12, rsi);
    rdi = dbg_close_stdout;
    atexit ();
    rax = 0x00017068;
    *(obj.sentinel_length) = 1;
    *(obj.separator) = rax;
    *(obj.separator_ends_record) = 1;
    do {
label_0:
        r8d = 0;
        rcx = r13;
        rdx = r12;
        rsi = rbx;
        edi = ebp;
        eax = getopt_long ();
        if (eax == 0xffffffff) {
            goto label_12;
        }
        if (eax == 0x62) {
            goto label_13;
        }
        if (eax <= 0x62) {
            if (eax == 0xffffff7d) {
                goto label_14;
            }
            if (eax != 0xffffff7e) {
                goto label_15;
            }
            eax = usage (0);
        }
        if (eax == 0x72) {
            goto label_16;
        }
        if (eax != 0x73) {
            goto label_15;
        }
        rax = optarg;
        *(obj.separator) = rax;
    } while (1);
label_13:
    *(obj.separator_ends_record) = 0;
    goto label_0;
label_16:
    *(obj.sentinel_length) = 0;
    goto label_0;
label_14:
    eax = 0;
    version_etc (*(obj.stdout), 0x00016034, "GNU coreutils", *(obj.Version), "Jay Lepreau", "David MacKenzie");
    exit (0);
label_15:
    usage (1);
label_12:
    r12 = separator;
    edx = *(r12);
    if (*(obj.sentinel_length) == 0) {
        goto label_17;
    }
    eax = 1;
    if (dl != 0) {
        rax = strlen (r12);
    }
    *(obj.sentinel_length) = rax;
    *(obj.match_length) = rax;
label_1:
    rsi = sentinel_length;
    edx = 0x33;
    edi = 0;
    eax = _init;
    *(obj.read_size) = 0x2000;
    while (rcx <= rsi) {
        edx--;
        if (edx == 0) {
            goto label_18;
        }
        rax += rax;
        edi = 1;
        rcx = rax;
        rcx >>= 1;
    }
    if (dil != 0) {
        *(obj.read_size) = rax;
    }
    rdx = rsi + rax + 1;
    rdi = rdx + rdx;
    al = (rdx <= rax) ? 1 : 0;
    *(obj.G_buffer_size) = rdi;
    dl = (rdi <= rdx) ? 1 : 0;
    al |= dl;
    if (al == 0) {
        goto label_19;
    }
label_2:
    xalloc_die ();
label_17:
    if (dl == 0) {
        goto label_20;
    }
    rax = obj_compiled_separator_fastmap;
    *(obj.compiled_separator) = 0;
    *(0x0001e228) = 0;
    *(obj.compiled_separator_fastmap) = rax;
    *(0x0001e248) = 0;
    rax = strlen (r12);
    rax = rpl_re_compile_pattern (r12, rax, obj.compiled_separator);
    if (rax == 0) {
        goto label_1;
    }
    rcx = rax;
    eax = 0;
    rax = error (1, 0, 0x00016550);
label_18:
    if (dil == 0) {
        goto label_2;
    }
    *(obj.read_size) = rax;
    goto label_2;
label_19:
    rax = xmalloc (rdi);
    r12 = sentinel_length;
    rcx = rax;
    rax = rax + 1;
    if (r12 != 0) {
        goto label_21;
    }
label_10:
    *(obj.G_buffer) = rax;
    rax = *(obj.optind);
    rdx = 0x0001ba60;
    if (eax < ebp) {
        rdx = rbx + rax*8;
    }
    *((rsp + 8)) = rdx;
    *((rsp + 0x1f)) = 1;
label_3:
    rax = *((rsp + 8));
    r14 = *(rax);
    if (r14 == 0) {
        goto label_22;
    }
    eax = strcmp (r14, 0x00016133);
    ebx = eax;
    if (eax != 0) {
        goto label_23;
    }
    edx = 5;
    ebp = 0;
    *(obj.have_read_stdin) = 1;
    rax = dcgettext (0, "standard input");
    r14 = rax;
label_4:
    edx = 2;
    esi = 0;
    edi = ebp;
    rax = lseek ();
    r12 = rax;
    if (rax >= 0) {
        eax = isatty (ebp);
        if (eax == 0) {
            goto label_24;
        }
    }
    if (*(obj.tempfile.3) == 0) {
        goto label_25;
    }
    r12 = tmp_fp.2;
    rdi = tmp_fp.2;
    clearerr_unlocked ();
    eax = rpl_fseeko (r12, 0, 0, rcx);
    if (eax < 0) {
        goto label_26;
    }
    eax = fileno (*(obj.tmp_fp.2));
    esi = 0;
    edi = eax;
    eax = ftruncate ();
    if (eax < 0) {
        goto label_26;
    }
label_9:
    rax = tempfile.3;
    r13 = tmp_fp.2;
    r12d = 0;
    *((rsp + 0x10)) = rax;
    while (rax != 0) {
        if (rax == -1) {
            goto label_27;
        }
        rdi = G_buffer;
        rcx = r13;
        rdx = rax;
        esi = 1;
        rax = fwrite_unlocked ();
        if (r15 != rax) {
            goto label_28;
        }
        r12 += r15;
        rax = safe_read (ebp, *(obj.G_buffer), *(obj.read_size));
        r15 = rax;
    }
    rdi = r13;
    eax = fflush_unlocked ();
    if (eax != 0) {
        goto label_28;
    }
    if (r12 < 0) {
        goto label_29;
    }
    eax = fileno (r13);
    eax = tac_seekable (eax, *((rsp + 0x10)), r12);
    r12d = eax;
label_7:
    if (ebx != 0) {
        goto label_30;
    }
label_5:
    goto label_3;
label_23:
    eax = 0;
    eax = open (r14, 0, rdx);
    if (eax >= 0) {
        goto label_4;
    }
    rsi = r14;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
label_6:
    rax = dcgettext (0, "failed to open %s for reading");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    r12d = 0;
    error (0, *(rax), r12);
    goto label_5;
label_30:
    eax = close (ebp);
    if (eax == 0) {
        goto label_5;
    }
    rdx = r14;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    edx = 5;
    rsi = "%s: read error";
    r13 = rax;
    goto label_6;
label_27:
    rdx = r14;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    edx = 5;
    r13 = rax;
    do {
label_8:
        rax = dcgettext (0, "%s: read error");
        r12 = rax;
        rax = errno_location ();
        rcx = r13;
        eax = 0;
        r12d = 0;
        error (0, *(rax), r12);
        goto label_7;
label_28:
        rdx = *((rsp + 0x10));
        esi = 3;
        edi = 0;
        rax = quotearg_n_style_colon ();
        edx = 5;
        rsi = "%s: write error";
        r13 = rax;
    } while (1);
label_26:
    rsi = tempfile.3;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    rsi = "failed to rewind stream for %s";
    r13 = rax;
    goto label_8;
label_25:
    rax = getenv ("TMPDIR");
    r12 = rax;
    while (1) {
        rax = mfile_name_concat (r12, "tacXXXXXX", 0);
        rdi = rax;
        *(obj.tempfile.3) = rax;
        eax = mkstemp_safer (rdi);
        r13d = eax;
        if (eax < 0) {
            goto label_31;
        }
        edi = eax;
        rsi = 0x00016173;
        rax = fdopen ();
        rdi = tempfile.3;
        *(obj.tmp_fp.2) = rax;
        if (rax == 0) {
            goto label_32;
        }
        unlink (rdi);
        goto label_9;
label_24:
        eax = tac_seekable (ebp, r14, r12);
        r12d = eax;
        goto label_7;
label_22:
        output (0, 0);
        if (*(obj.have_read_stdin) != 0) {
            goto label_33;
        }
label_11:
        edi = *((rsp + 0x1f));
        edi = (int32_t) dil;
        exit (1);
        r12 = "/tmp";
    }
label_21:
    memcpy (rcx, *(obj.separator), r12 + 1);
    rax = rax + r12;
    goto label_10;
label_33:
    eax = close (0);
    if (eax >= 0) {
        goto label_11;
    }
    rax = errno_location ();
    eax = 0;
    error (0, *(rax), 0x00016133);
    *((rsp + 0x1f)) = 0;
    goto label_11;
label_31:
    rsi = r12;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "failed to create temporary file in %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (0, *(rax), r12);
    do {
        r12d = 0;
        free (*(obj.tempfile.3));
        *(obj.tempfile.3) = 0;
        goto label_7;
label_20:
        edx = 5;
        rax = dcgettext (0, "separator cannot be empty");
        eax = 0;
        error (1, 0, rax);
label_29:
        r12d = 0;
        goto label_7;
label_32:
        rsi = rdi;
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        *((rsp + 0x10)) = rax;
        rax = dcgettext (0, "failed to open %s for writing");
        r12 = rax;
        rax = errno_location ();
        rcx = *((rsp + 0x10));
        eax = 0;
        error (0, *(rax), r12);
        close (r13d);
        unlink (*(obj.tempfile.3));
    } while (1);
}

/* /tmp/tmpvkjt68yd @ 0x3850 */
 
int64_t dbg_usage (int64_t arg1) {
    infomap const[7] const infomap;
    char * var_8h;
    int64_t var_10h;
    char * var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    char * var_60h;
    int64_t var_68h;
    int64_t var_78h;
    rdi = arg1;
    /* void usage(int status); */
    edx = 5;
    r12 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r12;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_0:
        exit (ebp);
    }
    rbx = rsp;
    rax = dcgettext (0, "Usage: %s [OPTION]... [FILE]...\n");
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "Write each FILE to standard output, last line first.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nWith no FILE, or when FILE is -, read standard input.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nMandatory arguments to long options are mandatory for short options too.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -b, --before             attach the separator before instead of after\n  -r, --regex              interpret the separator as a regular expression\n  -s, --separator=STRING   use STRING as the separator instead of newline\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = r12;
    r12 = 0x00016034;
    rdi = rax;
    fputs_unlocked ();
    rax = 0x00016038;
    rcx = "sha256sum";
    *((rsp + 0x60)) = 0;
    *(rsp) = rax;
    rax = "test invocation";
    *((rsp + 8)) = rax;
    rax = 0x000160b2;
    *((rsp + 0x10)) = rax;
    rax = "Multi-call invocation";
    *((rsp + 0x18)) = rax;
    rax = "sha224sum";
    *((rsp + 0x30)) = rcx;
    rcx = "sha384sum";
    *((rsp + 0x20)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x40)) = rcx;
    rcx = "sha512sum";
    *((rsp + 0x28)) = rax;
    *((rsp + 0x38)) = rax;
    *((rsp + 0x48)) = rax;
    *((rsp + 0x50)) = rcx;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x68)) = 0;
    do {
        rsi = *((rbx + 0x10));
        rbx += 0x10;
        if (rsi == 0) {
            goto label_2;
        }
        eax = strcmp (r12, rsi);
    } while (eax != 0);
label_2:
    r13 = *((rbx + 8));
    edx = 5;
    rsi = "\n%s online help: <%s>\n";
    edi = 0;
    if (r13 == 0) {
        goto label_3;
    }
    rax = dcgettext (rdi, rsi);
    r14 = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    rcx = r14;
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax != 0) {
        eax = strncmp (rdi, 0x000160bc, 3);
        if (eax != 0) {
            goto label_4;
        }
    }
label_1:
    edx = 5;
    rax = dcgettext (0, "Full documentation <%s%s>\n");
    rcx = r12;
    rdx = r14;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    rax = 0x00017069;
    r12 = 0x00016054;
    r12 = rax;
    while (1) {
        edx = 5;
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rcx = r12;
        rdx = r13;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        goto label_0;
label_3:
        rax = dcgettext (rdi, rsi);
        r14 = "https://www.gnu.org/software/coreutils/";
        edi = 1;
        rdx = "GNU coreutils";
        rsi = rax;
        rcx = r14;
        eax = 0;
        printf_chk ();
        rax = setlocale (5, 0);
        rdi = rax;
        if (rax != 0) {
            eax = strncmp (rdi, 0x000160bc, 3);
            if (eax != 0) {
                goto label_5;
            }
        }
        edx = 5;
        rax = dcgettext (0, "Full documentation <%s%s>\n");
        rcx = r12;
        rdx = r14;
        edi = 1;
        rsi = rax;
        eax = 0;
        r13 = 0x00016034;
        printf_chk ();
        r12 = 0x00016054;
    }
label_5:
    r13 = 0x00016034;
label_4:
    r15 = stdout;
    edx = 5;
    rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
    rdi = rax;
    rsi = r15;
    fputs_unlocked ();
    goto label_1;
}

/* /tmp/tmpvkjt68yd @ 0x5630 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x5d20 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0001e3d0]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x0001e3e0]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x155d0 */
 
int64_t dbg_rpl_fcntl (int64_t arg_80h, int64_t arg1, int32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6, int32_t target) {
    va_list arg;
    int64_t var_ch;
    int64_t var_10h;
    int64_t canary;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r13 = target;
    /* int rpl_fcntl(int fd,int action,va_args ...); */
    *((rsp + 0x40)) = rdx;
    *((rsp + 0x48)) = rcx;
    *((rsp + 0x50)) = r8;
    *((rsp + 0x58)) = r9;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = rsp + 0x80;
    *((rsp + 0x10)) = 0x10;
    *((rsp + 0x18)) = rax;
    rax = rsp + 0x30;
    *((rsp + 0x20)) = rax;
    if (esi == 0) {
        goto label_6;
    }
    if (esi == 0x406) {
        goto label_7;
    }
    if (esi > 0xb) {
        goto label_8;
    }
    if (esi > 0) {
        goto label_9;
    }
    do {
label_0:
        eax = *((rsp + 0x10));
        if (eax > 0x2f) {
            goto label_10;
        }
        rax += *((rsp + 0x20));
label_5:
        rdx = *(rax);
        eax = 0;
        eax = fcntl ();
        r12d = eax;
        goto label_1;
label_8:
        ecx = rsi - 0x400;
    } while (ecx > 0xa);
    eax = 1;
    rax <<= cl;
    if ((eax & 0x2c5) != 0) {
        goto label_11;
    }
    if ((eax & 0x502) == 0) {
        goto label_0;
    }
    do {
        eax = 0;
        eax = fcntl ();
        r12d = eax;
        goto label_1;
label_9:
        eax = 1;
        ecx = esi;
        rax <<= cl;
        if ((eax & 0x514) != 0) {
            goto label_11;
        }
    } while ((eax & 0xa0a) != 0);
    goto label_0;
label_6:
    rax = *((rsp + 0x20));
    esi = 0;
    edx = *((rax + 0x10));
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    do {
label_1:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_12;
        }
        eax = r12d;
        return rax;
label_7:
        rax = *((rsp + 0x20));
        *((rsp + 0x10)) = 0x18;
        r13d = *((rax + 0x10));
        eax = have_dupfd_cloexec.0;
        edx = r13d;
        if (eax < 0) {
            goto label_13;
        }
        esi = 0x406;
        eax = 0;
        *((rsp + 0xc)) = edi;
        eax = fcntl ();
        r12d = eax;
        if (eax < 0) {
            goto label_14;
        }
label_2:
        *(obj.have_dupfd_cloexec.0) = 1;
    } while (1);
label_11:
    eax = *((rsp + 0x10));
    if (eax > 0x2f) {
        goto label_15;
    }
    rax += *((rsp + 0x20));
label_4:
    edx = *(rax);
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    goto label_1;
label_13:
    esi = 0;
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    if (eax < 0) {
        goto label_1;
    }
    if (*(obj.have_dupfd_cloexec.0) != 0xffffffff) {
        goto label_1;
    }
label_3:
    eax = 0;
    esi = 1;
    edi = r12d;
    eax = fcntl ();
    if (eax < 0) {
        goto label_16;
    }
    eax |= 1;
    esi = 2;
    edi = r12d;
    edx = eax;
    eax = 0;
    eax = fcntl ();
    if (eax != 0xffffffff) {
        goto label_1;
    }
label_16:
    rax = errno_location ();
    r12d |= 0xffffffff;
    r13d = *(rax);
    close (r12d);
    *(rbp) = r13d;
    goto label_1;
label_14:
    rax = errno_location ();
    edi = *((rsp + 0xc));
    if (*(rax) != 0x16) {
        goto label_2;
    }
    edx = r13d;
    esi = 0;
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    if (eax < 0) {
        goto label_1;
    }
    *(obj.have_dupfd_cloexec.0) = 0xffffffff;
    goto label_3;
label_15:
    rax = *((rsp + 0x18));
    goto label_4;
label_10:
    rax = *((rsp + 0x18));
    goto label_5;
label_12:
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x5b50 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x293f)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x13fa0 */
 
uint64_t rpl_regerror (uint32_t arg1, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rdx = arg3;
    rcx = arg4;
    if (edi > 0x10) {
        void (*0x2962)() ();
    }
    rdi = (int64_t) edi;
    rax = obj___re_error_msgid_idx;
    r13 = rdx;
    rbx = rcx;
    edx = 5;
    rsi += *((rax + rdi*8));
    rax = dcgettext (0, "Success");
    rdi = rax;
    strlen (rdi);
    r12 = rax + 1;
    if (rbx == 0) {
        goto label_0;
    }
    rdx = r12;
    while (1) {
        memcpy (r13, rbp, rdx);
label_0:
        rax = r12;
        return rax;
        *((r13 + rbx - 1)) = 0;
        rdx = rbx - 1;
    }
}

/* /tmp/tmpvkjt68yd @ 0x5a30 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x2935)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x61c0 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpvkjt68yd @ 0x56d0 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x292a)() ();
    }
    if (rdx == 0) {
        void (*0x292a)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x5be0 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0001e3d0]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x0001e3e0]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x14ff0 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpvkjt68yd @ 0x15070 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpvkjt68yd @ 0x5930 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpvkjt68yd @ 0x14d10 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpvkjt68yd @ 0x61a0 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpvkjt68yd @ 0x3dd0 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x2840)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmpvkjt68yd @ 0x5670 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x14ef0 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmpvkjt68yd @ 0x15410 */
 
uint64_t dbg_fd_safer_flag (uint32_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* int fd_safer_flag(int fd,int flag); */
    r12d = edi;
    if (edi > 2) {
        eax = r12d;
        return eax;
    }
    eax = dup_safer_flag (rdi, rsi, rdx, rcx, r8, r9);
    rax = errno_location ();
    r12d = ebp;
    r13d = *(rax);
    rbx = rax;
    close (r12d);
    eax = r12d;
    *(rbx) = r13d;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x14b30 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmpvkjt68yd @ 0x26f0 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmpvkjt68yd @ 0x27e0 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmpvkjt68yd @ 0x3c00 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmpvkjt68yd @ 0x15260 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpvkjt68yd @ 0x2530 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmpvkjt68yd @ 0x14520 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = "%s (%s) %s\n";
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x00017023);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x00017310;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0x17310 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x28a0)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x28a0)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x28a0)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmpvkjt68yd @ 0x144c0 */
 
uint64_t dbg_fd_safer (uint32_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* int fd_safer(int fd); */
    r12d = edi;
    if (edi > 2) {
        eax = r12d;
        return eax;
    }
    eax = dup_safer (rdi, rsi, rdx, rcx, r8);
    rax = errno_location ();
    r12d = ebp;
    r13d = *(rax);
    rbx = rax;
    close (r12d);
    eax = r12d;
    *(rbx) = r13d;
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x149b0 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvkjt68yd @ 0x2000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmpvkjt68yd @ 0x15050 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpvkjt68yd @ 0x139b0 */
 
int32_t dbg_rpl_re_compile_fastmap (uint32_t arg_48h, int64_t arg_50h, uint32_t arg_58h, uint32_t arg_60h, int64_t arg1) {
    rdi = arg1;
    /* int rpl_re_compile_fastmap(re_pattern_buffer * bufp); */
    eax = 0;
    r12 = *((rdi + 0x20));
    rbx = rdi;
    rbp = *(rdi);
    rdi = r12 + 8;
    rcx = r12;
    *(r12) = 0;
    rdx = r12;
    *((r12 + 0xf8)) = 0;
    rdi &= 0xfffffffffffffff8;
    rcx -= rdi;
    ecx += 0x100;
    ecx >>= 3;
    do {
        *(rdi) = rax;
        rcx--;
        rdi += 8;
    } while (rcx != 0);
    re_compile_fastmap_iter (rbx, *((rbp + 0x48)), rdx, rcx);
    rsi = *((rbp + 0x50));
    if (*((rbp + 0x48)) != rsi) {
        re_compile_fastmap_iter (rbx, rsi, r12, rcx);
        rsi = *((rbp + 0x48));
    }
    r8 = *((rbp + 0x58));
    if (r8 != rsi) {
        re_compile_fastmap_iter (rbx, r8, r12, rcx);
        rsi = *((rbp + 0x48));
    }
    r8 = *((rbp + 0x60));
    if (r8 != rsi) {
        eax = re_compile_fastmap_iter (rbx, r8, r12, rcx);
    }
    *((rbx + 0x38)) |= 8;
    eax = 0;
    return eax;
}

/* /tmp/tmpvkjt68yd @ 0x15970 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmpvkjt68yd @ 0x14d50 */
 
uint64_t dbg_xnmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpvkjt68yd @ 0x15110 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi + 1);
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2700)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpvkjt68yd @ 0x24b0 */
 
void ctype_toupper_loc (void) {
    /* [15] -r-x section size 1136 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmpvkjt68yd @ 0x24c0 */
 
void getenv (void) {
    __asm ("bnd jmp qword [reloc.getenv]");
}

/* /tmp/tmpvkjt68yd @ 0x0 */
 
int64_t libc_start_main (int32_t argc, func init, func main, char ** ubp_av) {
    rsi = argc;
    rcx = init;
    rdi = main;
    rdx = ubp_av;
    bh &= *(rdi);
    *(rax) += dh;
    *((rcx + rsi)) ^= esi;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *((rdx - 0x55555556)) += ch;
    *(rdi) = al;
    rdi++;
    *(rdi) = al;
    rdi++;
    ch |= *((rdx - 0x55555556));
    *(rdi) = al;
    rdi++;
    *(rdi) = al;
    rdi++;
    al |= *(rcx);
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += cl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *((rbp + 1)) >>= 0;
    *(rax) += al;
    *(rax) += al;
    *(rcx)++;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rsi) += cl;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    bh += bh;
    *(rax)++;
    *(rdi) += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al += *(rax);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al += *(rax);
    *(rax) += al;
    *(rax) += al;
    *(rbx) += ah;
    *(rax) += eax;
    bh += bh;
    *(rax)++;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) -= eax;
    *(rax) += al;
    *(rax)++;
}

/* /tmp/tmpvkjt68yd @ 0x2500 */
 
void unlink (void) {
    __asm ("bnd jmp qword [reloc.unlink]");
}

/* /tmp/tmpvkjt68yd @ 0x2520 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmpvkjt68yd @ 0x2540 */
 
void isatty (void) {
    __asm ("bnd jmp qword [reloc.isatty]");
}

/* /tmp/tmpvkjt68yd @ 0x2560 */
 
void fcntl (void) {
    __asm ("bnd jmp qword [reloc.fcntl]");
}

/* /tmp/tmpvkjt68yd @ 0x2570 */
 
void clearerr_unlocked (void) {
    __asm ("bnd jmp qword [reloc.clearerr_unlocked]");
}

/* /tmp/tmpvkjt68yd @ 0x2580 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmpvkjt68yd @ 0x2590 */
 
void iswctype (void) {
    __asm ("bnd jmp qword [reloc.iswctype]");
}

/* /tmp/tmpvkjt68yd @ 0x25a0 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmpvkjt68yd @ 0x25b0 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmpvkjt68yd @ 0x25d0 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmpvkjt68yd @ 0x2600 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmpvkjt68yd @ 0x2610 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmpvkjt68yd @ 0x2640 */
 
void ftruncate (void) {
    __asm ("bnd jmp qword [reloc.ftruncate]");
}

/* /tmp/tmpvkjt68yd @ 0x2660 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmpvkjt68yd @ 0x2670 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmpvkjt68yd @ 0x2690 */
 
void read (void) {
    __asm ("bnd jmp qword [reloc.read]");
}

/* /tmp/tmpvkjt68yd @ 0x26a0 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmpvkjt68yd @ 0x26b0 */
 
void wctype (void) {
    __asm ("bnd jmp qword [reloc.wctype]");
}

/* /tmp/tmpvkjt68yd @ 0x26c0 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmpvkjt68yd @ 0x26e0 */
 
void strcmp (void) {
    __asm ("bnd jmp qword [reloc.strcmp]");
}

/* /tmp/tmpvkjt68yd @ 0x2710 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmpvkjt68yd @ 0x2720 */
 
void pthread_mutex_unlock (void) {
    __asm ("bnd jmp qword [reloc.pthread_mutex_unlock]");
}

/* /tmp/tmpvkjt68yd @ 0x2730 */
 
void iswalnum (void) {
    __asm ("bnd jmp qword [reloc.iswalnum]");
}

/* /tmp/tmpvkjt68yd @ 0x2750 */
 
void wcrtomb (void) {
    __asm ("bnd jmp qword [reloc.wcrtomb]");
}

/* /tmp/tmpvkjt68yd @ 0x2760 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmpvkjt68yd @ 0x2790 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmpvkjt68yd @ 0x27c0 */
 
void fdopen (void) {
    __asm ("bnd jmp qword [reloc.fdopen]");
}

/* /tmp/tmpvkjt68yd @ 0x27d0 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmpvkjt68yd @ 0x27f0 */
 
void btowc (void) {
    __asm ("bnd jmp qword [reloc.btowc]");
}

/* /tmp/tmpvkjt68yd @ 0x2800 */
 
void mempcpy (void) {
    __asm ("bnd jmp qword [reloc.mempcpy]");
}

/* /tmp/tmpvkjt68yd @ 0x2830 */
 
void open (void) {
    __asm ("bnd jmp qword [reloc.open]");
}

/* /tmp/tmpvkjt68yd @ 0x2840 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmpvkjt68yd @ 0x2850 */
 
void towlower (void) {
    __asm ("bnd jmp qword [reloc.towlower]");
}

/* /tmp/tmpvkjt68yd @ 0x2860 */
 
void towupper (void) {
    __asm ("bnd jmp qword [reloc.towupper]");
}

/* /tmp/tmpvkjt68yd @ 0x2870 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmpvkjt68yd @ 0x2880 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmpvkjt68yd @ 0x28a0 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmpvkjt68yd @ 0x28b0 */
 
void fflush_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fflush_unlocked]");
}

/* /tmp/tmpvkjt68yd @ 0x28c0 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmpvkjt68yd @ 0x28d0 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmpvkjt68yd @ 0x28e0 */
 
void pthread_mutex_init (void) {
    __asm ("bnd jmp qword [reloc.pthread_mutex_init]");
}

/* /tmp/tmpvkjt68yd @ 0x28f0 */
 
void pthread_mutex_lock (void) {
    __asm ("bnd jmp qword [reloc.pthread_mutex_lock]");
}

/* /tmp/tmpvkjt68yd @ 0x2900 */
 
void ctype_tolower_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_tolower_loc]");
}

/* /tmp/tmpvkjt68yd @ 0x2910 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmpvkjt68yd @ 0x2030 */
 
void fcn_00002030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 1152 named .plt */
    __asm ("bnd jmp qword [0x0001bd98]");
}

/* /tmp/tmpvkjt68yd @ 0x2040 */
 
void fcn_00002040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2050 */
 
void fcn_00002050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2060 */
 
void fcn_00002060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2070 */
 
void fcn_00002070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2080 */
 
void fcn_00002080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2090 */
 
void fcn_00002090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x20a0 */
 
void fcn_000020a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x20b0 */
 
void fcn_000020b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x20c0 */
 
void fcn_000020c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x20d0 */
 
void fcn_000020d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x20e0 */
 
void fcn_000020e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x20f0 */
 
void fcn_000020f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2100 */
 
void fcn_00002100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2110 */
 
void fcn_00002110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2120 */
 
void fcn_00002120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2130 */
 
void fcn_00002130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2140 */
 
void fcn_00002140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2150 */
 
void fcn_00002150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2160 */
 
void fcn_00002160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2170 */
 
void fcn_00002170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2180 */
 
void fcn_00002180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2190 */
 
void fcn_00002190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x21a0 */
 
void fcn_000021a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x21b0 */
 
void fcn_000021b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x21c0 */
 
void fcn_000021c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x21d0 */
 
void fcn_000021d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x21e0 */
 
void fcn_000021e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x21f0 */
 
void fcn_000021f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2200 */
 
void fcn_00002200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2210 */
 
void fcn_00002210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2220 */
 
void fcn_00002220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2230 */
 
void fcn_00002230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2240 */
 
void fcn_00002240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2250 */
 
void fcn_00002250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2260 */
 
void fcn_00002260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2270 */
 
void fcn_00002270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2280 */
 
void fcn_00002280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2290 */
 
void fcn_00002290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x22a0 */
 
void fcn_000022a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x22b0 */
 
void fcn_000022b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x22c0 */
 
void fcn_000022c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x22d0 */
 
void fcn_000022d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x22e0 */
 
void fcn_000022e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x22f0 */
 
void fcn_000022f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2300 */
 
void fcn_00002300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2310 */
 
void fcn_00002310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2320 */
 
void fcn_00002320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2330 */
 
void fcn_00002330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2340 */
 
void fcn_00002340 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2350 */
 
void fcn_00002350 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2360 */
 
void fcn_00002360 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2370 */
 
void fcn_00002370 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2380 */
 
void fcn_00002380 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2390 */
 
void fcn_00002390 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x23a0 */
 
void fcn_000023a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x23b0 */
 
void fcn_000023b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x23c0 */
 
void fcn_000023c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x23d0 */
 
void fcn_000023d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x23e0 */
 
void fcn_000023e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x23f0 */
 
void fcn_000023f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2400 */
 
void fcn_00002400 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2410 */
 
void fcn_00002410 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2420 */
 
void fcn_00002420 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2430 */
 
void fcn_00002430 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2440 */
 
void fcn_00002440 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2450 */
 
void fcn_00002450 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2460 */
 
void fcn_00002460 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2470 */
 
void fcn_00002470 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2480 */
 
void fcn_00002480 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvkjt68yd @ 0x2490 */
 
void fcn_00002490 (void) {
    return __asm ("bnd jmp section..plt");
}
