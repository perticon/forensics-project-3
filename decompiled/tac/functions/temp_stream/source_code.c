temp_stream (FILE **fp, char **file_name)
{
  static char *tempfile = NULL;
  static FILE *tmp_fp;
  if (tempfile == NULL)
    {
      char const *t = getenv ("TMPDIR");
      char const *tempdir = t ? t : DEFAULT_TMPDIR;
      tempfile = mfile_name_concat (tempdir, "tacXXXXXX", NULL);
      if (tempdir == NULL)
        {
          error (0, 0, _("memory exhausted"));
          return false;
        }

      /* FIXME: there's a small window between a successful mkstemp call
         and the unlink that's performed by record_or_unlink_tempfile.
         If we're interrupted in that interval, this code fails to remove
         the temporary file.  On systems that define DONT_UNLINK_WHILE_OPEN,
         the window is much larger -- it extends to the atexit-called
         unlink_tempfile.
         FIXME: clean up upon fatal signal.  Don't block them, in case
         $TMPFILE is a remote file system.  */

      int fd = mkstemp (tempfile);
      if (fd < 0)
        {
          error (0, errno, _("failed to create temporary file in %s"),
                 quoteaf (tempdir));
          goto Reset;
        }

      tmp_fp = fdopen (fd, (O_BINARY ? "w+b" : "w+"));
      if (! tmp_fp)
        {
          error (0, errno, _("failed to open %s for writing"),
                 quoteaf (tempfile));
          close (fd);
          unlink (tempfile);
        Reset:
          free (tempfile);
          tempfile = NULL;
          return false;
        }

      record_or_unlink_tempfile (tempfile, tmp_fp);
    }
  else
    {
      clearerr (tmp_fp);
      if (fseeko (tmp_fp, 0, SEEK_SET) < 0
          || ftruncate (fileno (tmp_fp), 0) < 0)
        {
          error (0, errno, _("failed to rewind stream for %s"),
                 quoteaf (tempfile));
          return false;
        }
    }

  *fp = tmp_fp;
  *file_name = tempfile;
  return true;
}