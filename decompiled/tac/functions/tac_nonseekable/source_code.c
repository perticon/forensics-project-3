tac_nonseekable (int input_fd, char const *file)
{
  FILE *tmp_stream;
  char *tmp_file;
  off_t bytes_copied = copy_to_temp (&tmp_stream, &tmp_file, input_fd, file);
  if (bytes_copied < 0)
    return false;

  bool ok = tac_seekable (fileno (tmp_stream), tmp_file, bytes_copied);
  return ok;
}