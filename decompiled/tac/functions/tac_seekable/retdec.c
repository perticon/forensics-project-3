bool tac_seekable(int32_t input_fd, char * file, uint64_t file_pos) {
    int64_t v1 = read_size; // 0x3261
    int64_t v2 = match_length; // 0x3275
    uint64_t v3 = file_pos % v1;
    char v4 = *(char *)separator; // 0x327f
    int64_t v5 = file_pos; // 0x329c
    int64_t v6 = v1; // 0x329c
    if (v3 != 0) {
        int64_t v7 = file_pos - v3; // 0x378c
        v5 = v7;
        v6 = v1;
        if (function_2660() < 0) {
            // 0x37a7
            quotearg_n_style_colon();
            function_25c0();
            function_24f0();
            function_2820();
            v5 = v7;
            v6 = read_size;
        }
    }
    int64_t v8 = safe_read(input_fd, (char *)G_buffer, v6); // 0x32cf
    int64_t v9 = read_size;
    int64_t v10 = v5; // 0x32d7
    int64_t v11 = v8; // 0x32d7
    int64_t v12 = v9; // 0x32d7
    if (v8 != 0) {
        goto lab_0x3348;
    } else {
        // 0x32d9
        if (v5 == 0) {
            goto lab_0x373f;
        } else {
            if (function_2660() < 0) {
                // branch -> 0x32c0
            }
            int64_t v13 = v5 - v9; // 0x32c0
            int64_t v14 = safe_read(input_fd, (char *)G_buffer, v9); // 0x32cf
            int64_t v15 = read_size;
            v10 = v13;
            v11 = v14;
            v12 = v15;
            while (v14 == 0) {
                // 0x32d9
                if (v13 == 0) {
                    goto lab_0x373f;
                }
                if (function_2660() < 0) {
                    // branch -> 0x32c0
                }
                // 0x32c0
                v13 -= v15;
                v14 = safe_read(input_fd, (char *)G_buffer, v15);
                v15 = read_size;
                v10 = v13;
                v11 = v14;
                v12 = v15;
            }
            goto lab_0x3348;
        }
    }
  lab_0x373f:;
    int64_t v16 = v9; // 0x3756
    int64_t v17 = 0; // 0x3756
    int64_t v18 = 0; // 0x3756
    int64_t v19 = v8; // 0x3756
    if (v9 != 0) {
        goto lab_0x33ba;
    } else {
        goto lab_0x3392;
    }
  lab_0x33ba:;
    int64_t v20 = G_buffer + v19; // 0x33d0
    int64_t v21 = v18; // 0x33f2
    int32_t v22 = 1; // 0x33f2
    int64_t v23 = v20; // 0x33f2
    int64_t v24 = G_buffer; // 0x33f2
    int64_t v25 = (sentinel_length != 0 ? 1 - v2 : 0) + v20; // 0x33f2
    int64_t v26; // 0x3250
    int64_t v27; // 0x3250
    int64_t v28; // 0x3250
    int64_t v29; // 0x3250
    int64_t v30; // 0x3250
    int64_t v31; // 0x3250
    int64_t v32; // 0x3250
    int64_t v33; // 0x3250
    int32_t v34; // 0x3250
    int32_t v35; // 0x3250
    while (true) {
      lab_0x33f8:
        // 0x33f8
        v32 = v21;
        v34 = v22;
        v30 = v23;
        v26 = v25;
        v28 = v24;
        v33 = v21;
        v35 = v22;
        v31 = v23;
        v27 = v25;
        v29 = v24;
        if (sentinel_length != 0) {
            goto lab_0x3590_2;
        } else {
            goto lab_0x3401;
        }
    }
  lab_0x37ff:
    // 0x37ff
    function_25c0();
    function_2820();
  lab_0x3823:
    // 0x3823
    xalloc_die();
  lab_0x3828:
    // 0x3828
    function_25c0();
    return function_2820() % 2 != 0;
  lab_0x3590_2:;
    int64_t v36 = v27; // 0x3250
    int64_t v37; // 0x3250
    while (true) {
      lab_0x3590:;
        int64_t v38 = v36 - 1; // 0x3593
        int64_t v39 = v38; // 0x359b
        while (*(char *)v38 != v4) {
            // 0x3590
            v38 = v39 - 1;
            v39 = v38;
        }
        // 0x359d
        v37 = v38;
        int64_t v40 = v38; // 0x35a0
        if (v2 == 1) {
            // break -> 0x3478
            break;
        }
        while (true) {
          lab_0x35a6:;
            int64_t v41 = v40;
            v37 = v41;
            if ((int32_t)function_2510() == 0) {
                // break (via goto) -> 0x3478
                goto lab_0x3478;
            }
            // 0x35b9
            v40 = v41 - 1;
            v36 = v40;
            if (*(char *)v40 != v4) {
                goto lab_0x3590;
            } else {
                goto lab_0x35a6;
            }
        }
    }
  lab_0x3478:;
    int64_t v42 = v33; // 0x3250
    int32_t v43 = v35; // 0x3250
    int64_t v44 = v31; // 0x3250
    int64_t v45 = v37; // 0x3250
    int64_t v46 = v29; // 0x3250
    goto lab_0x3478_2;
  lab_0x3401:;
    int64_t v82 = v28;
    int64_t v83 = v26 - v82; // 0x3401
    int64_t v84 = 1 - v83; // 0x340a
    if (v84 > 1) {
        // break -> 0x37ff
        goto lab_0x37ff;
    }
    int64_t v73 = v30;
    int32_t v72 = v34;
    int64_t v71 = v32;
    int64_t v74 = v82; // 0x3417
    if (v83 == 0) {
        goto lab_0x3637;
    } else {
        int64_t v85 = rpl_re_search((int32_t *)&compiled_separator, (char *)v82, v83, v83 - 1, v84, (int32_t *)&regs); // 0x3436
        switch (v85) {
            case -1: {
                // 0x3630
                v74 = G_buffer;
                goto lab_0x3637;
            }
            case -2: {
                goto lab_0x3828;
            }
            default: {
                int64_t v86 = *(int64_t *)g42; // 0x345d
                match_length = *(int64_t *)g43 - v86;
                v42 = v71;
                v43 = v72;
                v44 = v73;
                v45 = v86 + G_buffer;
                v46 = G_buffer;
                goto lab_0x3478_2;
            }
        }
    }
  lab_0x3478_2:;
    int64_t v47 = v46;
    int64_t v48 = v45;
    int64_t v49 = v44;
    int32_t v50 = v43;
    int64_t v51 = v42;
    if (v48 >= v47) {
        // 0x35d0
        int64_t v52; // 0x3250
        int64_t v53; // 0x3250
        int32_t v54; // 0x3250
        if (*(char *)&separator_ends_record == 0) {
            // 0x3660
            output((char *)v48, (char *)v49);
            v54 = v50;
            v53 = v48;
            v52 = G_buffer;
        } else {
            int64_t v55 = match_length + v48; // 0x35e9
            char v56 = (char)v50 ^ 1 | (char)(v49 != v55); // 0x35f5
            if (v56 != 0) {
                // 0x36c8
                output((char *)v55, (char *)v49);
                v54 = 0;
                v53 = v55;
                v52 = G_buffer;
            } else {
                // 0x3603
                v54 = v56;
                v53 = v55;
                v52 = v47;
            }
        }
        int64_t v57 = v52;
        int64_t v58 = v53;
        int32_t v59 = v54;
        v32 = v51;
        v34 = v59;
        v30 = v58;
        v26 = v48;
        v28 = v57;
        if (sentinel_length == 0) {
            goto lab_0x3401;
        } else {
            // 0x3616
            v21 = v51;
            v22 = v59;
            v23 = v58;
            v24 = v57;
            v25 = v48 + 1 - match_length;
            goto lab_0x33f8;
        }
    } else {
        if (v51 == 0) {
            // 0x376d
            output((char *)v47, (char *)v49);
            return true;
        }
        int64_t v60 = read_size; // 0x348c
        uint64_t v61 = v49 - v47; // 0x3493
        int64_t v62 = v60; // 0x3499
        if (v60 < v61) {
            int64_t v63 = sentinel_length; // 0x349b
            uint64_t v64 = G_buffer_size; // 0x34ab
            read_size = 2 * v60;
            int64_t v65 = v63 + (4 * v60 | 2); // 0x34bc
            G_buffer_size = v65;
            if (v65 < v64) {
                goto lab_0x3823;
            }
            // 0x34d8
            G_buffer = xrealloc() + (v63 != 0 ? v63 : 1);
            v62 = read_size;
        }
        uint64_t v66 = v62;
        int64_t v67; // 0x3250
        if (v51 < v66) {
            // 0x3640
            read_size = v51;
            v67 = 0;
        } else {
            // 0x3501
            v67 = v51 - v66;
        }
        // 0x3508
        if (function_2660() < 0) {
            // 0x3680
            quotearg_n_style_colon();
            function_25c0();
            function_24f0();
            function_2820();
        }
        // 0x3520
        function_2810();
        if (safe_read(input_fd, (char *)G_buffer, read_size) != read_size) {
            // 0x36ee
            quotearg_n_style_colon();
            function_25c0();
            function_24f0();
            function_2820();
            return false;
        }
        int64_t v68 = read_size + G_buffer;
        int64_t v69 = v68 + v61; // 0x3557
        int64_t v70 = sentinel_length == 0 ? v69 : v68; // 0x3562
        v32 = v67;
        v34 = v50;
        v30 = v69;
        v26 = v70;
        v28 = G_buffer;
        v33 = v67;
        v35 = v50;
        v31 = v69;
        v27 = v70;
        v29 = G_buffer;
        if (sentinel_length == 0) {
            goto lab_0x3401;
        } else {
            goto lab_0x3590_2;
        }
    }
  lab_0x3637:
    // 0x3637
    v42 = v71;
    v43 = v72;
    v44 = v73;
    v45 = v74 - 1;
    v46 = v74;
    goto lab_0x3478_2;
  lab_0x3348:
    // 0x3348
    v16 = v12;
    v17 = v10;
    int64_t v75 = v10; // 0x3366
    int64_t v76 = v11; // 0x3366
    if (v11 != v12) {
        goto lab_0x33b0;
    } else {
        goto lab_0x3392;
    }
  lab_0x33b0:
    // 0x33b0
    v18 = v75;
    v19 = v76;
    if (v76 == -1) {
        // 0x36ee
        quotearg_n_style_colon();
        function_25c0();
        function_24f0();
        function_2820();
        return false;
    }
    goto lab_0x33ba;
  lab_0x3392:;
    int64_t v77 = v17;
    int64_t v78 = safe_read(input_fd, (char *)G_buffer, v16); // 0x33a2
    v75 = v77;
    v76 = v16;
    while (v78 != 0) {
        if (v78 == -1) {
            // 0x36ee
            quotearg_n_style_colon();
            function_25c0();
            function_24f0();
            function_2820();
            return false;
        }
        int64_t v79 = v78 + v77; // 0x3382
        int64_t v80 = v78; // 0x338c
        int64_t v81 = v79; // 0x338c
        v18 = v79;
        v19 = v78;
        if (read_size != v78) {
            goto lab_0x33ba;
        }
        v77 = v81;
        v78 = safe_read(input_fd, (char *)G_buffer, v80);
        v75 = v77;
        v76 = v80;
    }
    goto lab_0x33b0;
}