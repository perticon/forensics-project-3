int32_t tac_seekable(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r12_7;
    void** r15_8;
    void** v9;
    void** rcx10;
    uint64_t rdx11;
    uint32_t ebx12;
    int32_t v13;
    void** v14;
    void** r13_15;
    void** rax16;
    void** rax17;
    void** v18;
    int32_t ebp19;
    void** r13_20;
    void** v21;
    void** r12_22;
    void** rsi23;
    int64_t rdi24;
    void** rax25;
    int64_t rdi26;
    void** rax27;
    void** rax28;
    void** v29;
    void** r15_30;
    void** rbp31;
    void** r12_32;
    void** r13_33;
    void** r12_34;
    void** rax35;
    unsigned char v36;
    void** r14_37;
    void** rbp38;
    void** rdx39;
    void** rax40;
    int32_t r14d41;
    void** r12_42;
    void** rcx43;
    void** r15_44;
    void** r12_45;
    void** rsi46;
    int64_t rdi47;
    int1_t zf48;
    void** rdi49;
    int32_t eax50;
    int1_t zf51;
    void** rsi52;
    void** rax53;
    void** rax54;
    uint32_t esi55;
    unsigned char sil56;
    void* rdx57;
    void** rax58;
    void** rbp59;
    void* r12_60;
    uint64_t r8_61;
    int64_t rax62;
    void** rax63;
    void** rax64;
    void** rax65;
    int64_t rdi66;
    void** rax67;
    void** rax68;
    void** r15_69;
    void** r8_70;
    int64_t rdi71;
    int1_t zf72;
    void** rax73;
    int1_t zf74;
    void** rax75;
    void** rcx76;
    void** rdx77;
    void** rdi78;
    void** rsi79;
    void* rax80;

    r12_7 = read_size;
    r15_8 = separator;
    v9 = rdx;
    rcx10 = match_length;
    rdx11 = reinterpret_cast<unsigned char>(rdx) % reinterpret_cast<unsigned char>(r12_7);
    ebx12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_8));
    v13 = *reinterpret_cast<int32_t*>(&rdi);
    v14 = rcx10;
    r13_15 = rcx10 + 0xffffffffffffffff;
    if (rdx11 && (v9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v9) - rdx11), rax16 = fun_2660(rdi, rdi), reinterpret_cast<signed char>(rax16) < reinterpret_cast<signed char>(0))) {
        rax17 = quotearg_n_style_colon();
        fun_25c0();
        fun_24f0();
        rcx10 = rax17;
        fun_2820();
        r12_7 = read_size;
    }
    v18 = r13_15;
    ebp19 = v13;
    r13_20 = r12_7;
    v21 = r15_8 + 1;
    r12_22 = v9;
    while (rsi23 = G_buffer, *reinterpret_cast<int32_t*>(&rdi24) = ebp19, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0, rax25 = safe_read(rdi24, rsi23, r13_20, rcx10, r8, r9), !rax25) {
        r13_20 = read_size;
        if (!r12_22) 
            goto addr_373f_6;
        *reinterpret_cast<int32_t*>(&rdi26) = ebp19;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi26) + 4) = 0;
        rax27 = fun_2660(rdi26, rdi26);
        if (reinterpret_cast<signed char>(rax27) < reinterpret_cast<signed char>(0)) {
            rax28 = quotearg_n_style_colon();
            fun_25c0();
            fun_24f0();
            rcx10 = rax28;
            fun_2820();
            r13_20 = read_size;
        }
        r12_22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_22) - reinterpret_cast<unsigned char>(r13_20));
    }
    v29 = r12_22;
    rcx10 = r12_22;
    r15_30 = v21;
    rbp31 = rax25;
    r12_32 = read_size;
    r13_33 = v18;
    if (rax25 != r12_32) {
        addr_33b0_11:
        if (rbp31 == 0xffffffffffffffff) {
            addr_36ee_12:
            quotearg_n_style_colon();
            fun_25c0();
            fun_24f0();
            fun_2820();
            return 0;
        } else {
            addr_33ba_13:
            r12_34 = G_buffer;
            rax35 = sentinel_length;
            v36 = 1;
            r14_37 = r15_30;
            rbp38 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp31) + reinterpret_cast<unsigned char>(r12_34));
            rdx39 = rbp38;
            if (rax35) {
                rdx39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp38) - reinterpret_cast<unsigned char>(v14) + 1);
            }
        }
    } else {
        rax40 = r12_32;
        r14d41 = v13;
        r12_42 = rcx10;
        goto addr_3392_16;
    }
    rcx43 = r12_34;
    r15_44 = rcx43;
    r12_45 = rdx39;
    goto addr_33f8_18;
    do {
        addr_3392_16:
        rsi46 = G_buffer;
        *reinterpret_cast<int32_t*>(&rdi47) = r14d41;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi47) + 4) = 0;
        rbp31 = rax40;
        rax40 = safe_read(rdi47, rsi46, rax40, rcx10, r8, r9);
        if (!rax40) 
            break;
        if (rax40 == 0xffffffffffffffff) 
            goto addr_36ee_12;
        r12_42 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_42) + reinterpret_cast<unsigned char>(rax40));
        zf48 = read_size == rax40;
    } while (zf48);
    goto addr_37f3_21;
    v29 = r12_42;
    goto addr_33b0_11;
    addr_37f3_21:
    v29 = r12_42;
    rbp31 = rax40;
    goto addr_33ba_13;
    addr_373f_6:
    v29 = r12_22;
    r15_30 = v21;
    rbp31 = rax25;
    r13_33 = v18;
    if (r13_20) 
        goto addr_33ba_13;
    rax40 = r13_20;
    r14d41 = v13;
    r12_42 = v29;
    goto addr_3392_16;
    addr_37ff_24:
    fun_25c0();
    fun_2820();
    addr_3823_25:
    xalloc_die();
    addr_3828_26:
    fun_25c0();
    fun_2820();
    addr_376d_28:
    output(r15_44, rbp38, r15_44, rbp38);
    return 1;
    while (1) {
        addr_3590_29:
        rdi49 = r12_45;
        --r12_45;
        if (*reinterpret_cast<void***>(r12_45) != *reinterpret_cast<void***>(&ebx12)) 
            continue;
        while (r13_33 && (eax50 = fun_2510(rdi49, r14_37, r13_33, rcx43), !!eax50)) {
            rdi49 = r12_45;
            --r12_45;
            if (*reinterpret_cast<void***>(r12_45) != *reinterpret_cast<void***>(&ebx12)) 
                goto addr_3590_29;
        }
        while (1) {
            if (reinterpret_cast<unsigned char>(r12_45) >= reinterpret_cast<unsigned char>(r15_44)) {
                zf51 = separator_ends_record == 0;
                if (zf51) {
                    rsi52 = rbp38;
                    rbp38 = r12_45;
                    output(r12_45, rsi52, r12_45, rsi52);
                    r15_44 = G_buffer;
                } else {
                    rax53 = match_length;
                    rax54 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax53) + reinterpret_cast<unsigned char>(r12_45));
                    esi55 = static_cast<uint32_t>(v36) ^ 1;
                    sil56 = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&esi55) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(rbp38 != rax54)));
                    v36 = sil56;
                    if (sil56) {
                        output(rax54, rbp38);
                        v36 = 0;
                        rbp38 = rax54;
                        r15_44 = G_buffer;
                    } else {
                        rbp38 = rax54;
                    }
                }
                rax35 = sentinel_length;
                if (rax35) {
                    rdx57 = reinterpret_cast<void*>(1 - reinterpret_cast<unsigned char>(match_length));
                    r12_45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_45) + reinterpret_cast<uint64_t>(rdx57));
                    addr_33f8_18:
                    if (rax35) 
                        goto addr_3590_29;
                }
            } else {
                if (!v29) 
                    goto addr_376d_28;
                rax58 = read_size;
                rbp59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp38) - reinterpret_cast<unsigned char>(r15_44));
                if (reinterpret_cast<unsigned char>(rax58) >= reinterpret_cast<unsigned char>(rbp59)) 
                    goto addr_34f4_43; else 
                    goto addr_349b_44;
            }
            addr_3401_45:
            r12_60 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_45) - reinterpret_cast<unsigned char>(r15_44));
            r8_61 = 1 - reinterpret_cast<uint64_t>(r12_60);
            if (reinterpret_cast<int64_t>(r8_61) > reinterpret_cast<int64_t>(1)) 
                goto addr_37ff_24;
            if (r8_61 != 1) {
                rcx43 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r12_60) + 0xffffffffffffffff);
                r9 = reinterpret_cast<void**>(0x1e100);
                rax62 = rpl_re_search(0x1e220, r15_44, r12_60, rcx43);
                if (rax62 == -1) {
                    r15_44 = G_buffer;
                } else {
                    if (rax62 == -2) 
                        goto addr_3828_26;
                    rax63 = g1e108;
                    r15_44 = G_buffer;
                    rax64 = g1e110;
                    r12_45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_44) + reinterpret_cast<uint64_t>(*rax63));
                    match_length = reinterpret_cast<void**>(reinterpret_cast<int64_t>(*rax64) - reinterpret_cast<uint64_t>(*rax63));
                    continue;
                }
            }
            r12_45 = r15_44 + 0xffffffffffffffff;
            continue;
            addr_34f4_43:
            rcx43 = v29;
            if (reinterpret_cast<unsigned char>(rcx43) < reinterpret_cast<unsigned char>(rax58)) {
                rax65 = v29;
                v29 = reinterpret_cast<void**>(0);
                read_size = rax65;
            } else {
                rcx43 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx43) - reinterpret_cast<unsigned char>(rax58));
                v29 = rcx43;
            }
            *reinterpret_cast<int32_t*>(&rdi66) = v13;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi66) + 4) = 0;
            rax67 = fun_2660(rdi66, rdi66);
            if (reinterpret_cast<signed char>(rax67) < reinterpret_cast<signed char>(0)) {
                rax68 = quotearg_n_style_colon();
                fun_25c0();
                fun_24f0();
                rcx43 = rax68;
                fun_2820();
            }
            r15_69 = G_buffer;
            r8_70 = read_size;
            r12_45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_69) + reinterpret_cast<unsigned char>(r8_70));
            fun_2810(r12_45, r15_69, rbp59, rcx43);
            *reinterpret_cast<int32_t*>(&rdi71) = v13;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi71) + 4) = 0;
            rbp38 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp59) + reinterpret_cast<unsigned char>(r8_70)) + reinterpret_cast<unsigned char>(r15_69));
            zf72 = sentinel_length == 0;
            if (zf72) {
                r12_45 = rbp38;
            }
            rax73 = safe_read(rdi71, r15_69, r8_70, rcx43, r8_70, r9);
            zf74 = rax73 == read_size;
            if (!zf74) 
                goto addr_36ee_12;
            rax75 = sentinel_length;
            r15_44 = G_buffer;
            if (!rax75) 
                goto addr_3401_45; else 
                break;
            addr_349b_44:
            rcx76 = sentinel_length;
            *reinterpret_cast<int32_t*>(&rdx77) = 1;
            *reinterpret_cast<int32_t*>(&rdx77 + 4) = 0;
            rdi78 = G_buffer_size;
            read_size = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax58) + reinterpret_cast<unsigned char>(rax58));
            rsi79 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rcx76 + reinterpret_cast<unsigned char>(rax58) * 4) + 2);
            if (rcx76) {
                rdx77 = rcx76;
            }
            G_buffer_size = rsi79;
            if (reinterpret_cast<unsigned char>(rsi79) < reinterpret_cast<unsigned char>(rdi78)) 
                goto addr_3823_25;
            rax80 = xrealloc();
            G_buffer = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax80) + reinterpret_cast<unsigned char>(rdx77));
            rax58 = read_size;
            goto addr_34f4_43;
        }
    }
}