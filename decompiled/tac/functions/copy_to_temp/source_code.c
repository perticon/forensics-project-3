copy_to_temp (FILE **g_tmp, char **g_tempfile, int input_fd, char const *file)
{
  FILE *fp;
  char *file_name;
  uintmax_t bytes_copied = 0;
  if (!temp_stream (&fp, &file_name))
    return -1;

  while (true)
    {
      size_t bytes_read = safe_read (input_fd, G_buffer, read_size);
      if (bytes_read == 0)
        break;
      if (bytes_read == SAFE_READ_ERROR)
        {
          error (0, errno, _("%s: read error"), quotef (file));
          return -1;
        }

      if (fwrite (G_buffer, 1, bytes_read, fp) != bytes_read)
        {
          error (0, errno, _("%s: write error"), quotef (file_name));
          return -1;
        }

      /* Implicitly <= OFF_T_MAX due to preceding fwrite(),
         but unsigned type used to avoid compiler warnings
         not aware of this fact.  */
      bytes_copied += bytes_read;
    }

  if (fflush (fp) != 0)
    {
      error (0, errno, _("%s: write error"), quotef (file_name));
      return -1;
    }

  *g_tmp = fp;
  *g_tempfile = file_name;
  return bytes_copied;
}