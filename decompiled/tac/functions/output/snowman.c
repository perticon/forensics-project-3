void** output(void** rdi, void** rsi, ...) {
    void** rsi3;
    void** rdx4;
    void** rbx5;
    void** rcx6;
    void** rbx7;
    void** r8_8;
    void** r9_9;
    void** rax10;
    void** rbp11;
    void** r12_12;
    void** rdi13;
    void** rdi14;
    void** rcx15;
    void** r8_16;
    void** r9_17;

    rsi3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi) - reinterpret_cast<unsigned char>(rdi));
    rdx4 = bytes_in_buffer_1;
    rbx5 = reinterpret_cast<void**>(0x2000 - reinterpret_cast<unsigned char>(rdx4));
    if (!rdi) {
        rcx6 = stdout;
        *reinterpret_cast<int32_t*>(&rbx7) = 0;
        *reinterpret_cast<int32_t*>(&rbx7 + 4) = 0;
        rax10 = fun_27a0(0x1c0e0, 1, rdx4, rcx6, r8_8, r9_9);
    } else {
        rbp11 = rdi;
        r12_12 = rsi3;
        if (reinterpret_cast<unsigned char>(rsi3) < reinterpret_cast<unsigned char>(rbx5)) {
            rdi13 = rdx4 + 0x1c0e0;
            rbx7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx4) + reinterpret_cast<unsigned char>(rsi3));
        } else {
            do {
                rdi14 = rdx4 + 0x1c0e0;
                r12_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_12) - reinterpret_cast<unsigned char>(rbx5));
                fun_2700(rdi14, rbp11, rdi14, rbp11);
                rbp11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp11) + reinterpret_cast<unsigned char>(rbx5));
                rcx15 = stdout;
                *reinterpret_cast<int32_t*>(&rbx5) = 0x2000;
                *reinterpret_cast<int32_t*>(&rbx5 + 4) = 0;
                fun_27a0(0x1c0e0, 1, 0x2000, rcx15, r8_16, r9_17);
                *reinterpret_cast<int32_t*>(&rdx4) = 0;
                *reinterpret_cast<int32_t*>(&rdx4 + 4) = 0;
                bytes_in_buffer_1 = reinterpret_cast<void**>(0);
            } while (reinterpret_cast<unsigned char>(r12_12) > reinterpret_cast<unsigned char>(0x1fff));
            rbx7 = r12_12;
            rdi13 = reinterpret_cast<void**>(0x1c0e0);
        }
        rax10 = fun_2700(rdi13, rbp11, rdi13, rbp11);
    }
    bytes_in_buffer_1 = rbx7;
    return rax10;
}