void output(void *param_1,long param_2)

{
  size_t __n;
  ulong __n_00;
  undefined1 *__dest;
  ulong uVar1;
  
  __n_00 = param_2 - (long)param_1;
  __n = 0x2000 - bytes_in_buffer_1;
  if (param_1 == (void *)0x0) {
    uVar1 = 0;
    fwrite_unlocked(buffer_0,1,bytes_in_buffer_1,stdout);
  }
  else {
    if (__n_00 < __n) {
      __dest = buffer_0 + bytes_in_buffer_1;
      uVar1 = bytes_in_buffer_1 + __n_00;
    }
    else {
      do {
        uVar1 = __n_00 - __n;
        memcpy(buffer_0 + bytes_in_buffer_1,param_1,__n);
        param_1 = (void *)((long)param_1 + __n);
        __n = 0x2000;
        fwrite_unlocked(buffer_0,1,0x2000,stdout);
        bytes_in_buffer_1 = 0;
        __n_00 = uVar1;
      } while (0x1fff < uVar1);
      __dest = buffer_0;
    }
    memcpy(__dest,param_1,__n_00);
  }
  bytes_in_buffer_1 = uVar1;
  return;
}