void output(char * start, char * past_end) {
    if (start == NULL) {
        // 0x3218
        function_27a0();
        // 0x31ff
        g39 = 0;
        return;
    }
    int64_t v1 = (int64_t)past_end - (int64_t)start; // 0x3162
    int64_t v2 = g39; // 0x3172
    int64_t v3 = 0x2000 - v2; // 0x3179
    int64_t v4 = v1; // 0x3195
    int64_t v5; // 0x3160
    if (v1 < v3) {
        // 0x3238
        v5 = v2 + v1;
    } else {
        v4 -= v3;
        function_2700();
        function_27a0();
        g39 = 0;
        v5 = v4;
        while (v4 >= 0x2000) {
            // 0x31a0
            v4 -= 0x2000;
            function_2700();
            function_27a0();
            g39 = 0;
            v5 = v4;
        }
    }
    // 0x31f4
    function_2700();
    // 0x31ff
    g39 = v5;
}