void main(int param_1,undefined8 *param_2)

{
  ulong uVar1;
  FILE *pFVar2;
  byte bVar3;
  uint uVar4;
  int iVar5;
  undefined8 uVar6;
  ulong uVar7;
  void *__dest;
  __off_t _Var8;
  size_t sVar9;
  size_t sVar10;
  undefined8 uVar11;
  int *piVar12;
  void *pvVar13;
  char *pcVar14;
  char cVar15;
  long lVar16;
  char *unaff_R14;
  undefined1 *local_50;
  byte local_39;
  
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  sentinel_length = 1;
  separator = "\n";
  separator_ends_record = 1;
  while( true ) {
    uVar6 = getopt_long(param_1,param_2,&DAT_00116114,longopts,0);
    iVar5 = (int)uVar6;
    if (iVar5 == -1) break;
    if (iVar5 == 0x62) {
      separator_ends_record = 0;
    }
    else {
      if (iVar5 < 99) {
        if (iVar5 == -0x83) {
          version_etc(stdout,&DAT_00116034,"GNU coreutils",Version,"Jay Lepreau","David MacKenzie",0
                      ,uVar6);
                    /* WARNING: Subroutine does not return */
          exit(0);
        }
        if (iVar5 == -0x82) {
          uVar6 = usage();
          goto LAB_00102a30;
        }
LAB_00102a9b:
        usage();
        break;
      }
LAB_00102a30:
      if ((int)uVar6 == 0x72) {
        sentinel_length = 0;
      }
      else {
        if ((int)uVar6 != 0x73) goto LAB_00102a9b;
        separator = optarg;
      }
    }
  }
  pcVar14 = separator;
  if (sentinel_length == 0) {
    if (*separator == '\0') {
      uVar6 = dcgettext(0,"separator cannot be empty",5);
      error(1,0,uVar6);
      goto LAB_00103001;
    }
    compiled_separator._0_8_ = 0;
    compiled_separator._8_8_ = 0;
    compiled_separator._32_8_ = compiled_separator_fastmap;
    compiled_separator._40_8_ = 0;
    sVar10 = strlen(separator);
    lVar16 = rpl_re_compile_pattern(pcVar14,sVar10,compiled_separator);
    uVar7 = match_length;
    uVar1 = sentinel_length;
    if (lVar16 != 0) {
      cVar15 = '\x01';
      uVar7 = error(1,0,"%s",lVar16);
LAB_00102bc1:
      if (cVar15 != '\0') {
        read_size = uVar7;
      }
      goto LAB_00102b4a;
    }
  }
  else {
    uVar7 = 1;
    uVar1 = 1;
    if (*separator != '\0') {
      uVar7 = strlen(separator);
      uVar1 = uVar7;
    }
  }
  sentinel_length = uVar1;
  match_length = uVar7;
  iVar5 = 0x33;
  cVar15 = '\0';
  read_size = 0x2000;
  for (uVar7 = 0x2000; uVar7 >> 1 <= sentinel_length; uVar7 = uVar7 * 2) {
    iVar5 = iVar5 + -1;
    if (iVar5 == 0) goto LAB_00102bc1;
    cVar15 = '\x01';
  }
  if (cVar15 != '\0') {
    read_size = uVar7;
  }
  uVar1 = sentinel_length + 1 + uVar7;
  G_buffer_size = uVar1 * 2;
  if (uVar7 < uVar1 && uVar1 < G_buffer_size) {
    __dest = (void *)xmalloc();
    uVar7 = sentinel_length;
    pvVar13 = (void *)((long)__dest + 1);
    if (sentinel_length != 0) {
      pvVar13 = memcpy(__dest,separator,sentinel_length + 1);
      pvVar13 = (void *)((long)pvVar13 + uVar7);
    }
    local_50 = default_file_list_4;
    if (optind < param_1) {
      local_50 = (undefined1 *)(param_2 + optind);
    }
    local_39 = 1;
    G_buffer = pvVar13;
    do {
      unaff_R14 = *(char **)local_50;
      if (unaff_R14 == (char *)0x0) {
        output(0,0);
        if ((have_read_stdin != '\0') && (iVar5 = close(0), iVar5 < 0)) {
          piVar12 = __errno_location();
          error(0,*piVar12,&DAT_00116133);
          local_39 = 0;
        }
                    /* WARNING: Subroutine does not return */
        exit((uint)(local_39 ^ 1));
      }
      uVar4 = strcmp(unaff_R14,"-");
      param_2 = (undefined8 *)(ulong)uVar4;
      if (uVar4 == 0) {
        param_1 = 0;
        have_read_stdin = '\x01';
        unaff_R14 = (char *)dcgettext(0,"standard input",5);
LAB_00102c5e:
        _Var8 = lseek(param_1,0,2);
        if ((_Var8 < 0) || (iVar5 = isatty(param_1), iVar5 != 0)) {
          pFVar2 = tmp_fp_2;
          if (tempfile_3 == (char *)0x0) {
            pcVar14 = getenv("TMPDIR");
            if (pcVar14 == (char *)0x0) {
              pcVar14 = "/tmp";
            }
            tempfile_3 = (char *)mfile_name_concat(pcVar14,"tacXXXXXX",0);
            iVar5 = mkstemp_safer();
            if (iVar5 < 0) {
              uVar6 = quotearg_style(4,pcVar14);
              uVar11 = dcgettext(0,"failed to create temporary file in %s",5);
              piVar12 = __errno_location();
              error(0,*piVar12,uVar11,uVar6);
            }
            else {
              tmp_fp_2 = fdopen(iVar5,"w+");
              if (tmp_fp_2 != (FILE *)0x0) {
                unlink(tempfile_3);
                goto LAB_00102cd1;
              }
              uVar6 = quotearg_style(4,tempfile_3);
              uVar11 = dcgettext(0,"failed to open %s for writing",5);
              piVar12 = __errno_location();
              error(0,*piVar12,uVar11,uVar6);
              close(iVar5);
              unlink(tempfile_3);
            }
            bVar3 = 0;
            free(tempfile_3);
            tempfile_3 = (char *)0x0;
          }
          else {
            clearerr_unlocked(tmp_fp_2);
            iVar5 = rpl_fseeko(pFVar2,0,0);
            if (iVar5 < 0) {
LAB_00102e64:
              uVar6 = quotearg_style(4,tempfile_3);
              pcVar14 = "failed to rewind stream for %s";
            }
            else {
              iVar5 = fileno(tmp_fp_2);
              iVar5 = ftruncate(iVar5,0);
              if (iVar5 < 0) goto LAB_00102e64;
LAB_00102cd1:
              pcVar14 = tempfile_3;
              pFVar2 = tmp_fp_2;
              lVar16 = 0;
              while (sVar10 = safe_read(param_1,G_buffer,read_size), sVar10 != 0) {
                if (sVar10 == 0xffffffffffffffff) {
                  uVar6 = quotearg_n_style_colon(0,3,unaff_R14);
                  pcVar14 = "%s: read error";
                  goto LAB_00102e1a;
                }
                sVar9 = fwrite_unlocked(G_buffer,1,sVar10,pFVar2);
                if (sVar10 != sVar9) goto LAB_00102e42;
                lVar16 = lVar16 + sVar10;
              }
              iVar5 = fflush_unlocked(pFVar2);
              if (iVar5 == 0) {
                if (lVar16 < 0) {
LAB_00103001:
                  uVar4 = (uint)param_2;
                  bVar3 = 0;
                }
                else {
                  iVar5 = fileno(pFVar2);
                  bVar3 = tac_seekable(iVar5,pcVar14,lVar16);
                }
                goto LAB_00102d66;
              }
LAB_00102e42:
              uVar6 = quotearg_n_style_colon(0,3,pcVar14);
              pcVar14 = "%s: write error";
            }
LAB_00102e1a:
            uVar11 = dcgettext(0,pcVar14,5);
            piVar12 = __errno_location();
            bVar3 = 0;
            error(0,*piVar12,uVar11,uVar6);
          }
        }
        else {
          bVar3 = tac_seekable(param_1,unaff_R14,_Var8);
        }
LAB_00102d66:
        if ((uVar4 != 0) && (iVar5 = close(param_1), iVar5 != 0)) {
          uVar6 = quotearg_n_style_colon(0,3,unaff_R14);
          pcVar14 = "%s: read error";
          goto LAB_00102dac;
        }
      }
      else {
        param_1 = open(unaff_R14,0);
        if (-1 < param_1) goto LAB_00102c5e;
        uVar6 = quotearg_style(4,unaff_R14);
        pcVar14 = "failed to open %s for reading";
LAB_00102dac:
        uVar11 = dcgettext(0,pcVar14,5);
        piVar12 = __errno_location();
        bVar3 = 0;
        error(0,*piVar12,uVar11,uVar6);
      }
      local_39 = local_39 & bVar3;
      local_50 = (undefined1 *)((long)local_50 + 8);
    } while( true );
  }
LAB_00102b4a:
                    /* WARNING: Subroutine does not return */
  xalloc_die();
}