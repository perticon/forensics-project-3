uint32_t wc(int32_t edi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void* rsp7;
    void* r11_8;
    void* rsp9;
    int32_t v10;
    void** v11;
    int64_t rax12;
    int64_t v13;
    void** v14;
    void** rax15;
    void** v16;
    uint64_t rax17;
    void* rsp18;
    uint32_t r12d19;
    uint32_t eax20;
    unsigned char v21;
    uint32_t eax22;
    int1_t zf23;
    uint32_t r15d24;
    int64_t rdi25;
    void** rcx26;
    int1_t zf27;
    int64_t rdi28;
    int64_t rdi29;
    uint64_t rax30;
    void* rbp31;
    void** r13_32;
    void** v33;
    void* r12_34;
    void** rsi35;
    void** rdi36;
    void** rax37;
    void** r14_38;
    void** rdx39;
    void** rdi40;
    void** v41;
    int32_t eax42;
    void*** rax43;
    int64_t rax44;
    uint32_t eax45;
    int32_t eax46;
    void** r14_47;
    uint32_t r15d48;
    void** rbp49;
    void* rbx50;
    void** r12_51;
    void* v52;
    void** v53;
    void** rdi54;
    void** rdx55;
    void** rax56;
    void** r13_57;
    uint32_t r9d58;
    void** r15_59;
    void** rcx60;
    uint32_t eax61;
    int64_t rax62;
    uint32_t r14d63;
    int32_t v64;
    int32_t eax65;
    void*** rax66;
    uint32_t eax67;
    void** r14_68;
    void** rax69;
    uint32_t eax70;
    void** rdx71;
    int64_t r14_72;
    int64_t rax73;
    unsigned char v74;
    int32_t eax75;
    int1_t zf76;
    int64_t rdi77;
    int64_t rax78;
    int32_t eax79;
    uint32_t eax80;
    uint32_t eax81;
    void* r12_82;
    void* rbx83;
    void** rcx84;
    void** rbx85;
    uint32_t eax86;
    int1_t below_or_equal87;
    void** tmp64_88;
    void** tmp64_89;
    void** tmp64_90;
    void** tmp64_91;
    int1_t cf92;
    int64_t rax93;
    int64_t v94;
    uint32_t eax95;
    int64_t rax96;
    int64_t rax97;
    int1_t zf98;
    void** eax99;
    int1_t zf100;
    int64_t rax101;
    void** r12_102;
    uint64_t r13_103;
    int64_t rdi104;
    void** rax105;
    uint64_t rsi106;
    uint64_t rcx107;
    void** r12_108;
    int64_t rdi109;
    void** rax110;
    int32_t ebx111;
    void** rbp112;
    int64_t rdi113;
    void** rdi114;
    void** rax115;
    int64_t rsi116;
    int1_t zf117;
    int1_t zf118;

    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8);
    r11_8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 0x4000);
    do {
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - reinterpret_cast<int64_t>("_full"));
    } while (rsp7 != r11_8);
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 0x78);
    v10 = edi;
    v11 = rsi;
    rax12 = g28;
    v13 = rax12;
    v14 = rsi;
    if (!rsi) {
        rax15 = fun_2530();
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        v14 = rax15;
    }
    v16 = reinterpret_cast<void**>(0);
    rax17 = fun_2540();
    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
    r12d19 = print_bytes;
    if (rax17 <= 1) {
        eax20 = print_chars;
        v21 = 0;
        if (!*reinterpret_cast<signed char*>(&r12d19)) {
            r12d19 = eax20;
        }
    } else {
        eax22 = print_chars;
        v21 = *reinterpret_cast<unsigned char*>(&eax22);
    }
    zf23 = print_words == 0;
    if (!zf23 || (r15d24 = print_linelength, !!*reinterpret_cast<signed char*>(&r15d24))) {
        *reinterpret_cast<int32_t*>(&rdi25) = v10;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi25) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx26) = 2;
        *reinterpret_cast<int32_t*>(&rcx26 + 4) = 0;
        fdadvise(rdi25);
        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
    } else {
        if (*reinterpret_cast<signed char*>(&r12d19) == 1 && !v21) {
            zf27 = print_lines == 0;
            if (zf27) 
                goto addr_38db_14;
            *reinterpret_cast<int32_t*>(&rdi28) = v10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi28) + 4) = 0;
            fdadvise(rdi28);
            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
            goto addr_4103_16;
        }
        *reinterpret_cast<int32_t*>(&rdi29) = v10;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi29) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx26) = 2;
        *reinterpret_cast<int32_t*>(&rcx26 + 4) = 0;
        fdadvise(rdi29);
        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
        if (v21 != 1) 
            goto addr_4218_18;
    }
    addr_3a8a_19:
    rax30 = fun_2540();
    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
    if (rax30 <= 1) {
        *reinterpret_cast<int32_t*>(&rbp31) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp31) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r13_32) = 0;
        *reinterpret_cast<int32_t*>(&r13_32 + 4) = 0;
        v33 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp18) + 96);
        *reinterpret_cast<uint32_t*>(&r12_34) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_34) + 4) = 0;
        while (rsi35 = v33, *reinterpret_cast<int32_t*>(&rdi36) = v10, *reinterpret_cast<int32_t*>(&rdi36 + 4) = 0, rax37 = safe_read(rdi36, rsi35, 0x4000, 2), !!rax37) {
            if (rax37 == 0xffffffffffffffff) 
                goto addr_3ff0_23;
            v16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v16) + reinterpret_cast<unsigned char>(rax37));
            r14_38 = v33;
            rdx39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_38) + reinterpret_cast<unsigned char>(rax37));
            while (*reinterpret_cast<uint32_t*>(&rdi40) = reinterpret_cast<unsigned char>(v41), *reinterpret_cast<int32_t*>(&rdi40 + 4) = 0, ++r14_38, eax42 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rdi40 + 0xfffffffffffffff7)), *reinterpret_cast<unsigned char*>(&eax42) > 23) {
                rax43 = fun_2870(rdi40, rsi35, rdx39, 2);
                rdx39 = rdx39;
                *reinterpret_cast<uint32_t*>(&rax44) = *reinterpret_cast<unsigned char*>(&rdi40);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax44) + 4) = 0;
                rsi35 = *rax43;
                eax45 = reinterpret_cast<uint16_t>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rsi35) + reinterpret_cast<uint64_t>(rax44 * 2)));
                if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax45) + 1) & 64)) {
                    addr_3ef8_27:
                    if (r14_38 != rdx39) 
                        continue; else 
                        goto addr_3f01_28;
                } else {
                    ++r13_32;
                    if (*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax45) + 1) & 32) 
                        goto addr_3f20_30;
                    eax46 = fun_2770(rax44);
                    *reinterpret_cast<uint32_t*>(&rsi35) = posixly_correct;
                    *reinterpret_cast<int32_t*>(&rsi35 + 4) = 0;
                    rdx39 = rdx39;
                    if (!*reinterpret_cast<unsigned char*>(&rsi35)) 
                        goto addr_3ed3_32;
                }
                *reinterpret_cast<uint32_t*>(&r12_34) = *reinterpret_cast<unsigned char*>(&rsi35);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_34) + 4) = 0;
                goto addr_3ef8_27;
                addr_3ed3_32:
                if (eax46 == 0xa0 || (eax46 == 0x2007 || (eax46 == 0x202f || eax46 == 0x2060))) {
                    addr_3f20_30:
                    rbp31 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp31) + reinterpret_cast<uint64_t>(r12_34));
                    *reinterpret_cast<uint32_t*>(&r12_34) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_34) + 4) = 0;
                    goto addr_3ef8_27;
                } else {
                    *reinterpret_cast<uint32_t*>(&r12_34) = 1;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_34) + 4) = 0;
                    goto addr_3ef8_27;
                }
            }
            goto addr_3e6f_35;
            addr_3f01_28:
        }
    } else {
        *reinterpret_cast<int32_t*>(&r14_47) = 0;
        *reinterpret_cast<int32_t*>(&r14_47 + 4) = 0;
        r15d48 = 0;
        *reinterpret_cast<int32_t*>(&rbp49) = 0;
        *reinterpret_cast<int32_t*>(&rbp49 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rbx50) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx50) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r12_51) = 0;
        *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
        v52 = reinterpret_cast<void*>(0);
        v53 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp18) + 96);
        while (*reinterpret_cast<int32_t*>(&rdi54) = v10, *reinterpret_cast<int32_t*>(&rdi54 + 4) = 0, rdx55 = reinterpret_cast<void**>(0x4000 - reinterpret_cast<unsigned char>(r14_47)), rax56 = safe_read(rdi54, reinterpret_cast<unsigned char>(v53) + reinterpret_cast<unsigned char>(r14_47), rdx55, rcx26), rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8), !!rax56) {
            if (rax56 == 0xffffffffffffffff) 
                goto addr_3f94_39;
            v16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v16) + reinterpret_cast<unsigned char>(rax56));
            r13_57 = v53;
            r9d58 = r15d48;
            r15_59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_47) + reinterpret_cast<unsigned char>(rax56));
            do {
                if (!*reinterpret_cast<unsigned char*>(&r9d58) && (*reinterpret_cast<uint32_t*>(&rcx60) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_57)), *reinterpret_cast<int32_t*>(&rcx60 + 4) = 0, eax61 = *reinterpret_cast<uint32_t*>(&rcx60), *reinterpret_cast<unsigned char*>(&eax61) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax61) >> 5), *reinterpret_cast<uint32_t*>(&rax62) = eax61 & 7, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax62) + 4) = 0, r14d63 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(0x9a00) + reinterpret_cast<uint64_t>(rax62 * 4))) >> *reinterpret_cast<unsigned char*>(&rcx60) & 1, !!r14d63)) {
                    v64 = *reinterpret_cast<signed char*>(&rcx60);
                    eax65 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rcx60 + 0xfffffffffffffff7));
                    if (*reinterpret_cast<unsigned char*>(&eax65) <= 23) 
                        goto addr_3b4d_43;
                    rax66 = fun_2870(rdi54, 0x9a00, rdx55, rcx60);
                    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                    *reinterpret_cast<uint32_t*>(&rcx26) = *reinterpret_cast<unsigned char*>(&rcx60);
                    *reinterpret_cast<int32_t*>(&rcx26 + 4) = 0;
                    r9d58 = *reinterpret_cast<unsigned char*>(&r9d58);
                    *reinterpret_cast<int32_t*>(&rdx55) = 1;
                    *reinterpret_cast<int32_t*>(&rdx55 + 4) = 0;
                    eax67 = reinterpret_cast<uint16_t>(*reinterpret_cast<void***>(*rax66 + reinterpret_cast<unsigned char>(rcx26) * 2));
                    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax67) + 1) & 64)) 
                        goto addr_3ba0_45;
                    ++rbp49;
                    if (*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax67) + 1) & 32) 
                        goto addr_3bd8_47;
                    *reinterpret_cast<uint32_t*>(&rbx50) = *reinterpret_cast<unsigned char*>(&r14d63);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx50) + 4) = 0;
                    goto addr_3ba0_45;
                }
                r14_68 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp18) + 88);
                rdx55 = r15_59;
                rdi54 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp18) + 68);
                rcx26 = r14_68;
                rax69 = rpl_mbrtowc(rdi54, r13_57, rdx55, rcx26);
                rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                if (rax69 == 0xfffffffffffffffe) 
                    break;
                if (rax69 != 0xffffffffffffffff) 
                    goto addr_3c56_51;
                ++r13_57;
                --r15_59;
                r9d58 = 1;
                continue;
                addr_3c56_51:
                eax70 = fun_2830(r14_68, r13_57, rdx55, rcx26);
                rdx71 = rax69;
                if (rdx71) {
                    *reinterpret_cast<int32_t*>(&r14_72) = v64;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_72) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rax73) = static_cast<uint32_t>(r14_72 - 9);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax73) + 4) = 0;
                    if (*reinterpret_cast<uint32_t*>(&rax73) <= 23) 
                        goto addr_3d42_54;
                    *reinterpret_cast<int32_t*>(&rdi54) = *reinterpret_cast<int32_t*>(&r14_72);
                    *reinterpret_cast<int32_t*>(&rdi54 + 4) = 0;
                } else {
                    v64 = 0;
                    *reinterpret_cast<int32_t*>(&rdi54) = 0;
                    *reinterpret_cast<int32_t*>(&rdi54 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&r14_72) = 0;
                    *reinterpret_cast<int32_t*>(&rdx71) = 1;
                    *reinterpret_cast<int32_t*>(&rdx71 + 4) = 0;
                }
                v74 = reinterpret_cast<uint1_t>(eax70 == 0);
                eax75 = fun_2840(rdi54, r13_57);
                rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 + 8);
                rdx55 = rdx71;
                r9d58 = v74;
                if (!eax75) {
                    addr_3ba0_45:
                    r13_57 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_57) + reinterpret_cast<unsigned char>(rdx55));
                    r15_59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_59) - reinterpret_cast<unsigned char>(rdx55));
                    ++r12_51;
                    continue;
                } else {
                    zf76 = print_linelength == 0;
                    if (!zf76) {
                        *reinterpret_cast<int32_t*>(&rdi77) = *reinterpret_cast<int32_t*>(&r14_72);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi77) + 4) = 0;
                        rax78 = fun_26e0(rdi77, r13_57);
                        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                        r9d58 = v74;
                        rdx55 = rdx71;
                        rcx26 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax78)) + reinterpret_cast<unsigned char>(rbp49));
                        if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax78) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax78) == 0))) {
                            rbp49 = rcx26;
                        }
                    }
                    *reinterpret_cast<int32_t*>(&rdi54) = v64;
                    *reinterpret_cast<int32_t*>(&rdi54 + 4) = 0;
                    eax79 = fun_2760(rdi54, r13_57);
                    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                    rdx55 = rdx55;
                    r9d58 = *reinterpret_cast<unsigned char*>(&r9d58);
                    if (eax79) 
                        goto addr_3bd8_47;
                    eax80 = posixly_correct;
                    if (!*reinterpret_cast<unsigned char*>(&eax80)) 
                        goto addr_3cf1_64;
                }
                *reinterpret_cast<uint32_t*>(&rbx50) = *reinterpret_cast<unsigned char*>(&eax80);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx50) + 4) = 0;
                goto addr_3ba0_45;
                addr_3cf1_64:
                if (v64 == 0xa0 || (v64 == 0x2007 || (v64 == 0x202f || v64 == 0x2060))) {
                    addr_3bd8_47:
                    v52 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v52) + reinterpret_cast<uint64_t>(rbx50));
                    *reinterpret_cast<uint32_t*>(&rbx50) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx50) + 4) = 0;
                    goto addr_3ba0_45;
                } else {
                    *reinterpret_cast<uint32_t*>(&rbx50) = 1;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx50) + 4) = 0;
                    goto addr_3ba0_45;
                }
            } while (r15_59);
            goto addr_3bb3_68;
            r14_47 = r15_59;
            if (r15_59) {
                if (reinterpret_cast<int1_t>(r15_59 == 0x4000)) {
                    ++r13_57;
                    *reinterpret_cast<int32_t*>(&r14_47) = 0x3fff;
                    *reinterpret_cast<int32_t*>(&r14_47 + 4) = 0;
                }
                *reinterpret_cast<uint32_t*>(&rcx26) = 0x4001;
                *reinterpret_cast<int32_t*>(&rcx26 + 4) = 0;
                fun_2690(v53, r13_57, r14_47, 0x4001);
                rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
            }
            r15d48 = 1;
            continue;
            addr_3bb3_68:
            r14_47 = r15_59;
            r15d48 = r9d58;
        }
        goto addr_3f73_74;
    }
    eax81 = *reinterpret_cast<uint32_t*>(&r12_34);
    r12_82 = rbp31;
    rbp49 = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint32_t*>(&rbx83) = *reinterpret_cast<unsigned char*>(&eax81);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx83) + 4) = 0;
    addr_3fd9_76:
    rcx84 = v16;
    if (reinterpret_cast<unsigned char>(0) < reinterpret_cast<unsigned char>(r13_32)) {
        rbp49 = r13_32;
    }
    rbx85 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbx83) + reinterpret_cast<uint64_t>(r12_82));
    *reinterpret_cast<int32_t*>(&r12_51) = 0;
    *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
    while (1) {
        addr_39e8_79:
        eax86 = v21;
        below_or_equal87 = print_chars <= *reinterpret_cast<unsigned char*>(&eax86);
        if (!below_or_equal87) {
            r12_51 = rcx84;
        }
        write_counts(0, rbx85, r12_51, rcx84, rbp49, v11);
        tmp64_88 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(total_words) + reinterpret_cast<unsigned char>(rbx85));
        total_words = tmp64_88;
        tmp64_89 = total_lines;
        total_lines = tmp64_89;
        tmp64_90 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(total_chars) + reinterpret_cast<unsigned char>(r12_51));
        total_chars = tmp64_90;
        tmp64_91 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(total_bytes) + reinterpret_cast<unsigned char>(v16));
        total_bytes = tmp64_91;
        cf92 = reinterpret_cast<unsigned char>(max_line_length) < reinterpret_cast<unsigned char>(rbp49);
        if (cf92) {
            max_line_length = rbp49;
        }
        rax93 = v13 - g28;
        if (!rax93) 
            break;
        fun_2560();
        addr_40dd_85:
        rcx84 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx84) - reinterpret_cast<unsigned char>(rbp49));
        addr_40c1_86:
        v16 = rcx84;
        *reinterpret_cast<int32_t*>(&rbp49) = 0;
        *reinterpret_cast<int32_t*>(&rbp49 + 4) = 0;
        *reinterpret_cast<int32_t*>(&r12_51) = 0;
        *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rbx85) = 0;
        *reinterpret_cast<int32_t*>(&rbx85 + 4) = 0;
    }
    goto v94;
    addr_3ff0_23:
    eax95 = *reinterpret_cast<uint32_t*>(&r12_34);
    r12_82 = rbp31;
    rbp49 = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint32_t*>(&rbx83) = *reinterpret_cast<unsigned char*>(&eax95);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx83) + 4) = 0;
    quotearg_n_style_colon();
    fun_2490();
    fun_2780();
    goto addr_3fd9_76;
    addr_3e6f_35:
    *reinterpret_cast<uint32_t*>(&rax96) = *reinterpret_cast<unsigned char*>(&eax42);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax96) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x92c8 + rax96 * 4) + 0x92c8;
    addr_3f73_74:
    addr_3f79_88:
    rcx84 = v16;
    if (reinterpret_cast<unsigned char>(0) >= reinterpret_cast<unsigned char>(rbp49)) {
        rbp49 = reinterpret_cast<void**>(0);
    }
    rbx85 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbx50) + reinterpret_cast<uint64_t>(v52));
    goto addr_39e8_79;
    addr_3f94_39:
    quotearg_n_style_colon();
    fun_2490();
    fun_2780();
    goto addr_3f79_88;
    addr_3d42_54:
    goto *reinterpret_cast<int32_t*>(0x9268 + rax73 * 4) + 0x9268;
    addr_3b4d_43:
    *reinterpret_cast<uint32_t*>(&rax97) = *reinterpret_cast<unsigned char*>(&eax65);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax97) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x9208 + rax97 * 4) + 0x9208;
    addr_4218_18:
    if (*reinterpret_cast<signed char*>(&r12d19)) {
        addr_4103_16:
        zf98 = print_lines == 0;
        if (zf98) {
            addr_38db_14:
            eax99 = *reinterpret_cast<void***>(rdx);
            if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(eax99) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(eax99 == 0))) {
                eax99 = fun_2850();
                rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                *reinterpret_cast<void***>(rdx) = eax99;
            }
        } else {
            addr_4110_92:
            if (!__intrinsic()) {
                addr_4185_93:
                zf100 = debug == 0;
                if (zf100) {
                    addr_4155_94:
                    rax101 = wc_lines_p;
                    goto addr_415c_95;
                } else {
                    goto addr_419a_97;
                }
            } else {
                if (!(__intrinsic() & 0x8000000)) 
                    goto addr_4140_99;
                if (__intrinsic() <= 6) 
                    goto addr_4185_93; else 
                    goto addr_4132_101;
            }
        }
    } else {
        if (!v21) 
            goto addr_4110_92;
        goto addr_3a8a_19;
    }
    if (!eax99 && ((*reinterpret_cast<uint32_t*>(rdx + 32) & 0xd000) == 0x8000 && (r12_102 = *reinterpret_cast<void***>(rdx + 56), reinterpret_cast<signed char>(r12_102) >= reinterpret_cast<signed char>(0)))) {
        r13_103 = page_size;
        rcx84 = r12_102;
        if (rcx == 0xffffffffffffffff) {
            *reinterpret_cast<int32_t*>(&rdi104) = v10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi104) + 4) = 0;
            rax105 = fun_25c0(rdi104);
            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
            rcx84 = r12_102;
            rbp49 = rax105;
            if (!(reinterpret_cast<unsigned char>(rcx84) % r13_103)) {
                rsi106 = reinterpret_cast<uint64_t>(~reinterpret_cast<unsigned char>(rbp49)) >> 63;
            } else {
                if (reinterpret_cast<unsigned char>(rbp49) <= reinterpret_cast<unsigned char>(rcx84)) 
                    goto addr_40dd_85;
                *reinterpret_cast<int32_t*>(&rcx84) = 0;
                *reinterpret_cast<int32_t*>(&rcx84 + 4) = 0;
                goto addr_40c1_86;
            }
        } else {
            if (reinterpret_cast<unsigned char>(r12_102) % r13_103) 
                goto addr_40c1_86;
            *reinterpret_cast<int32_t*>(&rsi106) = 1;
            *reinterpret_cast<int32_t*>(&rbp49) = 0;
            *reinterpret_cast<int32_t*>(&rbp49 + 4) = 0;
        }
        *reinterpret_cast<int32_t*>(&rcx107) = 0x201;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx107) + 4) = 0;
        if (reinterpret_cast<uint64_t>(*reinterpret_cast<int64_t*>(rdx + 64) - 1) <= 0x1fffffffffffffff) {
            rcx107 = reinterpret_cast<uint64_t>(*reinterpret_cast<int64_t*>(rdx + 64) + 1);
        }
        r12_108 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_102) - reinterpret_cast<unsigned char>(r12_102) % rcx107);
        if (reinterpret_cast<signed char>(r12_108) > reinterpret_cast<signed char>(rbp49) && (*reinterpret_cast<signed char*>(&rsi106) && (*reinterpret_cast<int32_t*>(&rdi109) = v10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi109) + 4) = 0, rax110 = fun_25c0(rdi109, rdi109), rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8), reinterpret_cast<signed char>(rax110) >= reinterpret_cast<signed char>(0)))) {
            v16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_108) - reinterpret_cast<unsigned char>(rbp49));
        }
    }
    ebx111 = v10;
    rbp112 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp18) + 96);
    *reinterpret_cast<int32_t*>(&rdi113) = ebx111;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi113) + 4) = 0;
    fdadvise(rdi113);
    while (*reinterpret_cast<int32_t*>(&rdi114) = ebx111, *reinterpret_cast<int32_t*>(&rdi114 + 4) = 0, rax115 = safe_read(rdi114, rbp112, 0x4000, 2), !!rax115) {
        if (rax115 == 0xffffffffffffffff) 
            goto addr_402f_119;
        v16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v16) + reinterpret_cast<unsigned char>(rax115));
    }
    rcx84 = v16;
    r12_51 = rax115;
    *reinterpret_cast<int32_t*>(&rbp49) = 0;
    *reinterpret_cast<int32_t*>(&rbp49 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rbx85) = 0;
    *reinterpret_cast<int32_t*>(&rbx85 + 4) = 0;
    goto addr_39e8_79;
    addr_402f_119:
    *reinterpret_cast<int32_t*>(&rbp49) = 0;
    *reinterpret_cast<int32_t*>(&rbp49 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rbx85) = 0;
    *reinterpret_cast<int32_t*>(&rbx85 + 4) = 0;
    quotearg_n_style_colon();
    fun_2490();
    *reinterpret_cast<int32_t*>(&r12_51) = 0;
    *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
    fun_2780();
    rcx84 = v16;
    goto addr_39e8_79;
    addr_415c_95:
    *reinterpret_cast<int32_t*>(&rsi116) = v10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi116) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rbp49) = 0;
    *reinterpret_cast<int32_t*>(&rbp49 + 4) = 0;
    *reinterpret_cast<int32_t*>(&r12_51) = 0;
    *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rbx85) = 0;
    *reinterpret_cast<int32_t*>(&rbx85 + 4) = 0;
    rax101(v14, rsi116, reinterpret_cast<int64_t>(rsp18) + 72, reinterpret_cast<int64_t>(rsp18) + 80);
    rcx84 = reinterpret_cast<void**>(0);
    goto addr_39e8_79;
    addr_419a_97:
    fun_2530();
    fun_2780();
    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 + 8);
    goto addr_4155_94;
    addr_4132_101:
    if (__intrinsic() & 32) {
        zf117 = debug == 0;
        if (!zf117) {
            fun_2530();
            fun_2780();
            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 + 8);
        }
        rax101 = 0x8720;
        wc_lines_p = 0x8720;
        goto addr_415c_95;
    } else {
        addr_4140_99:
        zf118 = debug == 0;
        if (!zf118) 
            goto addr_419a_97; else 
            goto addr_4155_94;
    }
}