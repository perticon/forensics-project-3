uint wc(undefined4 param_1,long param_2,int *param_3,long param_4)

{
  undefined uVar1;
  uint *puVar2;
  wint_t wVar3;
  int iVar4;
  size_t sVar5;
  __off_t _Var6;
  ulong uVar7;
  long lVar8;
  ushort **ppuVar9;
  undefined8 uVar10;
  int *piVar11;
  ulong uVar12;
  ulong uVar13;
  undefined *puVar14;
  undefined *puVar15;
  ulong uVar16;
  char *pcVar17;
  wchar_t __c;
  byte bVar18;
  long lVar19;
  byte *pbVar20;
  ulong uVar21;
  undefined *puVar22;
  uint uVar23;
  long in_FS_OFFSET;
  char cVar24;
  undefined local_4030 [16384];
  
  puVar14 = &stack0xffffffffffffffd0;
  do {
    puVar15 = puVar14;
    *(undefined8 *)(puVar15 + -0x1000) = *(undefined8 *)(puVar15 + -0x1000);
    puVar14 = puVar15 + -0x1000;
  } while (puVar15 + -0x1000 != local_4030);
  *(undefined4 *)(puVar15 + -0x1050) = param_1;
  *(long *)(puVar15 + -0x1048) = param_2;
  *(undefined8 *)(puVar15 + 0x2ff0) = *(undefined8 *)(in_FS_OFFSET + 0x28);
  *(long *)(puVar15 + -0x1040) = param_2;
  if (param_2 == 0) {
    *(undefined8 *)(puVar15 + -0x1080) = 0x103f69;
    uVar10 = dcgettext(0,"standard input",5);
    *(undefined8 *)(puVar15 + -0x1040) = uVar10;
  }
  *(undefined8 *)(puVar15 + -0x1028) = 0;
  *(undefined8 *)(puVar15 + -0x1030) = 0;
  *(undefined8 *)(puVar15 + -0x1080) = 0x10387e;
  sVar5 = __ctype_get_mb_cur_max();
  bVar18 = print_bytes;
  if (sVar5 < 2) {
    puVar15[-0x1049] = 0;
    if (print_bytes == 0) {
      bVar18 = print_chars;
    }
  }
  else {
    puVar15[-0x1049] = print_chars;
  }
  if ((print_words == '\0') && (uVar23 = (uint)print_linelength, print_linelength == 0)) {
    if ((bVar18 == 1) && (puVar15[-0x1049] == '\0')) {
      if (print_lines != '\0') {
        *(undefined8 *)(puVar15 + -0x1080) = 0x104103;
        fdadvise(*(int *)(puVar15 + -0x1050),0,0,2);
LAB_00104103:
        if (print_lines != '\0') {
LAB_00104110:
          piVar11 = (int *)cpuid_basic_info(0);
          uVar23 = piVar11[3];
          if (*piVar11 == 0) {
LAB_00104185:
            if (debug != '\0') {
              pcVar17 = "failed to get cpuid";
LAB_0010419a:
              *(undefined8 *)(puVar15 + -0x1080) = 0x1041a1;
              uVar10 = dcgettext(0,pcVar17,5,uVar23);
              *(undefined8 *)(puVar15 + -0x1080) = 0x1041b6;
              error(0,0,"%s",uVar10);
            }
          }
          else {
            lVar19 = cpuid_Version_info(1);
            uVar23 = *(uint *)(lVar19 + 0xc) & 0x8000000;
            if (uVar23 == 0) {
LAB_00104140:
              pcVar17 = "avx2 support not detected";
              if (debug != '\0') goto LAB_0010419a;
            }
            else {
              puVar2 = (uint *)cpuid_basic_info(0);
              uVar23 = puVar2[3];
              if (*puVar2 < 7) goto LAB_00104185;
              lVar19 = cpuid_Extended_Feature_Enumeration_info(7);
              uVar23 = *(uint *)(lVar19 + 0xc);
              if ((*(uint *)(lVar19 + 4) & 0x20) == 0) goto LAB_00104140;
              if (debug != '\0') {
                *(undefined8 *)(puVar15 + -0x1080) = 0x1041e4;
                uVar10 = dcgettext(0,"using avx2 hardware support",5,uVar23);
                *(undefined8 *)(puVar15 + -0x1080) = 0x1041f9;
                error(0,0,"%s",uVar10);
              }
              wc_lines_p = wc_lines_avx2;
            }
          }
          uVar16 = 0;
          uVar7 = 0;
          lVar19 = 0;
          *(undefined8 *)(puVar15 + -0x1080) = 0x104178;
          uVar23 = (*(code *)wc_lines_p)
                             (*(undefined8 *)(puVar15 + -0x1040),*(undefined4 *)(puVar15 + -0x1050),
                              puVar15 + -0x1030,puVar15 + -0x1028);
          uVar13 = *(ulong *)(puVar15 + -0x1028);
          goto LAB_001039e8;
        }
      }
      iVar4 = *param_3;
      if (0 < iVar4) {
        *(undefined8 *)(puVar15 + -0x1080) = 0x10407b;
        iVar4 = fstat(*(int *)(puVar15 + -0x1050),(stat *)(param_3 + 2));
        *param_3 = iVar4;
      }
      uVar16 = page_size;
      if (((iVar4 == 0) && ((param_3[8] & 0xd000U) == 0x8000)) &&
         (uVar13 = *(ulong *)(param_3 + 0xe), -1 < (long)uVar13)) {
        if (param_4 == -1) {
          *(ulong *)(puVar15 + -0x1070) = uVar13;
          *(undefined8 *)(puVar15 + -0x1080) = 0x1040a5;
          uVar12 = lseek(*(int *)(puVar15 + -0x1050),0,1);
          uVar7 = *(ulong *)(puVar15 + -0x1070);
          if (uVar7 % uVar16 != 0) {
            if (uVar7 < uVar12) {
              uVar13 = 0;
            }
            else {
              uVar13 = uVar7 - uVar12;
            }
            goto LAB_001040c1;
          }
          bVar18 = (byte)~(byte)(uVar12 >> 0x38) >> 7;
        }
        else {
          if (uVar13 % page_size != 0) {
LAB_001040c1:
            *(ulong *)(puVar15 + -0x1028) = uVar13;
            uVar16 = 0;
            uVar7 = 0;
            lVar19 = 0;
            uVar23 = 1;
            goto LAB_001039e8;
          }
          bVar18 = 1;
          uVar12 = 0;
        }
        uVar16 = 0x201;
        if (*(long *)(param_3 + 0x10) - 1U < 0x2000000000000000) {
          uVar16 = *(long *)(param_3 + 0x10) + 1;
        }
        lVar19 = uVar13 - uVar13 % uVar16;
        if (((long)uVar12 < lVar19) && (bVar18 != 0)) {
          *(undefined8 *)(puVar15 + -0x1080) = 0x103981;
          _Var6 = lseek(*(int *)(puVar15 + -0x1050),lVar19,1);
          if (-1 < _Var6) {
            *(ulong *)(puVar15 + -0x1028) = lVar19 - uVar12;
          }
        }
      }
      iVar4 = *(int *)(puVar15 + -0x1050);
      *(undefined8 *)(puVar15 + -0x1080) = 0x1039a7;
      fdadvise(iVar4,0,0,2);
      while( true ) {
        *(undefined8 *)(puVar15 + -0x1080) = 0x1039ce;
        uVar7 = safe_read(iVar4,puVar15 + -0x1018,0x4000);
        if (uVar7 == 0) break;
        if (uVar7 == 0xffffffffffffffff) {
          uVar16 = 0;
          lVar19 = 0;
          *(undefined8 *)(puVar15 + -0x1080) = 0x104044;
          uVar10 = quotearg_n_style_colon(0,3,*(undefined8 *)(puVar15 + -0x1040));
          *(undefined8 *)(puVar15 + -0x1080) = 0x10404c;
          piVar11 = __errno_location();
          iVar4 = *piVar11;
          uVar7 = 0;
          *(undefined8 *)(puVar15 + -0x1080) = 0x104064;
          error(0,iVar4,"%s",uVar10);
          uVar13 = *(ulong *)(puVar15 + -0x1028);
          goto LAB_001039e8;
        }
        *(ulong *)(puVar15 + -0x1028) = *(long *)(puVar15 + -0x1028) + uVar7;
      }
      uVar13 = *(ulong *)(puVar15 + -0x1028);
      uVar16 = 0;
      lVar19 = 0;
      uVar23 = 1;
      goto LAB_001039e8;
    }
    *(undefined8 *)(puVar15 + -0x1080) = 0x10420d;
    fdadvise(*(int *)(puVar15 + -0x1050),0,0,2);
    if (puVar15[-0x1049] != '\x01') {
      if (bVar18 != 0) goto LAB_00104103;
      if (puVar15[-0x1049] == '\0') goto LAB_00104110;
    }
  }
  else {
    *(undefined8 *)(puVar15 + -0x1080) = 0x103a8a;
    fdadvise(*(int *)(puVar15 + -0x1050),0,0,2);
  }
  *(undefined8 *)(puVar15 + -0x1080) = 0x103a8f;
  sVar5 = __ctype_get_mb_cur_max();
  if (sVar5 < 2) {
    uVar16 = 0;
    uVar21 = 0;
    *(undefined **)(puVar15 + -0x1058) = puVar15 + -0x1018;
    uVar12 = 0;
    uVar7 = uVar16;
    while( true ) {
      *(undefined8 *)(puVar15 + -0x1080) = 0x103e3b;
      lVar19 = safe_read(*(undefined4 *)(puVar15 + -0x1050),*(undefined8 *)(puVar15 + -0x1058),
                         0x4000);
      if (lVar19 == 0) break;
      if (lVar19 == -1) {
        uVar23 = 0;
        *(undefined8 *)(puVar15 + -0x1080) = 0x104010;
        uVar10 = quotearg_n_style_colon(0,3,*(undefined8 *)(puVar15 + -0x1040));
        *(undefined8 *)(puVar15 + -0x1080) = 0x104018;
        piVar11 = __errno_location();
        iVar4 = *piVar11;
        *(undefined8 *)(puVar15 + -0x1080) = 0x10402d;
        error(0,iVar4,"%s",uVar10);
        goto LAB_00103fd9;
      }
      *(long *)(puVar15 + -0x1028) = *(long *)(puVar15 + -0x1028) + lVar19;
      puVar22 = *(undefined **)(puVar15 + -0x1058);
      puVar14 = puVar22 + lVar19;
      do {
        uVar1 = *puVar22;
        puVar22 = puVar22 + 1;
        switch(uVar1) {
        case 9:
          uVar21 = (uVar21 & 0xfffffffffffffff8) + 8;
          break;
        case 10:
          *(long *)(puVar15 + -0x1030) = *(long *)(puVar15 + -0x1030) + 1;
        case 0xc:
        case 0xd:
          if (uVar16 < uVar21) {
            uVar16 = uVar21;
          }
          uVar21 = 0;
          break;
        case 0xb:
          break;
        default:
          *(undefined **)(puVar15 + -0x1068) = puVar14;
          puVar15[-0x1070] = uVar1;
          *(undefined8 *)(puVar15 + -0x1080) = 0x103e8f;
          ppuVar9 = __ctype_b_loc();
          puVar14 = *(undefined **)(puVar15 + -0x1068);
          bVar18 = puVar15[-0x1070];
          if (((*ppuVar9)[bVar18] & 0x4000) != 0) {
            uVar21 = uVar21 + 1;
            if (((*ppuVar9)[bVar18] & 0x2000) != 0) break;
            *(undefined **)(puVar15 + -0x1070) = puVar14;
            *(undefined8 *)(puVar15 + -0x1080) = 0x103ebe;
            wVar3 = btowc((uint)bVar18);
            puVar14 = *(undefined **)(puVar15 + -0x1070);
            if (posixly_correct == 0) {
              if (((wVar3 == 0xa0) || (wVar3 == 0x2007)) || ((wVar3 == 0x202f || (wVar3 == 0x2060)))
                 ) break;
              uVar12 = 1;
            }
            else {
              uVar12 = (ulong)posixly_correct;
            }
          }
          goto LAB_00103ef8;
        case 0x20:
          uVar21 = uVar21 + 1;
        }
        uVar7 = uVar7 + uVar12;
        uVar12 = 0;
LAB_00103ef8:
      } while (puVar22 != puVar14);
    }
    uVar23 = 1;
LAB_00103fd9:
    uVar13 = *(ulong *)(puVar15 + -0x1028);
    if (uVar16 < uVar21) {
      uVar16 = uVar21;
    }
    lVar19 = uVar12 + uVar7;
    uVar7 = 0;
  }
  else {
    lVar19 = 0;
    cVar24 = '\0';
    uVar16 = 0;
    *(undefined8 *)(puVar15 + -0x1020) = 0;
    uVar12 = 0;
    uVar7 = 0;
    *(undefined8 *)(puVar15 + -0x1060) = 0;
    *(undefined8 *)(puVar15 + -0x1068) = 0;
    *(undefined **)(puVar15 + -0x1058) = puVar15 + -0x1018;
LAB_00103ad0:
    *(undefined8 *)(puVar15 + -0x1080) = 0x103aea;
    lVar8 = safe_read(*(undefined4 *)(puVar15 + -0x1050),*(long *)(puVar15 + -0x1058) + lVar19,
                      0x4000 - lVar19);
    if (lVar8 != 0) {
      if (lVar8 == -1) {
        *(undefined8 *)(puVar15 + -0x1080) = 0x103fa5;
        uVar10 = quotearg_n_style_colon(0,3,*(undefined8 *)(puVar15 + -0x1040));
        *(undefined8 *)(puVar15 + -0x1080) = 0x103fad;
        piVar11 = __errno_location();
        iVar4 = *piVar11;
        uVar23 = 0;
        *(undefined8 *)(puVar15 + -0x1080) = 0x103fc5;
        error(0,iVar4,"%s",uVar10);
        goto LAB_00103f79;
      }
      *(long *)(puVar15 + -0x1028) = *(long *)(puVar15 + -0x1028) + lVar8;
      lVar19 = lVar19 + lVar8;
      pbVar20 = *(byte **)(puVar15 + -0x1058);
      do {
        if (cVar24 != '\0') {
LAB_00103c20:
          *(undefined8 *)(puVar15 + -0x1070) = *(undefined8 *)(puVar15 + -0x1020);
          *(undefined8 *)(puVar15 + -0x1080) = 0x103c42;
          lVar8 = rpl_mbrtowc(puVar15 + -0x1034,pbVar20,lVar19);
          if (lVar8 != -2) {
            if (lVar8 != -1) {
              *(long *)(puVar15 + -0x1070) = lVar8;
              *(undefined8 *)(puVar15 + -0x1080) = 0x103c63;
              iVar4 = mbsinit((mbstate_t *)(puVar15 + -0x1020));
              lVar8 = *(long *)(puVar15 + -0x1070);
              cVar24 = iVar4 == 0;
              if (lVar8 == 0) {
                *(undefined4 *)(puVar15 + -0x1034) = 0;
                __c = L'\0';
                lVar8 = 1;
              }
              else {
                __c = *(wchar_t *)(puVar15 + -0x1034);
                switch(__c) {
                case L'\t':
                  goto switchD_00103d50_caseD_9;
                case L'\n':
                  goto switchD_00103d50_caseD_a;
                case L'\v':
                  goto switchD_00103d50_caseD_b;
                case L'\f':
                case L'\r':
                  goto switchD_00103d50_caseD_c;
                case L' ':
                  goto switchD_00103d50_caseD_20;
                }
              }
              puVar15[-0x104a] = cVar24;
              *(long *)(puVar15 + -0x1070) = lVar8;
              *(undefined8 *)(puVar15 + -0x1080) = 0x103c98;
              iVar4 = iswprint(__c);
              lVar8 = *(long *)(puVar15 + -0x1070);
              cVar24 = puVar15[-0x104a];
              if (iVar4 != 0) {
                if (print_linelength != 0) {
                  *(undefined8 *)(puVar15 + -0x1080) = 0x103de8;
                  iVar4 = wcwidth(__c);
                  cVar24 = puVar15[-0x104a];
                  lVar8 = *(long *)(puVar15 + -0x1070);
                  if (0 < iVar4) {
                    uVar16 = (long)iVar4 + uVar16;
                  }
                }
                wVar3 = *(wint_t *)(puVar15 + -0x1034);
                puVar15[-0x104a] = cVar24;
                *(long *)(puVar15 + -0x1070) = lVar8;
                *(undefined8 *)(puVar15 + -0x1080) = 0x103ccf;
                iVar4 = iswspace(wVar3);
                lVar8 = *(long *)(puVar15 + -0x1070);
                cVar24 = puVar15[-0x104a];
                if (iVar4 != 0) goto switchD_00103d50_caseD_b;
                if (posixly_correct == 0) {
                  if ((((wVar3 == 0xa0) || (wVar3 == 0x2007)) || (wVar3 == 0x202f)) ||
                     (wVar3 == 0x2060)) goto switchD_00103d50_caseD_b;
                  uVar12 = 1;
                }
                else {
                  uVar12 = (ulong)posixly_correct;
                }
              }
              goto LAB_00103ba0;
            }
            pbVar20 = pbVar20 + 1;
            lVar8 = -1;
            cVar24 = '\x01';
            goto LAB_00103baa;
          }
          *(undefined8 *)(puVar15 + -0x1020) = *(undefined8 *)(puVar15 + -0x1070);
          if (lVar19 != 0) {
            if (lVar19 == 0x4000) {
              pbVar20 = pbVar20 + 1;
              lVar19 = 0x3fff;
            }
            *(undefined8 *)(puVar15 + -0x1080) = 0x103daa;
            __memmove_chk(*(undefined8 *)(puVar15 + -0x1058),pbVar20,lVar19);
          }
          cVar24 = '\x01';
          break;
        }
        bVar18 = *pbVar20;
        uVar23 = *(uint *)(is_basic_table + (ulong)(bVar18 >> 5) * 4) >> (bVar18 & 0x1f) & 1;
        if (uVar23 == 0) goto LAB_00103c20;
        *(int *)(puVar15 + -0x1034) = (int)(char)bVar18;
        switch(bVar18) {
        case 9:
          lVar8 = 1;
switchD_00103d50_caseD_9:
          uVar16 = (uVar16 & 0xfffffffffffffff8) + 8;
          break;
        case 10:
          lVar8 = 1;
switchD_00103d50_caseD_a:
          *(long *)(puVar15 + -0x1030) = *(long *)(puVar15 + -0x1030) + 1;
          goto switchD_00103d50_caseD_c;
        case 0xb:
          lVar8 = 1;
          break;
        case 0xc:
        case 0xd:
          lVar8 = 1;
switchD_00103d50_caseD_c:
          if (uVar16 <= *(ulong *)(puVar15 + -0x1060)) {
            uVar16 = *(ulong *)(puVar15 + -0x1060);
          }
          *(ulong *)(puVar15 + -0x1060) = uVar16;
          uVar16 = 0;
          break;
        default:
          puVar15[-0x104a] = 0;
          puVar15[-0x1070] = bVar18;
          *(undefined8 *)(puVar15 + -0x1080) = 0x103b76;
          ppuVar9 = __ctype_b_loc();
          cVar24 = puVar15[-0x104a];
          lVar8 = 1;
          if (((*ppuVar9)[(byte)puVar15[-0x1070]] & 0x4000) != 0) {
            uVar16 = uVar16 + 1;
            if (((*ppuVar9)[(byte)puVar15[-0x1070]] & 0x2000) != 0) break;
            uVar12 = (ulong)uVar23;
          }
          goto LAB_00103ba0;
        case 0x20:
          lVar8 = 1;
switchD_00103d50_caseD_20:
          uVar16 = uVar16 + 1;
        }
switchD_00103d50_caseD_b:
        *(ulong *)(puVar15 + -0x1068) = *(long *)(puVar15 + -0x1068) + uVar12;
        uVar12 = 0;
LAB_00103ba0:
        pbVar20 = pbVar20 + lVar8;
        lVar8 = -lVar8;
        uVar7 = uVar7 + 1;
LAB_00103baa:
        lVar19 = lVar19 + lVar8;
      } while (lVar19 != 0);
      goto LAB_00103ad0;
    }
    uVar23 = 1;
LAB_00103f79:
    uVar13 = *(ulong *)(puVar15 + -0x1028);
    if (uVar16 <= *(ulong *)(puVar15 + -0x1060)) {
      uVar16 = *(ulong *)(puVar15 + -0x1060);
    }
    lVar19 = uVar12 + *(long *)(puVar15 + -0x1068);
  }
LAB_001039e8:
  if ((byte)puVar15[-0x1049] < print_chars) {
    uVar7 = uVar13;
  }
  *(undefined8 *)(puVar15 + -0x1080) = 0x103a0f;
  write_counts(*(undefined8 *)(puVar15 + -0x1030),lVar19,uVar7,uVar13,uVar16,
               *(undefined8 *)(puVar15 + -0x1048));
  total_words = total_words + lVar19;
  total_lines = total_lines + *(long *)(puVar15 + -0x1030);
  total_chars = total_chars + uVar7;
  total_bytes = total_bytes + *(long *)(puVar15 + -0x1028);
  if (max_line_length < uVar16) {
    max_line_length = uVar16;
  }
  if (*(long *)(puVar15 + 0x2ff0) != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    *(undefined8 *)(puVar15 + -0x1080) = 0x1040dd;
    __stack_chk_fail();
  }
  return uVar23;
}