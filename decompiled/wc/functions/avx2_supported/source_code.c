avx2_supported (void)
{
  unsigned int eax = 0;
  unsigned int ebx = 0;
  unsigned int ecx = 0;
  unsigned int edx = 0;
  bool getcpuid_ok = false;
  bool avx_enabled = false;

  if (__get_cpuid (1, &eax, &ebx, &ecx, &edx))
    {
      getcpuid_ok = true;
      if (ecx & bit_OSXSAVE)
        avx_enabled = true;  /* Support is not disabled.  */
    }


  if (avx_enabled)
    {
      eax = ebx = ecx = edx = 0;
      if (! __get_cpuid_count (7, 0, &eax, &ebx, &ecx, &edx))
        getcpuid_ok = false;
      else
        {
          if (! (ebx & bit_AVX2))
            avx_enabled = false;  /* Hardware doesn't support it.  */
        }
    }


  if (! getcpuid_ok)
    {
      if (debug)
        error (0, 0, "%s", _("failed to get cpuid"));
      return false;
    }
  else if (! avx_enabled)
    {
      if (debug)
        error (0, 0, "%s", _("avx2 support not detected"));
      return false;
    }
  else
    {
      if (debug)
        error (0, 0, "%s", _("using avx2 hardware support"));
      return true;
    }
}