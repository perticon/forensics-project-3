uint main(ulong param_1,undefined8 *param_2)

{
  byte bVar1;
  byte bVar2;
  int iVar3;
  uint uVar4;
  char *pcVar5;
  long lVar6;
  char *pcVar7;
  undefined8 uVar8;
  undefined8 uVar9;
  ulong uVar10;
  int *piVar11;
  long lVar12;
  FILE *__stream;
  ulong uVar13;
  undefined8 *puVar14;
  int iVar15;
  stat *__buf;
  long in_FS_OFFSET;
  double dVar16;
  double dVar17;
  undefined8 uVar18;
  int *local_228;
  char local_20d;
  int local_1fc;
  ulong local_1f8;
  undefined8 *local_1f0;
  stat local_d8;
  long local_40;
  
  param_1 = param_1 & 0xffffffff;
  iVar15 = (int)param_1;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils");
  textdomain("coreutils");
  atexit(close_stdout);
  iVar3 = getpagesize();
  page_size = (long)iVar3;
  setvbuf(stdout,(char *)0x0,1,0);
  pcVar5 = getenv("POSIXLY_CORRECT");
  print_bytes = 0;
  print_chars = 0;
  posixly_correct = pcVar5 != (char *)0x0;
  print_words = 0;
  print_lines = 0;
  print_linelength = 0;
  max_line_length = 0;
  total_bytes = 0;
  total_chars = 0;
  total_words = 0;
  total_lines = 0;
  pcVar5 = (char *)0x0;
  do {
    uVar18 = 0x102a03;
    iVar3 = getopt_long(param_1,param_2,"clLmw",longopts,0);
    if (iVar3 == -1) {
LAB_00102b07:
      if (print_lines != 0) goto LAB_00102b1d;
      if (print_words == 0) goto LAB_001030be;
      goto LAB_00102b1d;
    }
    if (0x81 < iVar3) break;
    if (iVar3 < 0x4c) {
      if (iVar3 == -0x83) {
        version_etc(stdout,&DAT_0010905d,"GNU coreutils",Version,"Paul Rubin","David MacKenzie",0,
                    uVar18);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar3 != -0x82) break;
      usage(0);
      goto LAB_00102b07;
    }
    switch(iVar3) {
    case 0x4c:
      print_linelength = 1;
      break;
    default:
      goto switchD_00102a34_caseD_4d;
    case 99:
      print_bytes = 1;
      break;
    case 0x6c:
      print_lines = 1;
      break;
    case 0x6d:
      print_chars = 1;
      break;
    case 0x77:
      print_words = 1;
      break;
    case 0x80:
      debug = 1;
      break;
    case 0x81:
      pcVar5 = optarg;
    }
  } while( true );
switchD_00102a34_caseD_4d:
  while( true ) {
    usage(1);
LAB_001030be:
    if (((print_chars == 0) && (print_bytes == 0)) && (print_linelength == 0)) {
      print_bytes = 1;
      print_words = 1;
      print_lines = 1;
    }
LAB_00102b1d:
    iVar3 = optind;
    if (pcVar5 == (char *)0x0) goto LAB_00102fa7;
    if (iVar15 <= optind) break;
    pcVar5 = (char *)quotearg_style(4,param_2[optind]);
    uVar18 = dcgettext(0,"extra operand %s",5);
    error(0,0,uVar18,pcVar5);
    dcgettext(0,"file operands cannot be combined with --files0-from",5);
    __fprintf_chk(stderr,1,"%s\n");
  }
  iVar3 = strcmp(pcVar5,"-");
  __stream = stdin;
  if ((iVar3 != 0) && (__stream = fopen(pcVar5,"r"), __stream == (FILE *)0x0)) {
    lVar6 = quotearg_style(4,pcVar5);
    pcVar5 = (char *)dcgettext(0,"cannot open %s for reading",5);
    piVar11 = __errno_location();
    iVar3 = 1;
    uVar10 = error(1,*piVar11,pcVar5,lVar6);
    local_1f8 = param_1;
    goto LAB_001031c1;
  }
  iVar3 = fileno(__stream);
  iVar3 = fstat(iVar3,&local_d8);
  if ((iVar3 == 0) && ((local_d8.st_mode & 0xf000) == 0x8000)) {
    dVar16 = (double)physmem_available();
    dVar17 = DAT_001099b8;
    if (dVar16 * DAT_001099c0 <= DAT_001099b8) {
      dVar17 = (double)physmem_available();
      dVar17 = DAT_001099c0 * dVar17;
    }
    if (dVar17 < (double)local_d8.st_size) goto LAB_00102b85;
    readtokens0_init(&local_1f8);
    local_20d = readtokens0(__stream,&local_1f8);
    if ((local_20d == '\0') || (iVar3 = rpl_fclose(__stream), iVar3 != 0)) {
      uVar18 = quotearg_style(4,pcVar5);
      uVar8 = dcgettext(0,"cannot read file names from %s",5);
      error(1,0,uVar8,uVar18);
LAB_0010331f:
                    /* WARNING: Subroutine does not return */
      __stack_chk_fail();
    }
    lVar6 = argv_iter_init_argv(local_1f0);
    if (lVar6 == 0) goto LAB_00103324;
    uVar10 = 1;
    if (local_1f8 != 0) {
      uVar10 = local_1f8;
    }
    local_228 = (int *)xnmalloc(uVar10,0x98);
    if (local_1f8 != 0) goto LAB_00102fe8;
  }
  else {
LAB_00102b85:
    lVar6 = argv_iter_init_stream(__stream);
    if (lVar6 == 0) goto LAB_00103324;
    local_228 = (int *)xnmalloc(1,0x98);
    local_20d = '\0';
  }
  local_1f8 = 0;
  *local_228 = 1;
  number_width = 1;
LAB_00102bc2:
  param_2 = (undefined8 *)0x0;
  bVar1 = 1;
  while (pcVar7 = (char *)argv_iter(lVar6,&local_1fc), pcVar7 != (char *)0x0) {
    if (pcVar5 == (char *)0x0) {
      if (*pcVar7 == '\0') {
        dcgettext(0,"invalid zero-length file name",5);
        error(0,0,"%s");
LAB_00102d7d:
        bVar1 = 0;
      }
      else {
LAB_00102ca3:
        puVar14 = (undefined8 *)0x0;
        if (local_1f8 != 0) {
          puVar14 = param_2;
        }
LAB_00102c1c:
        iVar3 = strcmp(pcVar7,"-");
        piVar11 = (int *)((long)puVar14 + (long)local_228);
        if (iVar3 == 0) {
          have_read_stdin = '\x01';
          bVar2 = wc(0,pcVar7);
        }
        else {
LAB_00102eb2:
          iVar3 = open(pcVar7,0);
          if (iVar3 != -1) {
            bVar2 = wc(iVar3,pcVar7,piVar11);
            iVar3 = close(iVar3);
            if (iVar3 == 0) goto LAB_00102c58;
          }
          quotearg_n_style_colon(0,3,pcVar7);
          piVar11 = __errno_location();
          error(0,*piVar11,"%s");
          bVar2 = 0;
        }
LAB_00102c58:
        bVar1 = bVar1 & bVar2;
      }
    }
    else {
      if ((*pcVar5 == '-') && (pcVar5[1] == '\0')) {
        iVar3 = strcmp(pcVar7,"-");
        if (iVar3 == 0) {
          quotearg_style(4,pcVar7);
          uVar18 = dcgettext(0,"when reading file names from stdin, no file name of %s allowed",5);
          error(0,0,uVar18);
          if (*pcVar7 != '\0') goto LAB_00102d7d;
        }
        else if (*pcVar7 != '\0') {
          puVar14 = param_2;
          piVar11 = local_228;
          if (local_1f8 != 0) goto LAB_00102c1c;
          goto LAB_00102eb2;
        }
      }
      else if (*pcVar7 != '\0') goto LAB_00102ca3;
      uVar18 = argv_iter_n_args(lVar6);
      uVar8 = dcgettext(0,"invalid zero-length file name",5);
      uVar9 = quotearg_n_style_colon(0,3,pcVar5);
      error(0,0,"%s:%lu: %s",uVar9,uVar18,uVar8);
      bVar1 = 0;
    }
    if (local_1f8 == 0) {
      *local_228 = 1;
    }
    param_2 = param_2 + 0x13;
  }
  if (local_1fc != 3) {
    if (local_1fc == 4) {
      quotearg_n_style_colon(0,3,pcVar5);
      pcVar5 = (char *)dcgettext(0,"%s: read error",5);
      piVar11 = __errno_location();
      error(0,*piVar11,pcVar5);
      uVar4 = 0;
    }
    else {
      if (local_1fc != 2) {
                    /* WARNING: Subroutine does not return */
        __assert_fail("!\"unexpected error code from argv_iter\"","src/wc.c",0x3aa,
                      (char *)&__PRETTY_FUNCTION___1);
      }
      bVar2 = pcVar5 == (char *)0x0 & bVar1;
      local_1f8 = (ulong)(uint)bVar2;
      uVar4 = (uint)bVar1;
      if ((bVar2 != 0) && (lVar12 = argv_iter_n_args(lVar6), uVar4 = (uint)bVar2, lVar12 == 0)) {
        have_read_stdin = '\x01';
        uVar4 = wc(0,0,local_228);
      }
    }
    iVar15 = (int)local_1f8;
    if (local_20d != '\0') {
      readtokens0_free(&local_1f8);
    }
    uVar10 = argv_iter_n_args(lVar6);
    if (1 < uVar10) {
      uVar18 = dcgettext(0,"total",5);
      write_counts(total_lines,total_words,total_chars,total_bytes,max_line_length,uVar18);
    }
    argv_iter_free(lVar6);
    free(local_228);
    if ((have_read_stdin == '\0') || (iVar3 = close(0), iVar3 == 0)) {
      if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
        return (uVar4 ^ 1) & 0xff;
      }
      goto LAB_0010331f;
    }
    piVar11 = __errno_location();
    iVar3 = error(1,*piVar11,&DAT_0010915d);
LAB_00102fa7:
    if (iVar3 < iVar15) {
      local_1f0 = param_2 + iVar3;
      local_1f8 = (ulong)(iVar15 - iVar3);
    }
    else {
      local_1f8 = 1;
      local_1f0 = &stdin_only_2;
    }
    lVar6 = argv_iter_init_argv(local_1f0);
    if (lVar6 == 0) goto LAB_00103324;
    local_228 = (int *)xnmalloc(local_1f8,0x98);
    local_20d = '\0';
LAB_00102fe8:
    if ((local_1f8 == 1) &&
       (iVar3 = (uint)print_lines + (uint)print_words + (uint)print_chars + (uint)print_bytes +
                (uint)print_linelength, iVar3 == 1)) {
      *local_228 = 1;
      number_width = iVar3;
    }
    else {
      uVar10 = 0;
      __buf = (stat *)(local_228 + 2);
      do {
        pcVar7 = (char *)local_1f0[uVar10];
        if ((pcVar7 == (char *)0x0) || (iVar3 = strcmp(pcVar7,"-"), iVar3 == 0)) {
          iVar3 = fstat(0,__buf);
        }
        else {
          iVar3 = stat(pcVar7,__buf);
        }
        uVar10 = uVar10 + 1;
        *(int *)(__buf[-1].__unused + 2) = iVar3;
        __buf = (stat *)&__buf[1].st_ino;
      } while (uVar10 < local_1f8);
      number_width = 1;
      if (*local_228 < 1) {
        iVar3 = 1;
        uVar10 = 0;
        uVar13 = 0;
        piVar11 = local_228;
        do {
          if (*piVar11 == 0) {
            if ((piVar11[8] & 0xf000U) == 0x8000) {
              uVar10 = uVar10 + *(long *)(piVar11 + 0xe);
            }
            else {
              iVar3 = 7;
            }
          }
          uVar13 = uVar13 + 1;
          piVar11 = piVar11 + 0x26;
        } while (uVar13 < local_1f8);
LAB_001031c1:
        number_width = 1;
        for (; 9 < uVar10; uVar10 = uVar10 / 10) {
          number_width = number_width + 1;
        }
        if (number_width < iVar3) {
          number_width = iVar3;
        }
      }
    }
    goto LAB_00102bc2;
  }
LAB_00103324:
                    /* WARNING: Subroutine does not return */
  xalloc_die();
}