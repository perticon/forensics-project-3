void write_counts(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r15_7;
    void** r14_8;
    void** r12_9;
    void** rbp10;
    void** rbx11;
    void* rsp12;
    int64_t rax13;
    int1_t zf14;
    void** rax15;
    int1_t zf16;
    void** rax17;
    int1_t zf18;
    void** rax19;
    int1_t zf20;
    void** rax21;
    int1_t zf22;
    void** rax23;
    int64_t rax24;
    void* rsp25;
    void** rax26;
    struct s0* rdi27;
    void** rax28;
    int64_t rax29;
    void* rsp30;
    void* r11_31;
    int32_t r14d32;
    void** r13_33;
    int32_t r15d34;
    void** r12_35;
    void** v36;
    void** v37;
    int64_t rax38;
    int64_t v39;
    void** v40;
    void** rdi41;
    void** rax42;
    void** rbx43;
    void* v44;
    void** rbp45;
    void** rdi46;
    void** rax47;
    uint1_t below_or_equal48;
    void** rdx49;
    void** rax50;
    signed char v51;
    uint1_t below_or_equal52;
    int64_t rdx53;
    int64_t v54;

    r15_7 = rdx;
    r14_8 = rcx;
    r12_9 = r9;
    rbp10 = reinterpret_cast<void**>("%*s");
    rbx11 = rsi;
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 40);
    rax13 = g28;
    zf14 = print_lines == 0;
    if (!zf14) {
        rax15 = umaxtostr(rdi, rsp12);
        *reinterpret_cast<uint32_t*>(&rdx) = number_width;
        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
        rsi = reinterpret_cast<void**>("%*s");
        rcx = rax15;
        rbp10 = reinterpret_cast<void**>(" %*s");
        fun_2740(1, "%*s", rdx, rcx);
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8);
    }
    zf16 = print_words == 0;
    if (!zf16) {
        rax17 = umaxtostr(rbx11, rsp12);
        *reinterpret_cast<uint32_t*>(&rdx) = number_width;
        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
        rsi = rbp10;
        rcx = rax17;
        rbp10 = reinterpret_cast<void**>(" %*s");
        fun_2740(1, rsi, rdx, rcx);
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8);
    }
    zf18 = print_chars == 0;
    if (!zf18) {
        rax19 = umaxtostr(r15_7, rsp12);
        *reinterpret_cast<uint32_t*>(&rdx) = number_width;
        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
        rsi = rbp10;
        rcx = rax19;
        rbp10 = reinterpret_cast<void**>(" %*s");
        fun_2740(1, rsi, rdx, rcx);
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8);
    }
    zf20 = print_bytes == 0;
    if (!zf20) {
        rax21 = umaxtostr(r14_8, rsp12);
        *reinterpret_cast<uint32_t*>(&rdx) = number_width;
        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
        rsi = rbp10;
        rcx = rax21;
        rbp10 = reinterpret_cast<void**>(" %*s");
        fun_2740(1, rsi, rdx, rcx);
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8);
    }
    zf22 = print_linelength == 0;
    if (!zf22) {
        rax23 = umaxtostr(r8, rsp12);
        *reinterpret_cast<uint32_t*>(&rdx) = number_width;
        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
        rsi = rbp10;
        rcx = rax23;
        fun_2740(1, rsi, rdx, rcx);
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8);
    }
    if (r12_9) {
        rax24 = fun_2590(r12_9, 10);
        rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
        if (rax24) {
            rax26 = quotearg_n_style_colon();
            rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
            r12_9 = rax26;
        }
        rdx = r12_9;
        rsi = reinterpret_cast<void**>(" %s");
        fun_2740(1, " %s", rdx, rcx);
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
    }
    rdi27 = stdout;
    rax28 = rdi27->f28;
    if (reinterpret_cast<unsigned char>(rax28) >= reinterpret_cast<unsigned char>(rdi27->f30)) {
        *reinterpret_cast<int32_t*>(&rsi) = 10;
        fun_25a0();
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
    } else {
        rdx = rax28 + 1;
        rdi27->f28 = rdx;
        *reinterpret_cast<void***>(rax28) = reinterpret_cast<void**>(10);
    }
    rax29 = rax13 - g28;
    if (!rax29) {
        return;
    }
    fun_2560();
    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8);
    r11_31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 0x4000);
    do {
        rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - reinterpret_cast<int64_t>("_full"));
    } while (rsp30 != r11_31);
    r14d32 = 0;
    *reinterpret_cast<int32_t*>(&r13_33) = 0;
    *reinterpret_cast<int32_t*>(&r13_33 + 4) = 0;
    r15d34 = *reinterpret_cast<int32_t*>(&rsi);
    r12_35 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp30) - 72 + 48);
    v36 = rdx;
    v37 = rcx;
    rax38 = g28;
    v39 = rax38;
    v40 = reinterpret_cast<void**>(0);
    while (*reinterpret_cast<int32_t*>(&rdi41) = r15d34, *reinterpret_cast<int32_t*>(&rdi41 + 4) = 0, rax42 = safe_read(rdi41, r12_35, 0x4000, rcx), !!rax42) {
        if (rax42 == 0xffffffffffffffff) 
            goto addr_37a0_26;
        rcx = reinterpret_cast<void**>(0x8888888888888889);
        v40 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v40) + reinterpret_cast<unsigned char>(rax42));
        rbx43 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_35) + reinterpret_cast<unsigned char>(rax42));
        v44 = reinterpret_cast<void*>(__intrinsic() >> 3);
        if (*reinterpret_cast<unsigned char*>(&r14d32)) {
            *reinterpret_cast<void***>(rbx43) = reinterpret_cast<void**>(10);
            rbp45 = r13_33;
            rdi46 = r12_35;
            while (rax47 = fun_2650(rdi46, 10), reinterpret_cast<unsigned char>(rbx43) > reinterpret_cast<unsigned char>(rax47)) {
                rdi46 = rax47 + 1;
                ++rbp45;
            }
            below_or_equal48 = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp45) - reinterpret_cast<unsigned char>(r13_33)) <= reinterpret_cast<uint64_t>(v44));
            r13_33 = rbp45;
            *reinterpret_cast<unsigned char*>(&r14d32) = below_or_equal48;
        } else {
            if (rbx43 == r12_35) {
                r14d32 = 1;
            } else {
                rdx49 = r13_33;
                rax50 = r12_35;
                do {
                    ++rax50;
                    *reinterpret_cast<int32_t*>(&rcx) = 0;
                    *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
                    *reinterpret_cast<unsigned char*>(&rcx) = reinterpret_cast<uint1_t>(v51 == 10);
                    rdx49 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx49) + reinterpret_cast<unsigned char>(rcx));
                } while (rbx43 != rax50);
                below_or_equal52 = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx49) - reinterpret_cast<unsigned char>(r13_33)) <= reinterpret_cast<uint64_t>(v44));
                r13_33 = rdx49;
                *reinterpret_cast<unsigned char*>(&r14d32) = below_or_equal52;
            }
        }
    }
    *reinterpret_cast<void***>(v37) = v40;
    *reinterpret_cast<void***>(v36) = r13_33;
    addr_377a_38:
    rdx53 = v39 - g28;
    if (rdx53) {
        fun_2560();
    } else {
        goto v54;
    }
    addr_37a0_26:
    quotearg_n_style_colon();
    fun_2490();
    fun_2780();
    goto addr_377a_38;
}