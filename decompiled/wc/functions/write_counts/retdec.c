void write_counts(int64_t lines, int64_t words, int64_t chars, int64_t bytes, int64_t linelength, char * file) {
    int64_t v1 = __readfsqword(40); // 0x3444
    int64_t v2; // bp-88, 0x3420
    if (*(char *)&print_lines != 0) {
        // 0x35e0
        umaxtostr(lines, (char *)&v2);
        function_2740();
    }
    // 0x3461
    if (*(char *)&print_words != 0) {
        // 0x35b0
        umaxtostr(words, (char *)&v2);
        function_2740();
    }
    // 0x346e
    if (*(char *)&print_chars != 0) {
        // 0x3580
        umaxtostr(chars, (char *)&v2);
        function_2740();
    }
    // 0x347b
    if (*(char *)&print_bytes != 0) {
        // 0x3550
        umaxtostr(bytes, (char *)&v2);
        function_2740();
    }
    // 0x3488
    if (*(char *)&print_linelength != 0) {
        // 0x3520
        umaxtostr(linelength, (char *)&v2);
        function_2740();
    }
    if (file != NULL) {
        // 0x349a
        if (function_2590() != 0) {
            // 0x34ac
            quotearg_n_style_colon();
        }
        // 0x34be
        function_2740();
    }
    int64_t v3 = (int64_t)g28; // 0x34d4
    int64_t * v4 = (int64_t *)(v3 + 40); // 0x34db
    uint64_t v5 = *v4; // 0x34db
    if (v5 >= *(int64_t *)(v3 + 48)) {
        // 0x3610
        function_25a0();
    } else {
        // 0x34e9
        *v4 = v5 + 1;
        *(char *)v5 = 10;
    }
    // 0x34f4
    if (v1 == __readfsqword(40)) {
        // 0x3508
        return;
    }
    // 0x361f
    function_2560();
}