void write_counts(undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,
                 undefined8 param_5,char *param_6)

{
  char *pcVar1;
  undefined8 uVar2;
  undefined5 *puVar3;
  undefined5 *puVar4;
  long in_FS_OFFSET;
  undefined auStack88 [24];
  long local_40;
  
  puVar3 = (undefined5 *)0x109401;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (print_lines != '\0') {
    uVar2 = umaxtostr(param_1,auStack88);
    puVar3 = &format_sp_int_0;
    __printf_chk(1,0x109401,number_width,uVar2);
  }
  puVar4 = puVar3;
  if (print_words != '\0') {
    uVar2 = umaxtostr(param_2,auStack88);
    puVar4 = &format_sp_int_0;
    __printf_chk(1,puVar3,number_width,uVar2);
  }
  puVar3 = puVar4;
  if (print_chars != '\0') {
    uVar2 = umaxtostr(param_3,auStack88);
    puVar3 = &format_sp_int_0;
    __printf_chk(1,puVar4,number_width,uVar2);
  }
  puVar4 = puVar3;
  if (print_bytes != '\0') {
    uVar2 = umaxtostr(param_4,auStack88);
    puVar4 = &format_sp_int_0;
    __printf_chk(1,puVar3,number_width,uVar2);
  }
  if (print_linelength != '\0') {
    uVar2 = umaxtostr(param_5,auStack88);
    __printf_chk(1,puVar4,number_width,uVar2);
  }
  if (param_6 != (char *)0x0) {
    pcVar1 = strchr(param_6,10);
    if (pcVar1 != (char *)0x0) {
      param_6 = (char *)quotearg_n_style_colon(0,3,param_6);
    }
    __printf_chk(1," %s",param_6);
  }
  pcVar1 = stdout->_IO_write_ptr;
  if (pcVar1 < stdout->_IO_write_end) {
    stdout->_IO_write_ptr = pcVar1 + 1;
    *pcVar1 = '\n';
  }
  else {
    __overflow(stdout,10);
  }
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}