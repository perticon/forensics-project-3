void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002820(fn0000000000002530(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000004299;
	}
	fn0000000000002740(fn0000000000002530(0x05, "Usage: %s [OPTION]... [FILE]...\n  or:  %s [OPTION]... --files0-from=F\n", null), 0x01);
	fn0000000000002640(stdout, fn0000000000002530(0x05, "Print newline, word, and byte counts for each FILE, and a total line if\nmore than one FILE is specified.  A word is a non-zero-length sequence of\nprintable characters delimited by white space.\n", null));
	fn0000000000002640(stdout, fn0000000000002530(0x05, "\nWith no FILE, or when FILE is -, read standard input.\n", null));
	fn0000000000002640(stdout, fn0000000000002530(0x05, "\nThe options below may be used to select which counts are printed, always in\nthe following order: newline, word, character, byte, maximum line length.\n  -c, --bytes            print the byte counts\n  -m, --chars            print the character counts\n  -l, --lines            print the newline counts\n", null));
	fn0000000000002640(stdout, fn0000000000002530(0x05, "      --files0-from=F    read input from the files specified by\n                           NUL-terminated names in file F;\n                           If F is - then read names from standard input\n  -L, --max-line-length  print the maximum display width\n  -w, --words            print the word counts\n", null));
	fn0000000000002640(stdout, fn0000000000002530(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002640(stdout, fn0000000000002530(0x05, "      --version     output version information and exit\n", null));
	struct Eq_2494 * rdx_157 = fp + ~0xA7;
	do
	{
		struct Eq_2513 * rax_181 = rdx_157[1];
		++rdx_157;
	} while (rax_181 != null && ((word32) rax_181->b0000 != 0x77 || ((word32) rax_181->b0001 != 99 || rax_181->b0002 != 0x00)));
	ptr64 r12_202 = rdx_157->qw0008;
	if (r12_202 != 0x00)
	{
		fn0000000000002740(fn0000000000002530(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_38 rax_289 = fn0000000000002730(null, 0x05);
		if (rax_289 == 0x00 || fn00000000000024A0(0x03, "en_", rax_289) == 0x00)
			goto l00000000000044D6;
	}
	else
	{
		fn0000000000002740(fn0000000000002530(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_38 rax_231 = fn0000000000002730(null, 0x05);
		if (rax_231 == 0x00 || fn00000000000024A0(0x03, "en_", rax_231) == 0x00)
		{
			fn0000000000002740(fn0000000000002530(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000004513:
			fn0000000000002740(fn0000000000002530(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000004299:
			fn0000000000002800(edi);
		}
		r12_202 = 0x905D;
	}
	fn0000000000002640(stdout, fn0000000000002530(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l00000000000044D6:
	fn0000000000002740(fn0000000000002530(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000004513;
}