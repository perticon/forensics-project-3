iswnbspace (wint_t wc)
{
  return ! posixly_correct
         && (wc == 0x00A0 || wc == 0x2007
             || wc == 0x202F || wc == 0x2060);
}