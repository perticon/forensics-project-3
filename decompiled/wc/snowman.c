
int64_t g28;

void** fun_2530();

uint64_t fun_2540();

unsigned char print_bytes = 0;

unsigned char print_chars = 0;

unsigned char print_words = 0;

unsigned char print_linelength = 0;

void fdadvise(int64_t rdi);

unsigned char print_lines = 0;

void** safe_read(void** rdi, void** rsi, void** rdx, void** rcx);

void*** fun_2870(void** rdi, void** rsi, void** rdx, void** rcx);

int32_t fun_2770(int64_t rdi);

unsigned char posixly_correct = 0;

void** rpl_mbrtowc(void** rdi, void** rsi, void** rdx, void** rcx);

uint32_t fun_2830(void** rdi, void** rsi, void** rdx, void** rcx);

int32_t fun_2840(void** rdi, void** rsi);

int64_t fun_26e0(int64_t rdi, void** rsi);

int32_t fun_2760(void** rdi, void** rsi);

void fun_2690(void** rdi, void** rsi, void** rdx, void** rcx);

void write_counts(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** total_words = reinterpret_cast<void**>(0);

void** total_lines = reinterpret_cast<void**>(0);

void** total_chars = reinterpret_cast<void**>(0);

void** total_bytes = reinterpret_cast<void**>(0);

void** max_line_length = reinterpret_cast<void**>(0);

void fun_2560();

void** quotearg_n_style_colon();

int32_t* fun_2490();

void* fun_2780();

void** fun_2850();

signed char debug = 0;

int64_t wc_lines_p = 0x37f0;

uint64_t page_size = 0;

void** fun_25c0(int64_t rdi, ...);

uint32_t wc(int32_t edi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void* rsp7;
    void* r11_8;
    void* rsp9;
    int32_t v10;
    void** v11;
    int64_t rax12;
    int64_t v13;
    void** v14;
    void** rax15;
    void** v16;
    uint64_t rax17;
    void* rsp18;
    uint32_t r12d19;
    uint32_t eax20;
    unsigned char v21;
    uint32_t eax22;
    int1_t zf23;
    uint32_t r15d24;
    int64_t rdi25;
    void** rcx26;
    int1_t zf27;
    int64_t rdi28;
    int64_t rdi29;
    uint64_t rax30;
    void* rbp31;
    void** r13_32;
    void** v33;
    void* r12_34;
    void** rsi35;
    void** rdi36;
    void** rax37;
    void** r14_38;
    void** rdx39;
    void** rdi40;
    void** v41;
    int32_t eax42;
    void*** rax43;
    int64_t rax44;
    uint32_t eax45;
    int32_t eax46;
    void** r14_47;
    uint32_t r15d48;
    void** rbp49;
    void* rbx50;
    void** r12_51;
    void* v52;
    void** v53;
    void** rdi54;
    void** rdx55;
    void** rax56;
    void** r13_57;
    uint32_t r9d58;
    void** r15_59;
    void** rcx60;
    uint32_t eax61;
    int64_t rax62;
    uint32_t r14d63;
    int32_t v64;
    int32_t eax65;
    void*** rax66;
    uint32_t eax67;
    void** r14_68;
    void** rax69;
    uint32_t eax70;
    void** rdx71;
    int64_t r14_72;
    int64_t rax73;
    unsigned char v74;
    int32_t eax75;
    int1_t zf76;
    int64_t rdi77;
    int64_t rax78;
    int32_t eax79;
    uint32_t eax80;
    uint32_t eax81;
    void* r12_82;
    void* rbx83;
    void** rcx84;
    void** rbx85;
    uint32_t eax86;
    int1_t below_or_equal87;
    void** tmp64_88;
    void** tmp64_89;
    void** tmp64_90;
    void** tmp64_91;
    int1_t cf92;
    int64_t rax93;
    int64_t v94;
    uint32_t eax95;
    int64_t rax96;
    int64_t rax97;
    int1_t zf98;
    void** eax99;
    int1_t zf100;
    int64_t rax101;
    void** r12_102;
    uint64_t r13_103;
    int64_t rdi104;
    void** rax105;
    uint64_t rsi106;
    uint64_t rcx107;
    void** r12_108;
    int64_t rdi109;
    void** rax110;
    int32_t ebx111;
    void** rbp112;
    int64_t rdi113;
    void** rdi114;
    void** rax115;
    int64_t rsi116;
    int1_t zf117;
    int1_t zf118;

    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8);
    r11_8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 0x4000);
    do {
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - reinterpret_cast<int64_t>("_full"));
    } while (rsp7 != r11_8);
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 0x78);
    v10 = edi;
    v11 = rsi;
    rax12 = g28;
    v13 = rax12;
    v14 = rsi;
    if (!rsi) {
        rax15 = fun_2530();
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        v14 = rax15;
    }
    v16 = reinterpret_cast<void**>(0);
    rax17 = fun_2540();
    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
    r12d19 = print_bytes;
    if (rax17 <= 1) {
        eax20 = print_chars;
        v21 = 0;
        if (!*reinterpret_cast<signed char*>(&r12d19)) {
            r12d19 = eax20;
        }
    } else {
        eax22 = print_chars;
        v21 = *reinterpret_cast<unsigned char*>(&eax22);
    }
    zf23 = print_words == 0;
    if (!zf23 || (r15d24 = print_linelength, !!*reinterpret_cast<signed char*>(&r15d24))) {
        *reinterpret_cast<int32_t*>(&rdi25) = v10;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi25) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx26) = 2;
        *reinterpret_cast<int32_t*>(&rcx26 + 4) = 0;
        fdadvise(rdi25);
        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
    } else {
        if (*reinterpret_cast<signed char*>(&r12d19) == 1 && !v21) {
            zf27 = print_lines == 0;
            if (zf27) 
                goto addr_38db_14;
            *reinterpret_cast<int32_t*>(&rdi28) = v10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi28) + 4) = 0;
            fdadvise(rdi28);
            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
            goto addr_4103_16;
        }
        *reinterpret_cast<int32_t*>(&rdi29) = v10;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi29) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx26) = 2;
        *reinterpret_cast<int32_t*>(&rcx26 + 4) = 0;
        fdadvise(rdi29);
        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
        if (v21 != 1) 
            goto addr_4218_18;
    }
    addr_3a8a_19:
    rax30 = fun_2540();
    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
    if (rax30 <= 1) {
        *reinterpret_cast<int32_t*>(&rbp31) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp31) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r13_32) = 0;
        *reinterpret_cast<int32_t*>(&r13_32 + 4) = 0;
        v33 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp18) + 96);
        *reinterpret_cast<uint32_t*>(&r12_34) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_34) + 4) = 0;
        while (rsi35 = v33, *reinterpret_cast<int32_t*>(&rdi36) = v10, *reinterpret_cast<int32_t*>(&rdi36 + 4) = 0, rax37 = safe_read(rdi36, rsi35, 0x4000, 2), !!rax37) {
            if (rax37 == 0xffffffffffffffff) 
                goto addr_3ff0_23;
            v16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v16) + reinterpret_cast<unsigned char>(rax37));
            r14_38 = v33;
            rdx39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_38) + reinterpret_cast<unsigned char>(rax37));
            while (*reinterpret_cast<uint32_t*>(&rdi40) = reinterpret_cast<unsigned char>(v41), *reinterpret_cast<int32_t*>(&rdi40 + 4) = 0, ++r14_38, eax42 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rdi40 + 0xfffffffffffffff7)), *reinterpret_cast<unsigned char*>(&eax42) > 23) {
                rax43 = fun_2870(rdi40, rsi35, rdx39, 2);
                rdx39 = rdx39;
                *reinterpret_cast<uint32_t*>(&rax44) = *reinterpret_cast<unsigned char*>(&rdi40);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax44) + 4) = 0;
                rsi35 = *rax43;
                eax45 = reinterpret_cast<uint16_t>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rsi35) + reinterpret_cast<uint64_t>(rax44 * 2)));
                if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax45) + 1) & 64)) {
                    addr_3ef8_27:
                    if (r14_38 != rdx39) 
                        continue; else 
                        goto addr_3f01_28;
                } else {
                    ++r13_32;
                    if (*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax45) + 1) & 32) 
                        goto addr_3f20_30;
                    eax46 = fun_2770(rax44);
                    *reinterpret_cast<uint32_t*>(&rsi35) = posixly_correct;
                    *reinterpret_cast<int32_t*>(&rsi35 + 4) = 0;
                    rdx39 = rdx39;
                    if (!*reinterpret_cast<unsigned char*>(&rsi35)) 
                        goto addr_3ed3_32;
                }
                *reinterpret_cast<uint32_t*>(&r12_34) = *reinterpret_cast<unsigned char*>(&rsi35);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_34) + 4) = 0;
                goto addr_3ef8_27;
                addr_3ed3_32:
                if (eax46 == 0xa0 || (eax46 == 0x2007 || (eax46 == 0x202f || eax46 == 0x2060))) {
                    addr_3f20_30:
                    rbp31 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp31) + reinterpret_cast<uint64_t>(r12_34));
                    *reinterpret_cast<uint32_t*>(&r12_34) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_34) + 4) = 0;
                    goto addr_3ef8_27;
                } else {
                    *reinterpret_cast<uint32_t*>(&r12_34) = 1;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_34) + 4) = 0;
                    goto addr_3ef8_27;
                }
            }
            goto addr_3e6f_35;
            addr_3f01_28:
        }
    } else {
        *reinterpret_cast<int32_t*>(&r14_47) = 0;
        *reinterpret_cast<int32_t*>(&r14_47 + 4) = 0;
        r15d48 = 0;
        *reinterpret_cast<int32_t*>(&rbp49) = 0;
        *reinterpret_cast<int32_t*>(&rbp49 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rbx50) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx50) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r12_51) = 0;
        *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
        v52 = reinterpret_cast<void*>(0);
        v53 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp18) + 96);
        while (*reinterpret_cast<int32_t*>(&rdi54) = v10, *reinterpret_cast<int32_t*>(&rdi54 + 4) = 0, rdx55 = reinterpret_cast<void**>(0x4000 - reinterpret_cast<unsigned char>(r14_47)), rax56 = safe_read(rdi54, reinterpret_cast<unsigned char>(v53) + reinterpret_cast<unsigned char>(r14_47), rdx55, rcx26), rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8), !!rax56) {
            if (rax56 == 0xffffffffffffffff) 
                goto addr_3f94_39;
            v16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v16) + reinterpret_cast<unsigned char>(rax56));
            r13_57 = v53;
            r9d58 = r15d48;
            r15_59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_47) + reinterpret_cast<unsigned char>(rax56));
            do {
                if (!*reinterpret_cast<unsigned char*>(&r9d58) && (*reinterpret_cast<uint32_t*>(&rcx60) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_57)), *reinterpret_cast<int32_t*>(&rcx60 + 4) = 0, eax61 = *reinterpret_cast<uint32_t*>(&rcx60), *reinterpret_cast<unsigned char*>(&eax61) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax61) >> 5), *reinterpret_cast<uint32_t*>(&rax62) = eax61 & 7, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax62) + 4) = 0, r14d63 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(0x9a00) + reinterpret_cast<uint64_t>(rax62 * 4))) >> *reinterpret_cast<unsigned char*>(&rcx60) & 1, !!r14d63)) {
                    v64 = *reinterpret_cast<signed char*>(&rcx60);
                    eax65 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rcx60 + 0xfffffffffffffff7));
                    if (*reinterpret_cast<unsigned char*>(&eax65) <= 23) 
                        goto addr_3b4d_43;
                    rax66 = fun_2870(rdi54, 0x9a00, rdx55, rcx60);
                    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                    *reinterpret_cast<uint32_t*>(&rcx26) = *reinterpret_cast<unsigned char*>(&rcx60);
                    *reinterpret_cast<int32_t*>(&rcx26 + 4) = 0;
                    r9d58 = *reinterpret_cast<unsigned char*>(&r9d58);
                    *reinterpret_cast<int32_t*>(&rdx55) = 1;
                    *reinterpret_cast<int32_t*>(&rdx55 + 4) = 0;
                    eax67 = reinterpret_cast<uint16_t>(*reinterpret_cast<void***>(*rax66 + reinterpret_cast<unsigned char>(rcx26) * 2));
                    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax67) + 1) & 64)) 
                        goto addr_3ba0_45;
                    ++rbp49;
                    if (*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax67) + 1) & 32) 
                        goto addr_3bd8_47;
                    *reinterpret_cast<uint32_t*>(&rbx50) = *reinterpret_cast<unsigned char*>(&r14d63);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx50) + 4) = 0;
                    goto addr_3ba0_45;
                }
                r14_68 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp18) + 88);
                rdx55 = r15_59;
                rdi54 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp18) + 68);
                rcx26 = r14_68;
                rax69 = rpl_mbrtowc(rdi54, r13_57, rdx55, rcx26);
                rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                if (rax69 == 0xfffffffffffffffe) 
                    break;
                if (rax69 != 0xffffffffffffffff) 
                    goto addr_3c56_51;
                ++r13_57;
                --r15_59;
                r9d58 = 1;
                continue;
                addr_3c56_51:
                eax70 = fun_2830(r14_68, r13_57, rdx55, rcx26);
                rdx71 = rax69;
                if (rdx71) {
                    *reinterpret_cast<int32_t*>(&r14_72) = v64;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_72) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rax73) = static_cast<uint32_t>(r14_72 - 9);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax73) + 4) = 0;
                    if (*reinterpret_cast<uint32_t*>(&rax73) <= 23) 
                        goto addr_3d42_54;
                    *reinterpret_cast<int32_t*>(&rdi54) = *reinterpret_cast<int32_t*>(&r14_72);
                    *reinterpret_cast<int32_t*>(&rdi54 + 4) = 0;
                } else {
                    v64 = 0;
                    *reinterpret_cast<int32_t*>(&rdi54) = 0;
                    *reinterpret_cast<int32_t*>(&rdi54 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&r14_72) = 0;
                    *reinterpret_cast<int32_t*>(&rdx71) = 1;
                    *reinterpret_cast<int32_t*>(&rdx71 + 4) = 0;
                }
                v74 = reinterpret_cast<uint1_t>(eax70 == 0);
                eax75 = fun_2840(rdi54, r13_57);
                rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 + 8);
                rdx55 = rdx71;
                r9d58 = v74;
                if (!eax75) {
                    addr_3ba0_45:
                    r13_57 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_57) + reinterpret_cast<unsigned char>(rdx55));
                    r15_59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_59) - reinterpret_cast<unsigned char>(rdx55));
                    ++r12_51;
                    continue;
                } else {
                    zf76 = print_linelength == 0;
                    if (!zf76) {
                        *reinterpret_cast<int32_t*>(&rdi77) = *reinterpret_cast<int32_t*>(&r14_72);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi77) + 4) = 0;
                        rax78 = fun_26e0(rdi77, r13_57);
                        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                        r9d58 = v74;
                        rdx55 = rdx71;
                        rcx26 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax78)) + reinterpret_cast<unsigned char>(rbp49));
                        if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax78) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax78) == 0))) {
                            rbp49 = rcx26;
                        }
                    }
                    *reinterpret_cast<int32_t*>(&rdi54) = v64;
                    *reinterpret_cast<int32_t*>(&rdi54 + 4) = 0;
                    eax79 = fun_2760(rdi54, r13_57);
                    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                    rdx55 = rdx55;
                    r9d58 = *reinterpret_cast<unsigned char*>(&r9d58);
                    if (eax79) 
                        goto addr_3bd8_47;
                    eax80 = posixly_correct;
                    if (!*reinterpret_cast<unsigned char*>(&eax80)) 
                        goto addr_3cf1_64;
                }
                *reinterpret_cast<uint32_t*>(&rbx50) = *reinterpret_cast<unsigned char*>(&eax80);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx50) + 4) = 0;
                goto addr_3ba0_45;
                addr_3cf1_64:
                if (v64 == 0xa0 || (v64 == 0x2007 || (v64 == 0x202f || v64 == 0x2060))) {
                    addr_3bd8_47:
                    v52 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v52) + reinterpret_cast<uint64_t>(rbx50));
                    *reinterpret_cast<uint32_t*>(&rbx50) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx50) + 4) = 0;
                    goto addr_3ba0_45;
                } else {
                    *reinterpret_cast<uint32_t*>(&rbx50) = 1;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx50) + 4) = 0;
                    goto addr_3ba0_45;
                }
            } while (r15_59);
            goto addr_3bb3_68;
            r14_47 = r15_59;
            if (r15_59) {
                if (reinterpret_cast<int1_t>(r15_59 == 0x4000)) {
                    ++r13_57;
                    *reinterpret_cast<int32_t*>(&r14_47) = 0x3fff;
                    *reinterpret_cast<int32_t*>(&r14_47 + 4) = 0;
                }
                *reinterpret_cast<uint32_t*>(&rcx26) = 0x4001;
                *reinterpret_cast<int32_t*>(&rcx26 + 4) = 0;
                fun_2690(v53, r13_57, r14_47, 0x4001);
                rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
            }
            r15d48 = 1;
            continue;
            addr_3bb3_68:
            r14_47 = r15_59;
            r15d48 = r9d58;
        }
        goto addr_3f73_74;
    }
    eax81 = *reinterpret_cast<uint32_t*>(&r12_34);
    r12_82 = rbp31;
    rbp49 = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint32_t*>(&rbx83) = *reinterpret_cast<unsigned char*>(&eax81);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx83) + 4) = 0;
    addr_3fd9_76:
    rcx84 = v16;
    if (reinterpret_cast<unsigned char>(0) < reinterpret_cast<unsigned char>(r13_32)) {
        rbp49 = r13_32;
    }
    rbx85 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbx83) + reinterpret_cast<uint64_t>(r12_82));
    *reinterpret_cast<int32_t*>(&r12_51) = 0;
    *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
    while (1) {
        addr_39e8_79:
        eax86 = v21;
        below_or_equal87 = print_chars <= *reinterpret_cast<unsigned char*>(&eax86);
        if (!below_or_equal87) {
            r12_51 = rcx84;
        }
        write_counts(0, rbx85, r12_51, rcx84, rbp49, v11);
        tmp64_88 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(total_words) + reinterpret_cast<unsigned char>(rbx85));
        total_words = tmp64_88;
        tmp64_89 = total_lines;
        total_lines = tmp64_89;
        tmp64_90 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(total_chars) + reinterpret_cast<unsigned char>(r12_51));
        total_chars = tmp64_90;
        tmp64_91 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(total_bytes) + reinterpret_cast<unsigned char>(v16));
        total_bytes = tmp64_91;
        cf92 = reinterpret_cast<unsigned char>(max_line_length) < reinterpret_cast<unsigned char>(rbp49);
        if (cf92) {
            max_line_length = rbp49;
        }
        rax93 = v13 - g28;
        if (!rax93) 
            break;
        fun_2560();
        addr_40dd_85:
        rcx84 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx84) - reinterpret_cast<unsigned char>(rbp49));
        addr_40c1_86:
        v16 = rcx84;
        *reinterpret_cast<int32_t*>(&rbp49) = 0;
        *reinterpret_cast<int32_t*>(&rbp49 + 4) = 0;
        *reinterpret_cast<int32_t*>(&r12_51) = 0;
        *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rbx85) = 0;
        *reinterpret_cast<int32_t*>(&rbx85 + 4) = 0;
    }
    goto v94;
    addr_3ff0_23:
    eax95 = *reinterpret_cast<uint32_t*>(&r12_34);
    r12_82 = rbp31;
    rbp49 = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint32_t*>(&rbx83) = *reinterpret_cast<unsigned char*>(&eax95);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx83) + 4) = 0;
    quotearg_n_style_colon();
    fun_2490();
    fun_2780();
    goto addr_3fd9_76;
    addr_3e6f_35:
    *reinterpret_cast<uint32_t*>(&rax96) = *reinterpret_cast<unsigned char*>(&eax42);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax96) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x92c8 + rax96 * 4) + 0x92c8;
    addr_3f73_74:
    addr_3f79_88:
    rcx84 = v16;
    if (reinterpret_cast<unsigned char>(0) >= reinterpret_cast<unsigned char>(rbp49)) {
        rbp49 = reinterpret_cast<void**>(0);
    }
    rbx85 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbx50) + reinterpret_cast<uint64_t>(v52));
    goto addr_39e8_79;
    addr_3f94_39:
    quotearg_n_style_colon();
    fun_2490();
    fun_2780();
    goto addr_3f79_88;
    addr_3d42_54:
    goto *reinterpret_cast<int32_t*>(0x9268 + rax73 * 4) + 0x9268;
    addr_3b4d_43:
    *reinterpret_cast<uint32_t*>(&rax97) = *reinterpret_cast<unsigned char*>(&eax65);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax97) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x9208 + rax97 * 4) + 0x9208;
    addr_4218_18:
    if (*reinterpret_cast<signed char*>(&r12d19)) {
        addr_4103_16:
        zf98 = print_lines == 0;
        if (zf98) {
            addr_38db_14:
            eax99 = *reinterpret_cast<void***>(rdx);
            if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(eax99) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(eax99 == 0))) {
                eax99 = fun_2850();
                rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                *reinterpret_cast<void***>(rdx) = eax99;
            }
        } else {
            addr_4110_92:
            if (!__intrinsic()) {
                addr_4185_93:
                zf100 = debug == 0;
                if (zf100) {
                    addr_4155_94:
                    rax101 = wc_lines_p;
                    goto addr_415c_95;
                } else {
                    goto addr_419a_97;
                }
            } else {
                if (!(__intrinsic() & 0x8000000)) 
                    goto addr_4140_99;
                if (__intrinsic() <= 6) 
                    goto addr_4185_93; else 
                    goto addr_4132_101;
            }
        }
    } else {
        if (!v21) 
            goto addr_4110_92;
        goto addr_3a8a_19;
    }
    if (!eax99 && ((*reinterpret_cast<uint32_t*>(rdx + 32) & 0xd000) == 0x8000 && (r12_102 = *reinterpret_cast<void***>(rdx + 56), reinterpret_cast<signed char>(r12_102) >= reinterpret_cast<signed char>(0)))) {
        r13_103 = page_size;
        rcx84 = r12_102;
        if (rcx == 0xffffffffffffffff) {
            *reinterpret_cast<int32_t*>(&rdi104) = v10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi104) + 4) = 0;
            rax105 = fun_25c0(rdi104);
            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
            rcx84 = r12_102;
            rbp49 = rax105;
            if (!(reinterpret_cast<unsigned char>(rcx84) % r13_103)) {
                rsi106 = reinterpret_cast<uint64_t>(~reinterpret_cast<unsigned char>(rbp49)) >> 63;
            } else {
                if (reinterpret_cast<unsigned char>(rbp49) <= reinterpret_cast<unsigned char>(rcx84)) 
                    goto addr_40dd_85;
                *reinterpret_cast<int32_t*>(&rcx84) = 0;
                *reinterpret_cast<int32_t*>(&rcx84 + 4) = 0;
                goto addr_40c1_86;
            }
        } else {
            if (reinterpret_cast<unsigned char>(r12_102) % r13_103) 
                goto addr_40c1_86;
            *reinterpret_cast<int32_t*>(&rsi106) = 1;
            *reinterpret_cast<int32_t*>(&rbp49) = 0;
            *reinterpret_cast<int32_t*>(&rbp49 + 4) = 0;
        }
        *reinterpret_cast<int32_t*>(&rcx107) = 0x201;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx107) + 4) = 0;
        if (reinterpret_cast<uint64_t>(*reinterpret_cast<int64_t*>(rdx + 64) - 1) <= 0x1fffffffffffffff) {
            rcx107 = reinterpret_cast<uint64_t>(*reinterpret_cast<int64_t*>(rdx + 64) + 1);
        }
        r12_108 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_102) - reinterpret_cast<unsigned char>(r12_102) % rcx107);
        if (reinterpret_cast<signed char>(r12_108) > reinterpret_cast<signed char>(rbp49) && (*reinterpret_cast<signed char*>(&rsi106) && (*reinterpret_cast<int32_t*>(&rdi109) = v10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi109) + 4) = 0, rax110 = fun_25c0(rdi109, rdi109), rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8), reinterpret_cast<signed char>(rax110) >= reinterpret_cast<signed char>(0)))) {
            v16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_108) - reinterpret_cast<unsigned char>(rbp49));
        }
    }
    ebx111 = v10;
    rbp112 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp18) + 96);
    *reinterpret_cast<int32_t*>(&rdi113) = ebx111;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi113) + 4) = 0;
    fdadvise(rdi113);
    while (*reinterpret_cast<int32_t*>(&rdi114) = ebx111, *reinterpret_cast<int32_t*>(&rdi114 + 4) = 0, rax115 = safe_read(rdi114, rbp112, 0x4000, 2), !!rax115) {
        if (rax115 == 0xffffffffffffffff) 
            goto addr_402f_119;
        v16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v16) + reinterpret_cast<unsigned char>(rax115));
    }
    rcx84 = v16;
    r12_51 = rax115;
    *reinterpret_cast<int32_t*>(&rbp49) = 0;
    *reinterpret_cast<int32_t*>(&rbp49 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rbx85) = 0;
    *reinterpret_cast<int32_t*>(&rbx85 + 4) = 0;
    goto addr_39e8_79;
    addr_402f_119:
    *reinterpret_cast<int32_t*>(&rbp49) = 0;
    *reinterpret_cast<int32_t*>(&rbp49 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rbx85) = 0;
    *reinterpret_cast<int32_t*>(&rbx85 + 4) = 0;
    quotearg_n_style_colon();
    fun_2490();
    *reinterpret_cast<int32_t*>(&r12_51) = 0;
    *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
    fun_2780();
    rcx84 = v16;
    goto addr_39e8_79;
    addr_415c_95:
    *reinterpret_cast<int32_t*>(&rsi116) = v10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi116) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rbp49) = 0;
    *reinterpret_cast<int32_t*>(&rbp49 + 4) = 0;
    *reinterpret_cast<int32_t*>(&r12_51) = 0;
    *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
    *reinterpret_cast<int32_t*>(&rbx85) = 0;
    *reinterpret_cast<int32_t*>(&rbx85 + 4) = 0;
    rax101(v14, rsi116, reinterpret_cast<int64_t>(rsp18) + 72, reinterpret_cast<int64_t>(rsp18) + 80);
    rcx84 = reinterpret_cast<void**>(0);
    goto addr_39e8_79;
    addr_419a_97:
    fun_2530();
    fun_2780();
    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 + 8);
    goto addr_4155_94;
    addr_4132_101:
    if (__intrinsic() & 32) {
        zf117 = debug == 0;
        if (!zf117) {
            fun_2530();
            fun_2780();
            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 + 8);
        }
        rax101 = 0x8720;
        wc_lines_p = 0x8720;
        goto addr_415c_95;
    } else {
        addr_4140_99:
        zf118 = debug == 0;
        if (!zf118) 
            goto addr_419a_97; else 
            goto addr_4155_94;
    }
}

void** umaxtostr(void** rdi, void* rsi);

uint32_t number_width = 0;

void fun_2740(int64_t rdi, void** rsi, void** rdx, void** rcx);

int64_t fun_2590(void** rdi, int64_t rsi);

struct s0 {
    signed char[40] pad40;
    void** f28;
    signed char[7] pad48;
    void** f30;
};

struct s0* stdout = reinterpret_cast<struct s0*>(0);

void fun_25a0();

void** fun_2650(void** rdi, int64_t rsi);

void write_counts(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r15_7;
    void** r14_8;
    void** r12_9;
    void** rbp10;
    void** rbx11;
    void* rsp12;
    int64_t rax13;
    int1_t zf14;
    void** rax15;
    int1_t zf16;
    void** rax17;
    int1_t zf18;
    void** rax19;
    int1_t zf20;
    void** rax21;
    int1_t zf22;
    void** rax23;
    int64_t rax24;
    void* rsp25;
    void** rax26;
    struct s0* rdi27;
    void** rax28;
    int64_t rax29;
    void* rsp30;
    void* r11_31;
    int32_t r14d32;
    void** r13_33;
    int32_t r15d34;
    void** r12_35;
    void** v36;
    void** v37;
    int64_t rax38;
    int64_t v39;
    void** v40;
    void** rdi41;
    void** rax42;
    void** rbx43;
    void* v44;
    void** rbp45;
    void** rdi46;
    void** rax47;
    uint1_t below_or_equal48;
    void** rdx49;
    void** rax50;
    signed char v51;
    uint1_t below_or_equal52;
    int64_t rdx53;
    int64_t v54;

    r15_7 = rdx;
    r14_8 = rcx;
    r12_9 = r9;
    rbp10 = reinterpret_cast<void**>("%*s");
    rbx11 = rsi;
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 40);
    rax13 = g28;
    zf14 = print_lines == 0;
    if (!zf14) {
        rax15 = umaxtostr(rdi, rsp12);
        *reinterpret_cast<uint32_t*>(&rdx) = number_width;
        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
        rsi = reinterpret_cast<void**>("%*s");
        rcx = rax15;
        rbp10 = reinterpret_cast<void**>(" %*s");
        fun_2740(1, "%*s", rdx, rcx);
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8);
    }
    zf16 = print_words == 0;
    if (!zf16) {
        rax17 = umaxtostr(rbx11, rsp12);
        *reinterpret_cast<uint32_t*>(&rdx) = number_width;
        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
        rsi = rbp10;
        rcx = rax17;
        rbp10 = reinterpret_cast<void**>(" %*s");
        fun_2740(1, rsi, rdx, rcx);
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8);
    }
    zf18 = print_chars == 0;
    if (!zf18) {
        rax19 = umaxtostr(r15_7, rsp12);
        *reinterpret_cast<uint32_t*>(&rdx) = number_width;
        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
        rsi = rbp10;
        rcx = rax19;
        rbp10 = reinterpret_cast<void**>(" %*s");
        fun_2740(1, rsi, rdx, rcx);
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8);
    }
    zf20 = print_bytes == 0;
    if (!zf20) {
        rax21 = umaxtostr(r14_8, rsp12);
        *reinterpret_cast<uint32_t*>(&rdx) = number_width;
        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
        rsi = rbp10;
        rcx = rax21;
        rbp10 = reinterpret_cast<void**>(" %*s");
        fun_2740(1, rsi, rdx, rcx);
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8);
    }
    zf22 = print_linelength == 0;
    if (!zf22) {
        rax23 = umaxtostr(r8, rsp12);
        *reinterpret_cast<uint32_t*>(&rdx) = number_width;
        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
        rsi = rbp10;
        rcx = rax23;
        fun_2740(1, rsi, rdx, rcx);
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8);
    }
    if (r12_9) {
        rax24 = fun_2590(r12_9, 10);
        rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
        if (rax24) {
            rax26 = quotearg_n_style_colon();
            rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
            r12_9 = rax26;
        }
        rdx = r12_9;
        rsi = reinterpret_cast<void**>(" %s");
        fun_2740(1, " %s", rdx, rcx);
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
    }
    rdi27 = stdout;
    rax28 = rdi27->f28;
    if (reinterpret_cast<unsigned char>(rax28) >= reinterpret_cast<unsigned char>(rdi27->f30)) {
        *reinterpret_cast<int32_t*>(&rsi) = 10;
        fun_25a0();
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
    } else {
        rdx = rax28 + 1;
        rdi27->f28 = rdx;
        *reinterpret_cast<void***>(rax28) = reinterpret_cast<void**>(10);
    }
    rax29 = rax13 - g28;
    if (!rax29) {
        return;
    }
    fun_2560();
    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8);
    r11_31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 0x4000);
    do {
        rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - reinterpret_cast<int64_t>("_full"));
    } while (rsp30 != r11_31);
    r14d32 = 0;
    *reinterpret_cast<int32_t*>(&r13_33) = 0;
    *reinterpret_cast<int32_t*>(&r13_33 + 4) = 0;
    r15d34 = *reinterpret_cast<int32_t*>(&rsi);
    r12_35 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp30) - 72 + 48);
    v36 = rdx;
    v37 = rcx;
    rax38 = g28;
    v39 = rax38;
    v40 = reinterpret_cast<void**>(0);
    while (*reinterpret_cast<int32_t*>(&rdi41) = r15d34, *reinterpret_cast<int32_t*>(&rdi41 + 4) = 0, rax42 = safe_read(rdi41, r12_35, 0x4000, rcx), !!rax42) {
        if (rax42 == 0xffffffffffffffff) 
            goto addr_37a0_26;
        rcx = reinterpret_cast<void**>(0x8888888888888889);
        v40 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v40) + reinterpret_cast<unsigned char>(rax42));
        rbx43 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_35) + reinterpret_cast<unsigned char>(rax42));
        v44 = reinterpret_cast<void*>(__intrinsic() >> 3);
        if (*reinterpret_cast<unsigned char*>(&r14d32)) {
            *reinterpret_cast<void***>(rbx43) = reinterpret_cast<void**>(10);
            rbp45 = r13_33;
            rdi46 = r12_35;
            while (rax47 = fun_2650(rdi46, 10), reinterpret_cast<unsigned char>(rbx43) > reinterpret_cast<unsigned char>(rax47)) {
                rdi46 = rax47 + 1;
                ++rbp45;
            }
            below_or_equal48 = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp45) - reinterpret_cast<unsigned char>(r13_33)) <= reinterpret_cast<uint64_t>(v44));
            r13_33 = rbp45;
            *reinterpret_cast<unsigned char*>(&r14d32) = below_or_equal48;
        } else {
            if (rbx43 == r12_35) {
                r14d32 = 1;
            } else {
                rdx49 = r13_33;
                rax50 = r12_35;
                do {
                    ++rax50;
                    *reinterpret_cast<int32_t*>(&rcx) = 0;
                    *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
                    *reinterpret_cast<unsigned char*>(&rcx) = reinterpret_cast<uint1_t>(v51 == 10);
                    rdx49 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx49) + reinterpret_cast<unsigned char>(rcx));
                } while (rbx43 != rax50);
                below_or_equal52 = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx49) - reinterpret_cast<unsigned char>(r13_33)) <= reinterpret_cast<uint64_t>(v44));
                r13_33 = rdx49;
                *reinterpret_cast<unsigned char*>(&r14d32) = below_or_equal52;
            }
        }
    }
    *reinterpret_cast<void***>(v37) = v40;
    *reinterpret_cast<void***>(v36) = r13_33;
    addr_377a_38:
    rdx53 = v39 - g28;
    if (rdx53) {
        fun_2560();
    } else {
        goto v54;
    }
    addr_37a0_26:
    quotearg_n_style_colon();
    fun_2490();
    fun_2780();
    goto addr_377a_38;
}

int64_t fun_2480(void** rdi, ...);

void** quotearg_buffer_restyled(void** rdi, void** rsi, void** rdx, void** rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2540();
    if (r8d > 10) {
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x9ae0 + rax11 * 4) + 0x9ae0;
    }
}

struct s1 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

void fun_25e0();

struct s2 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void fun_2440(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(void** rdi, void** rsi, void** rdx, struct s1* rcx, ...) {
    int64_t rbx5;
    int64_t rax6;
    int64_t v7;
    int32_t* rax8;
    void** r15_9;
    int32_t v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    void** r8_15;
    struct s2* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** r9_23;
    void** rax24;
    void** rsi25;
    void** rax26;
    uint32_t r8d27;
    int64_t v28;
    int64_t v29;
    int64_t rax30;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x636f;
    rax8 = fun_2490();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
        fun_2480(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xd090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x6b81]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x63fb;
            fun_25e0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        *reinterpret_cast<uint32_t*>(&r8_15) = rcx->f0;
        *reinterpret_cast<int32_t*>(&r8_15 + 4) = 0;
        rbx16 = reinterpret_cast<struct s2*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        *reinterpret_cast<uint32_t*>(&r9_23) = r15d22;
        *reinterpret_cast<int32_t*>(&r9_23 + 4) = 0;
        rax24 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, *reinterpret_cast<uint32_t*>(&r8_15), *reinterpret_cast<uint32_t*>(&r9_23), &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax24)) {
            rsi25 = rax24 + 1;
            rbx16->f0 = rsi25;
            if (r14_19 != 0xd180) {
                fun_2440(r14_19, rsi25, rsi, rdx, r8_15, r9_23);
                rsi25 = rsi25;
            }
            rax26 = xcharalloc(rsi25, rsi25);
            r8d27 = rcx->f0;
            rbx16->f8 = rax26;
            v28 = rcx->f30;
            r14_19 = rax26;
            v29 = rcx->f28;
            quotearg_buffer_restyled(rax26, rsi25, rsi, rdx, r8d27, r15d22, rsi25, v29, v28, 0x648a);
        }
        *rax8 = v10;
        rax30 = rax6 - g28;
        if (rax30) {
            fun_2560();
        } else {
            return r14_19;
        }
    }
}

struct s3 {
    int64_t f0;
    signed char** f8;
    uint64_t* f10;
    signed char[8] pad32;
    int64_t f20;
    signed char* f28;
    signed char* f30;
    signed char* f38;
    signed char[8] pad72;
    void* f48;
    signed char[24] pad104;
    unsigned char f68;
    signed char[15] pad120;
    int64_t f78;
    signed char** f80;
    signed char** f88;
    signed char** f90;
    signed char[8] pad160;
    void* fa0;
    signed char[24] pad192;
    unsigned char fc0;
    signed char[15] pad208;
    int64_t fd0;
    uint64_t* fd8;
    uint64_t* fe0;
    uint64_t* fe8;
    signed char[8] pad248;
    void* ff8;
    signed char[24] pad280;
    unsigned char f118;
};

void _obstack_newchunk(int64_t rdi, struct s3* rsi);

void save_token(struct s3* rdi, struct s3* rsi) {
    signed char* rax3;
    signed char* r12_4;
    signed char* rax5;
    signed char* rdx6;
    signed char** rax7;
    signed char** rdx8;
    uint64_t* rdx9;
    uint64_t* rax10;

    rax3 = rdi->f30;
    r12_4 = rdi->f28;
    if (rax3 == r12_4) {
        rdi->f68 = reinterpret_cast<unsigned char>(rdi->f68 | 2);
    }
    rax5 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rax3) + reinterpret_cast<int64_t>(rdi->f48) & reinterpret_cast<uint64_t>(~reinterpret_cast<int64_t>(rdi->f48)));
    rdx6 = rdi->f38;
    if (reinterpret_cast<uint64_t>(rax5) - rdi->f20 <= reinterpret_cast<uint64_t>(rdx6) - rdi->f20) {
        rdx6 = rax5;
    }
    rax7 = rdi->f90;
    rdi->f30 = rdx6;
    rdi->f28 = rdx6;
    rdx8 = rdi->f88;
    if (reinterpret_cast<uint64_t>(rax7) - reinterpret_cast<uint64_t>(rdx8) <= 7) {
        _obstack_newchunk(reinterpret_cast<int64_t>(rdi) + 0x70, 8);
        rdx8 = rdi->f88;
    }
    *rdx8 = r12_4;
    rdx9 = rdi->fe0;
    rax10 = rdi->fe8;
    rdi->f88 = rdi->f88 + 1;
    if (reinterpret_cast<uint64_t>(rax10) - reinterpret_cast<uint64_t>(rdx9) <= 7) {
        _obstack_newchunk(reinterpret_cast<int64_t>(rdi) + 0xc8, 8);
        rdx9 = rdi->fe0;
    }
    *rdx9 = reinterpret_cast<uint64_t>(rax3) - reinterpret_cast<uint64_t>(r12_4) - 1;
    rdi->fe0 = rdi->fe0 + 1;
    rdi->f0 = rdi->f0 + 1;
    return;
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xd0a8;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s4 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s4* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s4* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0x9a7b);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0x9a74);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0x9a7f);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0x9a70);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gcdb8 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gcdb8;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2403() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2413() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2423() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2433() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t free = 0;

void fun_2443() {
    __asm__("cli ");
    goto free;
}

int64_t malloc = 0;

void fun_2453() {
    __asm__("cli ");
    goto malloc;
}

int64_t __cxa_finalize = 0;

void fun_2463() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t getenv = 0x2030;

void fun_2473() {
    __asm__("cli ");
    goto getenv;
}

int64_t abort = 0x2040;

void fun_2483() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2050;

void fun_2493() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2060;

void fun_24a3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2070;

void fun_24b3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2080;

void fun_24c3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t ferror = 0x2090;

void fun_24d3() {
    __asm__("cli ");
    goto ferror;
}

int64_t iswcntrl = 0x20a0;

void fun_24e3() {
    __asm__("cli ");
    goto iswcntrl;
}

int64_t reallocarray = 0x20b0;

void fun_24f3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t textdomain = 0x20c0;

void fun_2503() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20d0;

void fun_2513() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20e0;

void fun_2523() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x20f0;

void fun_2533() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2100;

void fun_2543() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2110;

void fun_2553() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2120;

void fun_2563() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2130;

void fun_2573() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2140;

void fun_2583() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x2150;

void fun_2593() {
    __asm__("cli ");
    goto strchr;
}

int64_t __overflow = 0x2160;

void fun_25a3() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x2170;

void fun_25b3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2180;

void fun_25c3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x2190;

void fun_25d3() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x21a0;

void fun_25e3() {
    __asm__("cli ");
    goto memset;
}

int64_t fgetc = 0x21b0;

void fun_25f3() {
    __asm__("cli ");
    goto fgetc;
}

int64_t close = 0x21c0;

void fun_2603() {
    __asm__("cli ");
    goto close;
}

int64_t posix_fadvise = 0x21d0;

void fun_2613() {
    __asm__("cli ");
    goto posix_fadvise;
}

int64_t read = 0x21e0;

void fun_2623() {
    __asm__("cli ");
    goto read;
}

int64_t memcmp = 0x21f0;

void fun_2633() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2200;

void fun_2643() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t rawmemchr = 0x2210;

void fun_2653() {
    __asm__("cli ");
    goto rawmemchr;
}

int64_t calloc = 0x2220;

void fun_2663() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2230;

void fun_2673() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2240;

void fun_2683() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t __memmove_chk = 0x2250;

void fun_2693() {
    __asm__("cli ");
    goto __memmove_chk;
}

int64_t feof = 0x2260;

void fun_26a3() {
    __asm__("cli ");
    goto feof;
}

int64_t stat = 0x2270;

void fun_26b3() {
    __asm__("cli ");
    goto stat;
}

int64_t memcpy = 0x2280;

void fun_26c3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2290;

void fun_26d3() {
    __asm__("cli ");
    goto fileno;
}

int64_t wcwidth = 0x22a0;

void fun_26e3() {
    __asm__("cli ");
    goto wcwidth;
}

int64_t fflush = 0x22b0;

void fun_26f3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x22c0;

void fun_2703() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x22d0;

void fun_2713() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x22e0;

void fun_2723() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x22f0;

void fun_2733() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2300;

void fun_2743() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t setvbuf = 0x2310;

void fun_2753() {
    __asm__("cli ");
    goto setvbuf;
}

int64_t iswspace = 0x2320;

void fun_2763() {
    __asm__("cli ");
    goto iswspace;
}

int64_t btowc = 0x2330;

void fun_2773() {
    __asm__("cli ");
    goto btowc;
}

int64_t error = 0x2340;

void fun_2783() {
    __asm__("cli ");
    goto error;
}

int64_t open = 0x2350;

void fun_2793() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x2360;

void fun_27a3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fopen = 0x2370;

void fun_27b3() {
    __asm__("cli ");
    goto fopen;
}

int64_t sysconf = 0x2380;

void fun_27c3() {
    __asm__("cli ");
    goto sysconf;
}

int64_t __cxa_atexit = 0x2390;

void fun_27d3() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t sysinfo = 0x23a0;

void fun_27e3() {
    __asm__("cli ");
    goto sysinfo;
}

int64_t getpagesize = 0x23b0;

void fun_27f3() {
    __asm__("cli ");
    goto getpagesize;
}

int64_t exit = 0x23c0;

void fun_2803() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x23d0;

void fun_2813() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x23e0;

void fun_2823() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x23f0;

void fun_2833() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2400;

void fun_2843() {
    __asm__("cli ");
    goto iswprint;
}

int64_t fstat = 0x2410;

void fun_2853() {
    __asm__("cli ");
    goto fstat;
}

int64_t getdelim = 0x2420;

void fun_2863() {
    __asm__("cli ");
    goto getdelim;
}

int64_t __ctype_b_loc = 0x2430;

void fun_2873() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(void** rdi);

void** fun_2730(int64_t rdi, ...);

void fun_2520(int64_t rdi, int64_t rsi);

void fun_2500(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int32_t fun_27f0(int64_t rdi, int64_t rsi);

void fun_2750(struct s0* rdi);

int64_t fun_2470(int64_t rdi);

int32_t fun_2570(int64_t rdi, void*** rsi, void** rdx, void** rcx);

int32_t optind = 0;

void** quotearg_style(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

struct s0* stderr = reinterpret_cast<struct s0*>(0);

void fun_2820(struct s0* rdi, void*** rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void usage();

void** argv_iter_init_argv(void** rdi, void*** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** xnmalloc(uint64_t rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t fun_2670(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** fun_26b0(void** rdi, void*** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** argv_iter(void** rdi, void** rsi, void** rdx);

void fun_25d0(int64_t rdi, int64_t rsi);

int32_t fun_2790(void** rdi);

int32_t fun_2600();

signed char have_read_stdin = 0;

void** argv_iter_n_args(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void readtokens0_free(void* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void argv_iter_free(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

struct s5 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    void** f90;
};

struct s5* fun_27b0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

struct s5* stdin = reinterpret_cast<struct s5*>(0);

int32_t fun_26d0(struct s5* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

struct s6 {
    int32_t f0;
    int32_t f4;
};

struct s7 {
    int32_t f0;
    int32_t f4;
};

void physmem_available();

void** argv_iter_init_stream(struct s5* rdi, int32_t* rsi, void** rdx, void** rcx, void** r8, void** r9);

void xalloc_die();

void readtokens0_init(void*** rdi, int32_t* rsi, void** rdx, void** rcx, void** r8, void** r9);

signed char readtokens0(struct s5* rdi, void*** rsi, void** rdx, void** rcx, void** r8, void** r9);

int64_t rpl_fclose(struct s5* rdi, void*** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** Version = reinterpret_cast<void**>(0xc8);

void version_etc(struct s0* rdi, void*** rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8);

int32_t fun_2800();

int64_t fun_28d3(uint32_t edi, void*** rsi) {
    void*** rbp3;
    uint64_t rbx4;
    void** rdi5;
    int64_t rax6;
    int64_t v7;
    int32_t eax8;
    struct s0* rdi9;
    int64_t rax10;
    void** r12_11;
    void** r8_12;
    void** rcx13;
    void** rdx14;
    void*** rsi15;
    int64_t rdi16;
    int32_t eax17;
    void* rsp18;
    int1_t zf19;
    int1_t zf20;
    void* rax21;
    void** rsi22;
    void** rax23;
    void** rax24;
    struct s0* rdi25;
    int64_t v26;
    int64_t v27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int1_t zf32;
    int1_t zf33;
    int1_t zf34;
    void** r15_35;
    void** r9_36;
    void** rax37;
    void** r13_38;
    void** rax39;
    void*** rsp40;
    signed char v41;
    void** v42;
    uint32_t eax43;
    uint32_t ecx44;
    uint32_t eax45;
    uint32_t eax46;
    uint32_t eax47;
    void** v48;
    uint64_t rbp49;
    void*** r14_50;
    void** r12_51;
    int32_t eax52;
    void** eax53;
    void** r15_54;
    unsigned char v55;
    void** rsi56;
    void** rax57;
    void** r14_58;
    uint32_t eax59;
    uint32_t v60;
    void** rdx61;
    void*** rdx62;
    void** rax63;
    int32_t eax64;
    void** rdx65;
    int32_t eax66;
    int32_t eax67;
    uint32_t eax68;
    int32_t eax69;
    void** rax70;
    uint32_t eax71;
    void** rax72;
    void** rax73;
    void** rax74;
    void** rax75;
    void** rax76;
    void** rax77;
    void** rax78;
    int32_t* rax79;
    void** rax80;
    void* rsp81;
    void** rax82;
    void** rdi83;
    int1_t zf84;
    int32_t eax85;
    int32_t* rax86;
    void** rax87;
    uint32_t eax88;
    uint32_t edi89;
    void* rax90;
    uint64_t rcx91;
    uint32_t r14d92;
    int64_t rax93;
    int64_t rdx94;
    void** rsi95;
    int32_t eax96;
    void* rsp97;
    struct s5* rax98;
    struct s5* rbp99;
    void** rax100;
    void** rax101;
    int32_t eax102;
    void* rsp103;
    int32_t* rsi104;
    struct s6* rdi105;
    void** eax106;
    void* rsp107;
    uint32_t eax108;
    uint32_t v109;
    int32_t v110;
    struct s7* rdi111;
    int32_t v112;
    int32_t v113;
    void** rax114;
    void** rax115;
    void*** r13_116;
    signed char al117;
    int64_t rax118;
    uint64_t v119;
    void** v120;
    void** rax121;
    uint64_t rdi122;
    void** rax123;
    struct s0* rdi124;
    int64_t rax125;

    __asm__("cli ");
    rbp3 = rsi;
    *reinterpret_cast<uint32_t*>(&rbx4) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx4) + 4) = 0;
    rdi5 = *rsi;
    rax6 = g28;
    v7 = rax6;
    set_program_name(rdi5);
    fun_2730(6, 6);
    fun_2520("coreutils", "/usr/local/share/locale");
    fun_2500("coreutils", "/usr/local/share/locale");
    atexit(0x4770, "/usr/local/share/locale");
    eax8 = fun_27f0(0x4770, "/usr/local/share/locale");
    rdi9 = stdout;
    page_size = reinterpret_cast<uint64_t>(static_cast<int64_t>(eax8));
    fun_2750(rdi9);
    rax10 = fun_2470("POSIXLY_CORRECT");
    print_bytes = 0;
    print_chars = 0;
    print_words = 0;
    print_lines = 0;
    print_linelength = 0;
    max_line_length = reinterpret_cast<void**>(0);
    total_bytes = reinterpret_cast<void**>(0);
    total_chars = reinterpret_cast<void**>(0);
    total_words = reinterpret_cast<void**>(0);
    total_lines = reinterpret_cast<void**>(0);
    posixly_correct = reinterpret_cast<uint1_t>(!!rax10);
    *reinterpret_cast<int32_t*>(&r12_11) = 0;
    *reinterpret_cast<int32_t*>(&r12_11 + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_12) = 0;
    *reinterpret_cast<int32_t*>(&r8_12 + 4) = 0;
    rcx13 = reinterpret_cast<void**>(0xca20);
    rdx14 = reinterpret_cast<void**>("clLmw");
    rsi15 = rbp3;
    *reinterpret_cast<uint32_t*>(&rdi16) = *reinterpret_cast<uint32_t*>(&rbx4);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
    eax17 = fun_2570(rdi16, rsi15, "clLmw", 0xca20);
    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1f8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    if (eax17 == -1) {
        addr_2b07_3:
        zf19 = print_lines == 0;
        if (!zf19) 
            goto addr_2b1d_4;
        zf20 = print_words == 0;
        if (zf20) 
            goto addr_30be_6;
    } else {
        if (eax17 > 0x81) 
            goto addr_30b4_8;
        if (eax17 <= 75) 
            goto addr_2ab0_10; else 
            goto addr_2a20_11;
    }
    addr_2b1d_4:
    while (rax21 = reinterpret_cast<void*>(static_cast<int64_t>(optind)), !!r12_11) {
        if (*reinterpret_cast<int32_t*>(&rbx4) <= *reinterpret_cast<int32_t*>(&rax21)) 
            goto addr_2b35_13;
        rsi22 = rbp3[reinterpret_cast<uint64_t>(rax21) * 8];
        rax23 = quotearg_style(4, rsi22, rdx14, rcx13, r8_12, "David MacKenzie");
        r12_11 = rax23;
        fun_2530();
        fun_2780();
        rax24 = fun_2530();
        rdi25 = stderr;
        *reinterpret_cast<int32_t*>(&rsi15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi15) + 4) = 0;
        rdx14 = reinterpret_cast<void**>("%s\n");
        rcx13 = rax24;
        fun_2820(rdi25, 1, "%s\n", rcx13, r8_12, "David MacKenzie", v26, v27, v28, v29, v30, v31);
        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        addr_30b4_8:
        usage();
        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
        addr_30be_6:
        zf32 = print_chars == 0;
        if (!zf32) 
            continue;
        zf33 = print_bytes == 0;
        if (!zf33) 
            continue;
        zf34 = print_linelength == 0;
        if (!zf34) 
            continue;
        print_bytes = 1;
        print_words = 1;
        print_lines = 1;
    }
    while (1) {
        if (*reinterpret_cast<int32_t*>(&rbx4) > *reinterpret_cast<int32_t*>(&rax21)) {
            rdx14 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax21)));
            rcx13 = reinterpret_cast<void**>(rbp3 + reinterpret_cast<unsigned char>(rdx14) * 8);
            rbx4 = reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rbx4) - *reinterpret_cast<uint32_t*>(&rax21))));
            r15_35 = rcx13;
        } else {
            *reinterpret_cast<uint32_t*>(&rbx4) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx4) + 4) = 0;
            r15_35 = reinterpret_cast<void**>(0xd110);
        }
        rax37 = argv_iter_init_argv(r15_35, rsi15, rdx14, rcx13, r8_12, r9_36);
        r13_38 = rax37;
        if (!rax37) 
            goto addr_3324_22;
        rax39 = xnmalloc(rbx4, 0x98, rdx14, rcx13, r8_12, r9_36);
        rsp40 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 + 8);
        v41 = 0;
        v42 = rax39;
        addr_2fe8_24:
        if (rbx4 != 1 || (eax43 = print_words, ecx44 = print_lines, eax45 = print_chars, eax46 = print_bytes, eax47 = print_linelength, *reinterpret_cast<uint32_t*>(&rcx13) = ecx44 + eax43 + eax45 + eax46 + eax47, *reinterpret_cast<int32_t*>(&rcx13 + 4) = 0, *reinterpret_cast<uint32_t*>(&rcx13) != 1)) {
            v48 = r12_11;
            *reinterpret_cast<int32_t*>(&rbp49) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp49) + 4) = 0;
            r14_50 = reinterpret_cast<void***>(v42 + 8);
            do {
                r12_51 = *reinterpret_cast<void***>(r15_35 + rbp49 * 8);
                if (!r12_51 || (eax52 = fun_2670(r12_51, "-", rdx14, rcx13, r8_12, r9_36), rsp40 = rsp40 - 8 + 8, eax52 == 0)) {
                    eax53 = fun_2850();
                    rsp40 = rsp40 - 8 + 8;
                } else {
                    eax53 = fun_26b0(r12_51, r14_50, rdx14, rcx13, r8_12, r9_36);
                    rsp40 = rsp40 - 8 + 8;
                }
                ++rbp49;
                *(r14_50 - 8) = eax53;
                r14_50 = r14_50 + 0x98;
            } while (rbp49 < rbx4);
            rdx14 = v42;
            r12_11 = v48;
            *reinterpret_cast<uint32_t*>(&rcx13) = 1;
            *reinterpret_cast<int32_t*>(&rcx13 + 4) = 0;
            if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdx14)) <= reinterpret_cast<signed char>(0)) 
                goto addr_312b_31;
        } else {
            *reinterpret_cast<void***>(v42) = reinterpret_cast<void**>(1);
        }
        addr_2bc2_33:
        number_width = *reinterpret_cast<uint32_t*>(&rcx13);
        *reinterpret_cast<int32_t*>(&rbp3) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp3) + 4) = 0;
        r15_54 = reinterpret_cast<void**>(rsp40 + 44);
        v55 = 1;
        while (1) {
            rsi56 = r15_54;
            rax57 = argv_iter(r13_38, rsi56, rdx14);
            rsp40 = rsp40 - 8 + 8;
            r14_58 = rax57;
            if (!rax57) {
                eax59 = v60;
                *reinterpret_cast<uint32_t*>(&r14_58) = v55;
                *reinterpret_cast<int32_t*>(&r14_58 + 4) = 0;
                if (eax59 == 3) 
                    goto addr_3324_22;
                if (eax59 == 4) 
                    break;
                if (eax59 == 2) 
                    goto addr_2f2a_38;
                *reinterpret_cast<int32_t*>(&rdx61) = 0x3aa;
                *reinterpret_cast<int32_t*>(&rdx61 + 4) = 0;
                fun_25d0("!\"unexpected error code from argv_iter\"", "src/wc.c");
                rsp40 = rsp40 - 8 + 8;
                goto addr_2eb2_40;
            }
            if (!r12_11) {
                if (*reinterpret_cast<void***>(rax57)) {
                    addr_2ca3_43:
                    *reinterpret_cast<int32_t*>(&rdx62) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx62) + 4) = 0;
                    if (rbx4) {
                        rdx62 = rbp3;
                    }
                } else {
                    rax63 = fun_2530();
                    rdx14 = reinterpret_cast<void**>("%s");
                    rcx13 = rax63;
                    fun_2780();
                    rsp40 = rsp40 - 8 + 8 - 8 + 8;
                    goto addr_2d7d_46;
                }
            } else {
                if (*reinterpret_cast<void***>(r12_11) != 45 || *reinterpret_cast<void***>(r12_11 + 1)) {
                    if (!*reinterpret_cast<void***>(r14_58)) 
                        goto addr_2cf3_50; else 
                        goto addr_2ca3_43;
                }
                rsi56 = reinterpret_cast<void**>("-");
                eax64 = fun_2670(rax57, "-", rdx14, rcx13, r8_12, r9_36);
                rsp40 = rsp40 - 8 + 8;
                if (!eax64) 
                    goto addr_2cb1_52; else 
                    goto addr_2c02_53;
            }
            addr_2c1c_54:
            rdx65 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rdx62) + reinterpret_cast<unsigned char>(v42));
            eax66 = fun_2670(r14_58, "-", rdx65, rcx13, r8_12, r9_36);
            rsp40 = rsp40 - 8 + 8;
            rdx61 = rdx65;
            if (eax66) {
                addr_2eb2_40:
                eax67 = fun_2790(r14_58);
                rsp40 = rsp40 - 8 + 8;
                if (eax67 == -1 || (*reinterpret_cast<uint32_t*>(&rcx13) = 0, *reinterpret_cast<int32_t*>(&rcx13 + 4) = 0, eax68 = wc(eax67, r14_58, rdx61, 0, r8_12, r9_36), eax69 = fun_2600(), rsp40 = rsp40 - 8 + 8 - 8 + 8, *reinterpret_cast<uint32_t*>(&rdx14) = *reinterpret_cast<unsigned char*>(&eax68), *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0, !!eax69)) {
                    rax70 = quotearg_n_style_colon();
                    fun_2490();
                    rcx13 = rax70;
                    fun_2780();
                    rsp40 = rsp40 - 8 + 8 - 8 + 8 - 8 + 8;
                    *reinterpret_cast<uint32_t*>(&rdx14) = 0;
                    *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
                }
            } else {
                rcx13 = reinterpret_cast<void**>(0xffffffffffffffff);
                have_read_stdin = 1;
                eax71 = wc(0, r14_58, rdx61, 0xffffffffffffffff, r8_12, r9_36);
                rsp40 = rsp40 - 8 + 8;
                *reinterpret_cast<uint32_t*>(&rdx14) = eax71;
                *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
            }
            v55 = reinterpret_cast<unsigned char>(v55 & *reinterpret_cast<unsigned char*>(&rdx14));
            addr_2c5c_58:
            if (!rbx4) {
                *reinterpret_cast<void***>(v42) = reinterpret_cast<void**>(1);
            }
            rbp3 = rbp3 + 0x98;
            continue;
            addr_2cb1_52:
            rax72 = quotearg_style(4, r14_58, rdx14, rcx13, r8_12, r9_36);
            rax73 = fun_2530();
            rcx13 = rax72;
            *reinterpret_cast<int32_t*>(&rsi56) = 0;
            *reinterpret_cast<int32_t*>(&rsi56 + 4) = 0;
            rdx14 = rax73;
            fun_2780();
            rsp40 = rsp40 - 8 + 8 - 8 + 8 - 8 + 8;
            if (*reinterpret_cast<void***>(r14_58)) {
                addr_2d7d_46:
                v55 = 0;
                goto addr_2c5c_58;
            } else {
                addr_2cf3_50:
                rax74 = argv_iter_n_args(r13_38, rsi56, rdx14, rcx13, r8_12, r9_36);
                rax75 = fun_2530();
                rax76 = quotearg_n_style_colon();
                r9_36 = rax75;
                r8_12 = rax74;
                rcx13 = rax76;
                rdx14 = reinterpret_cast<void**>("%s:%lu: %s");
                fun_2780();
                rsp40 = rsp40 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
                v55 = 0;
                goto addr_2c5c_58;
            }
            addr_2c02_53:
            if (!*reinterpret_cast<void***>(r14_58)) 
                goto addr_2cf3_50;
            rdx61 = v42;
            if (!rbx4) 
                goto addr_2eb2_40;
            rdx62 = rbp3;
            goto addr_2c1c_54;
        }
        rax77 = quotearg_n_style_colon();
        rax78 = fun_2530();
        r12_11 = rax78;
        rax79 = fun_2490();
        rcx13 = rax77;
        rdx14 = r12_11;
        *reinterpret_cast<int32_t*>(&rsi56) = *rax79;
        *reinterpret_cast<int32_t*>(&rsi56 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_58) = 0;
        fun_2780();
        rsp40 = rsp40 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
        addr_2de4_64:
        if (v41) {
            readtokens0_free(rsp40 + 48, rsi56, rdx14, rcx13, r8_12, r9_36);
            rsp40 = rsp40 - 8 + 8;
        }
        rax80 = argv_iter_n_args(r13_38, rsi56, rdx14, rcx13, r8_12, r9_36);
        rsp81 = reinterpret_cast<void*>(rsp40 - 8 + 8);
        if (reinterpret_cast<unsigned char>(rax80) > reinterpret_cast<unsigned char>(1)) {
            rax82 = fun_2530();
            r8_12 = max_line_length;
            rcx13 = total_bytes;
            rdx14 = total_chars;
            rsi56 = total_words;
            r9_36 = rax82;
            rdi83 = total_lines;
            write_counts(rdi83, rsi56, rdx14, rcx13, r8_12, r9_36);
            rsp81 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp81) - 8 + 8 - 8 + 8);
        }
        argv_iter_free(r13_38, rsi56, rdx14, rcx13, r8_12, r9_36);
        fun_2440(v42, rsi56, rdx14, rcx13, r8_12, r9_36);
        zf84 = have_read_stdin == 0;
        if (zf84) 
            break;
        eax85 = fun_2600();
        if (!eax85) 
            break;
        rax86 = fun_2490();
        rdx14 = reinterpret_cast<void**>("-");
        *reinterpret_cast<int32_t*>(&rsi15) = *rax86;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi15) + 4) = 0;
        rax21 = fun_2780();
        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp81) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        continue;
        addr_2f2a_38:
        *reinterpret_cast<unsigned char*>(&eax59) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r12_11 == 0)) & *reinterpret_cast<unsigned char*>(&r14_58));
        *reinterpret_cast<uint32_t*>(&rbx4) = eax59;
        if (*reinterpret_cast<unsigned char*>(&eax59) && (*reinterpret_cast<uint32_t*>(&r14_58) = *reinterpret_cast<uint32_t*>(&rbx4), rax87 = argv_iter_n_args(r13_38, rsi56, rdx14, rcx13, r8_12, r9_36), rsp40 = rsp40 - 8 + 8, !rax87)) {
            rdx14 = v42;
            rcx13 = reinterpret_cast<void**>(0xffffffffffffffff);
            *reinterpret_cast<int32_t*>(&rsi56) = 0;
            *reinterpret_cast<int32_t*>(&rsi56 + 4) = 0;
            have_read_stdin = 1;
            eax88 = wc(0, 0, rdx14, 0xffffffffffffffff, r8_12, r9_36);
            rsp40 = rsp40 - 8 + 8;
            *reinterpret_cast<uint32_t*>(&r14_58) = eax88;
            goto addr_2de4_64;
        }
        addr_312b_31:
        edi89 = 1;
        *reinterpret_cast<int32_t*>(&rax90) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax90) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rcx91) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx91) + 4) = 0;
        do {
            if (!*reinterpret_cast<void***>(rdx14)) {
                if ((*reinterpret_cast<uint32_t*>(rdx14 + 32) & 0xf000) != 0x8000) {
                    edi89 = 7;
                } else {
                    rax90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax90) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx14 + 56)));
                }
            }
            ++rcx91;
            rdx14 = rdx14 + 0x98;
        } while (rcx91 < rbx4);
        addr_31c1_77:
        *reinterpret_cast<uint32_t*>(&rcx13) = 1;
        *reinterpret_cast<int32_t*>(&rcx13 + 4) = 0;
        while (reinterpret_cast<uint64_t>(rax90) > 9) {
            *reinterpret_cast<uint32_t*>(&rcx13) = *reinterpret_cast<uint32_t*>(&rcx13) + 1;
            *reinterpret_cast<int32_t*>(&rcx13 + 4) = 0;
            rdx14 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax90) % 10);
            rax90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax90) / 10);
        }
        if (*reinterpret_cast<int32_t*>(&rcx13) >= reinterpret_cast<int32_t>(edi89)) 
            goto addr_31e0_81;
        *reinterpret_cast<uint32_t*>(&rcx13) = edi89;
        *reinterpret_cast<int32_t*>(&rcx13 + 4) = 0;
        addr_31e0_81:
        goto addr_2bc2_33;
    }
    r14d92 = *reinterpret_cast<uint32_t*>(&r14_58) ^ 1;
    *reinterpret_cast<uint32_t*>(&rax93) = *reinterpret_cast<unsigned char*>(&r14d92);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax93) + 4) = 0;
    rdx94 = v7 - g28;
    if (!rdx94) {
        return rax93;
    }
    addr_331f_85:
    fun_2560();
    goto addr_3324_22;
    addr_2b35_13:
    rsi95 = reinterpret_cast<void**>("-");
    eax96 = fun_2670(r12_11, "-", rdx14, rcx13, r8_12, "David MacKenzie");
    rsp97 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
    if (eax96) {
        rsi95 = reinterpret_cast<void**>("r");
        rax98 = fun_27b0(r12_11, "r", rdx14, rcx13, r8_12, "David MacKenzie");
        rsp97 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp97) - 8 + 8);
        rbp99 = rax98;
        if (!rax98) {
            rax100 = quotearg_style(4, r12_11, rdx14, rcx13, r8_12, "David MacKenzie");
            r13_38 = rax100;
            rax101 = fun_2530();
            r12_11 = rax101;
            fun_2490();
            rdx14 = r12_11;
            edi89 = 1;
            rax90 = fun_2780();
            rsp40 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp97) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            goto addr_31c1_77;
        }
    } else {
        rbp99 = stdin;
    }
    eax102 = fun_26d0(rbp99, rsi95, rdx14, rcx13, r8_12, "David MacKenzie");
    rsp103 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp97) - 8 + 8);
    rsi104 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rsp103) + 0x150);
    *reinterpret_cast<int32_t*>(&rdi105) = eax102;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi105) + 4) = 0;
    eax106 = fun_2850();
    rsp107 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp103) - 8 + 8);
    if (eax106) 
        goto addr_2b85_90;
    eax108 = v109 & 0xf000;
    if (eax108 != 0x8000) 
        goto addr_2b85_90;
    __asm__("pxor xmm2, xmm2");
    __asm__("cvtsi2sd xmm2, qword [rsp+0x180]");
    rdi105->f0 = v110;
    rdi111 = reinterpret_cast<struct s7*>(&rdi105->f4);
    physmem_available();
    rsp107 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp107) - 8 + 8);
    __asm__("mulsd xmm0, [rip+0x6778]");
    rdi111->f0 = v112;
    rsi104 = rsi104 + 1 + 1;
    __asm__("comisd xmm0, xmm1");
    if (eax108 > 0x8000) 
        goto addr_3256_99;
    physmem_available();
    rsp107 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp107) - 8 + 8);
    rdi111->f4 = v113;
    ++rsi104;
    __asm__("mulsd xmm1, xmm0");
    addr_3256_99:
    __asm__("comisd xmm1, [rsp]");
    if (eax108 < 0x8000) {
        addr_2b85_90:
        rax114 = argv_iter_init_stream(rbp99, rsi104, rdx14, rcx13, r8_12, "David MacKenzie");
        r13_38 = rax114;
        if (!rax114) {
            addr_3324_22:
            xalloc_die();
        } else {
            rax115 = xnmalloc(1, 0x98, rdx14, rcx13, r8_12, "David MacKenzie");
            rsp40 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp107) - 8 + 8 - 8 + 8);
            v41 = 0;
            v42 = rax115;
        }
    } else {
        r13_116 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp107) + 48);
        readtokens0_init(r13_116, rsi104, rdx14, rcx13, r8_12, "David MacKenzie");
        al117 = readtokens0(rbp99, r13_116, rdx14, rcx13, r8_12, "David MacKenzie");
        v41 = al117;
        if (!al117 || (rax118 = rpl_fclose(rbp99, r13_116, rdx14, rcx13, r8_12, "David MacKenzie"), !!*reinterpret_cast<int32_t*>(&rax118))) {
            quotearg_style(4, r12_11, rdx14, rcx13, r8_12, "David MacKenzie");
            fun_2530();
            fun_2780();
            goto addr_331f_85;
        } else {
            rbx4 = v119;
            r15_35 = v120;
            rax121 = argv_iter_init_argv(v120, r13_116, rdx14, rcx13, r8_12, "David MacKenzie");
            r13_38 = rax121;
            if (!rax121) 
                goto addr_3324_22;
            *reinterpret_cast<int32_t*>(&rdi122) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi122) + 4) = 0;
            if (rbx4) {
                rdi122 = rbx4;
            }
            rax123 = xnmalloc(rdi122, 0x98, rdx14, rcx13, r8_12, "David MacKenzie");
            rsp40 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp107) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            v42 = rax123;
            if (rbx4) 
                goto addr_2fe8_24;
        }
    }
    *reinterpret_cast<uint32_t*>(&rbx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx4) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rcx13) = 1;
    *reinterpret_cast<int32_t*>(&rcx13 + 4) = 0;
    *reinterpret_cast<void***>(v42) = reinterpret_cast<void**>(1);
    goto addr_2bc2_33;
    addr_2ab0_10:
    if (eax17 == 0xffffff7d) {
        rdi124 = stdout;
        rcx13 = Version;
        r9_36 = reinterpret_cast<void**>("David MacKenzie");
        r8_12 = reinterpret_cast<void**>("Paul Rubin");
        rdx14 = reinterpret_cast<void**>("GNU coreutils");
        rsi15 = reinterpret_cast<void***>("wc");
        version_etc(rdi124, "wc", "GNU coreutils", rcx13, "Paul Rubin", "David MacKenzie", 0, 0x2a03);
        eax17 = fun_2800();
        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 - 8 - 8 + 8 - 8 + 8);
    }
    if (eax17 != 0xffffff7e) 
        goto addr_30b4_8;
    usage();
    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
    goto addr_2b07_3;
    addr_2a20_11:
    *reinterpret_cast<uint32_t*>(&rax125) = eax17 - 76;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax125) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax125) > 53) 
        goto addr_30b4_8;
    goto *reinterpret_cast<int32_t*>(0x9328 + rax125 * 4) + 0x9328;
}

int64_t __libc_start_main = 0;

void fun_3333() {
    __asm__("cli ");
    __libc_start_main(0x28d0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xd008;

void fun_2460(int64_t rdi);

int64_t fun_33d3() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2460(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_3413() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

int64_t fun_37f3() {
    int64_t rdx1;
    int64_t rcx2;

    __asm__("cli ");
    if (!rdx1 || !rcx2) {
        return 0;
    } else {
        goto 0x3630;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_2640(void** rdi, struct s0* rsi, int64_t rdx, void** rcx);

unsigned char malloc;

unsigned char g1;

signed char g2;

int32_t fun_24a0(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

void fun_4243(int32_t edi) {
    void** r12_2;
    void** rax3;
    struct s0* r12_4;
    void** rax5;
    struct s0* r12_6;
    void** rax7;
    struct s0* r12_8;
    void** rax9;
    struct s0* r12_10;
    void** rax11;
    struct s0* r12_12;
    void** rax13;
    struct s0* r12_14;
    void** rax15;
    uint32_t ecx16;
    uint32_t ecx17;
    int1_t zf18;
    void** rax19;
    void** rax20;
    int32_t eax21;
    void** rax22;
    void** r13_23;
    void** rax24;
    void** rax25;
    int32_t eax26;
    void** rax27;
    struct s0* r14_28;
    void** rax29;
    void** rax30;
    void** rax31;
    struct s0* rdi32;
    void** r8_33;
    void** r9_34;
    int64_t v35;
    int64_t v36;
    int64_t v37;
    int64_t v38;
    int64_t v39;
    int64_t v40;

    __asm__("cli ");
    r12_2 = program_name;
    if (!edi) {
        while (1) {
            rax3 = fun_2530();
            fun_2740(1, rax3, r12_2, r12_2);
            r12_4 = stdout;
            rax5 = fun_2530();
            fun_2640(rax5, r12_4, 5, r12_2);
            r12_6 = stdout;
            rax7 = fun_2530();
            fun_2640(rax7, r12_6, 5, r12_2);
            r12_8 = stdout;
            rax9 = fun_2530();
            fun_2640(rax9, r12_8, 5, r12_2);
            r12_10 = stdout;
            rax11 = fun_2530();
            fun_2640(rax11, r12_10, 5, r12_2);
            r12_12 = stdout;
            rax13 = fun_2530();
            fun_2640(rax13, r12_12, 5, r12_2);
            r12_14 = stdout;
            rax15 = fun_2530();
            fun_2640(rax15, r12_14, 5, r12_2);
            do {
                if (1) 
                    break;
                ecx16 = malloc;
            } while (0x77 != ecx16 || ((ecx17 = g1, 99 != ecx17) || (zf18 = g2 == 0, !zf18)));
            r12_2 = reinterpret_cast<void**>(0);
            if (1) {
                rax19 = fun_2530();
                fun_2740(1, rax19, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax20 = fun_2730(5);
                if (!rax20 || (eax21 = fun_24a0(rax20, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax21)) {
                    rax22 = fun_2530();
                    r12_2 = reinterpret_cast<void**>("wc");
                    r13_23 = reinterpret_cast<void**>(" invocation");
                    fun_2740(1, rax22, "https://www.gnu.org/software/coreutils/", "wc");
                } else {
                    r12_2 = reinterpret_cast<void**>("wc");
                    goto addr_45d3_9;
                }
            } else {
                rax24 = fun_2530();
                fun_2740(1, rax24, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax25 = fun_2730(5);
                if (!rax25 || (eax26 = fun_24a0(rax25, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax26)) {
                    addr_44d6_11:
                    rax27 = fun_2530();
                    r13_23 = reinterpret_cast<void**>(" invocation");
                    fun_2740(1, rax27, "https://www.gnu.org/software/coreutils/", "wc");
                    if (!reinterpret_cast<int1_t>(r12_2 == "wc")) {
                        r13_23 = reinterpret_cast<void**>(0x9ea1);
                    }
                } else {
                    addr_45d3_9:
                    r14_28 = stdout;
                    rax29 = fun_2530();
                    fun_2640(rax29, r14_28, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_44d6_11;
                }
            }
            rax30 = fun_2530();
            fun_2740(1, rax30, r12_2, r13_23);
            addr_4299_14:
            fun_2800();
        }
    } else {
        rax31 = fun_2530();
        rdi32 = stderr;
        fun_2820(rdi32, 1, rax31, r12_2, r8_33, r9_34, v35, v36, v37, v38, v39, v40);
        goto addr_4299_14;
    }
}

struct s8 {
    int64_t f0;
    int64_t f8;
    int64_t f10;
    int64_t f18;
    int64_t f20;
    int64_t f28;
};

struct s8* fun_2450(void** rdi);

void fun_4603(int64_t rdi) {
    struct s8* rax2;

    __asm__("cli ");
    rax2 = fun_2450(48);
    if (rax2) {
        rax2->f0 = 0;
        rax2->f20 = rdi;
        rax2->f28 = rdi;
    }
    return;
}

void fun_4633(int64_t rdi) {
    struct s8* rax2;

    __asm__("cli ");
    rax2 = fun_2450(48);
    if (rax2) {
        rax2->f0 = rdi;
        rax2->f10 = 0;
        rax2->f18 = 0;
        rax2->f8 = 0;
        rax2->f20 = 0;
    }
    return;
}

struct s9 {
    int64_t f0;
    int64_t f8;
    int64_t f10;
    signed char[16] pad40;
    int64_t* f28;
};

int64_t fun_2860(int64_t* rdi, int64_t rsi);

uint32_t fun_26a0(int64_t rdi, int64_t rsi);

int64_t fun_4673(struct s9* rdi, uint32_t* rsi) {
    int64_t rax3;
    int64_t* rdx4;
    int64_t rsi5;
    int64_t rax6;
    int64_t rdi7;
    uint32_t eax8;

    __asm__("cli ");
    if (!rdi->f0) {
        rax3 = *rdi->f28;
        if (!rax3) {
            *rsi = 2;
        } else {
            rdx4 = rdi->f28 + 1;
            *rsi = 1;
            rdi->f28 = rdx4;
            return rax3;
        }
    } else {
        rsi5 = reinterpret_cast<int64_t>(&rdi->pad40);
        rax6 = fun_2860(&rdi->f10, rsi5);
        if (rax6 < 0) {
            rdi7 = rdi->f0;
            eax8 = fun_26a0(rdi7, rsi5);
            *rsi = (eax8 - (eax8 + reinterpret_cast<uint1_t>(eax8 < eax8 + reinterpret_cast<uint1_t>(eax8 < 1))) & 2) + 2;
            *reinterpret_cast<int32_t*>(&rax3) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        } else {
            *rsi = 1;
            rax3 = rdi->f10;
            rdi->f8 = rdi->f8 + 1;
        }
    }
    return rax3;
}

struct s10 {
    int64_t f0;
    int64_t f8;
    signed char[16] pad32;
    int64_t f20;
    int64_t f28;
};

int64_t fun_4713(struct s10* rdi) {
    __asm__("cli ");
    if (!rdi->f0) {
        return rdi->f28 - rdi->f20 >> 3;
    } else {
        return rdi->f8;
    }
}

struct s11 {
    int64_t f0;
    signed char[8] pad16;
    void** f10;
};

void fun_4733(struct s11* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rdi7;

    __asm__("cli ");
    if (rdi->f0) {
        rdi7 = rdi->f10;
        fun_2440(rdi7, rsi, rdx, rcx, r8, r9);
    }
    goto fun_2440;
}

int64_t file_name = 0;

void fun_4753(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_4763(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(struct s0* rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_24b0(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_4773() {
    struct s0* rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    struct s0* rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2490(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2530();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_4803_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2780();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_24b0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_4803_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2780();
    }
}

void fun_4823() {
    __asm__("cli ");
}

void fun_4833(struct s5* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        fun_26d0(rdi, rsi, rdx, rcx, r8, r9);
        goto 0x2610;
    }
}

int32_t fun_2710(struct s5* rdi);

int32_t rpl_fflush(struct s5* rdi);

int64_t fun_2510(struct s5* rdi);

int64_t fun_4863(struct s5* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    int32_t eax7;
    int32_t eax8;
    int32_t eax9;
    int64_t rdi10;
    void** rax11;
    int32_t eax12;
    int32_t* rax13;
    int32_t r12d14;
    int64_t rax15;

    __asm__("cli ");
    eax7 = fun_26d0(rdi, rsi, rdx, rcx, r8, r9);
    if (eax7 >= 0) {
        eax8 = fun_2710(rdi);
        if (!(eax8 && (eax9 = fun_26d0(rdi, rsi, rdx, rcx, r8, r9), *reinterpret_cast<int32_t*>(&rdi10) = eax9, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0, rax11 = fun_25c0(rdi10), reinterpret_cast<int1_t>(rax11 == 0xffffffffffffffff)) || (eax12 = rpl_fflush(rdi), eax12 == 0))) {
            rax13 = fun_2490();
            r12d14 = *rax13;
            rax15 = fun_2510(rdi);
            if (r12d14) {
                *rax13 = r12d14;
                *reinterpret_cast<int32_t*>(&rax15) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
            }
            return rax15;
        }
    }
    goto fun_2510;
}

void rpl_fseeko(struct s5* rdi);

void fun_48f3(struct s5* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2710(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_4943(struct s5* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    int32_t eax7;
    int64_t rdi8;
    void** rax9;
    int64_t rax10;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax7 = fun_26d0(rdi, rsi, rdx, rcx, r8, r9);
        *reinterpret_cast<int32_t*>(&rdi8) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
        rax9 = fun_25c0(rdi8, rdi8);
        if (rax9 == 0xffffffffffffffff) {
            *reinterpret_cast<uint32_t*>(&rax10) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax9;
            *reinterpret_cast<uint32_t*>(&rax10) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        }
        return rax10;
    }
}

struct s12 {
    signed char[20] pad20;
    signed char f14;
};

signed char* fun_49c3(uint64_t rdi, struct s12* rsi) {
    uint64_t rcx3;
    signed char* r8_4;
    uint64_t rdx5;
    uint64_t rsi6;
    uint64_t rax7;
    int32_t eax8;
    uint64_t rax9;

    __asm__("cli ");
    rsi->f14 = 0;
    rcx3 = rdi;
    r8_4 = &rsi->f14;
    do {
        --r8_4;
        rdx5 = __intrinsic() >> 3;
        rsi6 = rdx5 + rdx5 * 4;
        rax7 = rcx3 - (rsi6 + rsi6);
        eax8 = *reinterpret_cast<int32_t*>(&rax7) + 48;
        *r8_4 = *reinterpret_cast<signed char*>(&eax8);
        rax9 = rcx3;
        rcx3 = rdx5;
    } while (rax9 > 9);
    return r8_4;
}

int32_t fun_24e0(int64_t rdi);

int64_t fun_4a23(int64_t rdi, void** rsi) {
    int64_t rax3;
    int64_t rdi4;
    int32_t eax5;
    int64_t rax6;

    __asm__("cli ");
    rax3 = fun_26e0(rdi, rsi);
    if (*reinterpret_cast<int32_t*>(&rax3) < 0) {
        *reinterpret_cast<int32_t*>(&rdi4) = *reinterpret_cast<int32_t*>(&rdi);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
        eax5 = fun_24e0(rdi4);
        *reinterpret_cast<uint32_t*>(&rax6) = reinterpret_cast<uint1_t>(eax5 == 0);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    } else {
        return rax3;
    }
}

struct s13 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    signed char f10;
    signed char[3] pad20;
    int32_t f14;
    void** f18;
};

struct s14 {
    signed char[8] pad8;
    void** f8;
    signed char[7] pad16;
    unsigned char f10;
    signed char[3] pad20;
    int32_t f14;
    void** f18;
};

void** fun_26c0(void** rdi, void** rsi, void** rdx);

void fun_4a53(struct s13* rdi, struct s14* rsi) {
    void** rsi3;
    void** rcx4;
    void** rdx5;
    void** rax6;
    void** rax7;
    uint32_t eax8;

    __asm__("cli ");
    rsi3 = reinterpret_cast<void**>(&rsi->f18);
    rcx4 = *reinterpret_cast<void***>(rsi3 + 0xffffffffffffffe8);
    if (rcx4 == rsi3) {
        rdx5 = rsi->f8;
        rax6 = fun_26c0(&rdi->f18, rsi3, rdx5);
        rcx4 = rax6;
    }
    rax7 = rsi->f8;
    rdi->f0 = rcx4;
    rdi->f8 = rax7;
    eax8 = rsi->f10;
    rdi->f10 = *reinterpret_cast<signed char*>(&eax8);
    if (*reinterpret_cast<signed char*>(&eax8)) {
        rdi->f14 = rsi->f14;
    }
    return;
}

int64_t fun_4ab3(uint32_t edi) {
    uint32_t eax2;
    uint32_t ecx3;
    int64_t rax4;
    int64_t rax5;

    __asm__("cli ");
    eax2 = edi;
    ecx3 = edi;
    *reinterpret_cast<unsigned char*>(&eax2) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax2) >> 5);
    *reinterpret_cast<uint32_t*>(&rax4) = eax2 & 7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax5) = *reinterpret_cast<uint32_t*>(0x9a00 + rax4 * 4) >> *reinterpret_cast<signed char*>(&ecx3) & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    return rax5;
}

uint64_t fun_2580(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_4ad3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    int64_t rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    int64_t rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2580(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = rax5 - g28;
    if (rax9) {
        fun_2560();
    } else {
        return r12_7;
    }
}

void fun_27c0(int32_t* rdi);

int32_t g1e;

struct s15 {
    signed char[4] pad4;
    int32_t f4;
};

int32_t g22;

int32_t fun_27e0(void* rdi, int64_t rsi);

void fun_4b63() {
    int64_t rax1;
    int32_t* rsi2;
    int32_t* rsi3;
    struct s15* rsi4;
    int32_t eax5;
    int64_t v6;
    int64_t rax7;

    __asm__("cli ");
    rax1 = g28;
    fun_27c0(85);
    __asm__("pxor xmm0, xmm0");
    __asm__("cvtsi2sd xmm0, rax");
    g1e = *rsi2;
    rsi3 = &rsi4->f4;
    fun_27c0(34);
    g22 = *rsi3;
    __asm__("pxor xmm1, xmm1");
    __asm__("comisd xmm0, xmm1");
    if (static_cast<int1_t>(!1)) {
        eax5 = fun_27e0(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x98 - 8 + 8 - 8 + 8 + 16, rsi3 + 1);
        if (eax5) 
            goto addr_4bd6_12;
    } else {
        __asm__("mulsd xmm0, xmm2");
        goto addr_4bd6_12;
    }
    if (v6 < 0) {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, rdx");
        __asm__("addsd xmm0, xmm0");
    } else {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, rax");
    }
    __asm__("pxor xmm1, xmm1");
    __asm__("cvtsi2sd xmm1, rax");
    __asm__("mulsd xmm0, xmm1");
    addr_4bd6_12:
    rax7 = rax1 - g28;
    if (rax7) {
        fun_2560();
    } else {
        return;
    }
}

struct s16 {
    signed char[4] pad4;
    int32_t f4;
};

void physmem_total(void* rdi, int64_t rsi);

void fun_4c53() {
    int64_t rax1;
    int32_t* rsi2;
    int32_t* rsi3;
    struct s16* rsi4;
    int64_t rsi5;
    void* rdi6;
    int32_t eax7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t rax11;

    __asm__("cli ");
    rax1 = g28;
    fun_27c0(86);
    __asm__("pxor xmm0, xmm0");
    __asm__("cvtsi2sd xmm0, rax");
    g1e = *rsi2;
    rsi3 = &rsi4->f4;
    fun_27c0(34);
    g22 = *rsi3;
    rsi5 = reinterpret_cast<int64_t>(rsi3 + 1);
    __asm__("pxor xmm1, xmm1");
    __asm__("comisd xmm0, xmm1");
    if (static_cast<int1_t>(!1)) {
        rdi6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x98 - 8 + 8 - 8 + 8 + 16);
        eax7 = fun_27e0(rdi6, rsi5);
        if (eax7) {
            physmem_total(rdi6, rsi5);
            __asm__("mulsd xmm0, [rip+0x4cfb]");
        } else {
            if (v8 < 0) {
                __asm__("pxor xmm0, xmm0");
                __asm__("cvtsi2sd xmm0, rdx");
                __asm__("addsd xmm0, xmm0");
                if (v9 >= 0) {
                    addr_4ce3_12:
                    __asm__("pxor xmm1, xmm1");
                    __asm__("cvtsi2sd xmm1, rax");
                } else {
                    addr_4d63_13:
                    __asm__("pxor xmm1, xmm1");
                    __asm__("cvtsi2sd xmm1, rdx");
                    __asm__("addsd xmm1, xmm1");
                }
                __asm__("addsd xmm0, xmm1");
                __asm__("pxor xmm1, xmm1");
                __asm__("cvtsi2sd xmm1, rax");
                __asm__("mulsd xmm0, xmm1");
            } else {
                __asm__("pxor xmm0, xmm0");
                __asm__("cvtsi2sd xmm0, rax");
                if (v10 < 0) 
                    goto addr_4d63_13; else 
                    goto addr_4ce3_12;
            }
        }
    } else {
        __asm__("mulsd xmm0, xmm2");
    }
    rax11 = rax1 - g28;
    if (rax11) {
        fun_2560();
    } else {
        return;
    }
}

void fun_2810(void** rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

struct s17 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s17* fun_25b0();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_4d93(void** rdi) {
    struct s0* rcx2;
    void** rbx3;
    struct s17* rax4;
    void** r12_5;
    void** rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2810("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2480("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_25b0();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_24a0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<signed char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_6533(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2490();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xd280;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_6573(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xd280);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_6593(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xd280);
    }
    *rdi = esi;
    return 0xd280;
}

int64_t fun_65b3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xd280);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s18 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_65f3(struct s18* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s18*>(0xd280);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s19 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s19* fun_6613(struct s19* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s19*>(0xd280);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x288a;
    if (!rdx) 
        goto 0x288a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xd280;
}

struct s20 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_6653(void** rdi, void** rsi, void** rdx, void** rcx, struct s20* r8) {
    struct s20* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s20*>(0xd280);
    }
    rax7 = fun_2490();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x6686);
    *rax7 = r15d8;
    return rax13;
}

struct s21 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_66d3(void** rdi, void** rsi, void*** rdx, struct s21* rcx) {
    struct s21* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s21*>(0xd280);
    }
    rax6 = fun_2490();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x6701);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x675c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_67c3() {
    __asm__("cli ");
}

void** gd098 = reinterpret_cast<void**>(0x80);

int64_t slotvec0 = 0x100;

void fun_67d3() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rsi7;
    void** rdx8;
    void** rcx9;
    void** r8_10;
    void** r9_11;
    void** rdi12;
    void** rsi13;
    void** rdx14;
    void** rcx15;
    void** r8_16;
    void** r9_17;
    void** rsi18;
    void** rdx19;
    void** rcx20;
    void** r8_21;
    void** r9_22;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2440(rdi6, rsi7, rdx8, rcx9, r8_10, r9_11);
        } while (rbx4 != rbp5);
    }
    rdi12 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi12 != 0xd180) {
        fun_2440(rdi12, rsi13, rdx14, rcx15, r8_16, r9_17);
        gd098 = reinterpret_cast<void**>(0xd180);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xd090) {
        fun_2440(r12_2, rsi18, rdx19, rcx20, r8_21, r9_22);
        slotvec = reinterpret_cast<void**>(0xd090);
    }
    nslots = 1;
    return;
}

void fun_6873() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6893() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_68a3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_68c3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_68e3(void** rdi, int32_t esi, void** rdx) {
    int64_t rdx4;
    struct s1* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2890;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_2560();
    } else {
        return rax6;
    }
}

void** fun_6973(void** rdi, int32_t esi, void** rdx, void** rcx) {
    int64_t rcx5;
    struct s1* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2895;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_2560();
    } else {
        return rax7;
    }
}

void** fun_6a03(int32_t edi, void** rsi) {
    int64_t rax3;
    struct s1* rcx4;
    void** rax5;
    int64_t rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x289a;
    rcx4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, 0xffffffffffffffff, rcx4, 0, rsi, 0xffffffffffffffff, rcx4);
    rdx6 = rax3 - g28;
    if (rdx6) {
        fun_2560();
    } else {
        return rax5;
    }
}

void** fun_6a93(int32_t edi, void** rsi, void** rdx) {
    int64_t rax4;
    struct s1* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x289f;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = rax4 - g28;
    if (rdx7) {
        fun_2560();
    } else {
        return rax6;
    }
}

void** fun_6b23(void** rdi, void** rsi, uint32_t edx) {
    struct s1* rsp4;
    int64_t rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    int64_t rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6750]");
    __asm__("movdqa xmm1, [rip+0x6758]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x6741]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = rax5 - g28;
    if (rdx11) {
        fun_2560();
    } else {
        return rax10;
    }
}

void** fun_6bc3(void** rdi, uint32_t esi) {
    struct s1* rsp3;
    int64_t rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    int64_t rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x66b0]");
    __asm__("movdqa xmm1, [rip+0x66b8]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x66a1]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, 0xffffffffffffffff, rsp3);
    rdx10 = rax4 - g28;
    if (rdx10) {
        fun_2560();
    } else {
        return rax9;
    }
}

void** fun_6c63(void** rdi) {
    int64_t rax2;
    void** rax3;
    int64_t rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6610]");
    __asm__("movdqa xmm1, [rip+0x6618]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x65f9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, 0xffffffffffffffff, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = rax2 - g28;
    if (rdx4) {
        fun_2560();
    } else {
        return rax3;
    }
}

void** fun_6cf3(void** rdi, void** rsi) {
    int64_t rax3;
    void** rax4;
    int64_t rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6580]");
    __asm__("movdqa xmm1, [rip+0x6588]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x6576]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = rax3 - g28;
    if (rdx5) {
        fun_2560();
    } else {
        return rax4;
    }
}

void** fun_6d83(void** rdi, int32_t esi, void** rdx) {
    int64_t rdx4;
    struct s1* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x28a4;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_2560();
    } else {
        return rax6;
    }
}

void** fun_6e23(void** rdi, int64_t rsi, int64_t rdx, void** rcx) {
    int64_t rcx5;
    struct s1* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x644a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x6442]");
    __asm__("movdqa xmm2, [rip+0x644a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x28a9;
    if (!rdx) 
        goto 0x28a9;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, 0xffffffffffffffff, rcx6, rdi, rcx, 0xffffffffffffffff, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_2560();
    } else {
        return rax7;
    }
}

void** fun_6ec3(int32_t edi, int64_t rsi, int64_t rdx, void** rcx, void** r8) {
    int64_t rcx6;
    struct s1* rcx7;
    void** rdi8;
    void** rax9;
    int64_t rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x63aa]");
    __asm__("movdqa xmm1, [rip+0x63b2]");
    __asm__("movdqa xmm2, [rip+0x63ba]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x28ae;
    if (!rdx) 
        goto 0x28ae;
    rcx7 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = rcx6 - g28;
    if (rdx10) {
        fun_2560();
    } else {
        return rax9;
    }
}

void** fun_6f73(int64_t rdi, int64_t rsi, void** rdx) {
    int64_t rdx4;
    struct s1* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x62fa]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x62f2]");
    __asm__("movdqa xmm2, [rip+0x62fa]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x28b3;
    if (!rsi) 
        goto 0x28b3;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, 0xffffffffffffffff, rcx5, 0, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_2560();
    } else {
        return rax6;
    }
}

void** fun_7013(int64_t rdi, int64_t rsi, void** rdx, void** rcx) {
    int64_t rcx5;
    struct s1* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x625a]");
    __asm__("movdqa xmm1, [rip+0x6262]");
    __asm__("movdqa xmm2, [rip+0x626a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x28b8;
    if (!rsi) 
        goto 0x28b8;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_2560();
    } else {
        return rax7;
    }
}

void fun_70b3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_70c3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_70e3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_7103(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void _obstack_begin(int64_t rdi);

void fun_7213(int64_t rdi) {
    int64_t rdi2;

    __asm__("cli ");
    rdi2 = rdi + 24;
    *reinterpret_cast<int64_t*>(rdi2 - 24) = 0;
    *reinterpret_cast<int64_t*>(rdi2 - 16) = 0;
    *reinterpret_cast<int64_t*>(rdi2 - 8) = 0;
    _obstack_begin(rdi2);
    _obstack_begin(rdi + 0x70);
    goto _obstack_begin;
}

void _obstack_free(int64_t rdi);

void fun_7293(int64_t rdi) {
    __asm__("cli ");
    _obstack_free(rdi + 24);
    _obstack_free(rdi + 0x70);
    goto _obstack_free;
}

int32_t fun_25f0(int64_t rdi, struct s3* rsi);

int32_t fun_24d0(int64_t rdi);

int32_t fun_72c3(int64_t rdi, struct s3* rsi) {
    int64_t r13_3;
    int64_t r12_4;
    struct s3* rbp5;
    int32_t eax6;
    int32_t ebx7;
    signed char* rax8;
    signed char* rax9;
    signed char** rdx10;
    signed char** rcx11;
    signed char** rax12;
    void* rdx13;
    uint64_t* rcx14;
    signed char** rax15;
    signed char** rdx16;
    uint64_t* rax17;
    void* rdx18;
    uint64_t* rax19;
    uint64_t* rdx20;
    int32_t eax21;

    __asm__("cli ");
    r13_3 = reinterpret_cast<int64_t>(&rsi->pad32);
    r12_4 = rdi;
    rbp5 = rsi;
    while (eax6 = fun_25f0(r12_4, rsi), ebx7 = eax6, rax8 = rbp5->f30, ebx7 != -1) {
        if (rbp5->f38 != rax8) {
            rbp5->f30 = rax8 + 1;
            *rax8 = *reinterpret_cast<signed char*>(&ebx7);
            if (ebx7) 
                continue;
        } else {
            *reinterpret_cast<int32_t*>(&rsi) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi) + 4) = 0;
            _obstack_newchunk(r13_3, 1);
            rax9 = rbp5->f30;
            rbp5->f30 = rax9 + 1;
            *rax9 = *reinterpret_cast<signed char*>(&ebx7);
            if (ebx7) 
                continue;
        }
        save_token(rbp5, rsi);
    }
    if (rax8 != rbp5->f28) {
        if (rax8 == rbp5->f38) {
            *reinterpret_cast<int32_t*>(&rsi) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi) + 4) = 0;
            _obstack_newchunk(&rbp5->pad32, 1);
            rax8 = rbp5->f30;
        }
        rbp5->f30 = rax8 + 1;
        *rax8 = 0;
        save_token(rbp5, rsi);
    }
    rdx10 = rbp5->f88;
    if (reinterpret_cast<uint64_t>(rbp5->f90) - reinterpret_cast<uint64_t>(rdx10) <= 7) {
        _obstack_newchunk(reinterpret_cast<int64_t>(rbp5) + 0x70, 8);
        rdx10 = rbp5->f88;
    }
    *rdx10 = reinterpret_cast<signed char*>(0);
    rcx11 = rbp5->f80;
    rax12 = rbp5->f88 + 1;
    if (rax12 == rcx11) {
        rbp5->fc0 = reinterpret_cast<unsigned char>(rbp5->fc0 | 2);
    }
    rdx13 = rbp5->fa0;
    rbp5->f8 = rcx11;
    rcx14 = rbp5->fd8;
    rax15 = reinterpret_cast<signed char**>(reinterpret_cast<uint64_t>(rax12) + reinterpret_cast<int64_t>(rdx13) & reinterpret_cast<uint64_t>(~reinterpret_cast<int64_t>(rdx13)));
    rdx16 = rbp5->f90;
    if (reinterpret_cast<uint64_t>(rax15) - rbp5->f78 <= reinterpret_cast<uint64_t>(rdx16) - rbp5->f78) {
        rdx16 = rax15;
    }
    rax17 = rbp5->fe0;
    rbp5->f88 = rdx16;
    rbp5->f80 = rdx16;
    if (rax17 == rcx14) {
        rbp5->f118 = reinterpret_cast<unsigned char>(rbp5->f118 | 2);
    }
    rdx18 = rbp5->ff8;
    rbp5->f10 = rcx14;
    rax19 = reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rax17) + reinterpret_cast<int64_t>(rdx18) & reinterpret_cast<uint64_t>(~reinterpret_cast<int64_t>(rdx18)));
    rdx20 = rbp5->fe8;
    if (reinterpret_cast<uint64_t>(rax19) - rbp5->fd0 <= reinterpret_cast<uint64_t>(rdx20) - rbp5->fd0) {
        rdx20 = rax19;
    }
    rbp5->fe0 = rdx20;
    rbp5->fd8 = rdx20;
    eax21 = fun_24d0(r12_4);
    *reinterpret_cast<unsigned char*>(&eax21) = reinterpret_cast<uint1_t>(eax21 == 0);
    return eax21;
}

int64_t fun_2620(int64_t rdi, int64_t rsi, uint64_t rdx);

int64_t fun_74b3(int32_t edi, int64_t rsi, uint64_t rdx) {
    int32_t r13d4;
    int64_t rbp5;
    uint64_t rbx6;
    int64_t rdi7;
    int64_t rax8;
    int32_t* rax9;

    __asm__("cli ");
    r13d4 = edi;
    rbp5 = rsi;
    rbx6 = rdx;
    while (*reinterpret_cast<int32_t*>(&rdi7) = r13d4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0, rax8 = fun_2620(rdi7, rbp5, rbx6), rax8 < 0) {
        rax9 = fun_2490();
        if (*rax9 == 4) 
            continue;
        if (rbx6 <= 0x7ff00000) 
            break;
        if (*rax9 != 22) 
            break;
        *reinterpret_cast<int32_t*>(&rbx6) = 0x7ff00000;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
    }
    return rax8;
}

struct s22 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2680(int64_t rdi, struct s0* rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_7523(struct s0* rdi, void** rsi, void** rdx, void** rcx, struct s22* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    void** rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    void** rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2820(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2820(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2530();
    fun_2820(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2680(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2530();
    fun_2820(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2680(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2530();
        fun_2820(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xa148 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0xa148;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_7993() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s23 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_79b3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s23* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s23* rcx8;
    int64_t rax9;
    int64_t v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    int64_t rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = v10 - g28;
    if (rax18) {
        fun_2560();
    } else {
        return;
    }
}

void fun_7a53(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    int64_t rax15;
    int64_t v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    int64_t rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_7af6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_7b00_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = v16 - g28;
    if (rax20) {
        fun_2560();
    } else {
        return;
    }
    addr_7af6_5:
    goto addr_7b00_7;
}

void fun_7b33() {
    struct s0* rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2680(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2530();
    fun_2740(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2530();
    fun_2740(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2530();
    goto fun_2740;
}

int64_t fun_24f0();

void fun_7bd3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_24f0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7c13(void** rdi) {
    struct s8* rax2;

    __asm__("cli ");
    rax2 = fun_2450(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7c33(void** rdi) {
    struct s8* rax2;

    __asm__("cli ");
    rax2 = fun_2450(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7c53(void** rdi) {
    struct s8* rax2;

    __asm__("cli ");
    rax2 = fun_2450(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_2720();

void fun_7c73(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2720();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7ca3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2720();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7cd3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_24f0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7d13() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_24f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7d53(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_24f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7d83(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_24f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7dd3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_24f0();
            if (rax5) 
                break;
            addr_7e1d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_7e1d_5;
        rax8 = fun_24f0();
        if (rax8) 
            goto addr_7e06_9;
        if (rbx4) 
            goto addr_7e1d_5;
        addr_7e06_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_7e63(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_24f0();
            if (rax8) 
                break;
            addr_7eaa_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_7eaa_5;
        rax11 = fun_24f0();
        if (rax11) 
            goto addr_7e92_9;
        if (!rbx6) 
            goto addr_7e92_9;
        if (r12_4) 
            goto addr_7eaa_5;
        addr_7e92_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_7ef3(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_7f9d_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_7fb0_10:
                *r12_8 = 0;
            }
            addr_7f50_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_7f76_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_7fc4_14;
            if (rcx10 <= rsi9) 
                goto addr_7f6d_16;
            if (rsi9 >= 0) 
                goto addr_7fc4_14;
            addr_7f6d_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_7fc4_14;
            addr_7f76_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2720();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_7fc4_14;
            if (!rbp13) 
                break;
            addr_7fc4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_7f9d_9;
        } else {
            if (!r13_6) 
                goto addr_7fb0_10;
            goto addr_7f50_11;
        }
    }
}

int64_t fun_2660();

void fun_7ff3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2660();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8023() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2660();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8053() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2660();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8073() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2660();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_8093(int64_t rdi, void** rsi) {
    struct s8* rax3;

    __asm__("cli ");
    rax3 = fun_2450(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_26c0;
    }
}

void fun_80d3(int64_t rdi, void** rsi) {
    struct s8* rax3;

    __asm__("cli ");
    rax3 = fun_2450(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_26c0;
    }
}

struct s24 {
    signed char[1] pad1;
    void** f1;
};

void fun_8113(int64_t rdi, struct s24* rsi) {
    struct s8* rax3;

    __asm__("cli ");
    rax3 = fun_2450(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_26c0;
    }
}

void** fun_2550(void** rdi, ...);

void fun_8153(void** rdi) {
    void** rax2;
    struct s8* rax3;

    __asm__("cli ");
    rax2 = fun_2550(rdi);
    rax3 = fun_2450(rax2 + 1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_26c0;
    }
}

void fun_8193() {
    void** rdi1;

    __asm__("cli ");
    fun_2530();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_2780();
    fun_2480(rdi1);
}

int64_t fun_24c0();

int64_t fun_81d3(struct s5* rdi, void*** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    int64_t rax7;
    uint32_t ebx8;
    int64_t rax9;
    int32_t* rax10;
    int32_t* rax11;

    __asm__("cli ");
    rax7 = fun_24c0();
    ebx8 = rdi->f0 & 32;
    rax9 = rpl_fclose(rdi, rsi, rdx, rcx, r8, r9);
    if (ebx8) {
        if (*reinterpret_cast<int32_t*>(&rax9)) {
            addr_822e_3:
            *reinterpret_cast<int32_t*>(&rax9) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        } else {
            rax10 = fun_2490();
            *rax10 = 0;
            *reinterpret_cast<int32_t*>(&rax9) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax9)) {
            if (rax7) 
                goto addr_822e_3;
            rax11 = fun_2490();
            *reinterpret_cast<int32_t*>(&rax9) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax11 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        }
    }
    return rax9;
}

int32_t setlocale_null_r();

int64_t fun_8243() {
    int64_t rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    int64_t rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = rax1 - g28;
    if (rdx7) {
        fun_2560();
    } else {
        return rax3;
    }
}

signed char* fun_2700(int64_t rdi);

signed char* fun_82c3() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2700(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

struct s26 {
    void* f0;
    int64_t f8;
};

struct s25 {
    void*** f0;
    struct s26* f8;
    uint64_t f10;
    uint64_t f18;
    void* f20;
    signed char[8] pad48;
    void* f30;
    int64_t f38;
    signed char[8] pad72;
    void*** f48;
    unsigned char f50;
};

int64_t obstack_alloc_failed_handler = 0x8300;

void fun_8303() {
    void** rax1;
    struct s0* rdi2;
    void*** rsi3;
    void** r8_4;
    void** r9_5;
    int64_t rax6;
    int64_t v7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    struct s25* rdi11;
    void* r12_12;
    void** rbp13;
    int64_t rax14;
    void*** rdi15;
    struct s26* rax16;
    uint64_t rdx17;
    void* rdx18;

    __asm__("cli ");
    rax1 = fun_2530();
    rdi2 = stderr;
    *reinterpret_cast<int32_t*>(&rsi3) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi3) + 4) = 0;
    fun_2820(rdi2, 1, "%s\n", rax1, r8_4, r9_5, rax6, __return_address(), v7, v8, v9, v10);
    *reinterpret_cast<int32_t*>(&rdi11) = exit_failure;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi11) + 4) = 0;
    fun_2800();
    if (0) {
        *reinterpret_cast<int32_t*>(&r12_12) = 15;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_12) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rbp13) = 16;
        *reinterpret_cast<int32_t*>(&rbp13 + 4) = 0;
    } else {
        rbp13 = reinterpret_cast<void**>("%s\n");
        r12_12 = reinterpret_cast<void*>(" %s\n");
    }
    rdi11->f30 = r12_12;
    if (0) {
        rsi3 = reinterpret_cast<void***>("fstat");
    }
    rax14 = rdi11->f38;
    rdi11->f0 = rsi3;
    if (!(rdi11->f50 & 1)) {
        rdi15 = rsi3;
        rax16 = reinterpret_cast<struct s26*>(rax14(rdi15));
    } else {
        rdi15 = rdi11->f48;
        rax16 = reinterpret_cast<struct s26*>(rax14(rdi15));
    }
    rdi11->f8 = rax16;
    if (!rax16) {
        obstack_alloc_failed_handler(rdi15);
    } else {
        rdx17 = reinterpret_cast<int64_t>(rax16) + reinterpret_cast<uint64_t>(r12_12) + 16 & reinterpret_cast<uint64_t>(-reinterpret_cast<unsigned char>(rbp13));
        rdi11->f10 = rdx17;
        rdi11->f18 = rdx17;
        rdx18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdi11->f0) + reinterpret_cast<int64_t>(rax16));
        rax16->f0 = rdx18;
        rdi11->f20 = rdx18;
        rax16->f8 = 0;
        rdi11->f50 = reinterpret_cast<unsigned char>(rdi11->f50 & 0xf9);
        goto rax6;
    }
}

struct s27 {
    signed char[56] pad56;
    int64_t f38;
    int64_t f40;
    signed char[8] pad80;
    unsigned char f50;
};

void fun_83f3(struct s27* rdi) {
    int64_t rcx2;
    int64_t r8_3;

    __asm__("cli ");
    rdi->f50 = reinterpret_cast<unsigned char>(rdi->f50 & 0xfe);
    rdi->f38 = rcx2;
    rdi->f40 = r8_3;
    goto 0x8350;
}

struct s28 {
    signed char[56] pad56;
    int64_t f38;
    int64_t f40;
    int64_t f48;
    unsigned char f50;
};

void fun_8413(struct s28* rdi) {
    int64_t rcx2;
    int64_t r8_3;
    int64_t r9_4;

    __asm__("cli ");
    rdi->f50 = reinterpret_cast<unsigned char>(rdi->f50 | 1);
    rdi->f38 = rcx2;
    rdi->f40 = r8_3;
    rdi->f48 = r9_4;
    goto 0x8350;
}

struct s30 {
    void* f0;
    struct s30* f8;
};

struct s29 {
    struct s29* f0;
    struct s30* f8;
    void** f10;
    signed char[7] pad24;
    void* f18;
    void* f20;
    signed char[8] pad48;
    void* f30;
    int64_t f38;
    int64_t f40;
    struct s29* f48;
    unsigned char f50;
};

void** fun_8433(struct s29* rdi, struct s29* rsi) {
    int64_t rcx3;
    struct s29* rbx4;
    void** r13_5;
    struct s29* tmp64_6;
    struct s30* rbp7;
    struct s29* tmp64_8;
    struct s29* rsi9;
    struct s29* rax10;
    int64_t rax11;
    struct s30* rax12;
    struct s30* r12_13;
    struct s30* rax14;
    void* rax15;
    void** rsi16;
    void** r14_17;
    void** rax18;
    uint32_t edx19;
    int64_t rax20;
    struct s29* rdi21;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rcx3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    rbx4 = rdi;
    r13_5 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi->f18) - reinterpret_cast<unsigned char>(rdi->f10));
    tmp64_6 = reinterpret_cast<struct s29*>(reinterpret_cast<uint64_t>(rsi) + reinterpret_cast<unsigned char>(r13_5));
    rbp7 = rdi->f8;
    *reinterpret_cast<unsigned char*>(&rcx3) = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(tmp64_6) < reinterpret_cast<uint64_t>(rsi));
    tmp64_8 = reinterpret_cast<struct s29*>(reinterpret_cast<uint64_t>(tmp64_6) + reinterpret_cast<int64_t>(rdi->f30));
    rsi9 = tmp64_8;
    *reinterpret_cast<unsigned char*>(&rdi) = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(tmp64_8) < reinterpret_cast<uint64_t>(tmp64_6));
    rax10 = reinterpret_cast<struct s29*>(reinterpret_cast<uint64_t>(rsi9) + (reinterpret_cast<unsigned char>(r13_5) >> 3) + 100);
    if (reinterpret_cast<uint64_t>(rsi9) < reinterpret_cast<uint64_t>(rbx4->f0)) {
        rsi9 = rbx4->f0;
    }
    if (reinterpret_cast<uint64_t>(rax10) >= reinterpret_cast<uint64_t>(rsi9)) {
        rsi9 = rax10;
    }
    if (!rcx3 && (*reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<unsigned char*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi) + 4) = 0, !rdi)) {
        rax11 = rbx4->f38;
        if (rbx4->f50 & 1) {
            rdi = rbx4->f48;
            rax12 = reinterpret_cast<struct s30*>(rax11(rdi));
            r12_13 = rax12;
        } else {
            rdi = rsi9;
            rax14 = reinterpret_cast<struct s30*>(rax11(rdi));
            r12_13 = rax14;
        }
        if (r12_13) {
            rax15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r12_13) + reinterpret_cast<uint64_t>(rsi9));
            rbx4->f8 = r12_13;
            rsi16 = rbx4->f10;
            r12_13->f8 = rbp7;
            rbx4->f20 = rax15;
            r12_13->f0 = rax15;
            r14_17 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_13) + reinterpret_cast<int64_t>(rbx4->f30) + 16) & reinterpret_cast<uint64_t>(~reinterpret_cast<int64_t>(rbx4->f30)));
            rax18 = fun_26c0(r14_17, rsi16, r13_5);
            edx19 = rbx4->f50;
            if (!(*reinterpret_cast<unsigned char*>(&edx19) & 2) && (rax18 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(~reinterpret_cast<int64_t>(rbx4->f30)) & reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp7) + reinterpret_cast<int64_t>(rbx4->f30) + 16)), rbx4->f10 == rax18)) {
                r12_13->f8 = rbp7->f8;
                rax20 = rbx4->f40;
                if (!(edx19 & 1)) {
                    rax18 = reinterpret_cast<void**>(rax20(rbp7, rsi16));
                } else {
                    rdi21 = rbx4->f48;
                    rax18 = reinterpret_cast<void**>(rax20(rdi21, rbp7));
                }
            }
            rbx4->f10 = r14_17;
            rbx4->f18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r14_17) + reinterpret_cast<unsigned char>(r13_5));
            rbx4->f50 = reinterpret_cast<unsigned char>(rbx4->f50 & 0xfd);
            return rax18;
        }
    }
    obstack_alloc_failed_handler(rdi);
}

struct s31 {
    struct s31* f0;
    struct s31* f8;
};

struct s32 {
    signed char[8] pad8;
    struct s31* f8;
};

struct s31* fun_8563(struct s32* rdi, struct s31* rsi) {
    struct s31* rax3;

    __asm__("cli ");
    rax3 = rdi->f8;
    if (!rax3) {
        return rax3;
    }
    do {
        if (reinterpret_cast<uint64_t>(rsi) <= reinterpret_cast<uint64_t>(rax3)) 
            continue;
        if (reinterpret_cast<uint64_t>(rax3->f0) >= reinterpret_cast<uint64_t>(rsi)) 
            break;
        rax3 = rax3->f8;
    } while (rax3);
    goto addr_8583_7;
    return 1;
    addr_8583_7:
    return 0;
}

struct s34 {
    struct s34* f0;
    struct s34* f8;
};

struct s33 {
    signed char[8] pad8;
    struct s34* f8;
    struct s34* f10;
    struct s34* f18;
    struct s34* f20;
    signed char[24] pad64;
    int64_t f40;
    int64_t f48;
    unsigned char f50;
};

void fun_85a3(struct s33* rdi, struct s34* rsi) {
    struct s34* r12_3;
    struct s34* rsi4;
    struct s33* rbx5;
    struct s34* rax6;
    struct s34* rbp7;
    int64_t rax8;
    int64_t rdi9;

    __asm__("cli ");
    r12_3 = rsi;
    rsi4 = rdi->f8;
    rbx5 = rdi;
    if (rsi4) {
        while (reinterpret_cast<uint64_t>(rsi4) >= reinterpret_cast<uint64_t>(r12_3) || (rax6 = rsi4->f0, reinterpret_cast<uint64_t>(rax6) < reinterpret_cast<uint64_t>(r12_3))) {
            rbp7 = rsi4->f8;
            rax8 = rbx5->f40;
            if (rbx5->f50 & 1) {
                rdi9 = rbx5->f48;
                rax8(rdi9);
                rbx5->f50 = reinterpret_cast<unsigned char>(rbx5->f50 | 2);
                if (!rbp7) 
                    goto addr_85fb_5;
            } else {
                rax8(rsi4);
                rbx5->f50 = reinterpret_cast<unsigned char>(rbx5->f50 | 2);
                if (!rbp7) 
                    goto addr_85fb_5;
            }
            rsi4 = rbp7;
        }
    } else {
        goto addr_85fb_5;
    }
    rbx5->f18 = r12_3;
    rbx5->f10 = r12_3;
    rbx5->f20 = rax6;
    rbx5->f8 = rsi4;
    return;
    addr_85fb_5:
    if (r12_3) 
        goto 0x28bd;
    return;
}

struct s36 {
    int64_t f0;
    struct s36* f8;
};

struct s35 {
    signed char[8] pad8;
    struct s36* f8;
};

int64_t fun_8633(struct s35* rdi) {
    struct s36* rax2;
    int64_t r8_3;
    int64_t rdx4;

    __asm__("cli ");
    rax2 = rdi->f8;
    *reinterpret_cast<int32_t*>(&r8_3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_3) + 4) = 0;
    if (rax2) {
        do {
            rdx4 = rax2->f0 - reinterpret_cast<int64_t>(rax2);
            rax2 = rax2->f8;
            r8_3 = r8_3 + rdx4;
        } while (rax2);
    }
    return r8_3;
}

int64_t fun_8663(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2730(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax6 = fun_2550(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_26c0(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_26c0(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_8713() {
    __asm__("cli ");
    goto fun_2730;
}

int64_t fun_8723(int64_t rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    int64_t rax5;
    int32_t r13d6;
    int64_t rax7;
    int64_t rax8;

    __asm__("cli ");
    rax5 = g28;
    *reinterpret_cast<unsigned char*>(&r13d6) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdx == 0)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(rcx == 0)));
    if (!*reinterpret_cast<unsigned char*>(&r13d6)) {
    }
    __asm__("in eax, dx");
    rax7 = rax5 - g28;
    if (!rax7) 
        goto addr_88ee_7;
    fun_2560();
    addr_88ee_7:
    *reinterpret_cast<int32_t*>(&rax8) = r13d6;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    return rax8;
}

void fun_87bc() {
    uint1_t zf1;

    if (!reinterpret_cast<uint1_t>(static_cast<uint32_t>(zf1))) 
        goto 0x87a7;
    __asm__("in eax, dx");
    __asm__("in al, dx");
}

void fun_87c2() {
    uint64_t rax1;
    uint32_t eax2;
    int64_t* v3;
    int64_t r14_4;
    int64_t* v5;
    int64_t rbx6;

    __asm__("outsd ");
    *reinterpret_cast<uint32_t*>(&rax1) = eax2 & 0x1a15;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax1) + 4) = 0;
    if (!rax1) {
        *v3 = r14_4;
        *v5 = rbx6;
    }
    if (0) {
    }
    if (rax1 > 63) 
        goto addr_87ef_9;
    addr_87ef_9:
}

void fun_87f4() {
    int64_t rcx1;
    uint1_t zf2;

    if (reinterpret_cast<uint1_t>(!!(rcx1 - 1)) & zf2) 
        goto 0x87e5;
    __asm__("fisttp dword [rcx+rcx*4-0x6]");
}

void fun_87ff() {
    __asm__("outsd ");
    __asm__("ror dword [rcx-0x73], cl");
}

void fun_8811() {
    __asm__("fnsave [rdx+rcx-0x3b]");
    __asm__("fnsave [rdx+rax*2+0x20]");
}

void fun_881e() {
    __asm__("in eax, dx");
}

void fun_8822() {
    __asm__("in eax, 0xf8");
    __asm__("rol ch, 0xfd");
    __asm__("outsd ");
    __asm__("rol ebp, 1");
    __asm__("outsd ");
    __asm__("fmul dword [rax+0x39]");
    return;
}

void fun_8833() {
}

void fun_8854() {
    int64_t rcx1;
    int64_t rcx2;
    int64_t rax3;
    int64_t rcx4;
    int64_t rax5;
    int32_t eax6;

    __asm__("rol dword [rsp+rax*8], cl");
    if (!rcx1) {
        *reinterpret_cast<int32_t*>(rcx2 + rax3 - 62) = *reinterpret_cast<int32_t*>(rcx4 + rax5 - 62) + eax6;
    }
}

void fun_8860() {
    int1_t sf1;
    int1_t sf2;

    if (!sf1) 
        goto 0x8827;
    __asm__("rol dword [rax], 0xc5");
    if (!sf2) 
        goto 0x882c;
    __asm__("retf 0x4400");
}

void fun_886d() {
    int1_t sf1;
    int64_t rcx2;
    int32_t ebp3;
    int32_t eax4;

    if (sf1) {
        __asm__("rol dword [rsp+rax*2], 0x1");
        return;
    }
    __asm__("outsd ");
    __asm__("fld st4");
    if (!rcx2) 
        goto addr_8845_5;
    addr_8845_5:
    __asm__("outsd ");
    __asm__("rol ch, 1");
    if (ebp3 + eax4 >= 0) 
        goto 0x8814;
    return;
}

void fun_8877() {
    return;
}

void fun_887d(int64_t rdi) {
    int1_t sf2;
    int64_t rax3;
    int64_t rax4;
    int64_t tmp64_5;

    if (sf2) {
        __asm__("enter 0x4800, 0x63");
        __asm__("rol byte [rbp+0x1], cl");
        __asm__("enter 0x79c5, 0xc5");
        __asm__("enter 0x4504, 0x1");
        __asm__("enter 0x634d, 0xc0");
        if (rdi == rax3) 
            goto 0x87b0;
        do {
            rax4 = tmp64_5;
        } while (rdi != rax4);
        goto 0x87b0;
    }
}

void fun_88c9() {
    __asm__("out dx, eax");
    __asm__("ror byte [rcx+rcx*4-0x8], 0xc5");
    __asm__("outsd ");
    __asm__("enter 0x5ae9, 0xff");
}

struct s37 {
    signed char[49] pad49;
    int32_t f31;
};

struct s38 {
    signed char[49] pad49;
    int32_t f31;
};

void fun_88d7() {
    struct s37* rbp1;
    struct s38* rbp2;

    rbp1->f31 = rbp2->f31 + 1;
}

void fun_8917() {
    uint1_t zf1;

    if (!reinterpret_cast<uint1_t>(static_cast<uint32_t>(zf1))) 
        goto 0x8905;
    __asm__("ror byte [rax-0x75], 0x54");
}

void fun_8928() {
    uint1_t zf1;
    int64_t rcx2;
    int64_t rcx3;

    if (!reinterpret_cast<uint1_t>(static_cast<uint32_t>(zf1))) 
        goto 0x8913;
    __asm__("in al, 0xff");
    *reinterpret_cast<int32_t*>(rcx2 - 0x77) = *reinterpret_cast<int32_t*>(rcx3 - 0x77) - 1;
}

void fun_8932() {
    fun_2490();
    fun_2780();
    goto 0x88db;
}

void fun_8963() {
    __asm__("cli ");
}

void fun_8977() {
    __asm__("cli ");
    return;
}

void fun_2a40() {
    goto 0x29f0;
}

void fun_3bc0() {
    uint64_t v1;
    uint64_t rbp2;

    if (v1 >= rbp2) {
    }
}

void fun_3f10() {
    uint64_t rbx1;
    uint64_t r13_2;

    if (rbx1 < r13_2) {
    }
}

uint32_t fun_2630(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_4fc5() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    uint64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void** r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    void** rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void*** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    uint64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    uint64_t rax104;
    uint64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    int64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2530();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2530();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2550(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_52c3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_52c3_22; else 
                            goto addr_56bd_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_577d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_5ad0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_52c0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_52c0_30; else 
                                goto addr_5ae9_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2550(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_5ad0_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2630(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_5ad0_28; else 
                            goto addr_516c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_5c30_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_5ab0_40:
                        if (r11_27 == 1) {
                            addr_563d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_5bf8_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_5277_44;
                            }
                        } else {
                            goto addr_5ac0_46;
                        }
                    } else {
                        addr_5c3f_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_563d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_52c3_22:
                                if (v47 != 1) {
                                    addr_5819_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2550(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_5864_54;
                                    }
                                } else {
                                    goto addr_52d0_56;
                                }
                            } else {
                                addr_5275_57:
                                ebp36 = 0;
                                goto addr_5277_44;
                            }
                        } else {
                            addr_5aa4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_5c3f_47; else 
                                goto addr_5aae_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_563d_41;
                        if (v47 == 1) 
                            goto addr_52d0_56; else 
                            goto addr_5819_52;
                    }
                }
                addr_5331_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_51c8_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_51ed_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_54f0_65;
                    } else {
                        addr_5359_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_5ba8_67;
                    }
                } else {
                    goto addr_5350_69;
                }
                addr_5201_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_524c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_5ba8_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_524c_81;
                }
                addr_5350_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_51ed_64; else 
                    goto addr_5359_66;
                addr_5277_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_532f_91;
                if (v22) 
                    goto addr_528f_93;
                addr_532f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_5331_62;
                addr_5864_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void**>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v62) - reinterpret_cast<unsigned char>(r13_69));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70, rdx10, rcx44);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_5feb_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_605b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_5e5f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(&rdi75 + 4) = 0;
                    eax77 = fun_2840(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2830(r12_64, rsi70, rdx10, rcx44);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_595e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_531c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_5968_112;
                    }
                } else {
                    addr_5968_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_5a39_114;
                }
                addr_5328_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_532f_91;
                while (1) {
                    addr_5a39_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_5f47_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_59a6_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_5f55_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_5a27_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_5a27_128;
                        }
                    }
                    addr_59d5_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_5a27_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_59a6_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_59d5_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_524c_81;
                addr_5f55_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_5ba8_67;
                addr_5feb_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_595e_109;
                addr_605b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_595e_109;
                addr_52d0_56:
                rax93 = fun_2870(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*rax93 + reinterpret_cast<unsigned char>(rax24) * 2) + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_531c_110;
                addr_5aae_59:
                goto addr_5ab0_40;
                addr_577d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_52c3_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_5328_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_5275_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_52c3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_57c2_160;
                if (!v22) 
                    goto addr_5b97_162; else 
                    goto addr_5da3_163;
                addr_57c2_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_5b97_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_5ba8_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_566b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_54d3_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_5201_70; else 
                    goto addr_54e7_169;
                addr_566b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_51c8_63;
                goto addr_5350_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_5aa4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_5bdf_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_52c0_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_51b8_178; else 
                        goto addr_5b62_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5aa4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_52c3_22;
                }
                addr_5bdf_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_52c0_30:
                    r8d42 = 0;
                    goto addr_52c3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_5331_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_5bf8_42;
                    }
                }
                addr_51b8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_51c8_63;
                addr_5b62_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_5ac0_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_5331_62;
                } else {
                    addr_5b72_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_52c3_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_6322_188;
                if (v28) 
                    goto addr_5b97_162;
                addr_6322_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_54d3_168;
                addr_516c_37:
                if (v22) 
                    goto addr_6163_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_5183_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_5c30_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_5cbb_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_52c3_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_51b8_178; else 
                        goto addr_5c97_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5aa4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_52c3_22;
                }
                addr_5cbb_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_52c3_22;
                }
                addr_5c97_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_5ac0_46;
                goto addr_5b72_186;
                addr_5183_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_52c3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_52c3_22; else 
                    goto addr_5194_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_626e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_60f4_210;
            if (1) 
                goto addr_60f2_212;
            if (!v29) 
                goto addr_5d2e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2540();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_6261_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_54f0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_52ab_219; else 
            goto addr_550a_220;
        addr_528f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_52a3_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_550a_220; else 
            goto addr_52ab_219;
        addr_5e5f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_550a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2540();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_5e7d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2540();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_62f0_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_5d56_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_5f47_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_52a3_221;
        addr_5da3_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_52a3_221;
        addr_54e7_169:
        goto addr_54f0_65;
        addr_626e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_550a_220;
        goto addr_5e7d_222;
        addr_60f4_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = v112 - g28;
        if (!rax111) 
            goto addr_614e_236;
        fun_2560();
        rsp25 = rsp25 - 8 + 8;
        goto addr_62f0_225;
        addr_60f2_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_60f4_210;
        addr_5d2e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_60f4_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_5d56_226;
        }
        addr_6261_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_56bd_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x9c0c + rax113 * 4) + 0x9c0c;
    addr_5ae9_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x9d0c + rax114 * 4) + 0x9d0c;
    addr_6163_190:
    addr_52ab_219:
    goto 0x4f90;
    addr_5194_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x9b0c + rax115 * 4) + 0x9b0c;
    addr_614e_236:
    goto v116;
}

void fun_51b0() {
}

void fun_5368() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x5062;
}

void fun_53c1() {
    goto 0x5062;
}

void fun_54ae() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x5331;
    }
    if (v2) 
        goto 0x5da3;
    if (!r10_3) 
        goto addr_5f0e_5;
    if (!v4) 
        goto addr_5dde_7;
    addr_5f0e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_5dde_7:
    goto 0x51e4;
}

void fun_54cc() {
}

void fun_5577() {
    signed char v1;

    if (v1) {
        goto 0x54ff;
    } else {
        goto 0x523a;
    }
}

void fun_5591() {
    signed char v1;

    if (!v1) 
        goto 0x558a; else 
        goto "???";
}

void fun_55b8() {
    goto 0x54d3;
}

void fun_5638() {
}

void fun_5650() {
}

void fun_567f() {
    goto 0x54d3;
}

void fun_56d1() {
    goto 0x5660;
}

void fun_5700() {
    goto 0x5660;
}

void fun_5733() {
    goto 0x5660;
}

void fun_5b00() {
    goto 0x51b8;
}

void fun_5dfe() {
    signed char v1;

    if (v1) 
        goto 0x5da3;
    goto 0x51e4;
}

void fun_5ea5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x51e4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x51c8;
        goto 0x51e4;
    }
}

void fun_62c2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x5530;
    } else {
        goto 0x5062;
    }
}

void fun_75f8() {
    fun_2530();
}

void fun_8850() {
}

void fun_887a() {
}

void fun_2a50() {
    debug = 1;
    goto 0x29f0;
}

void fun_3be8() {
    goto 0x3bd8;
}

void fun_3f30() {
    goto 0x3f20;
}

void fun_53ee() {
    goto 0x5062;
}

void fun_55c4() {
    goto 0x557c;
}

void fun_568b() {
    goto 0x51b8;
}

void fun_56dd() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x5660;
    goto 0x528f;
}

void fun_570f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x566b;
        goto 0x5090;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x550a;
        goto 0x52ab;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x5ea8;
    if (r10_8 > r15_9) 
        goto addr_55f5_9;
    addr_55fa_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x5eb3;
    goto 0x51e4;
    addr_55f5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_55fa_10;
}

void fun_5742() {
    goto 0x5277;
}

void fun_5b10() {
    goto 0x5277;
}

void fun_62af() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x53cc;
    } else {
        goto 0x5530;
    }
}

void fun_76b0() {
}

void fun_2a60() {
    print_words = 1;
    goto 0x29f0;
}

void fun_3bf8() {
    goto 0x3bd8;
}

void fun_3f40() {
    goto 0x3f20;
}

void fun_574c() {
    goto 0x56e7;
}

void fun_5b1a() {
    goto 0x563d;
}

void fun_7710() {
    fun_2530();
    goto fun_2820;
}

void fun_2a70() {
    print_chars = 1;
    goto 0x29f0;
}

void fun_3c00() {
    goto 0x3bc5;
}

void fun_541d() {
    goto 0x5062;
}

void fun_5758() {
    goto 0x56e7;
}

void fun_5b27() {
    goto 0x568e;
}

void fun_7750() {
    fun_2530();
    goto fun_2820;
}

void fun_2a80() {
    print_lines = 1;
    goto 0x29f0;
}

void fun_3c10() {
    goto 0x3bd8;
}

void fun_544a() {
    goto 0x5062;
}

void fun_5764() {
    goto 0x5660;
}

void fun_7790() {
    fun_2530();
    goto fun_2820;
}

void fun_2a90() {
    print_bytes = 1;
    goto 0x29f0;
}

void fun_546c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x5e00;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x5331;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x5331;
    }
    if (v11) 
        goto 0x6163;
    if (r10_12 > r15_13) 
        goto addr_61b3_8;
    addr_61b8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x5ef1;
    addr_61b3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_61b8_9;
}

struct s39 {
    signed char[24] pad24;
    int64_t f18;
};

struct s40 {
    signed char[16] pad16;
    void** f10;
};

struct s41 {
    signed char[8] pad8;
    void** f8;
};

void fun_77e0() {
    int64_t r15_1;
    struct s39* rbx2;
    void** r14_3;
    struct s40* rbx4;
    void** r13_5;
    struct s41* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    struct s0* rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2530();
    fun_2820(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x7802, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_2aa0() {
    print_linelength = 1;
    goto 0x29f0;
}

void fun_7838() {
    fun_2530();
    goto 0x7809;
}

struct s42 {
    signed char[32] pad32;
    int64_t f20;
};

struct s43 {
    signed char[24] pad24;
    int64_t f18;
};

struct s44 {
    signed char[16] pad16;
    void** f10;
};

struct s45 {
    signed char[8] pad8;
    void** f8;
};

struct s46 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_7870() {
    int64_t rcx1;
    struct s42* rbx2;
    int64_t r15_3;
    struct s43* rbx4;
    void** r14_5;
    struct s44* rbx6;
    void** r13_7;
    struct s45* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s46* rbx12;
    void** rax13;
    struct s0* rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2530();
    fun_2820(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x78a4, __return_address(), rcx1);
    goto v15;
}

void fun_78e8() {
    fun_2530();
    goto 0x78ab;
}
