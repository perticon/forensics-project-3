#include <stdint.h>

/* /tmp/tmpvpgaznxb @ 0x3330 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (dbg.main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmpvpgaznxb @ 0x8630 */
 
int64_t dbg_obstack_memory_used (obstack * h) {
    rdi = h;
    /* size_t _obstack_memory_used(obstack * h); */
    rax = *((rdi + 8));
    r8d = 0;
    if (rax == 0) {
        goto label_0;
    }
    do {
        rdx = *(rax);
        rdx -= rax;
        rax = *((rax + 8));
        r8 += rdx;
    } while (rax != 0);
label_0:
    rax = r8;
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x83f0 */
 
void dbg_obstack_begin (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* int _obstack_begin(obstack * h,size_t size,size_t alignment,void * (*)() chunkfun,void (*)() freefun); */
    *((rdi + 0x50)) &= 0xfe;
    *((rdi + 0x38)) = rcx;
    *((rdi + 0x40)) = r8;
    return void (*0x8350)() ();
}

/* /tmp/tmpvpgaznxb @ 0x85a0 */
 
int64_t obstack_free (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rsi;
    rsi = *((rdi + 8));
    rbx = rdi;
    if (rsi != 0) {
        goto label_1;
    }
    goto label_2;
    do {
        rdi = *((rbx + 0x48));
        void (*rax)() ();
        *((rbx + 0x50)) |= 2;
        if (rbp == 0) {
            goto label_2;
        }
label_0:
        rsi = rbp;
label_1:
        if (rsi < r12) {
            rax = *(rsi);
            if (rax >= r12) {
                goto label_3;
            }
        }
        rbp = *((rsi + 8));
        rax = *((rbx + 0x40));
    } while ((*((rbx + 0x50)) & 1) != 0);
    rdi = rsi;
    rax = void (*rax)() ();
    *((rbx + 0x50)) |= 2;
    if (rbp != 0) {
        goto label_0;
    }
label_2:
    if (r12 != 0) {
        void (*0x28bd)() ();
    }
    return rax;
label_3:
    *((rbx + 0x18)) = r12;
    *((rbx + 0x10)) = r12;
    *((rbx + 0x20)) = rax;
    *((rbx + 8)) = rsi;
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x8560 */
 
int64_t dbg_obstack_allocated_p (obstack * h, void * obj) {
    rdi = h;
    rsi = obj;
    /* int _obstack_allocated_p(obstack * h,void * obj); */
    rax = *((rdi + 8));
    if (rax == 0) {
        goto label_0;
    }
    do {
        if (rsi > rax) {
            if (*(rax) >= rsi) {
                goto label_1;
            }
        }
        rax = *((rax + 8));
    } while (rax != 0);
    eax = 0;
    return rax;
label_1:
    eax = 1;
    return rax;
label_0:
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x8410 */
 
void dbg_obstack_begin_1 (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* int _obstack_begin_1(obstack * h,size_t size,size_t alignment,void * (*)() chunkfun,void (*)() freefun,void * arg); */
    *((rdi + 0x50)) |= 1;
    *((rdi + 0x38)) = rcx;
    *((rdi + 0x40)) = r8;
    *((rdi + 0x48)) = r9;
    return void (*0x8350)() ();
}

/* /tmp/tmpvpgaznxb @ 0x8430 */
 
int64_t dbg_obstack_newchunk (int64_t arg_8h, int64_t arg_10h, int64_t arg1, uint32_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void _obstack_newchunk(obstack * h,size_t length); */
    ecx = 0;
    rbx = rdi;
    r13 = *((rdi + 0x18));
    r13 -= *((rdi + 0x10));
    rdx = *(rbx);
    rsi += r13;
    rax = r13;
    rbp = *((rdi + 8));
    cl = (rsi < 0) ? 1 : 0;
    rsi += *((rdi + 0x30));
    dil = (rsi < 0) ? 1 : 0;
    rax >>= 3;
    rax = rsi + rax + 0x64;
    if (rsi < rdx) {
        rsi = rdx;
    }
    if (rax >= rsi) {
        rsi = rax;
    }
    if (rcx != 0) {
        goto label_1;
    }
    edi = (int32_t) dil;
    if (rdi != 0) {
        goto label_1;
    }
    rax = *((rbx + 0x38));
    r14 = rsi;
    if ((*((rbx + 0x50)) & 1) != 0) {
        goto label_2;
    }
    rdi = rsi;
    rax = void (*rax)() ();
    r12 = rax;
    do {
        if (r12 == 0) {
            goto label_1;
        }
        rax = r12 + r14;
        *((rbx + 8)) = r12;
        *((r12 + 8)) = rbp;
        *((rbx + 0x20)) = rax;
        *(r12) = rax;
        rax = *((rbx + 0x30));
        r14 = r12 + rax + 0x10;
        rax = ~rax;
        r14 &= rax;
        memcpy (r14, *((rbx + 0x10)), r13);
        edx = *((rbx + 0x50));
        if ((dl & 2) == 0) {
            rax = *((rbx + 0x30));
            rcx = rbp + rax + 0x10;
            rax = ~rax;
            rax &= rcx;
            if (*((rbx + 0x10)) == rax) {
                goto label_3;
            }
        }
label_0:
        *((rbx + 0x10)) = r14;
        r14 += r13;
        *((rbx + 0x18)) = r14;
        *((rbx + 0x50)) &= 0xfd;
        return rax;
label_2:
        rdi = *((rbx + 0x48));
        rax = void (*rax)() ();
        r12 = rax;
    } while (1);
label_3:
    rax = *((rbp + 8));
    edx &= 1;
    *((r12 + 8)) = rax;
    rax = *((rbx + 0x40));
    if (edx != 0) {
        rdi = *((rbx + 0x48));
        rsi = rbp;
        rax = void (*rax)() ();
        goto label_0;
    }
    rdi = rbp;
    void (*rax)() ();
    goto label_0;
label_1:
    uint64_t (*obstack_alloc_failed_handler)() ();
}

/* /tmp/tmpvpgaznxb @ 0x4e40 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x00009a7f;
        rdx = 0x00009a70;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x00009a77;
        rdx = 0x00009a79;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x00009a7b;
    rdx = 0x00009a74;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x82c0 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = "ASCII";
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = "ASCII";
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x2700 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmpvpgaznxb @ 0x4f20 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x2880)() ();
    }
    rdx = 0x00009ae0;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0x9ae0 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x00009a83;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x00009a79;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x00009b0c;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0x9b0c */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x00009a77;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x00009a79;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x00009a77;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x00009a79;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x00009c0c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0x9c0c */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x00009d0c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0x9d0c */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x00009a79;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x00009a77;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x00009a77;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x00009a79;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmpvpgaznxb @ 0x2880 */
 
void quotearg_buffer_restyled_cold (void) {
    /* [16] -r-x section size 24818 named .text */
    return abort ();
}

/* /tmp/tmpvpgaznxb @ 0x6340 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x2885)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                rdi = r14;
                *((rsp + 0x10)) = rsi;
                fcn_00002440 ();
                rsi = *((rsp + 0x10));
            }
            rdi = *((rsp + 0x10));
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc ();
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x2885 */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmpvpgaznxb @ 0x288a */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmpvpgaznxb @ 0x2480 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmpvpgaznxb @ 0x2890 */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmpvpgaznxb @ 0x2895 */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpvpgaznxb @ 0x289a */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmpvpgaznxb @ 0x289f */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpvpgaznxb @ 0x28a4 */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmpvpgaznxb @ 0x28a9 */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpvpgaznxb @ 0x28ae */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpvpgaznxb @ 0x28b3 */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpvpgaznxb @ 0x28b8 */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpvpgaznxb @ 0x8300 */
 
uint64_t dbg_print_and_abort (void) {
    /* void print_and_abort(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rdi = stderr;
    esi = 1;
    rdx = 0x00009e50;
    rcx = rax;
    eax = 0;
    fprintf_chk ();
    return exit (*(obj.exit_failure));
}

/* /tmp/tmpvpgaznxb @ 0x2530 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmpvpgaznxb @ 0x2820 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmpvpgaznxb @ 0x2800 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmpvpgaznxb @ 0x8350 */
 
int64_t dbg_obstack_begin_worker (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* int _obstack_begin_worker(obstack * h,size_t size,size_t alignment); */
    rbx = rdi;
    if (rdx == 0) {
        goto label_1;
    }
    r12 = rdx - 1;
    do {
        eax = 0xfe0;
        *((rbx + 0x30)) = r12;
        if (rsi == 0) {
            rsi = rax;
        }
        rax = *((rbx + 0x38));
        *(rbx) = rsi;
        if ((*((rbx + 0x50)) & 1) == 0) {
            goto label_2;
        }
        rdi = *((rbx + 0x48));
        rax = void (*rax)() ();
label_0:
        *((rbx + 8)) = rax;
        if (rax == 0) {
            goto label_3;
        }
        rdx = rax + r12 + 0x10;
        rbp = -rbp;
        rdx &= rbp;
        *((rbx + 0x10)) = rdx;
        *((rbx + 0x18)) = rdx;
        rdx = *(rbx);
        rdx += rax;
        *(rax) = rdx;
        *((rbx + 0x20)) = rdx;
        *((rax + 8)) = 0;
        eax = 1;
        *((rbx + 0x50)) &= 0xf9;
        return rax;
label_1:
        r12d = 0xf;
    } while (1);
label_2:
    rdi = rsi;
    void (*rax)() ();
    goto label_0;
label_3:
    uint64_t (*obstack_alloc_failed_handler)() ();
}

/* /tmp/tmpvpgaznxb @ 0x28bd */
 
void obstack_free_cold (void) {
    return abort ();
}

/* /tmp/tmpvpgaznxb @ 0x3420 */
 
int64_t dbg_write_counts (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    char[21] buf;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void write_counts(uintmax_t lines,uintmax_t words,uintmax_t chars,uintmax_t bytes,uintmax_t linelength,char const * file); */
    r15 = rdx;
    r14 = rcx;
    r13 = r8;
    r12 = r9;
    rbp = 0x00009401;
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    if (*(obj.print_lines) != 0) {
        goto label_5;
    }
label_3:
    if (*(obj.print_words) != 0) {
        goto label_6;
    }
label_2:
    if (*(obj.print_chars) != 0) {
        goto label_7;
    }
label_1:
    if (*(obj.print_bytes) != 0) {
        goto label_8;
    }
label_0:
    while (1) {
        if (r12 != 0) {
            rax = strchr (r12, 0xa);
            if (rax != 0) {
                rdx = r12;
                esi = 3;
                edi = 0;
                rax = quotearg_n_style_colon ();
                r12 = rax;
            }
            rdx = rax;
            rsi = 0x000099e4;
            edi = 1;
            eax = 0;
            printf_chk ();
        }
        rdi = stdout;
        rax = *((rdi + 0x28));
        if (rax >= *((rdi + 0x30))) {
            goto label_9;
        }
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0xa;
label_4:
        rax = *((rsp + 0x18));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_10;
        }
        return rax;
        rax = umaxtostr (r13, rsp, rdx);
        edx = number_width;
        rsi = rbp;
        edi = 1;
        rcx = rax;
        eax = 0;
        printf_chk ();
    }
label_8:
    rax = umaxtostr (r14, rsp, rdx);
    edx = number_width;
    rsi = rbp;
    edi = 1;
    rcx = rax;
    eax = 0;
    rbp = " %*s";
    printf_chk ();
    goto label_0;
label_7:
    rax = umaxtostr (r15, rsp, rdx);
    edx = number_width;
    rsi = rbp;
    edi = 1;
    rcx = rax;
    eax = 0;
    rbp = " %*s";
    printf_chk ();
    goto label_1;
label_6:
    rax = umaxtostr (rbx, rsp, rdx);
    edx = number_width;
    rsi = rbp;
    edi = 1;
    rcx = rax;
    eax = 0;
    rbp = " %*s";
    printf_chk ();
    goto label_2;
label_5:
    rax = umaxtostr (rdi, rsp, rdx);
    edx = number_width;
    rsi = rbp;
    edi = 1;
    rcx = rax;
    eax = 0;
    rbp--;
    printf_chk ();
    goto label_3;
label_9:
    esi = 0xa;
    overflow ();
    goto label_4;
label_10:
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x3630 */
 
int64_t wc_lines_part_0 (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    uint32_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r11 = rsp - 0x4000;
    do {
    } while (rsp != r11);
    r14d = 0;
    r13d = 0;
    *((rsp + 0x28)) = rdi;
    r15d = esi;
    r12 = rsp + 0x30;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x20)) = rcx;
    rax = *(fs:0x28);
    *((rsp + 0x4038)) = rax;
    eax = 0;
    *((rsp + 0x10)) = 0;
label_0:
    rax = safe_read (r15d, r12, 0x4000);
    if (rax == 0) {
        goto label_1;
    }
    if (rax == -1) {
        goto label_2;
    }
    rcx = 0x8888888888888889;
    rbx = r12 + rax;
    rdx:rax = rax * rcx;
    rdx >>= 3;
    *((rsp + 8)) = rdx;
    if (r14b != 0) {
        goto label_3;
    }
    if (rbx == r12) {
        goto label_4;
    }
    rdx = r13;
    rax = r12;
    do {
        rax++;
        ecx = 0;
        cl = (*((rax - 1)) == 0xa) ? 1 : 0;
        rdx += rcx;
    } while (rbx != rax);
    rax = rdx;
    rax -= r13;
    r13 = rdx;
    r14b = (rax <= *((rsp + 8))) ? 1 : 0;
    goto label_0;
label_3:
    *(rbx) = 0xa;
    rdi = r12;
    while (rbx > rax) {
        rdi = rax + 1;
        rbp++;
        esi = 0xa;
        rax = rawmemchr ();
    }
    rax = rbp;
    rax -= r13;
    r13 = rbp;
    r14b = (rax <= *((rsp + 8))) ? 1 : 0;
    goto label_0;
label_1:
    rax = *((rsp + 0x20));
    rcx = *((rsp + 0x10));
    *(rax) = rcx;
    rax = *((rsp + 0x18));
    *(rax) = r13;
    eax = 1;
    do {
        rdx = *((rsp + 0x4038));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_5;
        }
        return rax;
label_2:
        rdx = *((rsp + 0x28));
        esi = 3;
        edi = 0;
        rax = quotearg_n_style_colon ();
        r12 = rax;
        rax = errno_location ();
        rcx = r12;
        eax = 0;
        eax = error (0, *(rax), 0x000099e5);
        eax = 0;
    } while (1);
label_4:
    r14d = 1;
    goto label_0;
label_5:
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x37f0 */
 
int32_t wc_lines (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    if (rdx != 0) {
        if (rcx == 0) {
            goto label_0;
        }
        void (*0x3630)() ();
    }
label_0:
    eax = 0;
    return eax;
}

/* /tmp/tmpvpgaznxb @ 0x3810 */
 
int64_t dbg_wc (int64_t arg1, uint32_t arg2, int64_t arg3, int64_t arg4) {
    wchar_t wide_char;
    uintmax_t lines;
    uintmax_t bytes;
    mbstate_t state;
    char[16385] buf;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t fildes;
    int64_t var_2eh;
    uint32_t var_2fh;
    uint32_t var_30h;
    int64_t var_38h;
    wint_t wc;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* _Bool wc(int fd,char const * file_x,fstatus * fstatus,off_t current_pos); */
    r11 = rsp - 0x4000;
    do {
    } while (rsp != r11);
    *((rsp + 0x28)) = edi;
    rbx = rdx;
    *((rsp + 0x30)) = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x4068)) = rax;
    eax = 0;
    *((rsp + 0x38)) = rsi;
    if (rsi == 0) {
        goto label_21;
    }
label_10:
    *((rsp + 0x50)) = 0;
    *((rsp + 0x48)) = 0;
    rax = ctype_get_mb_cur_max ();
    r12d = *(obj.print_bytes);
    if (rax <= 1) {
        goto label_22;
    }
    eax = *(obj.print_chars);
    *((rsp + 0x2f)) = al;
label_5:
    if (*(obj.print_words) != 0) {
        goto label_23;
    }
    r15d = *(obj.print_linelength);
    if (r15b != 0) {
        goto label_23;
    }
    if (r12b != 1) {
        goto label_24;
    }
    if (*((rsp + 0x2f)) != 0) {
        goto label_24;
    }
    if (*(obj.print_lines) != 0) {
        goto label_25;
    }
label_15:
    eax = *(rbx);
    if (eax > 0) {
        goto label_26;
    }
label_12:
    if (eax == 0) {
        eax = *((rbx + 0x20));
        eax &= loc.data_start;
        if (eax != 0x8000) {
            goto label_27;
        }
        r12 = *((rbx + 0x38));
        if (r12 < 0) {
            goto label_27;
        }
        r13 = page_size;
        rcx = r12;
        if (rbp == -1) {
            goto label_28;
        }
        rax = r12;
        edx = 0;
        rax = rdx:rax / r13;
        rdx = rdx:rax % r13;
        if (rdx != 0) {
            goto label_29;
        }
        esi = 1;
        ebp = 0;
label_14:
        rdx = 0x1fffffffffffffff;
        rax = *((rbx + 0x40));
        rcx = rax - 1;
        rax++;
        ecx = 0x201;
        if (rcx <= rdx) {
            rcx = rax;
        }
        rax = r12;
        edx = 0;
        rax = rdx:rax / rcx;
        rdx = rdx:rax % rcx;
        r12 -= rdx;
        if (r12 <= rbp) {
            goto label_27;
        }
        if (sil == 0) {
            goto label_27;
        }
        edi = *((rsp + 0x28));
        edx = 1;
        rsi = r12;
        rax = lseek ();
        if (rax < 0) {
            goto label_27;
        }
        r12 -= rbp;
        *((rsp + 0x50)) = r12;
    }
label_27:
    ebx = *((rsp + 0x28));
    ecx = 2;
    edx = 0;
    esi = 0;
    rbp = rsp + 0x60;
    edi = ebx;
    rax = fdadvise ();
    while (rax != 0) {
        if (rax == -1) {
            goto label_30;
        }
        rax = safe_read (ebx, rbp, 0x4000);
    }
    rcx = *((rsp + 0x50));
    r12 = rax;
    ebp = 0;
    ebx = 0;
    r15d = 1;
label_11:
    eax = *((rsp + 0x2f));
    r9 = *((rsp + 0x30));
    r8 = rbp;
    rsi = rbx;
    rdi = *((rsp + 0x48));
    if (*(obj.print_chars) > al) {
        r12 = rcx;
    }
    write_counts (rdi, rsi, r12, rcx, r8, r9);
    rax = *((rsp + 0x48));
    *(obj.total_words) += rbx;
    *(obj.total_lines) += rax;
    rax = *((rsp + 0x50));
    *(obj.total_chars) += r12;
    *(obj.total_bytes) += rax;
    if (*(obj.max_line_length) < rbp) {
        *(obj.max_line_length) = rbp;
    }
    rax = *((rsp + 0x4068));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_31;
    }
    eax = r15d;
    return rax;
label_23:
    edi = *((rsp + 0x28));
    ecx = 2;
    edx = 0;
    esi = 0;
    fdadvise ();
label_18:
    rax = ctype_get_mb_cur_max ();
    if (rax <= 1) {
        goto label_32;
    }
    rax = rsp + 0x60;
    r14d = 0;
    r15d = 0;
    ebp = 0;
    *((rsp + 0x58)) = 0;
    ebx = 0;
    r12d = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x20)) = rax;
label_0:
    rax = *((rsp + 0x20));
    rdx -= r14;
    rax = safe_read (*((rsp + 0x28)), rax + r14, 0x4000);
    if (rax == 0) {
        goto label_33;
    }
    if (rax == -1) {
        goto label_34;
    }
    r14 += rax;
    r13 = *((rsp + 0x20));
    r9d = r15d;
    r15 = r14;
    do {
        if (r9b != 0) {
            goto label_35;
        }
        ecx = *(r13);
        rsi = obj_is_basic_table;
        eax = ecx;
        al >>= 5;
        eax &= 7;
        eax = *((rsi + rax*4));
        eax >>= cl;
        r14d = eax;
        r14d &= 1;
        if (r14d == 0) {
            goto label_35;
        }
        eax = (int32_t) cl;
        *((rsp + 0x44)) = eax;
        eax = rcx - 9;
        if (al <= 0x17) {
            rsi = 0x00009208;
            eax = (int32_t) al;
            rax = *((rsi + rax*4));
            rax += rsi;
            /* switch table (24 cases) at 0x9208 */
            void (*rax)() ();
        }
        *((rsp + 0x2e)) = r9b;
        *((rsp + 8)) = cl;
        rax = ctype_b_loc ();
        ecx = *((rsp + 8));
        r9d = *((rsp + 0x2e));
        edx = 1;
        rax = *(rax);
        eax = *((rax + rcx*2));
        if ((ah & 0x40) != 0) {
            rbp++;
            if ((ah & 0x20) != 0) {
                goto label_2;
            }
            ebx = (int32_t) r14b;
        }
label_1:
        r13 += rdx;
        r15 -= rdx;
        r12++;
label_4:
    } while (r15 != 0);
    r14 = r15;
    r15d = r9d;
    goto label_0;
    edx = 1;
label_3:
    rax = *((rsp + 0x18));
    if (rax >= rbp) {
    }
    *((rsp + 0x18)) = rbp;
    ebp = 0;
    do {
label_2:
        ebx = 0;
        goto label_1;
        edx = 1;
        rbp++;
    } while (1);
    edx = 1;
    goto label_2;
    edx = 1;
    goto label_3;
    edx = 1;
    rbp &= 0xfffffffffffffff8;
    rbp += 8;
    goto label_2;
label_35:
    rax = *((rsp + 0x58));
    r14 = rsp + 0x58;
    *((rsp + 8)) = rax;
    rax = rpl_mbrtowc (rsp + 0x44, r13, r15, r14);
    if (rax == 0xfffffffffffffffe) {
        goto label_36;
    }
    if (rax == -1) {
        goto label_37;
    }
    *((rsp + 8)) = rax;
    eax = mbsinit (r14);
    rdx = *((rsp + 8));
    r9b = (eax == 0) ? 1 : 0;
    if (rdx != 0) {
        goto label_38;
    }
    *((rsp + 0x44)) = 0;
    r14d = 0;
    edx = 1;
label_13:
    *((rsp + 0x2e)) = r9b;
    *((rsp + 8)) = rdx;
    eax = iswprint (0);
    rdx = *((rsp + 8));
    r9d = *((rsp + 0x2e));
    if (eax == 0) {
        goto label_1;
    }
    if (*(obj.print_linelength) != 0) {
        goto label_39;
    }
label_6:
    r14d = *((rsp + 0x44));
    *((rsp + 0x2e)) = r9b;
    *((rsp + 8)) = rdx;
    eax = iswspace (r14d);
    rdx = *((rsp + 8));
    r9d = *((rsp + 0x2e));
    if (eax != 0) {
        goto label_2;
    }
    eax = *(obj.posixly_correct);
    if (al != 0) {
        goto label_40;
    }
    if (r14d == 0xa0) {
        goto label_2;
    }
    if (r14d == 0x2007) {
        goto label_2;
    }
    if (r14d == 0x202f) {
        goto label_2;
    }
    if (r14d == fcn.00002060) {
        goto label_2;
    }
    ebx = 1;
    goto label_1;
label_38:
    r14d = *((rsp + 0x44));
    eax = r14 - 9;
    if (eax > 0x17) {
        goto label_41;
    }
    rsi = 0x00009268;
    rax = *((rsi + rax*4));
    rax += rsi;
    /* switch table (24 cases) at 0x9268 */
    void (*rax)() ();
label_37:
    r13++;
    r15--;
    r9d = 1;
    goto label_4;
label_36:
    rax = *((rsp + 8));
    r14 = r15;
    *((rsp + 0x58)) = rax;
    if (r15 != 0) {
        if (r15 == 0x4000) {
            r13++;
            r14d = 0x3fff;
        }
        rdi = *((rsp + 0x20));
        ecx = 0x4001;
        rdx = r14;
        rsi = r13;
        memmove_chk ();
    }
    r15d = 1;
    goto label_0;
label_22:
    eax = *(obj.print_chars);
    *((rsp + 0x2f)) = 0;
    if (r12b == 0) {
        r12d = eax;
    }
    goto label_5;
label_40:
    ebx = (int32_t) al;
    goto label_1;
label_39:
    edi = r14d;
    eax = wcwidth ();
    r9d = *((rsp + 0x2e));
    rdx = *((rsp + 8));
    rcx = (int64_t) eax;
    rcx += rbp;
    if (eax > 0) {
    }
    goto label_6;
label_32:
    ebx = 0;
    rax = rsp + 0x60;
    ebp = 0;
    r13d = 0;
    *((rsp + 0x20)) = rax;
    eax = ebx;
    r15 = 0x000092c8;
    rbx = rbp;
    r12d = (int32_t) al;
label_7:
    rax = safe_read (*((rsp + 0x28)), *((rsp + 0x20)), 0x4000);
    if (rax == 0) {
        goto label_42;
    }
    if (rax == -1) {
        goto label_43;
    }
    r14 = *((rsp + 0x20));
    rdx = r14 + rax;
    do {
        edi = *(r14);
        r14++;
        eax = rdi - 9;
        if (al <= 0x17) {
            eax = (int32_t) al;
            rax = *((r15 + rax*4));
            rax += r15;
            /* switch table (24 cases) at 0x92c8 */
            void (*rax)() ();
        }
        *((rsp + 0x10)) = rdx;
        *((rsp + 8)) = dil;
        rax = ctype_b_loc ();
        rdx = *((rsp + 0x10));
        r9 = rax;
        eax = *((rsp + 8));
        rsi = *(r9);
        rdi = rax;
        eax = *((rsi + rax*2));
        if ((ah & 0x40) != 0) {
            r13++;
            if ((ah & 0x20) != 0) {
                goto label_9;
            }
            *((rsp + 8)) = rdx;
            eax = btowc (rdi);
            esi = *(obj.posixly_correct);
            rdx = *((rsp + 8));
            if (sil != 0) {
                goto label_44;
            }
            if (eax == 0xa0) {
                goto label_9;
            }
            if (eax == 0x2007) {
                goto label_9;
            }
            if (eax == 0x202f) {
                goto label_9;
            }
            if (eax == fcn.00002060) {
                goto label_9;
            }
            r12d = 1;
        }
label_8:
    } while (r14 != rdx);
    goto label_7;
    if (rbx < r13) {
        rbx = r13;
    }
    r13d = 0;
    do {
label_9:
        rbp += r12;
        r12d = 0;
        goto label_8;
        r13 &= 0xfffffffffffffff8;
        r13 += 8;
    } while (1);
    r13++;
    goto label_9;
label_44:
    r12d = (int32_t) sil;
    goto label_8;
label_21:
    edx = 5;
    rax = dcgettext (0, "standard input");
    *((rsp + 0x38)) = rax;
    goto label_10;
label_33:
    r15d = 1;
    do {
        rax = *((rsp + 0x18));
        rcx = *((rsp + 0x50));
        if (rax >= rbp) {
        }
        rbx += *((rsp + 0x10));
        goto label_11;
label_34:
        rdx = *((rsp + 0x38));
        esi = 3;
        edi = 0;
        rax = quotearg_n_style_colon ();
        r15 = rax;
        rax = errno_location ();
        rcx = r15;
        eax = 0;
        r15d = 0;
        error (0, *(rax), 0x000099e5);
    } while (1);
label_42:
    eax = r12d;
    r15d = 1;
    r12 = rbp;
    ebx = (int32_t) al;
    do {
        rcx = *((rsp + 0x50));
        if (rbp < r13) {
        }
        rbx += r12;
        r12d = 0;
        goto label_11;
label_43:
        rdx = *((rsp + 0x38));
        eax = r12d;
        esi = 3;
        edi = 0;
        r12 = rbp;
        ebx = (int32_t) al;
        r15d = 0;
        rax = quotearg_n_style_colon ();
        r14 = rax;
        rax = errno_location ();
        rcx = r14;
        eax = 0;
        error (0, *(rax), 0x000099e5);
    } while (1);
label_30:
    rdx = *((rsp + 0x38));
    esi = 3;
    edi = 0;
    ebp = 0;
    ebx = 0;
    rax = quotearg_n_style_colon ();
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    r12d = 0;
    error (0, *(rax), 0x000099e5);
    rcx = *((rsp + 0x50));
    goto label_11;
label_26:
    eax = fstat (*((rsp + 0x28)), rbx + 8);
    *(rbx) = eax;
    goto label_12;
label_41:
    edi = r14d;
    goto label_13;
label_28:
    edi = *((rsp + 0x28));
    edx = 1;
    esi = 0;
    *((rsp + 8)) = r12;
    rax = lseek ();
    rcx = *((rsp + 8));
    edx = 0;
    rax = rcx;
    rax = rdx:rax / r13;
    rdx = rdx:rax % r13;
    if (rdx == 0) {
        goto label_45;
    }
    if (rbp <= rcx) {
        goto label_46;
    }
    ecx = 0;
    do {
label_29:
        *((rsp + 0x50)) = rcx;
        ebp = 0;
        r12d = 0;
        ebx = 0;
        r15d = 1;
        goto label_11;
label_31:
        stack_chk_fail ();
label_46:
        rcx -= rbp;
    } while (1);
label_45:
    rsi = rbp;
    rsi = ~rsi;
    rsi >>= 0x3f;
    goto label_14;
label_25:
    edi = *((rsp + 0x28));
    ecx = 2;
    edx = 0;
    esi = 0;
    fdadvise ();
label_19:
    if (*(obj.print_lines) == 0) {
        goto label_15;
    }
label_20:
    esi = 0;
    eax = esi;
    __asm ("cpuid");
    if (eax == 0) {
        goto label_47;
    }
    eax = 1;
    __asm ("cpuid");
    ecx &= 0x8000000;
    if (ecx != 0) {
        eax = esi;
        __asm ("cpuid");
        if (eax <= 6) {
            goto label_47;
        }
        eax = 7;
        ecx = esi;
        __asm ("cpuid");
        ebx &= 0x20;
        if (ebx != 0) {
            goto label_48;
        }
    }
    edx = 5;
    rsi = "avx2 support not detected";
    if (*(obj.debug) != 0) {
        goto label_49;
    }
    do {
label_16:
        rax = wc_lines_p;
label_17:
        rcx = rsp + 0x50;
        esi = *((rsp + 0x28));
        rdi = *((rsp + 0x38));
        ebp = 0;
        rdx = rsp + 0x48;
        r12d = 0;
        ebx = 0;
        eax = void (*rax)() ();
        rcx = *((rsp + 0x50));
        r15d = eax;
        goto label_11;
label_47:
    } while (*(obj.debug) == 0);
    edx = 5;
label_49:
    rax = dcgettext (0, "failed to get cpuid");
    rcx = rax;
    eax = 0;
    error (0, 0, 0x000099e5);
    goto label_16;
label_48:
    while (1) {
        rax = dbg_wc_lines_avx2;
        *(obj.wc_lines_p) = rax;
        goto label_17;
        edx = 5;
        rax = dcgettext (0, "using avx2 hardware support");
        rcx = rax;
        eax = 0;
        error (0, 0, 0x000099e5);
    }
label_24:
    edi = *((rsp + 0x28));
    edx = 0;
    esi = 0;
    ecx = 2;
    fdadvise ();
    if (*((rsp + 0x2f)) == 1) {
        goto label_18;
    }
    if (r12b != 0) {
        goto label_19;
    }
    if (*((rsp + 0x2f)) == 0) {
        goto label_20;
    }
    goto label_18;
}

/* /tmp/tmpvpgaznxb @ 0x3360 */
 
uint64_t deregister_tm_clones (void) {
    rdi = loc__edata;
    rax = loc__edata;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x3390 */
 
int64_t register_tm_clones (void) {
    rdi = loc__edata;
    rsi = loc__edata;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x33d0 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_00002460 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmpvpgaznxb @ 0x2460 */
 
void fcn_00002460 (void) {
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmpvpgaznxb @ 0x3410 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmpvpgaznxb @ 0x7120 */
 
int64_t dbg_save_token (int64_t arg1) {
    rdi = arg1;
    /* void save_token(Tokens * t); */
    rax = *((rdi + 0x30));
    rbx = rdi;
    r12 = *((rdi + 0x28));
    rbp -= r12;
    rbp--;
    if (rax == r12) {
        goto label_2;
    }
label_1:
    rdx = *((rbx + 0x48));
    rax += rdx;
    rdx = ~rdx;
    rax &= rdx;
    rdx = *((rbx + 0x38));
    rsi = rax;
    rsi -= *((rbx + 0x20));
    rcx = rdx;
    rcx -= *((rbx + 0x20));
    if (rsi <= rcx) {
        rdx = rax;
    }
    rax = *((rbx + 0x90));
    *((rbx + 0x30)) = rdx;
    *((rbx + 0x28)) = rdx;
    rdx = *((rbx + 0x88));
    rax -= rdx;
    while (1) {
        *(rdx) = r12;
        rdx = *((rbx + 0xe0));
        rax = *((rbx + 0xe8));
        *((rbx + 0x88)) += 8;
        rax -= rdx;
        if (rax <= 7) {
            goto label_3;
        }
label_0:
        *((rbx + 0xe0)) += 8;
        *(rbx)++;
        r12 = rbx;
        return rax;
        _obstack_newchunk (rbx + 0x70, 8, rbp, rcx);
        rdx = *((rbx + 0x88));
    }
label_3:
    _obstack_newchunk (rbx + 0xc8, 8, rdx, rcx);
    rdx = *((rbx + 0xe0));
    goto label_0;
label_2:
    *((rdi + 0x68)) |= 2;
    goto label_1;
}

/* /tmp/tmpvpgaznxb @ 0x8960 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmpvpgaznxb @ 0x6890 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpvpgaznxb @ 0x6bc0 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000d290]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x0000d2a0]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x2560 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmpvpgaznxb @ 0x7a50 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x4600 */
 
uint64_t dbg_argv_iter_init_argv (int64_t arg1) {
    rdi = arg1;
    /* argv_iterator * argv_iter_init_argv(char ** argv); */
    rbx = rdi;
    edi = 0x30;
    rax = fcn_00002450 ();
    if (rax != 0) {
        *(rax) = 0;
        *((rax + 0x20)) = rbx;
        *((rax + 0x28)) = rbx;
    }
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x2450 */
 
void fcn_00002450 (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmpvpgaznxb @ 0x66d0 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc ();
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x2490 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmpvpgaznxb @ 0x7c50 */
 
uint64_t dbg_xcharalloc (void) {
    /* char * xcharalloc(size_t n); */
    rax = fcn_00002450 ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpvpgaznxb @ 0x8190 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x000099e5);
    return abort ();
}

/* /tmp/tmpvpgaznxb @ 0x2780 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmpvpgaznxb @ 0x2720 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmpvpgaznxb @ 0x24f0 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmpvpgaznxb @ 0x65f0 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x7290 */
 
void dbg_readtokens0_free (int64_t arg1) {
    rdi = arg1;
    /* void readtokens0_free(Tokens * t); */
    esi = 0;
    rbx = rdi;
    rdi = rdi + 0x18;
    _obstack_free ();
    rdi = rbx + 0x70;
    esi = 0;
    _obstack_free ();
    rdi = rbx + 0xc8;
    esi = 0;
    return obstack_free ();
}

/* /tmp/tmpvpgaznxb @ 0x4ad0 */
 
int64_t dbg_rpl_mbrtowc (int64_t arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x68e0 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x2890)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x6650 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x4d90 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite (0x00009a30, 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmpvpgaznxb @ 0x25b0 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmpvpgaznxb @ 0x24a0 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmpvpgaznxb @ 0x2810 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmpvpgaznxb @ 0x6e20 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x0000d290]");
    __asm ("movdqa xmm2, xmmword [0x0000d2a0]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x28a9)() ();
    }
    if (rdx == 0) {
        void (*0x28a9)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x6ec0 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000d290]");
    __asm ("movdqa xmm2, xmmword [0x0000d2a0]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x28ae)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x28ae)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x6a00 */
 
int64_t quotearg_style (uint32_t arg1, int64_t arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x289a)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x6d80 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x28a4)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x8974 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmpvpgaznxb @ 0x4820 */
 
void fdadvise (void) {
    return posix_fadvise ();
}

/* /tmp/tmpvpgaznxb @ 0x7cd0 */
 
uint64_t xreallocarray (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpvpgaznxb @ 0x7dd0 */
 
int64_t dbg_x2realloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x4670 */
 
uint64_t dbg_argv_iter (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* char * argv_iter(argv_iterator * ai,argv_iter_err * err); */
    rbx = rdi;
    rcx = *(rdi);
    if (rcx == 0) {
        goto label_1;
    }
    rsi = rdi + 0x18;
    edx = 0;
    rdi = rdi + 0x10;
    rax = getdelim ();
    if (rax < 0) {
        goto label_2;
    }
    *(rbp) = 1;
    rax = *((rbx + 0x10));
    *((rbx + 8))++;
    do {
label_0:
        return rax;
label_1:
        rdx = *((rdi + 0x28));
        rax = *(rdx);
        if (rax == 0) {
            goto label_3;
        }
        rdx += 8;
        *(rsi) = 1;
        *((rdi + 0x28)) = rdx;
        return rax;
label_2:
        eax = feof (*(rbx));
        eax -= eax;
        eax &= 2;
        eax += 2;
        *(rbp) = eax;
        eax = 0;
    } while (1);
label_3:
    *(rsi) = 2;
    goto label_0;
}

/* /tmp/tmpvpgaznxb @ 0x2860 */
 
void getdelim (void) {
    __asm ("bnd jmp qword [reloc.getdelim]");
}

/* /tmp/tmpvpgaznxb @ 0x26a0 */
 
void feof (void) {
    __asm ("bnd jmp qword [reloc.feof]");
}

/* /tmp/tmpvpgaznxb @ 0x7c70 */
 
uint64_t xrealloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmpvpgaznxb @ 0x48f0 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x26f0)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmpvpgaznxb @ 0x4b60 */
 
int64_t physmem_total (void) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_30h;
    int64_t var_78h;
    int64_t var_88h;
    edi = 0x55;
    rax = *(fs:0x28);
    *((rsp + 0x88)) = rax;
    eax = 0;
    rax = sysconf ();
    xmm0 = 0;
    edi = 0x1e;
    __asm ("cvtsi2sd xmm0, rax");
    *((rsp + 8)) = xmm0;
    rax = sysconf ();
    xmm0 = *((rsp + 8));
    xmm1 = 0;
    __asm ("comisd xmm0, xmm1");
    if (? >= ?) {
        xmm2 = 0;
        __asm ("cvtsi2sd xmm2, rax");
        __asm ("comisd xmm2, xmm1");
        if (? >= ?) {
            goto label_1;
        }
    }
    rdi = rsp + 0x10;
    eax = sysinfo ();
    xmm0 = *(0x00009a20);
    if (eax == 0) {
        goto label_2;
    }
    do {
label_0:
        rax = *((rsp + 0x88));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        return rax;
label_1:
        __asm ("mulsd xmm0, xmm2");
    } while (1);
label_2:
    rax = *((rsp + 0x30));
    if (rax < 0) {
        goto label_4;
    }
    xmm0 = 0;
    __asm ("cvtsi2sd xmm0, rax");
    do {
        eax = *((rsp + 0x78));
        xmm1 = 0;
        __asm ("cvtsi2sd xmm1, rax");
        __asm ("mulsd xmm0, xmm1");
        goto label_0;
label_4:
        rdx = rax;
        eax &= 1;
        xmm0 = 0;
        rdx >>= 1;
        rdx |= rax;
        __asm ("cvtsi2sd xmm0, rdx");
        __asm ("addsd xmm0, xmm0");
    } while (1);
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x4730 */
 
void dbg_argv_iter_free (uint32_t arg1) {
    rdi = arg1;
    /* void argv_iter_free(argv_iterator * ai); */
    if (*(rdi) != 0) {
        rdi = *((rdi + 0x10));
        fcn_00002440 ();
    }
    rdi = rbp;
    return void (*0x2440)() ();
}

/* /tmp/tmpvpgaznxb @ 0x4a20 */
 
uint32_t dbg_mb_width_aux (int64_t arg1) {
    rdi = arg1;
    /* int mb_width_aux(wint_t wc); */
    eax = wcwidth ();
    if (eax >= 0) {
        return eax;
    }
    eax = iswcntrl (ebp);
    al = (eax == 0) ? 1 : 0;
    eax = (int32_t) al;
    return eax;
}

/* /tmp/tmpvpgaznxb @ 0x26e0 */
 
void wcwidth (void) {
    __asm ("bnd jmp qword [reloc.wcwidth]");
}

/* /tmp/tmpvpgaznxb @ 0x24e0 */
 
void iswcntrl (void) {
    __asm ("bnd jmp qword [reloc.iswcntrl]");
}

/* /tmp/tmpvpgaznxb @ 0x7c10 */
 
uint64_t xmalloc (void) {
    rax = fcn_00002450 ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpvpgaznxb @ 0x68c0 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpvpgaznxb @ 0x8150 */
 
uint64_t dbg_xstrdup (int64_t arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rdi = r12;
    rax = fcn_00002450 ();
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x26c0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpvpgaznxb @ 0x2550 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmpvpgaznxb @ 0x7c30 */
 
uint64_t ximalloc (void) {
    rax = fcn_00002450 ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpvpgaznxb @ 0x7990 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0x7520)() ();
}

/* /tmp/tmpvpgaznxb @ 0x4770 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, "write error");
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x000099e5);
    } while (1);
}

/* /tmp/tmpvpgaznxb @ 0x49c0 */
 
int64_t dbg_umaxtostr (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * umaxtostr(uintmax_t i,char * buf); */
    *((rsi + 0x14)) = 0;
    rcx = rdi;
    r8 = rsi + 0x14;
    rdi = 0xcccccccccccccccd;
    do {
        rax = rcx;
        r8--;
        rdx:rax = rax * rdi;
        rax = rcx;
        rdx >>= 3;
        rsi = rdx * 5;
        rsi += rsi;
        rax -= rsi;
        eax += 0x30;
        *(r8) = al;
        rax = rcx;
        rcx = rdx;
    } while (rax > 9);
    rax = r8;
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x8660 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmpvpgaznxb @ 0x7bd0 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpvpgaznxb @ 0x8240 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x6f70 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x0000d290]");
    __asm ("movdqa xmm2, xmmword [0x0000d2a0]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x28b3)() ();
    }
    if (rax == 0) {
        void (*0x28b3)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x8720 */
 
int64_t dbg_wc_lines_avx2 (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_28h;
    __m256i[510] avx_buf;
    int64_t var_3ff8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* _Bool wc_lines_avx2(char const * file,int fd,uintmax_t * lines_out,uintmax_t * bytes_out); */
    *((rsp + 0x18)) = rdi;
    *((rsp + 0x10)) = rdx;
    *((rsp + 8)) = rcx;
    rax = *(fs:0x28);
    *((rsp + 0x3ff8)) = rax;
    eax = 0;
    r13b = (rdx == 0) ? 1 : 0;
    al = (rcx == 0) ? 1 : 0;
    r13b |= al;
    if (r13b != 0) {
        goto label_3;
    }
    r12d = esi;
    ebx = 0;
    r15 = rsp + 0x20;
    r14d = 0;
label_0:
    __asm ("vzeroupper");
    rax = safe_read (r12d, r15, 0x3fc0);
    __asm ("vmovdqa ymm4, ymmword [str._n_n_n_n_n_n_n_n_n_n_n_n_n_n_n_n_n_n_n_n_n_n_n_n_n_n_n_n_n_n_n_n]");
    if (rax == 0) {
        goto label_4;
    }
    if (rax == -1) {
        goto label_5;
    }
    rbx += rax;
    rdi = r15 + rax;
    if (rax <= 0x3f) {
        goto label_6;
    }
    rax -= 0x40;
    __asm ("vpxor xmm3, xmm3, xmm3");
    rdx = r15;
    rax &= 0xffffffffffffffc0;
    __asm ("vmovdqa ymm2, ymm3");
    rax = r15 + rax + 0x40;
    do {
        __asm ("vpcmpeqb ymm1, ymm4, ymmword [rdx]");
        __asm ("vpcmpeqb ymm0, ymm4, ymmword [rdx + 0x20]");
        rdx += 0x40;
        __asm ("vpsubb ymm1, ymm2, ymm1");
        __asm ("vpsubb ymm0, ymm3, ymm0");
        __asm ("vmovdqa ymm2, ymm1");
        __asm ("vmovdqa ymm3, ymm0");
    } while (rdx != rax);
label_1:
    __asm ("vpxor xmm2, xmm2, xmm2");
    __asm ("vpsadbw ymm1, ymm1, ymm2");
    __asm ("vpsadbw ymm0, ymm0, ymm2");
    __asm ("vmovdqa xmm3, xmm1");
    __asm ("vextracti128 xmm1, ymm1, 1");
    __asm ("vmovdqa xmm2, xmm0");
    __asm ("vpextrw r8d, xmm3, 0");
    __asm ("vpextrw edx, xmm3, 4");
    __asm ("vextracti128 xmm0, ymm0, 1");
    edx += r8d;
    __asm ("vpextrw r8d, xmm1, 0");
    __asm ("vpextrw r9d, xmm2, 0");
    edx += r8d;
    __asm ("vpextrw r8d, xmm1, 4");
    edx += r8d;
    __asm ("vpextrw r8d, xmm2, 4");
    r8d += r9d;
    __asm ("vpextrw r9d, xmm0, 0");
    rdx = (int64_t) edx;
    r8d += r9d;
    __asm ("vpextrw r9d, xmm0, 4");
    r8d += r9d;
    r8 = (int64_t) r8d;
    rdx += r8;
    r14 += rdx;
    if (rdi == rax) {
        goto label_0;
    }
    do {
        rax++;
        edx = 0;
        dl = (*((rax - 1)) == 0xa) ? 1 : 0;
        r14 += rdx;
    } while (rdi != rax);
    goto label_0;
label_6:
    __asm ("vpxor xmm0, xmm0, xmm0");
    rax = r15;
    __asm ("vmovdqa ymm1, ymm0");
    goto label_1;
label_3:
    r13d = 0;
    do {
label_2:
        rax = *((rsp + 0x3ff8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_7;
        }
        rsp = rbp - 0x28;
        eax = r13d;
        return rax;
label_4:
        rax = *((rsp + 0x10));
        r13d = 1;
        *(rax) = r14;
        rax = *((rsp + 8));
        *(rax) = rbx;
        __asm ("vzeroupper");
    } while (1);
label_5:
    rdx = *((rsp + 0x18));
    esi = 3;
    edi = 0;
    __asm ("vzeroupper");
    rax = quotearg_n_style_colon ();
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    error (0, *(rax), 0x000099e5);
    goto label_2;
label_7:
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x6cf0 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000d290]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x0000d2a0]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x8020 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpvpgaznxb @ 0x2660 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmpvpgaznxb @ 0x6590 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x70b0 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpvpgaznxb @ 0x6530 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x8090 */
 
uint64_t xmemdup (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rsi;
    rdi = rsi;
    rax = fcn_00002450 ();
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x26c0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpvpgaznxb @ 0x67d0 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rdi = *(rbx);
        rbx += 0x10;
        fcn_00002440 ();
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        fcn_00002440 ();
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        rdi = r12;
        fcn_00002440 ();
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x4630 */
 
uint64_t dbg_argv_iter_init_stream (int64_t arg1) {
    rdi = arg1;
    /* argv_iterator * argv_iter_init_stream(FILE * fp); */
    rbx = rdi;
    edi = 0x30;
    rax = fcn_00002450 ();
    if (rax != 0) {
        *(rax) = rbx;
        *((rax + 0x10)) = 0;
        *((rax + 0x18)) = 0;
        *((rax + 8)) = 0;
        *((rax + 0x20)) = 0;
    }
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x4760 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmpvpgaznxb @ 0x7ca0 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpvpgaznxb @ 0x7e60 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x4860 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x2510)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpvpgaznxb @ 0x7d80 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmpvpgaznxb @ 0x70c0 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpvpgaznxb @ 0x7010 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000d290]");
    __asm ("movdqa xmm2, xmmword [0x0000d2a0]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x28b8)() ();
    }
    if (rax == 0) {
        void (*0x28b8)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x72c0 */
 
int64_t dbg_readtokens0 (int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, uint32_t arg_28h, int64_t arg_30h, uint32_t arg_38h, int64_t arg_70h, int64_t arg_78h, int64_t arg_80h, int64_t arg_88h, int64_t arg_90h, int64_t arg_a0h, int64_t arg_c0h, FILE * stream, int64_t arg_d8h, int64_t arg_e0h, int64_t arg_e8h, int64_t arg_f8h, int64_t arg_118h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* _Bool readtokens0(FILE * in,Tokens * t); */
    r13 = rsi + 0x18;
    r12 = rdi;
    while (*((rbp + 0x38)) != rax) {
        rdx = rax + 1;
        *((rbp + 0x30)) = rdx;
        *(rax) = bl;
        if (ebx == 0) {
            goto label_4;
        }
label_0:
        eax = fgetc (r12);
        ebx = eax;
        rax = *((rbp + 0x30));
        if (ebx == 0xffffffff) {
            goto label_5;
        }
    }
    _obstack_newchunk (r13, 1, rdx, rcx);
    rax = *((rbp + 0x30));
    rdx = rax + 1;
    *((rbp + 0x30)) = rdx;
    *(rax) = bl;
    if (ebx != 0) {
        goto label_0;
    }
label_4:
    rax = save_token (rbp);
    goto label_0;
label_5:
    if (rax != *((rbp + 0x28))) {
        if (rax == *((rbp + 0x38))) {
            goto label_6;
        }
label_3:
        rdx = rax + 1;
        *((rbp + 0x30)) = rdx;
        *(rax) = 0;
        save_token (rbp);
    }
    rdx = *((rbp + 0x88));
    rax = *((rbp + 0x90));
    rax -= rdx;
    while (1) {
        *(rdx) = 0;
        rax = *((rbp + 0x88));
        rcx = *((rbp + 0x80));
        rax += 8;
        if (rax == rcx) {
            goto label_7;
        }
label_2:
        rdx = *((rbp + 0xa0));
        *((rbp + 8)) = rcx;
        rcx = *((rbp + 0xd8));
        rax += rdx;
        rdx = ~rdx;
        rax &= rdx;
        rdx = *((rbp + 0x90));
        rdi = rax;
        rdi -= *((rbp + 0x78));
        rsi = rdx;
        rsi -= *((rbp + 0x78));
        if (rdi <= rsi) {
            rdx = rax;
        }
        rax = *((rbp + 0xe0));
        *((rbp + 0x88)) = rdx;
        *((rbp + 0x80)) = rdx;
        if (rax == rcx) {
            goto label_8;
        }
label_1:
        rdx = *((rbp + 0xf8));
        *((rbp + 0x10)) = rcx;
        rax += rdx;
        rdx = ~rdx;
        rax &= rdx;
        rdx = *((rbp + 0xe8));
        rdi = rax;
        rdi -= *((rbp + 0xd0));
        rsi = rdx;
        rsi -= *((rbp + 0xd0));
        rdi = r12;
        if (rdi <= rsi) {
            rdx = rax;
        }
        *((rbp + 0xe0)) = rdx;
        *((rbp + 0xd8)) = rdx;
        eax = ferror (rdi);
        al = (eax == 0) ? 1 : 0;
        return rax;
        _obstack_newchunk (rbp + 0x70, 8, rdx, rcx);
        rdx = *((rbp + 0x88));
    }
label_8:
    *((rbp + 0x118)) |= 2;
    goto label_1;
label_7:
    *((rbp + 0xc0)) |= 2;
    goto label_2;
label_6:
    _obstack_newchunk (rbp + 0x18, 1, rdx, rcx);
    rax = *((rbp + 0x30));
    goto label_3;
}

/* /tmp/tmpvpgaznxb @ 0x80d0 */
 
uint64_t dbg_ximemdup (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rdi = rsi;
    rax = fcn_00002450 ();
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x26c0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpvpgaznxb @ 0x4710 */
 
int64_t dbg_argv_iter_n_args (argv_iterator const * ai) {
    rdi = ai;
    /* size_t argv_iter_n_args(argv_iterator const * ai); */
    if (*(rdi) != 0) {
        rax = *((rdi + 8));
        return rax;
    }
    rax = *((rdi + 0x28));
    rax -= *((rdi + 0x20));
    rax >>= 3;
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x67c0 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0x66d0)() ();
}

/* /tmp/tmpvpgaznxb @ 0x7210 */
 
void dbg_readtokens0_init (int64_t arg1) {
    rdi = arg1;
    /* void readtokens0_init(Tokens * t); */
    r12 = *(reloc.malloc);
    rbp = *(reloc.free);
    rbx = rdi;
    *((rdi - 0x18)) = 0;
    *((rdi - 0x10)) = 0;
    *((rdi - 8)) = 0;
    _obstack_begin (rdi + 0x18, 0, 0, r12, rbp);
    _obstack_begin (rbx + 0x70, 0, 0, r12, rbp);
    rdi = rbx + 0xc8;
    r8 = rbp;
    rcx = r12;
    edx = 0;
    esi = 0;
    return void (*0x83f0)() ();
}

/* /tmp/tmpvpgaznxb @ 0x74b0 */
 
uint64_t dbg_safe_read (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* size_t safe_read(int fd,void * buf,size_t count); */
    r13d = edi;
    rbx = rdx;
    do {
label_0:
        rax = read (r13d, rbp, rbx);
        r12 = rax;
        if (rax >= 0) {
            goto label_1;
        }
        rax = errno_location ();
        eax = *(rax);
    } while (eax == 4);
    if (rbx > 0x7ff00000) {
        if (eax != 0x16) {
            goto label_1;
        }
        ebx = 0x7ff00000;
        goto label_0;
    }
label_1:
    rax = r12;
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x68a0 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpvpgaznxb @ 0x28d0 */
 
int64_t dbg_main (int32_t argc, char ** argv) {
    argv_iter_err ai_err;
    Tokens tok;
    stat st;
    int64_t var_8h;
    int64_t var_10h;
    uint32_t var_1bh;
    int64_t fildes;
    int64_t var_2ch;
    int64_t var_30h;
    int64_t var_38h;
    void * buf;
    int64_t var_168h;
    int64_t var_180h;
    int64_t var_1e8h;
    rdi = argc;
    rsi = argv;
    /* int main(int argc,char ** argv); */
    r15 = obj_longopts;
    r14 = "clLmw";
    r13 = 0x00009328;
    r12 = 0x000090da;
    ebx = edi;
    rax = *(fs:0x28);
    *((rsp + 0x1e8)) = rax;
    eax = 0;
    set_program_name (*(rsi), rsi, rdx);
    setlocale (6, 0x00009ea1);
    bindtextdomain (r12, "/usr/local/share/locale");
    textdomain (r12, rsi);
    rdi = dbg_close_stdout;
    atexit ();
    rax = getpagesize ();
    rax = (int64_t) eax;
    *(obj.page_size) = rax;
    setvbuf (*(obj.stdout), 0, 1, 0);
    rax = getenv ("POSIXLY_CORRECT");
    *(obj.print_bytes) = 0;
    *(obj.print_chars) = 0;
    *(obj.print_words) = 0;
    *(obj.print_lines) = 0;
    *(obj.print_linelength) = 0;
    *(obj.max_line_length) = 0;
    *(obj.total_bytes) = 0;
    *(obj.total_chars) = 0;
    *(obj.total_words) = 0;
    *(obj.total_lines) = 0;
    obj.posixly_correct = (rax != 0) ? 1 : 0;
    r12d = 0;
    do {
label_0:
        r8d = 0;
        rcx = r15;
        rdx = r14;
        rsi = rbp;
        edi = ebx;
        eax = getopt_long ();
        if (eax == 0xffffffff) {
            goto label_20;
        }
        if (eax > 0x81) {
            goto label_21;
        }
        if (eax <= 0x4b) {
            goto label_22;
        }
        eax -= 0x4c;
        if (eax > 0x35) {
            goto label_21;
        }
        rax = *((r13 + rax*4));
        rax += r13;
        /* switch table (54 cases) at 0x9328 */
        eax = void (*rax)() ();
        r12 = optarg;
    } while (1);
    *(obj.debug) = 1;
    goto label_0;
    *(obj.print_words) = 1;
    goto label_0;
    *(obj.print_chars) = 1;
    goto label_0;
    *(obj.print_lines) = 1;
    goto label_0;
    *(obj.print_bytes) = 1;
    goto label_0;
    *(obj.print_linelength) = 1;
    goto label_0;
label_22:
    if (eax == 0xffffff7d) {
        eax = 0;
        version_etc (*(obj.stdout), 0x0000905d, "GNU coreutils", *(obj.Version), "Paul Rubin", "David MacKenzie");
        eax = exit (0);
    }
    if (eax != 0xffffff7e) {
        goto label_21;
    }
    usage (0);
label_20:
    if (*(obj.print_lines) == 0) {
        if (*(obj.print_words) == 0) {
            goto label_23;
        }
    }
label_9:
    rax = *(obj.optind);
    if (r12 == 0) {
        goto label_24;
    }
    if (ebx > eax) {
        goto label_25;
    }
    eax = strcmp (r12, 0x0000915d);
    if (eax != 0) {
        goto label_26;
    }
    rbp = stdin;
label_14:
    eax = fileno (*(obj.stdin));
    eax = fstat (eax, rsp + 0x150);
    if (eax == 0) {
        eax = *((rsp + 0x168));
        eax &= 0xf000;
        if (eax == 0x8000) {
            goto label_27;
        }
    }
label_16:
    rax = argv_iter_init_stream (rbp);
    r13 = rax;
    if (rax == 0) {
        goto label_28;
    }
    rax = xnmalloc (1, 0x98);
    *((rsp + 0x1b)) = 0;
    *(rsp) = rax;
label_18:
    rax = *(rsp);
    ebx = 0;
    ecx = 1;
    *(rax) = 1;
label_11:
    r14d = 1;
    *(obj.number_width) = ecx;
    ebp = 0;
    r15 = rsp + 0x2c;
    *((rsp + 8)) = r14b;
    while (*(r12) == 0x2d) {
        if (*((r12 + 1)) != 0) {
            goto label_29;
        }
        eax = strcmp (rax, 0x0000915d);
        if (eax == 0) {
            goto label_30;
        }
        if (*(r14) == 0) {
            goto label_31;
        }
        rdx = *(rsp);
        if (rbx == 0) {
            goto label_32;
        }
        rdx = rbp;
label_1:
        rdx += *(rsp);
        *((rsp + 0x10)) = rdx;
        eax = strcmp (r14, 0x0000915d);
        rdx = *((rsp + 0x10));
        if (eax != 0) {
            goto label_32;
        }
        rcx |= 0xffffffffffffffff;
        *(obj.have_read_stdin) = 1;
        eax = wc (0, r14, rdx, rcx);
        edx = eax;
label_3:
label_2:
        if (rbx == 0) {
            rax = *(rsp);
            *(rax) = 1;
        }
        rbp += 0x98;
        rax = argv_iter (r13, r15);
        r14 = rax;
        if (rax == 0) {
            goto label_33;
        }
        if (r12 == 0) {
            goto label_34;
        }
    }
label_29:
    while (*(rax) != 0) {
        edx = 0;
        if (rbx != 0) {
            rdx = rbp;
        }
        goto label_1;
label_30:
        rsi = r14;
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        *((rsp + 8)) = rax;
        rax = dcgettext (0, "when reading file names from stdin, no file name of %s allowed");
        rcx = *((rsp + 8));
        eax = 0;
        error (0, 0, rax);
        if (*(r14) != 0) {
            goto label_35;
        }
label_31:
        rdi = r13;
        rax = argv_iter_n_args ();
        edx = 5;
        r14 = rax;
        rax = dcgettext (0, "invalid zero-length file name");
        rdx = r12;
        esi = 3;
        edi = 0;
        *((rsp + 8)) = rax;
        rax = quotearg_n_style_colon ();
        r9 = *((rsp + 8));
        r8 = r14;
        rcx = rax;
        eax = 0;
        rax = error (0, 0, "%s:%lu: %s");
        *((rsp + 8)) = 0;
        goto label_2;
label_34:
    }
    edx = 5;
    rax = dcgettext (0, "invalid zero-length file name");
    rcx = rax;
    eax = 0;
    error (0, 0, 0x000099e5);
label_35:
    *((rsp + 8)) = 0;
    goto label_2;
label_33:
    eax = *((rsp + 0x2c));
    r14d = *((rsp + 8));
    if (eax == 3) {
        goto label_28;
    }
    if (eax == 4) {
        rdx = r12;
        esi = 3;
        edi = 0;
        rax = quotearg_n_style_colon ();
        edx = 5;
        r14 = rax;
        rax = dcgettext (0, "%s: read error");
        r12 = rax;
        rax = errno_location ();
        rcx = r14;
        eax = 0;
        r14d = 0;
        error (0, *(rax), r12);
label_4:
        if (*((rsp + 0x1b)) != 0) {
            goto label_36;
        }
label_5:
        rdi = r13;
        rax = argv_iter_n_args ();
        if (rax > 1) {
            edx = 5;
            rax = dcgettext (0, "total");
            write_counts (*(obj.total_lines), *(obj.total_words), *(obj.total_chars), *(obj.total_bytes), *(obj.max_line_length), rax);
        }
        argv_iter_free (r13);
        rdi = *(rsp);
        fcn_00002440 ();
        if (*(obj.have_read_stdin) != 0) {
            goto label_37;
        }
label_6:
        r14d ^= 1;
        eax = (int32_t) r14b;
        rdx = *((rsp + 0x1e8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_38;
        }
        return rax;
    }
    if (eax == 2) {
        goto label_39;
    }
    eax = assert_fail ("!\"unexpected error code from argv_iter\", "src/wc.c", 0x3aa, "main");
label_32:
    eax = 0;
    *((rsp + 0x10)) = rdx;
    eax = open (r14, 0, rdx);
    rdx = *((rsp + 0x10));
    edi = eax;
    if (eax == 0xffffffff) {
        goto label_40;
    }
    *((rsp + 0x1c)) = eax;
    al = wc (rdi, r14, rdx, 0);
    *((rsp + 0x10)) = al;
    eax = close (*((rsp + 0x1c)));
    edx = *((rsp + 0x10));
    if (eax == 0) {
        goto label_3;
    }
label_40:
    rdx = r14;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    r14 = rax;
    rax = errno_location ();
    rcx = r14;
    eax = 0;
    error (0, *(rax), 0x000099e5);
    edx = 0;
    goto label_3;
label_39:
    al = (r12 == 0) ? 1 : 0;
    al &= r14b;
    ebx = eax;
    if (al == 0) {
        goto label_4;
    }
    rdi = r13;
    r14d = ebx;
    rax = argv_iter_n_args ();
    if (rax != 0) {
        goto label_4;
    }
    rcx |= 0xffffffffffffffff;
    *(obj.have_read_stdin) = 1;
    eax = wc (0, 0, *(rsp), rcx);
    r14d = eax;
    goto label_4;
label_36:
    readtokens0_free (rsp + 0x30);
    goto label_5;
label_37:
    eax = close (0);
    if (eax == 0) {
        goto label_6;
    }
    rax = errno_location ();
    eax = 0;
    eax = error (1, *(rax), 0x0000915d);
label_24:
    if (ebx > eax) {
        goto label_41;
    }
    rax = 0x0000d110;
    ebx = 1;
    r15 = rax;
label_10:
    rax = argv_iter_init_argv (rax);
    r13 = rax;
    if (rax == 0) {
        goto label_28;
    }
    rax = xnmalloc (rbx, 0x98);
    *((rsp + 0x1b)) = 0;
    *(rsp) = rax;
label_17:
    if (rbx == 1) {
        goto label_42;
    }
label_15:
    rax = *(rsp);
    *((rsp + 8)) = r12;
    ebp = 0;
    r14 = rax + 8;
    while (r12 == 0) {
label_7:
        eax = fstat (0, r14);
label_8:
        rbp++;
        *((r14 - 8)) = eax;
        r14 += 0x98;
        if (rbp >= rbx) {
            goto label_43;
        }
        r12 = *((r15 + rbp*8));
    }
    eax = strcmp (r12, 0x0000915d);
    if (eax == 0) {
        goto label_7;
    }
    rsi = r14;
    rdi = r12;
    stat ();
    goto label_8;
label_25:
    rsi = *((rbp + rax*8));
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "extra operand %s");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    edx = 5;
    rax = dcgettext (0, "file operands cannot be combined with --files0-from");
    rdi = stderr;
    esi = 1;
    rdx = 0x00009e50;
    rcx = rax;
    eax = 0;
    fprintf_chk ();
label_21:
    eax = usage (1);
label_23:
    if (*(obj.print_chars) != 0) {
        goto label_9;
    }
    if (*(obj.print_bytes) != 0) {
        goto label_9;
    }
    if (*(obj.print_linelength) != 0) {
        goto label_9;
    }
    *(obj.print_bytes) = 1;
    *(obj.print_words) = 1;
    *(obj.print_lines) = 1;
    goto label_9;
label_41:
    rdx = (int64_t) eax;
    ebx -= eax;
    rcx = rbp + rdx*8;
    rbx = (int64_t) ebx;
    r15 = rcx;
    goto label_10;
label_43:
    rdx = *(rsp);
    r12 = *((rsp + 8));
    ecx = 1;
    if (*(rdx) > 0) {
        goto label_11;
    }
    edi = 1;
    eax = 0;
    ecx = 0;
    goto label_44;
label_12:
    edi = 7;
    do {
label_13:
        rcx++;
        rdx += 0x98;
        if (rcx >= rbx) {
            goto label_45;
        }
label_44:
    } while (*(rdx) != 0);
    esi = *((rdx + 0x20));
    esi &= 0xf000;
    if (esi != 0x8000) {
        goto label_12;
    }
    rax += *((rdx + 0x38));
    goto label_13;
label_26:
    rax = fopen (r12, 0x00009187);
    if (rax != 0) {
        goto label_14;
    }
    rsi = r12;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "cannot open %s for reading");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (1, *(rax), r12);
label_45:
    ecx = 1;
    esi = 0xa;
    while (rax > 9) {
        edx = 0;
        ecx++;
        rax = rdx:rax / rsi;
        rdx = rdx:rax % rsi;
    }
    if (ecx < edi) {
        ecx = edi;
    }
    goto label_11;
label_42:
    eax = *(obj.print_words);
    ecx = *(obj.print_lines);
    ecx += eax;
    eax = *(obj.print_chars);
    ecx += eax;
    eax = *(obj.print_bytes);
    ecx += eax;
    eax = *(obj.print_linelength);
    ecx += eax;
    if (ecx != 1) {
        goto label_15;
    }
    rax = *(rsp);
    *(rax) = 1;
    goto label_11;
label_27:
    xmm2 = 0;
    __asm ("cvtsi2sd xmm2, qword [rsp + 0x180]");
    *(rsp) = xmm2;
    physmem_available ();
    __asm ("mulsd xmm0, qword [0x000099c0]");
    xmm1 = *(0x000099b8);
    __asm ("comisd xmm0, xmm1");
    if (ecx <= 1) {
        goto label_46;
    }
label_19:
    __asm ("comisd xmm1, xmmword [rsp]");
    if (ecx < 1) {
        goto label_16;
    }
    r13 = rsp + 0x30;
    readtokens0_init (r13);
    al = readtokens0 (rbp, r13, rdx, rcx, r8, r9);
    *((rsp + 0x1b)) = al;
    if (al == 0) {
        goto label_47;
    }
    eax = rpl_fclose (rbp);
    if (eax != 0) {
        goto label_47;
    }
    rax = *((rsp + 0x38));
    rbx = *((rsp + 0x30));
    rdi = rax;
    r15 = rax;
    rax = argv_iter_init_argv (rdi);
    r13 = rax;
    if (rax == 0) {
        goto label_28;
    }
    edi = 1;
    esi = 0x98;
    if (rbx != 0) {
        rdi = rbx;
    }
    rax = xnmalloc (rdi, rsi);
    *(rsp) = rax;
    if (rbx != 0) {
        goto label_17;
    }
    goto label_18;
label_46:
    physmem_available ();
    xmm1 = *(0x000099c0);
    __asm ("mulsd xmm1, xmm0");
    goto label_19;
label_47:
    rsi = r12;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "cannot read file names from %s");
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
label_38:
    stack_chk_fail ();
label_28:
    return xalloc_die ();
}

/* /tmp/tmpvpgaznxb @ 0x4240 */
 
int64_t dbg_usage (int64_t arg1) {
    infomap const[7] const infomap;
    int64_t var_8h;
    int64_t var_10h;
    char * var_18h;
    char * var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    rdi = arg1;
    /* void usage(int status); */
    edx = 5;
    r12 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r12;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_1:
        exit (ebp);
    }
    rbx = "sha256sum";
    rax = dcgettext (0, "Usage: %s [OPTION]... [FILE]...\n  or:  %s [OPTION]... --files0-from=F\n");
    rcx = r12;
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "Print newline, word, and byte counts for each FILE, and a total line if\nmore than one FILE is specified.  A word is a non-zero-length sequence of\nprintable characters delimited by white space.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nWith no FILE, or when FILE is -, read standard input.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nThe options below may be used to select which counts are printed, always in\nthe following order: newline, word, character, byte, maximum line length.\n  -c, --bytes            print the byte counts\n  -m, --chars            print the character counts\n  -l, --lines            print the newline counts\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --files0-from=F    read input from the files specified by\n                           NUL-terminated names in file F;\n                           If F is - then read names from standard input\n  -L, --max-line-length  print the maximum display width\n  -w, --words            print the word counts\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    edx = 5;
    r12 = stdout;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    rax = 0x00009060;
    *((rsp + 0x30)) = rbx;
    rbx = "sha384sum";
    *(rsp) = rax;
    rax = "test invocation";
    rdx = rsp;
    esi = 0x77;
    *((rsp + 8)) = rax;
    rax = 0x000090da;
    edi = 0x63;
    *((rsp + 0x10)) = rax;
    rax = "Multi-call invocation";
    *((rsp + 0x18)) = rax;
    rax = "sha224sum";
    *((rsp + 0x20)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x40)) = rbx;
    rbx = "sha512sum";
    *((rsp + 0x28)) = rax;
    *((rsp + 0x38)) = rax;
    *((rsp + 0x48)) = rax;
    *((rsp + 0x50)) = rbx;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x60)) = 0;
    *((rsp + 0x68)) = 0;
    do {
label_0:
        rax = *((rdx + 0x10));
        rdx += 0x10;
        if (rax == 0) {
            goto label_3;
        }
        ecx = *(rax);
    } while (esi != ecx);
    ecx = *((rax + 1));
    if (edi != ecx) {
        goto label_0;
    }
    if (*((rax + 2)) != 0) {
        goto label_0;
    }
label_3:
    r12 = *((rdx + 8));
    rsi = "\n%s online help: <%s>\n";
    edx = 5;
    edi = 0;
    if (r12 == 0) {
        goto label_4;
    }
    rax = dcgettext (rdi, rsi);
    r13 = "https://www.gnu.org/software/coreutils/";
    rdx = "GNU coreutils";
    edi = 1;
    rsi = rax;
    rcx = r13;
    rbx = 0x0000905d;
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax != 0) {
        eax = strncmp (rdi, 0x000090e4, 3);
        if (eax != 0) {
            goto label_5;
        }
    }
label_2:
    edx = 5;
    rax = dcgettext (0, "Full documentation <%s%s>\n");
    rdx = r13;
    rcx = rbx;
    edi = 1;
    rsi = rax;
    eax = 0;
    r13 = 0x0000907c;
    printf_chk ();
    rax = 0x00009ea1;
    r13 = rax;
    while (1) {
        edx = 5;
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rcx = r13;
        rdx = r12;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        goto label_1;
label_4:
        rax = dcgettext (rdi, rsi);
        r13 = "https://www.gnu.org/software/coreutils/";
        edi = 1;
        rdx = "GNU coreutils";
        rsi = rax;
        rcx = r13;
        eax = 0;
        printf_chk ();
        rax = setlocale (5, 0);
        rdi = rax;
        if (rax != 0) {
            eax = strncmp (rdi, 0x000090e4, 3);
            if (eax != 0) {
                goto label_6;
            }
        }
        edx = 5;
        rax = dcgettext (0, "Full documentation <%s%s>\n");
        r12 = 0x0000905d;
        rdx = r13;
        edi = 1;
        rsi = rax;
        rcx = r12;
        r13 = 0x0000907c;
        eax = 0;
        printf_chk ();
    }
label_6:
    rbx = 0x0000905d;
    r12 = rbx;
label_5:
    r14 = stdout;
    edx = 5;
    rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
    rdi = rax;
    rsi = r14;
    fputs_unlocked ();
    goto label_2;
}

/* /tmp/tmpvpgaznxb @ 0x2740 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmpvpgaznxb @ 0x2640 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmpvpgaznxb @ 0x2730 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmpvpgaznxb @ 0x6570 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x6c60 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000d290]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x0000d2a0]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x6a90 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x289f)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x6970 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x2895)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x4a50 */
 
int64_t dbg_mb_copy (int64_t arg_8h, int64_t arg_10h, int64_t arg_14h, uint32_t arg2, void ** s1) {
    rsi = arg2;
    rdi = s1;
    /* void mb_copy(mbchar_t * new_mbc,mbchar_t const * old_mbc); */
    rbx = rsi;
    rsi += 0x18;
    rcx = *((rsi - 0x18));
    while (1) {
        rax = *((rbx + 8));
        *(rbp) = rcx;
        *((rbp + 8)) = rax;
        eax = *((rbx + 0x10));
        *((rbp + 0x10)) = al;
        if (al != 0) {
            eax = *((rbx + 0x14));
            *((rbp + 0x14)) = eax;
        }
        return rax;
        rcx = rdi + 0x18;
        rax = memcpy (rcx, rsi, *((rbx + 8)));
        rcx = rax;
    }
}

/* /tmp/tmpvpgaznxb @ 0x4ab0 */
 
int32_t dbg_is_basic (char c) {
    rdi = c;
    /* _Bool is_basic(char c); */
    eax = edi;
    rdx = obj_is_basic_table;
    ecx = edi;
    al >>= 5;
    eax &= 7;
    eax = *((rdx + rax*4));
    eax >>= cl;
    eax &= 1;
    return eax;
}

/* /tmp/tmpvpgaznxb @ 0x7100 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpvpgaznxb @ 0x6610 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x288a)() ();
    }
    if (rdx == 0) {
        void (*0x288a)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x6b20 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000d290]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x0000d2a0]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x7ff0 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpvpgaznxb @ 0x8070 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpvpgaznxb @ 0x4830 */
 
uint32_t dbg_fadvise (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void fadvise(FILE * fp,fadvice_t advice); */
    if (rdi != 0) {
        r12d = esi;
        eax = fileno (rdi);
        ecx = r12d;
        edx = 0;
        esi = 0;
        edi = eax;
        void (*0x2610)() ();
    }
    return eax;
}

/* /tmp/tmpvpgaznxb @ 0x26d0 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmpvpgaznxb @ 0x6870 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpvpgaznxb @ 0x4c50 */
 
int64_t dbg_physmem_available (void) {
    sysinfo si;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_38h;
    int64_t var_48h;
    int64_t var_78h;
    int64_t var_88h;
    /* double physmem_available(); */
    edi = 0x56;
    rax = *(fs:0x28);
    *((rsp + 0x88)) = rax;
    eax = 0;
    rax = sysconf ();
    xmm0 = 0;
    edi = 0x1e;
    __asm ("cvtsi2sd xmm0, rax");
    *((rsp + 8)) = xmm0;
    rax = sysconf ();
    xmm0 = *((rsp + 8));
    xmm1 = 0;
    __asm ("comisd xmm0, xmm1");
    if (? >= ?) {
        xmm2 = 0;
        __asm ("cvtsi2sd xmm2, rax");
        __asm ("comisd xmm2, xmm1");
        if (? >= ?) {
            goto label_3;
        }
    }
    rdi = rsp + 0x10;
    eax = sysinfo ();
    if (eax != 0) {
        goto label_4;
    }
    rax = *((rsp + 0x38));
    if (rax < 0) {
        goto label_5;
    }
    xmm0 = 0;
    __asm ("cvtsi2sd xmm0, rax");
    rax = *((rsp + 0x48));
    if (rax < 0) {
        goto label_6;
    }
label_1:
    xmm1 = 0;
    __asm ("cvtsi2sd xmm1, rax");
label_2:
    eax = *((rsp + 0x78));
    __asm ("addsd xmm0, xmm1");
    xmm1 = 0;
    __asm ("cvtsi2sd xmm1, rax");
    __asm ("mulsd xmm0, xmm1");
    do {
label_0:
        rax = *((rsp + 0x88));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_7;
        }
        return rax;
label_4:
        rax = physmem_total ();
        __asm ("mulsd xmm0, qword [0x00009a28]");
    } while (1);
label_3:
    __asm ("mulsd xmm0, xmm2");
    goto label_0;
label_5:
    rdx = rax;
    eax &= 1;
    xmm0 = 0;
    rdx >>= 1;
    rdx |= rax;
    rax = *((rsp + 0x48));
    __asm ("cvtsi2sd xmm0, rdx");
    __asm ("addsd xmm0, xmm0");
    if (rax >= 0) {
        goto label_1;
    }
label_6:
    rdx = rax;
    eax &= 1;
    xmm1 = 0;
    rdx >>= 1;
    rdx |= rax;
    __asm ("cvtsi2sd xmm1, rdx");
    __asm ("addsd xmm1, xmm1");
    goto label_2;
label_7:
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x7d10 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpvpgaznxb @ 0x70e0 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpvpgaznxb @ 0x4940 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x27a0)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmpvpgaznxb @ 0x65b0 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x7ef0 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmpvpgaznxb @ 0x7b30 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmpvpgaznxb @ 0x2680 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmpvpgaznxb @ 0x4750 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmpvpgaznxb @ 0x81d0 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpvpgaznxb @ 0x24c0 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmpvpgaznxb @ 0x7520 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = 0x00009e48;
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x00009e5b);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x0000a148;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0xa148 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x2820)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2820)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2820)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmpvpgaznxb @ 0x79b0 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpvpgaznxb @ 0x2000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmpvpgaznxb @ 0x8050 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpvpgaznxb @ 0x8710 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmpvpgaznxb @ 0x7d50 */
 
uint64_t dbg_xnmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpvpgaznxb @ 0x8110 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rdi = rsi + 1;
    rax = fcn_00002450 ();
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x26c0)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpvpgaznxb @ 0x2470 */
 
void getenv (void) {
    /* [15] -r-x section size 1040 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmpvpgaznxb @ 0x0 */
 
int64_t libc_start_main (int32_t argc, func init, func main, char ** ubp_av) {
    rsi = argc;
    rcx = init;
    rdi = main;
    rdx = ubp_av;
    bh &= *(rdi);
    *(rax) += dh;
    *((rax + 0x17)) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax += *(rax);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax)++;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al += bl;
    al += *(rax);
    *(rax) += al;
    *(rax) += al;
    al += bl;
    al += *(rax);
    *(rax) += al;
    *(rax) += al;
    *(rax) += cl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
}

/* /tmp/tmpvpgaznxb @ 0x24b0 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmpvpgaznxb @ 0x24d0 */
 
void ferror (void) {
    __asm ("bnd jmp qword [reloc.ferror]");
}

/* /tmp/tmpvpgaznxb @ 0x2500 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmpvpgaznxb @ 0x2510 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmpvpgaznxb @ 0x2520 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmpvpgaznxb @ 0x2540 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmpvpgaznxb @ 0x2570 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmpvpgaznxb @ 0x2580 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmpvpgaznxb @ 0x2590 */
 
void strchr (void) {
    __asm ("bnd jmp qword [reloc.strchr]");
}

/* /tmp/tmpvpgaznxb @ 0x25a0 */
 
void overflow (void) {
    __asm ("bnd jmp qword [reloc.__overflow]");
}

/* /tmp/tmpvpgaznxb @ 0x25c0 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmpvpgaznxb @ 0x25d0 */
 
void assert_fail (void) {
    __asm ("bnd jmp qword [reloc.__assert_fail]");
}

/* /tmp/tmpvpgaznxb @ 0x25e0 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmpvpgaznxb @ 0x25f0 */
 
void fgetc (void) {
    __asm ("bnd jmp qword [reloc.fgetc]");
}

/* /tmp/tmpvpgaznxb @ 0x2600 */
 
void close (void) {
    __asm ("bnd jmp qword [reloc.close]");
}

/* /tmp/tmpvpgaznxb @ 0x2610 */
 
void posix_fadvise (void) {
    __asm ("bnd jmp qword [reloc.posix_fadvise]");
}

/* /tmp/tmpvpgaznxb @ 0x2620 */
 
void read (void) {
    __asm ("bnd jmp qword [reloc.read]");
}

/* /tmp/tmpvpgaznxb @ 0x2630 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmpvpgaznxb @ 0x2650 */
 
void rawmemchr (void) {
    __asm ("bnd jmp qword [reloc.rawmemchr]");
}

/* /tmp/tmpvpgaznxb @ 0x2670 */
 
void strcmp (void) {
    __asm ("bnd jmp qword [reloc.strcmp]");
}

/* /tmp/tmpvpgaznxb @ 0x2690 */
 
void memmove_chk (void) {
    __asm ("bnd jmp qword [reloc.__memmove_chk]");
}

/* /tmp/tmpvpgaznxb @ 0x26b0 */
 
void stat (void) {
    __asm ("bnd jmp qword [reloc.stat]");
}

/* /tmp/tmpvpgaznxb @ 0x26c0 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmpvpgaznxb @ 0x26f0 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmpvpgaznxb @ 0x2710 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmpvpgaznxb @ 0x2750 */
 
void setvbuf (void) {
    __asm ("bnd jmp qword [reloc.setvbuf]");
}

/* /tmp/tmpvpgaznxb @ 0x2760 */
 
void iswspace (void) {
    __asm ("bnd jmp qword [reloc.iswspace]");
}

/* /tmp/tmpvpgaznxb @ 0x2770 */
 
void btowc (void) {
    __asm ("bnd jmp qword [reloc.btowc]");
}

/* /tmp/tmpvpgaznxb @ 0x2790 */
 
void open (void) {
    __asm ("bnd jmp qword [reloc.open]");
}

/* /tmp/tmpvpgaznxb @ 0x27a0 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmpvpgaznxb @ 0x27b0 */
 
void fopen (void) {
    __asm ("bnd jmp qword [reloc.fopen]");
}

/* /tmp/tmpvpgaznxb @ 0x27c0 */
 
void sysconf (void) {
    __asm ("bnd jmp qword [reloc.sysconf]");
}

/* /tmp/tmpvpgaznxb @ 0x27d0 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmpvpgaznxb @ 0x27e0 */
 
void sysinfo (void) {
    __asm ("bnd jmp qword [reloc.sysinfo]");
}

/* /tmp/tmpvpgaznxb @ 0x27f0 */
 
void getpagesize (void) {
    __asm ("bnd jmp qword [reloc.getpagesize]");
}

/* /tmp/tmpvpgaznxb @ 0x2830 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmpvpgaznxb @ 0x2840 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmpvpgaznxb @ 0x2850 */
 
void fstat (void) {
    __asm ("bnd jmp qword [reloc.fstat]");
}

/* /tmp/tmpvpgaznxb @ 0x2870 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmpvpgaznxb @ 0x2440 */
 
void fcn_00002440 (void) {
    /* [14] -r-x section size 48 named .plt.got */
    __asm ("bnd jmp qword [reloc.free]");
}

/* /tmp/tmpvpgaznxb @ 0x2030 */
 
void fcn_00002030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 1056 named .plt */
    __asm ("bnd jmp qword [0x0000cdb8]");
}

/* /tmp/tmpvpgaznxb @ 0x2040 */
 
void fcn_00002040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2050 */
 
void fcn_00002050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2060 */
 
void fcn_00002060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2070 */
 
void fcn_00002070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2080 */
 
void fcn_00002080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2090 */
 
void fcn_00002090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x20a0 */
 
void fcn_000020a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x20b0 */
 
void fcn_000020b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x20c0 */
 
void fcn_000020c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x20d0 */
 
void fcn_000020d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x20e0 */
 
void fcn_000020e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x20f0 */
 
void fcn_000020f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2100 */
 
void fcn_00002100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2110 */
 
void fcn_00002110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2120 */
 
void fcn_00002120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2130 */
 
void fcn_00002130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2140 */
 
void fcn_00002140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2150 */
 
void fcn_00002150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2160 */
 
void fcn_00002160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2170 */
 
void fcn_00002170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2180 */
 
void fcn_00002180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2190 */
 
void fcn_00002190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x21a0 */
 
void fcn_000021a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x21b0 */
 
void fcn_000021b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x21c0 */
 
void fcn_000021c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x21d0 */
 
void fcn_000021d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x21e0 */
 
void fcn_000021e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x21f0 */
 
void fcn_000021f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2200 */
 
void fcn_00002200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2210 */
 
void fcn_00002210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2220 */
 
void fcn_00002220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2230 */
 
void fcn_00002230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2240 */
 
void fcn_00002240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2250 */
 
void fcn_00002250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2260 */
 
void fcn_00002260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2270 */
 
void fcn_00002270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2280 */
 
void fcn_00002280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2290 */
 
void fcn_00002290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x22a0 */
 
void fcn_000022a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x22b0 */
 
void fcn_000022b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x22c0 */
 
void fcn_000022c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x22d0 */
 
void fcn_000022d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x22e0 */
 
void fcn_000022e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x22f0 */
 
void fcn_000022f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2300 */
 
void fcn_00002300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2310 */
 
void fcn_00002310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2320 */
 
void fcn_00002320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2330 */
 
void fcn_00002330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2340 */
 
void fcn_00002340 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2350 */
 
void fcn_00002350 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2360 */
 
void fcn_00002360 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2370 */
 
void fcn_00002370 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2380 */
 
void fcn_00002380 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2390 */
 
void fcn_00002390 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x23a0 */
 
void fcn_000023a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x23b0 */
 
void fcn_000023b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x23c0 */
 
void fcn_000023c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x23d0 */
 
void fcn_000023d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x23e0 */
 
void fcn_000023e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x23f0 */
 
void fcn_000023f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2400 */
 
void fcn_00002400 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2410 */
 
void fcn_00002410 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2420 */
 
void fcn_00002420 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpvpgaznxb @ 0x2430 */
 
void fcn_00002430 (void) {
    return __asm ("bnd jmp section..plt");
}
