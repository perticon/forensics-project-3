
struct s0 {
    int32_t f0;
    signed char[12] pad16;
    int64_t f10;
    int64_t f18;
    uint64_t f20;
    uint64_t f28;
};

struct s0* fun_11c0();

int64_t stderr = 0;

void fun_11f0(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_11e0(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

struct s0* xalloc(void* rdi, ...) {
    struct s0* rax2;
    int64_t rcx3;

    rax2 = fun_11c0();
    if (!rax2) {
        rcx3 = stderr;
        fun_11f0("Virtual memory exhausted.\n", 1, 26, rcx3);
        fun_11e0(1, 1, 26, rcx3);
    } else {
        return rax2;
    }
}

void fun_11d0(int64_t rdi, int64_t rsi, void* rdx, struct s0* rcx);

void fun_1140(int64_t rdi);

void print_wide_uint(uint64_t rdi, uint64_t rsi, int64_t rdx, struct s0* rcx) {
    int64_t rbx5;
    int64_t rdx6;
    uint64_t r15_7;
    struct s0* rcx8;
    int64_t rdx9;
    void* rdx10;

    *reinterpret_cast<int32_t*>(&rbx5) = *reinterpret_cast<int32_t*>(&rdx);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx5) + 4) = 0;
    if (!(rsi | rdi & 0xfffffffff0000000)) {
        if (*reinterpret_cast<int32_t*>(&rbx5)) {
            fun_11d0(1, "(uintmax_t) ", 7, rcx);
        }
    } else {
        *reinterpret_cast<int32_t*>(&rdx6) = static_cast<int32_t>(rdx + 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx6) + 4) = 0;
        __asm__("shrd r14, r9, 0x1c");
        __asm__("shrd r8, r9, 0x38");
        r15_7 = rsi >> 28;
        if (rdi | rsi >> 56) {
            fun_1140(40);
            *reinterpret_cast<int32_t*>(&rcx8) = *reinterpret_cast<int32_t*>(&rcx);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx8) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx9) = *reinterpret_cast<int32_t*>(&rdx6);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx9) + 4) = 0;
            print_wide_uint(rdi, r15_7, rdx9, rcx8);
            *reinterpret_cast<int32_t*>(&rdx10) = static_cast<int32_t>(rbx5 + 3);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx10) + 4) = 0;
            rcx = reinterpret_cast<struct s0*>(0x2087);
            fun_11d0(1, ")\n%*s", rdx10, 0x2087);
        } else {
            print_wide_uint(rdi, r15_7, rdx6, rcx);
        }
        fun_11d0(1, " << %d | ", 28, rcx);
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0x4010;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

int64_t __gmon_start__ = 0;

void fun_1003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t g3f58 = 0;

void fun_1033() {
    __asm__("cli ");
    goto g3f58;
}

void fun_1043() {
    __asm__("cli ");
    goto 0x1020;
}

void fun_1053() {
    __asm__("cli ");
    goto 0x1020;
}

void fun_1063() {
    __asm__("cli ");
    goto 0x1020;
}

void fun_1073() {
    __asm__("cli ");
    goto 0x1020;
}

void fun_1083() {
    __asm__("cli ");
    goto 0x1020;
}

void fun_1093() {
    __asm__("cli ");
    goto 0x1020;
}

void fun_10a3() {
    __asm__("cli ");
    goto 0x1020;
}

void fun_10b3() {
    __asm__("cli ");
    goto 0x1020;
}

void fun_10c3() {
    __asm__("cli ");
    goto 0x1020;
}

void fun_10d3() {
    __asm__("cli ");
    goto 0x1020;
}

void fun_10e3() {
    __asm__("cli ");
    goto 0x1020;
}

void fun_10f3() {
    __asm__("cli ");
    goto 0x1020;
}

void fun_1103() {
    __asm__("cli ");
    goto 0x1020;
}

void fun_1113() {
    __asm__("cli ");
    goto 0x1020;
}

int64_t __cxa_finalize = 0;

void fun_1123() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t free = 0x1030;

void fun_1133() {
    __asm__("cli ");
    goto free;
}

int64_t putchar = 0x1040;

void fun_1143() {
    __asm__("cli ");
    goto putchar;
}

int64_t abort = 0x1050;

void fun_1153() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x1060;

void fun_1163() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t puts = 0x1070;

void fun_1173() {
    __asm__("cli ");
    goto puts;
}

int64_t ferror = 0x1080;

void fun_1183() {
    __asm__("cli ");
    goto ferror;
}

int64_t fclose = 0x1090;

void fun_1193() {
    __asm__("cli ");
    goto fclose;
}

int64_t memset = 0x10a0;

void fun_11a3() {
    __asm__("cli ");
    goto memset;
}

int64_t strtol = 0x10b0;

void fun_11b3() {
    __asm__("cli ");
    goto strtol;
}

int64_t malloc = 0x10c0;

void fun_11c3() {
    __asm__("cli ");
    goto malloc;
}

int64_t __printf_chk = 0x10d0;

void fun_11d3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t exit = 0x10e0;

void fun_11e3() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x10f0;

void fun_11f3() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x1100;

void fun_1203() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t strerror = 0x1110;

void fun_1213() {
    __asm__("cli ");
    goto strerror;
}

struct s1 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_11b0(int64_t rdi);

void fun_11a0(struct s0* rdi, int64_t rsi, void* rdx);

void fun_1170(int64_t rdi, int64_t rsi, void* rdx, struct s0* rcx);

void fun_1130(struct s0* rdi, int64_t rsi, void* rdx, struct s0* rcx);

int64_t stdout = 0;

int32_t fun_1180(int64_t rdi, int64_t rsi, void* rdx, struct s0* rcx);

int32_t fun_1190(int64_t rdi, int64_t rsi, void* rdx, struct s0* rcx);

int32_t* fun_1160(int64_t rdi, int64_t rsi, void* rdx, struct s0* rcx);

int64_t fun_1210(int64_t rdi, int64_t rsi, void* rdx, struct s0* rcx);

void fun_1200(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_1150();

struct s2 {
    int32_t f0;
    signed char[12] pad16;
    uint64_t f10;
    uint64_t f18;
    int64_t f20;
    void* f28;
};

int64_t __udivti3(int64_t rdi, int64_t rsi);

int64_t fun_1233(int32_t edi, struct s1* rsi) {
    int64_t rdi3;
    int64_t rax4;
    int64_t rcx5;
    int64_t rdx6;
    int32_t ebx7;
    void* rbp8;
    struct s0* rax9;
    void* rdx10;
    int64_t rsi11;
    struct s0* r14_12;
    void* rdi13;
    struct s0* rax14;
    struct s0* v15;
    int64_t rax16;
    void* rbx17;
    uint32_t v18;
    void* rdx19;
    int64_t rsi20;
    uint32_t r13d21;
    struct s0* v22;
    uint32_t r12d23;
    uint32_t r14d24;
    int64_t* r13_25;
    uint32_t r15d26;
    uint32_t edx27;
    int64_t rax28;
    struct s0* rcx29;
    void* rdx30;
    uint64_t rdi31;
    uint64_t rsi32;
    struct s0* rcx33;
    struct s0* r8_34;
    uint32_t r9d35;
    int64_t rdi36;
    int64_t rsi37;
    int64_t rax38;
    void* rdx39;
    int64_t rdi40;
    int32_t eax41;
    int64_t rdi42;
    int32_t eax43;
    int64_t rax44;
    int32_t* rax45;
    int64_t rdi46;
    int64_t rax47;
    int64_t rdi48;
    struct s0* rax49;
    void* rdi50;
    struct s0* rax51;
    uint64_t r13_52;
    void* r8_53;
    struct s2* rax54;
    struct s2* v55;
    void* r15_56;
    void* rax57;
    int64_t rax58;
    int32_t ecx59;
    uint64_t rax60;
    uint64_t r12_61;
    uint64_t rcx62;
    uint64_t r10_63;
    uint64_t r11_64;
    uint64_t rsi65;
    uint64_t rax66;
    uint64_t rdx67;
    uint64_t rsi68;
    uint64_t rdi69;
    int64_t rax70;
    void* rax71;

    __asm__("cli ");
    if (edi == 2) {
        rdi3 = rsi->f8;
        rax4 = fun_11b0(rdi3);
        if (*reinterpret_cast<int32_t*>(&rax4) <= 2) 
            goto addr_1512_3;
        if (*reinterpret_cast<unsigned char*>(&rax4) & 1) 
            goto addr_12b4_5;
    } else {
        rcx5 = rsi->f0;
        rdx6 = reinterpret_cast<int64_t>("Usage: %s LIMIT\nProduces a list of odd primes <= LIMIT\n");
        goto addr_1251_7;
    }
    ebx7 = static_cast<int32_t>(rax4 - 2) >> 1;
    rbp8 = reinterpret_cast<void*>(static_cast<int64_t>(ebx7));
    rax9 = xalloc(rbp8);
    rdx10 = rbp8;
    *reinterpret_cast<int32_t*>(&rsi11) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
    r14_12 = rax9;
    fun_11a0(rax9, 1, rdx10);
    rdi13 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp8) * 48);
    rax14 = xalloc(rdi13, rdi13);
    v15 = rax14;
    if (ebx7) {
        addr_12e5_9:
        *reinterpret_cast<uint32_t*>(&rax16) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rbx17) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx17) + 4) = 0;
        goto addr_12f0_10;
    } else {
        v18 = 0;
    }
    addr_1424_12:
    fun_1170("/* Generated file -- DO NOT EDIT */\n", rsi11, rdx10, 0);
    *reinterpret_cast<uint32_t*>(&rdx19) = 0x80;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx19) + 4) = 0;
    rsi20 = reinterpret_cast<int64_t>("#define WIDE_UINT_BITS %u\n");
    fun_11d0(1, "#define WIDE_UINT_BITS %u\n", 0x80, 0);
    if (!v18) {
        r13d21 = 2;
    } else {
        v22 = r14_12;
        r12d23 = 8;
        r14d24 = 2;
        r13_25 = &v15->f10;
        r15d26 = v18 + 8;
        do {
            edx27 = r14d24;
            r14d24 = *reinterpret_cast<uint32_t*>(r13_25 - 2);
            if (v18 > r12d23) {
                *reinterpret_cast<uint32_t*>(&rax28) = r12d23;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax28) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rcx29) = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(v15) + (rax28 + rax28 * 2 << 4)) - r14d24;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx29) + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rcx29) > 0xff) 
                    goto addr_1220_17;
            } else {
                *reinterpret_cast<uint32_t*>(&rcx29) = 0xff;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx29) + 4) = 0;
            }
            ++r12d23;
            r13_25 = r13_25 + 6;
            *reinterpret_cast<uint32_t*>(&rdx30) = r14d24 - edx27;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx30) + 4) = 0;
            fun_11d0(1, "P (%u, %u,\n   (", rdx30, rcx29);
            rdi31 = *reinterpret_cast<uint64_t*>(r13_25 - 6);
            rsi32 = *reinterpret_cast<uint64_t*>(r13_25 - 5);
            *reinterpret_cast<int32_t*>(&rcx33) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx33) + 4) = 0;
            print_wide_uint(rdi31, rsi32, 0, 0x80);
            *reinterpret_cast<uint32_t*>(&rdx19) = r14d24;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx19) + 4) = 0;
            rsi20 = reinterpret_cast<int64_t>("),\n   UINTMAX_MAX / %u)\n");
            fun_11d0(1, "),\n   UINTMAX_MAX / %u)\n", rdx19, 0x80);
        } while (r15d26 != r12d23);
        goto addr_1569_20;
    }
    addr_1570_21:
    fun_1170("\n#undef FIRST_OMITTED_PRIME", rsi20, rdx19, rcx33);
    r8_34 = v15;
    r9d35 = reinterpret_cast<uint32_t>(r8_34->f0 * r8_34->f0);
    while (r13d21 = r13d21 + 2, r13d21 >= r9d35) {
        *reinterpret_cast<uint32_t*>(&rdi36) = r13d21;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi36) + 4) = 0;
        rcx33 = r8_34;
        *reinterpret_cast<int32_t*>(&rsi37) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi37) + 4) = 0;
        do {
            if (rcx33->f28 >= __intrinsic() + rcx33->f18 * rdi36 + static_cast<uint64_t>(reinterpret_cast<uint1_t>(rcx33->f20 < reinterpret_cast<uint64_t>(rdi36 * rcx33->f10)))) 
                break;
            *reinterpret_cast<int32_t*>(&rax38) = static_cast<int32_t>(rsi37 + 1);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax38) + 4) = 0;
            rsi37 = rax38;
            rcx33 = reinterpret_cast<struct s0*>((rax38 + rax38 * 2 << 4) + reinterpret_cast<int64_t>(r8_34));
        } while (r13d21 >= reinterpret_cast<uint32_t>(rcx33->f0 * rcx33->f0));
        break;
    }
    *reinterpret_cast<uint32_t*>(&rdx39) = r13d21;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx39) + 4) = 0;
    fun_11d0(1, "#define FIRST_OMITTED_PRIME %u\n", rdx39, rcx33);
    fun_1130(r14_12, "#define FIRST_OMITTED_PRIME %u\n", rdx39, rcx33);
    fun_1130(v15, "#define FIRST_OMITTED_PRIME %u\n", rdx39, rcx33);
    rdi40 = stdout;
    eax41 = fun_1180(rdi40, "#define FIRST_OMITTED_PRIME %u\n", rdx39, rcx33);
    rdi42 = stdout;
    eax43 = fun_1190(rdi42, "#define FIRST_OMITTED_PRIME %u\n", rdx39, rcx33);
    if (!(eax41 + eax43)) {
        addr_1512_3:
        *reinterpret_cast<int32_t*>(&rax44) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax44) + 4) = 0;
    } else {
        rax45 = fun_1160(rdi42, "#define FIRST_OMITTED_PRIME %u\n", rdx39, rcx33);
        *reinterpret_cast<int32_t*>(&rdi46) = *rax45;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi46) + 4) = 0;
        rax47 = fun_1210(rdi46, "#define FIRST_OMITTED_PRIME %u\n", rdx39, rcx33);
        rdx6 = reinterpret_cast<int64_t>("write error: %s\n");
        rcx5 = rax47;
        goto addr_1251_7;
    }
    addr_1269_28:
    return rax44;
    addr_1251_7:
    rdi48 = stderr;
    fun_1200(rdi48, 1, rdx6, rcx5);
    *reinterpret_cast<int32_t*>(&rax44) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax44) + 4) = 0;
    goto addr_1269_28;
    addr_1220_17:
    fun_1150();
    addr_1569_20:
    r13d21 = r14d24;
    r14_12 = v22;
    goto addr_1570_21;
    addr_12b4_5:
    rbp8 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax4) - 1 >> 1));
    rax49 = xalloc(rbp8);
    r14_12 = rax49;
    fun_11a0(rax49, 1, rbp8);
    rdi50 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp8) * 48);
    rax51 = xalloc(rdi50, rdi50);
    v15 = rax51;
    goto addr_12e5_9;
    while (rbx17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx17) + 1), reinterpret_cast<uint64_t>(rbx17) < reinterpret_cast<uint64_t>(rbp8)) {
        while (1) {
            if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_12) + reinterpret_cast<uint64_t>(rbx17))) 
                break;
            *reinterpret_cast<uint32_t*>(&rax16) = v18;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
            addr_12f0_10:
            *reinterpret_cast<int32_t*>(&r13_52) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_52) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r8_53) = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbx17) + reinterpret_cast<uint64_t>(rbx17) + 3);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_53) + 4) = 0;
            rax54 = reinterpret_cast<struct s2*>((rax16 + rax16 * 2 << 4) + reinterpret_cast<int64_t>(v15));
            v18 = static_cast<uint32_t>(rax16 + 1);
            v55 = rax54;
            r15_56 = r8_53;
            rax54->f0 = *reinterpret_cast<int32_t*>(&r8_53);
            rax57 = r8_53;
            __asm__("shrd rax, r9, 0x1");
            *reinterpret_cast<uint32_t*>(&rax58) = *reinterpret_cast<uint32_t*>(&rax57) & 7;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax58) + 4) = 0;
            ecx59 = static_cast<int32_t>(rax58 * 4);
            *reinterpret_cast<uint32_t*>(&rax60) = 0xf5397db1 >> *reinterpret_cast<signed char*>(&ecx59);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax60) + 4) = 0;
            r12_61 = rax60;
            do {
                rcx62 = r13_52;
                __asm__("shld rdi, r12, 0x1");
                r10_63 = r12_61;
                r11_64 = r13_52 * r12_61;
                rsi65 = r12_61 + r12_61;
                rax66 = r12_61 * r12_61 * reinterpret_cast<uint64_t>(r8_53);
                rdx67 = __intrinsic() + (__intrinsic() + (r11_64 + r11_64)) * reinterpret_cast<uint64_t>(r8_53);
                rsi68 = rsi65 - rax66;
                rdi69 = r13_52 - (rdx67 + reinterpret_cast<uint1_t>(r13_52 < rdx67 + reinterpret_cast<uint1_t>(rsi65 < rax66)));
                r12_61 = rsi68;
                r13_52 = rdi69;
            } while (rcx62 ^ rdi69 | r10_63 ^ rsi68);
            rsi11 = -1;
            v55->f18 = rcx62;
            rcx33 = reinterpret_cast<struct s0*>(0);
            v55->f10 = r10_63;
            rax70 = __udivti3(-1, -1);
            v55->f20 = rax70;
            v55->f28 = r8_53;
            *reinterpret_cast<uint32_t*>(&rax71) = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&r15_56) * *reinterpret_cast<int32_t*>(&r15_56) - 3) >> 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax71) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax71);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx10) + 4) = 0;
            if (reinterpret_cast<uint64_t>(rbp8) <= reinterpret_cast<uint64_t>(rdx10)) 
                break;
            do {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_12) + reinterpret_cast<uint64_t>(rdx10)) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<uint64_t>(rax71) + reinterpret_cast<uint64_t>(r15_56));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx10) + 4) = 0;
                rax71 = rdx10;
            } while (reinterpret_cast<uint64_t>(rdx10) < reinterpret_cast<uint64_t>(rbp8));
            rbx17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx17) + 1);
            if (reinterpret_cast<uint64_t>(rbx17) >= reinterpret_cast<uint64_t>(rbp8)) 
                goto addr_1424_12;
        }
    }
    goto addr_1424_12;
}

int64_t __libc_start_main = 0;

void fun_1663() {
    __asm__("cli ");
    __libc_start_main(0x1230, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0x4008;

void fun_1120(int64_t rdi);

int64_t fun_1703() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_1120(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_1743() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

uint64_t fun_18c3(uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8) {
    uint64_t r8_6;
    int32_t ecx7;
    int64_t rdx8;
    int32_t ecx9;
    int32_t ecx10;
    uint64_t rbx11;
    int32_t ecx12;
    int32_t ecx13;
    uint64_t rax14;
    uint64_t rax15;
    uint64_t r11_16;
    int32_t ecx17;
    uint64_t rax18;
    uint64_t rcx19;

    __asm__("cli ");
    if (rcx) {
        if (rcx <= rsi) {
            __asm__("bsr r8, rcx");
            r8_6 = r8 ^ 63;
            if (*reinterpret_cast<int32_t*>(&r8_6)) {
                ecx7 = *reinterpret_cast<int32_t*>(&r8_6);
                rdx8 = 64 - *reinterpret_cast<int32_t*>(&r8_6);
                ecx9 = *reinterpret_cast<int32_t*>(&rdx8);
                ecx10 = *reinterpret_cast<int32_t*>(&r8_6);
                rbx11 = rdx >> *reinterpret_cast<signed char*>(&ecx9) | rcx << *reinterpret_cast<unsigned char*>(&ecx7);
                ecx12 = *reinterpret_cast<int32_t*>(&r8_6);
                ecx13 = *reinterpret_cast<int32_t*>(&rdx8);
                rax14 = rdi >> *reinterpret_cast<signed char*>(&ecx13) | rsi << *reinterpret_cast<unsigned char*>(&ecx12);
                rax15 = rax14 / rbx11;
                r11_16 = rax14 % rbx11;
                if (r11_16 < __intrinsic() || (ecx17 = *reinterpret_cast<int32_t*>(&r8_6), rdi << *reinterpret_cast<unsigned char*>(&ecx17) < rax15 * (rdx << *reinterpret_cast<unsigned char*>(&ecx10))) && r11_16 == __intrinsic()) {
                    return rax15 + 0xffffffffffffffff;
                } else {
                    return rax15;
                }
            } else {
                if (rcx < rsi || (*reinterpret_cast<int32_t*>(&rax18) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0, rdx <= rdi)) {
                    return 1;
                }
            }
        } else {
            return 0;
        }
    } else {
        if (rdx <= rsi) {
            rcx19 = rdx;
            if (!rdx) {
                rcx19 = 1 / rdx;
            }
            return rdi / rcx19;
        } else {
            rax18 = rdi / rdx;
        }
    }
    return rax18;
}

void fun_19bb() {
    __asm__("cli ");
    return;
}
