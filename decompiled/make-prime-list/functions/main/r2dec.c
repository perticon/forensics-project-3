int32_t main (int64_t arg5, int64_t arg6, uint32_t argc, char ** str) {
    int64_t var_8h;
    uint32_t var_14h;
    void * ptr;
    int64_t var_20h;
    int64_t var_28h;
    r8 = arg5;
    r9 = arg6;
    rdi = argc;
    rsi = str;
    if (edi != 2) {
        rcx = *(rsi);
        rdx = "Usage: %s LIMIT\nProduces a list of odd primes <= LIMIT\n";
label_8:
        rdi = stderr;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        eax = 1;
label_2:
        return eax;
    }
    *(rsp) = r8;
    *((rsp + 8)) = r9;
    eax = strtol (*((rsi + 8)), 0, 0xa);
    r8 = *(rsp);
    r9 = *((rsp + 8));
    if (eax <= 2) {
        goto label_7;
    }
    *(rsp) = r8;
    *((rsp + 8)) = r9;
    if ((al & 1) == 0) {
        goto label_10;
    }
    eax--;
    eax >>= 1;
    rbp = (int64_t) eax;
    rax = xalloc (rbp);
    rdi = rax;
    r14 = rax;
    memset (rdi, 1, rbp);
    rdi = rbp * 0x30;
    rax = xalloc (rdi);
    *((rsp + 0x18)) = rax;
label_3:
    eax = 0;
    ebx = 0;
label_5:
    edi = rax + 1;
    rax *= 3;
    r9d = 0;
    r13d = 0;
    r8d = rbx + rbx + 3;
    rax <<= 4;
    rax += *((rsp + 0x18));
    *((rsp + 0x14)) = edi;
    *(rsp) = rax;
    r15 = r8;
    *(rax) = r8d;
    rax = r8;
    __asm ("shrd rax, r9, 1");
    eax &= 7;
    ecx = rax*4;
    eax = 0xf5397db1;
    eax >>= cl;
    r12 = rax;
    do {
        r11 = r13;
        rax = r12;
        rcx = r13;
        rdi = r13;
        rdx:rax = rax * r12;
        rsi = r12;
        __asm ("shld rdi, r12, 1");
        r10 = r12;
        r11 *= r12;
        rsi += r12;
        r13 = rdx;
        r12 = rax;
        rdx = r9;
        r11 += r11;
        rdx *= r12;
        rax = r12;
        r13 += r11;
        r11 = r13;
        r11 *= r8;
        r11 += rdx;
        rdx:rax = rax * r8;
        rdx += r11;
        rsi -= rax;
        rax = rcx;
        rdi -= rdx;
        rdx = r10;
        r12 = rsi;
        rax ^= rdi;
        rdx ^= rsi;
        r13 = rdi;
        rax |= rdx;
    } while (rax != 0);
    rax = *(rsp);
    rdx = r8;
    rdi = 0xffffffffffffffff;
    rsi = 0xffffffffffffffff;
    *((rsp + 0x20)) = r8;
    *((rax + 0x18)) = rcx;
    rcx = r9;
    *((rax + 0x10)) = r10;
    *((rsp + 0x28)) = r9;
    rax = _udivti3 ();
    rdi = *(rsp);
    *((rdi + 0x20)) = rax;
    eax = r15d;
    eax *= r15d;
    *((rdi + 0x28)) = rdx;
    eax -= 3;
    eax >>= 1;
    edx = eax;
    if (rbp <= rdx) {
        goto label_11;
    }
    do {
        *((r14 + rdx)) = 0;
        edx = rax + r15;
        rax = rdx;
    } while (rdx < rbp);
    rbx++;
    if (rbx >= rbp) {
        goto label_4;
    }
    do {
        if (*((r14 + rbx)) != 0) {
            goto label_12;
        }
label_11:
        rbx++;
    } while (rbx < rbp);
label_4:
    eax = puts ("/* Generated file -- DO NOT EDIT */\n");
    edx = 0x80;
    rsi = "#define WIDE_UINT_BITS %u\n";
    eax = 0;
    edi = 1;
    printf_chk ();
    eax = *((rsp + 0x14));
    if (eax == 0) {
        goto label_13;
    }
    rbx = *((rsp + 0x18));
    r13d = 2;
    eax += 8;
    *(rsp) = r14;
    r12d = 8;
    r14d = r13d;
    rbp = "P (%u, %u,\n   (";
    r15 = rbx + 0x10;
    rbx = "),\n   UINTMAX_MAX / %u)\n";
    r13 = r15;
    r15d = eax;
    goto label_14;
label_0:
    eax = r12d;
    rdi = *((rsp + 0x18));
    rax *= 3;
    rax <<= 4;
    ecx = *((rdi + rax));
    ecx -= r14d;
    if (ecx > 0xff) {
        void (*0x1220)() ();
    }
label_1:
    r11d = r14d;
    rsi = rbp;
    edi = 1;
    eax = 0;
    r11d -= edx;
    r12d++;
    r13 += 0x30;
    edx = r11d;
    printf_chk ();
    eax = print_wide_uint (*((r13 - 0x30)), *((r13 - 0x28)), 0, 0x80);
    edx = r14d;
    rsi = rbx;
    edi = 1;
    eax = 0;
    eax = printf_chk ();
    if (r15d == r12d) {
        goto label_15;
    }
label_14:
    edx = r14d;
    r14d = *((r13 - 0x10));
    if (*((rsp + 0x14)) > r12d) {
        goto label_0;
    }
    ecx = 0xff;
    goto label_1;
label_7:
    eax = 0;
    goto label_2;
label_10:
    ebx = rax - 2;
    ebx >>= 1;
    rbp = (int64_t) ebx;
    rax = xalloc (rbp);
    rdi = rax;
    r14 = rax;
    memset (rdi, 1, rbp);
    rdi = rbp * 0x30;
    rax = xalloc (rdi);
    *((rsp + 0x18)) = rax;
    if (ebx != 0) {
        goto label_3;
    }
    *((rsp + 0x14)) = 0;
    goto label_4;
label_12:
    eax = *((rsp + 0x14));
    goto label_5;
label_15:
    r13d = r14d;
    r14 = *(rsp);
label_9:
    puts ("\n#undef FIRST_OMITTED_PRIME");
    r8 = *((rsp + 0x18));
    r9d = *(r8);
    r9d *= r9d;
    do {
        r13d += 2;
        if (r13d < r9d) {
            goto label_16;
        }
        edi = r13d;
        rcx = r8;
        esi = 0;
label_6:
        r10 = *((rcx + 0x18));
        rax = rdi;
        rdx:rax = rax * *((rcx + 0x10));
        r10 *= rdi;
        rdx += r10;
        r10 = *((rcx + 0x28));
        r10 -= rdx;
    } while (r10 >= 0);
    eax = rsi + 1;
    rcx = rax * 3;
    rsi = rax;
    rcx <<= 4;
    rcx += r8;
    eax = *(rcx);
    eax *= eax;
    if (r13d >= eax) {
        goto label_6;
    }
label_16:
    edx = r13d;
    rsi = "#define FIRST_OMITTED_PRIME %u\n";
    edi = 1;
    eax = 0;
    printf_chk ();
    free (r14);
    free (*((rsp + 0x18)));
    eax = ferror (*(obj.stdout));
    ebx = eax;
    eax = fclose (*(obj.stdout));
    ebx += eax;
    if (ebx == 0) {
        goto label_7;
    }
    rax = errno_location ();
    rax = strerror (*(rax));
    rdx = "write error: %s\n";
    rcx = rax;
    goto label_8;
label_13:
    r13d = 2;
    goto label_9;
}