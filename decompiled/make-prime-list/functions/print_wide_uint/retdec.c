void print_wide_uint(int128_t n, int32_t nesting, int32_t wide_uint_bits) {
    // 0x1750
    int64_t v1; // 0x1750
    uint64_t v2 = v1;
    uint64_t v3 = (int64_t)n;
    if ((v2 || v3 & -0x10000000) == 0) {
        if (nesting != 0) {
            // 0x184d
            function_11d0();
        }
        // 0x17c7
        function_11d0();
        return;
    }
    int32_t v4 = nesting + 1;
    int64_t v5 = 0x1000000000 * v2 | v3 / 0x10000000; // 0x1788
    if ((256 * v2 || v3 / 0x100000000000000 || v2 / 0x100000000000000) != 0) {
        // 0x17f0
        function_1140();
        print_wide_uint((int128_t)v5, v4, wide_uint_bits);
        function_11d0();
    } else {
        // 0x179f
        print_wide_uint((int128_t)v5, v4, wide_uint_bits);
    }
    // 0x17aa
    function_11d0();
    // 0x17c7
    function_11d0();
}