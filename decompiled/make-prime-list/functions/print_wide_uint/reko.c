void print_wide_uint(word32 ecx, word32 edx, uint64 rsi, Eq_188 rdi)
{
	word64 rdx;
	word32 edx = (word32) rdx;
	if ((rsi | rdi & ~0x0FFFFFFF) != 0x00)
	{
		Eq_188 r14_71 = __shrd<word64>(rdi, rsi, 0x1C);
		if ((__shrd<word64>(rdi, rsi, 0x38) | rsi >> 0x38) == 0x00)
			print_wide_uint(ecx, edx, rsi >> 0x1C, r14_71);
		else
		{
			fn0000000000001140(0x28);
			print_wide_uint(ecx, edx + 0x01, rsi >> 0x1C, r14_71);
			fn00000000000011D0(")\n%*s", 0x01);
		}
		fn00000000000011D0(" << %d | ", 0x01);
	}
	else if (edx != 0x00)
		fn00000000000011D0("(uintmax_t) ", 0x01);
	fn00000000000011D0("0x%0*xU", 0x01);
}