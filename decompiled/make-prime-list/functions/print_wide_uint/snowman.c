void print_wide_uint(uint64_t rdi, uint64_t rsi, int64_t rdx, struct s0* rcx) {
    int64_t rbx5;
    int64_t rdx6;
    uint64_t r15_7;
    struct s0* rcx8;
    int64_t rdx9;
    void* rdx10;

    *reinterpret_cast<int32_t*>(&rbx5) = *reinterpret_cast<int32_t*>(&rdx);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx5) + 4) = 0;
    if (!(rsi | rdi & 0xfffffffff0000000)) {
        if (*reinterpret_cast<int32_t*>(&rbx5)) {
            fun_11d0(1, "(uintmax_t) ", 7, rcx);
        }
    } else {
        *reinterpret_cast<int32_t*>(&rdx6) = static_cast<int32_t>(rdx + 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx6) + 4) = 0;
        __asm__("shrd r14, r9, 0x1c");
        __asm__("shrd r8, r9, 0x38");
        r15_7 = rsi >> 28;
        if (rdi | rsi >> 56) {
            fun_1140(40);
            *reinterpret_cast<int32_t*>(&rcx8) = *reinterpret_cast<int32_t*>(&rcx);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx8) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx9) = *reinterpret_cast<int32_t*>(&rdx6);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx9) + 4) = 0;
            print_wide_uint(rdi, r15_7, rdx9, rcx8);
            *reinterpret_cast<int32_t*>(&rdx10) = static_cast<int32_t>(rbx5 + 3);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx10) + 4) = 0;
            rcx = reinterpret_cast<struct s0*>(0x2087);
            fun_11d0(1, ")\n%*s", rdx10, 0x2087);
        } else {
            print_wide_uint(rdi, r15_7, rdx6, rcx);
        }
        fun_11d0(1, " << %d | ", 28, rcx);
    }
}