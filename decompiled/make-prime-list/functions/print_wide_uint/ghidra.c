void print_wide_uint(ulong param_1,ulong param_2,int param_3,int param_4)

{
  int iVar1;
  ulong uVar2;
  
  if ((param_2 | param_1 & 0xfffffffff0000000) == 0) {
    iVar1 = 7;
    if (param_3 != 0) {
      __printf_chk(1,"(uintmax_t) ",7);
      iVar1 = (param_4 - 1U & 3) + 1;
    }
  }
  else {
    uVar2 = param_1 >> 0x1c | param_2 << 0x24;
    if ((param_1 >> 0x38 | param_2 << 8 | param_2 >> 0x38) == 0) {
      print_wide_uint(uVar2,param_2 >> 0x1c);
    }
    else {
      putchar(0x28);
      print_wide_uint(uVar2,param_2 >> 0x1c,param_3 + 1,param_4);
      __printf_chk(1,")\n%*s",param_3 + 3,"");
    }
    __printf_chk(1," << %d | ",0x1c);
    iVar1 = 7;
  }
  __printf_chk(1,"0x%0*xU",iVar1,(uint)param_1 & 0xfffffff);
  return;
}