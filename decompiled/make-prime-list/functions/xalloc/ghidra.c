void xalloc(size_t param_1)

{
  void *pvVar1;
  
  pvVar1 = malloc(param_1);
  if (pvVar1 != (void *)0x0) {
    return;
  }
  fwrite("Virtual memory exhausted.\n",1,0x1a,stderr);
                    /* WARNING: Subroutine does not return */
  exit(1);
}