struct s0* xalloc(void* rdi, ...) {
    struct s0* rax2;
    int64_t rcx3;

    rax2 = fun_11c0();
    if (!rax2) {
        rcx3 = stderr;
        fun_11f0("Virtual memory exhausted.\n", 1, 26, rcx3);
        fun_11e0(1, 1, 26, rcx3);
    } else {
        return rax2;
    }
}