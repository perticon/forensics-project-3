//
// This file was generated by the Retargetable Decompiler
// Website: https://retdec.com
//

#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// ---------------- Integer Types Definitions -----------------

typedef int64_t int128_t;

// ------------------------ Structures ------------------------

struct _IO_FILE {
    int32_t e0;
};

// ------------------- Function Prototypes --------------------

int64_t __do_global_dtors_aux(void);
int64_t __udivti3(void);
int64_t _fini(void);
int64_t _init(void);
int64_t _start(void);
int64_t deregister_tm_clones(void);
int64_t frame_dummy(void);
int64_t function_1003(void);
int64_t function_1120(void);
void function_1123(int64_t * d);
int64_t function_1130(void);
void function_1133(int64_t * ptr);
int64_t function_1140(void);
int32_t function_1143(int32_t c);
int64_t function_1150(void);
void function_1153(void);
int64_t function_1160(void);
int32_t * function_1163(void);
int64_t function_1170(void);
int32_t function_1173(char * s);
int64_t function_1180(void);
int32_t function_1183(struct _IO_FILE * stream);
int64_t function_1190(void);
int32_t function_1193(struct _IO_FILE * stream);
int64_t function_11a0(void);
int64_t * function_11a3(int64_t * s, int32_t c, int32_t n);
int64_t function_11b0(void);
int32_t function_11b3(char * nptr, char ** endptr, int32_t base);
int64_t function_11c0(void);
int64_t * function_11c3(int32_t size);
int64_t function_11d0(void);
int32_t function_11d3(int32_t flag, char * format, ...);
int64_t function_11e0(void);
void function_11e3(int32_t status);
int64_t function_11f0(void);
int32_t function_11f3(int64_t * ptr, int32_t size, int32_t n, struct _IO_FILE * s);
int64_t function_1200(void);
int32_t function_1203(struct _IO_FILE * stream, int32_t flag, char * format, ...);
int64_t function_1210(void);
char * function_1213(int32_t errnum);
int64_t function_1233(int64_t a1, int64_t a2, int64_t a3, int64_t a4, int64_t a5, int64_t a6);
int64_t function_1703(void);
int64_t function_1743(void);
int64_t function_18c3(uint64_t a1, uint64_t a2, uint64_t a3, uint64_t a4, int64_t a5);
int64_t function_19bb(void);
int64_t main_cold(void);
void print_wide_uint(int128_t n, int32_t nesting, int32_t wide_uint_bits);
int64_t register_tm_clones(void);
char * xalloc(int64_t s);

// --------------------- Global Variables ---------------------

char g1 = 0; // 0x4048
int32_t g2;

// ------------------------ Functions -------------------------

// Address range: 0x1000 - 0x1001
int64_t _init(void) {
    // 0x1000
    int64_t result; // 0x1000
    return result;
}

// Address range: 0x1003 - 0x101b
int64_t function_1003(void) {
    int64_t result = 0; // 0x1012
    if (*(int64_t *)0x3fe8 != 0) {
        // 0x1014
        __gmon_start__();
        result = &g2;
    }
    // 0x1016
    return result;
}

// Address range: 0x1120 - 0x1121
int64_t function_1120(void) {
    // 0x1120
    int64_t result; // 0x1120
    return result;
}

// Address range: 0x1123 - 0x112b
void function_1123(int64_t * d) {
    // 0x1123
    __cxa_finalize(d);
}

// Address range: 0x1130 - 0x1131
int64_t function_1130(void) {
    // 0x1130
    int64_t result; // 0x1130
    return result;
}

// Address range: 0x1133 - 0x113b
void function_1133(int64_t * ptr) {
    // 0x1133
    free(ptr);
}

// Address range: 0x1140 - 0x1141
int64_t function_1140(void) {
    // 0x1140
    int64_t result; // 0x1140
    return result;
}

// Address range: 0x1143 - 0x114b
int32_t function_1143(int32_t c) {
    // 0x1143
    return putchar(c);
}

// Address range: 0x1150 - 0x1151
int64_t function_1150(void) {
    // 0x1150
    int64_t result; // 0x1150
    return result;
}

// Address range: 0x1153 - 0x115b
void function_1153(void) {
    // 0x1153
    abort();
}

// Address range: 0x1160 - 0x1161
int64_t function_1160(void) {
    // 0x1160
    int64_t result; // 0x1160
    return result;
}

// Address range: 0x1163 - 0x116b
int32_t * function_1163(void) {
    // 0x1163
    return __errno_location();
}

// Address range: 0x1170 - 0x1171
int64_t function_1170(void) {
    // 0x1170
    int64_t result; // 0x1170
    return result;
}

// Address range: 0x1173 - 0x117b
int32_t function_1173(char * s) {
    // 0x1173
    return puts(s);
}

// Address range: 0x1180 - 0x1181
int64_t function_1180(void) {
    // 0x1180
    int64_t result; // 0x1180
    return result;
}

// Address range: 0x1183 - 0x118b
int32_t function_1183(struct _IO_FILE * stream) {
    // 0x1183
    return ferror(stream);
}

// Address range: 0x1190 - 0x1191
int64_t function_1190(void) {
    // 0x1190
    int64_t result; // 0x1190
    return result;
}

// Address range: 0x1193 - 0x119b
int32_t function_1193(struct _IO_FILE * stream) {
    // 0x1193
    return fclose(stream);
}

// Address range: 0x11a0 - 0x11a1
int64_t function_11a0(void) {
    // 0x11a0
    int64_t result; // 0x11a0
    return result;
}

// Address range: 0x11a3 - 0x11ab
int64_t * function_11a3(int64_t * s, int32_t c, int32_t n) {
    // 0x11a3
    return memset(s, c, n);
}

// Address range: 0x11b0 - 0x11b1
int64_t function_11b0(void) {
    // 0x11b0
    int64_t result; // 0x11b0
    return result;
}

// Address range: 0x11b3 - 0x11bb
int32_t function_11b3(char * nptr, char ** endptr, int32_t base) {
    // 0x11b3
    return strtol(nptr, endptr, base);
}

// Address range: 0x11c0 - 0x11c1
int64_t function_11c0(void) {
    // 0x11c0
    int64_t result; // 0x11c0
    return result;
}

// Address range: 0x11c3 - 0x11cb
int64_t * function_11c3(int32_t size) {
    // 0x11c3
    return malloc(size);
}

// Address range: 0x11d0 - 0x11d1
int64_t function_11d0(void) {
    // 0x11d0
    int64_t result; // 0x11d0
    return result;
}

// Address range: 0x11d3 - 0x11db
int32_t function_11d3(int32_t flag, char * format, ...) {
    // 0x11d3
    return __printf_chk(flag, format);
}

// Address range: 0x11e0 - 0x11e1
int64_t function_11e0(void) {
    // 0x11e0
    int64_t result; // 0x11e0
    return result;
}

// Address range: 0x11e3 - 0x11eb
void function_11e3(int32_t status) {
    // 0x11e3
    exit(status);
}

// Address range: 0x11f0 - 0x11f1
int64_t function_11f0(void) {
    // 0x11f0
    int64_t result; // 0x11f0
    return result;
}

// Address range: 0x11f3 - 0x11fb
int32_t function_11f3(int64_t * ptr, int32_t size, int32_t n, struct _IO_FILE * s) {
    // 0x11f3
    return fwrite(ptr, size, n, s);
}

// Address range: 0x1200 - 0x1201
int64_t function_1200(void) {
    // 0x1200
    int64_t result; // 0x1200
    return result;
}

// Address range: 0x1203 - 0x120b
int32_t function_1203(struct _IO_FILE * stream, int32_t flag, char * format, ...) {
    // 0x1203
    return __fprintf_chk(stream, flag, format);
}

// Address range: 0x1210 - 0x1211
int64_t function_1210(void) {
    // 0x1210
    int64_t result; // 0x1210
    return result;
}

// Address range: 0x1213 - 0x121b
char * function_1213(int32_t errnum) {
    // 0x1213
    return strerror(errnum);
}

// Address range: 0x1220 - 0x1225
int64_t main_cold(void) {
    // 0x1220
    return function_1150();
}

// Address range: 0x1230 - 0x1231
int main(int argc, char ** argv) {
    // 0x1230
    int64_t result; // 0x1230
    return result;
}

// Address range: 0x1233 - 0x1652
int64_t function_1233(int64_t a1, int64_t a2, int64_t a3, int64_t a4, int64_t a5, int64_t a6) {
    if ((int32_t)a1 != 2) {
        // 0x1251
        function_1200();
        // 0x1269
        return 1;
    }
    uint64_t v1 = function_11b0(); // 0x128c
    int32_t v2 = v1; // 0x129d
    if (v2 < 3) {
        // 0x1269
        return 0;
    }
    int64_t v3; // 0x1233
    char * v4; // 0x1233
    int64_t v5; // 0x1233
    int32_t * v6; // 0x1233
    int32_t * v7; // 0x1233
    int32_t v8; // 0x1233
    if (v1 % 2 == 0) {
        int32_t v9 = v2 - 2 >> 1; // 0x151c
        int64_t v10 = v9; // 0x151e
        char * v11 = xalloc(v10); // 0x1524
        function_11a0();
        char * v12 = xalloc(48 * v10); // 0x1540
        v6 = (int32_t *)v12;
        v5 = v10;
        v4 = v11;
        if (v9 != 0) {
            goto lab_0x12e5;
        } else {
            // 0x1519
            v3 = (int64_t)v12;
            v7 = (int32_t *)v12;
            v8 = 0;
            goto lab_0x1424_2;
        }
    } else {
        int64_t v13 = v2 - 1 >> 1; // 0x12b9
        char * v14 = xalloc(v13); // 0x12bf
        function_11a0();
        v6 = (int32_t *)xalloc(48 * v13);
        v5 = v13;
        v4 = v14;
        goto lab_0x12e5;
    }
  lab_0x141b:;
    // 0x141b
    int64_t v15; // 0x1233
    int64_t v16 = v15 + 1; // 0x141b
    int64_t v17 = v16; // 0x1422
    int64_t v18; // 0x1233
    v3 = v18;
    v7 = v6;
    int32_t v19; // 0x12f0
    v8 = v19;
    if (v16 >= v5) {
        // break -> 0x1424
        goto lab_0x1424_2;
    }
    goto lab_0x1410;
  lab_0x1410:;
    int64_t v20 = v17;
    v15 = v20;
    int64_t v21; // 0x1233
    int64_t v22; // 0x1233
    int64_t v23; // 0x1233
    if (*(char *)(v20 + v21) != 0) {
        // 0x1560
        v22 = v19;
        v23 = v20;
        goto lab_0x12f0;
    } else {
        goto lab_0x141b;
    }
  lab_0x12e5:
    // 0x12e5
    v21 = (int64_t)v4;
    v18 = (int64_t)v6;
    v22 = 0;
    v23 = 0;
    while (true) {
      lab_0x12f0:;
        int64_t v24 = v23;
        int64_t v25 = 2 * v24 + 3; // 0x12fd
        int64_t v26 = v25 & 0xffffffff; // 0x12fd
        int64_t v27 = 48 * v22 + v18; // 0x1306
        int32_t v28 = v25; // 0x1316
        *(int32_t *)v27 = v28;
        int64_t v29 = 0xf5397db1 >> (2 * v28 & 28);
        int64_t v30 = 2 * v29 - v29 * v29 * v26; // 0x139a
        // 0x139c
        v19 = (int32_t)v22 + 1;
        *(int64_t *)(v27 + 24) = 0;
        *(int64_t *)(v27 + 16) = v29;
        *(int64_t *)(v27 + 32) = __udivti3();
        int64_t v31 = 0x100000000 * v25 >> 32; // 0x13d6
        *(int64_t *)(v27 + 40) = v26;
        uint64_t v32 = (v31 * v31 + 0xfffffffd) / 2 % 0x80000000; // 0x13e1
        int64_t v33 = v32; // 0x13e8
        v15 = v24;
        if (v5 > v32) {
            *(char *)(v33 + v21) = 0;
            int64_t v34 = v33 + v25 & 0xffffffff; // 0x13f5
            v33 = v34;
            while (v34 < v5) {
                // 0x13f0
                *(char *)(v33 + v21) = 0;
                v34 = v33 + v25 & 0xffffffff;
                v33 = v34;
            }
            int64_t v35 = v24 + 1; // 0x1401
            v17 = v35;
            v3 = v18;
            v7 = v6;
            v8 = v19;
            if (v35 >= v5) {
                // break -> 0x1424
                break;
            }
            goto lab_0x1410;
        } else {
            goto lab_0x141b;
        }
    }
    goto lab_0x1424_2;
  lab_0x1424_2:
    // 0x1424
    function_1170();
    function_11d0();
    int64_t v36 = 4; // 0x144e
    if (v8 != 0) {
        int64_t v37 = v3 + 16; // 0x1487
        int64_t v38 = 8;
        uint32_t v39 = *(int32_t *)(v37 - 16); // 0x1500
        if (v8 > (int32_t)v38) {
            // 0x1490
            if (*(int32_t *)(48 * v38 + v3) - v39 >= 256) {
                main_cold();
            }
        }
        int64_t v40 = v38 + 1; // 0x14c2
        function_11d0();
        print_wide_uint((int128_t)*(int64_t *)v37, 0, 128);
        function_11d0();
        v37 += 48;
        while (v8 + 8 != (int32_t)v40) {
            // 0x14fd
            v38 = v40 & 0xffffffff;
            v39 = *(int32_t *)(v37 - 16);
            if (v8 > (int32_t)v38) {
                // 0x1490
                if (*(int32_t *)(48 * v38 + v3) - v39 >= 256) {
                    main_cold();
                }
            }
            // 0x14b2
            v40 = v38 + 1;
            function_11d0();
            print_wide_uint((int128_t)*(int64_t *)v37, 0, 128);
            function_11d0();
            v37 += 48;
        }
        // 0x1570
        v36 = (int64_t)v39 + 2;
    }
    // 0x1570
    function_1170();
    int32_t v41 = *v7; // 0x1581
    uint32_t v42 = v41 * v41; // 0x1584
    int32_t v43 = v36; // 0x1594
    if (v42 <= v43) {
        int64_t v44 = v36 & 0xffffffff;
        int64_t v45 = v3; // 0x15d5
        int64_t v46 = 0; // 0x15c7
        int32_t v47; // 0x15d8
        while (*(int64_t *)(v45 + 32) < *(int64_t *)(v45 + 16) * v44) {
            // 0x15c7
            v46 = v46 + 1 & 0xffffffff;
            v45 = 48 * v46 + v3;
            v47 = *(int32_t *)v45;
            if (v47 * v47 > v43) {
                // break (via goto) -> 0x15e2
                goto lab_0x15e2;
            }
        }
        int64_t v48 = v44 + 2; // 0x1590
        int32_t v49 = v48; // 0x1594
        while (v42 <= v49) {
            uint32_t v50 = v49;
            v44 = v48 & 0xffffffff;
            v45 = v3;
            v46 = 0;
            while (*(int64_t *)(v45 + 32) < *(int64_t *)(v45 + 16) * v44) {
                // 0x15c7
                v46 = v46 + 1 & 0xffffffff;
                v45 = 48 * v46 + v3;
                v47 = *(int32_t *)v45;
                if (v47 * v47 > v50) {
                    // break (via goto) -> 0x15e2
                    goto lab_0x15e2;
                }
            }
            // 0x1590
            v48 = v44 + 2;
            v49 = v48;
        }
    }
  lab_0x15e2:
    // 0x15e2
    function_11d0();
    function_1130();
    function_1130();
    int64_t v51 = function_1180(); // 0x1611
    if ((int32_t)v51 == -(int32_t)function_1190()) {
        // 0x1269
        return 0;
    }
    // 0x162c
    function_1160();
    function_1210();
    // 0x1251
    function_1200();
    // 0x1269
    return 1;
}

// Address range: 0x1660 - 0x1661
int64_t _start(void) {
    // 0x1660
    int64_t result; // 0x1660
    return result;
}

// Address range: 0x1690 - 0x16b9
int64_t deregister_tm_clones(void) {
    // 0x1690
    return 0x4010;
}

// Address range: 0x16c0 - 0x16f9
int64_t register_tm_clones(void) {
    // 0x16c0
    return 0;
}

// Address range: 0x1700 - 0x1701
int64_t __do_global_dtors_aux(void) {
    // 0x1700
    int64_t result; // 0x1700
    return result;
}

// Address range: 0x1703 - 0x1739
int64_t function_1703(void) {
    // 0x1703
    if (g1 != 0) {
        // 0x1738
        int64_t result; // 0x1703
        return result;
    }
    // 0x170d
    if (*(int64_t *)0x3ff8 != 0) {
        // 0x171b
        function_1120();
    }
    int64_t result2 = deregister_tm_clones(); // 0x1727
    g1 = 1;
    return result2;
}

// Address range: 0x1740 - 0x1741
int64_t frame_dummy(void) {
    // 0x1740
    int64_t result; // 0x1740
    return result;
}

// Address range: 0x1743 - 0x1749
int64_t function_1743(void) {
    // 0x1743
    return register_tm_clones();
}

// From module:   /home/davide/Desktop/coreutils/src/make-prime-list.c
// Address range: 0x1750 - 0x1876
// Line range:    83 - 107
void print_wide_uint(int128_t n, int32_t nesting, int32_t wide_uint_bits) {
    // 0x1750
    int64_t v1; // 0x1750
    uint64_t v2 = v1;
    uint64_t v3 = (int64_t)n;
    if ((v2 || v3 & -0x10000000) == 0) {
        if (nesting != 0) {
            // 0x184d
            function_11d0();
        }
        // 0x17c7
        function_11d0();
        return;
    }
    int32_t v4 = nesting + 1;
    int64_t v5 = 0x1000000000 * v2 | v3 / 0x10000000; // 0x1788
    if ((256 * v2 || v3 / 0x100000000000000 || v2 / 0x100000000000000) != 0) {
        // 0x17f0
        function_1140();
        print_wide_uint((int128_t)v5, v4, wide_uint_bits);
        function_11d0();
    } else {
        // 0x179f
        print_wide_uint((int128_t)v5, v4, wide_uint_bits);
    }
    // 0x17aa
    function_11d0();
    // 0x17c7
    function_11d0();
}

// From module:   /home/davide/Desktop/coreutils/src/make-prime-list.c
// Address range: 0x1880 - 0x18c0
// Line range:    166 - 173
char * xalloc(int64_t s) {
    int64_t v1 = function_11c0(); // 0x1884
    if (v1 != 0) {
        // 0x188e
        return (char *)v1;
    }
    // 0x1893
    function_11f0();
    return (char *)function_11e0();
}

// Address range: 0x18c0 - 0x18c1
int64_t __udivti3(void) {
    // 0x18c0
    int64_t result; // 0x18c0
    return result;
}

// Address range: 0x18c3 - 0x19b8
int64_t function_18c3(uint64_t a1, uint64_t a2, uint64_t a3, uint64_t a4, int64_t a5) {
    if (a4 == 0) {
        if (a3 <= a2) {
            // 0x1918
            return a1 / (a3 != 0 ? a3 : (int64_t)(bool)(a3 == 1));
        }
        // 0x18df
        return a1 / a3;
    }
    // 0x18e0
    if (a4 > a2) {
        // 0x18df
        return 0;
    }
    uint64_t v1 = llvm_ctlz_i64(a4, true); // 0x18f0
    if (v1 == 0) {
        // 0x18fd
        if (a3 > a1 == a4 >= a2) {
            // 0x18df
            return 0;
        }
        // 0x18df
        return 1;
    }
    uint64_t v2 = -v1 % 64; // 0x1962
    int64_t result = (a1 >> v2 | a2 << v1) / (a3 >> v2 | a4 << v1);
    if (false || a1 << v1 < result * (a3 << v1)) {
        // 0x19b0
        return result - 1;
    }
    // 0x18df
    return result;
}

// Address range: 0x19b8 - 0x19b9
int64_t _fini(void) {
    // 0x19b8
    int64_t result; // 0x19b8
    return result;
}

// Address range: 0x19bb - 0x19c5
int64_t function_19bb(void) {
    // 0x19bb
    int64_t result; // 0x19bb
    return result;
}

// --------------- Dynamically Linked Functions ---------------

// void __cxa_finalize(void * d);
// int * __errno_location(void);
// int __fprintf_chk(FILE * restrict stream, int flag, const char * restrict format, ...);
// void __gmon_start__(void);
// int __printf_chk(int flag, const char * restrict format, ...);
// void abort(void);
// void exit(int status);
// int fclose(FILE * stream);
// int ferror(FILE * stream);
// void free(void * ptr);
// size_t fwrite(const void * restrict ptr, size_t size, size_t n, FILE * restrict s);
// void * malloc(size_t size);
// void * memset(void * s, int c, size_t n);
// int putchar(int c);
// int puts(const char * s);
// char * strerror(int errnum);
// long int strtol(const char * restrict nptr, char ** restrict endptr, int base);

// --------------------- Meta-Information ---------------------

// Detected compiler/packer: gcc (11.2.0)
// Detected functions: 50

