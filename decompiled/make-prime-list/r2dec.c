#include <stdint.h>

/* /tmp/tmp3tncsxms @ 0x1660 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmp3tncsxms @ 0x1750 */
 
int64_t dbg_print_wide_uint (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, wide_uint n) {
    int64_t var_8h;
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r9 = n;
    do {
        /* void print_wide_uint(wide_uint n,int nesting,unsigned int wide_uint_bits); */
label_0:
        rax = rdi;
        r9 = rsi;
        rax &= 0xfffffffff0000000;
        r12d = edi;
        r12d &= 0xfffffff;
        ebx = edx;
        rsi |= rax;
        if (rsi == 0) {
            goto label_2;
        }
        r8 = rdi;
        r14 = rdi;
        edx = rdx + 1;
        r15 = r9;
        __asm ("shrd r14, r9, 0x1c");
        __asm ("shrd r8, r9, 0x38");
        r9 >>= 0x38;
        r15 >>= 0x1c;
        r8 |= r9;
        if (r8 != 0) {
            goto label_3;
        }
        eax = print_wide_uint (r14, r15, rdx, rcx);
    } while (1);
    do {
        edx = 0x1c;
        rsi = " << %d | ";
        edi = 1;
        eax = 0;
        eax = printf_chk ();
        edx = 7;
label_1:
        ecx = r12d;
        rsi = "0x%0*xU";
        eax = 0;
        edi = 1;
        void (*0x11d0)() ();
label_3:
        *((rsp + 0xc)) = ecx;
        *((rsp + 8)) = edx;
        putchar (0x28);
        eax = print_wide_uint (r14, r15, *((rsp + 8)), *((rsp + 0xc)));
        goto label_0;
        edx = rbx + 3;
        edi = 1;
        eax = 0;
        rcx = 0x00002087;
        rsi = ")\n%*s";
        eax = printf_chk ();
    } while (1);
label_2:
    edx = 7;
    if (ebx == 0) {
        goto label_1;
    }
    rsi = "(uintmax_t) ";
    edi = 1;
    eax = 0;
    *((rsp + 8)) = ecx;
    printf_chk ();
    ecx = *((rsp + 8));
    edx = rcx - 1;
    edx &= 3;
    edx++;
    goto label_1;
}

/* /tmp/tmp3tncsxms @ 0x11d0 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmp3tncsxms @ 0x1140 */
 
void putchar (void) {
    __asm ("bnd jmp qword [reloc.putchar]");
}

/* /tmp/tmp3tncsxms @ 0x1880 */
 
uint64_t dbg_xalloc (size_t size) {
    rdi = size;
    /* void * xalloc(size_t s); */
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    fwrite ("Virtual memory exhausted.\n", 1, 0x1a, *(obj.stderr));
    return exit (1);
}

/* /tmp/tmp3tncsxms @ 0x11c0 */
 
void malloc (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmp3tncsxms @ 0x11f0 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmp3tncsxms @ 0x11e0 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmp3tncsxms @ 0x1220 */
 
void main_cold (void) {
    /* [16] -r-x section size 1944 named .text */
    return abort ();
}

/* /tmp/tmp3tncsxms @ 0x1150 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmp3tncsxms @ 0x1690 */
 
uint64_t deregister_tm_clones (void) {
    rdi = loc__edata;
    rax = loc__edata;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmp3tncsxms @ 0x16c0 */
 
int64_t register_tm_clones (void) {
    rdi = loc__edata;
    rsi = loc__edata;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmp3tncsxms @ 0x1700 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_00001120 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmp3tncsxms @ 0x1120 */
 
void fcn_00001120 (void) {
    /* [14] -r-x section size 16 named .plt.got */
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmp3tncsxms @ 0x1740 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmp3tncsxms @ 0x18c0 */
 
int64_t udivti3 (uint32_t arg1, uint32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = rdx;
    rax = rcx;
    if (rcx != 0) {
        goto label_0;
    }
    if (rdx <= rsi) {
        goto label_1;
    }
    rax = rdi;
    rdx = rsi;
    rax = rdx:rax / r9;
    rdx = rdx:rax % r9;
    edx = 0;
    do {
        return rax;
label_0:
        if (rcx > rsi) {
            edx = 0;
            eax = 0;
            return rax;
        }
        __asm ("bsr r8, rcx");
        r8 ^= 0x3f;
        if (r8d != 0) {
            goto label_2;
        }
        if (rcx < rsi) {
            goto label_3;
        }
        edx = 0;
        eax = 0;
    } while (r9 > rdi);
label_3:
    edx = 0;
    eax = 1;
    return rax;
label_1:
    rcx = rdx;
    if (rdx == 0) {
        eax = 1;
        edx = 0;
        rax = rdx:rax / r9;
        rdx = rdx:rax % r9;
        rcx = rax;
    }
    edx = 0;
    rax = rsi;
    rax = rdx:rax / rcx;
    rdx = rdx:rax % rcx;
    rsi = rax;
    rax = rdi;
    rax = rdx:rax / rcx;
    rdx = rdx:rax % rcx;
    rdx = rsi;
    return rax;
label_2:
    ecx = r8d;
    edx = 0x40;
    rbx = r9;
    rax <<= cl;
    rcx = (int64_t) r8d;
    r11 = rsi;
    rdx -= rcx;
    ecx = edx;
    rbx >>= cl;
    ecx = r8d;
    r9 <<= cl;
    ecx = edx;
    rbx |= rax;
    rax = rdi;
    r11 >>= cl;
    ecx = r8d;
    rsi <<= cl;
    ecx = edx;
    rdx = r11;
    rax >>= cl;
    rax |= rsi;
    rax = rdx:rax / rbx;
    rdx = rdx:rax % rbx;
    r11 = rdx;
    rsi = rax;
    rdx:rax = rax * r9;
    if (r11 >= rdx) {
        ecx = r8d;
        rdi <<= cl;
        if (rdi < rax) {
            if (r11 == rdx) {
                goto label_4;
            }
        }
        rax = rsi;
        edx = 0;
        return rax;
    }
label_4:
    rax = rsi - 1;
    edx = 0;
    return rax;
}

/* /tmp/tmp3tncsxms @ 0x19b8 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmp3tncsxms @ 0x1230 */
 
int32_t main (int64_t arg5, int64_t arg6, uint32_t argc, char ** str) {
    int64_t var_8h;
    uint32_t var_14h;
    void * ptr;
    int64_t var_20h;
    int64_t var_28h;
    r8 = arg5;
    r9 = arg6;
    rdi = argc;
    rsi = str;
    if (edi != 2) {
        rcx = *(rsi);
        rdx = "Usage: %s LIMIT\nProduces a list of odd primes <= LIMIT\n";
label_8:
        rdi = stderr;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        eax = 1;
label_2:
        return eax;
    }
    *(rsp) = r8;
    *((rsp + 8)) = r9;
    eax = strtol (*((rsi + 8)), 0, 0xa);
    r8 = *(rsp);
    r9 = *((rsp + 8));
    if (eax <= 2) {
        goto label_7;
    }
    *(rsp) = r8;
    *((rsp + 8)) = r9;
    if ((al & 1) == 0) {
        goto label_10;
    }
    eax--;
    eax >>= 1;
    rbp = (int64_t) eax;
    rax = xalloc (rbp);
    rdi = rax;
    r14 = rax;
    memset (rdi, 1, rbp);
    rdi = rbp * 0x30;
    rax = xalloc (rdi);
    *((rsp + 0x18)) = rax;
label_3:
    eax = 0;
    ebx = 0;
label_5:
    edi = rax + 1;
    rax *= 3;
    r9d = 0;
    r13d = 0;
    r8d = rbx + rbx + 3;
    rax <<= 4;
    rax += *((rsp + 0x18));
    *((rsp + 0x14)) = edi;
    *(rsp) = rax;
    r15 = r8;
    *(rax) = r8d;
    rax = r8;
    __asm ("shrd rax, r9, 1");
    eax &= 7;
    ecx = rax*4;
    eax = 0xf5397db1;
    eax >>= cl;
    r12 = rax;
    do {
        r11 = r13;
        rax = r12;
        rcx = r13;
        rdi = r13;
        rdx:rax = rax * r12;
        rsi = r12;
        __asm ("shld rdi, r12, 1");
        r10 = r12;
        r11 *= r12;
        rsi += r12;
        r13 = rdx;
        r12 = rax;
        rdx = r9;
        r11 += r11;
        rdx *= r12;
        rax = r12;
        r13 += r11;
        r11 = r13;
        r11 *= r8;
        r11 += rdx;
        rdx:rax = rax * r8;
        rdx += r11;
        rsi -= rax;
        rax = rcx;
        rdi -= rdx;
        rdx = r10;
        r12 = rsi;
        rax ^= rdi;
        rdx ^= rsi;
        r13 = rdi;
        rax |= rdx;
    } while (rax != 0);
    rax = *(rsp);
    rdx = r8;
    rdi = 0xffffffffffffffff;
    rsi = 0xffffffffffffffff;
    *((rsp + 0x20)) = r8;
    *((rax + 0x18)) = rcx;
    rcx = r9;
    *((rax + 0x10)) = r10;
    *((rsp + 0x28)) = r9;
    rax = _udivti3 ();
    rdi = *(rsp);
    *((rdi + 0x20)) = rax;
    eax = r15d;
    eax *= r15d;
    *((rdi + 0x28)) = rdx;
    eax -= 3;
    eax >>= 1;
    edx = eax;
    if (rbp <= rdx) {
        goto label_11;
    }
    do {
        *((r14 + rdx)) = 0;
        edx = rax + r15;
        rax = rdx;
    } while (rdx < rbp);
    rbx++;
    if (rbx >= rbp) {
        goto label_4;
    }
    do {
        if (*((r14 + rbx)) != 0) {
            goto label_12;
        }
label_11:
        rbx++;
    } while (rbx < rbp);
label_4:
    eax = puts ("/* Generated file -- DO NOT EDIT */\n");
    edx = 0x80;
    rsi = "#define WIDE_UINT_BITS %u\n";
    eax = 0;
    edi = 1;
    printf_chk ();
    eax = *((rsp + 0x14));
    if (eax == 0) {
        goto label_13;
    }
    rbx = *((rsp + 0x18));
    r13d = 2;
    eax += 8;
    *(rsp) = r14;
    r12d = 8;
    r14d = r13d;
    rbp = "P (%u, %u,\n   (";
    r15 = rbx + 0x10;
    rbx = "),\n   UINTMAX_MAX / %u)\n";
    r13 = r15;
    r15d = eax;
    goto label_14;
label_0:
    eax = r12d;
    rdi = *((rsp + 0x18));
    rax *= 3;
    rax <<= 4;
    ecx = *((rdi + rax));
    ecx -= r14d;
    if (ecx > 0xff) {
        void (*0x1220)() ();
    }
label_1:
    r11d = r14d;
    rsi = rbp;
    edi = 1;
    eax = 0;
    r11d -= edx;
    r12d++;
    r13 += 0x30;
    edx = r11d;
    printf_chk ();
    eax = print_wide_uint (*((r13 - 0x30)), *((r13 - 0x28)), 0, 0x80);
    edx = r14d;
    rsi = rbx;
    edi = 1;
    eax = 0;
    eax = printf_chk ();
    if (r15d == r12d) {
        goto label_15;
    }
label_14:
    edx = r14d;
    r14d = *((r13 - 0x10));
    if (*((rsp + 0x14)) > r12d) {
        goto label_0;
    }
    ecx = 0xff;
    goto label_1;
label_7:
    eax = 0;
    goto label_2;
label_10:
    ebx = rax - 2;
    ebx >>= 1;
    rbp = (int64_t) ebx;
    rax = xalloc (rbp);
    rdi = rax;
    r14 = rax;
    memset (rdi, 1, rbp);
    rdi = rbp * 0x30;
    rax = xalloc (rdi);
    *((rsp + 0x18)) = rax;
    if (ebx != 0) {
        goto label_3;
    }
    *((rsp + 0x14)) = 0;
    goto label_4;
label_12:
    eax = *((rsp + 0x14));
    goto label_5;
label_15:
    r13d = r14d;
    r14 = *(rsp);
label_9:
    puts ("\n#undef FIRST_OMITTED_PRIME");
    r8 = *((rsp + 0x18));
    r9d = *(r8);
    r9d *= r9d;
    do {
        r13d += 2;
        if (r13d < r9d) {
            goto label_16;
        }
        edi = r13d;
        rcx = r8;
        esi = 0;
label_6:
        r10 = *((rcx + 0x18));
        rax = rdi;
        rdx:rax = rax * *((rcx + 0x10));
        r10 *= rdi;
        rdx += r10;
        r10 = *((rcx + 0x28));
        r10 -= rdx;
    } while (r10 >= 0);
    eax = rsi + 1;
    rcx = rax * 3;
    rsi = rax;
    rcx <<= 4;
    rcx += r8;
    eax = *(rcx);
    eax *= eax;
    if (r13d >= eax) {
        goto label_6;
    }
label_16:
    edx = r13d;
    rsi = "#define FIRST_OMITTED_PRIME %u\n";
    edi = 1;
    eax = 0;
    printf_chk ();
    free (r14);
    free (*((rsp + 0x18)));
    eax = ferror (*(obj.stdout));
    ebx = eax;
    eax = fclose (*(obj.stdout));
    ebx += eax;
    if (ebx == 0) {
        goto label_7;
    }
    rax = errno_location ();
    rax = strerror (*(rax));
    rdx = "write error: %s\n";
    rcx = rax;
    goto label_8;
label_13:
    r13d = 2;
    goto label_9;
}

/* /tmp/tmp3tncsxms @ 0x1200 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmp3tncsxms @ 0x11b0 */
 
void strtol (void) {
    __asm ("bnd jmp qword [reloc.strtol]");
}

/* /tmp/tmp3tncsxms @ 0x11a0 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmp3tncsxms @ 0x1170 */
 
void puts (void) {
    __asm ("bnd jmp qword [reloc.puts]");
}

/* /tmp/tmp3tncsxms @ 0x1130 */
 
void free (void) {
    /* [15] -r-x section size 240 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmp3tncsxms @ 0x1180 */
 
void ferror (void) {
    __asm ("bnd jmp qword [reloc.ferror]");
}

/* /tmp/tmp3tncsxms @ 0x1190 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmp3tncsxms @ 0x1160 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmp3tncsxms @ 0x1210 */
 
void strerror (void) {
    __asm ("bnd jmp qword [reloc.strerror]");
}

/* /tmp/tmp3tncsxms @ 0x1000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmp3tncsxms @ 0x0 */
 
int64_t libc_start_main (int32_t argc, func init, char ** ubp_av) {
    rsi = argc;
    rcx = init;
    rdx = ubp_av;
    /* [38] ---- section size 394 named .shstrtab */
    eax += *(rax);
    *(rax) += al;
    al += *(rcx);
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    tmp_0 = edi;
    edi = eax;
    eax = tmp_0;
    __asm ("sldt word [rax]");
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax |= 0x27004000;
    *(rsi) += ah;
    *(rsi) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) |= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax += *(rax);
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) |= eax;
    *(rax) += al;
    *(rax) += al;
    al += dh;
    *(rax) |= eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *(0x000000f1) += al;
    rdx = rbx;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) |= eax;
    *(rax) += al;
    *(rax) += al;
    ch += al;
    *(rax) |= eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rax) += ah;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += ah;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += ah;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) |= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) |= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
}

/* /tmp/tmp3tncsxms @ 0x1030 */
 
void fcn_00001030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 256 named .plt */
    __asm ("bnd jmp qword [0x00003f58]");
}

/* /tmp/tmp3tncsxms @ 0x1040 */
 
void fcn_00001040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp3tncsxms @ 0x1050 */
 
void fcn_00001050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp3tncsxms @ 0x1060 */
 
void fcn_00001060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp3tncsxms @ 0x1070 */
 
void fcn_00001070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp3tncsxms @ 0x1080 */
 
void fcn_00001080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp3tncsxms @ 0x1090 */
 
void fcn_00001090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp3tncsxms @ 0x10a0 */
 
void fcn_000010a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp3tncsxms @ 0x10b0 */
 
void fcn_000010b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp3tncsxms @ 0x10c0 */
 
void fcn_000010c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp3tncsxms @ 0x10d0 */
 
void fcn_000010d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp3tncsxms @ 0x10e0 */
 
void fcn_000010e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp3tncsxms @ 0x10f0 */
 
void fcn_000010f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp3tncsxms @ 0x1100 */
 
void fcn_00001100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp3tncsxms @ 0x1110 */
 
void fcn_00001110 (void) {
    return __asm ("bnd jmp section..plt");
}
