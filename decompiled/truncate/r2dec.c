#include <stdint.h>

/* /tmp/tmpaz2gywv9 @ 0x3070 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (dbg.main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmpaz2gywv9 @ 0x3700 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x000088cf;
        rdx = 0x000088c0;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x000088c7;
        rdx = 0x000088c9;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x000088cb;
    rdx = 0x000088c4;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmpaz2gywv9 @ 0x6fa0 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = "ASCII";
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = "ASCII";
    return rax;
}

/* /tmp/tmpaz2gywv9 @ 0x2580 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmpaz2gywv9 @ 0x37e0 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x2680)() ();
    }
    rdx = 0x00008940;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0x8940 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x000088d3;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x000088c9;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x0000896c;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0x896c */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x000088c7;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x000088c9;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x000088c7;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x000088c9;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x00008a6c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0x8a6c */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x00008b6c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0x8b6c */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x000088c9;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x000088c7;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x000088c7;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x000088c9;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmpaz2gywv9 @ 0x2680 */
 
void quotearg_buffer_restyled_cold (void) {
    /* [16] -r-x section size 19266 named .text */
    return abort ();
}

/* /tmp/tmpaz2gywv9 @ 0x4c00 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x2685)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                *((rsp + 0x10)) = rsi;
                free (r14);
                rsi = *((rsp + 0x10));
            }
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc (*((rsp + 0x10)));
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmpaz2gywv9 @ 0x2685 */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmpaz2gywv9 @ 0x268a */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmpaz2gywv9 @ 0x2380 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmpaz2gywv9 @ 0x2690 */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmpaz2gywv9 @ 0x2695 */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpaz2gywv9 @ 0x269a */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmpaz2gywv9 @ 0x269f */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpaz2gywv9 @ 0x26a4 */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmpaz2gywv9 @ 0x26a9 */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpaz2gywv9 @ 0x26ae */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpaz2gywv9 @ 0x26b3 */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpaz2gywv9 @ 0x26b8 */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpaz2gywv9 @ 0x30a0 */
 
uint64_t deregister_tm_clones (void) {
    rdi = obj___progname;
    rax = obj___progname;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpaz2gywv9 @ 0x30d0 */
 
int64_t register_tm_clones (void) {
    rdi = obj___progname;
    rsi = obj___progname;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpaz2gywv9 @ 0x3110 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_00002350 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmpaz2gywv9 @ 0x2350 */
 
void fcn_00002350 (void) {
    /* [14] -r-x section size 16 named .plt.got */
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmpaz2gywv9 @ 0x3150 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmpaz2gywv9 @ 0x71b0 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmpaz2gywv9 @ 0x5150 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpaz2gywv9 @ 0x5480 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000c210]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x0000c220]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaz2gywv9 @ 0x2440 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmpaz2gywv9 @ 0x5f10 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaz2gywv9 @ 0x4f90 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc (rdi);
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmpaz2gywv9 @ 0x2390 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmpaz2gywv9 @ 0x6110 */
 
uint64_t dbg_xcharalloc (size_t size) {
    rdi = size;
    /* char * xcharalloc(size_t n); */
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpaz2gywv9 @ 0x2560 */
 
void malloc (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmpaz2gywv9 @ 0x6650 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x00008878);
    return abort ();
}

/* /tmp/tmpaz2gywv9 @ 0x2410 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmpaz2gywv9 @ 0x25d0 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmpaz2gywv9 @ 0x25a0 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmpaz2gywv9 @ 0x23d0 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmpaz2gywv9 @ 0x4eb0 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmpaz2gywv9 @ 0x6fe0 */
 
int64_t dbg_rpl_mbrtowc (int64_t arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpaz2gywv9 @ 0x51a0 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x2690)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaz2gywv9 @ 0x4f10 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmpaz2gywv9 @ 0x3650 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite ("A NULL argv[0] was passed through an exec system call.\n", 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmpaz2gywv9 @ 0x2480 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmpaz2gywv9 @ 0x23a0 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmpaz2gywv9 @ 0x2620 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmpaz2gywv9 @ 0x56e0 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x0000c210]");
    __asm ("movdqa xmm2, xmmword [0x0000c220]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x26a9)() ();
    }
    if (rdx == 0) {
        void (*0x26a9)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaz2gywv9 @ 0x5780 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000c210]");
    __asm ("movdqa xmm2, xmmword [0x0000c220]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x26ae)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x26ae)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaz2gywv9 @ 0x52c0 */
 
int64_t quotearg_style (uint32_t arg1, int64_t arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x269a)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaz2gywv9 @ 0x67d0 */
 
int64_t dbg_xstrtoimax (int64_t arg1, int64_t arg2, uint32_t arg3, int64_t arg4, int64_t arg5) {
    char const[11] const __PRETTY_FUNCTION__;
    char * t_ptr;
    int64_t var_8h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_1fh;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* strtol_error xstrtoimax(char const * s,char ** ptr,int strtol_base,intmax_t * val,char const * valid_suffixes); */
    *(rsp) = rcx;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    if (edx > 0x24) {
        goto label_25;
    }
    rax = rsp + 0x20;
    r13 = rdi;
    if (rsi == 0) {
    }
    r12d = edx;
    r14 = r8;
    errno_location ();
    *(rax) = 0;
    r15 = rax;
    rax = strtoimax (r13, rbp, r12d);
    rdx = *(rbp);
    rbx = rax;
    if (rdx == r13) {
        goto label_26;
    }
    eax = *(r15);
    if (eax != 0) {
        goto label_27;
    }
    r12d = 0;
label_0:
    if (r14 != 0) {
        r13d = *(rdx);
        if (r13b != 0) {
            goto label_28;
        }
    }
label_3:
    rax = *(rsp);
    *(rax) = rbx;
    do {
label_1:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_29;
        }
        eax = r12d;
        return rax;
label_27:
        r12d = 4;
    } while (eax != 0x22);
    r12d = 1;
    goto label_0;
label_26:
    if (r14 == 0) {
        goto label_30;
    }
    r13d = *(rdx);
    *((rsp + 8)) = rdx;
    r12d = 4;
    if (r13b == 0) {
        goto label_1;
    }
    esi = (int32_t) r13b;
    r12d = 0;
    ebx = 1;
    rax = strchr (r14, rsi);
    rdx = *((rsp + 8));
    if (rax == 0) {
        goto label_30;
    }
    do {
        r8d = r13 - 0x45;
        r9d = 1;
        ecx = 0x400;
        if (r8b <= 0x2f) {
            rax = 0x814400308945;
            if (((rax >> r8) & 1) < 0) {
                goto label_31;
            }
        }
label_2:
        r13d -= 0x42;
        if (r13b > 0x35) {
            goto label_32;
        }
        r13d = (int32_t) r13b;
        rax = *((rsi + r13*4));
        rax += rsi;
        /* switch table (54 cases) at 0x9058 */
        void (*rax)() ();
label_28:
        esi = (int32_t) r13b;
        *((rsp + 8)) = rdx;
        rax = strchr (r14, 0x00009058);
        rdx = *((rsp + 8));
    } while (rax != 0);
label_32:
    rax = *(rsp);
    r12d |= 2;
    *(rax) = rbx;
    goto label_1;
label_31:
    *((rsp + 0x18)) = r9d;
    *((rsp + 0x14)) = ecx;
    *((rsp + 8)) = rdx;
    *((rsp + 0x1f)) = r8b;
    rax = strchr (r14, 0x30);
    rdx = *((rsp + 8));
    ecx = 0x400;
    r9d = 1;
    if (rax == 0) {
        goto label_2;
    }
    eax = *((rdx + 1));
    if (al == 0x44) {
        goto label_33;
    }
    if (al == 0x69) {
        goto label_34;
    }
    r8d = *((rsp + 0x1f));
    if (al == 0x42) {
        goto label_33;
    }
    rcx = 0x00009130;
    rax = *((rcx + r8*4));
    rax += rcx;
    /* switch table (48 cases) at 0x9130 */
    void (*rax)() ();
label_30:
    r12d = 4;
    goto label_1;
    r9 = (int64_t) r9d;
label_10:
    esi = 4;
    edi = 0;
    rax = (int64_t) ecx;
    r10 = 0x8000000000000000;
    r8 = 0x7fffffffffffffff;
    do {
        rcx = rbx;
        rcx *= rax;
        if (rax overflow 0) {
            goto label_35;
        }
        rbx = rcx;
label_21:
        esi--;
    } while (esi != 0);
label_6:
    r12d |= edi;
    do {
label_5:
        rax = rdx + r9;
        edx = r12d;
        edx |= 2;
        *(rbp) = rax;
        if (*(rax) != 0) {
            r12d = edx;
        }
        goto label_3;
        r9 = (int64_t) r9d;
label_11:
        rax = (int64_t) ecx;
        rcx = rbx;
        rcx *= rax;
        if (*(rax) overflow 0) {
            goto label_9;
        }
        rax *= rcx;
        if (*(rax) overflow 0) {
            goto label_36;
        }
label_4:
        rbx = rax;
    } while (1);
    r9 = (int64_t) r9d;
label_12:
    rax = (int64_t) ecx;
    rax *= rbx;
    if (*(rax) !overflow 0) {
        goto label_4;
    }
label_9:
    r12d = 1;
    if (rbx < 0) {
        goto label_37;
    }
label_7:
    rbx = 0x7fffffffffffffff;
    goto label_5;
    r9 = (int64_t) r9d;
label_13:
    esi = 3;
    edi = 0;
    rax = (int64_t) ecx;
    r10 = 0x8000000000000000;
    r8 = 0x7fffffffffffffff;
    do {
        rcx = rbx;
        rcx *= rax;
        if (rbx overflow 0) {
            goto label_38;
        }
        rbx = rcx;
label_20:
        esi--;
    } while (esi != 0);
    goto label_6;
    rax = rbx * 0x400;
    r9 = (int64_t) r9d;
    if (esi !overflow 0) {
        goto label_4;
    }
label_8:
    r12d = 1;
    if (rbx >= 0) {
        goto label_7;
    }
    r12d = 1;
label_37:
    rbx = 0x8000000000000000;
    goto label_5;
    rax = rbx * 2;
    r9 = (int64_t) r9d;
    if (rbx !overflow 0) {
        goto label_4;
    }
    goto label_8;
    r9 = (int64_t) r9d;
label_16:
    esi = 5;
    edi = 0;
    rax = (int64_t) ecx;
    r10 = 0x8000000000000000;
    r8 = 0x7fffffffffffffff;
    do {
        rcx = rbx;
        rcx *= rax;
        if (rbx overflow 0) {
            goto label_39;
        }
        rbx = rcx;
label_22:
        esi--;
    } while (esi != 0);
    goto label_6;
    r9 = (int64_t) r9d;
label_14:
    esi = 6;
    edi = 0;
    rax = (int64_t) ecx;
    r10 = 0x8000000000000000;
    r8 = 0x7fffffffffffffff;
    do {
        rcx = rbx;
        rcx *= rax;
        if (esi overflow 0) {
            goto label_40;
        }
        rbx = rcx;
label_23:
        esi--;
    } while (esi != 0);
    goto label_6;
    r9 = (int64_t) r9d;
    goto label_5;
    r9 = (int64_t) r9d;
label_17:
    esi = 7;
    edi = 0;
    rax = (int64_t) ecx;
    r10 = 0x8000000000000000;
    r8 = 0x7fffffffffffffff;
    do {
        rcx = rbx;
        rcx *= rax;
        if (esi overflow 0) {
            goto label_41;
        }
        rbx = rcx;
label_19:
        esi--;
    } while (esi != 0);
    goto label_6;
    r9 = (int64_t) r9d;
label_18:
    esi = 8;
    edi = 0;
    rax = (int64_t) ecx;
    r10 = 0x8000000000000000;
    r8 = 0x7fffffffffffffff;
    do {
        rcx = rbx;
        rcx *= rax;
        if (esi overflow 0) {
            goto label_42;
        }
        rbx = rcx;
label_24:
        esi--;
    } while (esi != 0);
    goto label_6;
    r9 = (int64_t) r9d;
label_15:
    rax = rbx * 0x200;
    if (esi !overflow 0) {
        goto label_4;
    }
    goto label_9;
    r9d = 1;
    ecx = 0x400;
    goto label_10;
    r9d = 1;
    ecx = 0x400;
    goto label_11;
    r9d = 1;
    ecx = 0x400;
    goto label_12;
    r9d = 1;
    ecx = 0x400;
    goto label_13;
    r9d = 1;
    ecx = 0x400;
    goto label_14;
    r9d = 1;
    goto label_15;
    r9d = 1;
    ecx = 0x400;
    goto label_16;
    r9d = 1;
    ecx = 0x400;
    goto label_17;
    r9d = 1;
    ecx = 0x400;
    goto label_18;
    r9d = 1;
    goto label_5;
label_33:
    r9d = 2;
    ecx = 0x3e8;
    goto label_2;
label_34:
    r9d = 0;
    r9b = (*((rdx + 2)) == 0x42) ? 1 : 0;
    r9d = r9 + r9 + 1;
    goto label_2;
label_25:
    assert_fail ("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 0x55, "xstrtoimax");
label_29:
    stack_chk_fail ();
label_41:
    edi = 1;
    if (rbx >= 0) {
        rbx = r8;
        goto label_19;
    }
    rbx = r10;
    goto label_19;
label_38:
    edi = 1;
    if (rbx >= 0) {
        rbx = r8;
        goto label_20;
    }
    rbx = r10;
    goto label_20;
label_35:
    edi = 1;
    if (rbx >= 0) {
        rbx = r8;
        goto label_21;
    }
    rbx = r10;
    goto label_21;
label_39:
    edi = 1;
    if (rbx >= 0) {
        rbx = r8;
        goto label_22;
    }
    rbx = r10;
    goto label_22;
label_40:
    edi = 1;
    if (rbx >= 0) {
        rbx = r8;
        goto label_23;
    }
    rbx = r10;
    goto label_23;
label_42:
    edi = 1;
    if (rbx >= 0) {
        rbx = r8;
        goto label_24;
    }
    rbx = r10;
    goto label_24;
label_36:
    r12d = 1;
    rbx = 0x8000000000000000;
    rax = 0x7fffffffffffffff;
    __asm ("cmovns rbx, rax");
    goto label_5;
}

/* /tmp/tmpaz2gywv9 @ 0x5640 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x26a4)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaz2gywv9 @ 0x71c4 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmpaz2gywv9 @ 0x6190 */
 
uint64_t xreallocarray (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpaz2gywv9 @ 0x67a0 */
 
void dbg_xdectoimax (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* intmax_t xdectoimax(char const * n_str,intmax_t min,intmax_t max,char const * suffixes,char const * err,int err_exit); */
    xnumtoimax (rdi, 0xa, rsi, rdx, rcx, r8);
}

/* /tmp/tmpaz2gywv9 @ 0x6690 */
 
int64_t dbg_xnumtoimax (uint32_t status, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg6) {
    intmax_t tnum;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r9 = arg6;
    /* intmax_t xnumtoimax(char const * n_str,int base,intmax_t min,intmax_t max,char const * suffixes,char const * err,int err_exit); */
    r14 = r9;
    r13 = rcx;
    r12 = rdx;
    edx = esi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    eax = xstrtoimax (rdi, 0, edx, rsp, r8);
    if (eax == 0) {
        r15 = *(rsp);
        if (r15 < r12) {
            goto label_2;
        }
        if (r15 > r13) {
            goto label_2;
        }
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        rax = r15;
        return rax;
    }
    ebx = eax;
    rax = errno_location ();
    r12 = rax;
    if (ebx != 1) {
        if (ebx != 3) {
            goto label_1;
        }
        *(rax) = 0;
    } else {
label_0:
        *(r12) = 0x4b;
    }
label_1:
    rax = quote (rbp, rsi, rdx, rcx, r8);
    esi = *(r12);
    r8 = rax;
    while (1) {
        if (*((rsp + 0x50)) == 0) {
            *((rsp + 0x50)) = 1;
        }
        rcx = r14;
        eax = 0;
        error (*((rsp + 0x50)), rsi, "%s: %s");
        esi = 0;
    }
label_2:
    rax = errno_location ();
    r12 = rax;
    if (r15 > 0x3fffffff) {
        goto label_0;
    }
    if (r15 < 0xffffffffc0000000) {
        goto label_0;
    }
    *(rax) = 0x22;
    goto label_1;
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmpaz2gywv9 @ 0x59c0 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpaz2gywv9 @ 0x6290 */
 
int64_t dbg_x2realloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpaz2gywv9 @ 0x6130 */
 
uint64_t xrealloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmpaz2gywv9 @ 0x6ed0 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x2570)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmpaz2gywv9 @ 0x60d0 */
 
uint64_t xmalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpaz2gywv9 @ 0x5180 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpaz2gywv9 @ 0x6610 */
 
uint64_t dbg_xstrdup (int64_t arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rax = malloc (r12);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2540)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpaz2gywv9 @ 0x2430 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmpaz2gywv9 @ 0x60f0 */
 
uint64_t ximalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpaz2gywv9 @ 0x5e50 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0x59e0)() ();
}

/* /tmp/tmpaz2gywv9 @ 0x35a0 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, "write error");
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x00008878);
    } while (1);
}

/* /tmp/tmpaz2gywv9 @ 0x70f0 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmpaz2gywv9 @ 0x6090 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpaz2gywv9 @ 0x7070 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpaz2gywv9 @ 0x5830 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x0000c210]");
    __asm ("movdqa xmm2, xmmword [0x0000c220]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x26b3)() ();
    }
    if (rax == 0) {
        void (*0x26b3)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaz2gywv9 @ 0x55b0 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000c210]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x0000c220]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaz2gywv9 @ 0x64e0 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpaz2gywv9 @ 0x2500 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmpaz2gywv9 @ 0x4e50 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmpaz2gywv9 @ 0x5970 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpaz2gywv9 @ 0x4df0 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmpaz2gywv9 @ 0x6550 */
 
uint64_t xmemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2540)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpaz2gywv9 @ 0x5090 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rbx += 0x10;
        free (*(rbx));
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        free (rdi);
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        free (r12);
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmpaz2gywv9 @ 0x3590 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmpaz2gywv9 @ 0x6160 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpaz2gywv9 @ 0x6320 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpaz2gywv9 @ 0x6e40 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x23f0)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpaz2gywv9 @ 0x6240 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmpaz2gywv9 @ 0x5980 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpaz2gywv9 @ 0x58d0 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000c210]");
    __asm ("movdqa xmm2, xmmword [0x0000c220]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x26b8)() ();
    }
    if (rax == 0) {
        void (*0x26b8)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaz2gywv9 @ 0x6590 */
 
uint64_t dbg_ximemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2540)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpaz2gywv9 @ 0x5080 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0x4f90)() ();
}

/* /tmp/tmpaz2gywv9 @ 0x5160 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpaz2gywv9 @ 0x26c0 */
 
int64_t dbg_main (int32_t argc, char ** argv) {
    stat sb;
    uint32_t var_fh;
    int64_t var_10h;
    int64_t var_18h;
    uint32_t var_20h;
    int64_t var_28h;
    void * buf;
    int64_t var_48h;
    int64_t var_60h;
    int64_t var_68h;
    uint32_t var_c8h;
    rdi = argc;
    rsi = argv;
    /* int main(int argc,char ** argv); */
    r15d = 0;
    r14 = "EgGkKmMPtTYZ0";
    r13 = obj_longopts;
    r12 = 0x000080a4;
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0xc8)) = rax;
    eax = 0;
    set_program_name (*(rsi), rsi, rdx);
    setlocale (6, 0x00008029);
    bindtextdomain (r12, "/usr/local/share/locale");
    r12 = "cor:s:";
    textdomain (r12, rsi);
    rdi = dbg_close_stdout;
    atexit ();
    *(rsp) = 0;
    *((rsp + 0xf)) = 0;
    do {
label_0:
        r8d = 0;
        rcx = r13;
        rdx = r12;
        rsi = rbx;
        edi = ebp;
        eax = getopt_long ();
        if (eax == 0xffffffff) {
            goto label_17;
        }
        if (eax == 0x6f) {
            goto label_18;
        }
        if (eax > 0x6f) {
            goto label_19;
        }
        if (eax == 0xffffff7e) {
            goto label_20;
        }
        if (eax != 0x63) {
            goto label_21;
        }
        *(obj.no_create) = 1;
    } while (1);
label_19:
    if (eax != 0x72) {
        goto label_22;
    }
    rax = optarg;
    *(obj.ref_file) = rax;
    goto label_0;
label_21:
    if (eax == 0xffffff7d) {
        goto label_23;
    }
    do {
label_2:
        rax = dbg_usage (1);
label_22:
    } while (eax != 0x73);
    rax = ctype_b_loc ();
    rcx = *(rax);
    rax = optarg;
    while ((*((rcx + rsi*2 + 1)) & 0x20) != 0) {
        *(obj.optarg) = rax;
        esi = *(rax);
        rdi = rax;
        rax++;
    }
    if (sil == 0x3c) {
        goto label_24;
    }
    if (sil > 0x3c) {
        goto label_25;
    }
    if (sil == 0x25) {
        goto label_26;
    }
    if (sil == 0x2f) {
        rdi++;
        r15d = 4;
        *(obj.optarg) = rdi;
    }
label_1:
    rax = optarg;
    while ((*((rcx + rsi*2 + 1)) & 0x20) != 0) {
        *(obj.optarg) = rax;
        esi = *(rax);
        rax++;
        rdx = rsi;
    }
    edx -= 0x2b;
    edx &= 0xfd;
    edx = 5;
    if (edx != 0) {
        goto label_27;
    }
    if (r15d != 0) {
        goto label_28;
    }
    r15d = 1;
    rax = dcgettext (0, "Invalid number");
    rsi <<= 0x3f;
    rdx = 0x7fffffffffffffff;
    rax = xdectoimax (*(obj.optarg), 1, rdx, r14, rax, 0);
    *((rsp + 0xf)) = 1;
    *(rsp) = rax;
    goto label_0;
label_25:
    if (sil != 0x3e) {
        goto label_1;
    }
    rdi++;
    r15d = 2;
    *(obj.optarg) = rdi;
    goto label_1;
label_18:
    *(obj.block_mode) = 1;
    goto label_0;
label_23:
    eax = 0;
    version_etc (*(obj.stdout), "truncate", "GNU coreutils", *(obj.Version), "Padraig Brady", 0);
    exit (0);
label_27:
    rax = dcgettext (0, "Invalid number");
    rsi <<= 0x3f;
    rdx = 0x7fffffffffffffff;
    rax = xdectoimax (*(obj.optarg), 1, rdx, r14, rax, 0);
    *(rsp) = rax;
    rcx = rax;
    eax = r15 - 4;
    if (eax <= 1) {
        if (rcx == 0) {
            goto label_29;
        }
    }
    *((rsp + 0xf)) = 1;
    goto label_0;
label_26:
    rdi++;
    r15d = 5;
    *(obj.optarg) = rdi;
    goto label_1;
label_24:
    rdi++;
    r15d = 3;
    *(obj.optarg) = rdi;
    goto label_1;
label_17:
    rdx = *(obj.optind);
    r12 = ref_file;
    rbx = rbx + rdx*8;
    ebp -= edx;
    if (r12 == 0) {
        goto label_30;
    }
    if (r15d == 0) {
        if (*((rsp + 0xf)) != 0) {
            rsi = "--reference";
            edi = 1;
            rax = quote_n ();
            rsi = "--size";
            edi = 0;
            rbx = rax;
            rax = quote_n ();
            edx = 5;
            rsi = "you must specify a relative %s with %s";
            r12 = rax;
        }
    } else {
        if (*(obj.block_mode) != 1) {
            goto label_31;
        }
        if (*((rsp + 0xf)) != 0) {
            goto label_31;
        }
        rsi = "--size";
        edi = 1;
        rax = quote_n ();
        rsi = "--io-blocks";
        edi = 0;
        rbx = rax;
        rax = quote_n ();
        edx = 5;
        r12 = rax;
    }
label_15:
    rax = dcgettext (0, "%s was specified but %s was not");
    r8 = rbx;
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    goto label_2;
label_31:
    if (ebp <= 0) {
        goto label_9;
    }
    rsi = rsp + 0x30;
    rdi = r12;
    eax = stat ();
    if (eax != 0) {
        goto label_32;
    }
    eax = *((rsp + 0x48));
    eax &= 0xd000;
    if (eax != obj._IO_stdin_used) {
        goto label_33;
    }
    r13 = *((rsp + 0x60));
    if (r13 < 0) {
        goto label_34;
    }
label_12:
    *((rsp + 0x18)) = 0xffffffffffffffff;
    if (*((rsp + 0xf)) != 0) {
        *((rsp + 0x18)) = r13;
        r13 = *(rsp);
    }
    *(rsp) = r13;
label_10:
    rbp = *(rbx);
    *((rsp + 0xf)) = 0;
    r12d -= r12d;
    r12d &= 0x40;
    r12d += 0x801;
    if (rbp == 0) {
        goto label_35;
    }
    rax = *((rsp + 0x18));
    rax >>= 0x3f;
    *((rsp + 0x20)) = rax;
    while (eax < 0) {
        rax = errno_location ();
        r13 = rax;
        if (*(obj.no_create) != 0) {
            if (*(rax) == 2) {
                goto label_3;
            }
        }
        rsi = rbp;
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        r14 = rax;
        rax = dcgettext (0, "cannot open %s for writing");
        rcx = r14;
        eax = 0;
        eax = error (0, *(r13), rax);
        *((rsp + 0xf)) = 1;
label_3:
        rbp = *((rbx + 8));
        rbx += 8;
        if (rbp == 0) {
            goto label_35;
        }
        eax = 0;
        eax = open (rbp, r12d, 0x1b6);
        r14d = eax;
    }
    if (*(obj.block_mode) != 0) {
        goto label_36;
    }
    if (r15d == 0) {
        goto label_37;
    }
    if (*((rsp + 0x20)) == 0) {
        goto label_37;
    }
    eax = fstat (eax, rsp + 0x30);
    r13 = *(rsp);
    if (eax == 0) {
        goto label_38;
    }
label_5:
    rsi = rbp;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    *((rsp + 0x10)) = rax;
label_11:
    rax = dcgettext (0, "cannot fstat %s");
    r13 = rax;
    rax = errno_location ();
    rcx = *((rsp + 0x10));
    eax = 0;
    r13d = 0;
    error (0, *(rax), r13);
    do {
label_4:
        eax = close (r14d);
        if (eax != 0) {
            goto label_39;
        }
        r13d ^= 1;
        goto label_3;
label_9:
        edx = 5;
label_16:
        rax = dcgettext (0, "missing file operand");
        eax = 0;
        error (0, 0, rax);
        dbg_usage (1);
label_37:
        r13 = *(rsp);
label_6:
        if (r15d != 0) {
            rax = *((rsp + 0x18));
            rcx = *((rsp + 0x18));
            rax++;
            if (rax == 0) {
                goto label_38;
            }
label_7:
            if (r15d == 2) {
                goto label_40;
            }
            if (r15d == 3) {
                goto label_41;
            }
            if (r15d == 4) {
                goto label_42;
            }
            if (r15d == 5) {
                goto label_43;
            }
label_13:
            rcx += r13;
            r13 = rcx;
            if (rcx overflow 0) {
                goto label_44;
            }
        }
label_8:
        r8d = 0;
        edi = r14d;
        __asm ("cmovns r8, r13");
        r13d = 1;
        rsi = r8;
        *((rsp + 0x10)) = r8;
        eax = ftruncate ();
        r8 = *((rsp + 0x10));
    } while (eax == 0);
    rsi = rbp;
    edi = 4;
    *((rsp + 0x28)) = r8;
    rax = quotearg_style ();
    edx = 5;
    *((rsp + 0x10)) = rax;
    rax = dcgettext (0, "failed to truncate %s at %ld bytes");
    r13 = rax;
    rax = errno_location ();
    r8 = *((rsp + 0x28));
    rcx = *((rsp + 0x10));
    eax = 0;
    r13d = 0;
    error (0, *(rax), r13);
    goto label_4;
label_39:
    rsi = rbp;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r14 = rax;
    rax = dcgettext (0, "failed to close %s");
    r13 = rax;
    rax = errno_location ();
    rcx = r14;
    eax = 0;
    eax = error (0, *(rax), r13);
    *((rsp + 0xf)) = 1;
    goto label_3;
label_36:
    eax = fstat (eax, rsp + 0x30);
    if (eax != 0) {
        goto label_5;
    }
    r8 = *((rsp + 0x68));
    r13 = *(rsp);
    rdi = 0x1fffffffffffffff;
    rax = r8 - 1;
    eax = 0x200;
    if (rax > rdi) {
        r8 = rax;
    }
    r13 *= r8;
    if (rax !overflow rdi) {
        goto label_6;
    }
    rsi = rbp;
    edi = 4;
    *((rsp + 0x10)) = r8;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "overflow in %ld * %ld byte blocks for file %s");
    r8 = *((rsp + 0x10));
    rcx = *(rsp);
    r9 = r13;
    eax = 0;
    error (0, 0, rax);
    r13d = 0;
    goto label_4;
label_35:
    eax = *((rsp + 0xf));
    rdx = *((rsp + 0xc8));
    rdx -= *(fs:0x28);
    if (rdx != 0) {
        goto label_45;
    }
    return rax;
label_38:
    eax = *((rsp + 0x48));
    eax &= 0xd000;
    if (eax != obj._IO_stdin_used) {
        goto label_46;
    }
    rcx = *((rsp + 0x60));
    if (rcx >= 0) {
        goto label_7;
    }
    rsi = rbp;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
label_14:
    rax = dcgettext (0, "%s has unusable, apparently negative size");
    rcx = r13;
    eax = 0;
    r13d = 0;
    error (0, 0, rax);
    goto label_4;
label_40:
    if (r13 < rcx) {
        r13 = rcx;
    }
    goto label_8;
label_41:
    if (r13 > rcx) {
        r13 = rcx;
    }
    goto label_8;
label_30:
    if (*((rsp + 0xf)) == 0) {
        goto label_47;
    }
    if (ebp <= 0) {
        goto label_9;
    }
    *((rsp + 0x18)) = 0xffffffffffffffff;
    goto label_10;
label_46:
    edx = 2;
    esi = 0;
    edi = r14d;
    rax = lseek ();
    rcx = rax;
    if (rax >= 0) {
        goto label_7;
    }
    rsi = rbp;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    rsi = "cannot get the size of %s";
    *((rsp + 0x10)) = rax;
    goto label_11;
label_42:
    rax = rcx;
    __asm ("cqo");
    rax = rdx:rax / r13;
    rdx = rdx:rax % r13;
    rcx -= rdx;
    r13 = rcx;
    goto label_8;
label_33:
    eax = 0;
    eax = open (r12, 0, rdx);
    r14d = eax;
    rax = errno_location ();
    if (r14d < 0) {
        goto label_34;
    }
    esi = 0;
    edi = r14d;
    edx = 2;
    rax = lseek ();
    r12d = *(rbp);
    r13 = rax;
    close (r14d);
    if (r13 >= 0) {
        goto label_12;
    }
    *(rbp) = r12d;
label_34:
    rsi = ref_file;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "cannot get the size of %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (1, *(rax), r12);
label_43:
    rax = rcx;
    __asm ("cqo");
    rax = rdx:rax / r13;
    rdx = rdx:rax % r13;
    if (rdx != 0) {
        r13 -= rdx;
        goto label_13;
label_44:
        rsi = rbp;
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        rsi = "overflow extending size of file %s";
        r13 = rax;
        goto label_14;
    }
    r13 = rcx;
    goto label_8;
label_20:
    dbg_usage (0);
label_29:
    edx = 5;
    rax = dcgettext (0, "division by zero");
    eax = 0;
    error (1, 0, rax);
label_32:
    rsi = r12;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "cannot stat %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (1, *(rax), r12);
label_47:
    rsi = "--reference";
    edi = 1;
    rax = quote_n ();
    rsi = "--size";
    edi = 0;
    rbx = rax;
    rax = quote_n ();
    edx = 5;
    rsi = "you must specify either %s or %s";
    r12 = rax;
    goto label_15;
label_28:
    rsi = "multiple relative modifiers specified";
    goto label_16;
label_45:
    return stack_chk_fail ();
}

/* /tmp/tmpaz2gywv9 @ 0x3160 */
 
int64_t dbg_usage (int64_t arg1) {
    infomap const[7] const infomap;
    char * var_8h;
    int64_t var_10h;
    char * var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    char * var_60h;
    int64_t var_68h;
    int64_t var_78h;
    rdi = arg1;
    /* void usage(int status); */
    edx = 5;
    r12 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r12;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_0:
        exit (ebp);
    }
    rbx = rsp;
    rax = dcgettext (0, "Usage: %s OPTION... FILE...\n");
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "Shrink or extend the size of each FILE to the specified size\n\nA FILE argument that does not exist is created.\n\nIf a FILE is larger than the specified size, the extra data is lost.\nIf a FILE is shorter, it is extended and the sparse extended part (hole)\nreads as zero bytes.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nMandatory arguments to long options are mandatory for short options too.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -c, --no-create        do not create any files\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -o, --io-blocks        treat SIZE as number of IO blocks instead of bytes\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -r, --reference=RFILE  base size on RFILE\n  -s, --size=SIZE        set or adjust the file size by SIZE bytes\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nThe SIZE argument is an integer and optional unit (example: 10K is 10*1024).\nUnits are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers of 1000).\nBinary prefixes can be used, too: KiB=K, MiB=M, and so on.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nSIZE may also be prefixed by one of the following modifying characters:\n'+' extend by, '-' reduce by, '<' at most, '>' at least,\n'/' round down to multiple of, '%' round up to multiple of.\n");
    rsi = r12;
    r12 = "truncate";
    rdi = rax;
    fputs_unlocked ();
    rax = 0x0000802a;
    rcx = "sha256sum";
    *((rsp + 0x60)) = 0;
    *(rsp) = rax;
    rax = "test invocation";
    *((rsp + 8)) = rax;
    rax = 0x000080a4;
    *((rsp + 0x10)) = rax;
    rax = "Multi-call invocation";
    *((rsp + 0x18)) = rax;
    rax = "sha224sum";
    *((rsp + 0x30)) = rcx;
    rcx = "sha384sum";
    *((rsp + 0x20)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x40)) = rcx;
    rcx = "sha512sum";
    *((rsp + 0x28)) = rax;
    *((rsp + 0x38)) = rax;
    *((rsp + 0x48)) = rax;
    *((rsp + 0x50)) = rcx;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x68)) = 0;
    do {
        rsi = *((rbx + 0x10));
        rbx += 0x10;
        if (rsi == 0) {
            goto label_2;
        }
        eax = strcmp (r12, rsi);
    } while (eax != 0);
label_2:
    r13 = *((rbx + 8));
    edx = 5;
    rsi = "\n%s online help: <%s>\n";
    edi = 0;
    if (r13 == 0) {
        goto label_3;
    }
    rax = dcgettext (rdi, rsi);
    r14 = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    rcx = r14;
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax != 0) {
        eax = strncmp (rdi, 0x000080ae, 3);
        if (eax != 0) {
            goto label_4;
        }
    }
label_1:
    edx = 5;
    rax = dcgettext (0, "Full documentation <%s%s>\n");
    rcx = r12;
    rdx = r14;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    rax = 0x00008029;
    r12 = 0x00008046;
    r12 = rax;
    while (1) {
        edx = 5;
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rcx = r12;
        rdx = r13;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        goto label_0;
label_3:
        rax = dcgettext (rdi, rsi);
        r14 = "https://www.gnu.org/software/coreutils/";
        edi = 1;
        rdx = "GNU coreutils";
        rsi = rax;
        rcx = r14;
        eax = 0;
        printf_chk ();
        rax = setlocale (5, 0);
        rdi = rax;
        if (rax != 0) {
            eax = strncmp (rdi, 0x000080ae, 3);
            if (eax != 0) {
                goto label_5;
            }
        }
        edx = 5;
        rax = dcgettext (0, "Full documentation <%s%s>\n");
        rcx = r12;
        rdx = r14;
        edi = 1;
        rsi = rax;
        eax = 0;
        r13 = "truncate";
        printf_chk ();
        r12 = 0x00008046;
    }
label_5:
    r13 = "truncate";
label_4:
    r15 = stdout;
    edx = 5;
    rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
    rdi = rax;
    rsi = r15;
    fputs_unlocked ();
    goto label_1;
}

/* /tmp/tmpaz2gywv9 @ 0x2630 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmpaz2gywv9 @ 0x2610 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmpaz2gywv9 @ 0x25c0 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmpaz2gywv9 @ 0x24f0 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmpaz2gywv9 @ 0x2510 */
 
void strcmp (void) {
    __asm ("bnd jmp qword [reloc.strcmp]");
}

/* /tmp/tmpaz2gywv9 @ 0x25b0 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmpaz2gywv9 @ 0x4e30 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmpaz2gywv9 @ 0x5520 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000c210]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x0000c220]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaz2gywv9 @ 0x5350 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x269f)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaz2gywv9 @ 0x5230 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x2695)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaz2gywv9 @ 0x4ed0 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x268a)() ();
    }
    if (rdx == 0) {
        void (*0x268a)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmpaz2gywv9 @ 0x53e0 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000c210]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x0000c220]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaz2gywv9 @ 0x64b0 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpaz2gywv9 @ 0x6530 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpaz2gywv9 @ 0x5130 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpaz2gywv9 @ 0x61d0 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpaz2gywv9 @ 0x59a0 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpaz2gywv9 @ 0x6f20 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x25f0)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmpaz2gywv9 @ 0x4e70 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmpaz2gywv9 @ 0x63b0 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmpaz2gywv9 @ 0x5ff0 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmpaz2gywv9 @ 0x2520 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmpaz2gywv9 @ 0x3580 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmpaz2gywv9 @ 0x6dd0 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpaz2gywv9 @ 0x23c0 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmpaz2gywv9 @ 0x59e0 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = 0x00008ca8;
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x00008cbb);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x00008fa8;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0x8fa8 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x2630)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2630)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2630)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmpaz2gywv9 @ 0x5e70 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpaz2gywv9 @ 0x2000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmpaz2gywv9 @ 0x6510 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpaz2gywv9 @ 0x71a0 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmpaz2gywv9 @ 0x6210 */
 
uint64_t dbg_xnmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpaz2gywv9 @ 0x65d0 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi + 1);
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2540)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpaz2gywv9 @ 0x2360 */
 
void free (void) {
    /* [15] -r-x section size 800 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmpaz2gywv9 @ 0x2370 */
 
void strtoimax (void) {
    __asm ("bnd jmp qword [reloc.strtoimax]");
}

/* /tmp/tmpaz2gywv9 @ 0x0 */
 
uint32_t rotate_left32 (uint32_t value, uint32_t count) {
    const uint32_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value << count) | (value >> (-count & mask));
}
 
int64_t libc_start_main (int32_t argc, func init, func main, char ** ubp_av) {
    rsi = argc;
    rcx = init;
    rdi = main;
    rdx = ubp_av;
    bh &= *(rdi);
    *(rax) += dh;
    *((rcx + rsi)) ^= esi;
    *(rsi) += bh;
    *(rcx) += al;
    *(rax) += al;
    *((rax + 0x30)) += dh;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *((rax + 0x2a9)) += bl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax |= 0x28004000;
    *(rax) += al;
    *(rsi) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) |= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax += *(rax);
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    __asm ("enter 0x15, 0");
    *(rax) += al;
    *(rax) += al;
    __asm ("enter 0x15, 0");
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    eax += 0;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    cl += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) = rotate_left32 (*(rcx), );
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) = 0;
    *(rax) += al;
    *(rax) += al;
    *((rax + 0x23)) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *(rsi) += al;
    *(rax) += al;
    al += dh;
    *(rdi) = al;
    rdi++;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
}

/* /tmp/tmpaz2gywv9 @ 0x23b0 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmpaz2gywv9 @ 0x23e0 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmpaz2gywv9 @ 0x23f0 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmpaz2gywv9 @ 0x2400 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmpaz2gywv9 @ 0x2420 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmpaz2gywv9 @ 0x2450 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmpaz2gywv9 @ 0x2460 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmpaz2gywv9 @ 0x2470 */
 
void strchr (void) {
    __asm ("bnd jmp qword [reloc.strchr]");
}

/* /tmp/tmpaz2gywv9 @ 0x2490 */
 
void ftruncate (void) {
    __asm ("bnd jmp qword [reloc.ftruncate]");
}

/* /tmp/tmpaz2gywv9 @ 0x24a0 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmpaz2gywv9 @ 0x24b0 */
 
void assert_fail (void) {
    __asm ("bnd jmp qword [reloc.__assert_fail]");
}

/* /tmp/tmpaz2gywv9 @ 0x24c0 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmpaz2gywv9 @ 0x24d0 */
 
void close (void) {
    __asm ("bnd jmp qword [reloc.close]");
}

/* /tmp/tmpaz2gywv9 @ 0x24e0 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmpaz2gywv9 @ 0x2530 */
 
void stat (void) {
    __asm ("bnd jmp qword [reloc.stat]");
}

/* /tmp/tmpaz2gywv9 @ 0x2540 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmpaz2gywv9 @ 0x2550 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmpaz2gywv9 @ 0x2570 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmpaz2gywv9 @ 0x2590 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmpaz2gywv9 @ 0x25e0 */
 
void open (void) {
    __asm ("bnd jmp qword [reloc.open]");
}

/* /tmp/tmpaz2gywv9 @ 0x25f0 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmpaz2gywv9 @ 0x2600 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmpaz2gywv9 @ 0x2640 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmpaz2gywv9 @ 0x2650 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmpaz2gywv9 @ 0x2660 */
 
void fstat (void) {
    __asm ("bnd jmp qword [reloc.fstat]");
}

/* /tmp/tmpaz2gywv9 @ 0x2670 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmpaz2gywv9 @ 0x2030 */
 
void fcn_00002030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 816 named .plt */
    __asm ("bnd jmp qword [0x0000be38]");
}

/* /tmp/tmpaz2gywv9 @ 0x2040 */
 
void fcn_00002040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2050 */
 
void fcn_00002050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2060 */
 
void fcn_00002060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2070 */
 
void fcn_00002070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2080 */
 
void fcn_00002080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2090 */
 
void fcn_00002090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x20a0 */
 
void fcn_000020a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x20b0 */
 
void fcn_000020b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x20c0 */
 
void fcn_000020c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x20d0 */
 
void fcn_000020d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x20e0 */
 
void fcn_000020e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x20f0 */
 
void fcn_000020f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2100 */
 
void fcn_00002100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2110 */
 
void fcn_00002110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2120 */
 
void fcn_00002120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2130 */
 
void fcn_00002130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2140 */
 
void fcn_00002140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2150 */
 
void fcn_00002150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2160 */
 
void fcn_00002160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2170 */
 
void fcn_00002170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2180 */
 
void fcn_00002180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2190 */
 
void fcn_00002190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x21a0 */
 
void fcn_000021a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x21b0 */
 
void fcn_000021b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x21c0 */
 
void fcn_000021c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x21d0 */
 
void fcn_000021d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x21e0 */
 
void fcn_000021e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x21f0 */
 
void fcn_000021f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2200 */
 
void fcn_00002200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2210 */
 
void fcn_00002210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2220 */
 
void fcn_00002220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2230 */
 
void fcn_00002230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2240 */
 
void fcn_00002240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2250 */
 
void fcn_00002250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2260 */
 
void fcn_00002260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2270 */
 
void fcn_00002270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2280 */
 
void fcn_00002280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2290 */
 
void fcn_00002290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x22a0 */
 
void fcn_000022a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x22b0 */
 
void fcn_000022b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x22c0 */
 
void fcn_000022c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x22d0 */
 
void fcn_000022d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x22e0 */
 
void fcn_000022e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x22f0 */
 
void fcn_000022f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2300 */
 
void fcn_00002300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2310 */
 
void fcn_00002310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2320 */
 
void fcn_00002320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2330 */
 
void fcn_00002330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpaz2gywv9 @ 0x2340 */
 
void fcn_00002340 (void) {
    return __asm ("bnd jmp section..plt");
}
