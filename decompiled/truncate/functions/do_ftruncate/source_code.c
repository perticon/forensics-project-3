do_ftruncate (int fd, char const *fname, off_t ssize, off_t rsize,
              rel_mode_t rel_mode)
{
  struct stat sb;
  off_t nsize;

  if ((block_mode || (rel_mode && rsize < 0)) && fstat (fd, &sb) != 0)
    {
      error (0, errno, _("cannot fstat %s"), quoteaf (fname));
      return false;
    }
  if (block_mode)
    {
      ptrdiff_t blksize = ST_BLKSIZE (sb);
      intmax_t ssize0 = ssize;
      if (INT_MULTIPLY_WRAPV (ssize, blksize, &ssize))
        {
          error (0, 0,
                 _("overflow in %" PRIdMAX
                   " * %" PRIdPTR " byte blocks for file %s"),
                 ssize0, blksize, quoteaf (fname));
          return false;
        }
    }
  if (rel_mode)
    {
      off_t fsize;

      if (0 <= rsize)
        fsize = rsize;
      else
        {
          if (usable_st_size (&sb))
            {
              fsize = sb.st_size;
              if (fsize < 0)
                {
                  /* Sanity check.  Overflow is the only reason I can think
                     this would ever go negative. */
                  error (0, 0, _("%s has unusable, apparently negative size"),
                         quoteaf (fname));
                  return false;
                }
            }
          else
            {
              fsize = lseek (fd, 0, SEEK_END);
              if (fsize < 0)
                {
                  error (0, errno, _("cannot get the size of %s"),
                         quoteaf (fname));
                  return false;
                }
            }
        }

      if (rel_mode == rm_min)
        nsize = MAX (fsize, ssize);
      else if (rel_mode == rm_max)
        nsize = MIN (fsize, ssize);
      else if (rel_mode == rm_rdn)
        /* 0..ssize-1 -> 0 */
        nsize = fsize - fsize % ssize;
      else
        {
          if (rel_mode == rm_rup)
            {
              /* 1..ssize -> ssize */
              off_t r = fsize % ssize;
              ssize = r == 0 ? 0 : ssize - r;
            }
          if (INT_ADD_WRAPV (fsize, ssize, &nsize))
            {
              error (0, 0, _("overflow extending size of file %s"),
                     quoteaf (fname));
              return false;
            }
        }
    }
  else
    nsize = ssize;
  if (nsize < 0)
    nsize = 0;

  if (ftruncate (fd, nsize) != 0)
    {
      intmax_t s = nsize;
      error (0, errno, _("failed to truncate %s at %"PRIdMAX" bytes"),
             quoteaf (fname), s);
      return false;
    }

  return true;
}