byte main(ulong param_1,int **param_2)

{
  int iVar1;
  int iVar2;
  ushort **ppuVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  int *piVar6;
  int *piVar7;
  ulong uVar8;
  uint uVar9;
  char *pcVar10;
  long lVar11;
  char *pcVar12;
  byte *pbVar13;
  byte bVar14;
  undefined1 *puVar15;
  ulong uVar16;
  ulong uVar17;
  int iVar18;
  long in_FS_OFFSET;
  ulong local_108;
  byte local_f9;
  undefined8 local_f8;
  ulong local_f0;
  ulong local_e8;
  stat local_d8;
  long local_40;
  
  iVar18 = 0;
  iVar2 = 0x1080f4;
  puVar15 = longopts;
  piVar7 = (int *)(param_1 & 0xffffffff);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  pcVar12 = "cor:s:";
  textdomain("coreutils");
  atexit(close_stdout);
  local_108 = 0;
  local_f9 = 0;
LAB_00102755:
  do {
    while( true ) {
      iVar1 = getopt_long(piVar7,param_2,pcVar12,puVar15);
      pbVar13 = ref_file;
      if (iVar1 == -1) break;
      if (iVar1 == 0x6f) {
        block_mode = '\x01';
      }
      else if (iVar1 < 0x70) {
        if (iVar1 == -0x82) {
          usage(0);
          goto LAB_00102fbc;
        }
        if (iVar1 != 99) {
          if (iVar1 != -0x83) goto LAB_001027bb;
          version_etc(stdout,"truncate","GNU coreutils",Version,"Padraig Brady",0);
                    /* WARNING: Subroutine does not return */
          exit(0);
        }
        no_create = '\x01';
      }
      else {
        if (iVar1 != 0x72) goto LAB_001027c8;
        ref_file = optarg;
      }
    }
    param_2 = param_2 + optind;
    uVar9 = (int)piVar7 - optind;
    piVar7 = (int *)(ulong)uVar9;
    if (ref_file == (byte *)0x0) {
      if (local_f9 == 0) goto LAB_0010301f;
      if ((int)uVar9 < 1) goto LAB_00102c0d;
      local_f0 = 0xffffffffffffffff;
      uVar17 = local_108;
LAB_00102ac2:
      local_108 = uVar17;
      piVar7 = *param_2;
      local_f9 = 0;
      pcVar12 = (char *)(ulong)((-(uint)(no_create == '\0') & 0x40) + 0x801);
      if (piVar7 == (int *)0x0) goto LAB_00102dd7;
      local_e8 = local_f0 >> 0x3f;
      break;
    }
    if ((iVar18 == 0) && (local_f9 != 0)) {
      param_2 = (int **)quote_n(1,"--reference");
      pcVar12 = (char *)quote_n(0,"--size");
      pcVar10 = "you must specify a relative %s with %s";
      goto LAB_00102a46;
    }
    if ((block_mode == '\x01') && (local_f9 == 0)) {
      param_2 = (int **)quote_n(1,"--size");
      pcVar12 = (char *)quote_n(0,"--io-blocks");
      pcVar10 = "%s was specified but %s was not";
      goto LAB_00102a46;
    }
    if ((int)uVar9 < 1) {
LAB_00102c0d:
      pcVar10 = "missing file operand";
      pcVar12 = (char *)ref_file;
LAB_00102c19:
      uVar4 = dcgettext(0,pcVar10,5);
      error(0,0,uVar4);
      usage();
      uVar17 = local_108;
      goto LAB_00102c3c;
    }
    iVar1 = stat((char *)ref_file,&local_d8);
    if (iVar1 == 0) {
      if ((local_d8.st_mode & 0xd000) == 0x8000) {
        uVar8 = local_d8.st_size;
        if (local_d8.st_size < 0) goto LAB_00102f34;
      }
      else {
        iVar2 = open((char *)pbVar13,0);
        piVar7 = __errno_location();
        if (iVar2 < 0) {
LAB_00102f34:
          uVar17 = quotearg_style(4,ref_file);
          pcVar12 = (char *)dcgettext(0,"cannot get the size of %s",5);
          piVar6 = __errno_location();
          uVar8 = uVar17;
          error(1,*piVar6,pcVar12);
          goto LAB_00102f77;
        }
        uVar8 = lseek(iVar2,0,2);
        iVar1 = *piVar7;
        close(iVar2);
        if ((long)uVar8 < 0) {
          *piVar7 = iVar1;
          goto LAB_00102f34;
        }
      }
      local_f0 = 0xffffffffffffffff;
      uVar17 = uVar8;
      if (local_f9 != 0) {
        uVar17 = local_108;
        local_f0 = uVar8;
      }
      goto LAB_00102ac2;
    }
    while( true ) {
      puVar15 = (undefined1 *)quotearg_style(4,pbVar13);
      uVar4 = dcgettext(0,"cannot stat %s",5);
      piVar6 = __errno_location();
      error(1,*piVar6,uVar4,puVar15);
LAB_0010301f:
      param_2 = (int **)quote_n(1,"--reference");
      pcVar12 = (char *)quote_n(0,"--size");
      pcVar10 = "you must specify either %s or %s";
LAB_00102a46:
      uVar4 = dcgettext(0,pcVar10,5);
      error(0,0,uVar4,pcVar12);
LAB_001027bb:
      do {
        iVar1 = usage(1);
LAB_001027c8:
      } while (iVar1 != 0x73);
      ppuVar3 = __ctype_b_loc();
      pbVar13 = optarg;
      do {
        optarg = pbVar13;
        bVar14 = *optarg;
        pbVar13 = optarg + 1;
      } while ((*(byte *)((long)*ppuVar3 + (ulong)bVar14 * 2 + 1) & 0x20) != 0);
      if (bVar14 == 0x3c) {
        iVar18 = 3;
        pbVar13 = optarg + 1;
      }
      else if ((char)bVar14 < '=') {
        if (bVar14 == 0x25) {
          iVar18 = 5;
          pbVar13 = optarg + 1;
        }
        else {
          pbVar13 = optarg;
          if (bVar14 == 0x2f) {
            iVar18 = 4;
            pbVar13 = optarg + 1;
          }
        }
      }
      else {
        pbVar13 = optarg;
        if (bVar14 == 0x3e) {
          iVar18 = 2;
          pbVar13 = optarg + 1;
        }
      }
      do {
        optarg = pbVar13;
        pbVar13 = optarg + 1;
      } while ((*(byte *)((long)*ppuVar3 + (ulong)*optarg * 2 + 1) & 0x20) != 0);
      if ((*optarg - 0x2b & 0xfd) == 0) {
        if (iVar18 != 0) {
          pcVar10 = "multiple relative modifiers specified";
          goto LAB_00102c19;
        }
        iVar18 = 1;
        dcgettext(0,"Invalid number",5);
        local_108 = xdectoimax(optarg,0x8000000000000000,0x7fffffffffffffff,"EgGkKmMPtTYZ0");
        local_f9 = 1;
        goto LAB_00102755;
      }
      dcgettext(0,"Invalid number",5);
      local_108 = xdectoimax(optarg,0x8000000000000000,0x7fffffffffffffff,"EgGkKmMPtTYZ0");
      if ((1 < iVar18 - 4U) || (local_108 != 0)) break;
LAB_00102fbc:
      uVar4 = dcgettext(0,"division by zero",5);
      error(1,0,uVar4);
      pbVar13 = (byte *)pcVar12;
    }
    local_f9 = 1;
  } while( true );
LAB_00102b5a:
  iVar2 = open((char *)piVar7,(int)pcVar12,0x1b6);
  if (iVar2 < 0) {
    piVar6 = __errno_location();
    if ((no_create == '\0') || (*piVar6 != 2)) {
      uVar4 = quotearg_style(4,piVar7);
      uVar5 = dcgettext(0,"cannot open %s for writing",5);
      error(0,*piVar6,uVar5,uVar4);
      local_f9 = 1;
    }
  }
  else {
    if (block_mode == '\0') {
      uVar17 = local_108;
      if ((iVar18 == 0) || ((char)local_e8 == '\0')) {
LAB_00102c3c:
        uVar16 = uVar17;
        if (iVar18 != 0) {
          uVar8 = local_f0;
          if (local_f0 == 0xffffffffffffffff) goto LAB_00102e05;
          goto LAB_00102c53;
        }
      }
      else {
        iVar1 = fstat(iVar2,&local_d8);
        if (iVar1 != 0) goto LAB_00102bac;
LAB_00102e05:
        if ((local_d8.st_mode & 0xd000) == 0x8000) {
          uVar8 = local_d8.st_size;
          if (-1 < local_d8.st_size) goto LAB_00102c53;
          uVar4 = quotearg_style(4,piVar7);
          pcVar10 = "%s has unusable, apparently negative size";
LAB_00102e43:
          uVar5 = dcgettext(0,pcVar10,5);
          bVar14 = 0;
          error(0,0,uVar5,uVar4);
          goto LAB_00102bef;
        }
        uVar8 = lseek(iVar2,0,2);
        if ((long)uVar8 < 0) {
          local_f8 = quotearg_style(4,piVar7);
          pcVar10 = "cannot get the size of %s";
          goto LAB_00102bca;
        }
LAB_00102c53:
        uVar16 = uVar17;
        if (iVar18 == 2) {
          if ((long)uVar17 < (long)uVar8) {
            uVar16 = uVar8;
          }
        }
        else if (iVar18 == 3) {
          if ((long)uVar8 < (long)uVar17) {
            uVar16 = uVar8;
          }
        }
        else if (iVar18 == 4) {
          uVar16 = uVar8 - (long)uVar8 % (long)uVar17;
        }
        else {
          if (iVar18 == 5) {
LAB_00102f77:
            uVar16 = uVar8;
            if ((long)uVar8 % (long)uVar17 == 0) goto LAB_00102c87;
            uVar17 = uVar17 - (long)uVar8 % (long)uVar17;
          }
          uVar16 = uVar8 + uVar17;
          if (SCARRY8(uVar8,uVar17)) {
            uVar4 = quotearg_style(4,piVar7);
            pcVar10 = "overflow extending size of file %s";
            goto LAB_00102e43;
          }
        }
      }
LAB_00102c87:
      uVar17 = 0;
      if (-1 < (long)uVar16) {
        uVar17 = uVar16;
      }
      bVar14 = 1;
      iVar1 = ftruncate(iVar2,uVar17);
      if (iVar1 != 0) {
        uVar4 = quotearg_style(4,piVar7);
        uVar5 = dcgettext(0,"failed to truncate %s at %ld bytes",5);
        piVar6 = __errno_location();
        bVar14 = 0;
        error(0,*piVar6,uVar5,uVar4);
      }
    }
    else {
      iVar1 = fstat(iVar2,&local_d8);
      if (iVar1 == 0) {
        lVar11 = local_d8.st_blksize;
        if (0x1fffffffffffffff < local_d8.st_blksize - 1U) {
          lVar11 = 0x200;
        }
        uVar17 = local_108 * lVar11;
        if (SEXT816((long)(local_108 * lVar11)) == SEXT816((long)local_108) * SEXT816(lVar11))
        goto LAB_00102c3c;
        uVar4 = quotearg_style(4,piVar7);
        uVar5 = dcgettext(0,"overflow in %ld * %ld byte blocks for file %s",5);
        error(0,0,uVar5,local_108,lVar11,uVar4);
        bVar14 = 0;
      }
      else {
LAB_00102bac:
        local_f8 = quotearg_style(4,piVar7);
        pcVar10 = "cannot fstat %s";
LAB_00102bca:
        uVar4 = dcgettext(0,pcVar10,5);
        piVar6 = __errno_location();
        bVar14 = 0;
        error(0,*piVar6,uVar4,local_f8);
      }
    }
LAB_00102bef:
    iVar2 = close(iVar2);
    if (iVar2 == 0) {
      local_f9 = local_f9 | bVar14 ^ 1;
    }
    else {
      uVar4 = quotearg_style(4,piVar7);
      uVar5 = dcgettext(0,"failed to close %s",5);
      piVar7 = __errno_location();
      error(0,*piVar7,uVar5,uVar4);
      local_f9 = 1;
    }
  }
  piVar7 = param_2[1];
  param_2 = param_2 + 1;
  if (piVar7 == (int *)0x0) {
LAB_00102dd7:
    if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
      __stack_chk_fail();
    }
    return local_f9;
  }
  goto LAB_00102b5a;
}