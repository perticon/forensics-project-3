void next_line_num(struct s0* rdi, struct s0* rsi) {
    struct s0* rcx3;
    struct s0* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s0* rdx7;
    struct s0* rax8;

    rcx3 = line_num_start;
    rax4 = reinterpret_cast<struct s0*>("0\t");
    do {
        edx5 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rax4->f0));
        if (*reinterpret_cast<signed char*>(&edx5) <= 56) 
            break;
        rax4 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax4) - 1);
        rax4->f1 = 48;
    } while (reinterpret_cast<unsigned char>(rcx3) <= reinterpret_cast<unsigned char>(rax4));
    goto addr_3580_4;
    edx6 = edx5 + 1;
    *reinterpret_cast<struct s0**>(&rax4->f0) = *reinterpret_cast<struct s0**>(&edx6);
    return;
    addr_3580_4:
    if (reinterpret_cast<unsigned char>(rcx3) <= reinterpret_cast<unsigned char>("                 0\t")) {
        line_buf = 62;
        rdx7 = rcx3;
    } else {
        rdx7 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rcx3) + 0xffffffffffffffff);
        *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rcx3) + 0xffffffffffffffff) = 49;
        line_num_start = rdx7;
    }
    rax8 = line_num_print;
    if (reinterpret_cast<unsigned char>(rax8) > reinterpret_cast<unsigned char>(rdx7)) {
        line_num_print = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax8) - 1);
    }
    return;
}