word32 next_line_num()
{
	struct Eq_82 * rcx_5 = line_num_start;
	struct Eq_82 * rax_14 = &g_tC021;
	do
	{
		uint64 rdx_10 = (uint64) rax_14->b0000;
		word32 edx_35 = (word32) rdx_10;
		if ((byte) rdx_10 <= 0x38)
		{
			uint64 rdx_37 = (uint64) (edx_35 + 0x01);
			rax_14->b0000 = (byte) rdx_37;
			return (word32) rdx_37;
		}
		--rax_14;
		rax_14[1] = (struct Eq_82) 0x30;
	} while (rcx_5 <= rax_14);
	struct Eq_82 * rdx_23;
	if (rcx_5 > &g_bC010)
	{
		rcx_5->bFFFFFFFF = 0x31;
		line_num_start = rcx_5 - 1;
		rdx_23 = rcx_5 - 1;
	}
	else
	{
		g_bC010 = 0x3E;
		rdx_23 = rcx_5;
	}
	struct Eq_82 * rax_28 = line_num_print;
	if (rax_28 > rdx_23)
		line_num_print = rax_28 - 0x01;
	return (word32) rdx_23;
}