void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn00000000000026A0(fn0000000000002460(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l000000000000365E;
	}
	fn0000000000002610(fn0000000000002460(0x05, "Usage: %s [OPTION]... [FILE]...\n", null), 0x01);
	fn0000000000002550(stdout, fn0000000000002460(0x05, "Concatenate FILE(s) to standard output.\n", null));
	fn0000000000002550(stdout, fn0000000000002460(0x05, "\nWith no FILE, or when FILE is -, read standard input.\n", null));
	fn0000000000002550(stdout, fn0000000000002460(0x05, "\n  -A, --show-all           equivalent to -vET\n  -b, --number-nonblank    number nonempty output lines, overrides -n\n  -e                       equivalent to -vE\n  -E, --show-ends          display $ at end of each line\n  -n, --number             number all output lines\n  -s, --squeeze-blank      suppress repeated empty output lines\n", null));
	fn0000000000002550(stdout, fn0000000000002460(0x05, "  -t                       equivalent to -vT\n  -T, --show-tabs          display TAB characters as ^I\n  -u                       (ignored)\n  -v, --show-nonprinting   use ^ and M- notation, except for LFD and TAB\n", null));
	fn0000000000002550(stdout, fn0000000000002460(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002550(stdout, fn0000000000002460(0x05, "      --version     output version information and exit\n", null));
	fn0000000000002610(fn0000000000002460(0x05, "\nExamples:\n  %s f - g  Output f's contents, then standard input, then g's contents.\n  %s        Copy standard input to standard output.\n", null), 0x01);
	struct Eq_1563 * rbx_199 = fp - 0xB8 + 16;
	do
	{
		char * rsi_201 = rbx_199->qw0000;
		++rbx_199;
	} while (rsi_201 != null && fn0000000000002570(rsi_201, "cat") != 0x00);
	ptr64 r13_214 = rbx_199->qw0008;
	if (r13_214 != 0x00)
	{
		fn0000000000002610(fn0000000000002460(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_301 = fn0000000000002600(null, 0x05);
		if (rax_301 == 0x00 || fn00000000000023D0(0x03, "en_", rax_301) == 0x00)
			goto l00000000000038B6;
	}
	else
	{
		fn0000000000002610(fn0000000000002460(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_243 = fn0000000000002600(null, 0x05);
		if (rax_243 == 0x00 || fn00000000000023D0(0x03, "en_", rax_243) == 0x00)
		{
			fn0000000000002610(fn0000000000002460(0x05, "Full documentation <%s%s>\n", null), 0x01);
l00000000000038F3:
			fn0000000000002610(fn0000000000002460(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l000000000000365E:
			fn0000000000002680(edi);
		}
		r13_214 = 0x8010;
	}
	fn0000000000002550(stdout, fn0000000000002460(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l00000000000038B6:
	fn0000000000002610(fn0000000000002460(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l00000000000038F3;
}