copy_cat (void)
{
  /* Copy at most COPY_MAX bytes at a time; this is min
     (SSIZE_MAX, SIZE_MAX) truncated to a value that is
     surely aligned well.  */
  ssize_t copy_max = MIN (SSIZE_MAX, SIZE_MAX) >> 30 << 30;

  /* copy_file_range does not support some cases, and it
     incorrectly returns 0 when reading from the proc file
     system on the Linux kernel through at least 5.6.19 (2020),
     so fall back on read+write if the copy_file_range is
     unsupported or the input file seems empty.  */

  for (bool some_copied = false; ; some_copied = true)
    switch (copy_file_range (input_desc, NULL, STDOUT_FILENO, NULL,
                             copy_max, 0))
      {
      case 0:
        return some_copied;

      case -1:
        if (errno == ENOSYS || is_ENOTSUP (errno) || errno == EINVAL
            || errno == EBADF || errno == EXDEV || errno == ETXTBSY
            || errno == EPERM)
          return 0;
        error (0, errno, "%s", quotef (infile));
        return -1;
      }
}