write_pending (char *outbuf, char **bpout)
{
  idx_t n_write = *bpout - outbuf;
  if (0 < n_write)
    {
      if (full_write (STDOUT_FILENO, outbuf, n_write) != n_write)
        die (EXIT_FAILURE, errno, _("write error"));
      *bpout = outbuf;
    }
}