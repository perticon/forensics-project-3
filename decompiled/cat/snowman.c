
struct s0 {
    struct s0* f0;
    unsigned char f1;
    signed char f2;
    signed char[5] pad8;
    struct s0* f8;
    signed char[15] pad24;
    struct s0* f18;
};

struct s0* line_num_start = reinterpret_cast<struct s0*>(33);

signed char line_buf = 32;

struct s0* line_num_print = reinterpret_cast<struct s0*>(28);

void next_line_num(struct s0* rdi, struct s0* rsi) {
    struct s0* rcx3;
    struct s0* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s0* rdx7;
    struct s0* rax8;

    rcx3 = line_num_start;
    rax4 = reinterpret_cast<struct s0*>("0\t");
    do {
        edx5 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rax4->f0));
        if (*reinterpret_cast<signed char*>(&edx5) <= 56) 
            break;
        rax4 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax4) - 1);
        rax4->f1 = 48;
    } while (reinterpret_cast<unsigned char>(rcx3) <= reinterpret_cast<unsigned char>(rax4));
    goto addr_3580_4;
    edx6 = edx5 + 1;
    *reinterpret_cast<struct s0**>(&rax4->f0) = *reinterpret_cast<struct s0**>(&edx6);
    return;
    addr_3580_4:
    if (reinterpret_cast<unsigned char>(rcx3) <= reinterpret_cast<unsigned char>("                 0\t")) {
        line_buf = 62;
        rdx7 = rcx3;
    } else {
        rdx7 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rcx3) + 0xffffffffffffffff);
        *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rcx3) + 0xffffffffffffffff) = 49;
        line_num_start = rdx7;
    }
    rax8 = line_num_print;
    if (reinterpret_cast<unsigned char>(rax8) > reinterpret_cast<unsigned char>(rdx7)) {
        line_num_print = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax8) - 1);
    }
    return;
}

struct s0* fun_2460();

struct s0* fun_23c0();

uint32_t fun_2630();

/* write_pending.part.0 */
void write_pending_part_0(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, int64_t r8, int64_t r9) {
    fun_2460();
    fun_23c0();
    fun_2630();
}

int64_t fun_2470();

int64_t fun_23b0(struct s0* rdi, ...);

struct s0* quotearg_buffer_restyled(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, uint32_t r8d, uint32_t r9d, struct s0* a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2470();
    if (r8d > 10) {
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x87e0 + rax11 * 4) + 0x87e0;
    }
}

struct s1 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* g28;

struct s0* slotvec = reinterpret_cast<struct s0*>(0xb0);

uint32_t nslots = 1;

struct s0* xpalloc();

void fun_24e0();

struct s2 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
};

void fun_23a0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, int64_t r8, int64_t r9);

struct s0* xcharalloc(struct s0* rdi, ...);

void fun_2490();

struct s0* quotearg_n_options(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s1* rcx, ...) {
    int64_t rbx5;
    struct s0* rax6;
    int64_t v7;
    struct s0* rax8;
    struct s0* r15_9;
    struct s0* v10;
    uint32_t eax11;
    struct s0* rax12;
    struct s0* rax13;
    int64_t rax14;
    int64_t r8_15;
    struct s2* rbx16;
    uint32_t r15d17;
    struct s0* rsi18;
    struct s0* r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    int64_t r9_23;
    struct s0* rax24;
    struct s0* rsi25;
    struct s0* rax26;
    uint32_t r8d27;
    int64_t v28;
    int64_t v29;
    void* rax30;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x514f;
    rax8 = fun_23c0();
    r15_9 = slotvec;
    v10 = *reinterpret_cast<struct s0**>(&rax8->f0);
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
        fun_23b0(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xc0b0) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x6dc1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x51db;
            fun_24e0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        *reinterpret_cast<uint32_t*>(&r8_15) = rcx->f0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_15) + 4) = 0;
        rbx16 = reinterpret_cast<struct s2*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        *reinterpret_cast<uint32_t*>(&r9_23) = r15d22;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_23) + 4) = 0;
        rax24 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, *reinterpret_cast<uint32_t*>(&r8_15), *reinterpret_cast<uint32_t*>(&r9_23), &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax24)) {
            rsi25 = reinterpret_cast<struct s0*>(&rax24->f1);
            rbx16->f0 = rsi25;
            if (r14_19 != 0xc120) {
                fun_23a0(r14_19, rsi25, rsi, rdx, r8_15, r9_23);
                rsi25 = rsi25;
            }
            rax26 = xcharalloc(rsi25, rsi25);
            r8d27 = rcx->f0;
            rbx16->f8 = rax26;
            v28 = rcx->f30;
            r14_19 = rax26;
            v29 = rcx->f28;
            quotearg_buffer_restyled(rax26, rsi25, rsi, rdx, r8d27, r15d22, rsi25, v29, v28, 0x526a);
        }
        *reinterpret_cast<struct s0**>(&rax8->f0) = v10;
        rax30 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax30) {
            fun_2490();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xc0c0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s3 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s3* locale_charset();

/* gettext_quote.part.0 */
struct s0* gettext_quote_part_0(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s3* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s0* rax7;
    uint32_t edx8;
    uint32_t edx9;
    struct s0* rax10;
    struct s0* rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<struct s0*>(0x877b);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax7 = reinterpret_cast<struct s0*>(0x8774);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<struct s0*>(0x877f);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax10 = reinterpret_cast<struct s0*>(0x8770);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<struct s0*>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<struct s0*>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gbe18 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gbe18;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2393() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t free = 0x2030;

void fun_23a3() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2040;

void fun_23b3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2050;

void fun_23c3() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2060;

void fun_23d3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2070;

void fun_23e3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2080;

void fun_23f3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x2090;

void fun_2403() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t write = 0x20a0;

void fun_2413() {
    __asm__("cli ");
    goto write;
}

int64_t textdomain = 0x20b0;

void fun_2423() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20c0;

void fun_2433() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20d0;

void fun_2443() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t stpcpy = 0x20e0;

void fun_2453() {
    __asm__("cli ");
    goto stpcpy;
}

int64_t dcgettext = 0x20f0;

void fun_2463() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2100;

void fun_2473() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2110;

void fun_2483() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2120;

void fun_2493() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2130;

void fun_24a3() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2140;

void fun_24b3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strrchr = 0x2150;

void fun_24c3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2160;

void fun_24d3() {
    __asm__("cli ");
    goto lseek;
}

int64_t memset = 0x2170;

void fun_24e3() {
    __asm__("cli ");
    goto memset;
}

int64_t ioctl = 0x2180;

void fun_24f3() {
    __asm__("cli ");
    goto ioctl;
}

int64_t copy_file_range = 0x2190;

void fun_2503() {
    __asm__("cli ");
    goto copy_file_range;
}

int64_t close = 0x21a0;

void fun_2513() {
    __asm__("cli ");
    goto close;
}

int64_t posix_fadvise = 0x21b0;

void fun_2523() {
    __asm__("cli ");
    goto posix_fadvise;
}

int64_t read = 0x21c0;

void fun_2533() {
    __asm__("cli ");
    goto read;
}

int64_t memcmp = 0x21d0;

void fun_2543() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21e0;

void fun_2553() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21f0;

void fun_2563() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2200;

void fun_2573() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2210;

void fun_2583() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t memcpy = 0x2220;

void fun_2593() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2230;

void fun_25a3() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2240;

void fun_25b3() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2250;

void fun_25c3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2260;

void fun_25d3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2270;

void fun_25e3() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x2280;

void fun_25f3() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2290;

void fun_2603() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22a0;

void fun_2613() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t memmove = 0x22b0;

void fun_2623() {
    __asm__("cli ");
    goto memmove;
}

int64_t error = 0x22c0;

void fun_2633() {
    __asm__("cli ");
    goto error;
}

int64_t open = 0x22d0;

void fun_2643() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x22e0;

void fun_2653() {
    __asm__("cli ");
    goto fseeko;
}

int64_t __cxa_atexit = 0x22f0;

void fun_2663() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t getpagesize = 0x2300;

void fun_2673() {
    __asm__("cli ");
    goto getpagesize;
}

int64_t exit = 0x2310;

void fun_2683() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2320;

void fun_2693() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2330;

void fun_26a3() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t aligned_alloc = 0x2340;

void fun_26b3() {
    __asm__("cli ");
    goto aligned_alloc;
}

int64_t mbsinit = 0x2350;

void fun_26c3() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2360;

void fun_26d3() {
    __asm__("cli ");
    goto iswprint;
}

int64_t fstat = 0x2370;

void fun_26e3() {
    __asm__("cli ");
    goto fstat;
}

int64_t __ctype_b_loc = 0x2380;

void fun_26f3() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(struct s0* rdi);

struct s0* fun_2600(int64_t rdi, ...);

void fun_2440(int64_t rdi, int64_t rsi);

void fun_2420(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int32_t fun_24a0(int64_t rdi, struct s0** rsi, struct s0* rdx, struct s0* rcx);

int32_t fun_26e0(int64_t rdi, void* rsi, struct s0* rdx, struct s0* rcx, int64_t r8, int64_t r9);

int32_t optind = 0;

struct s0* infile = reinterpret_cast<struct s0*>(0);

int32_t fun_2670(int64_t rdi, void* rsi);

struct s0* g8110 = reinterpret_cast<struct s0*>(45);

int64_t stdout = 0;

struct s0* Version = reinterpret_cast<struct s0*>(28);

void version_etc(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8);

int32_t fun_2680();

void usage();

signed char pending_cr = 0;

struct s0* fun_2450(struct s0* rdi, struct s0* rsi);

int32_t input_desc = 0;

int32_t fun_2640();

struct s0* quotearg_n_style_colon();

void fdadvise(int64_t rdi);

int64_t fun_24d0(int64_t rdi, ...);

int32_t fun_2510();

struct s0* xalignalloc(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, int64_t r8, int64_t r9);

int32_t newlines2 = 0;

int64_t fun_2500(int64_t rdi);

struct s0* full_write(int64_t rdi, struct s0* rsi, struct s0* rdx, ...);

void fun_2620(struct s0* rdi, struct s0* rsi);

int32_t fun_24f0(int64_t rdi, int64_t rsi, void* rdx);

struct s0* safe_read(int64_t rdi, struct s0* rsi, struct s0* rdx, ...);

struct s0* quotearg_style(int64_t rdi, struct s0* rsi);

void xalloc_die();

int64_t fun_2743(int32_t edi, struct s0** rsi) {
    struct s0* r13_3;
    struct s0* rbx4;
    int32_t v5;
    struct s0* rdi6;
    struct s0** v7;
    struct s0* rax8;
    struct s0* v9;
    int64_t r8_10;
    struct s0* rcx11;
    struct s0* rdx12;
    int64_t rdi13;
    int32_t eax14;
    void* rsp15;
    void* rax16;
    void* v17;
    int32_t eax18;
    void* rsp19;
    struct s0* v20;
    int32_t r12d21;
    struct s0* rdx22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    uint32_t v27;
    uint32_t v28;
    int32_t eax29;
    void* rsp30;
    unsigned char v31;
    signed char v32;
    int64_t v33;
    struct s0* v34;
    struct s0* rdi35;
    uint32_t eax36;
    uint32_t eax37;
    uint32_t v38;
    int64_t rdi39;
    int64_t r9_40;
    int64_t rax41;
    int64_t rax42;
    struct s0* rdi43;
    struct s0* v44;
    struct s0* r14_45;
    struct s0* v46;
    struct s0* rax47;
    struct s0* rax48;
    struct s0* v49;
    struct s0* rax50;
    uint32_t ebx51;
    int1_t zf52;
    int32_t r12d53;
    struct s0* rsi54;
    struct s0* rax55;
    struct s0** rsp56;
    uint32_t eax57;
    uint32_t ebx58;
    uint32_t ebx59;
    uint32_t ebx60;
    struct s0* v61;
    struct s0* rax62;
    int1_t zf63;
    uint32_t eax64;
    uint32_t eax65;
    uint32_t eax66;
    int32_t r12d67;
    int32_t eax68;
    struct s0* rax69;
    int64_t rdi70;
    int32_t eax71;
    void* rsp72;
    struct s0* rax73;
    int64_t rdi74;
    struct s0* rax75;
    struct s0* v76;
    struct s0* v77;
    void* rsp78;
    int64_t v79;
    int64_t v80;
    int64_t rdi81;
    int64_t rax82;
    int64_t v83;
    int32_t eax84;
    struct s0* r12_85;
    struct s0* rax86;
    void* rsp87;
    struct s0* rax88;
    struct s0* rbx89;
    unsigned char v90;
    struct s0* v91;
    struct s0* r15_92;
    uint32_t v93;
    int64_t rdi94;
    struct s0* rsi95;
    int64_t rax96;
    struct s0* rax97;
    struct s0* rax98;
    struct s0* rdx99;
    void* rsp100;
    int64_t rdi101;
    int32_t eax102;
    struct s0* rax103;
    int64_t rax104;
    int64_t rdi105;
    struct s0* rax106;
    struct s0* rax107;
    struct s0* rax108;
    struct s0* rax109;
    struct s0* rax110;
    struct s0* rbx111;
    struct s0* rax112;
    struct s0* rsi113;
    struct s0* rax114;
    struct s0* rax115;
    struct s0* rax116;
    uint32_t eax117;
    uint64_t rax118;
    struct s0* rax119;
    struct s0* rax120;
    struct s0* rax121;
    struct s0* rax122;
    int32_t eax123;
    uint32_t eax124;
    void* rdx125;
    struct s0* rax126;
    struct s0* rax127;
    struct s0* rax128;
    struct s0* rax129;
    struct s0* rax130;
    struct s0* r15_131;
    int64_t rdi132;
    struct s0* rax133;
    struct s0* rax134;
    struct s0* rax135;
    int1_t zf136;
    struct s0* rax137;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r13_3) = edi;
    *reinterpret_cast<int32_t*>(&r13_3 + 4) = 0;
    rbx4 = reinterpret_cast<struct s0*>(0x8644);
    v5 = edi;
    rdi6 = *rsi;
    v7 = rsi;
    rax8 = g28;
    v9 = rax8;
    set_program_name(rdi6);
    fun_2600(6, 6);
    fun_2440("coreutils", "/usr/local/share/locale");
    fun_2420("coreutils", "/usr/local/share/locale");
    atexit(0x3a00, "/usr/local/share/locale");
    *reinterpret_cast<int32_t*>(&r8_10) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_10) + 4) = 0;
    rcx11 = reinterpret_cast<struct s0*>(0xba80);
    rdx12 = reinterpret_cast<struct s0*>("benstuvAET");
    *reinterpret_cast<int32_t*>(&rdi13) = *reinterpret_cast<int32_t*>(&r13_3);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi13) + 4) = 0;
    eax14 = fun_24a0(rdi13, rsi, "benstuvAET", 0xba80);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x138 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    if (eax14 == -1) {
        addr_28ca_3:
        rax16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) + 0x90);
        v17 = rax16;
        eax18 = fun_26e0(1, rax16, rdx12, rcx11, r8_10, "Richard M. Stallman");
        rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
        if (eax18 < 0) 
            goto addr_3337_4;
    } else {
        if (eax14 > 0x76) 
            goto addr_332d_6;
        if (eax14 <= 64) 
            goto addr_2873_8; else 
            goto addr_2815_9;
    }
    r13_3 = v20;
    rbx4 = reinterpret_cast<struct s0*>("-");
    r12d21 = optind;
    infile = reinterpret_cast<struct s0*>("-");
    rdx22 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r13_3) + 0xfffffffffffe0000);
    if (reinterpret_cast<unsigned char>(rdx22) > reinterpret_cast<unsigned char>(0x1ffffffffffe0000)) {
        r13_3 = reinterpret_cast<struct s0*>(0x20000);
    }
    v23 = v24;
    v25 = v26;
    v27 = v28 & 0xf000;
    eax29 = fun_2670(1, rax16);
    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
    v31 = 1;
    v32 = 0;
    v33 = eax29;
    v34 = reinterpret_cast<struct s0*>(static_cast<int64_t>(r12d21));
    if (v5 > r12d21) 
        goto addr_2a4b_13;
    rdi35 = reinterpret_cast<struct s0*>("-");
    eax36 = reinterpret_cast<unsigned char>(g8110);
    eax37 = eax36 - 45;
    v38 = eax37;
    if (eax37) 
        goto addr_2a70_15; else 
        goto addr_2999_16;
    addr_2873_8:
    if (eax14 == 0xffffff7d) {
        rdi39 = stdout;
        rcx11 = Version;
        r9_40 = reinterpret_cast<int64_t>("Richard M. Stallman");
        r8_10 = reinterpret_cast<int64_t>("Torbjorn Granlund");
        rdx12 = reinterpret_cast<struct s0*>("GNU coreutils");
        version_etc(rdi39, "cat", "GNU coreutils", rcx11, "Torbjorn Granlund", "Richard M. Stallman", 0, 0x27fe);
        eax14 = fun_2680();
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 - 8 - 8 + 8 - 8 + 8);
    }
    if (eax14 != 0xffffff7e) 
        goto addr_332d_6;
    usage();
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
    goto addr_28ca_3;
    addr_2815_9:
    *reinterpret_cast<uint32_t*>(&rax41) = eax14 - 65;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax41) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax41) > 53) 
        goto addr_332d_6;
    goto static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(0x8644) + reinterpret_cast<uint64_t>(rax41 * 4)))) + reinterpret_cast<unsigned char>(0x8644);
    addr_32ef_21:
    return rax42;
    while (1) {
        addr_2bf8_22:
        rdi43 = v44;
        r14_45 = v46;
        rax47 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax48) + reinterpret_cast<unsigned char>(rdi43));
        v49 = rax47;
        *reinterpret_cast<struct s0**>(&rax47->f0) = reinterpret_cast<struct s0*>(10);
        rax50 = rdi43;
        while (1) {
            ebx51 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rax50->f0));
            while (1) {
                if (*reinterpret_cast<struct s0**>(&ebx51) == 10) 
                    goto addr_2b90_25;
                zf52 = pending_cr == 0;
                if (!zf52) {
                    pending_cr = 0;
                    r13_3 = reinterpret_cast<struct s0*>(&r13_3->f1);
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r13_3) + 0xffffffffffffffff) = 13;
                }
                if (r12d53 >= 0 && !1) {
                    next_line_num(rdi43, rsi54);
                    rsi54 = line_num_print;
                    rdi43 = r13_3;
                    rax55 = fun_2450(rdi43, rsi54);
                    rsp56 = rsp56 - 8 + 8 - 8 + 8;
                    r13_3 = rax55;
                }
                if (1) {
                    rcx11 = reinterpret_cast<struct s0*>(0);
                    *reinterpret_cast<int32_t*>(&rcx11 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rsi54) = 0;
                    *reinterpret_cast<int32_t*>(&rsi54 + 4) = 0;
                    while (1) {
                        if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&ebx51) == 9) || !0) {
                            if (*reinterpret_cast<struct s0**>(&ebx51) == 10) 
                                break;
                            eax57 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r14_45->f0));
                            if (*reinterpret_cast<struct s0**>(&ebx51) != 13) 
                                goto addr_2e8c_35;
                            *reinterpret_cast<unsigned char*>(&rdx22) = 0;
                            if (!1) 
                                goto addr_2ea9_37;
                        } else {
                            *reinterpret_cast<int32_t*>(&rdi43) = 0x495e;
                            *reinterpret_cast<int32_t*>(&rdi43 + 4) = 0;
                            eax57 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r14_45->f0));
                            r13_3 = reinterpret_cast<struct s0*>(&r13_3->f2);
                            *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(r13_3) + 0xfffffffffffffffe) = 0x495e;
                            goto addr_2e6a_39;
                        }
                        addr_2e8c_35:
                        *reinterpret_cast<struct s0**>(&r13_3->f0) = *reinterpret_cast<struct s0**>(&ebx51);
                        r13_3 = reinterpret_cast<struct s0*>(&r13_3->f1);
                        addr_2e6a_39:
                        r14_45 = reinterpret_cast<struct s0*>(&r14_45->f1);
                        ebx51 = eax57;
                        continue;
                        addr_2ea9_37:
                        if (r14_45 == v49) {
                            *reinterpret_cast<int32_t*>(&rsi54) = *reinterpret_cast<int32_t*>(&rdx22);
                            *reinterpret_cast<int32_t*>(&rsi54 + 4) = 0;
                            eax57 = 10;
                            goto addr_2e6a_39;
                        } else {
                            *reinterpret_cast<int32_t*>(&rdx22) = 0x4d5e;
                            r13_3 = reinterpret_cast<struct s0*>(&r13_3->f2);
                            eax57 = 10;
                            *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(r13_3) + 0xfffffffffffffffe) = 0x4d5e;
                            goto addr_2e6a_39;
                        }
                    }
                    if (!0) 
                        goto addr_2d29_43;
                } else {
                    while (1) {
                        if (reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&ebx51)) > 31) {
                            if (reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&ebx51)) > 0x7e) {
                                if (*reinterpret_cast<struct s0**>(&ebx51) == 0x7f) {
                                    r13_3 = reinterpret_cast<struct s0*>(&r13_3->f2);
                                    *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(r13_3) + 0xfffffffffffffffe) = 0x3f5e;
                                } else {
                                    *reinterpret_cast<int32_t*>(&r9_40) = reinterpret_cast<int32_t>("\r");
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_40) + 4) = 0;
                                    *reinterpret_cast<struct s0**>(&r13_3->f0) = reinterpret_cast<struct s0*>("\r");
                                    if (reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&ebx51)) <= 0x9f) {
                                        ebx58 = ebx51 - 64;
                                        r13_3->f2 = 94;
                                        r13_3 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r13_3) + 4);
                                        *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r13_3) + 0xffffffffffffffff) = *reinterpret_cast<unsigned char*>(&ebx58);
                                    } else {
                                        if (*reinterpret_cast<struct s0**>(&ebx51) == 0xff) {
                                            *reinterpret_cast<int32_t*>(&r8_10) = 0x3f5e;
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_10) + 4) = 0;
                                            r13_3 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r13_3) + 4);
                                            *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(r13_3) + 0xfffffffffffffffe) = 0x3f5e;
                                        } else {
                                            ebx59 = ebx51 - 0x80;
                                            r13_3 = reinterpret_cast<struct s0*>(&r13_3->pad8);
                                            *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r13_3) + 0xffffffffffffffff) = *reinterpret_cast<unsigned char*>(&ebx59);
                                        }
                                    }
                                }
                            } else {
                                *reinterpret_cast<struct s0**>(&r13_3->f0) = *reinterpret_cast<struct s0**>(&ebx51);
                                r13_3 = reinterpret_cast<struct s0*>(&r13_3->f1);
                            }
                        } else {
                            if (static_cast<int1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&ebx51) == 9))) {
                                if (*reinterpret_cast<struct s0**>(&ebx51) == 10) 
                                    goto addr_2d29_43;
                                ebx60 = ebx51 + 64;
                                *reinterpret_cast<struct s0**>(&r13_3->f0) = reinterpret_cast<struct s0*>(94);
                                r13_3 = reinterpret_cast<struct s0*>(&r13_3->f2);
                                *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r13_3) + 0xffffffffffffffff) = *reinterpret_cast<unsigned char*>(&ebx60);
                            } else {
                                *reinterpret_cast<struct s0**>(&r13_3->f0) = reinterpret_cast<struct s0*>(9);
                                r13_3 = reinterpret_cast<struct s0*>(&r13_3->f1);
                            }
                        }
                        ebx51 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r14_45->f0));
                        r14_45 = reinterpret_cast<struct s0*>(&r14_45->f1);
                    }
                }
                pending_cr = 1;
                r12d53 = -1;
                addr_2d2f_61:
                if (reinterpret_cast<unsigned char>(v61) > reinterpret_cast<unsigned char>(r13_3)) 
                    goto addr_2b9b_62; else 
                    goto addr_2d3a_63;
                addr_2d29_43:
                r12d53 = -1;
                goto addr_2d2f_61;
                addr_2d8f_64:
                ++r12d53;
                rdx22 = reinterpret_cast<struct s0*>(&r14_45->f1);
                if (reinterpret_cast<uint1_t>(r12d53 < 0) | reinterpret_cast<uint1_t>(r12d53 == 0)) 
                    break;
                if (r12d53 == 1) 
                    goto addr_2db3_66;
                r12d53 = 2;
                if (!0) 
                    goto addr_2db3_66;
                ebx51 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r14_45->f0));
                r14_45 = rdx22;
                continue;
                while (1) {
                    v34 = reinterpret_cast<struct s0*>(&v34->f1);
                    rax62 = v34;
                    if (v5 <= *reinterpret_cast<int32_t*>(&rax62)) {
                        zf63 = pending_cr == 0;
                        if (!zf63) 
                            goto addr_33d4_71; else 
                            goto addr_32c2_72;
                    }
                    addr_2a4b_13:
                    rcx11 = v34;
                    rdi35 = v7[reinterpret_cast<unsigned char>(rcx11) * 8];
                    eax64 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi35->f0));
                    infile = rdi35;
                    eax65 = eax64 - 45;
                    v38 = eax65;
                    if (!eax65 && (eax66 = rdi35->f1, v38 = eax66, !eax66)) {
                        input_desc = 0;
                        r12d67 = 0;
                        v32 = 1;
                        goto addr_2a8a_74;
                    }
                    addr_2a70_15:
                    eax68 = fun_2640();
                    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                    input_desc = eax68;
                    r12d67 = eax68;
                    if (eax68 < 0) {
                        addr_2ff5_75:
                        rax69 = quotearg_n_style_colon();
                        fun_23c0();
                        rcx11 = rax69;
                        rdx22 = reinterpret_cast<struct s0*>("%s");
                        fun_2630();
                        rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8 - 8 + 8 - 8 + 8);
                        v31 = 0;
                        continue;
                    } else {
                        addr_2a8a_74:
                        *reinterpret_cast<int32_t*>(&rdi70) = r12d67;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi70) + 4) = 0;
                        eax71 = fun_26e0(rdi70, v17, rdx22, rcx11, r8_10, r9_40);
                        rsp72 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                        if (eax71 < 0) {
                            rax73 = quotearg_n_style_colon();
                            fun_23c0();
                            rcx11 = rax73;
                            rdx22 = reinterpret_cast<struct s0*>("%s");
                            fun_2630();
                            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp72) - 8 + 8 - 8 + 8 - 8 + 8);
                            v31 = 0;
                        } else {
                            *reinterpret_cast<int32_t*>(&rdi74) = r12d67;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi74) + 4) = 0;
                            *reinterpret_cast<int32_t*>(&rax75) = 0x20000;
                            *reinterpret_cast<int32_t*>(&rax75 + 4) = 0;
                            if (reinterpret_cast<unsigned char>(v76) + 0xfffffffffffe0000 <= 0x1ffffffffffe0000) {
                                rax75 = v76;
                            }
                            *reinterpret_cast<int32_t*>(&rdx22) = 0;
                            *reinterpret_cast<int32_t*>(&rdx22 + 4) = 0;
                            rcx11 = reinterpret_cast<struct s0*>(2);
                            *reinterpret_cast<int32_t*>(&rcx11 + 4) = 0;
                            v77 = rax75;
                            fdadvise(rdi74);
                            rsp78 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp72) - 8 + 8);
                            if (v27 != 0x8000) 
                                goto addr_2ae8_80;
                            if (v79 != v23) 
                                goto addr_2ae8_80;
                            if (v80 != v25) 
                                goto addr_2ae8_80;
                            *reinterpret_cast<int32_t*>(&rdi81) = input_desc;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi81) + 4) = 0;
                            *reinterpret_cast<int32_t*>(&rdx22) = 1;
                            *reinterpret_cast<int32_t*>(&rdx22 + 4) = 0;
                            rax82 = fun_24d0(rdi81);
                            rsp78 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp78) - 8 + 8);
                            if (rax82 >= v83) 
                                goto addr_2ae8_80; else 
                                goto addr_30ae_84;
                        }
                    }
                    addr_2a2a_85:
                    if (!v38) 
                        continue;
                    eax84 = fun_2510();
                    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                    if (eax84 >= 0) 
                        continue; else 
                        goto addr_2ff5_75;
                    addr_2ae8_80:
                    if (static_cast<int1_t>(*reinterpret_cast<int32_t*>(&r12_85) = 0, !1)) {
                        rax86 = xalignalloc(v33, &v77->f1, rdx22, 2, r8_10, r9_40);
                        rsp87 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp78) - 8 + 8);
                        v44 = rax86;
                        rbx4 = rax86;
                        if (__intrinsic()) 
                            goto addr_338f_88;
                        if (__intrinsic()) 
                            goto addr_338f_88;
                        rsi54 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v77) * 4 + reinterpret_cast<unsigned char>(r13_3) + 19);
                        if (__intrinsic()) 
                            goto addr_338f_88;
                        rax88 = xalignalloc(v33, rsi54, rdx22, 2, r8_10, r9_40);
                        rsp56 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rsp87) - 8 + 8);
                        rcx11 = rbx4;
                        rbx89 = reinterpret_cast<struct s0*>(&rbx4->f1);
                        v90 = 1;
                        rdi43 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax88) + reinterpret_cast<unsigned char>(r13_3));
                        v91 = rax88;
                        r14_45 = rbx89;
                        r12d53 = newlines2;
                        v46 = rbx89;
                        v61 = rdi43;
                        v49 = rcx11;
                        r15_92 = r13_3;
                        r13_3 = rax88;
                        addr_2b90_25:
                        if (reinterpret_cast<unsigned char>(v61) > reinterpret_cast<unsigned char>(r13_3)) 
                            goto addr_2b9b_62;
                    } else {
                        if (v27 != 0x8000) 
                            goto addr_314f_93;
                        *reinterpret_cast<int32_t*>(&rbx4) = 0;
                        if ((v93 & 0xf000) != 0x8000) 
                            goto addr_314f_93;
                        while (*reinterpret_cast<int32_t*>(&rdi94) = input_desc, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi94) + 4) = 0, *reinterpret_cast<int32_t*>(&r9_40) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_40) + 4) = 0, rcx11 = reinterpret_cast<struct s0*>(0), *reinterpret_cast<int32_t*>(&rcx11 + 4) = 0, rsi95 = reinterpret_cast<struct s0*>(0), *reinterpret_cast<int32_t*>(&rsi95 + 4) = 0, r8_10 = 0x7fffffffc0000000, *reinterpret_cast<int32_t*>(&rdx22) = 1, *reinterpret_cast<int32_t*>(&rdx22 + 4) = 0, rax96 = fun_2500(rdi94), rsp78 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp78) - 8 + 8), rax96 != -1) {
                            if (!rax96) 
                                goto addr_3420_97;
                            *reinterpret_cast<int32_t*>(&rbx4) = 1;
                        }
                        rax97 = fun_23c0();
                        rsp78 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp78) - 8 + 8);
                        rcx11 = *reinterpret_cast<struct s0**>(&rax97->f0);
                        *reinterpret_cast<int32_t*>(&rcx11 + 4) = 0;
                        rbx4 = rax97;
                        if (rcx11 == 38) 
                            goto addr_314f_93;
                        if (reinterpret_cast<signed char>(rcx11) > reinterpret_cast<signed char>(26)) 
                            goto addr_3444_101; else 
                            goto addr_3271_102;
                    }
                    addr_2d3a_63:
                    rbx4 = v91;
                    do {
                        rax98 = full_write(1, rbx4, r15_92);
                        rsp56 = rsp56 - 8 + 8;
                        if (rax98 != r15_92) 
                            break;
                        rbx4 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx4) + reinterpret_cast<unsigned char>(r15_92));
                        rdx99 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r13_3) - reinterpret_cast<unsigned char>(rbx4));
                    } while (reinterpret_cast<signed char>(r15_92) <= reinterpret_cast<signed char>(rdx99));
                    goto addr_2d67_105;
                    fun_2460();
                    fun_23c0();
                    fun_2630();
                    rsp15 = reinterpret_cast<void*>(rsp56 - 8 + 8 - 8 + 8 - 8 + 8);
                    addr_332d_6:
                    usage();
                    rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
                    addr_3337_4:
                    fun_2460();
                    fun_23c0();
                    fun_2630();
                    rsp100 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8);
                    goto addr_3363_107;
                    addr_2d67_105:
                    rsi54 = rbx4;
                    rdi43 = v91;
                    fun_2620(rdi43, rsi54);
                    rsp56 = rsp56 - 8 + 8;
                    r13_3 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v91) + reinterpret_cast<unsigned char>(rdx99));
                    if (reinterpret_cast<unsigned char>(v49) >= reinterpret_cast<unsigned char>(r14_45)) 
                        goto addr_2d8f_64;
                    addr_2ba5_108:
                    if (!v90) 
                        goto addr_2bbb_109;
                    *reinterpret_cast<int32_t*>(&r14_45) = input_desc;
                    *reinterpret_cast<int32_t*>(&rdi101) = *reinterpret_cast<int32_t*>(&r14_45);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi101) + 4) = 0;
                    eax102 = fun_24f0(rdi101, 0x541b, rsp56 + 0x8c);
                    rsp56 = rsp56 - 8 + 8;
                    if (eax102 >= 0) 
                        goto addr_2f02_111;
                    while (1) {
                        rax103 = fun_23c0();
                        rsp56 = rsp56 - 8 + 8;
                        rbx4 = rax103;
                        *reinterpret_cast<struct s0**>(&rax104) = *reinterpret_cast<struct s0**>(&rax103->f0);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax104) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&rcx11) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<struct s0**>(&rax104) == 95)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<struct s0**>(&rax104) == 25)));
                        v90 = *reinterpret_cast<unsigned char*>(&rcx11);
                        if (*reinterpret_cast<unsigned char*>(&rcx11)) {
                            v90 = 0;
                        } else {
                            if (reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rax104)) > reinterpret_cast<unsigned char>(38)) 
                                goto addr_2f7b_115;
                            rcx11 = reinterpret_cast<struct s0*>(0x4000480000);
                            if (!static_cast<int1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(0x4000480000) >> rax104))) 
                                goto addr_2f7b_115;
                        }
                        addr_2f02_111:
                        if (0) {
                            addr_2bd3_117:
                            rdx22 = v77;
                            rsi54 = v44;
                            *reinterpret_cast<int32_t*>(&rdi105) = *reinterpret_cast<int32_t*>(&r14_45);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi105) + 4) = 0;
                            rax48 = safe_read(rdi105, rsi54, rdx22);
                            rsp56 = rsp56 - 8 + 8;
                            if (rax48 == 0xffffffffffffffff) {
                                rax106 = r13_3;
                                r13_3 = r15_92;
                                rax107 = quotearg_n_style_colon();
                                rbx4 = rax107;
                                fun_23c0();
                                rcx11 = rbx4;
                                rdx22 = reinterpret_cast<struct s0*>("%s");
                                fun_2630();
                                rsp56 = rsp56 - 8 + 8 - 8 + 8 - 8 + 8;
                                rsi95 = v91;
                                r15_92 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax106) - reinterpret_cast<unsigned char>(rsi95));
                                if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r15_92) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(r15_92 == 0))) 
                                    break;
                                rdx22 = r15_92;
                                rax108 = full_write(1, rsi95, rdx22, 1, rsi95, rdx22);
                                rsp56 = rsp56 - 8 + 8;
                                if (r15_92 == rax108) 
                                    break;
                            } else {
                                if (rax48) 
                                    goto addr_2bf8_22;
                                rax109 = r13_3;
                                rsi95 = v91;
                                r13_3 = r15_92;
                                rbx4 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax109) - reinterpret_cast<unsigned char>(rsi95));
                                if (reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rbx4) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rbx4 == 0)) 
                                    goto addr_2a0f_123;
                                rdx22 = rbx4;
                                rax110 = full_write(1, rsi95, rdx22);
                                rsp56 = rsp56 - 8 + 8;
                                if (rbx4 == rax110) 
                                    goto addr_2a0f_123;
                            }
                        } else {
                            addr_2bbb_109:
                            rbx111 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r13_3) - reinterpret_cast<unsigned char>(v91));
                            if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rbx111) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rbx111 == 0))) 
                                goto addr_2bcc_127;
                            r13_3 = v91;
                            rdx22 = rbx111;
                            rsi95 = r13_3;
                            rax112 = full_write(1, rsi95, rdx22);
                            rsp56 = rsp56 - 8 + 8;
                            if (rbx111 == rax112) 
                                goto addr_2bcc_127;
                        }
                        write_pending_part_0(1, rsi95, rdx22, rcx11, r8_10, r9_40);
                        rsp56 = rsp56 - 8 + 8;
                        continue;
                        addr_2bcc_127:
                        *reinterpret_cast<int32_t*>(&r14_45) = input_desc;
                        goto addr_2bd3_117;
                    }
                    v31 = 0;
                    addr_2a0f_123:
                    newlines2 = r12d53;
                    fun_23a0(v91, rsi95, rdx22, rcx11, r8_10, r9_40);
                    rsp78 = reinterpret_cast<void*>(rsp56 - 8 + 8);
                    addr_2a20_131:
                    fun_23a0(v44, rsi95, rdx22, rcx11, r8_10, r9_40);
                    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp78) - 8 + 8);
                    goto addr_2a2a_85;
                    addr_2f7b_115:
                    rsi113 = infile;
                    r13_3 = r15_92;
                    rax114 = quotearg_style(4, rsi113);
                    rax115 = fun_2460();
                    rsi95 = *reinterpret_cast<struct s0**>(&rbx4->f0);
                    *reinterpret_cast<int32_t*>(&rsi95 + 4) = 0;
                    rcx11 = rax114;
                    rdx22 = rax115;
                    fun_2630();
                    rsp56 = rsp56 - 8 + 8 - 8 + 8 - 8 + 8;
                    v31 = 0;
                    goto addr_2a0f_123;
                    addr_2b9b_62:
                    if (reinterpret_cast<unsigned char>(v49) >= reinterpret_cast<unsigned char>(r14_45)) 
                        goto addr_2d8f_64; else 
                        goto addr_2ba5_108;
                    addr_3444_101:
                    if (reinterpret_cast<int1_t>(rcx11 == 95)) {
                        goto addr_314f_93;
                    }
                    addr_3285_133:
                    rax116 = quotearg_n_style_colon();
                    rsi95 = *reinterpret_cast<struct s0**>(&rbx4->f0);
                    *reinterpret_cast<int32_t*>(&rsi95 + 4) = 0;
                    rdx22 = reinterpret_cast<struct s0*>("%s");
                    rcx11 = rax116;
                    fun_2630();
                    rsp78 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp78) - 8 + 8 - 8 + 8);
                    eax117 = 0xffffffff;
                    addr_342d_134:
                    v44 = reinterpret_cast<struct s0*>(0);
                    v31 = reinterpret_cast<unsigned char>(v31 & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax117) < reinterpret_cast<int32_t>(0)))))));
                    goto addr_2a20_131;
                    addr_3271_102:
                    if (reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rcx11) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rcx11 == 0)) 
                        goto addr_3285_133;
                    rax118 = 0x4440202 >> *reinterpret_cast<unsigned char*>(&rcx11);
                    if (*reinterpret_cast<unsigned char*>(&rax118) & 1) 
                        goto addr_314f_93; else 
                        goto addr_3285_133;
                    addr_30ae_84:
                    rax119 = quotearg_n_style_colon();
                    rax120 = fun_2460();
                    rcx11 = rax119;
                    rdx22 = rax120;
                    fun_2630();
                    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp78) - 8 + 8 - 8 + 8 - 8 + 8);
                    v31 = 0;
                    goto addr_2a2a_85;
                    addr_3209_136:
                    *reinterpret_cast<int32_t*>(&r12_85) = 1;
                    addr_31e4_137:
                    v31 = reinterpret_cast<unsigned char>(v31 & *reinterpret_cast<unsigned char*>(&r12_85));
                    goto addr_2a20_131;
                    addr_31b4_138:
                    rax121 = quotearg_n_style_colon();
                    rax122 = fun_23c0();
                    rcx11 = rax121;
                    rdx22 = reinterpret_cast<struct s0*>("%s");
                    rsi95 = *reinterpret_cast<struct s0**>(&rax122->f0);
                    *reinterpret_cast<int32_t*>(&rsi95 + 4) = 0;
                    fun_2630();
                    rsp78 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp78) - 8 + 8 - 8 + 8 - 8 + 8);
                    goto addr_31e4_137;
                    while (1) {
                        eax123 = fun_2510();
                        rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                        if (eax123 >= 0) {
                            addr_32cd_140:
                            eax124 = static_cast<uint32_t>(v31) ^ 1;
                            *reinterpret_cast<uint32_t*>(&rax42) = *reinterpret_cast<unsigned char*>(&eax124);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax42) + 4) = 0;
                            rdx125 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
                            if (!rdx125) 
                                goto addr_32ef_21;
                        } else {
                            fun_2460();
                            fun_23c0();
                            fun_2630();
                            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8 - 8 + 8 - 8 + 8);
                            addr_33d4_71:
                            rax126 = full_write(1, "\r", 1, 1, "\r", 1);
                            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                            if (!(reinterpret_cast<unsigned char>(rax126) - 1)) 
                                goto addr_32c2_72; else 
                                goto addr_33f4_142;
                        }
                        addr_3394_143:
                        fun_2490();
                        rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                        continue;
                        addr_32c2_72:
                        if (v32) 
                            continue; else 
                            goto addr_32cd_140;
                        addr_33f4_142:
                        rax127 = fun_2460();
                        r12_85 = rax127;
                        rax128 = fun_23c0();
                        rdx22 = r12_85;
                        rsi95 = *reinterpret_cast<struct s0**>(&rax128->f0);
                        *reinterpret_cast<int32_t*>(&rsi95 + 4) = 0;
                        fun_2630();
                        rsp78 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8 - 8 + 8 - 8 + 8);
                        addr_3420_97:
                        eax117 = 1;
                        if (*reinterpret_cast<signed char*>(&rbx4)) 
                            goto addr_342d_134;
                        addr_314f_93:
                        rax129 = v77;
                        if (reinterpret_cast<signed char>(r13_3) < reinterpret_cast<signed char>(rax129)) 
                            goto addr_3160_144;
                        rax129 = r13_3;
                        addr_3160_144:
                        rbx4 = rax129;
                        rax130 = xalignalloc(v33, rax129, rdx22, rcx11, r8_10, r9_40);
                        rsp100 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp78) - 8 + 8);
                        v44 = rax130;
                        r15_131 = rax130;
                        do {
                            *reinterpret_cast<int32_t*>(&rdi132) = input_desc;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi132) + 4) = 0;
                            rdx22 = rbx4;
                            rsi95 = r15_131;
                            rax133 = safe_read(rdi132, rsi95, rdx22, rdi132, rsi95, rdx22);
                            rsp78 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp100) - 8 + 8);
                            if (reinterpret_cast<int1_t>(rax133 == 0xffffffffffffffff)) 
                                goto addr_31b4_138;
                            if (!rax133) 
                                goto addr_3209_136;
                            rax134 = full_write(1, r15_131, rax133, 1, r15_131, rax133);
                            rsp100 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp78) - 8 + 8);
                        } while (rax133 == rax134);
                        addr_3363_107:
                        fun_2460();
                        fun_23c0();
                        fun_2630();
                        rsp87 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp100) - 8 + 8 - 8 + 8 - 8 + 8);
                        addr_338f_88:
                        xalloc_die();
                        rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp87) - 8 + 8);
                        goto addr_3394_143;
                    }
                }
            }
            addr_2de8_149:
            rax135 = r13_3;
            if (!1) {
                zf136 = pending_cr == 0;
                if (!zf136) {
                    pending_cr = 0;
                    r13_3 = reinterpret_cast<struct s0*>(&r13_3->f2);
                    *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(r13_3) + 0xfffffffffffffffe) = 0x4d5e;
                }
                *reinterpret_cast<struct s0**>(&r13_3->f0) = reinterpret_cast<struct s0*>(36);
                rax135 = reinterpret_cast<struct s0*>(&r13_3->f1);
            }
            *reinterpret_cast<struct s0**>(&rax135->f0) = reinterpret_cast<struct s0*>(10);
            r13_3 = reinterpret_cast<struct s0*>(&rax135->f1);
            rax50 = r14_45;
            r14_45 = rdx22;
            continue;
            addr_2db3_66:
            if (!0 && !1) {
                rdi43 = r13_3;
                next_line_num(rdi43, rsi54);
                rsi54 = line_num_print;
                rax137 = fun_2450(rdi43, rsi54);
                rsp56 = rsp56 - 8 + 8 - 8 + 8;
                rdx22 = rdx22;
                r13_3 = rax137;
                goto addr_2de8_149;
            }
        }
    }
}

int64_t __libc_start_main = 0;

void fun_3463() {
    __asm__("cli ");
    __libc_start_main(0x2740, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xc008;

void fun_2390(int64_t rdi);

int64_t fun_3503() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2390(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_3543() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

struct s0* program_name = reinterpret_cast<struct s0*>(0);

void fun_2610(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

void fun_2550(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

int32_t fun_2570(int64_t rdi);

int32_t fun_23d0(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

int64_t stderr = 0;

void fun_26a0(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_3603(int32_t edi) {
    struct s0* r12_2;
    struct s0* rax3;
    struct s0* v4;
    struct s0* rax5;
    struct s0* rcx6;
    int64_t r12_7;
    struct s0* rax8;
    int64_t r12_9;
    struct s0* rax10;
    int64_t r12_11;
    struct s0* rax12;
    int64_t r12_13;
    struct s0* rax14;
    int64_t r12_15;
    struct s0* rax16;
    int64_t r12_17;
    struct s0* rax18;
    struct s0* r12_19;
    struct s0* rax20;
    int32_t eax21;
    struct s0* r13_22;
    struct s0* rax23;
    struct s0* rax24;
    int32_t eax25;
    struct s0* rax26;
    struct s0* rax27;
    struct s0* rax28;
    int32_t eax29;
    struct s0* rax30;
    int64_t r15_31;
    struct s0* rax32;
    struct s0* rax33;
    struct s0* rax34;
    int64_t rdi35;
    struct s0* r8_36;
    struct s0* r9_37;
    int64_t v38;
    int64_t v39;
    int64_t v40;
    int64_t v41;
    int64_t v42;
    int64_t v43;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2460();
            fun_2610(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_2460();
            fun_2550(rax8, r12_7, 5, rcx6);
            r12_9 = stdout;
            rax10 = fun_2460();
            fun_2550(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_2460();
            fun_2550(rax12, r12_11, 5, rcx6);
            r12_13 = stdout;
            rax14 = fun_2460();
            fun_2550(rax14, r12_13, 5, rcx6);
            r12_15 = stdout;
            rax16 = fun_2460();
            fun_2550(rax16, r12_15, 5, rcx6);
            r12_17 = stdout;
            rax18 = fun_2460();
            fun_2550(rax18, r12_17, 5, rcx6);
            r12_19 = program_name;
            rax20 = fun_2460();
            fun_2610(1, rax20, r12_19, r12_19);
            do {
                if (1) 
                    break;
                eax21 = fun_2570("cat");
            } while (eax21);
            r13_22 = v4;
            if (!r13_22) {
                rax23 = fun_2460();
                fun_2610(1, rax23, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax24 = fun_2600(5);
                if (!rax24 || (eax25 = fun_23d0(rax24, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax25)) {
                    rax26 = fun_2460();
                    r13_22 = reinterpret_cast<struct s0*>("cat");
                    fun_2610(1, rax26, "https://www.gnu.org/software/coreutils/", "cat");
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                } else {
                    r13_22 = reinterpret_cast<struct s0*>("cat");
                    goto addr_39b0_9;
                }
            } else {
                rax27 = fun_2460();
                fun_2610(1, rax27, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax28 = fun_2600(5);
                if (!rax28 || (eax29 = fun_23d0(rax28, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax29)) {
                    addr_38b6_11:
                    rax30 = fun_2460();
                    fun_2610(1, rax30, "https://www.gnu.org/software/coreutils/", "cat");
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_22 == "cat")) {
                        r12_2 = reinterpret_cast<struct s0*>(0x8ba1);
                    }
                } else {
                    addr_39b0_9:
                    r15_31 = stdout;
                    rax32 = fun_2460();
                    fun_2550(rax32, r15_31, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_38b6_11;
                }
            }
            rax33 = fun_2460();
            rcx6 = r12_2;
            fun_2610(1, rax33, r13_22, rcx6);
            addr_365e_14:
            fun_2680();
        }
    } else {
        rax34 = fun_2460();
        rdi35 = stderr;
        rcx6 = r12_2;
        fun_26a0(rdi35, 1, rax34, rcx6, r8_36, r9_37, v38, v39, v40, v41, v42, v43);
        goto addr_365e_14;
    }
}

int64_t file_name = 0;

void fun_39e3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_39f3(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(int64_t rdi);

struct s0* quotearg_colon();

int32_t exit_failure = 1;

struct s0* fun_23e0(int64_t rdi, int64_t rsi, int64_t rdx, struct s0* rcx, struct s0* r8);

void fun_3a03() {
    int64_t rdi1;
    int32_t eax2;
    struct s0* rax3;
    int1_t zf4;
    struct s0* rbx5;
    int64_t rdi6;
    int32_t eax7;
    struct s0* rax8;
    int64_t rdi9;
    struct s0* rax10;
    int64_t rsi11;
    struct s0* r8_12;
    struct s0* rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_23c0(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax3->f0) == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2460();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_3a93_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<struct s0**>(&rsi11) = *reinterpret_cast<struct s0**>(&rbx5->f0);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2630();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_23e0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_3a93_5:
        *reinterpret_cast<struct s0**>(&rsi11) = *reinterpret_cast<struct s0**>(&rbx5->f0);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2630();
    }
}

void fun_3ab3() {
    __asm__("cli ");
}

struct s4 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_25a0(struct s4* rdi);

void fun_3ac3(struct s4* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        fun_25a0(rdi);
        goto 0x2520;
    }
}

int64_t safe_write(int64_t rdi, int64_t rsi, int64_t rdx);

int64_t fun_3af3(int32_t edi, int64_t rsi, int64_t rdx) {
    int64_t r13_4;
    int32_t r12d5;
    int64_t rbp6;
    int64_t rbx7;
    int64_t rdi8;
    int64_t rax9;
    struct s0* rax10;

    __asm__("cli ");
    if (!rdx) {
        *reinterpret_cast<int32_t*>(&r13_4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_4) + 4) = 0;
    } else {
        r12d5 = edi;
        rbp6 = rsi;
        rbx7 = rdx;
        *reinterpret_cast<int32_t*>(&r13_4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_4) + 4) = 0;
        do {
            *reinterpret_cast<int32_t*>(&rdi8) = r12d5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
            rax9 = safe_write(rdi8, rbp6, rbx7);
            if (rax9 == -1) 
                break;
            if (!rax9) 
                goto addr_3b50_6;
            r13_4 = r13_4 + rax9;
            rbp6 = rbp6 + rax9;
            rbx7 = rbx7 - rax9;
        } while (rbx7);
    }
    return r13_4;
    addr_3b50_6:
    rax10 = fun_23c0();
    *reinterpret_cast<struct s0**>(&rax10->f0) = reinterpret_cast<struct s0*>(28);
    return r13_4;
}

void fun_2690(struct s0* rdi, int64_t rsi, int64_t rdx, int64_t rcx);

struct s5 {
    signed char[1] pad1;
    struct s0* f1;
    signed char[2] pad4;
    struct s0* f4;
};

struct s5* fun_24c0();

struct s0* __progname = reinterpret_cast<struct s0*>(0);

struct s0* __progname_full = reinterpret_cast<struct s0*>(0);

void fun_3b73(struct s0* rdi) {
    int64_t rcx2;
    struct s0* rbx3;
    struct s5* rax4;
    struct s0* r12_5;
    struct s0* rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2690("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_23b0("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_24c0();
        if (rax4 && ((r12_5 = reinterpret_cast<struct s0*>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_23d0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax4->f1) == 0x6c) || (r12_5->f1 != 0x74 || r12_5->f2 != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<struct s0*>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_5313(int64_t rdi) {
    int64_t rbp2;
    struct s0* rax3;
    struct s0* r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_23c0();
    r12d4 = *reinterpret_cast<struct s0**>(&rax3->f0);
    if (!rbp2) {
        rbp2 = 0xc220;
    }
    xmemdup(rbp2, 56);
    *reinterpret_cast<struct s0**>(&rax3->f0) = r12d4;
    return;
}

int64_t fun_5353(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xc220);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_5373(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xc220);
    }
    *rdi = esi;
    return 0xc220;
}

int64_t fun_5393(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xc220);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s6 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_53d3(struct s6* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s6*>(0xc220);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s7 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s7* fun_53f3(struct s7* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s7*>(0xc220);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x270a;
    if (!rdx) 
        goto 0x270a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xc220;
}

struct s8 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_5433(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s8* r8) {
    struct s8* rbx6;
    struct s0* rax7;
    struct s0* r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    struct s0* rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s8*>(0xc220);
    }
    rax7 = fun_23c0();
    r15d8 = *reinterpret_cast<struct s0**>(&rax7->f0);
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x5466);
    *reinterpret_cast<struct s0**>(&rax7->f0) = r15d8;
    return rax13;
}

struct s9 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_54b3(struct s0* rdi, struct s0* rsi, struct s0** rdx, struct s9* rcx) {
    struct s9* rbx5;
    struct s0* rax6;
    uint32_t r9d7;
    struct s0* r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    struct s0* v11;
    int64_t v12;
    int64_t v13;
    struct s0* rax14;
    struct s0* rsi15;
    struct s0* rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s9*>(0xc220);
    }
    rax6 = fun_23c0();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<struct s0*>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *reinterpret_cast<struct s0**>(&rax6->f0);
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x54e1);
    rsi15 = reinterpret_cast<struct s0*>(&rax14->f1);
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x553c);
    *reinterpret_cast<struct s0**>(&rax6->f0) = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_55a3() {
    __asm__("cli ");
}

struct s0* gc0b8 = reinterpret_cast<struct s0*>(32);

int64_t slotvec0 = 0x100;

void fun_55b3() {
    uint32_t eax1;
    struct s0* r12_2;
    uint64_t rax3;
    struct s0** rbx4;
    struct s0** rbp5;
    struct s0* rdi6;
    struct s0* rsi7;
    struct s0* rdx8;
    struct s0* rcx9;
    int64_t r8_10;
    int64_t r9_11;
    struct s0* rdi12;
    struct s0* rsi13;
    struct s0* rdx14;
    struct s0* rcx15;
    int64_t r8_16;
    int64_t r9_17;
    struct s0* rsi18;
    struct s0* rdx19;
    struct s0* rcx20;
    int64_t r8_21;
    int64_t r9_22;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = &r12_2->f18;
        rbp5 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_23a0(rdi6, rsi7, rdx8, rcx9, r8_10, r9_11);
        } while (rbx4 != rbp5);
    }
    rdi12 = r12_2->f8;
    if (rdi12 != 0xc120) {
        fun_23a0(rdi12, rsi13, rdx14, rcx15, r8_16, r9_17);
        gc0b8 = reinterpret_cast<struct s0*>(0xc120);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xc0b0) {
        fun_23a0(r12_2, rsi18, rdx19, rcx20, r8_21, r9_22);
        slotvec = reinterpret_cast<struct s0*>(0xc0b0);
    }
    nslots = 1;
    return;
}

void fun_5653() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5673() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5683(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_56a3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s0* fun_56c3(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2710;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2490();
    } else {
        return rax6;
    }
}

struct s0* fun_5753(struct s0* rdi, int32_t esi, struct s0* rdx, struct s0* rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2715;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2490();
    } else {
        return rax7;
    }
}

struct s0* fun_57e3(int32_t edi, struct s0* rsi) {
    struct s0* rax3;
    struct s1* rcx4;
    struct s0* rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x271a;
    rcx4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, 0xffffffffffffffff, rcx4, 0, rsi, 0xffffffffffffffff, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2490();
    } else {
        return rax5;
    }
}

struct s0* fun_5873(int32_t edi, struct s0* rsi, struct s0* rdx) {
    struct s0* rax4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x271f;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2490();
    } else {
        return rax6;
    }
}

struct s0* fun_5903(struct s0* rdi, struct s0* rsi, uint32_t edx) {
    struct s1* rsp4;
    struct s0* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    struct s0* rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6910]");
    __asm__("movdqa xmm1, [rip+0x6918]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x6901]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2490();
    } else {
        return rax10;
    }
}

struct s0* fun_59a3(struct s0* rdi, uint32_t esi) {
    struct s1* rsp3;
    struct s0* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6870]");
    __asm__("movdqa xmm1, [rip+0x6878]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x6861]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, 0xffffffffffffffff, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2490();
    } else {
        return rax9;
    }
}

struct s0* fun_5a43(struct s0* rdi) {
    struct s0* rax2;
    struct s0* rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x67d0]");
    __asm__("movdqa xmm1, [rip+0x67d8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x67b9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, 0xffffffffffffffff, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2490();
    } else {
        return rax3;
    }
}

struct s0* fun_5ad3(struct s0* rdi, struct s0* rsi) {
    struct s0* rax3;
    struct s0* rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6740]");
    __asm__("movdqa xmm1, [rip+0x6748]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x6736]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2490();
    } else {
        return rax4;
    }
}

struct s0* fun_5b63(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2724;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2490();
    } else {
        return rax6;
    }
}

struct s0* fun_5c03(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x660a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x6602]");
    __asm__("movdqa xmm2, [rip+0x660a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2729;
    if (!rdx) 
        goto 0x2729;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, 0xffffffffffffffff, rcx6, rdi, rcx, 0xffffffffffffffff, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2490();
    } else {
        return rax7;
    }
}

struct s0* fun_5ca3(int32_t edi, int64_t rsi, int64_t rdx, struct s0* rcx, struct s0* r8) {
    struct s0* rcx6;
    struct s1* rcx7;
    struct s0* rdi8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x656a]");
    __asm__("movdqa xmm1, [rip+0x6572]");
    __asm__("movdqa xmm2, [rip+0x657a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x272e;
    if (!rdx) 
        goto 0x272e;
    rcx7 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2490();
    } else {
        return rax9;
    }
}

struct s0* fun_5d53(int64_t rdi, int64_t rsi, struct s0* rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x64ba]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x64b2]");
    __asm__("movdqa xmm2, [rip+0x64ba]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2733;
    if (!rsi) 
        goto 0x2733;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, 0xffffffffffffffff, rcx5, 0, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2490();
    } else {
        return rax6;
    }
}

struct s0* fun_5df3(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x641a]");
    __asm__("movdqa xmm1, [rip+0x6422]");
    __asm__("movdqa xmm2, [rip+0x642a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2738;
    if (!rsi) 
        goto 0x2738;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2490();
    } else {
        return rax7;
    }
}

void fun_5e93() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5ea3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5ec3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5ee3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

int64_t fun_2530(int64_t rdi, int64_t rsi, uint64_t rdx);

int64_t fun_5f03(int32_t edi, int64_t rsi, uint64_t rdx) {
    int32_t r13d4;
    int64_t rbp5;
    uint64_t rbx6;
    int64_t rdi7;
    int64_t rax8;
    struct s0* rax9;

    __asm__("cli ");
    r13d4 = edi;
    rbp5 = rsi;
    rbx6 = rdx;
    while (*reinterpret_cast<int32_t*>(&rdi7) = r13d4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0, rax8 = fun_2530(rdi7, rbp5, rbx6), rax8 < 0) {
        rax9 = fun_23c0();
        if (*reinterpret_cast<struct s0**>(&rax9->f0) == 4) 
            continue;
        if (rbx6 <= 0x7ff00000) 
            break;
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax9->f0) == 22)) 
            break;
        *reinterpret_cast<int32_t*>(&rbx6) = 0x7ff00000;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
    }
    return rax8;
}

int64_t fun_2410(int64_t rdi, int64_t rsi, uint64_t rdx);

int64_t fun_5f73(int32_t edi, int64_t rsi, uint64_t rdx) {
    int32_t r13d4;
    int64_t rbp5;
    uint64_t rbx6;
    int64_t rdi7;
    int64_t rax8;
    struct s0* rax9;

    __asm__("cli ");
    r13d4 = edi;
    rbp5 = rsi;
    rbx6 = rdx;
    while (*reinterpret_cast<int32_t*>(&rdi7) = r13d4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0, rax8 = fun_2410(rdi7, rbp5, rbx6), rax8 < 0) {
        rax9 = fun_23c0();
        if (*reinterpret_cast<struct s0**>(&rax9->f0) == 4) 
            continue;
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax9->f0) == 22)) 
            break;
        if (rbx6 <= 0x7ff00000) 
            break;
        *reinterpret_cast<int32_t*>(&rbx6) = 0x7ff00000;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
    }
    return rax8;
}

struct s10 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
    signed char[7] pad16;
    struct s0* f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2580(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9);

void fun_5fe3(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s10* r8, struct s0* r9) {
    struct s0* r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    struct s0* rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    struct s0* rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    struct s0* r14_40;
    struct s0* r13_41;
    struct s0* r12_42;
    struct s0* rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_26a0(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_26a0(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2460();
    fun_26a0(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2580(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2460();
    fun_26a0(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2580(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2460();
        fun_26a0(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x8e48 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x8e48;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_6453() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s11 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_6473(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s11* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s11* rcx8;
    struct s0* rax9;
    struct s0* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2490();
    } else {
        return;
    }
}

void fun_6513(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    struct s0* rax15;
    struct s0* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_65b6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_65c0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2490();
    } else {
        return;
    }
    addr_65b6_5:
    goto addr_65c0_7;
}

void fun_65f3() {
    int64_t rsi1;
    struct s0* rdx2;
    struct s0* rcx3;
    struct s0* r8_4;
    struct s0* r9_5;
    struct s0* rax6;
    struct s0* rcx7;
    struct s0* rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2580(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2460();
    fun_2610(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2460();
    fun_2610(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2460();
    goto fun_2610;
}

int64_t fun_26b0();

void fun_6693() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_26b0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_2400();

void fun_66b3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2400();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_25b0(unsigned char* rdi);

void fun_66f3(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_25b0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6713(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_25b0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6733(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_25b0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_25f0();

void fun_6753(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_25f0();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6783() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25f0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_67b3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2400();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_67f3() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2400();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6833(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2400();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6863(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2400();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_68b3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2400();
            if (rax5) 
                break;
            addr_68fd_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_68fd_5;
        rax8 = fun_2400();
        if (rax8) 
            goto addr_68e6_9;
        if (rbx4) 
            goto addr_68fd_5;
        addr_68e6_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_6943(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2400();
            if (rax8) 
                break;
            addr_698a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_698a_5;
        rax11 = fun_2400();
        if (rax11) 
            goto addr_6972_9;
        if (!rbx6) 
            goto addr_6972_9;
        if (r12_4) 
            goto addr_698a_5;
        addr_6972_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_69d3(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_6a7d_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_6a90_10:
                *r12_8 = 0;
            }
            addr_6a30_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_6a56_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_6aa4_14;
            if (rcx10 <= rsi9) 
                goto addr_6a4d_16;
            if (rsi9 >= 0) 
                goto addr_6aa4_14;
            addr_6a4d_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_6aa4_14;
            addr_6a56_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_25f0();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_6aa4_14;
            if (!rbp13) 
                break;
            addr_6aa4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_6a7d_9;
        } else {
            if (!r13_6) 
                goto addr_6a90_10;
            goto addr_6a30_11;
        }
    }
}

int64_t fun_2560();

void fun_6ad3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2560();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6b03() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2560();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6b33() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2560();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6b53() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2560();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_2590(signed char* rdi, struct s0* rsi, unsigned char* rdx);

void fun_6b73(int64_t rdi, unsigned char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_25b0(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2590;
    }
}

void fun_6bb3(int64_t rdi, unsigned char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_25b0(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2590;
    }
}

struct s12 {
    signed char[1] pad1;
    unsigned char f1;
};

void fun_6bf3(int64_t rdi, struct s12* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_25b0(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_2590;
    }
}

struct s0* fun_2480(struct s0* rdi, ...);

void fun_6c33(struct s0* rdi) {
    struct s0* rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_2480(rdi);
    rax3 = fun_25b0(&rax2->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2590;
    }
}

void fun_6c73() {
    struct s0* rdi1;

    __asm__("cli ");
    fun_2460();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_2630();
    fun_23b0(rdi1);
}

int64_t fun_23f0();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_6cb3(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    struct s0* rax5;
    struct s0* rax6;

    __asm__("cli ");
    rax2 = fun_23f0();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_6d0e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_23c0();
            *reinterpret_cast<struct s0**>(&rax5->f0) = reinterpret_cast<struct s0*>(0);
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_6d0e_3;
            rax6 = fun_23c0();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax6->f0) == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

int32_t fun_25e0(struct s4* rdi);

int32_t rpl_fflush(struct s4* rdi);

int64_t fun_2430(struct s4* rdi);

int64_t fun_6d23(struct s4* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    struct s0* rax8;
    struct s0* r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_25a0(rdi);
    if (eax2 >= 0) {
        eax3 = fun_25e0(rdi);
        if (!(eax3 && (eax4 = fun_25a0(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_24d0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_23c0();
            r12d9 = *reinterpret_cast<struct s0**>(&rax8->f0);
            rax10 = fun_2430(rdi);
            if (r12d9) {
                *reinterpret_cast<struct s0**>(&rax8->f0) = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2430;
}

void rpl_fseeko(struct s4* rdi);

void fun_6db3(struct s4* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_25e0(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_6e03(struct s4* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_25a0(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_24d0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_25d0(int64_t rdi);

signed char* fun_6e83() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_25d0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_24b0(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_6ec3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    struct s0* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_24b0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2490();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_6f53() {
    struct s0* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2490();
    } else {
        return rax3;
    }
}

int64_t fun_6fd3(int64_t rdi, signed char* rsi, struct s0* rdx) {
    struct s0* rax4;
    int32_t r13d5;
    struct s0* rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2600(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_2480(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2590(rsi, rax4, &rax6->f1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2590(rsi, rax4, reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsi) + reinterpret_cast<unsigned char>(rdx)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_7083() {
    __asm__("cli ");
    goto fun_2600;
}

void fun_7093() {
    __asm__("cli ");
}

void fun_70a7() {
    __asm__("cli ");
    return;
}

void fun_282b() {
    goto 0x27ea;
}

uint32_t fun_2540(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* rpl_mbrtowc(void* rdi, struct s0* rsi);

int32_t fun_26d0(int64_t rdi, struct s0* rsi);

uint32_t fun_26c0(struct s0* rdi, struct s0* rsi);

void** fun_26f0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

void fun_3da5() {
    struct s0** rsp1;
    int32_t ebp2;
    struct s0* rax3;
    struct s0** rsp4;
    struct s0* r11_5;
    struct s0* r11_6;
    struct s0* v7;
    int32_t ebp8;
    struct s0* rax9;
    struct s0* rdx10;
    struct s0* rax11;
    struct s0* r11_12;
    struct s0* v13;
    int32_t ebp14;
    struct s0* rax15;
    struct s0* r15_16;
    int32_t ebx17;
    uint32_t eax18;
    struct s0* r13_19;
    void* r14_20;
    signed char* r12_21;
    struct s0* v22;
    int32_t ebx23;
    struct s0* rax24;
    struct s0** rsp25;
    struct s0* v26;
    struct s0* r11_27;
    struct s0* v28;
    struct s0* v29;
    struct s0* rsi30;
    struct s0* v31;
    struct s0* v32;
    struct s0* r10_33;
    struct s0* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    struct s0* r9_37;
    struct s0* v38;
    struct s0* rdi39;
    struct s0* v40;
    struct s0* rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    struct s0* rcx44;
    unsigned char al45;
    struct s0* v46;
    int64_t v47;
    struct s0* v48;
    struct s0* v49;
    struct s0* rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    struct s0* v58;
    unsigned char v59;
    struct s0* v60;
    struct s0* v61;
    struct s0* v62;
    signed char* v63;
    struct s0* r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    struct s0* r13_69;
    struct s0* rsi70;
    void* v71;
    struct s0* r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    struct s0* v107;
    struct s0* rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<struct s0**>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2460();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2460();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx10->f0)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2480(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<struct s0*>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<struct s0*>(0);
    while (1) {
        v32 = *reinterpret_cast<struct s0**>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_40a3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_40a3_22; else 
                            goto addr_449d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_455d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_48b0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_40a0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_40a0_30; else 
                                goto addr_48c9_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2480(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_48b0_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2540(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_48b0_28; else 
                            goto addr_3f4c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_4a10_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_4890_40:
                        if (r11_27 == 1) {
                            addr_441d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_49d8_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_4057_44;
                            }
                        } else {
                            goto addr_48a0_46;
                        }
                    } else {
                        addr_4a1f_47:
                        rax24 = v46;
                        if (!rax24->f1) {
                            goto addr_441d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_40a3_22:
                                if (v47 != 1) {
                                    addr_45f9_52:
                                    v48 = reinterpret_cast<struct s0*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2480(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_4644_54;
                                    }
                                } else {
                                    goto addr_40b0_56;
                                }
                            } else {
                                addr_4055_57:
                                ebp36 = 0;
                                goto addr_4057_44;
                            }
                        } else {
                            addr_4884_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_4a1f_47; else 
                                goto addr_488e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_441d_41;
                        if (v47 == 1) 
                            goto addr_40b0_56; else 
                            goto addr_45f9_52;
                    }
                }
                addr_4111_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<struct s0*>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_3fa8_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_3fcd_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_42d0_65;
                    } else {
                        addr_4139_66:
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_4988_67;
                    }
                } else {
                    goto addr_4130_69;
                }
                addr_3fe1_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad8);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                addr_402c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_4988_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_402c_81;
                }
                addr_4130_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_3fcd_64; else 
                    goto addr_4139_66;
                addr_4057_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_410f_91;
                if (v22) 
                    goto addr_406f_93;
                addr_410f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4111_62;
                addr_4644_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_4dcb_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_4e3b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx10->f0)) - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_4c3f_103;
                            rdx10 = reinterpret_cast<struct s0*>(&rdx10->f1);
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_26d0(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_26c0(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_473e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_40fc_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_4748_112;
                    }
                } else {
                    addr_4748_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_4819_114;
                }
                addr_4108_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_410f_91;
                while (1) {
                    addr_4819_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_4d27_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_4786_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                        }
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_4d35_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_4807_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_4807_128;
                        }
                    }
                    addr_47b5_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad8);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_4807_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                    continue;
                    addr_4786_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad8);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_47b5_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_402c_81;
                addr_4d35_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4988_67;
                addr_4dcb_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_473e_109;
                addr_4e3b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_473e_109;
                addr_40b0_56:
                rax93 = fun_26f0(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_40fc_110;
                addr_488e_59:
                goto addr_4890_40;
                addr_455d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_40a3_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_4108_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4055_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_40a3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_45a2_160;
                if (!v22) 
                    goto addr_4977_162; else 
                    goto addr_4b83_163;
                addr_45a2_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_4977_162:
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_4988_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_444b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_42b3_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_3fe1_70; else 
                    goto addr_42c7_169;
                addr_444b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_3fa8_63;
                goto addr_4130_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_4884_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_49bf_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_40a0_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_3f98_178; else 
                        goto addr_4942_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_4884_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_40a3_22;
                }
                addr_49bf_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_40a0_30:
                    r8d42 = 0;
                    goto addr_40a3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_4111_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_49d8_42;
                    }
                }
                addr_3f98_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3fa8_63;
                addr_4942_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_48a0_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_4111_62;
                } else {
                    addr_4952_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_40a3_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_5102_188;
                if (v28) 
                    goto addr_4977_162;
                addr_5102_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_42b3_168;
                addr_3f4c_37:
                if (v22) 
                    goto addr_4f43_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_3f63_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_4a10_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_4a9b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_40a3_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_3f98_178; else 
                        goto addr_4a77_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_4884_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_40a3_22;
                }
                addr_4a9b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_40a3_22;
                }
                addr_4a77_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_48a0_46;
                goto addr_4952_186;
                addr_3f63_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_40a3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_40a3_22; else 
                    goto addr_3f74_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_504e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_4ed4_210;
            if (1) 
                goto addr_4ed2_212;
            if (!v29) 
                goto addr_4b0e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2470();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<struct s0*>(1);
            v47 = rax101;
            v26 = reinterpret_cast<struct s0*>("\"");
            if (!0) 
                goto addr_5041_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<struct s0*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<struct s0*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_42d0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_408b_219; else 
            goto addr_42ea_220;
        addr_406f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_4083_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_42ea_220; else 
            goto addr_408b_219;
        addr_4c3f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_42ea_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2470();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_4c5d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2470();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<struct s0*>("'");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<struct s0*>(0);
            continue;
        }
        addr_50d0_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_4b36_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<struct s0*>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<struct s0*>(0);
        v22 = reinterpret_cast<struct s0*>(0);
        v28 = reinterpret_cast<struct s0*>(1);
        v26 = reinterpret_cast<struct s0*>("'");
        continue;
        addr_4d27_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4083_221;
        addr_4b83_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4083_221;
        addr_42c7_169:
        goto addr_42d0_65;
        addr_504e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_42ea_220;
        goto addr_4c5d_222;
        addr_4ed4_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&v26->f0)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                rdx108 = reinterpret_cast<struct s0*>(&rdx108->f1);
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_4f2e_236;
        fun_2490();
        rsp25 = rsp25 - 8 + 8;
        goto addr_50d0_225;
        addr_4ed2_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_4ed4_210;
        addr_4b0e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_4ed4_210;
        } else {
            rdx10 = reinterpret_cast<struct s0*>(0);
            goto addr_4b36_226;
        }
        addr_5041_216:
        r13_34 = reinterpret_cast<struct s0*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<struct s0*>("\"");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        v31 = reinterpret_cast<struct s0*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_449d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x890c + rax113 * 4) + 0x890c;
    addr_48c9_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x8a0c + rax114 * 4) + 0x8a0c;
    addr_4f43_190:
    addr_408b_219:
    goto 0x3d70;
    addr_3f74_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x880c + rax115 * 4) + 0x880c;
    addr_4f2e_236:
    goto v116;
}

void fun_3f90() {
}

void fun_4148() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x3e42;
}

void fun_41a1() {
    goto 0x3e42;
}

void fun_428e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x4111;
    }
    if (v2) 
        goto 0x4b83;
    if (!r10_3) 
        goto addr_4cee_5;
    if (!v4) 
        goto addr_4bbe_7;
    addr_4cee_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_4bbe_7:
    goto 0x3fc4;
}

void fun_42ac() {
}

void fun_4357() {
    signed char v1;

    if (v1) {
        goto 0x42df;
    } else {
        goto 0x401a;
    }
}

void fun_4371() {
    signed char v1;

    if (!v1) 
        goto 0x436a; else 
        goto "???";
}

void fun_4398() {
    goto 0x42b3;
}

void fun_4418() {
}

void fun_4430() {
}

void fun_445f() {
    goto 0x42b3;
}

void fun_44b1() {
    goto 0x4440;
}

void fun_44e0() {
    goto 0x4440;
}

void fun_4513() {
    goto 0x4440;
}

void fun_48e0() {
    goto 0x3f98;
}

void fun_4bde() {
    signed char v1;

    if (v1) 
        goto 0x4b83;
    goto 0x3fc4;
}

void fun_4c85() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x3fc4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x3fa8;
        goto 0x3fc4;
    }
}

void fun_50a2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x4310;
    } else {
        goto 0x3e42;
    }
}

void fun_60b8() {
    fun_2460();
}

void fun_2837() {
    goto 0x27ea;
}

void fun_3211() {
    goto 0x27ea;
}

void fun_41ce() {
    goto 0x3e42;
}

void fun_43a4() {
    goto 0x435c;
}

void fun_446b() {
    goto 0x3f98;
}

void fun_44bd() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x4440;
    goto 0x406f;
}

void fun_44ef() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x444b;
        goto 0x3e70;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x42ea;
        goto 0x408b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x4c88;
    if (r10_8 > r15_9) 
        goto addr_43d5_9;
    addr_43da_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x4c93;
    goto 0x3fc4;
    addr_43d5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_43da_10;
}

void fun_4522() {
    goto 0x4057;
}

void fun_48f0() {
    goto 0x4057;
}

void fun_508f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x41ac;
    } else {
        goto 0x4310;
    }
}

void fun_6170() {
}

void fun_2848() {
    goto 0x27ea;
}

void fun_452c() {
    goto 0x44c7;
}

void fun_48fa() {
    goto 0x441d;
}

void fun_61d0() {
    fun_2460();
    goto fun_26a0;
}

void fun_284f() {
    goto 0x27ea;
}

void fun_41fd() {
    goto 0x3e42;
}

void fun_4538() {
    goto 0x44c7;
}

void fun_4907() {
    goto 0x446e;
}

void fun_6210() {
    fun_2460();
    goto fun_26a0;
}

void fun_285b() {
    goto 0x27ea;
}

void fun_422a() {
    goto 0x3e42;
}

void fun_4544() {
    goto 0x4440;
}

void fun_6250() {
    fun_2460();
    goto fun_26a0;
}

void fun_2862() {
    goto 0x27ea;
}

void fun_424c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x4be0;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x4111;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x4111;
    }
    if (v11) 
        goto 0x4f43;
    if (r10_12 > r15_13) 
        goto addr_4f93_8;
    addr_4f98_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x4cd1;
    addr_4f93_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_4f98_9;
}

struct s13 {
    signed char[24] pad24;
    int64_t f18;
};

struct s14 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s15 {
    signed char[8] pad8;
    struct s0* f8;
};

void fun_62a0() {
    int64_t r15_1;
    struct s13* rbx2;
    struct s0* r14_3;
    struct s14* rbx4;
    struct s0* r13_5;
    struct s15* rbx6;
    struct s0* r12_7;
    struct s0** rbx8;
    struct s0* rax9;
    int64_t rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2460();
    fun_26a0(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x62c2, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_2869() {
    goto 0x27ea;
}

void fun_62f8() {
    fun_2460();
    goto 0x62c9;
}

struct s16 {
    signed char[32] pad32;
    int64_t f20;
};

struct s17 {
    signed char[24] pad24;
    int64_t f18;
};

struct s18 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s19 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s20 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_6330() {
    int64_t rcx1;
    struct s16* rbx2;
    int64_t r15_3;
    struct s17* rbx4;
    struct s0* r14_5;
    struct s18* rbx6;
    struct s0* r13_7;
    struct s19* rbx8;
    struct s0* r12_9;
    struct s0** rbx10;
    int64_t v11;
    struct s20* rbx12;
    struct s0* rax13;
    int64_t rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2460();
    fun_26a0(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x6364, __return_address(), rcx1);
    goto v15;
}

void fun_63a8() {
    fun_2460();
    goto 0x636b;
}
