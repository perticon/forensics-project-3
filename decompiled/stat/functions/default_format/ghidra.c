undefined8 default_format(char param_1,char param_2,char param_3)

{
  undefined8 uVar1;
  void *pvVar2;
  void *__ptr;
  char *pcVar3;
  
  if (param_1 == '\0') {
    if (param_2 == '\0') {
      uVar1 = dcgettext(0,"  File: %N\n  Size: %-10s\tBlocks: %-10b IO Block: %-6o %F\n",5);
      pvVar2 = (void *)xstrdup(uVar1);
      pcVar3 = "Device: %Hd,%Ld\tInode: %-10i  Links: %-5h Device type: %Hr,%Lr\n";
      if (param_3 == '\0') {
        pcVar3 = "Device: %Hd,%Ld\tInode: %-10i  Links: %h\n";
      }
      uVar1 = dcgettext(0,pcVar3,5);
      __ptr = (void *)xasprintf(&DAT_0011224b,pvVar2,uVar1);
      free(pvVar2);
      uVar1 = dcgettext(0,"Access: (%04a/%10.10A)  Uid: (%5u/%8U)   Gid: (%5g/%8G)\n",5);
      pvVar2 = (void *)xasprintf(&DAT_0011224b,__ptr,uVar1);
      free(__ptr);
      uVar1 = dcgettext(0,"Access: %x\nModify: %y\nChange: %z\n Birth: %w\n",5);
      uVar1 = xasprintf(&DAT_0011224b,pvVar2,uVar1);
      free(pvVar2);
      return uVar1;
    }
    pcVar3 = "%n %s %b %f %u %g %D %i %h %t %T %X %Y %Z %W %o\n";
  }
  else {
    pcVar3 = "%n %i %l %t %s %S %b %f %a %c %d\n";
    if (param_2 == '\0') {
      pcVar3 = (char *)dcgettext(0,
                                 "  File: \"%n\"\n    ID: %-8i Namelen: %-7l Type: %T\nBlock size: %-10s Fundamental block size: %S\nBlocks: Total: %-10b Free: %-10f Available: %a\nInodes: Total: %-10c Free: %d\n"
                                 ,5);
    }
  }
  uVar1 = xstrdup(pcVar3);
  return uVar1;
}