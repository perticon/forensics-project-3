char * default_format(bool fs, bool terse, bool device) {
    int64_t v1; // 0x3090
    if (!fs) {
        // 0x30d0
        v1 = (int64_t)"%n %s %b %f %u %g %D %i %h %t %T %X %Y %Z %W %o\n";
        if (!terse) {
            char * v2 = xstrdup((char *)function_26c0()); // 0x30f6
            char * v3 = xasprintf("%s%s", v2, (char *)function_26c0()); // 0x312e
            function_25b0();
            char * v4 = xasprintf("%s%s", v3, (char *)function_26c0()); // 0x315c
            function_25b0();
            char * result = xasprintf("%s%s", v4, (char *)function_26c0()); // 0x318a
            function_25b0();
            return result;
        }
    } else {
        // 0x309f
        v1 = (int64_t)"%n %i %l %t %s %S %b %f %a %c %d\n";
        if (!terse) {
            // 0x30ab
            v1 = function_26c0();
        }
    }
    // 0x30c1
    return xstrdup((char *)v1);
}