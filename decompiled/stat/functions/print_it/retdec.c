bool print_it(char * format, int32_t fd, char * filename, bool (*print_func)(char *, int64_t, char, char, int32_t, char *, int32_t *), int32_t * data) {
    bool (*v1)(char *, int64_t, char, char, int32_t, char *, int32_t *) = print_func; // bp-88, 0x352d
    int64_t v2 = function_26e0(); // 0x3536
    int64_t v3 = xmalloc(); // 0x353f
    char v4 = (char)v2 + 3;
    if (v4 == 0) {
        // 0x3620
        function_25b0();
        function_2810();
        return false;
    }
    int64_t v5 = (int64_t)&v1; // 0x3520
    char v6 = v4; // 0x3581
    int64_t v7 = 0;
    int64_t v8 = (int64_t)format; // 0x3510
    int64_t v9; // 0x3510
    int64_t v10; // 0x3510
    int64_t v11; // 0x3510
    int64_t v12; // 0x3510
    int64_t v13; // 0x3510
    int64_t v14; // 0x3510
    int64_t v15; // 0x3510
    int64_t v16; // 0x3510
    int64_t v17; // 0x3510
    int64_t v18; // 0x3510
    int64_t v19; // 0x35a0
    unsigned char v20; // 0x35a6
    int64_t v21; // 0x35a6
    int64_t v22; // 0x3795
    int64_t v23; // 0x37a0
    uint64_t v24; // 0x3653
    char v25; // 0x3669
    while (true) {
      lab_0x358d:
        // 0x358d
        v10 = v7;
        v12 = v8;
        char v26 = v6;
        while (v26 != 37) {
            if (v26 != 92) {
                int64_t v27 = (int64_t)g33; // 0x3558
                int64_t v28 = v12 + 1; // 0x355f
                int64_t * v29 = (int64_t *)(v27 + 40); // 0x3563
                uint64_t v30 = *v29; // 0x3563
                if (v30 >= *(int64_t *)(v27 + 48)) {
                    // 0x3828
                    function_2760();
                    v15 = v12;
                    v9 = v10;
                    v8 = v28;
                    goto lab_0x3581;
                } else {
                    // 0x3571
                    *v29 = v30 + 1;
                    *(char *)v30 = v26;
                    v15 = v12;
                    v9 = v10;
                    v8 = v28;
                    goto lab_0x3581;
                }
            }
            // 0x3599
            v19 = v12 + 1;
            if (*(char *)&interpret_backslash_escapes != 0) {
                // 0x35a6
                v20 = *(char *)v19;
                v21 = v20;
                int64_t v31 = v21 + 0xffffffd0; // 0x35ab
                if ((char)v31 < 8) {
                    int64_t v32 = v12 + 2; // 0x3730
                    int64_t v33 = 0x100000000000000 * v31;
                    int64_t v34 = (int64_t)*(char *)v32 + 0xffffffd0; // 0x3738
                    if ((char)v34 < 8) {
                        int64_t v35 = v12 + 3; // 0x3744
                        int64_t v36 = (0x100000000000000 * v34 >> 56) + (v33 >> 53) & 0xffffffff; // 0x374c
                        int64_t v37 = (int64_t)*(char *)v35 + 0xffffffd0; // 0x374f
                        v14 = v36;
                        v13 = v35;
                        if ((char)v37 < 8) {
                            // 0x375b
                            v14 = (0x100000000000000 * v37 >> 56) + 8 * v36 & 0xffffffff;
                            v13 = v12 + 4;
                            goto lab_0x3765;
                        } else {
                            goto lab_0x3765;
                        }
                    } else {
                        // 0x39db
                        v14 = v33 >> 56;
                        v13 = v32;
                        goto lab_0x3765;
                    }
                }
                if (v20 == 120) {
                    int64_t v38 = function_2a80(); // 0x3790
                    v22 = v12 + 2;
                    unsigned char v39 = *(char *)v22; // 0x3795
                    v23 = *(int64_t *)v38 + 1;
                    if ((*(char *)(v23 + 2 * (int64_t)v39) & 16) == 0) {
                        goto lab_0x384e;
                    } else {
                        int64_t v40 = v39; // 0x37ae
                        if (v39 < 103) {
                            // 0x3919
                            v18 = v40 + 0xffffffa9;
                            goto lab_0x37cb;
                        } else {
                            // 0x37bb
                            v18 = (v39 < 71 ? 0xffffffc9 : 0xffffffd0) + v40;
                            goto lab_0x37cb;
                        }
                    }
                }
                if (v20 != 0) {
                    if (v20 == 34) {
                        goto lab_0x3880;
                    } else {
                        goto lab_0x384e;
                    }
                }
                // 0x35ca
                function_26c0();
                function_2990();
            }
            int64_t v41 = (int64_t)g33; // 0x35eb
            int64_t * v42 = (int64_t *)(v41 + 40); // 0x35f2
            uint64_t v43 = *v42; // 0x35f2
            if (v43 >= *(int64_t *)(v41 + 48)) {
                // 0x38c0
                function_2760();
                v15 = v12;
                v9 = v10;
                v8 = v19;
                goto lab_0x3581;
            }
            // 0x3600
            *v42 = v43 + 1;
            *(char *)v43 = 92;
            char v44 = *(char *)v19; // 0x3614
            v11 = v10;
            if (v44 == 0) {
                // 0x3620
                function_25b0();
                function_2810();
                return v11 % 2 != 0;
            }
            v12 = v19;
            v26 = v44;
        }
        // 0x3650
        v24 = format_code_offset((char *)v12);
        int64_t v45 = v24 + v12; // 0x365e
        v25 = *(char *)v45;
        function_2880();
        v16 = v45;
        if (v25 == 37) {
            goto lab_0x3685;
        } else {
            if (v25 > 37) {
                // 0x36c0
                v17 = v45;
                if ((v25 - 72 & -5) == 0) {
                    // 0x3700
                    v17 = v45;
                    if (v1 == (bool (*)(char *, int64_t, char, char, int32_t, char *, int32_t *))0x5000) {
                        int64_t v46 = v45 + 1; // 0x370d
                        char v47 = *(char *)v46; // 0x370d
                        v17 = v47 != 100 == (v47 != 114) ? v45 : v46;
                    }
                }
                goto lab_0x36cc;
            } else {
                // 0x367a
                v17 = v45;
                if (v25 != 0) {
                    goto lab_0x36cc;
                } else {
                    // 0x3681
                    v16 = v45 - 1;
                    goto lab_0x3685;
                }
            }
        }
    }
  lab_0x39f1:;
    int64_t v48 = v24 + v3;
    *(char *)v48 = v25;
    *(char *)(v48 + 1) = 0;
    quote((char *)v3);
    function_26c0();
    return function_2990() % 2 != 0;
  lab_0x3685:
    if (v24 >= 2) {
        // break -> 0x39f1
        goto lab_0x39f1;
    }
    int64_t v49 = v16;
    int64_t v50 = (int64_t)g33; // 0x368f
    int64_t v51 = v49 + 1; // 0x3696
    int64_t * v52 = (int64_t *)(v50 + 40); // 0x369a
    uint64_t v53 = *v52; // 0x369a
    if (v53 >= *(int64_t *)(v50 + 48)) {
        // 0x38e0
        function_2760();
        v15 = v49;
        v9 = v10;
        v8 = v51;
    } else {
        // 0x36a8
        *v52 = v53 + 1;
        *(char *)v53 = 37;
        v15 = v49;
        v9 = v10;
        v8 = v51;
    }
    goto lab_0x3581;
  lab_0x3581:
    // 0x3581
    v7 = v9;
    v6 = *(char *)(v15 + 1);
    v11 = v7;
    if (v6 == 0) {
        // 0x3620
        function_25b0();
        function_2810();
        return v11 % 2 != 0;
    }
    goto lab_0x358d;
  lab_0x36cc:
    // 0x36cc
    *(int64_t *)(v5 - 16) = *(int64_t *)(v5 + 24);
    v15 = v17;
    v9 = (v10 | (int64_t)v1) & 0xffffffff;
    v8 = v17 + 1;
    goto lab_0x3581;
  lab_0x3765:;
    int64_t v54 = (int64_t)g33; // 0x3765
    int64_t * v55 = (int64_t *)(v54 + 40); // 0x376c
    uint64_t v56 = *v55; // 0x376c
    if (v56 >= *(int64_t *)(v54 + 48)) {
        // 0x39a9
        function_2760();
        goto lab_0x3784;
    } else {
        // 0x377a
        *v55 = v56 + 1;
        *(char *)v56 = (char)v14;
        goto lab_0x3784;
    }
  lab_0x3784:
    // 0x3784
    v15 = v13 - 1;
    v9 = v10;
    v8 = v13;
    goto lab_0x3581;
  lab_0x384e:;
    int64_t v57 = v21 + 0xffffffa4; // 0x384e
    if ((char)v57 < 27) {
        uint32_t v58 = *(int32_t *)((4 * v57 & 1020) + (int64_t)&g2); // 0x3864
        return v58 % 2 != 0 != (bool)&g2;
    }
    // 0x38f0
    function_26c0();
    function_2990();
    goto lab_0x3880;
  lab_0x3880:;
    int64_t v59 = (int64_t)g33; // 0x3880
    int64_t v60 = v12 + 2; // 0x3887
    int64_t * v61 = (int64_t *)(v59 + 40); // 0x388b
    uint64_t v62 = *v61; // 0x388b
    if (v62 >= *(int64_t *)(v59 + 48)) {
        // 0x399b
        function_2760();
        v15 = v19;
        v9 = v10;
        v8 = v60;
        goto lab_0x3581;
    } else {
        // 0x3899
        *v61 = v62 + 1;
        *(char *)v62 = v20;
        v15 = v19;
        v9 = v10;
        v8 = v60;
        goto lab_0x3581;
    }
  lab_0x37cb:;
    int64_t v63 = v18 & 0xffffffff;
    int64_t v64 = v12 + 3; // 0x37cb
    unsigned char v65 = *(char *)v64; // 0x37cb
    int64_t v66 = v63; // 0x37dc
    int64_t v67 = v22; // 0x37dc
    int64_t v68; // 0x3510
    if ((*(char *)(2 * (int64_t)v65 + v23) & 16) == 0) {
        goto lab_0x37fb;
    } else {
        int64_t v69 = v65; // 0x37de
        if (v65 < 103) {
            // 0x37f6
            v68 = v69 + 0xffffffa9;
            goto lab_0x37f9;
        } else {
            // 0x3987
            v68 = (v65 < 71 ? 0xffffffc9 : 0xffffffd0) + v69;
            goto lab_0x37f9;
        }
    }
  lab_0x37fb:;
    int64_t v70 = v67;
    int64_t v71 = (int64_t)g33; // 0x37fb
    int64_t v72 = v70 + 1; // 0x3802
    int64_t * v73 = (int64_t *)(v71 + 40); // 0x3806
    uint64_t v74 = *v73; // 0x3806
    if (v74 >= *(int64_t *)(v71 + 48)) {
        // 0x39e4
        function_2760();
        v15 = v70;
        v9 = v10;
        v8 = v72;
        goto lab_0x3581;
    } else {
        // 0x3814
        *v73 = v74 + 1;
        *(char *)v74 = (char)v66;
        v15 = v70;
        v9 = v10;
        v8 = v72;
        goto lab_0x3581;
    }
  lab_0x37f9:
    // 0x37f9
    v66 = v68 + 16 * v63 & 0xffffffff;
    v67 = v64;
    goto lab_0x37fb;
}