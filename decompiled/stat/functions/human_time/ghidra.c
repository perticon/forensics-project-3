undefined1 * human_time(undefined8 param_1,ulong param_2)

{
  ulong uVar1;
  long lVar2;
  undefined8 uVar3;
  char *pcVar4;
  long in_FS_OFFSET;
  undefined8 local_88;
  ulong local_80;
  undefined local_78 [64];
  undefined local_38 [24];
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  local_88 = param_1;
  local_80 = param_2;
  if (tz_1 == 0) {
    pcVar4 = getenv("TZ");
    tz_1 = tzalloc(pcVar4);
  }
  uVar1 = local_80;
  lVar2 = localtime_rz(tz_1,&local_88,local_78);
  if (lVar2 == 0) {
    uVar3 = imaxtostr(local_88,local_38);
    __sprintf_chk(str_0,1,0x3d,"%s.%09d",uVar3,uVar1 & 0xffffffff);
  }
  else {
    nstrftime(str_0,0x3d,"%Y-%m-%d %H:%M:%S.%N %z",local_78,tz_1,uVar1 & 0xffffffff);
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return str_0;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}