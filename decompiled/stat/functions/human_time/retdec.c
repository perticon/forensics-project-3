char * human_time(int64_t t) {
    int64_t v1 = t; // bp-136, 0x33b7
    int64_t v2 = __readfsqword(40); // 0x33c7
    int32_t * v3 = g40; // 0x33da
    if (g40 == NULL) {
        // 0x3480
        v3 = tzalloc((char *)function_2590());
        g40 = v3;
    }
    // 0x33e0
    int64_t v4; // bp-120, 0x33b0
    if (localtime_rz(v3, &v1, (struct tm *)&v4) == NULL) {
        // 0x3440
        int64_t v5; // bp-56, 0x33b0
        imaxtostr(v1, (char *)&v5);
        function_2a90();
    } else {
        // 0x33fa
        int64_t v6; // 0x33b0
        nstrftime((char *)&g39, 61, "%Y-%m-%d %H:%M:%S.%N %z", (struct tm *)&v4, g40, (int32_t)v6);
    }
    // 0x3422
    if (v2 != __readfsqword(40)) {
        // 0x34a3
        return (char *)function_2710();
    }
    // 0x3432
    return (char *)&g39;
}