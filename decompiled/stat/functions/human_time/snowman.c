void** human_time(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7) {
    void*** rsp8;
    void** rdi9;
    void** rax10;
    void** rax11;
    void** rax12;
    void** r12_13;
    void** rbx14;
    int64_t rax15;
    void** rax16;
    void** r9_17;
    void** rdx18;
    void** r8_19;
    void** rcx20;
    void* rax21;
    void* rax22;
    void** rbx23;
    void* rax24;
    struct s11* rbx25;

    rsp8 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 0x78);
    rdi9 = tz_1;
    rax10 = g28;
    if (!rdi9) {
        rax11 = fun_2590("TZ", rsi, rdx, rcx, r8, r9);
        rax12 = tzalloc(rax11, rsi);
        rsp8 = rsp8 - 8 + 8 - 8 + 8;
        tz_1 = rax12;
        rdi9 = rax12;
    }
    r12_13 = reinterpret_cast<void**>(rsp8 + 16);
    rbx14 = rsi;
    rax15 = localtime_rz(rdi9, rsp8, r12_13, rcx);
    if (!rax15) {
        rax16 = imaxtostr(rdi, rsp8 - 8 + 8 + 80, r12_13, rcx);
        *reinterpret_cast<int32_t*>(&r9_17) = *reinterpret_cast<int32_t*>(&rbx14);
        *reinterpret_cast<int32_t*>(&r9_17 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdx18) = 61;
        *reinterpret_cast<int32_t*>(&rdx18 + 4) = 0;
        r8_19 = rax16;
        rcx20 = reinterpret_cast<void**>("%s.%09d");
        fun_2a90(0x18100, 1, 61, "%s.%09d", r8_19, r9_17, rdi);
    } else {
        rcx20 = r12_13;
        *reinterpret_cast<int32_t*>(&r9_17) = *reinterpret_cast<int32_t*>(&rbx14);
        *reinterpret_cast<int32_t*>(&r9_17 + 4) = 0;
        r8_19 = tz_1;
        rdx18 = reinterpret_cast<void**>("%Y-%m-%d %H:%M:%S.%N %z");
        nstrftime();
    }
    rax21 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax10) - reinterpret_cast<unsigned char>(g28));
    if (!rax21) {
        return 0x18100;
    }
    fun_2710();
    rax22 = fun_27e0(0x18101, "'-+ #0I", rdx18, rcx20, r8_19, r9_17);
    rbx23 = reinterpret_cast<void**>(0x18100 + reinterpret_cast<uint64_t>(rax22) + 1);
    rax24 = fun_27e0(rbx23, "0123456789", rdx18, rcx20, r8_19, r9_17);
    rbx25 = reinterpret_cast<struct s11*>(reinterpret_cast<unsigned char>(rbx23) + reinterpret_cast<uint64_t>(rax24));
    if (rbx25->f0 == 46) 
        goto addr_34e6_10;
    addr_34f7_11:
    goto rdi;
    addr_34e6_10:
    fun_27e0(&rbx25->f1, "0123456789", rdx18, rcx20, r8_19, r9_17);
    goto addr_34f7_11;
}