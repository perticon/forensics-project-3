ulong out_epoch_sec(char *param_1,size_t param_2,long param_3,long param_4)

{
  bool bVar1;
  int iVar2;
  int iVar3;
  char *pcVar4;
  ulong uVar5;
  ulong uVar6;
  long lVar7;
  int iVar8;
  int iVar9;
  uint uVar10;
  char *pcVar11;
  long lVar12;
  char *pcVar13;
  int iVar14;
  int local_4c;
  
  pcVar4 = (char *)memchr(param_1,0x2e,param_2);
  if (pcVar4 == (char *)0x0) {
    local_4c = 0;
    uVar5 = 0;
LAB_00103fcb:
    iVar3 = (int)uVar5;
    uVar5 = uVar5 & 0xffffffff;
    iVar8 = 1;
    do {
      uVar10 = (int)uVar5 + 1;
      uVar5 = (ulong)uVar10;
      iVar8 = iVar8 * 10;
    } while (uVar10 != 9);
    lVar12 = (long)iVar8;
    lVar7 = param_4 / lVar12;
  }
  else {
    param_1[param_2] = '\0';
    param_2 = (long)pcVar4 - (long)param_1;
    lVar7 = param_4;
    if ((int)pcVar4[1] - 0x30U < 10) {
      uVar6 = strtol(pcVar4 + 1,(char **)0x0,10);
      uVar5 = 0x7fffffff;
      if ((long)uVar6 < 0x80000000) {
        uVar5 = uVar6;
      }
      local_4c = (int)uVar5;
      if (local_4c != 0) {
        if ((int)pcVar4[-1] - 0x30U < 10) goto LAB_0010407e;
        local_4c = 0;
        goto LAB_00104148;
      }
      uVar5 = 0;
      goto LAB_00103fcb;
    }
    if ((int)pcVar4[-1] - 0x30U < 10) {
      uVar5 = 9;
LAB_0010407e:
      *pcVar4 = '\0';
      do {
        pcVar13 = pcVar4 + -2;
        pcVar4 = pcVar4 + -1;
      } while ((int)*pcVar13 - 0x30U < 10);
      lVar12 = strtol(pcVar4,(char **)0x0,10);
      if (0x7fffffff < lVar12) {
        lVar12 = 0x7fffffff;
      }
      local_4c = (int)lVar12;
      if (1 < local_4c) {
        pcVar4 = pcVar4 + (*pcVar4 == '0');
        param_2 = (long)pcVar4 - (long)param_1;
        if (((decimal_point_len < (ulong)(long)local_4c) &&
            (iVar3 = local_4c - (int)decimal_point_len, 1 < iVar3)) &&
           (iVar3 = iVar3 - (int)uVar5, 1 < iVar3)) {
          pcVar13 = param_1;
          if (param_1 < pcVar4) {
            bVar1 = false;
            pcVar11 = param_1;
            do {
              if (*pcVar11 == '-') {
                bVar1 = true;
              }
              else {
                *pcVar13 = *pcVar11;
                pcVar13 = pcVar13 + 1;
              }
              pcVar11 = pcVar11 + 1;
            } while (pcVar4 != pcVar11);
            param_2 = (long)pcVar13 - (long)param_1;
            if (bVar1) goto LAB_00104148;
          }
          else {
            param_2 = 0;
          }
          iVar3 = __sprintf_chk(pcVar13,1,0xffffffffffffffff,&DAT_00112ce1,iVar3);
          param_2 = param_2 + (long)iVar3;
        }
      }
LAB_00104148:
      iVar3 = (int)uVar5;
      if (iVar3 < 9) goto LAB_00103fcb;
      lVar12 = 1;
      iVar8 = 1;
    }
    else {
      lVar12 = 1;
      iVar8 = 1;
      local_4c = 0;
      iVar3 = 9;
    }
  }
  iVar9 = (int)lVar7;
  if ((param_3 < 0) && (param_4 != 0)) {
    iVar9 = ((int)(1000000000 / (long)iVar8) - iVar9) + -1 + (uint)(param_4 % lVar12 == 0);
    param_3 = (param_3 + 1) - (ulong)(iVar9 == 0);
    if (param_3 == 0) {
      make_format(param_1,param_2,"\'-+ 0",&DAT_001122b4);
      uVar5 = __printf_chk(DAT_00112ac0,1,param_1);
      goto LAB_00103ee9;
    }
  }
  make_format(param_1,param_2,"\'-+ 0",&DAT_00112603);
  uVar5 = __printf_chk(1,param_1,param_3);
LAB_00103ee9:
  if (iVar3 != 0) {
    iVar8 = 9;
    if (iVar3 < 10) {
      iVar8 = iVar3;
    }
    iVar2 = (int)uVar5;
    if ((int)uVar5 < 0) {
      iVar2 = 0;
    }
    iVar14 = 0;
    if ((iVar2 < local_4c) && (decimal_point_len < (ulong)(long)(local_4c - iVar2))) {
      iVar14 = (local_4c - iVar2) - (iVar8 + (int)decimal_point_len);
    }
    uVar5 = (ulong)(uint)(iVar3 - iVar8);
    __printf_chk(1,"%s%.*d%-*.*d",decimal_point,iVar8,iVar9,iVar14,(ulong)(uint)(iVar3 - iVar8),0);
  }
  return uVar5;
}