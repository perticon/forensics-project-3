void out_epoch_sec(char * pformat, int64_t prefix_len, int64_t arg) {
    // 0x3e20
    int64_t v1; // 0x3e20
    uint64_t v2 = v1;
    int64_t v3 = function_27f0(); // 0x3e45
    int64_t v4 = prefix_len; // 0x3e4d
    int64_t v5 = 0; // 0x3e4d
    int64_t v6; // 0x3e20
    int64_t v7; // 0x3e20
    int64_t v8; // 0x3e20
    int64_t v9; // 0x3e20
    int64_t v10; // 0x3e20
    int32_t v11; // 0x3e20
    int64_t v12; // 0x3e20
    int64_t v13; // 0x3e20
    int64_t v14; // 0x3e66
    if (v3 == 0) {
        goto lab_0x3fcb;
    } else {
        // 0x3e53
        v13 = (int64_t)pformat;
        *(char *)(v13 + prefix_len) = 0;
        char v15 = *(char *)(v3 + 1); // 0x3e62
        v14 = v3 - v13;
        if (v15 == 57 || (int32_t)v15 < 57) {
            int64_t v16 = function_2870(); // 0x3f79
            int64_t v17 = v16 - 0x7fffffff; // 0x3f87
            int64_t v18 = v17 == 0 | v17 < 0 != (0x7ffffffe - v16 & v16) < 0 ? v16 : 0x7fffffff; // 0x3f8a
            v4 = v14;
            v5 = 0;
            if ((int32_t)v18 == 0) {
                goto lab_0x3fcb;
            } else {
                char v19 = *(char *)(v3 - 1); // 0x3fa2
                v7 = v18;
                v10 = v14;
                v8 = v18;
                if (v19 == 57 || (int32_t)v19 < 57) {
                    goto lab_0x407e;
                } else {
                    goto lab_0x4148;
                }
            }
        } else {
            char v20 = *(char *)(v3 - 1); // 0x3e75
            v11 = 1;
            v9 = v14;
            v12 = 1;
            v6 = 9;
            v7 = 9;
            if (v20 == 57 || (int32_t)v20 < 57) {
                goto lab_0x407e;
            } else {
                goto lab_0x3ea8;
            }
        }
    }
  lab_0x3fcb:;
    int64_t v21 = 1;
    int64_t v22 = (v5 & 0xffffffff) + 1; // 0x3fdb
    int64_t v23 = 10 * v21; // 0x3fde
    int64_t v24 = v22; // 0x3fe3
    while ((int32_t)v22 != 9) {
        // 0x3fd8
        v21 = v23 & 0xfffffffe;
        v22 = (v24 & 0xffffffff) + 1;
        v23 = 10 * v21;
        v24 = v22;
    }
    int64_t v25 = 0xa00000000 * v21 >> 32; // 0x3fe8
    v11 = v23;
    v9 = v4;
    v12 = v25;
    int64_t v26 = v2 / v25; // 0x3ff3
    v6 = v5;
    goto lab_0x3ea8;
  lab_0x3ea8:
    // 0x3ea8
    if (arg < 0 == (v2 != 0)) {
        // 0x4000
        if ((int64_t)((int32_t)((v26 ^ 0xffffffff) + (int64_t)(0x3b9aca00 / v11) + (int64_t)(v2 % v12 == 0)) != 0) != -arg) {
            // 0x3eb6
            make_format(pformat, v9, "'-+ 0", "ld");
            function_2960();
            goto lab_0x3ee9;
        } else {
            // 0x4032
            make_format(pformat, v9, "'-+ 0", ".0f");
            __asm_movsd(-0x4030000000000000);
            function_2960();
            goto lab_0x3ee9;
        }
    } else {
        // 0x3eb6
        make_format(pformat, v9, "'-+ 0", "ld");
        function_2960();
        goto lab_0x3ee9;
    }
  lab_0x407e:
    // 0x407e
    *(char *)v3 = 0;
    char v27 = *(char *)(v3 - 2); // 0x4088
    int64_t v28 = v3 - 1; // 0x408c
    int64_t v29 = v28; // 0x4096
    while (v27 == 57 || (int32_t)v27 < 57) {
        // 0x4088
        v27 = *(char *)(v29 - 2);
        v28 = v29 - 1;
        v29 = v28;
    }
    int64_t v30 = function_2870(); // 0x40ab
    int64_t v31 = v30 - 0x7fffffff; // 0x40b9
    int64_t v32 = v31 < 0 == (0x7ffffffe - v30 & v30) < 0 == (v31 != 0) ? 0x7fffffff : v30; // 0x40bc
    v10 = v14;
    v8 = v7;
    int64_t v33; // 0x4176
    if ((int32_t)v32 < 2) {
        goto lab_0x4148;
    } else {
        uint64_t v34 = v28 + (int64_t)(*(char *)v28 == 48); // 0x40d9
        int64_t v35 = v34 - v13; // 0x40e6
        v10 = v35;
        v8 = v7;
        if (0x100000000 * v32 >> 32 > decimal_point_len) {
            int64_t v36 = v32 - decimal_point_len; // 0x40ee
            v10 = v35;
            v8 = v7;
            if ((int32_t)v36 < 2) {
                goto lab_0x4148;
            } else {
                // 0x40f5
                v10 = v35;
                v8 = v7;
                if ((int32_t)(v36 - v7) < 2) {
                    goto lab_0x4148;
                } else {
                    // 0x40fd
                    v33 = 0;
                    if (v34 <= v13) {
                        goto lab_0x417e;
                    } else {
                        char v37 = *(char *)v13; // 0x4128
                        int64_t v38 = v13; // 0x412f
                        int64_t v39 = 1; // 0x412f
                        if (v37 != 45) {
                            // 0x4118
                            v39 = 0;
                            *(char *)v13 = v37;
                            v38 = v13 + 1;
                        }
                        int64_t v40 = v39;
                        int64_t v41 = v38;
                        int64_t v42 = v13 + 1; // 0x411f
                        int64_t v43 = v42; // 0x4126
                        while (v34 != v42) {
                            int64_t v44 = v41;
                            v37 = *(char *)v43;
                            v38 = v44;
                            v39 = 1;
                            if (v37 != 45) {
                                // 0x4118
                                v39 = v40;
                                *(char *)v44 = v37;
                                v38 = v44 + 1;
                            }
                            // 0x411f
                            v40 = v39;
                            v41 = v38;
                            v42 = v43 + 1;
                            v43 = v42;
                        }
                        // 0x4173
                        v33 = v41 - v13;
                        v10 = v33;
                        v8 = v7;
                        if ((char)v40 != 0) {
                            goto lab_0x4148;
                        } else {
                            goto lab_0x417e;
                        }
                    }
                }
            }
        } else {
            goto lab_0x4148;
        }
    }
  lab_0x3ee9:
    if ((int32_t)v6 != 0) {
        // 0x3eee
        function_2960();
    }
  lab_0x4148:
    // 0x4148
    v11 = 1;
    v9 = v10;
    v12 = 1;
    v6 = v8;
    v4 = v10;
    v5 = v8;
    if ((int32_t)v8 < 9) {
        goto lab_0x3fcb;
    } else {
        goto lab_0x3ea8;
    }
  lab_0x417e:
    // 0x417e
    v10 = (0x100000000 * function_2a90() >> 32) + v33;
    v8 = v7;
    goto lab_0x4148;
}