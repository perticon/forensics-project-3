int64_t out_epoch_sec(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r15_5;
    void** r13_6;
    void** r12_7;
    void** rbp8;
    void** rbx9;
    struct s1* rax10;
    int32_t v11;
    int64_t r14_12;
    struct s1* r8_13;
    struct s1* rcx14;
    void** r10_15;
    void** v16;
    struct s1* v17;
    int64_t rax18;
    int64_t rdx19;
    void** r8_20;
    int64_t rsi21;
    int64_t rcx22;
    int64_t rax23;
    void* rdx24;
    void** rcx25;
    void** rdx26;
    int32_t eax27;
    int32_t eax28;
    int32_t edx29;
    void** rdi30;
    void** rdx31;
    int32_t r8d32;
    uint32_t esi33;
    int32_t ecx34;
    void** ecx35;
    void** r15_36;
    void** r9_37;
    int64_t rax38;
    void** r9_39;
    void** rcx40;
    int64_t r14_41;
    void** rax42;
    void** rdx43;
    void** r8_44;
    void** r9_45;
    int32_t eax46;

    r15_5 = rdx;
    r13_6 = rdi;
    r12_7 = rdx;
    rbp8 = rsi;
    rbx9 = rcx;
    rax10 = fun_27f0(rdi, 46, rsi);
    if (!rax10) {
        v11 = 0;
        *reinterpret_cast<int32_t*>(&r14_12) = 0;
        goto addr_3fcb_3;
    }
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r13_6) + reinterpret_cast<unsigned char>(rbp8)) = 0;
    r8_13 = rax10;
    rcx14 = rax10;
    r10_15 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax10) - reinterpret_cast<unsigned char>(r13_6));
    if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(&rax10->f1) - 48) <= 9) {
        v16 = r10_15;
        v17 = r8_13;
        rax18 = fun_2870(&r8_13->f1);
        *reinterpret_cast<int32_t*>(&rdx19) = 0x7fffffff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx19) + 4) = 0;
        r10_15 = v16;
        if (rax18 <= 0x7fffffff) {
            rdx19 = rax18;
        }
        v11 = *reinterpret_cast<int32_t*>(&rdx19);
        r14_12 = rdx19;
        if (*reinterpret_cast<int32_t*>(&rdx19)) 
            goto addr_3f9d_8;
    } else {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r8_13) - 1) - 48 <= 9) {
            *reinterpret_cast<int32_t*>(&r14_12) = 9;
            goto addr_407e_11;
        } else {
            r8_20 = rbx9;
            rbp8 = r10_15;
            *reinterpret_cast<int32_t*>(&rsi21) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi21) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rcx22) = 1;
            v11 = 0;
            *reinterpret_cast<int32_t*>(&r14_12) = 9;
            goto addr_3ea8_13;
        }
    }
    *reinterpret_cast<int32_t*>(&r14_12) = 0;
    rbp8 = r10_15;
    goto addr_3fcb_3;
    addr_3f9d_8:
    r8_13 = r8_13;
    if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r8_13) - 1) - 48 > 9) {
        v11 = 0;
        rbp8 = r10_15;
        goto addr_4148_16;
    } else {
        rcx14 = v17;
    }
    addr_407e_11:
    r8_13->f0 = 0;
    do {
        rcx14 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(rcx14) - 1);
    } while ((rcx14 - 1)->f0 - 48 <= 9);
    v16 = r10_15;
    rax23 = fun_2870(rcx14);
    rbp8 = v16;
    if (rax23 > 0x7fffffff) {
        rax23 = 0x7fffffff;
    }
    v11 = *reinterpret_cast<int32_t*>(&rax23);
    if (*reinterpret_cast<int32_t*>(&rax23) <= 1 || ((*reinterpret_cast<int32_t*>(&rdx24) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx24) + 4) = 0, *reinterpret_cast<unsigned char*>(&rdx24) = reinterpret_cast<uint1_t>(rcx14->f0 == 48), rcx25 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx14) + reinterpret_cast<uint64_t>(rdx24)), rdx26 = decimal_point_len, rbp8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx25) - reinterpret_cast<unsigned char>(r13_6)), reinterpret_cast<unsigned char>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax23))) <= reinterpret_cast<unsigned char>(rdx26)) || ((eax27 = *reinterpret_cast<int32_t*>(&rax23) - *reinterpret_cast<int32_t*>(&rdx26), eax27 <= 1) || (eax28 = eax27 - *reinterpret_cast<int32_t*>(&r14_12), eax28 <= 1)))) {
        addr_4148_16:
        if (*reinterpret_cast<int32_t*>(&r14_12) <= 8) {
            addr_3fcb_3:
            edx29 = *reinterpret_cast<int32_t*>(&r14_12);
            *reinterpret_cast<int32_t*>(&rcx22) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx22) + 4) = 0;
        } else {
            r8_20 = rbx9;
            *reinterpret_cast<int32_t*>(&rsi21) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi21) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rcx22) = 1;
            goto addr_3ea8_13;
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_6) >= reinterpret_cast<unsigned char>(rcx25)) {
            rdi30 = r13_6;
            *reinterpret_cast<int32_t*>(&rbp8) = 0;
            *reinterpret_cast<int32_t*>(&rbp8 + 4) = 0;
            goto addr_417e_25;
        } else {
            rdx31 = r13_6;
            rdi30 = r13_6;
            r8d32 = 0;
            do {
                esi33 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx31));
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&esi33) == 45)) {
                    *reinterpret_cast<void***>(rdi30) = *reinterpret_cast<void***>(&esi33);
                    ++rdi30;
                } else {
                    r8d32 = 1;
                }
                ++rdx31;
            } while (rcx25 != rdx31);
            rbp8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi30) - reinterpret_cast<unsigned char>(r13_6));
            if (*reinterpret_cast<signed char*>(&r8d32)) 
                goto addr_4148_16; else 
                goto addr_417e_25;
        }
    }
    do {
        ecx34 = static_cast<int32_t>(rcx22 + rcx22 * 4);
        ++edx29;
        *reinterpret_cast<int32_t*>(&rcx22) = ecx34 + ecx34;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx22) + 4) = 0;
    } while (edx29 != 9);
    rsi21 = *reinterpret_cast<int32_t*>(&rcx22);
    r8_20 = reinterpret_cast<void**>(reinterpret_cast<signed char>(rbx9) / rsi21);
    addr_3ea8_13:
    if (reinterpret_cast<signed char>(r15_5) >= reinterpret_cast<signed char>(0) || (!rbx9 || (ecx35 = reinterpret_cast<void**>(0x3b9aca00 / *reinterpret_cast<int32_t*>(&rcx22) - reinterpret_cast<unsigned char>(r8_20) + 0xffffffff + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<signed char>(rbx9) % rsi21) < 1)), r8_20 = ecx35, *reinterpret_cast<int32_t*>(&r8_20 + 4) = 0, r15_36 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_5) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_5) < reinterpret_cast<unsigned char>(1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(ecx35) < reinterpret_cast<unsigned char>(1)))))))), r12_7 = r15_36, !!r15_36))) {
        v16 = r8_20;
        make_format(r13_6, rbp8, "'-+ 0", "ld", r8_20, r9_37, v16);
        rax38 = fun_2960(1, r13_6, r12_7, "ld");
    } else {
        v16 = ecx35;
        make_format(r13_6, rbp8, "'-+ 0", ".0f", r8_20, r9_39, v16);
        *reinterpret_cast<void***>(r13_6) = *reinterpret_cast<void***>(rbp8);
        rax38 = fun_2960(1, r13_6, "'-+ 0", ".0f");
    }
    if (*reinterpret_cast<int32_t*>(&r14_12)) {
        *reinterpret_cast<int32_t*>(&rcx40) = 9;
        *reinterpret_cast<int32_t*>(&rcx40 + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&r14_12) <= 9) {
            *reinterpret_cast<int32_t*>(&rcx40) = *reinterpret_cast<int32_t*>(&r14_12);
            *reinterpret_cast<int32_t*>(&rcx40 + 4) = 0;
        }
        *reinterpret_cast<int32_t*>(&r14_41) = *reinterpret_cast<int32_t*>(&r14_12) - *reinterpret_cast<int32_t*>(&rcx40);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_41) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rax38) < 0) {
            *reinterpret_cast<int32_t*>(&rax38) = 0;
        }
        if (*reinterpret_cast<int32_t*>(&rax38) < v11 && (rax42 = decimal_point_len, reinterpret_cast<unsigned char>(static_cast<int64_t>(v11 - *reinterpret_cast<int32_t*>(&rax38))) > reinterpret_cast<unsigned char>(rax42))) {
        }
        rdx43 = decimal_point;
        fun_2960(1, "%s%.*d%-*.*d", rdx43, rcx40, 1, "%s%.*d%-*.*d", rdx43, rcx40);
        rax38 = r14_41;
    }
    return rax38;
    addr_417e_25:
    *reinterpret_cast<int32_t*>(&r8_44) = eax28;
    *reinterpret_cast<int32_t*>(&r8_44 + 4) = 0;
    eax46 = fun_2a90(rdi30, 1, 0xffffffffffffffff, "%d", r8_44, r9_45, v16);
    rbp8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp8) + reinterpret_cast<uint64_t>(static_cast<int64_t>(eax46)));
    goto addr_4148_16;
}