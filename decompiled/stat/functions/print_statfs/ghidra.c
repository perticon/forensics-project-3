print_statfs(undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined param_4,
            undefined8 param_5,undefined8 param_6,char **param_7)

{
  undefined *puVar1;
  char *pcVar2;
  char *pcVar3;
  
  switch(param_4) {
  case 0x53:
    pcVar3 = param_7[9];
    if (pcVar3 == (char *)0x0) {
      pcVar3 = param_7[1];
    }
    goto LAB_00104222;
  case 0x54:
    pcVar2 = *param_7;
    if (pcVar2 == (char *)0x42494e4d) {
      pcVar3 = "binfmt_misc";
    }
    else if ((long)pcVar2 < 0x42494e4e) {
      if (pcVar2 == (char *)0xef53) {
        pcVar3 = "ext2/ext3";
      }
      else if ((long)pcVar2 < 0xef54) {
        pcVar3 = "hfs+";
        if (pcVar2 != (char *)0x482b) {
          if ((long)pcVar2 < 0x482c) {
            pcVar3 = "devpts";
            if (pcVar2 != (char *)0x1cd1) {
              if ((long)pcVar2 < 0x1cd2) {
                pcVar3 = "devfs";
                if (pcVar2 != (char *)0x1373) {
                  if ((long)pcVar2 < 0x1374) {
                    pcVar3 = "autofs";
                    if (pcVar2 != (char *)0x187) {
                      if ((long)pcVar2 < 0x188) {
                        pcVar3 = "qnx4";
                        if (pcVar2 != (char *)0x2f) {
                          if (pcVar2 != (char *)0x33) goto LAB_00104fc8;
                          pcVar3 = "z3fold";
                        }
                      }
                      else {
                        if (pcVar2 != (char *)0x7c0) goto LAB_00104fc8;
                        pcVar3 = "jffs";
                      }
                    }
                  }
                  else {
                    pcVar3 = "minix";
                    if (pcVar2 != (char *)0x137f) {
                      if (pcVar2 == (char *)0x138f) {
                        pcVar3 = "minix (30 char.)";
                      }
                      else {
                        if (pcVar2 != (char *)0x137d) goto LAB_00104fc8;
                        pcVar3 = "ext";
                      }
                    }
                  }
                }
              }
              else {
                pcVar3 = "isofs";
                if (pcVar2 != (char *)0x4000) {
                  if ((long)pcVar2 < 0x4001) {
                    pcVar3 = "minix v2 (30 char.)";
                    if (pcVar2 != (char *)0x2478) {
                      if (pcVar2 == (char *)0x3434) {
                        pcVar3 = "nilfs";
                      }
                      else {
                        if (pcVar2 != (char *)0x2468) goto LAB_00104fc8;
                        pcVar3 = "minix v2";
                      }
                    }
                  }
                  else {
                    pcVar3 = "fat";
                    if (pcVar2 != (char *)0x4006) {
                      if (pcVar2 == (char *)0x4244) {
                        pcVar3 = "hfs";
                      }
                      else {
                        if (pcVar2 != (char *)0x4004) goto LAB_00104fc8;
                        pcVar3 = "isofs";
                      }
                    }
                  }
                }
              }
            }
          }
          else {
            pcVar3 = "jffs2";
            if (pcVar2 != (char *)0x72b6) {
              if ((long)pcVar2 < 0x72b7) {
                pcVar3 = "novell";
                if (pcVar2 != (char *)0x564c) {
                  if ((long)pcVar2 < 0x564d) {
                    pcVar3 = "minix3";
                    if (pcVar2 != (char *)0x4d5a) {
                      if ((long)pcVar2 < 0x4d5b) {
                        pcVar3 = "hfsx";
                        if (pcVar2 != (char *)0x4858) {
                          if (pcVar2 != (char *)0x4d44) goto LAB_00104fc8;
                          pcVar3 = "msdos";
                        }
                      }
                      else if (pcVar2 == (char *)0x517b) {
                        pcVar3 = "smb";
                      }
                      else {
LAB_00104fc8:
                        __sprintf_chk(buf_5,1,0x1d,"UNKNOWN (0x%lx)");
                        pcVar3 = buf_5;
                      }
                    }
                  }
                  else {
                    pcVar3 = "nfs";
                    if (pcVar2 != (char *)0x6969) {
                      if (pcVar2 == (char *)0x7275) {
                        pcVar3 = "romfs";
                      }
                      else {
                        if (pcVar2 != (char *)0x5df5) goto LAB_00104fc8;
                        pcVar3 = "exofs";
                      }
                    }
                  }
                }
              }
              else {
                pcVar3 = "usbdevfs";
                if (pcVar2 != (char *)0x9fa2) {
                  if ((long)pcVar2 < 0x9fa3) {
                    pcVar3 = "proc";
                    if (pcVar2 != (char *)0x9fa0) {
                      if (pcVar2 == (char *)0x9fa1) {
                        pcVar3 = "openprom";
                      }
                      else {
                        if (pcVar2 != (char *)0x9660) goto LAB_00104fc8;
                        pcVar3 = "isofs";
                      }
                    }
                  }
                  else {
                    pcVar3 = "affs";
                    if (pcVar2 != (char *)0xadff) {
                      if (pcVar2 == (char *)0xef51) {
                        pcVar3 = "ext2";
                      }
                      else {
                        if (pcVar2 != (char *)0xadf5) goto LAB_00104fc8;
                        pcVar3 = "adfs";
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      else {
        pcVar3 = "anon-inode FS";
        if (pcVar2 != (char *)0x9041934) {
          if ((long)pcVar2 < 0x9041935) {
            pcVar3 = "gfs/gfs2";
            if (pcVar2 != (char *)0x1161970) {
              if ((long)pcVar2 < 0x1161971) {
                pcVar3 = "hostfs";
                if (pcVar2 != (char *)0xc0ffee) {
                  if ((long)pcVar2 < 0xc0ffef) {
                    pcVar3 = "cgroupfs";
                    if (pcVar2 != (char *)0x27e0eb) {
                      if ((long)pcVar2 < 0x27e0ec) {
                        pcVar3 = "ecryptfs";
                        if (pcVar2 != (char *)0xf15f) {
                          if (pcVar2 != (char *)0x11954) goto LAB_00104fc8;
                          pcVar3 = "ufs";
                        }
                      }
                      else {
                        if (pcVar2 != (char *)0x414a53) goto LAB_00104fc8;
                        pcVar3 = "efs";
                      }
                    }
                  }
                  else {
                    pcVar3 = "tmpfs";
                    if (pcVar2 != (char *)0x1021994) {
                      if (pcVar2 == (char *)0x1021997) {
                        pcVar3 = "v9fs";
                      }
                      else {
                        if (pcVar2 != (char *)0xc36400) goto LAB_00104fc8;
                        pcVar3 = "ceph";
                      }
                    }
                  }
                }
              }
              else {
                pcVar3 = "sysv2";
                if (pcVar2 != (char *)0x12ff7b6) {
                  if ((long)pcVar2 < 0x12ff7b7) {
                    pcVar3 = "xenix";
                    if (pcVar2 != (char *)0x12ff7b4) {
                      if (pcVar2 == (char *)0x12ff7b5) {
                        pcVar3 = "sysv4";
                      }
                      else {
                        if (pcVar2 != (char *)0x12fd16d) goto LAB_00104fc8;
                        pcVar3 = "xia";
                      }
                    }
                  }
                  else {
                    pcVar3 = "ibrix";
                    if (pcVar2 != (char *)0x13111a8) {
                      if (pcVar2 == (char *)0x7655821) {
                        pcVar3 = "rdt";
                      }
                      else {
                        if (pcVar2 != (char *)0x12ff7b7) goto LAB_00104fc8;
                        pcVar3 = "coh";
                      }
                    }
                  }
                }
              }
            }
          }
          else {
            pcVar3 = "bfs";
            if (pcVar2 != (char *)0x1badface) {
              if ((long)pcVar2 < 0x1badfacf) {
                pcVar3 = "balloon-kvm-fs";
                if (pcVar2 != (char *)0x13661366) {
                  if ((long)pcVar2 < 0x13661367) {
                    pcVar3 = "lustre";
                    if (pcVar2 != (char *)0xbd00bd0) {
                      if (pcVar2 == (char *)0x11307854) {
                        pcVar3 = "inodefs";
                      }
                      else {
                        if (pcVar2 != (char *)0xbad1dea) goto LAB_00104fc8;
                        pcVar3 = "futexfs";
                      }
                    }
                  }
                  else {
                    pcVar3 = "mqueue";
                    if (pcVar2 != (char *)0x19800202) {
                      if (pcVar2 == (char *)0x19830326) {
                        pcVar3 = "fhgfs";
                      }
                      else {
                        if (pcVar2 != (char *)0x15013346) goto LAB_00104fc8;
                        pcVar3 = "udf";
                      }
                    }
                  }
                }
              }
              else {
                pcVar3 = "inotifyfs";
                if (pcVar2 != (char *)0x2bad1dea) {
                  if ((long)pcVar2 < 0x2bad1deb) {
                    pcVar3 = "ubifs";
                    if (pcVar2 != (char *)0x24051905) {
                      if (pcVar2 == (char *)0x28cd3d45) {
                        pcVar3 = "cramfs";
                      }
                      else {
                        if (pcVar2 != (char *)0x2011bab0) goto LAB_00104fc8;
                        pcVar3 = "exfat";
                      }
                    }
                  }
                  else {
                    pcVar3 = "jfs";
                    if (pcVar2 != (char *)0x3153464a) {
                      if (pcVar2 == (char *)0x42465331) {
                        pcVar3 = "befs";
                      }
                      else {
                        if (pcVar2 != (char *)0x2fc12fc1) goto LAB_00104fc8;
                        pcVar3 = "zfs";
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    else if (pcVar2 == (char *)0x67596969) {
      pcVar3 = "rpc_pipefs";
    }
    else if ((long)pcVar2 < 0x6759696a) {
      pcVar3 = "zsmallocfs";
      if (pcVar2 != (char *)0x58295829) {
        if ((long)pcVar2 < 0x5829582a) {
          pcVar3 = "secretmem";
          if (pcVar2 != (char *)0x5345434d) {
            if ((long)pcVar2 < 0x5345434e) {
              pcVar3 = "exfs";
              if (pcVar2 != (char *)0x45584653) {
                if ((long)pcVar2 < 0x45584654) {
                  pcVar3 = "cramfs-wend";
                  if (pcVar2 != (char *)0x453dcd28) {
                    if ((long)pcVar2 < 0x453dcd29) {
                      pcVar3 = "smackfs";
                      if (pcVar2 != (char *)0x43415d53) {
                        if (pcVar2 != (char *)0x444d4142) goto LAB_00104fc8;
                        pcVar3 = "dma-buf-fs";
                      }
                    }
                    else {
                      if (pcVar2 != (char *)0x454d444d) goto LAB_00104fc8;
                      pcVar3 = "devmem";
                    }
                  }
                }
                else {
                  pcVar3 = "pipefs";
                  if (pcVar2 != (char *)0x50495045) {
                    if (pcVar2 == (char *)0x52654973) {
                      pcVar3 = "reiserfs";
                    }
                    else {
                      if (pcVar2 != (char *)0x47504653) goto LAB_00104fc8;
                      pcVar3 = "gpfs";
                    }
                  }
                }
              }
            }
            else {
              pcVar3 = "ntfs";
              if (pcVar2 != (char *)0x5346544e) {
                if ((long)pcVar2 < 0x5346544f) {
                  pcVar3 = "afs";
                  if (pcVar2 != (char *)0x5346414f) {
                    if (pcVar2 == (char *)0x53464846) {
                      pcVar3 = "wslfs";
                    }
                    else {
                      if (pcVar2 != (char *)0x5346314d) goto LAB_00104fc8;
                      pcVar3 = "m1fs";
                    }
                  }
                }
                else {
                  pcVar3 = "ufs";
                  if (pcVar2 != (char *)0x54190100) {
                    if (pcVar2 == (char *)0x565a4653) {
                      pcVar3 = "vzfs";
                    }
                    else {
                      if (pcVar2 != (char *)0x534f434b) goto LAB_00104fc8;
                      pcVar3 = "sockfs";
                    }
                  }
                }
              }
            }
          }
        }
        else {
          pcVar3 = "bdevfs";
          if (pcVar2 != (char *)0x62646576) {
            if ((long)pcVar2 < 0x62646577) {
              pcVar3 = "sdcardfs";
              if (pcVar2 != (char *)0x5dca2df5) {
                if ((long)pcVar2 < 0x5dca2df6) {
                  pcVar3 = "aafs";
                  if (pcVar2 != (char *)0x5a3c69f0) {
                    if (pcVar2 == (char *)0x5a4f4653) {
                      pcVar3 = "zonefs";
                    }
                    else {
                      if (pcVar2 != (char *)0x58465342) goto LAB_00104fc8;
                      pcVar3 = "xfs";
                    }
                  }
                }
                else {
                  pcVar3 = "pstorefs";
                  if (pcVar2 != (char *)0x6165676c) {
                    if (pcVar2 == (char *)0x61756673) {
                      pcVar3 = "aufs";
                    }
                    else {
                      if (pcVar2 != (char *)0x61636673) goto LAB_00104fc8;
                      pcVar3 = "acfs";
                    }
                  }
                }
              }
            }
            else {
              pcVar3 = "debugfs";
              if (pcVar2 != (char *)0x64626720) {
                if ((long)pcVar2 < 0x64626721) {
                  pcVar3 = "sysfs";
                  if (pcVar2 != (char *)0x62656572) {
                    if (pcVar2 == (char *)0x63677270) {
                      pcVar3 = "cgroup2fs";
                    }
                    else {
                      if (pcVar2 != (char *)0x62656570) goto LAB_00104fc8;
                      pcVar3 = "configfs";
                    }
                  }
                }
                else {
                  pcVar3 = "fusectl";
                  if (pcVar2 != (char *)0x65735543) {
                    if (pcVar2 == (char *)0x65735546) {
                      pcVar3 = "fuseblk";
                    }
                    else {
                      if (pcVar2 != (char *)0x64646178) goto LAB_00104fc8;
                      pcVar3 = "daxfs";
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    else {
      pcVar3 = "hugetlbfs";
      if (pcVar2 != (char *)0x958458f6) {
        if ((long)pcVar2 < 0x958458f7) {
          pcVar3 = "coda";
          if (pcVar2 != (char *)0x73757245) {
            if ((long)pcVar2 < 0x73757246) {
              pcVar3 = "nsfs";
              if (pcVar2 != (char *)0x6e736673) {
                if ((long)pcVar2 < 0x6e736674) {
                  pcVar3 = "binderfs";
                  if (pcVar2 != (char *)0x6c6f6f70) {
                    if ((long)pcVar2 < 0x6c6f6f71) {
                      pcVar3 = "qnx6";
                      if (pcVar2 != (char *)0x68191122) {
                        if (pcVar2 != (char *)0x6b414653) goto LAB_00104fc8;
                        pcVar3 = "k-afs";
                      }
                    }
                    else {
                      if (pcVar2 != (char *)0x6e667364) goto LAB_00104fc8;
                      pcVar3 = "nfsd";
                    }
                  }
                }
                else {
                  pcVar3 = "squashfs";
                  if (pcVar2 != (char *)0x73717368) {
                    if (pcVar2 == (char *)0x73727279) {
                      pcVar3 = "btrfs_test";
                    }
                    else {
                      if (pcVar2 != (char *)0x73636673) goto LAB_00104fc8;
                      pcVar3 = "securityfs";
                    }
                  }
                }
              }
            }
            else {
              pcVar3 = "overlayfs";
              if (pcVar2 != (char *)0x794c7630) {
                if ((long)pcVar2 < 0x794c7631) {
                  pcVar3 = "tracefs";
                  if (pcVar2 != (char *)0x74726163) {
                    if (pcVar2 == (char *)0x786f4256) {
                      pcVar3 = "vboxsf";
                    }
                    else {
                      if (pcVar2 != (char *)0x7461636f) goto LAB_00104fc8;
                      pcVar3 = "ocfs2";
                    }
                  }
                }
                else {
                  pcVar3 = "ramfs";
                  if (pcVar2 != (char *)0x858458f6) {
                    if (pcVar2 == (char *)0x9123683e) {
                      pcVar3 = "btrfs";
                    }
                    else {
                      if (pcVar2 != (char *)0x7c7c6673) goto LAB_00104fc8;
                      pcVar3 = "prl_fs";
                    }
                  }
                }
              }
            }
          }
        }
        else {
          pcVar3 = "bpf_fs";
          if (pcVar2 != (char *)0xcafe4a11) {
            if ((long)pcVar2 < 0xcafe4a12) {
              pcVar3 = "vmhgfs";
              if (pcVar2 != (char *)0xbacbacbc) {
                if ((long)pcVar2 < 0xbacbacbd) {
                  pcVar3 = "panfs";
                  if (pcVar2 != (char *)0xaad7aaea) {
                    if (pcVar2 == (char *)0xabba1974) {
                      pcVar3 = "xenfs";
                    }
                    else {
                      if (pcVar2 != (char *)0xa501fcf5) goto LAB_00104fc8;
                      pcVar3 = "vxfs";
                    }
                  }
                }
                else {
                  pcVar3 = "ppc-cmm-fs";
                  if (pcVar2 != (char *)0xc7571590) {
                    if (pcVar2 == (char *)0xc97e8168) {
                      pcVar3 = "logfs";
                    }
                    else {
                      if (pcVar2 != (char *)0xbeefdead) goto LAB_00104fc8;
                      pcVar3 = "snfs";
                    }
                  }
                }
              }
            }
            else {
              pcVar3 = "selinux";
              if (pcVar2 != (char *)0xf97cff8c) {
                if ((long)pcVar2 < 0xf97cff8d) {
                  pcVar3 = "erofs";
                  if (pcVar2 != (char *)0xe0f5e1e2) {
                    if (pcVar2 == (char *)0xf2f52010) {
                      pcVar3 = "f2fs";
                    }
                    else {
                      if (pcVar2 != (char *)0xde5e81e4) goto LAB_00104fc8;
                      pcVar3 = "efivarfs";
                    }
                  }
                }
                else {
                  pcVar3 = "smb2";
                  if (pcVar2 != (char *)0xfe534d42) {
                    if (pcVar2 == (char *)0xff534d42) {
                      pcVar3 = "cifs";
                    }
                    else {
                      if (pcVar2 != (char *)0xf995e849) goto LAB_00104fc8;
                      pcVar3 = "hpfs";
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    puVar1 = &DAT_00112bd3;
    pcVar2 = "-";
    break;
  default:
    pcVar3 = stdout->_IO_write_ptr;
    if (pcVar3 < stdout->_IO_write_end) {
      stdout->_IO_write_ptr = pcVar3 + 1;
      *pcVar3 = '?';
      return 0;
    }
    __overflow(stdout,0x3f);
    return 0;
  case 0x61:
    pcVar3 = param_7[4];
    goto LAB_00104639;
  case 0x62:
    pcVar3 = param_7[2];
    goto LAB_00104639;
  case 99:
    pcVar3 = param_7[5];
LAB_00104222:
    puVar1 = &DAT_0011260f;
    pcVar2 = "\'-0";
    break;
  case 100:
    pcVar3 = param_7[6];
    goto LAB_00104639;
  case 0x66:
    pcVar3 = param_7[3];
LAB_00104639:
    puVar1 = &DAT_00112603;
    pcVar2 = "\'-+ 0";
    break;
  case 0x69:
    puVar1 = &DAT_00112608;
    pcVar2 = "-#0";
    pcVar3 = (char *)CONCAT44(*(undefined4 *)(param_7 + 7),*(undefined4 *)((long)param_7 + 0x3c));
    break;
  case 0x6c:
    puVar1 = &DAT_0011260f;
    pcVar2 = "\'-0";
    pcVar3 = param_7[8];
    break;
  case 0x6e:
    make_format(param_1,param_2,&DAT_00112606,&DAT_00112bd3);
    __printf_chk(1,param_1,param_6);
    return 0;
  case 0x73:
    puVar1 = &DAT_0011260f;
    pcVar2 = "\'-0";
    pcVar3 = param_7[1];
    break;
  case 0x74:
    puVar1 = &DAT_00112608;
    pcVar2 = "-#0";
    pcVar3 = *param_7;
  }
  make_format(param_1,param_2,pcVar2,puVar1);
  __printf_chk(1,param_1,pcVar3);
  return 0;
}