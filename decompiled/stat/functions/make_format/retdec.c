void make_format(char * pformat, int64_t prefix_len, char * allowed_flags, char * suffix) {
    int64_t v1 = (int64_t)pformat;
    uint64_t v2 = v1 + prefix_len; // 0x31b2
    int64_t v3 = v1 + 1; // 0x31c7
    if (v3 >= v2) {
        // 0x324e
        function_2610();
        return;
    }
    uint64_t v4 = 0x100000000 * v2 / 0x100000000;
    int64_t v5 = v3;
    uint64_t v6 = v3;
    char v7 = *(char *)v6; // 0x320a
    while (function_2740() != 0) {
        int64_t v8 = v5; // 0x31f6
        if (function_2740() != 0) {
            // 0x31f8
            *(char *)v5 = v7;
            v8 = v5 + 1;
        }
        int64_t v9 = v6 + 1; // 0x3200
        if (v9 >= v4) {
            // 0x324e
            function_2610();
            return;
        }
        v5 = v8;
        v6 = v9;
        v7 = *(char *)v6;
    }
    if (v4 <= v6) {
        // 0x324e
        function_2610();
        return;
    }
    int64_t v10 = v4 - v6; // 0x322f
    *(char *)v5 = v7;
    if (v10 == 1) {
        // 0x324e
        function_2610();
        return;
    }
    int64_t v11 = 1;
    *(char *)(v11 + v5) = *(char *)(v11 + v6);
    int64_t v12 = v11 + 1; // 0x3242
    while (v10 != v12) {
        // 0x3238
        v11 = v12;
        *(char *)(v11 + v5) = *(char *)(v11 + v6);
        v12 = v11 + 1;
    }
    // 0x324e
    function_2610();
}