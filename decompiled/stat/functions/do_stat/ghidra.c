uint do_stat(char *param_1,char *param_2,char *param_3)

{
  char cVar1;
  int iVar2;
  uint uVar3;
  char *pcVar4;
  int *piVar5;
  undefined8 uVar6;
  undefined8 uVar7;
  long lVar8;
  byte bVar9;
  char *pcVar10;
  undefined8 *puVar11;
  ushort uVar12;
  long in_FS_OFFSET;
  uint local_20c;
  char *local_208;
  ulong *local_1f8;
  undefined8 local_1f0;
  ulong local_1e8;
  ulong local_1d8;
  undefined8 local_1d0;
  ulong local_1c8;
  uint local_1c0;
  undefined8 local_1bc;
  ulong local_1b0;
  undefined8 local_1a8;
  ulong local_1a0;
  undefined8 local_198;
  undefined8 local_190;
  ulong local_188;
  undefined8 local_180;
  ulong local_178;
  undefined8 local_170;
  ulong local_168;
  undefined8 local_148;
  uint local_138;
  undefined8 local_134;
  ushort local_12c;
  undefined8 local_128;
  undefined8 local_120;
  undefined8 local_118;
  undefined8 local_108;
  uint local_100;
  undefined8 local_f8;
  uint local_f0;
  undefined8 local_e8;
  uint local_e0;
  undefined8 local_d8;
  uint local_d0;
  uint local_c8;
  uint local_c4;
  uint local_c0;
  uint local_bc;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if ((*param_1 == '-') && (local_20c = (uint)(byte)param_1[1], local_20c == 0)) {
    uVar3 = 0x1000;
    puVar11 = &local_148;
    for (lVar8 = 0x20; lVar8 != 0; lVar8 = lVar8 + -1) {
      *puVar11 = 0;
      puVar11 = puVar11 + 1;
    }
    local_208 = "";
  }
  else {
    puVar11 = &local_148;
    for (lVar8 = 0x20; lVar8 != 0; lVar8 = lVar8 + -1) {
      *puVar11 = 0;
      puVar11 = puVar11 + 1;
    }
    uVar3 = (follow_links ^ 1) << 8;
    local_20c = 0xffffff9c;
    local_208 = param_1;
  }
  local_1f8 = &local_1d8;
  local_1e8 = 0xffffffffffffffff;
  local_1f0 = 0xffffffffffffffff;
  if (dont_sync == '\0') {
    if (force_sync != '\0') {
      uVar3 = uVar3 | 0x2000;
      goto LAB_00103adb;
    }
  }
  else {
    uVar3 = uVar3 | 0x4000;
    if (force_sync != '\0') goto LAB_00103adb;
  }
  uVar3 = uVar3 | 0x800;
LAB_00103adb:
  uVar12 = 0;
  cVar1 = *param_2;
  pcVar10 = param_2;
  while (cVar1 != '\0') {
    while (cVar1 == '%') {
      lVar8 = format_code_offset(pcVar10);
      pcVar4 = pcVar10 + lVar8;
      if (*pcVar4 == '\0') goto LAB_00103b3c;
      bVar9 = *pcVar4 + 0xbf;
      if (bVar9 < 0x3a) {
        uVar12 = uVar12 | *(ushort *)(CSWTCH_124 + (ulong)bVar9 * 2);
      }
      pcVar10 = pcVar4 + 1;
      cVar1 = pcVar4[1];
      if (cVar1 == '\0') goto LAB_00103b3c;
    }
    cVar1 = pcVar10[1];
    pcVar10 = pcVar10 + 1;
  }
LAB_00103b3c:
  iVar2 = statx(local_20c,local_208,uVar3,uVar12,&local_148);
  if (iVar2 < 0) {
    piVar5 = __errno_location();
    if ((uVar3 & 0x1000) == 0) {
      uVar6 = quotearg_style(4,param_1);
      uVar7 = dcgettext(0,"cannot statx %s",5);
      error(0,*piVar5,uVar7,uVar6);
      uVar3 = 0;
    }
    else {
      uVar6 = dcgettext(0,"cannot stat standard input",5);
      error(0,*piVar5,uVar6);
      uVar3 = 0;
    }
  }
  else {
    local_1c0 = (uint)local_12c;
    if ((local_12c & 0xb000) != 0x2000) {
      param_3 = param_2;
    }
    local_1d8 = ((ulong)local_bc & 0xffffff00) << 0xc |
                ((ulong)local_c0 & 0xfffff000) << 0x20 | (ulong)((local_c0 & 0xfff) << 8) |
                (ulong)(byte)local_bc;
    local_1d0 = local_128;
    local_1c8 = (ulong)local_138;
    local_1bc = local_134;
    local_1b0 = ((ulong)local_c4 & 0xffffff00) << 0xc |
                ((ulong)local_c8 & 0xfffff000) << 0x20 | (ulong)((local_c8 & 0xfff) << 8) |
                (ulong)(byte)local_c4;
    local_1a8 = local_120;
    local_1a0 = (ulong)local_148._4_4_;
    local_198 = local_118;
    local_190 = local_108;
    local_188 = (ulong)local_100;
    local_180 = local_d8;
    local_178 = (ulong)local_d0;
    local_170 = local_e8;
    local_168 = (ulong)local_e0;
    if ((local_148._1_1_ & 8) != 0) {
      local_1f0 = local_f8;
      local_1e8 = (ulong)local_f0;
    }
    uVar3 = print_it(param_3,iVar2,param_1,print_stat,&local_1f8);
    uVar3 = uVar3 ^ 1;
  }
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar3;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}