bool do_stat(char * filename, char * format, char * format2) {
    // 0x3a30
    int32_t v1; // 0x3a30
    uint32_t v2 = v1;
    int64_t v3 = __readfsqword(40); // 0x3a4b
    int64_t v4; // 0x3a30
    int64_t v5; // bp-328, 0x3a30
    int64_t v6; // bp-472, 0x3a30
    int64_t v7; // bp-504, 0x3a30
    int64_t v8; // 0x3a30
    if ((char)v8 != 45) {
        goto lab_0x3d10;
    } else {
        // 0x3a6f
        if (*(char *)((int64_t)filename + 1) != 0) {
            goto lab_0x3d10;
        } else {
            // 0x3a80
            __asm_rep_stosq_memset((char *)&v5, 0, 32);
            v7 = &v6;
            v4 = (int64_t)"hr";
            goto lab_0x3abc;
        }
    }
  lab_0x3d10:
    // 0x3d10
    __asm_rep_stosq_memset((char *)&v5, 0, 32);
    v7 = &v6;
    v4 = 256 * (int64_t)(*(char *)&follow_links ^ 1);
    goto lab_0x3abc;
  lab_0x3abc:;
    char v9 = *(char *)&force_sync; // 0x3ac3
    int64_t v10; // 0x3a30
    if (*(char *)&dont_sync == 0) {
        // 0x3d00
        if (v9 != 0) {
            // 0x3d80
            v10 = v4 | 0x2000;
            goto lab_0x3adb;
        } else {
            // 0x3d04
            v10 = v4 | 2048;
            goto lab_0x3adb;
        }
    } else {
        int64_t v11 = v4 | 0x4000; // 0x3ad0
        v10 = v11;
        if (v9 == 0) {
            // 0x3d04
            v10 = v11 | 2048;
            goto lab_0x3adb;
        } else {
            goto lab_0x3adb;
        }
    }
  lab_0x3adb:;
    char v12 = v8;
    if (v12 != 0) {
        char v13 = v12; // 0x3b34
        int64_t v14 = (int64_t)format;
        int64_t v15; // 0x3b30
        while (v13 != 37) {
            // 0x3b2d
            v15 = v14 + 1;
            v13 = *(char *)v15;
            if (v13 == 0) {
                // break (via goto) -> 0x3b3c
                goto lab_0x3b3c;
            }
            v14 = v15;
        }
        int64_t v16 = format_code_offset((char *)v14) + v14; // 0x3b00
        while (*(char *)v16 != 0) {
            int64_t v17 = v16 + 1; // 0x3b1d
            char v18 = *(char *)v17; // 0x3b21
            if (v18 == 0) {
                // break -> 0x3b3c
                break;
            }
            v13 = v18;
            v14 = v17;
            while (v13 != 37) {
                // 0x3b2d
                v15 = v14 + 1;
                v13 = *(char *)v15;
                if (v13 == 0) {
                    // break (via goto) -> 0x3b3c
                    goto lab_0x3b3c;
                }
                v14 = v15;
            }
            // 0x3af8
            v16 = format_code_offset((char *)v14) + v14;
        }
    }
  lab_0x3b3c:;
    int32_t v19 = function_2970(); // 0x3b56
    int64_t v20; // 0x3a30
    if (v19 < 0) {
        // 0x3d90
        function_25e0();
        if ((v10 & 0x1000) == 0) {
            // 0x3dd0
            quotearg_style();
            function_26c0();
            function_2990();
            v20 = 0;
        } else {
            // 0x3d9d
            function_26c0();
            function_2990();
            v20 = 0;
        }
    } else {
        char * v21 = (v1 & 0xb000) != 0x2000 ? format : format2;
        int64_t v22 = 0x100000000 * (int64_t)v1; // 0x3b98
        int64_t v23 = v1 < 0xfffff001 ? v22 : v22 + 0xfffffffffff; // 0x3ba3
        v6 = 0x1000 * (int64_t)v2 & 0xffffff00000 | (int64_t)(v2 % 256) | (int64_t)(256 * v1 & 0xfff00) | v23;
        bool v24 = print_it(v21, v19, filename, (bool (*)(char *, int64_t, char, char, int32_t, char *, int32_t *))0x5000, (int32_t *)&v7); // 0x3ccf
        v20 = !v24;
    }
    // 0x3cd7
    if (v3 != __readfsqword(40)) {
        // 0x3e0c
        return function_2710() % 2 != 0;
    }
    // 0x3cee
    return v20 != 0;
}