void** find_bind_mount(void** rdi, int64_t rsi) {
    void** r12_3;
    void** rsp4;
    void** rax5;
    void** v6;
    int1_t zf7;
    struct s4* rax8;
    void** rax9;
    void** rcx10;
    void** rsi11;
    void** rdi12;
    int32_t eax13;
    int64_t* rsp14;
    struct s4* rbx15;
    void** rax16;
    void** r13_17;
    void** rbp18;
    uint32_t eax19;
    int32_t eax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    void** rdx25;
    void*** rsp26;
    void** rdi27;
    void** rax28;
    void** r8_29;
    void** r9_30;
    void** rax31;
    void** rax32;
    void** r12_33;
    void** rbx34;
    int64_t rax35;
    void** rax36;
    void** r9_37;
    void** rdx38;
    void** r8_39;
    void** rcx40;
    void* rax41;
    int64_t v42;
    void* rax43;
    void** rbx44;
    void* rax45;
    struct s5* rbx46;

    r12_3 = rdi;
    rsp4 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x138);
    rax5 = g28;
    v6 = rax5;
    zf7 = tried_mount_list_3 == 0;
    if (zf7) {
        rax8 = read_file_system_list();
        rsp4 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp4 - 8) + 8);
        mount_list_2 = rax8;
        if (!rax8) {
            rax9 = fun_26c0();
            fun_25e0();
            rcx10 = rax9;
            fun_2990();
            rsp4 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp4 - 8) + 8 - 8 + 8 - 8 + 8);
        }
        tried_mount_list_3 = 1;
    }
    rsi11 = rsp4;
    rdi12 = r12_3;
    eax13 = fun_2860(rdi12, rsi11, "%s", rcx10);
    rsp14 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp4 - 8) + 8);
    if (eax13 || (rbx15 = mount_list_2, rbx15 == 0)) {
        addr_3328_6:
        *reinterpret_cast<int32_t*>(&rax16) = 0;
        *reinterpret_cast<int32_t*>(&rax16 + 4) = 0;
    } else {
        r13_17 = reinterpret_cast<void**>(rsp14 + 18);
        do {
            if (!(rbx15->f28 & 1)) 
                continue;
            rbp18 = rbx15->f0;
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbp18) == 47)) 
                continue;
            rdi12 = rbx15->f8;
            rsi11 = r12_3;
            eax19 = fun_2840(rdi12, rsi11, "%s", rdi12, rsi11, "%s");
            rsp14 = rsp14 - 1 + 1;
            if (eax19) 
                continue;
            rsi11 = r13_17;
            rdi12 = rbp18;
            eax20 = fun_2860(rdi12, rsi11, "%s", rcx10);
            rsp14 = rsp14 - 1 + 1;
            if (eax20) 
                continue;
            if (v21 != v22) 
                continue;
            if (v23 == v24) 
                goto addr_331c_14;
            rbx15 = rbx15->f30;
        } while (rbx15);
        goto addr_3328_6;
    }
    addr_332a_16:
    rdx25 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v6) - reinterpret_cast<unsigned char>(g28));
    if (!rdx25) {
        return rax16;
    }
    fun_2710();
    rsp26 = reinterpret_cast<void***>(rsp14 - 1 + 1 - 1 - 1 - 15);
    rdi27 = tz_1;
    rax28 = g28;
    if (rdi27) 
        goto addr_33e0_20;
    rax31 = fun_2590("TZ", rsi11, rdx25, rcx10, r8_29, r9_30);
    rax32 = tzalloc(rax31, rsi11);
    rsp26 = rsp26 - 8 + 8 - 8 + 8;
    tz_1 = rax32;
    rdi27 = rax32;
    addr_33e0_20:
    r12_33 = reinterpret_cast<void**>(rsp26 + 16);
    rbx34 = rsi11;
    rax35 = localtime_rz(rdi27, rsp26, r12_33, rcx10);
    if (!rax35) {
        rax36 = imaxtostr(rdi12, rsp26 - 8 + 8 + 80, r12_33, rcx10);
        *reinterpret_cast<int32_t*>(&r9_37) = *reinterpret_cast<int32_t*>(&rbx34);
        *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdx38) = 61;
        *reinterpret_cast<int32_t*>(&rdx38 + 4) = 0;
        r8_39 = rax36;
        rcx40 = reinterpret_cast<void**>("%s.%09d");
        fun_2a90(0x18100, 1, 61, "%s.%09d", r8_39, r9_37, rdi12);
    } else {
        rcx40 = r12_33;
        *reinterpret_cast<int32_t*>(&r9_37) = *reinterpret_cast<int32_t*>(&rbx34);
        *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
        r8_39 = tz_1;
        rdx38 = reinterpret_cast<void**>("%Y-%m-%d %H:%M:%S.%N %z");
        nstrftime();
    }
    rax41 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax28) - reinterpret_cast<unsigned char>(g28));
    if (!rax41) {
        goto v42;
    }
    fun_2710();
    rax43 = fun_27e0(0x18101, "'-+ #0I", rdx38, rcx40, r8_39, r9_37);
    rbx44 = reinterpret_cast<void**>(0x18100 + reinterpret_cast<uint64_t>(rax43) + 1);
    rax45 = fun_27e0(rbx44, "0123456789", rdx38, rcx40, r8_39, r9_37);
    rbx46 = reinterpret_cast<struct s5*>(reinterpret_cast<unsigned char>(rbx44) + reinterpret_cast<uint64_t>(rax45));
    if (rbx46->f0 == 46) 
        goto addr_34e6_28;
    addr_34f7_29:
    goto rdi12;
    addr_34e6_28:
    fun_27e0(&rbx46->f1, "0123456789", rdx38, rcx40, r8_39, r9_37);
    goto addr_34f7_29;
    addr_331c_14:
    rax16 = rbx15->f0;
    goto addr_332a_16;
}