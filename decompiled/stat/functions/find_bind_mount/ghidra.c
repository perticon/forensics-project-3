char * find_bind_mount(char *param_1)

{
  int iVar1;
  char *pcVar2;
  undefined8 uVar3;
  int *piVar4;
  char **ppcVar5;
  long in_FS_OFFSET;
  stat local_158;
  stat local_c8;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (tried_mount_list_3 == '\0') {
    mount_list_2 = (char **)read_file_system_list(0);
    if (mount_list_2 == (char **)0x0) {
      uVar3 = dcgettext(0,"cannot read table of mounted file systems",5);
      piVar4 = __errno_location();
      error(0,*piVar4,&DAT_00112bd2,uVar3);
    }
    tried_mount_list_3 = '\x01';
  }
  iVar1 = stat(param_1,&local_158);
  if ((iVar1 == 0) && (mount_list_2 != (char **)0x0)) {
    ppcVar5 = mount_list_2;
    do {
      if (((*(byte *)(ppcVar5 + 5) & 1) != 0) && (pcVar2 = *ppcVar5, *pcVar2 == '/')) {
        iVar1 = strcmp(ppcVar5[1],param_1);
        if (iVar1 == 0) {
          iVar1 = stat(pcVar2,&local_c8);
          if (((iVar1 == 0) && (local_158.st_ino == local_c8.st_ino)) &&
             (local_158.st_dev == local_c8.st_dev)) {
            pcVar2 = *ppcVar5;
            goto LAB_0010332a;
          }
        }
      }
      ppcVar5 = (char **)ppcVar5[6];
    } while (ppcVar5 != (char **)0x0);
  }
  pcVar2 = (char *)0x0;
LAB_0010332a:
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return pcVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}