long format_code_offset(long param_1)

{
  size_t sVar1;
  char *pcVar2;
  
  sVar1 = strspn((char *)(param_1 + 1),"\'-+ #0I");
  pcVar2 = (char *)(param_1 + 1 + sVar1);
  sVar1 = strspn(pcVar2,digits);
  pcVar2 = pcVar2 + sVar1;
  if (*pcVar2 == '.') {
    sVar1 = strspn(pcVar2 + 1,digits);
    pcVar2 = pcVar2 + sVar1 + 1;
  }
  return (long)pcVar2 - param_1;
}