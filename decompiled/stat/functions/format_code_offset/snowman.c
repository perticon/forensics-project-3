void** format_code_offset(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, ...) {
    void* rax7;
    void** rbx8;
    void* rax9;
    struct s10* rbx10;
    void* rax11;

    rax7 = fun_27e0(rdi + 1, "'-+ #0I", rdx, rcx, r8, r9);
    rbx8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<uint64_t>(rax7) + 1);
    rax9 = fun_27e0(rbx8, "0123456789", rdx, rcx, r8, r9);
    rbx10 = reinterpret_cast<struct s10*>(reinterpret_cast<unsigned char>(rbx8) + reinterpret_cast<uint64_t>(rax9));
    if (rbx10->f0 == 46) {
        rax11 = fun_27e0(&rbx10->f1, "0123456789", rdx, rcx, r8, r9);
        rbx10 = reinterpret_cast<struct s10*>(reinterpret_cast<uint64_t>(rbx10) + reinterpret_cast<uint64_t>(rax11) + 1);
    }
    return reinterpret_cast<uint64_t>(rbx10) - reinterpret_cast<unsigned char>(rdi);
}