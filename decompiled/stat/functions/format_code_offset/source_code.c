format_code_offset (char const *directive)
{
  size_t len = strspn (directive + 1, printf_flags);
  char const *fmt_char = directive + len + 1;
  fmt_char += strspn (fmt_char, digits);
  if (*fmt_char == '.')
    fmt_char += 1 + strspn (fmt_char + 1, digits);
  return fmt_char - directive;
}