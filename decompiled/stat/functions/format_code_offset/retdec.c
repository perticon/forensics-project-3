int64_t format_code_offset(char * directive) {
    int64_t v1 = (int64_t)directive;
    int64_t v2 = function_27e0(); // 0x34c9
    int64_t v3 = v1 + 1 + v2 + function_27e0(); // 0x34de
    int64_t v4 = v3; // 0x34e4
    if (*(char *)v3 == 46) {
        // 0x34e6
        v4 = v3 + 1 + function_27e0();
    }
    // 0x34f7
    return v4 - v1;
}