fmt_to_mask (char fmt)
{
  switch (fmt)
    {
    case 'N':
      return STATX_MODE;
    case 'd':
    case 'D':
      return STATX_MODE;
    case 'i':
      return STATX_INO;
    case 'a':
    case 'A':
      return STATX_MODE;
    case 'f':
      return STATX_MODE|STATX_TYPE;
    case 'F':
      return STATX_TYPE;
    case 'h':
      return STATX_NLINK;
    case 'u':
    case 'U':
      return STATX_UID;
    case 'g':
    case 'G':
      return STATX_GID;
    case 'm':
      return STATX_MODE|STATX_INO;
    case 's':
      return STATX_SIZE;
    case 't':
    case 'T':
      return STATX_MODE;
    case 'b':
      return STATX_BLOCKS;
    case 'w':
    case 'W':
      return STATX_BTIME;
    case 'x':
    case 'X':
      return STATX_ATIME;
    case 'y':
    case 'Y':
      return STATX_MTIME;
    case 'z':
    case 'Z':
      return STATX_CTIME;
    }
  return 0;
}