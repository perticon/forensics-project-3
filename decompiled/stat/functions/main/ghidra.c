void main(int param_1,undefined8 *param_2)

{
  undefined uVar1;
  byte bVar2;
  int iVar3;
  lconv *plVar4;
  undefined8 uVar5;
  long lVar6;
  char *pcVar7;
  undefined8 uVar8;
  int *piVar9;
  char cVar10;
  byte bVar11;
  long in_FS_OFFSET;
  char *local_c8;
  char *local_c0;
  statfs local_b8;
  undefined8 local_40;
  
  local_40 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  plVar4 = localeconv();
  decimal_point = plVar4->decimal_point;
  if (*decimal_point == '\0') {
    decimal_point = ".";
  }
  cVar10 = '\0';
  decimal_point_len = strlen(decimal_point);
  atexit(close_stdout);
  uVar1 = 0;
  local_c8 = (char *)0x0;
LAB_00102bb0:
  while( true ) {
    uVar5 = getopt_long(param_1,param_2,"c:fLt",long_options,0);
    iVar3 = (int)uVar5;
    if (iVar3 == -1) {
      if (optind != param_1) {
        if (local_c8 == (char *)0x0) {
          local_c8 = (char *)default_format(cVar10,uVar1,0);
          local_c0 = (char *)default_format(cVar10,uVar1,1);
        }
        else {
          pcVar7 = strstr(local_c8,"%N");
          local_c0 = local_c8;
          if (pcVar7 != (char *)0x0) {
            pcVar7 = getenv("QUOTING_STYLE");
            if (pcVar7 == (char *)0x0) {
              set_quoting_style(0,4);
            }
            else {
              iVar3 = argmatch(pcVar7,quoting_style_args,quoting_style_vals,4);
              if (iVar3 < 0) {
                set_quoting_style(0,4);
                uVar5 = quote();
                uVar8 = dcgettext(0,
                                  "ignoring invalid value of environment variable QUOTING_STYLE: %s"
                                  ,5);
                error(0,0,uVar8,uVar5);
              }
              else {
                set_quoting_style(0,*(undefined4 *)(quoting_style_vals + (long)iVar3 * 4));
              }
            }
          }
        }
        bVar11 = 1;
        for (lVar6 = (long)optind; (int)lVar6 < param_1; lVar6 = lVar6 + 1) {
          pcVar7 = (char *)param_2[lVar6];
          if (cVar10 == '\0') {
            bVar2 = do_stat(pcVar7,local_c8,local_c0);
          }
          else {
            iVar3 = strcmp(pcVar7,"-");
            if (iVar3 == 0) {
              uVar5 = quotearg_style(4,pcVar7);
              uVar8 = dcgettext(0,
                                "using %s to denote standard input does not work in file system mode"
                                ,5);
              error(0,0,uVar8,uVar5);
              bVar2 = 0;
            }
            else {
              iVar3 = statfs(pcVar7,&local_b8);
              if (iVar3 == 0) {
                bVar2 = print_it(local_c8,0xffffffff,pcVar7,print_statfs);
                bVar2 = bVar2 ^ 1;
              }
              else {
                uVar5 = quotearg_style(4,pcVar7);
                uVar8 = dcgettext(0,"cannot read file system information for %s",5);
                piVar9 = __errno_location();
                error(0,*piVar9,uVar8,uVar5);
                bVar2 = 0;
              }
            }
          }
          bVar11 = bVar11 & bVar2;
        }
                    /* WARNING: Subroutine does not return */
        exit((uint)(bVar11 ^ 1));
      }
      uVar5 = dcgettext(0,"missing operand",5);
      error(0,0,uVar5);
      goto LAB_00102c83;
    }
    if (iVar3 != 99) break;
    interpret_backslash_escapes = 0;
    local_c8 = optarg;
    trailing_delim = "\n";
  }
  if (iVar3 < 100) {
    if (iVar3 == 0) goto LAB_00102c8d;
    if (iVar3 < 1) {
      if (iVar3 == -0x83) {
        version_etc(stdout,&DAT_0011266f,"GNU coreutils",Version,"Michael Meskes",0);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar3 == -0x82) {
        uVar5 = usage();
        goto LAB_00102c00;
      }
      goto LAB_00102c83;
    }
    if (iVar3 == 0x4c) {
      follow_links = 1;
      goto LAB_00102bb0;
    }
  }
  else {
LAB_00102c00:
    iVar3 = (int)uVar5;
    if (iVar3 == 0x74) {
      uVar1 = 1;
      goto LAB_00102bb0;
    }
    if (iVar3 == 0x80) {
      local_c8 = optarg;
      interpret_backslash_escapes = 1;
      trailing_delim = "";
      goto LAB_00102bb0;
    }
    if (iVar3 == 0x66) {
      cVar10 = '\x01';
      goto LAB_00102bb0;
    }
  }
LAB_00102c83:
  uVar5 = usage(1);
LAB_00102c8d:
  lVar6 = __xargmatch_internal("--cached",optarg,cached_args,cached_modes,4,argmatch_die,1,uVar5);
  iVar3 = *(int *)(cached_modes + lVar6 * 4);
  if (iVar3 == 1) {
    force_sync = 1;
    dont_sync = 0;
  }
  else if (iVar3 == 2) {
    force_sync = 0;
    dont_sync = 1;
  }
  else if (iVar3 == 0) {
    force_sync = 0;
    dont_sync = 0;
  }
  goto LAB_00102bb0;
}