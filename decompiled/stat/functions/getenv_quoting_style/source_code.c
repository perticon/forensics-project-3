getenv_quoting_style (void)
{
  char const *q_style = getenv ("QUOTING_STYLE");
  if (q_style)
    {
      int i = ARGMATCH (q_style, quoting_style_args, quoting_style_vals);
      if (0 <= i)
        set_quoting_style (NULL, quoting_style_vals[i]);
      else
        {
          set_quoting_style (NULL, shell_escape_always_quoting_style);
          error (0, 0, _("ignoring invalid value of environment "
                         "variable QUOTING_STYLE: %s"), quote (q_style));
        }
    }
  else
    set_quoting_style (NULL, shell_escape_always_quoting_style);
}