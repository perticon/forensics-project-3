format_to_mask (char const *format)
{
  unsigned int mask = 0;
  char const *b;

  for (b = format; *b; b++)
    {
      if (*b != '%')
        continue;

      b += format_code_offset (b);
      if (*b == '\0')
        break;
      mask |= fmt_to_mask (*b);
    }
  return mask;
}