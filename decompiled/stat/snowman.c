
void** fun_26e0(void** rdi, ...);

void** xmalloc(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, ...);

void** format_code_offset(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, ...);

void** fun_2880(void** rdi, void** rsi, void** rdx, ...);

void** stdout = reinterpret_cast<void**>(0);

void fun_2760();

signed char interpret_backslash_escapes = 0;

void** fun_2a80(void** rdi, void** rsi, void** rdx, void** rcx, ...);

void** fun_26c0();

void fun_2990();

void** quote(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** g28;

signed char g1;

unsigned char g2;

unsigned char follow_links = 0;

signed char dont_sync = 0;

unsigned char force_sync = 0;

void** localtime_r;

struct s0 {
    unsigned char f0;
    void** f1;
};

void** fun_2970(int64_t rdi, void** rsi, void** rdx);

void** fun_25e0();

void** quotearg_style(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7);

void fun_2710();

struct s1 {
    signed char f0;
    void** f1;
};

struct s1* fun_27f0(void** rdi, int64_t rsi, void** rdx);

int64_t fun_2870(struct s1* rdi);

void** decimal_point_len = reinterpret_cast<void**>(0);

void make_format(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7);

int64_t fun_2960(int64_t rdi, void** rsi, void** rdx, void** rcx, ...);

void** decimal_point = reinterpret_cast<void**>(0);

int32_t fun_2a90(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7);

void fun_25b0(void** rdi, ...);

void** trailing_delim = reinterpret_cast<void**>(65);

void fun_2810(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, void*** a8, void** a9, void** a10, int64_t a11, void*** a12, int64_t a13, int64_t a14, int64_t a15, int64_t a16, int64_t a17, int64_t a18, void** a19, int64_t a20);

uint32_t print_it(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, int64_t a8) {
    int64_t v9;
    int64_t v10;
    int64_t r15_11;
    int64_t v12;
    int64_t r14_13;
    int64_t v14;
    int64_t r13_15;
    void** r13_16;
    int64_t v17;
    int64_t r12_18;
    uint32_t r12d19;
    int64_t v20;
    int64_t rbp21;
    void*** v22;
    void*** rbx23;
    void*** v24;
    void** v25;
    void** v26;
    void** v27;
    void** rax28;
    void** rdi29;
    void** rax30;
    void*** rsp31;
    void** rbp32;
    uint32_t eax33;
    void** rax34;
    void** rbx35;
    int64_t r14_36;
    uint32_t eax37;
    uint32_t eax38;
    uint32_t eax39;
    void** r14_40;
    int1_t zf41;
    void** rax42;
    void** rax43;
    void** rbx44;
    int64_t r14_45;
    int32_t eax46;
    int64_t rax47;
    int32_t edx48;
    int64_t rdx49;
    int32_t edx50;
    int64_t rdx51;
    void** rax52;
    int64_t rcx53;
    int64_t rax54;
    int32_t r15d55;
    int32_t esi56;
    int64_t rcx57;
    uint32_t eax58;
    int32_t eax59;
    int64_t rdx60;
    uint32_t edx61;
    int32_t eax62;
    void** rax63;
    void** rax64;
    void** rax65;
    void** rax66;
    void** rax67;
    void** rsi68;
    void** rdx69;
    void** r14_70;
    void*** rsp71;
    void** rax72;
    void** v73;
    int1_t zf74;
    void** rdi75;
    uint32_t eax76;
    int64_t v77;
    uint32_t ebx78;
    void** rcx79;
    void** v80;
    uint32_t ebx81;
    void** v82;
    int64_t rbx83;
    int1_t zf84;
    uint32_t eax85;
    void** rbp86;
    void** r12_87;
    uint32_t eax88;
    void** rax89;
    void** rax90;
    struct s0* rax91;
    uint32_t edx92;
    int64_t rdx93;
    void** rcx94;
    void** rdx95;
    int64_t rdi96;
    void** eax97;
    void** rsi98;
    void** rax99;
    void** rax100;
    void** rdi101;
    uint32_t eax102;
    uint16_t v103;
    unsigned char v104;
    void** rdx105;
    void** r15_106;
    void** r13_107;
    void** r12_108;
    void** rbp109;
    void** rbx110;
    struct s1* rax111;
    int32_t v112;
    int64_t r14_113;
    struct s1* r8_114;
    struct s1* rcx115;
    void** r10_116;
    void** v117;
    struct s1* v118;
    int64_t rax119;
    int64_t rdx120;
    void** r8_121;
    int64_t rsi122;
    int64_t rcx123;
    int64_t rax124;
    void* rdx125;
    void** rcx126;
    void** rdx127;
    int32_t eax128;
    int32_t eax129;
    int32_t edx130;
    void** rdi131;
    void** rdx132;
    int32_t r8d133;
    uint32_t esi134;
    int32_t ecx135;
    void** ecx136;
    void** r15_137;
    int64_t rax138;
    void** rcx139;
    void** rax140;
    void** rdx141;
    void** r8_142;
    int32_t eax143;
    void** rsi144;
    void** rdi145;
    int64_t v146;
    int64_t rax147;

    v9 = reinterpret_cast<int64_t>(__return_address());
    v10 = r15_11;
    v12 = r14_13;
    v14 = r13_15;
    r13_16 = rdi;
    v17 = r12_18;
    r12d19 = 0;
    v20 = rbp21;
    v22 = rbx23;
    *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v24) + 4) = *reinterpret_cast<uint32_t*>(&rsi);
    v25 = rdx;
    v26 = rcx;
    v27 = r8;
    rax28 = fun_26e0(rdi);
    rdi29 = rax28 + 3;
    rax30 = xmalloc(rdi29, rsi, rdx, rcx, r8);
    rsp31 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 40 - 8 + 8 - 8 + 8);
    rbp32 = rax30;
    eax33 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_16));
    if (*reinterpret_cast<void***>(&eax33)) {
        do {
            addr_358d_2:
            if (*reinterpret_cast<void***>(&eax33) == 37) {
                rax34 = format_code_offset(r13_16, rsi, rdx, rcx, r8, r9, r13_16, rsi);
                rsi = r13_16;
                rbx35 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_16) + reinterpret_cast<unsigned char>(rax34));
                rdx = rax34;
                *reinterpret_cast<uint32_t*>(&r14_36) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx35));
                fun_2880(rbp32, rsi, rdx, rbp32, rsi, rdx);
                rsp31 = rsp31 - 8 + 8 - 8 + 8;
                if (*reinterpret_cast<signed char*>(&r14_36) == 37) {
                    addr_3685_4:
                    if (reinterpret_cast<unsigned char>(rax34) > reinterpret_cast<unsigned char>(1)) 
                        break;
                } else {
                    if (*reinterpret_cast<signed char*>(&r14_36) > 37) {
                        *reinterpret_cast<int32_t*>(&rdx) = 0;
                        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                        eax37 = *reinterpret_cast<uint32_t*>(&r14_36) & 0xfffffffb;
                        if (*reinterpret_cast<signed char*>(&eax37) == 72 && (reinterpret_cast<int1_t>(v26 == 0x5000) && ((eax38 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx35 + 1)), *reinterpret_cast<signed char*>(&eax38) == 100) || *reinterpret_cast<signed char*>(&eax38) == 0x72))) {
                            *reinterpret_cast<int32_t*>(&rdx) = *reinterpret_cast<signed char*>(&r14_36);
                            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                            ++rbx35;
                            *reinterpret_cast<uint32_t*>(&r14_36) = eax38;
                            goto addr_36cc_8;
                        }
                    }
                    *reinterpret_cast<int32_t*>(&rdx) = 0;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&r14_36)) {
                        addr_36cc_8:
                        *reinterpret_cast<int32_t*>(&rcx) = *reinterpret_cast<signed char*>(&r14_36);
                        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
                        rsi = rax34;
                        rdi29 = rbp32;
                        r9 = v25;
                        r13_16 = rbx35 + 1;
                        *reinterpret_cast<uint32_t*>(&r8) = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v24) + 4);
                        *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
                        eax39 = reinterpret_cast<uint32_t>(v26(rdi29, rsi, rdx, rcx, r8, r9));
                        r12d19 = r12d19 | eax39;
                        rdx = reinterpret_cast<void**>(0x3672);
                        rsp31 = rsp31 - 8 - 8 - 8 + 8 + 8 + 8;
                        continue;
                    } else {
                        --rbx35;
                        goto addr_3685_4;
                    }
                }
            } else {
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&eax33) == 92)) {
                    rdi29 = stdout;
                    r14_40 = r13_16 + 1;
                    rcx = *reinterpret_cast<void***>(rdi29 + 40);
                    if (reinterpret_cast<unsigned char>(rcx) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi29 + 48))) {
                        *reinterpret_cast<uint32_t*>(&rsi) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&eax33));
                        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                        rbx35 = r13_16;
                        r13_16 = r14_40;
                        fun_2760();
                        rsp31 = rsp31 - 8 + 8;
                        continue;
                    } else {
                        rsi = rcx + 1;
                        rbx35 = r13_16;
                        r13_16 = r14_40;
                        *reinterpret_cast<void***>(rdi29 + 40) = rsi;
                        *reinterpret_cast<void***>(rcx) = *reinterpret_cast<void***>(&eax33);
                        continue;
                    }
                } else {
                    zf41 = interpret_backslash_escapes == 0;
                    rbx35 = r13_16 + 1;
                    if (zf41) {
                        addr_35eb_16:
                        rdi29 = stdout;
                        rax42 = *reinterpret_cast<void***>(rdi29 + 40);
                        if (reinterpret_cast<unsigned char>(rax42) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi29 + 48))) {
                            *reinterpret_cast<uint32_t*>(&rsi) = 92;
                            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                            fun_2760();
                            rsp31 = rsp31 - 8 + 8;
                            rax43 = rbx35;
                            rbx35 = r13_16;
                            r13_16 = rax43;
                            continue;
                        } else {
                            rdx = rax42 + 1;
                            *reinterpret_cast<void***>(rdi29 + 40) = rdx;
                            *reinterpret_cast<void***>(rax42) = reinterpret_cast<void**>(92);
                            rbx44 = r13_16;
                            r13_16 = rbx35;
                            eax33 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx44 + 1));
                            if (*reinterpret_cast<void***>(&eax33)) 
                                goto addr_358d_2; else 
                                goto addr_3620_19;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&r14_45) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_16 + 1));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_45) + 4) = 0;
                        eax46 = static_cast<int32_t>(r14_45 - 48);
                        if (*reinterpret_cast<unsigned char*>(&eax46) <= 7) {
                            *reinterpret_cast<uint32_t*>(&rsi) = *reinterpret_cast<unsigned char*>(r13_16 + 2);
                            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                            *reinterpret_cast<int32_t*>(&rax47) = *reinterpret_cast<signed char*>(&eax46);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax47) + 4) = 0;
                            edx48 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rsi + 0xffffffffffffffd0));
                            if (*reinterpret_cast<unsigned char*>(&edx48) > 7) {
                                r13_16 = r13_16 + 2;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rsi) = *reinterpret_cast<unsigned char*>(r13_16 + 3);
                                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                                *reinterpret_cast<int32_t*>(&rdx49) = *reinterpret_cast<signed char*>(&edx48);
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx49) + 4) = 0;
                                *reinterpret_cast<int32_t*>(&rax47) = static_cast<int32_t>(rdx49 + rax47 * 8);
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax47) + 4) = 0;
                                edx50 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rsi + 0xffffffffffffffd0));
                                if (*reinterpret_cast<unsigned char*>(&edx50) > 7) {
                                    r13_16 = r13_16 + 3;
                                } else {
                                    *reinterpret_cast<int32_t*>(&rdx51) = *reinterpret_cast<signed char*>(&edx50);
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx51) + 4) = 0;
                                    r13_16 = r13_16 + 4;
                                    *reinterpret_cast<int32_t*>(&rax47) = static_cast<int32_t>(rdx51 + rax47 * 8);
                                }
                            }
                            rdi29 = stdout;
                            rdx = *reinterpret_cast<void***>(rdi29 + 40);
                            if (reinterpret_cast<unsigned char>(rdx) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi29 + 48))) {
                                *reinterpret_cast<uint32_t*>(&rsi) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax47));
                                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                                fun_2760();
                                rsp31 = rsp31 - 8 + 8;
                            } else {
                                rcx = rdx + 1;
                                *reinterpret_cast<void***>(rdi29 + 40) = rcx;
                                *reinterpret_cast<void***>(rdx) = *reinterpret_cast<void***>(&rax47);
                            }
                            rbx35 = r13_16 + 0xffffffffffffffff;
                            continue;
                        } else {
                            if (*reinterpret_cast<void***>(&r14_45) == 0x78) {
                                rax52 = fun_2a80(rdi29, rsi, rdx, rcx, rdi29, rsi, rdx, rcx);
                                rsp31 = rsp31 - 8 + 8;
                                *reinterpret_cast<uint32_t*>(&rcx53) = *reinterpret_cast<unsigned char*>(r13_16 + 2);
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx53) + 4) = 0;
                                rax54 = rcx53;
                                if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax52) + rcx53 * 2 + 1) & 16)) {
                                    r15d55 = 0x78;
                                    goto addr_384e_33;
                                } else {
                                    esi56 = static_cast<int32_t>(rax54 - 97);
                                    *reinterpret_cast<int32_t*>(&rcx57) = *reinterpret_cast<signed char*>(&rcx53);
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx57) + 4) = 0;
                                    if (*reinterpret_cast<unsigned char*>(&esi56) <= 5) {
                                        eax58 = static_cast<uint32_t>(rcx57 - 87);
                                    } else {
                                        eax59 = *reinterpret_cast<int32_t*>(&rax54) - 65;
                                        eax58 = static_cast<uint32_t>(rcx57 - 55);
                                        if (*reinterpret_cast<unsigned char*>(&eax59) > 5) {
                                            eax58 = *reinterpret_cast<int32_t*>(&rcx57) - 48;
                                        }
                                    }
                                    *reinterpret_cast<uint32_t*>(&rsi) = *reinterpret_cast<unsigned char*>(r13_16 + 3);
                                    *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                                    rbx35 = r13_16 + 2;
                                    rcx = rsi;
                                    if (*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax52) + reinterpret_cast<unsigned char>(rsi) * 2 + 1) & 16) {
                                        *reinterpret_cast<int32_t*>(&rdx60) = *reinterpret_cast<signed char*>(&rsi);
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx60) + 4) = 0;
                                        *reinterpret_cast<uint32_t*>(&rsi) = static_cast<uint32_t>(reinterpret_cast<uint64_t>(rsi + 0xffffffffffffff9f));
                                        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                                        rbx35 = r13_16 + 3;
                                        if (*reinterpret_cast<unsigned char*>(&rsi) > 5) {
                                            *reinterpret_cast<int32_t*>(&rcx) = *reinterpret_cast<int32_t*>(&rcx) - 65;
                                            *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
                                            *reinterpret_cast<uint32_t*>(&rsi) = static_cast<uint32_t>(rdx60 - 55);
                                            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                                            edx61 = *reinterpret_cast<int32_t*>(&rdx60) - 48;
                                            if (*reinterpret_cast<unsigned char*>(&rcx) <= 5) {
                                                edx61 = *reinterpret_cast<uint32_t*>(&rsi);
                                            }
                                        } else {
                                            edx61 = *reinterpret_cast<int32_t*>(&rdx60) - 87;
                                        }
                                        eax58 = (eax58 << 4) + edx61;
                                    }
                                    rdi29 = stdout;
                                    r13_16 = rbx35 + 1;
                                    rdx = *reinterpret_cast<void***>(rdi29 + 40);
                                    if (reinterpret_cast<unsigned char>(rdx) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi29 + 48))) {
                                        *reinterpret_cast<uint32_t*>(&rsi) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&eax58));
                                        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                                        fun_2760();
                                        rsp31 = rsp31 - 8 + 8;
                                        continue;
                                    } else {
                                        rcx = rdx + 1;
                                        *reinterpret_cast<void***>(rdi29 + 40) = rcx;
                                        *reinterpret_cast<void***>(rdx) = *reinterpret_cast<void***>(&eax58);
                                        continue;
                                    }
                                }
                            } else {
                                if (*reinterpret_cast<void***>(&r14_45)) {
                                    r15d55 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(&r14_45));
                                    if (*reinterpret_cast<void***>(&r14_45) == 34) {
                                        r15d55 = 34;
                                        goto addr_3880_51;
                                    } else {
                                        addr_384e_33:
                                        eax62 = static_cast<int32_t>(r14_45 - 92);
                                        if (*reinterpret_cast<unsigned char*>(&eax62) > 26) 
                                            goto addr_38f0_52; else 
                                            goto addr_385a_53;
                                    }
                                } else {
                                    rax63 = fun_26c0();
                                    *reinterpret_cast<uint32_t*>(&rsi) = 0;
                                    *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                                    rdx = rax63;
                                    fun_2990();
                                    rsp31 = rsp31 - 8 + 8 - 8 + 8;
                                    goto addr_35eb_16;
                                }
                            }
                        }
                    }
                }
            }
            rdi29 = stdout;
            r13_16 = rbx35 + 1;
            rax64 = *reinterpret_cast<void***>(rdi29 + 40);
            if (reinterpret_cast<unsigned char>(rax64) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi29 + 48))) {
                *reinterpret_cast<uint32_t*>(&rsi) = 37;
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                fun_2760();
                rsp31 = rsp31 - 8 + 8;
                continue;
            } else {
                rdx = rax64 + 1;
                *reinterpret_cast<void***>(rdi29 + 40) = rdx;
                *reinterpret_cast<void***>(rax64) = reinterpret_cast<void**>(37);
                continue;
            }
            addr_3880_51:
            rdi29 = stdout;
            r13_16 = r13_16 + 2;
            rax65 = *reinterpret_cast<void***>(rdi29 + 40);
            if (reinterpret_cast<unsigned char>(rax65) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi29 + 48))) {
                *reinterpret_cast<uint32_t*>(&rsi) = *reinterpret_cast<unsigned char*>(&r15d55);
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                fun_2760();
                rsp31 = rsp31 - 8 + 8;
                continue;
            } else {
                rdx = rax65 + 1;
                *reinterpret_cast<void***>(rdi29 + 40) = rdx;
                *reinterpret_cast<void***>(rax65) = *reinterpret_cast<void***>(&r14_45);
                continue;
            }
            addr_38f0_52:
            rax66 = fun_26c0();
            *reinterpret_cast<int32_t*>(&rcx) = r15d55;
            *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rsi) = 0;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            rdx = rax66;
            fun_2990();
            rsp31 = rsp31 - 8 + 8 - 8 + 8;
            goto addr_3880_51;
            eax33 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx35 + 1));
        } while (*reinterpret_cast<void***>(&eax33));
        goto addr_3620_19;
    } else {
        goto addr_3620_19;
    }
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<unsigned char>(rax34)) = *reinterpret_cast<signed char*>(&r14_36);
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp32) + reinterpret_cast<unsigned char>(rax34) + 1) = 0;
    quote(rbp32, rsi, rdx, rcx, r8, r9);
    rax67 = fun_26c0();
    *reinterpret_cast<int32_t*>(&rsi68) = 0;
    *reinterpret_cast<int32_t*>(&rsi68 + 4) = 0;
    rdx69 = rax67;
    fun_2990();
    r14_70 = rdx69;
    rsp71 = rsp31 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1e8;
    rax72 = g28;
    v73 = rax72;
    zf74 = g1 == 45;
    rdi75 = reinterpret_cast<void**>(rsp71 + 0xd0);
    if (!zf74 || (eax76 = g2, *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v77) + 4) = eax76, !!eax76)) {
        ebx78 = follow_links;
        *reinterpret_cast<int32_t*>(&rcx79) = 32;
        *reinterpret_cast<int32_t*>(&rcx79 + 4) = 0;
        v80 = rdi75;
        while (rcx79) {
            --rcx79;
            rsi68 = rsi68 + 8;
        }
        ebx81 = ebx78 ^ 1;
        v82 = reinterpret_cast<void**>(1);
        *reinterpret_cast<uint32_t*>(&rbx83) = static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&ebx81)) << 8;
        *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v77) + 4) = 0xffffff9c;
    } else {
        *reinterpret_cast<int32_t*>(&rcx79) = 32;
        *reinterpret_cast<int32_t*>(&rcx79 + 4) = 0;
        v80 = rdi75;
        *reinterpret_cast<uint32_t*>(&rbx83) = reinterpret_cast<uint32_t>("hr");
        while (rcx79) {
            --rcx79;
            rsi68 = rsi68 + 8;
        }
        v82 = reinterpret_cast<void**>(0x13341);
    }
    zf84 = dont_sync == 0;
    eax85 = force_sync;
    if (zf84) {
        if (*reinterpret_cast<signed char*>(&eax85)) {
            *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&rbx83) + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&rbx83) + 1) | 32);
        } else {
            addr_3d04_75:
            *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&rbx83) + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&rbx83) + 1) | 8);
        }
    } else {
        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&rbx83) + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&rbx83) + 1) | 64);
        if (!*reinterpret_cast<signed char*>(&eax85)) 
            goto addr_3d04_75;
    }
    rbp86 = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint32_t*>(&r12_87) = 0;
    eax88 = reinterpret_cast<unsigned char>(localtime_r);
    if (*reinterpret_cast<signed char*>(&eax88)) {
        do {
            addr_3b29_79:
            if (*reinterpret_cast<signed char*>(&eax88) != 37) {
                rax89 = rbp86;
                rbp86 = rax89 + 1;
                eax88 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax89 + 1));
                if (*reinterpret_cast<signed char*>(&eax88)) 
                    goto addr_3b29_79; else 
                    break;
            }
            rax90 = format_code_offset(rbp86, rsi68, rdx69, rcx79, r8, r9);
            rsp71 = rsp71 - 8 + 8;
            rax91 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax90) + reinterpret_cast<unsigned char>(rbp86));
            edx92 = rax91->f0;
            if (!*reinterpret_cast<signed char*>(&edx92)) 
                break;
            *reinterpret_cast<uint32_t*>(&rdx69) = edx92 - 65;
            *reinterpret_cast<int32_t*>(&rdx69 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rdx69) > 57) 
                continue;
            *reinterpret_cast<uint32_t*>(&rdx93) = *reinterpret_cast<unsigned char*>(&rdx69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx93) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rdx69) = reinterpret_cast<uint16_t>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(0x129a0) + reinterpret_cast<uint64_t>(rdx93 * 2)));
            *reinterpret_cast<int32_t*>(&rdx69 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r12_87) = *reinterpret_cast<uint32_t*>(&r12_87) | *reinterpret_cast<uint32_t*>(&rdx69);
            rbp86 = reinterpret_cast<void**>(&rax91->f1);
            eax88 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax91->f1));
        } while (*reinterpret_cast<signed char*>(&eax88));
    }
    *reinterpret_cast<uint32_t*>(&rcx94) = *reinterpret_cast<uint32_t*>(&r12_87);
    *reinterpret_cast<int32_t*>(&rcx94 + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rdx95) = *reinterpret_cast<uint32_t*>(&rbx83);
    *reinterpret_cast<int32_t*>(&rdx95 + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rdi96) = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v77) + 4);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi96) + 4) = 0;
    eax97 = fun_2970(rdi96, v82, rdx95);
    rsi98 = eax97;
    *reinterpret_cast<int32_t*>(&rsi98 + 4) = 0;
    if (reinterpret_cast<signed char>(eax97) < reinterpret_cast<signed char>(0)) {
        rax99 = fun_25e0();
        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&rbx83) + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&rbx83) + 1) & 16);
        if (!*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&rbx83) + 1)) {
            rax100 = quotearg_style(4, 1, rdx95, rcx94, v80, r9, 0);
            fun_26c0();
            rsi98 = *reinterpret_cast<void***>(rax99);
            *reinterpret_cast<int32_t*>(&rsi98 + 4) = 0;
            rcx94 = rax100;
            *reinterpret_cast<int32_t*>(&rdi101) = 0;
            *reinterpret_cast<int32_t*>(&rdi101 + 4) = 0;
            fun_2990();
        } else {
            fun_26c0();
            rsi98 = *reinterpret_cast<void***>(rax99);
            *reinterpret_cast<int32_t*>(&rsi98 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi101) = 0;
            *reinterpret_cast<int32_t*>(&rdi101 + 4) = 0;
            fun_2990();
        }
    } else {
        eax102 = v103;
        if ((*reinterpret_cast<uint16_t*>(&eax102) & 0xb000) != 0x2000) {
            r14_70 = reinterpret_cast<void**>(0);
        }
        if (v104 & 8) {
        }
        rcx94 = reinterpret_cast<void**>(0x5000);
        rdi101 = r14_70;
        print_it(rdi101, rsi98, 1, 0x5000, rsp71 - 8 + 8 + 32, r9, 0, v77);
    }
    rdx105 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v73) - reinterpret_cast<unsigned char>(g28));
    if (!rdx105) {
        goto v26;
    }
    fun_2710();
    r15_106 = rdx105;
    r13_107 = rdi101;
    r12_108 = rdx105;
    rbp109 = rsi98;
    rbx110 = rcx94;
    rax111 = fun_27f0(rdi101, 46, rsi98);
    if (rax111) 
        goto addr_3e53_98;
    v112 = 0;
    *reinterpret_cast<int32_t*>(&r14_113) = 0;
    goto addr_3fcb_100;
    addr_3e53_98:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r13_107) + reinterpret_cast<unsigned char>(rbp109)) = 0;
    r8_114 = rax111;
    rcx115 = rax111;
    r10_116 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax111) - reinterpret_cast<unsigned char>(r13_107));
    if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(&rax111->f1) - 48) <= 9) {
        v117 = r10_116;
        v118 = r8_114;
        rax119 = fun_2870(&r8_114->f1);
        *reinterpret_cast<int32_t*>(&rdx120) = 0x7fffffff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx120) + 4) = 0;
        r10_116 = v117;
        if (rax119 <= 0x7fffffff) {
            rdx120 = rax119;
        }
        v112 = *reinterpret_cast<int32_t*>(&rdx120);
        r14_113 = rdx120;
        if (*reinterpret_cast<int32_t*>(&rdx120)) 
            goto addr_3f9d_104;
    } else {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r8_114) - 1) - 48 <= 9) {
            *reinterpret_cast<int32_t*>(&r14_113) = 9;
            goto addr_407e_107;
        } else {
            r8_121 = rbx110;
            rbp109 = r10_116;
            *reinterpret_cast<int32_t*>(&rsi122) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi122) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rcx123) = 1;
            v112 = 0;
            *reinterpret_cast<int32_t*>(&r14_113) = 9;
            goto addr_3ea8_109;
        }
    }
    *reinterpret_cast<int32_t*>(&r14_113) = 0;
    rbp109 = r10_116;
    goto addr_3fcb_100;
    addr_3f9d_104:
    r8_114 = r8_114;
    if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r8_114) - 1) - 48 > 9) {
        v112 = 0;
        rbp109 = r10_116;
        goto addr_4148_112;
    } else {
        rcx115 = v118;
    }
    addr_407e_107:
    r8_114->f0 = 0;
    do {
        rcx115 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(rcx115) - 1);
    } while ((rcx115 - 1)->f0 - 48 <= 9);
    v117 = r10_116;
    rax124 = fun_2870(rcx115);
    rbp109 = v117;
    if (rax124 > 0x7fffffff) {
        rax124 = 0x7fffffff;
    }
    v112 = *reinterpret_cast<int32_t*>(&rax124);
    if (*reinterpret_cast<int32_t*>(&rax124) <= 1 || ((*reinterpret_cast<int32_t*>(&rdx125) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx125) + 4) = 0, *reinterpret_cast<unsigned char*>(&rdx125) = reinterpret_cast<uint1_t>(rcx115->f0 == 48), rcx126 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx115) + reinterpret_cast<uint64_t>(rdx125)), rdx127 = decimal_point_len, rbp109 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx126) - reinterpret_cast<unsigned char>(r13_107)), reinterpret_cast<unsigned char>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax124))) <= reinterpret_cast<unsigned char>(rdx127)) || ((eax128 = *reinterpret_cast<int32_t*>(&rax124) - *reinterpret_cast<int32_t*>(&rdx127), eax128 <= 1) || (eax129 = eax128 - *reinterpret_cast<int32_t*>(&r14_113), eax129 <= 1)))) {
        addr_4148_112:
        if (*reinterpret_cast<int32_t*>(&r14_113) <= 8) {
            addr_3fcb_100:
            edx130 = *reinterpret_cast<int32_t*>(&r14_113);
            *reinterpret_cast<int32_t*>(&rcx123) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx123) + 4) = 0;
        } else {
            r8_121 = rbx110;
            *reinterpret_cast<int32_t*>(&rsi122) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi122) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rcx123) = 1;
            goto addr_3ea8_109;
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_107) >= reinterpret_cast<unsigned char>(rcx126)) {
            rdi131 = r13_107;
            *reinterpret_cast<int32_t*>(&rbp109) = 0;
            *reinterpret_cast<int32_t*>(&rbp109 + 4) = 0;
            goto addr_417e_121;
        } else {
            rdx132 = r13_107;
            rdi131 = r13_107;
            r8d133 = 0;
            do {
                esi134 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx132));
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&esi134) == 45)) {
                    *reinterpret_cast<void***>(rdi131) = *reinterpret_cast<void***>(&esi134);
                    ++rdi131;
                } else {
                    r8d133 = 1;
                }
                ++rdx132;
            } while (rcx126 != rdx132);
            rbp109 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi131) - reinterpret_cast<unsigned char>(r13_107));
            if (*reinterpret_cast<signed char*>(&r8d133)) 
                goto addr_4148_112; else 
                goto addr_417e_121;
        }
    }
    do {
        ecx135 = static_cast<int32_t>(rcx123 + rcx123 * 4);
        ++edx130;
        *reinterpret_cast<int32_t*>(&rcx123) = ecx135 + ecx135;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx123) + 4) = 0;
    } while (edx130 != 9);
    rsi122 = *reinterpret_cast<int32_t*>(&rcx123);
    r8_121 = reinterpret_cast<void**>(reinterpret_cast<signed char>(rbx110) / rsi122);
    addr_3ea8_109:
    if (reinterpret_cast<signed char>(r15_106) >= reinterpret_cast<signed char>(0) || (!rbx110 || (ecx136 = reinterpret_cast<void**>(0x3b9aca00 / *reinterpret_cast<int32_t*>(&rcx123) - reinterpret_cast<unsigned char>(r8_121) + 0xffffffff + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<signed char>(rbx110) % rsi122) < 1)), r8_121 = ecx136, *reinterpret_cast<int32_t*>(&r8_121 + 4) = 0, r15_137 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_106) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_106) < reinterpret_cast<unsigned char>(1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(ecx136) < reinterpret_cast<unsigned char>(1)))))))), r12_108 = r15_137, !!r15_137))) {
        v117 = r8_121;
        make_format(r13_107, rbp109, "'-+ 0", "ld", r8_121, r9, v117);
        rax138 = fun_2960(1, r13_107, r12_108, "ld", 1, r13_107, r12_108, "ld");
    } else {
        v117 = ecx136;
        make_format(r13_107, rbp109, "'-+ 0", ".0f", r8_121, r9, v117);
        *reinterpret_cast<void***>(r13_107) = *reinterpret_cast<void***>(rbp109);
        rax138 = fun_2960(1, r13_107, "'-+ 0", ".0f", 1, r13_107, "'-+ 0", ".0f");
    }
    if (*reinterpret_cast<int32_t*>(&r14_113)) {
        *reinterpret_cast<int32_t*>(&rcx139) = 9;
        *reinterpret_cast<int32_t*>(&rcx139 + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&r14_113) <= 9) {
            *reinterpret_cast<int32_t*>(&rcx139) = *reinterpret_cast<int32_t*>(&r14_113);
            *reinterpret_cast<int32_t*>(&rcx139 + 4) = 0;
        }
        if (*reinterpret_cast<int32_t*>(&rax138) < 0) {
            *reinterpret_cast<int32_t*>(&rax138) = 0;
        }
        if (*reinterpret_cast<int32_t*>(&rax138) < v112 && (rax140 = decimal_point_len, reinterpret_cast<unsigned char>(static_cast<int64_t>(v112 - *reinterpret_cast<int32_t*>(&rax138))) > reinterpret_cast<unsigned char>(rax140))) {
        }
        rdx141 = decimal_point;
        fun_2960(1, "%s%.*d%-*.*d", rdx141, rcx139, 1, "%s%.*d%-*.*d", rdx141, rcx139);
    }
    goto 0;
    addr_417e_121:
    *reinterpret_cast<int32_t*>(&r8_142) = eax129;
    *reinterpret_cast<int32_t*>(&r8_142 + 4) = 0;
    eax143 = fun_2a90(rdi131, 1, 0xffffffffffffffff, "%d", r8_142, r9, v117);
    rbp109 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp109) + reinterpret_cast<uint64_t>(static_cast<int64_t>(eax143)));
    goto addr_4148_112;
    addr_3620_19:
    fun_25b0(rbp32, rbp32);
    rsi144 = stdout;
    rdi145 = trailing_delim;
    fun_2810(rdi145, rsi144, rdx, rcx, r8, r9, v26, v24, v25, v27, v146, v22, v20, v17, v14, v12, v10, v9, a7, a8);
    return r12d19;
    addr_385a_53:
    *reinterpret_cast<uint32_t*>(&rax147) = *reinterpret_cast<unsigned char*>(&eax62);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax147) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x127c0 + rax147 * 4) + 0x127c0;
}

struct s2 {
    unsigned char f0;
    void** f1;
};

signed char do_stat(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r14_7;
    void** r13_8;
    void*** rsp9;
    void** v10;
    void** rax11;
    void** v12;
    void** rdi13;
    uint32_t eax14;
    int64_t v15;
    uint32_t ebx16;
    void** rcx17;
    void** v18;
    uint32_t ebx19;
    void** v20;
    int64_t rbx21;
    int1_t zf22;
    uint32_t eax23;
    void** rbp24;
    void** r12_25;
    uint32_t eax26;
    void** rax27;
    void** rax28;
    struct s2* rax29;
    uint32_t edx30;
    int64_t rdx31;
    void** rcx32;
    void** rdx33;
    int64_t rdi34;
    void** eax35;
    void** rsi36;
    void** rax37;
    void** rax38;
    void** rdi39;
    uint32_t eax40;
    uint32_t eax41;
    uint16_t v42;
    unsigned char v43;
    uint32_t eax44;
    void** rdx45;
    void** r15_46;
    void** r13_47;
    void** r12_48;
    void** rbp49;
    void** rbx50;
    struct s1* rax51;
    int32_t v52;
    int64_t r14_53;
    struct s1* r8_54;
    struct s1* rcx55;
    void** r10_56;
    void** v57;
    struct s1* v58;
    int64_t rax59;
    int64_t rdx60;
    void** r8_61;
    int64_t rsi62;
    int64_t rcx63;
    int64_t rax64;
    void* rdx65;
    void** rcx66;
    void** rdx67;
    int32_t eax68;
    int32_t eax69;
    int32_t edx70;
    void** rdi71;
    void** rdx72;
    int32_t r8d73;
    uint32_t esi74;
    int32_t ecx75;
    void** ecx76;
    void** r15_77;
    int64_t rax78;
    void** rcx79;
    void** rax80;
    void** rdx81;
    void** r8_82;
    int32_t eax83;

    r14_7 = rdx;
    r13_8 = rdi;
    rsp9 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1e8);
    v10 = rsi;
    rax11 = g28;
    v12 = rax11;
    rdi13 = reinterpret_cast<void**>(rsp9 + 0xd0);
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 45) || (eax14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_8 + 1)), *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v15) + 4) = eax14, !!eax14)) {
        ebx16 = follow_links;
        *reinterpret_cast<int32_t*>(&rcx17) = 32;
        *reinterpret_cast<int32_t*>(&rcx17 + 4) = 0;
        v18 = rdi13;
        while (rcx17) {
            --rcx17;
            rsi = rsi + 8;
        }
        ebx19 = ebx16 ^ 1;
        v20 = r13_8;
        *reinterpret_cast<uint32_t*>(&rbx21) = static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&ebx19)) << 8;
        *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v15) + 4) = 0xffffff9c;
    } else {
        *reinterpret_cast<int32_t*>(&rcx17) = 32;
        *reinterpret_cast<int32_t*>(&rcx17 + 4) = 0;
        v18 = rdi13;
        *reinterpret_cast<uint32_t*>(&rbx21) = reinterpret_cast<uint32_t>("hr");
        while (rcx17) {
            --rcx17;
            rsi = rsi + 8;
        }
        v20 = reinterpret_cast<void**>(0x13341);
    }
    zf22 = dont_sync == 0;
    eax23 = force_sync;
    if (zf22) {
        if (*reinterpret_cast<signed char*>(&eax23)) {
            *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&rbx21) + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&rbx21) + 1) | 32);
        } else {
            addr_3d04_13:
            *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&rbx21) + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&rbx21) + 1) | 8);
        }
    } else {
        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&rbx21) + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&rbx21) + 1) | 64);
        if (!*reinterpret_cast<signed char*>(&eax23)) 
            goto addr_3d04_13;
    }
    rbp24 = v10;
    *reinterpret_cast<uint32_t*>(&r12_25) = 0;
    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp24));
    if (*reinterpret_cast<signed char*>(&eax26)) {
        do {
            addr_3b29_17:
            if (*reinterpret_cast<signed char*>(&eax26) != 37) {
                rax27 = rbp24;
                rbp24 = rax27 + 1;
                eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax27 + 1));
                if (*reinterpret_cast<signed char*>(&eax26)) 
                    goto addr_3b29_17; else 
                    break;
            }
            rax28 = format_code_offset(rbp24, rsi, rdx, rcx17, r8, r9);
            rsp9 = rsp9 - 8 + 8;
            rax29 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(rax28) + reinterpret_cast<unsigned char>(rbp24));
            edx30 = rax29->f0;
            if (!*reinterpret_cast<signed char*>(&edx30)) 
                break;
            *reinterpret_cast<uint32_t*>(&rdx) = edx30 - 65;
            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rdx) > 57) 
                continue;
            *reinterpret_cast<uint32_t*>(&rdx31) = *reinterpret_cast<unsigned char*>(&rdx);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx31) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rdx) = *reinterpret_cast<uint16_t*>(0x129a0 + rdx31 * 2);
            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r12_25) = *reinterpret_cast<uint32_t*>(&r12_25) | *reinterpret_cast<uint32_t*>(&rdx);
            rbp24 = reinterpret_cast<void**>(&rax29->f1);
            eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax29->f1));
        } while (*reinterpret_cast<signed char*>(&eax26));
    }
    *reinterpret_cast<uint32_t*>(&rcx32) = *reinterpret_cast<uint32_t*>(&r12_25);
    *reinterpret_cast<int32_t*>(&rcx32 + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rdx33) = *reinterpret_cast<uint32_t*>(&rbx21);
    *reinterpret_cast<int32_t*>(&rdx33 + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rdi34) = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v15) + 4);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
    eax35 = fun_2970(rdi34, v20, rdx33);
    rsi36 = eax35;
    *reinterpret_cast<int32_t*>(&rsi36 + 4) = 0;
    if (reinterpret_cast<signed char>(eax35) < reinterpret_cast<signed char>(0)) {
        rax37 = fun_25e0();
        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&rbx21) + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&rbx21) + 1) & 16);
        if (!*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&rbx21) + 1)) {
            rax38 = quotearg_style(4, r13_8, rdx33, rcx32, v18, r9, v10);
            fun_26c0();
            rsi36 = *reinterpret_cast<void***>(rax37);
            *reinterpret_cast<int32_t*>(&rsi36 + 4) = 0;
            rcx32 = rax38;
            *reinterpret_cast<int32_t*>(&rdi39) = 0;
            *reinterpret_cast<int32_t*>(&rdi39 + 4) = 0;
            fun_2990();
            eax40 = 0;
        } else {
            fun_26c0();
            rsi36 = *reinterpret_cast<void***>(rax37);
            *reinterpret_cast<int32_t*>(&rsi36 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi39) = 0;
            *reinterpret_cast<int32_t*>(&rdi39 + 4) = 0;
            fun_2990();
            eax40 = 0;
        }
    } else {
        eax41 = v42;
        if ((*reinterpret_cast<uint16_t*>(&eax41) & 0xb000) != 0x2000) {
            r14_7 = v10;
        }
        if (v43 & 8) {
        }
        rcx32 = reinterpret_cast<void**>(0x5000);
        rdi39 = r14_7;
        eax44 = print_it(rdi39, rsi36, r13_8, 0x5000, rsp9 - 8 + 8 + 32, r9, v10, v15);
        eax40 = eax44 ^ 1;
    }
    rdx45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v12) - reinterpret_cast<unsigned char>(g28));
    if (!rdx45) {
        return *reinterpret_cast<signed char*>(&eax40);
    }
    fun_2710();
    r15_46 = rdx45;
    r13_47 = rdi39;
    r12_48 = rdx45;
    rbp49 = rsi36;
    rbx50 = rcx32;
    rax51 = fun_27f0(rdi39, 46, rsi36);
    if (rax51) 
        goto addr_3e53_36;
    v52 = 0;
    *reinterpret_cast<int32_t*>(&r14_53) = 0;
    goto addr_3fcb_38;
    addr_3e53_36:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r13_47) + reinterpret_cast<unsigned char>(rbp49)) = 0;
    r8_54 = rax51;
    rcx55 = rax51;
    r10_56 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax51) - reinterpret_cast<unsigned char>(r13_47));
    if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(&rax51->f1) - 48) <= 9) {
        v57 = r10_56;
        v58 = r8_54;
        rax59 = fun_2870(&r8_54->f1);
        *reinterpret_cast<int32_t*>(&rdx60) = 0x7fffffff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx60) + 4) = 0;
        r10_56 = v57;
        if (rax59 <= 0x7fffffff) {
            rdx60 = rax59;
        }
        v52 = *reinterpret_cast<int32_t*>(&rdx60);
        r14_53 = rdx60;
        if (*reinterpret_cast<int32_t*>(&rdx60)) 
            goto addr_3f9d_42;
    } else {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r8_54) - 1) - 48 <= 9) {
            *reinterpret_cast<int32_t*>(&r14_53) = 9;
            goto addr_407e_45;
        } else {
            r8_61 = rbx50;
            rbp49 = r10_56;
            *reinterpret_cast<int32_t*>(&rsi62) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi62) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rcx63) = 1;
            v52 = 0;
            *reinterpret_cast<int32_t*>(&r14_53) = 9;
            goto addr_3ea8_47;
        }
    }
    *reinterpret_cast<int32_t*>(&r14_53) = 0;
    rbp49 = r10_56;
    goto addr_3fcb_38;
    addr_3f9d_42:
    r8_54 = r8_54;
    if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r8_54) - 1) - 48 > 9) {
        v52 = 0;
        rbp49 = r10_56;
        goto addr_4148_50;
    } else {
        rcx55 = v58;
    }
    addr_407e_45:
    r8_54->f0 = 0;
    do {
        rcx55 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(rcx55) - 1);
    } while ((rcx55 - 1)->f0 - 48 <= 9);
    v57 = r10_56;
    rax64 = fun_2870(rcx55);
    rbp49 = v57;
    if (rax64 > 0x7fffffff) {
        rax64 = 0x7fffffff;
    }
    v52 = *reinterpret_cast<int32_t*>(&rax64);
    if (*reinterpret_cast<int32_t*>(&rax64) <= 1 || ((*reinterpret_cast<int32_t*>(&rdx65) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx65) + 4) = 0, *reinterpret_cast<unsigned char*>(&rdx65) = reinterpret_cast<uint1_t>(rcx55->f0 == 48), rcx66 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx55) + reinterpret_cast<uint64_t>(rdx65)), rdx67 = decimal_point_len, rbp49 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx66) - reinterpret_cast<unsigned char>(r13_47)), reinterpret_cast<unsigned char>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax64))) <= reinterpret_cast<unsigned char>(rdx67)) || ((eax68 = *reinterpret_cast<int32_t*>(&rax64) - *reinterpret_cast<int32_t*>(&rdx67), eax68 <= 1) || (eax69 = eax68 - *reinterpret_cast<int32_t*>(&r14_53), eax69 <= 1)))) {
        addr_4148_50:
        if (*reinterpret_cast<int32_t*>(&r14_53) <= 8) {
            addr_3fcb_38:
            edx70 = *reinterpret_cast<int32_t*>(&r14_53);
            *reinterpret_cast<int32_t*>(&rcx63) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx63) + 4) = 0;
        } else {
            r8_61 = rbx50;
            *reinterpret_cast<int32_t*>(&rsi62) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi62) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rcx63) = 1;
            goto addr_3ea8_47;
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_47) >= reinterpret_cast<unsigned char>(rcx66)) {
            rdi71 = r13_47;
            *reinterpret_cast<int32_t*>(&rbp49) = 0;
            *reinterpret_cast<int32_t*>(&rbp49 + 4) = 0;
            goto addr_417e_59;
        } else {
            rdx72 = r13_47;
            rdi71 = r13_47;
            r8d73 = 0;
            do {
                esi74 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx72));
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&esi74) == 45)) {
                    *reinterpret_cast<void***>(rdi71) = *reinterpret_cast<void***>(&esi74);
                    ++rdi71;
                } else {
                    r8d73 = 1;
                }
                ++rdx72;
            } while (rcx66 != rdx72);
            rbp49 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi71) - reinterpret_cast<unsigned char>(r13_47));
            if (*reinterpret_cast<signed char*>(&r8d73)) 
                goto addr_4148_50; else 
                goto addr_417e_59;
        }
    }
    do {
        ecx75 = static_cast<int32_t>(rcx63 + rcx63 * 4);
        ++edx70;
        *reinterpret_cast<int32_t*>(&rcx63) = ecx75 + ecx75;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx63) + 4) = 0;
    } while (edx70 != 9);
    rsi62 = *reinterpret_cast<int32_t*>(&rcx63);
    r8_61 = reinterpret_cast<void**>(reinterpret_cast<signed char>(rbx50) / rsi62);
    addr_3ea8_47:
    if (reinterpret_cast<signed char>(r15_46) >= reinterpret_cast<signed char>(0) || (!rbx50 || (ecx76 = reinterpret_cast<void**>(0x3b9aca00 / *reinterpret_cast<int32_t*>(&rcx63) - reinterpret_cast<unsigned char>(r8_61) + 0xffffffff + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<signed char>(rbx50) % rsi62) < 1)), r8_61 = ecx76, *reinterpret_cast<int32_t*>(&r8_61 + 4) = 0, r15_77 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_46) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_46) < reinterpret_cast<unsigned char>(1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(ecx76) < reinterpret_cast<unsigned char>(1)))))))), r12_48 = r15_77, !!r15_77))) {
        v57 = r8_61;
        make_format(r13_47, rbp49, "'-+ 0", "ld", r8_61, r9, v57);
        rax78 = fun_2960(1, r13_47, r12_48, "ld", 1, r13_47, r12_48, "ld");
    } else {
        v57 = ecx76;
        make_format(r13_47, rbp49, "'-+ 0", ".0f", r8_61, r9, v57);
        *reinterpret_cast<void***>(r13_47) = *reinterpret_cast<void***>(rbp49);
        rax78 = fun_2960(1, r13_47, "'-+ 0", ".0f", 1, r13_47, "'-+ 0", ".0f");
    }
    if (*reinterpret_cast<int32_t*>(&r14_53)) {
        *reinterpret_cast<int32_t*>(&rcx79) = 9;
        *reinterpret_cast<int32_t*>(&rcx79 + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&r14_53) <= 9) {
            *reinterpret_cast<int32_t*>(&rcx79) = *reinterpret_cast<int32_t*>(&r14_53);
            *reinterpret_cast<int32_t*>(&rcx79 + 4) = 0;
        }
        if (*reinterpret_cast<int32_t*>(&rax78) < 0) {
            *reinterpret_cast<int32_t*>(&rax78) = 0;
        }
        if (*reinterpret_cast<int32_t*>(&rax78) < v52 && (rax80 = decimal_point_len, reinterpret_cast<unsigned char>(static_cast<int64_t>(v52 - *reinterpret_cast<int32_t*>(&rax78))) > reinterpret_cast<unsigned char>(rax80))) {
        }
        rdx81 = decimal_point;
        fun_2960(1, "%s%.*d%-*.*d", rdx81, rcx79, 1, "%s%.*d%-*.*d", rdx81, rcx79);
    }
    goto v10;
    addr_417e_59:
    *reinterpret_cast<int32_t*>(&r8_82) = eax69;
    *reinterpret_cast<int32_t*>(&r8_82 + 4) = 0;
    eax83 = fun_2a90(rdi71, 1, 0xffffffffffffffff, "%d", r8_82, r9, v57);
    rbp49 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp49) + reinterpret_cast<uint64_t>(static_cast<int64_t>(eax83)));
    goto addr_4148_50;
}

struct s3 {
    signed char f0;
    void** f1;
};

struct s3* fun_2740(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

void make_format(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7) {
    void** rax8;
    void** r13_9;
    void** rbp10;
    void** rbx11;
    void** v12;
    uint32_t r12d13;
    void** rsi14;
    uint32_t r14d15;
    struct s3* rax16;
    void** rsi17;
    struct s3* rax18;
    void* rax19;
    void* r12_20;

    rax8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(rsi));
    r13_9 = rdx;
    rbp10 = rdi + 1;
    rbx11 = rbp10;
    v12 = rax8;
    if (reinterpret_cast<unsigned char>(rbp10) < reinterpret_cast<unsigned char>(rax8)) {
        do {
            r12d13 = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx11))));
            *reinterpret_cast<uint32_t*>(&rsi14) = r12d13;
            *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
            r14d15 = r12d13;
            rax16 = fun_2740("'-+ #0I", rsi14, rdx, rcx, r8);
            if (!rax16) 
                break;
            *reinterpret_cast<uint32_t*>(&rsi17) = r12d13;
            *reinterpret_cast<int32_t*>(&rsi17 + 4) = 0;
            rax18 = fun_2740(r13_9, rsi17, rdx, rcx, r8);
            if (rax18) {
                *reinterpret_cast<void***>(rbp10) = *reinterpret_cast<void***>(&r14d15);
                ++rbp10;
            }
            ++rbx11;
        } while (reinterpret_cast<unsigned char>(v12) > reinterpret_cast<unsigned char>(rbx11));
        goto addr_324e_6;
    } else {
        goto addr_324e_6;
    }
    if (reinterpret_cast<unsigned char>(v12) > reinterpret_cast<unsigned char>(rbx11)) {
        *reinterpret_cast<int32_t*>(&rax19) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax19) + 4) = 0;
        r12_20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v12) - reinterpret_cast<unsigned char>(rbx11));
        while (*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<uint64_t>(rax19)) = *reinterpret_cast<void***>(&r14d15), rax19 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax19) + 1), r12_20 != rax19) {
            r14d15 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbx11) + reinterpret_cast<uint64_t>(rax19));
        }
    }
    addr_324e_6:
}

/* tried_mount_list.3 */
signed char tried_mount_list_3 = 0;

struct s4 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[31] pad40;
    unsigned char f28;
    signed char[7] pad48;
    struct s4* f30;
};

struct s4* read_file_system_list();

/* mount_list.2 */
struct s4* mount_list_2 = reinterpret_cast<struct s4*>(0);

int32_t fun_2860(void** rdi, void** rsi, void** rdx, void** rcx);

uint32_t fun_2840(void** rdi, void** rsi, void** rdx, ...);

/* tz.1 */
void** tz_1 = reinterpret_cast<void**>(0);

void** fun_2590(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** tzalloc(void** rdi, void** rsi);

int64_t localtime_rz(void** rdi, void*** rsi, void** rdx, void** rcx);

void** imaxtostr(void** rdi, void* rsi, void** rdx, void** rcx);

void nstrftime();

void* fun_27e0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

struct s5 {
    signed char f0;
    void** f1;
};

void** find_bind_mount(void** rdi, int64_t rsi) {
    void** r12_3;
    void** rsp4;
    void** rax5;
    void** v6;
    int1_t zf7;
    struct s4* rax8;
    void** rax9;
    void** rcx10;
    void** rsi11;
    void** rdi12;
    int32_t eax13;
    int64_t* rsp14;
    struct s4* rbx15;
    void** rax16;
    void** r13_17;
    void** rbp18;
    uint32_t eax19;
    int32_t eax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    void** rdx25;
    void*** rsp26;
    void** rdi27;
    void** rax28;
    void** r8_29;
    void** r9_30;
    void** rax31;
    void** rax32;
    void** r12_33;
    void** rbx34;
    int64_t rax35;
    void** rax36;
    void** r9_37;
    void** rdx38;
    void** r8_39;
    void** rcx40;
    void* rax41;
    int64_t v42;
    void* rax43;
    void** rbx44;
    void* rax45;
    struct s5* rbx46;

    r12_3 = rdi;
    rsp4 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x138);
    rax5 = g28;
    v6 = rax5;
    zf7 = tried_mount_list_3 == 0;
    if (zf7) {
        rax8 = read_file_system_list();
        rsp4 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp4 - 8) + 8);
        mount_list_2 = rax8;
        if (!rax8) {
            rax9 = fun_26c0();
            fun_25e0();
            rcx10 = rax9;
            fun_2990();
            rsp4 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp4 - 8) + 8 - 8 + 8 - 8 + 8);
        }
        tried_mount_list_3 = 1;
    }
    rsi11 = rsp4;
    rdi12 = r12_3;
    eax13 = fun_2860(rdi12, rsi11, "%s", rcx10);
    rsp14 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp4 - 8) + 8);
    if (eax13 || (rbx15 = mount_list_2, rbx15 == 0)) {
        addr_3328_6:
        *reinterpret_cast<int32_t*>(&rax16) = 0;
        *reinterpret_cast<int32_t*>(&rax16 + 4) = 0;
    } else {
        r13_17 = reinterpret_cast<void**>(rsp14 + 18);
        do {
            if (!(rbx15->f28 & 1)) 
                continue;
            rbp18 = rbx15->f0;
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbp18) == 47)) 
                continue;
            rdi12 = rbx15->f8;
            rsi11 = r12_3;
            eax19 = fun_2840(rdi12, rsi11, "%s", rdi12, rsi11, "%s");
            rsp14 = rsp14 - 1 + 1;
            if (eax19) 
                continue;
            rsi11 = r13_17;
            rdi12 = rbp18;
            eax20 = fun_2860(rdi12, rsi11, "%s", rcx10);
            rsp14 = rsp14 - 1 + 1;
            if (eax20) 
                continue;
            if (v21 != v22) 
                continue;
            if (v23 == v24) 
                goto addr_331c_14;
            rbx15 = rbx15->f30;
        } while (rbx15);
        goto addr_3328_6;
    }
    addr_332a_16:
    rdx25 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v6) - reinterpret_cast<unsigned char>(g28));
    if (!rdx25) {
        return rax16;
    }
    fun_2710();
    rsp26 = reinterpret_cast<void***>(rsp14 - 1 + 1 - 1 - 1 - 15);
    rdi27 = tz_1;
    rax28 = g28;
    if (rdi27) 
        goto addr_33e0_20;
    rax31 = fun_2590("TZ", rsi11, rdx25, rcx10, r8_29, r9_30);
    rax32 = tzalloc(rax31, rsi11);
    rsp26 = rsp26 - 8 + 8 - 8 + 8;
    tz_1 = rax32;
    rdi27 = rax32;
    addr_33e0_20:
    r12_33 = reinterpret_cast<void**>(rsp26 + 16);
    rbx34 = rsi11;
    rax35 = localtime_rz(rdi27, rsp26, r12_33, rcx10);
    if (!rax35) {
        rax36 = imaxtostr(rdi12, rsp26 - 8 + 8 + 80, r12_33, rcx10);
        *reinterpret_cast<int32_t*>(&r9_37) = *reinterpret_cast<int32_t*>(&rbx34);
        *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdx38) = 61;
        *reinterpret_cast<int32_t*>(&rdx38 + 4) = 0;
        r8_39 = rax36;
        rcx40 = reinterpret_cast<void**>("%s.%09d");
        fun_2a90(0x18100, 1, 61, "%s.%09d", r8_39, r9_37, rdi12);
    } else {
        rcx40 = r12_33;
        *reinterpret_cast<int32_t*>(&r9_37) = *reinterpret_cast<int32_t*>(&rbx34);
        *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
        r8_39 = tz_1;
        rdx38 = reinterpret_cast<void**>("%Y-%m-%d %H:%M:%S.%N %z");
        nstrftime();
    }
    rax41 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax28) - reinterpret_cast<unsigned char>(g28));
    if (!rax41) {
        goto v42;
    }
    fun_2710();
    rax43 = fun_27e0(0x18101, "'-+ #0I", rdx38, rcx40, r8_39, r9_37);
    rbx44 = reinterpret_cast<void**>(0x18100 + reinterpret_cast<uint64_t>(rax43) + 1);
    rax45 = fun_27e0(rbx44, "0123456789", rdx38, rcx40, r8_39, r9_37);
    rbx46 = reinterpret_cast<struct s5*>(reinterpret_cast<unsigned char>(rbx44) + reinterpret_cast<uint64_t>(rax45));
    if (rbx46->f0 == 46) 
        goto addr_34e6_28;
    addr_34f7_29:
    goto rdi12;
    addr_34e6_28:
    fun_27e0(&rbx46->f1, "0123456789", rdx38, rcx40, r8_39, r9_37);
    goto addr_34f7_29;
    addr_331c_14:
    rax16 = rbx15->f0;
    goto addr_332a_16;
}

int64_t out_epoch_sec(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r15_5;
    void** r13_6;
    void** r12_7;
    void** rbp8;
    void** rbx9;
    struct s1* rax10;
    int32_t v11;
    int64_t r14_12;
    struct s1* r8_13;
    struct s1* rcx14;
    void** r10_15;
    void** v16;
    struct s1* v17;
    int64_t rax18;
    int64_t rdx19;
    void** r8_20;
    int64_t rsi21;
    int64_t rcx22;
    int64_t rax23;
    void* rdx24;
    void** rcx25;
    void** rdx26;
    int32_t eax27;
    int32_t eax28;
    int32_t edx29;
    void** rdi30;
    void** rdx31;
    int32_t r8d32;
    uint32_t esi33;
    int32_t ecx34;
    void** ecx35;
    void** r15_36;
    void** r9_37;
    int64_t rax38;
    void** r9_39;
    void** rcx40;
    int64_t r14_41;
    void** rax42;
    void** rdx43;
    void** r8_44;
    void** r9_45;
    int32_t eax46;

    r15_5 = rdx;
    r13_6 = rdi;
    r12_7 = rdx;
    rbp8 = rsi;
    rbx9 = rcx;
    rax10 = fun_27f0(rdi, 46, rsi);
    if (!rax10) {
        v11 = 0;
        *reinterpret_cast<int32_t*>(&r14_12) = 0;
        goto addr_3fcb_3;
    }
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r13_6) + reinterpret_cast<unsigned char>(rbp8)) = 0;
    r8_13 = rax10;
    rcx14 = rax10;
    r10_15 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax10) - reinterpret_cast<unsigned char>(r13_6));
    if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(&rax10->f1) - 48) <= 9) {
        v16 = r10_15;
        v17 = r8_13;
        rax18 = fun_2870(&r8_13->f1);
        *reinterpret_cast<int32_t*>(&rdx19) = 0x7fffffff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx19) + 4) = 0;
        r10_15 = v16;
        if (rax18 <= 0x7fffffff) {
            rdx19 = rax18;
        }
        v11 = *reinterpret_cast<int32_t*>(&rdx19);
        r14_12 = rdx19;
        if (*reinterpret_cast<int32_t*>(&rdx19)) 
            goto addr_3f9d_8;
    } else {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r8_13) - 1) - 48 <= 9) {
            *reinterpret_cast<int32_t*>(&r14_12) = 9;
            goto addr_407e_11;
        } else {
            r8_20 = rbx9;
            rbp8 = r10_15;
            *reinterpret_cast<int32_t*>(&rsi21) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi21) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rcx22) = 1;
            v11 = 0;
            *reinterpret_cast<int32_t*>(&r14_12) = 9;
            goto addr_3ea8_13;
        }
    }
    *reinterpret_cast<int32_t*>(&r14_12) = 0;
    rbp8 = r10_15;
    goto addr_3fcb_3;
    addr_3f9d_8:
    r8_13 = r8_13;
    if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r8_13) - 1) - 48 > 9) {
        v11 = 0;
        rbp8 = r10_15;
        goto addr_4148_16;
    } else {
        rcx14 = v17;
    }
    addr_407e_11:
    r8_13->f0 = 0;
    do {
        rcx14 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(rcx14) - 1);
    } while ((rcx14 - 1)->f0 - 48 <= 9);
    v16 = r10_15;
    rax23 = fun_2870(rcx14);
    rbp8 = v16;
    if (rax23 > 0x7fffffff) {
        rax23 = 0x7fffffff;
    }
    v11 = *reinterpret_cast<int32_t*>(&rax23);
    if (*reinterpret_cast<int32_t*>(&rax23) <= 1 || ((*reinterpret_cast<int32_t*>(&rdx24) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx24) + 4) = 0, *reinterpret_cast<unsigned char*>(&rdx24) = reinterpret_cast<uint1_t>(rcx14->f0 == 48), rcx25 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rcx14) + reinterpret_cast<uint64_t>(rdx24)), rdx26 = decimal_point_len, rbp8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx25) - reinterpret_cast<unsigned char>(r13_6)), reinterpret_cast<unsigned char>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax23))) <= reinterpret_cast<unsigned char>(rdx26)) || ((eax27 = *reinterpret_cast<int32_t*>(&rax23) - *reinterpret_cast<int32_t*>(&rdx26), eax27 <= 1) || (eax28 = eax27 - *reinterpret_cast<int32_t*>(&r14_12), eax28 <= 1)))) {
        addr_4148_16:
        if (*reinterpret_cast<int32_t*>(&r14_12) <= 8) {
            addr_3fcb_3:
            edx29 = *reinterpret_cast<int32_t*>(&r14_12);
            *reinterpret_cast<int32_t*>(&rcx22) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx22) + 4) = 0;
        } else {
            r8_20 = rbx9;
            *reinterpret_cast<int32_t*>(&rsi21) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi21) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rcx22) = 1;
            goto addr_3ea8_13;
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_6) >= reinterpret_cast<unsigned char>(rcx25)) {
            rdi30 = r13_6;
            *reinterpret_cast<int32_t*>(&rbp8) = 0;
            *reinterpret_cast<int32_t*>(&rbp8 + 4) = 0;
            goto addr_417e_25;
        } else {
            rdx31 = r13_6;
            rdi30 = r13_6;
            r8d32 = 0;
            do {
                esi33 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx31));
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&esi33) == 45)) {
                    *reinterpret_cast<void***>(rdi30) = *reinterpret_cast<void***>(&esi33);
                    ++rdi30;
                } else {
                    r8d32 = 1;
                }
                ++rdx31;
            } while (rcx25 != rdx31);
            rbp8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi30) - reinterpret_cast<unsigned char>(r13_6));
            if (*reinterpret_cast<signed char*>(&r8d32)) 
                goto addr_4148_16; else 
                goto addr_417e_25;
        }
    }
    do {
        ecx34 = static_cast<int32_t>(rcx22 + rcx22 * 4);
        ++edx29;
        *reinterpret_cast<int32_t*>(&rcx22) = ecx34 + ecx34;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx22) + 4) = 0;
    } while (edx29 != 9);
    rsi21 = *reinterpret_cast<int32_t*>(&rcx22);
    r8_20 = reinterpret_cast<void**>(reinterpret_cast<signed char>(rbx9) / rsi21);
    addr_3ea8_13:
    if (reinterpret_cast<signed char>(r15_5) >= reinterpret_cast<signed char>(0) || (!rbx9 || (ecx35 = reinterpret_cast<void**>(0x3b9aca00 / *reinterpret_cast<int32_t*>(&rcx22) - reinterpret_cast<unsigned char>(r8_20) + 0xffffffff + reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<signed char>(rbx9) % rsi21) < 1)), r8_20 = ecx35, *reinterpret_cast<int32_t*>(&r8_20 + 4) = 0, r15_36 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_5) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_5) < reinterpret_cast<unsigned char>(1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(ecx35) < reinterpret_cast<unsigned char>(1)))))))), r12_7 = r15_36, !!r15_36))) {
        v16 = r8_20;
        make_format(r13_6, rbp8, "'-+ 0", "ld", r8_20, r9_37, v16);
        rax38 = fun_2960(1, r13_6, r12_7, "ld");
    } else {
        v16 = ecx35;
        make_format(r13_6, rbp8, "'-+ 0", ".0f", r8_20, r9_39, v16);
        *reinterpret_cast<void***>(r13_6) = *reinterpret_cast<void***>(rbp8);
        rax38 = fun_2960(1, r13_6, "'-+ 0", ".0f");
    }
    if (*reinterpret_cast<int32_t*>(&r14_12)) {
        *reinterpret_cast<int32_t*>(&rcx40) = 9;
        *reinterpret_cast<int32_t*>(&rcx40 + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&r14_12) <= 9) {
            *reinterpret_cast<int32_t*>(&rcx40) = *reinterpret_cast<int32_t*>(&r14_12);
            *reinterpret_cast<int32_t*>(&rcx40 + 4) = 0;
        }
        *reinterpret_cast<int32_t*>(&r14_41) = *reinterpret_cast<int32_t*>(&r14_12) - *reinterpret_cast<int32_t*>(&rcx40);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_41) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rax38) < 0) {
            *reinterpret_cast<int32_t*>(&rax38) = 0;
        }
        if (*reinterpret_cast<int32_t*>(&rax38) < v11 && (rax42 = decimal_point_len, reinterpret_cast<unsigned char>(static_cast<int64_t>(v11 - *reinterpret_cast<int32_t*>(&rax38))) > reinterpret_cast<unsigned char>(rax42))) {
        }
        rdx43 = decimal_point;
        fun_2960(1, "%s%.*d%-*.*d", rdx43, rcx40, 1, "%s%.*d%-*.*d", rdx43, rcx40);
        rax38 = r14_41;
    }
    return rax38;
    addr_417e_25:
    *reinterpret_cast<int32_t*>(&r8_44) = eax28;
    *reinterpret_cast<int32_t*>(&r8_44 + 4) = 0;
    eax46 = fun_2a90(rdi30, 1, 0xffffffffffffffff, "%d", r8_44, r9_45, v16);
    rbp8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp8) + reinterpret_cast<uint64_t>(static_cast<int64_t>(eax46)));
    goto addr_4148_16;
}

void unescape_tab(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rbx5;
    void** rax6;
    void** rcx7;
    void** rdx8;
    void** rax9;
    uint32_t r8d10;
    void** rsi11;
    void** r9_12;
    int64_t rdi13;
    int64_t r11_14;
    int64_t r10_15;
    int64_t r11_16;
    int64_t rdx17;
    int32_t edx18;

    rbx5 = rdi;
    rax6 = fun_26e0(rdi);
    rcx7 = rbx5;
    *reinterpret_cast<int32_t*>(&rdx8) = 0;
    *reinterpret_cast<int32_t*>(&rdx8 + 4) = 0;
    rax9 = rax6 + 1;
    while (1) {
        r8d10 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbx5) + reinterpret_cast<unsigned char>(rdx8));
        rsi11 = rdx8 + 1;
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&r8d10) == 92) || ((r9_12 = rdx8 + 4, reinterpret_cast<unsigned char>(r9_12) >= reinterpret_cast<unsigned char>(rax9)) || ((*reinterpret_cast<uint32_t*>(&rdi13) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbx5) + reinterpret_cast<unsigned char>(rsi11)) - 48, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi13) + 4) = 0, *reinterpret_cast<unsigned char*>(&rdi13) > 3) || ((*reinterpret_cast<uint32_t*>(&r11_14) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbx5) + reinterpret_cast<unsigned char>(rdx8) + 2), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_14) + 4) = 0, *reinterpret_cast<int32_t*>(&r10_15) = static_cast<int32_t>(r11_14 - 48), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_15) + 4) = 0, *reinterpret_cast<unsigned char*>(&r10_15) > 7) || (*reinterpret_cast<uint32_t*>(&r11_16) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbx5) + reinterpret_cast<unsigned char>(rdx8) + 3) - 48, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_16) + 4) = 0, *reinterpret_cast<unsigned char*>(&r11_16) > 7))))) {
            *reinterpret_cast<void***>(rcx7) = *reinterpret_cast<void***>(&r8d10);
            ++rcx7;
            if (reinterpret_cast<unsigned char>(rax9) <= reinterpret_cast<unsigned char>(rsi11)) 
                break;
        } else {
            *reinterpret_cast<int32_t*>(&rdx17) = static_cast<int32_t>(r10_15 + rdi13 * 8);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx17) + 4) = 0;
            ++rcx7;
            rsi11 = r9_12;
            edx18 = static_cast<int32_t>(r11_16 + rdx17 * 8);
            *reinterpret_cast<signed char*>(rcx7 + 0xffffffffffffffff) = *reinterpret_cast<signed char*>(&edx18);
        }
        rdx8 = rsi11;
    }
    return;
}

void* fun_2980();

void fun_27a0(void** rdi, ...);

uint32_t** fun_2580();

struct s6 {
    signed char[1] pad1;
    void** f1;
};

uint32_t** fun_2a70(void** rdi, ...);

/* __strftime_internal.isra.0 */
void** __strftime_internal_isra_0(void** rdi, void** rsi, void** rdx, void** rcx, unsigned char r8b, void** r9d, int32_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11) {
    void** r14_12;
    void** rbp13;
    void** rbx14;
    void** v15;
    void** r15_16;
    void** v17;
    unsigned char v18;
    void** rax19;
    void** v20;
    void** rax21;
    void* rsp22;
    void** r11_23;
    void** r10d24;
    void** v25;
    void** v26;
    uint32_t eax27;
    void** r13_28;
    void* rax29;
    uint32_t edx30;
    unsigned char v31;
    void** rdi32;
    void** r8_33;
    int64_t rax34;
    uint64_t rax35;
    int32_t eax36;
    void** rbp37;
    void* rax38;
    void* rcx39;
    uint32_t ecx40;
    int64_t r9_41;
    void** rcx42;
    int64_t r9_43;
    void** rbp44;
    void** v45;
    signed char* rbp46;
    uint32_t** rax47;
    void** r8_48;
    int1_t cf49;
    void** rax50;
    void** rcx51;
    void** rbx52;
    uint32_t eax53;
    uint32_t eax54;
    int64_t rdx55;
    int32_t ecx56;
    uint64_t rax57;
    struct s6* rcx58;
    void** r8_59;
    void** r9_60;
    void** r15_61;
    struct s6* r15_62;
    uint32_t** rax63;
    int64_t rcx64;
    int1_t cf65;
    void** r8_66;
    void** v67;
    void* rbp68;
    uint32_t** rax69;
    int64_t rcx70;
    int1_t cf71;
    void* rbp72;
    uint32_t** rax73;
    int64_t rcx74;
    int1_t cf75;
    int64_t rax76;

    r14_12 = rdi;
    rbp13 = rdx;
    rbx14 = rcx;
    v15 = rsi;
    r15_16 = reinterpret_cast<void**>(static_cast<int64_t>(a7));
    v17 = r9d;
    v18 = r8b;
    rax19 = g28;
    v20 = rax19;
    rax21 = fun_25e0();
    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x4c8 - 8 + 8);
    r11_23 = *reinterpret_cast<void***>(rbx14 + 48);
    r10d24 = *reinterpret_cast<void***>(rbx14 + 8);
    v25 = rax21;
    v26 = *reinterpret_cast<void***>(rax21);
    if (!r11_23) {
    }
    if (reinterpret_cast<signed char>(r10d24) <= reinterpret_cast<signed char>(12)) {
        if (!r10d24) {
            r10d24 = reinterpret_cast<void**>(12);
        }
    } else {
        r10d24 = r10d24 - 12;
    }
    eax27 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp13));
    *reinterpret_cast<int32_t*>(&r13_28) = 0;
    *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
    if (*reinterpret_cast<signed char*>(&eax27)) {
        goto addr_777d_10;
    }
    while (1) {
        addr_77db_11:
        if (r14_12 && v15) {
            *reinterpret_cast<void***>(r14_12) = reinterpret_cast<void**>(0);
        }
        rdi = v26;
        *reinterpret_cast<void***>(v25) = rdi;
        while (rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v20) - reinterpret_cast<unsigned char>(g28)), !!rax29) {
            fun_2710();
            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
            edx30 = *reinterpret_cast<uint32_t*>(&rdx) + 100;
            if (!r10d24) {
                if (v17 == 43) {
                    addr_8e1f_18:
                } else {
                    r10d24 = v17;
                    goto addr_7d4c_20;
                }
            } else {
                if (!reinterpret_cast<int1_t>(r10d24 == 43)) 
                    goto addr_7d4c_20; else 
                    goto addr_8e1f_18;
            }
            r10d24 = reinterpret_cast<void**>(43);
            v31 = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r15_16) > reinterpret_cast<signed char>(2));
            addr_7d63_24:
            if (static_cast<int1_t>(!reinterpret_cast<int1_t>(rdi == 79))) {
                if (0) {
                    edx30 = -edx30;
                }
                rdi32 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 0xc7);
                r8_33 = rdi32;
                while (1) {
                    rsi = r8_33;
                    if (!1) {
                        --rsi;
                    }
                    *reinterpret_cast<uint32_t*>(&rax34) = edx30;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax34) + 4) = 0;
                    r8_33 = rsi + 0xffffffffffffffff;
                    rax35 = reinterpret_cast<uint64_t>(rax34 * 0xcccccccd) >> 35;
                    if (edx30 > 9) 
                        goto addr_85c3_33;
                    if (1) 
                        break;
                    addr_85c3_33:
                    edx30 = *reinterpret_cast<uint32_t*>(&rax35);
                }
                if (!r10d24) {
                    eax36 = 1;
                    r10d24 = reinterpret_cast<void**>(48);
                } else {
                    *reinterpret_cast<unsigned char*>(&eax36) = reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(r10d24 == 45));
                }
                if (reinterpret_cast<signed char>(r15_16) < reinterpret_cast<signed char>(0)) {
                    r15_16 = reinterpret_cast<void**>(2);
                    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
                }
                rdi = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi32) - reinterpret_cast<unsigned char>(r8_33));
                rbp37 = rdi;
                if (!0) 
                    goto addr_819a_41;
            } else {
                rdx = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 0xab);
                rdi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 0xb0);
                rax38 = fun_2980();
                rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                rcx39 = rax38;
                if (!rax38) 
                    goto addr_77c8_45; else 
                    goto addr_7af9_46;
            }
            ecx40 = 45;
            addr_8ab5_48:
            rsi = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<uint64_t>(r15_16 + 0xffffffffffffffff)));
            *reinterpret_cast<uint32_t*>(&rdx) = reinterpret_cast<unsigned char>(rsi) - *reinterpret_cast<uint32_t*>(&rbp37);
            if (reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rdx) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rdx) == 0) || !*reinterpret_cast<unsigned char*>(&eax36)) {
                *reinterpret_cast<uint32_t*>(&rdx) = 0;
            }
            if (r10d24 == 95) {
                *reinterpret_cast<void**>(&r9_41) = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r15_16) - *reinterpret_cast<uint32_t*>(&rdx));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_41) + 4) = 0;
                rdx = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rdx)));
                r13_28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_28) + reinterpret_cast<unsigned char>(rdx));
                r15_16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28));
                if (!r14_12) {
                    if (reinterpret_cast<unsigned char>(r15_16) <= reinterpret_cast<unsigned char>(1)) 
                        goto addr_7800_53;
                    ++r13_28;
                    rsi = reinterpret_cast<void**>(static_cast<uint32_t>(r9_41 - 1));
                    rcx42 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbp37)));
                    goto addr_8afa_55;
                } else {
                    rdi = r14_12;
                    fun_27a0(rdi);
                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                    rdx = rdx;
                    r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rdx));
                    if (reinterpret_cast<unsigned char>(r15_16) <= reinterpret_cast<unsigned char>(1)) 
                        goto addr_7800_53;
                    *reinterpret_cast<void**>(&r9_43) = *reinterpret_cast<void**>(&r9_41);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_43) + 4) = 0;
                    r8_33 = r8_33;
                    r10d24 = r10d24;
                    ecx40 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&ecx40));
                    rsi = reinterpret_cast<void**>(static_cast<uint32_t>(r9_43 - 1));
                }
            } else {
                if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28)) <= 1) 
                    goto addr_7800_53;
                if (!r14_12) 
                    goto addr_8aed_60;
            }
            *reinterpret_cast<void***>(r14_12) = *reinterpret_cast<void***>(&ecx40);
            ++r14_12;
            addr_8aed_60:
            ++r13_28;
            rcx42 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbp37)));
            if (r10d24 == 45) {
                addr_8b2e_62:
                r15_16 = rcx42;
                *reinterpret_cast<uint32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                goto addr_81c6_63;
            } else {
                addr_8afa_55:
                r15_16 = rcx42;
                *reinterpret_cast<uint32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                if (reinterpret_cast<signed char>(rsi) < reinterpret_cast<signed char>(0)) {
                    addr_81c6_63:
                    if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28)) <= reinterpret_cast<unsigned char>(r15_16)) {
                        addr_7800_53:
                        *reinterpret_cast<void***>(v25) = reinterpret_cast<void**>(34);
                    } else {
                        if (r14_12) {
                            if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rcx42)) {
                                rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx) - reinterpret_cast<unsigned char>(rcx42));
                                rbp44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rdx));
                                if (r10d24 == 48 || r10d24 == 43) {
                                    rdi = r14_12;
                                    r14_12 = rbp44;
                                    fun_27a0(rdi);
                                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                                    rcx42 = rcx42;
                                    r8_33 = r8_33;
                                } else {
                                    rdi = r14_12;
                                    r14_12 = rbp44;
                                    fun_27a0(rdi);
                                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                                    r8_33 = r8_33;
                                    rcx42 = rcx42;
                                }
                            }
                            if (!*reinterpret_cast<signed char*>(&v45)) {
                                rdx = rcx42;
                                rdi = r14_12;
                                v45 = rcx42;
                                fun_2880(rdi, r8_33, rdx);
                                rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                                rcx42 = v45;
                            } else {
                                v45 = r8_33;
                                rbp46 = reinterpret_cast<signed char*>(rcx42 + 0xffffffffffffffff);
                                if (rcx42) {
                                    rax47 = fun_2580();
                                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                                    rcx42 = rcx42;
                                    r8_48 = v45;
                                    do {
                                        rsi = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r8_48) + reinterpret_cast<uint64_t>(rbp46))));
                                        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                                        *reinterpret_cast<uint32_t*>(&rdx) = (*rax47)[reinterpret_cast<unsigned char>(rsi)];
                                        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<uint64_t>(rbp46)) = *reinterpret_cast<signed char*>(&rdx);
                                        cf49 = reinterpret_cast<uint64_t>(rbp46) < 1;
                                        --rbp46;
                                    } while (!cf49);
                                }
                            }
                            r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rcx42));
                            goto addr_7bd7_75;
                        }
                    }
                } else {
                    r15_16 = rsi;
                    goto addr_81b9_77;
                }
            }
            *reinterpret_cast<int32_t*>(&r13_28) = 0;
            *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
            continue;
            addr_7bd7_75:
            r13_28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_28) + reinterpret_cast<unsigned char>(r15_16));
            addr_77c8_45:
            while (eax27 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14 + 1)), rbp13 = rbx14 + 1, r15_16 = reinterpret_cast<void**>(0xffffffffffffffff), !!*reinterpret_cast<signed char*>(&eax27)) {
                addr_777d_10:
                if (*reinterpret_cast<signed char*>(&eax27) != 37) {
                    *reinterpret_cast<int32_t*>(&rax50) = 0;
                    *reinterpret_cast<int32_t*>(&rax50 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rcx51) = 1;
                    *reinterpret_cast<int32_t*>(&rcx51 + 4) = 0;
                    if (reinterpret_cast<signed char>(r15_16) >= reinterpret_cast<signed char>(0)) {
                        rax50 = r15_16;
                    }
                    if (rax50) {
                        rcx51 = rax50;
                    }
                    rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28));
                    if (reinterpret_cast<unsigned char>(rcx51) >= reinterpret_cast<unsigned char>(rdx)) 
                        goto addr_7800_53;
                    if (r14_12) {
                        if (reinterpret_cast<signed char>(r15_16) > reinterpret_cast<signed char>(1)) {
                            rbx52 = rax50 + 0xffffffffffffffff;
                            rdi = r14_12;
                            v45 = rcx51;
                            rdx = rbx52;
                            r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rbx52));
                            fun_27a0(rdi);
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            rcx51 = v45;
                        }
                        eax53 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp13));
                        ++r14_12;
                        *reinterpret_cast<signed char*>(r14_12 + 0xffffffffffffffff) = *reinterpret_cast<signed char*>(&eax53);
                    }
                    r13_28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_28) + reinterpret_cast<unsigned char>(rcx51));
                    rbx14 = rbp13;
                    continue;
                }
                eax54 = v18;
                rbx14 = rbp13;
                r10d24 = reinterpret_cast<void**>(0);
                *reinterpret_cast<signed char*>(&v45) = *reinterpret_cast<signed char*>(&eax54);
                while (*reinterpret_cast<void***>(&rdx55) = reinterpret_cast<void**>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx14 + 1)))), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx55) + 4) = 0, ++rbx14, ecx56 = static_cast<int32_t>(rdx55 - 35), rsi = *reinterpret_cast<void***>(&rdx55), rdi = *reinterpret_cast<void***>(&rdx55), *reinterpret_cast<unsigned char*>(&ecx56) <= 60) {
                    rax57 = 1 << *reinterpret_cast<unsigned char*>(&ecx56);
                    if (rax57 & 0x1000000000002500) {
                        r10d24 = *reinterpret_cast<void***>(&rdx55);
                    } else {
                        if (*reinterpret_cast<unsigned char*>(&ecx56) == 59) {
                            *reinterpret_cast<signed char*>(&v45) = 1;
                        } else {
                            if (!(*reinterpret_cast<uint32_t*>(&rax57) & 1)) 
                                break;
                        }
                    }
                }
                if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(&rdx55) - 48) > 9) 
                    goto addr_78df_98;
                r15_16 = reinterpret_cast<void**>(0);
                do {
                    if (__intrinsic() || (r15_16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_16) * 10 + reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14) - 48)), *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0, __intrinsic())) {
                        r15_16 = reinterpret_cast<void**>(0x7fffffff);
                        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
                    }
                    rdi = reinterpret_cast<void**>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx14 + 1))));
                    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                    ++rbx14;
                    rsi = rdi;
                } while (static_cast<uint32_t>(reinterpret_cast<uint64_t>(rdi + 0xffffffffffffffd0)) <= 9);
                addr_78df_98:
                if (*reinterpret_cast<unsigned char*>(&rsi) == 69 || *reinterpret_cast<unsigned char*>(&rsi) == 79) {
                    rsi = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14 + 1))));
                    ++rbx14;
                } else {
                    rdi = reinterpret_cast<void**>(0);
                }
                if (*reinterpret_cast<unsigned char*>(&rsi) <= 0x7a) 
                    goto addr_78fb_106;
                rcx58 = reinterpret_cast<struct s6*>(reinterpret_cast<unsigned char>(rbx14) - reinterpret_cast<unsigned char>(rbp13));
                r8_59 = reinterpret_cast<void**>(&rcx58->f1);
                if (reinterpret_cast<signed char>(r15_16) < reinterpret_cast<signed char>(0) || r10d24 == 45) {
                    r9_60 = r8_59;
                    *reinterpret_cast<uint32_t*>(&rdx) = 0;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                } else {
                    rdx = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(r15_16)));
                    r9_60 = rdx;
                    if (reinterpret_cast<unsigned char>(r8_59) >= reinterpret_cast<unsigned char>(rdx)) {
                        r9_60 = r8_59;
                    }
                }
                if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28)) <= reinterpret_cast<unsigned char>(r9_60)) 
                    goto addr_7800_53;
                if (r14_12) {
                    if (reinterpret_cast<unsigned char>(r8_59) < reinterpret_cast<unsigned char>(rdx)) {
                        rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx) - reinterpret_cast<unsigned char>(r8_59));
                        r15_61 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rdx));
                        if (r10d24 == 48 || r10d24 == 43) {
                            rdi = r14_12;
                            r14_12 = r15_61;
                            fun_27a0(rdi);
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            rcx58 = rcx58;
                            r8_59 = r8_59;
                            r9_60 = r9_60;
                        } else {
                            rdi = r14_12;
                            r14_12 = r15_61;
                            fun_27a0(rdi);
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            r9_60 = r9_60;
                            r8_59 = r8_59;
                            rcx58 = rcx58;
                        }
                    }
                    if (!*reinterpret_cast<signed char*>(&v45)) {
                        rdx = r8_59;
                        rdi = r14_12;
                        v45 = r8_59;
                        fun_2880(rdi, rbp13, rdx);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        r9_60 = r9_60;
                        r8_59 = v45;
                    } else {
                        r15_62 = rcx58;
                        if (r8_59) {
                            v45 = r8_59;
                            rax63 = fun_2580();
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            r8_59 = v45;
                            r9_60 = r9_60;
                            do {
                                *reinterpret_cast<uint32_t*>(&rcx64) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp13) + reinterpret_cast<uint64_t>(r15_62));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx64) + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&rdx) = (*rax63)[rcx64];
                                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<uint64_t>(r15_62)) = *reinterpret_cast<signed char*>(&rdx);
                                cf65 = reinterpret_cast<uint64_t>(r15_62) < 1;
                                r15_62 = reinterpret_cast<struct s6*>(reinterpret_cast<uint64_t>(r15_62) - 1);
                            } while (!cf65);
                        }
                    }
                    r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(r8_59));
                }
                r13_28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_28) + reinterpret_cast<unsigned char>(r9_60));
            }
            goto addr_77db_11;
            addr_81b9_77:
            rdx = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(r15_16)));
            r15_16 = rcx42;
            if (reinterpret_cast<unsigned char>(rdx) >= reinterpret_cast<unsigned char>(rcx42)) {
                r15_16 = rdx;
                goto addr_81c6_63;
            }
            addr_819a_41:
            if (v31) {
                ecx40 = 43;
                goto addr_8ab5_48;
            } else {
                rcx42 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(rdi)));
                if (reinterpret_cast<signed char>(r15_16) <= reinterpret_cast<signed char>(rdi)) 
                    goto addr_8b24_129;
                if (*reinterpret_cast<unsigned char*>(&eax36)) 
                    goto addr_81b9_77;
                addr_8b24_129:
                if (!reinterpret_cast<int1_t>(r10d24 == 45)) 
                    goto addr_81b9_77; else 
                    goto addr_8b2e_62;
            }
            addr_7af9_46:
            r10d24 = r10d24;
            r8_66 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax38) + 0xffffffffffffffff);
            if (r10d24 == 45 || reinterpret_cast<signed char>(r15_16) < reinterpret_cast<signed char>(0)) {
                r15_16 = r8_66;
                *reinterpret_cast<uint32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            } else {
                rdx = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(r15_16)));
                r15_16 = rdx;
                if (reinterpret_cast<unsigned char>(r8_66) >= reinterpret_cast<unsigned char>(rdx)) {
                    r15_16 = r8_66;
                }
            }
            if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28)) <= reinterpret_cast<unsigned char>(r15_16)) 
                goto addr_7800_53;
            if (r14_12) {
                if (reinterpret_cast<unsigned char>(r8_66) < reinterpret_cast<unsigned char>(rdx)) {
                    rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx) - reinterpret_cast<unsigned char>(r8_66));
                    v67 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rdx));
                    if (r10d24 == 48 || r10d24 == 43) {
                        rdi = r14_12;
                        fun_27a0(rdi);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        r14_12 = v67;
                        rcx39 = rcx39;
                        r8_66 = r8_66;
                    } else {
                        rdi = r14_12;
                        fun_27a0(rdi);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        r14_12 = v67;
                        r8_66 = r8_66;
                        rcx39 = rcx39;
                    }
                }
                if (0) {
                    rbp68 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rcx39) + 0xfffffffffffffffe);
                    if (r8_66) {
                        v45 = r8_66;
                        rax69 = fun_2a70(rdi);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        r8_66 = v45;
                        rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 0xb1);
                        do {
                            *reinterpret_cast<uint32_t*>(&rcx70) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<uint64_t>(rbp68));
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx70) + 4) = 0;
                            *reinterpret_cast<uint32_t*>(&rdx) = (*rax69)[rcx70];
                            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<uint64_t>(rbp68)) = *reinterpret_cast<signed char*>(&rdx);
                            cf71 = reinterpret_cast<uint64_t>(rbp68) < 1;
                            rbp68 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp68) - 1);
                        } while (!cf71);
                    }
                } else {
                    if (!*reinterpret_cast<signed char*>(&v45)) {
                        rdx = r8_66;
                        rdi = r14_12;
                        v45 = r8_66;
                        fun_2880(rdi, reinterpret_cast<int64_t>(rsp22) + 0xb1, rdx);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        r8_66 = v45;
                    } else {
                        rbp72 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rcx39) + 0xfffffffffffffffe);
                        if (r8_66) {
                            v45 = r8_66;
                            rax73 = fun_2580();
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            r8_66 = v45;
                            rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 0xb1);
                            do {
                                *reinterpret_cast<uint32_t*>(&rcx74) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<uint64_t>(rbp72));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx74) + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&rdx) = (*rax73)[rcx74];
                                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<uint64_t>(rbp72)) = *reinterpret_cast<signed char*>(&rdx);
                                cf75 = reinterpret_cast<uint64_t>(rbp72) < 1;
                                rbp72 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp72) - 1);
                            } while (!cf75);
                        }
                    }
                }
                r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(r8_66));
                goto addr_7bd7_75;
            }
            addr_7d4c_20:
            v31 = 0;
            goto addr_7d63_24;
        }
        break;
    }
    return r13_28;
    addr_78fb_106:
    *reinterpret_cast<uint32_t*>(&rax76) = *reinterpret_cast<unsigned char*>(&rsi);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax76) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x12cf4 + rax76 * 4) + 0x12cf4;
}

int64_t fun_26d0();

int64_t fun_25d0(void** rdi, ...);

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_26d0();
    if (r8d > 10) {
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x12f80 + rax11 * 4) + 0x12f80;
    }
}

struct s7 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

struct s8 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(void** rdi, int64_t rsi, int64_t rdx, struct s7* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    void** rax8;
    void** r15_9;
    void** v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rdi14;
    void** rdi15;
    int64_t rax16;
    uint32_t r8d17;
    struct s8* rbx18;
    uint32_t r15d19;
    void** rsi20;
    void** r14_21;
    int64_t v22;
    int64_t v23;
    uint32_t r15d24;
    void** rax25;
    void** rsi26;
    void** rax27;
    uint32_t r8d28;
    int64_t v29;
    int64_t v30;
    void* rax31;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0xa84f;
    rax8 = fun_25e0();
    r15_9 = slotvec;
    v10 = *reinterpret_cast<void***>(rax8);
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
        fun_25d0(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0x18090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0xd6a1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            rdi14 = reinterpret_cast<int32_t>(nslots);
            rdi15 = reinterpret_cast<void**>((rdi14 << 4) + reinterpret_cast<unsigned char>(r15_9));
            v7 = 0xa8db;
            fun_27a0(rdi15, rdi15);
            rax16 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax16);
        }
        r8d17 = rcx->f0;
        rbx18 = reinterpret_cast<struct s8*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d19 = rcx->f4;
        rsi20 = rbx18->f0;
        r14_21 = rbx18->f8;
        v22 = rcx->f30;
        v23 = rcx->f28;
        r15d24 = r15d19 | 1;
        rax25 = quotearg_buffer_restyled(r14_21, rsi20, rsi, rdx, r8d17, r15d24, &rcx->f8, v23, v22, v7);
        if (reinterpret_cast<unsigned char>(rsi20) <= reinterpret_cast<unsigned char>(rax25)) {
            rsi26 = rax25 + 1;
            rbx18->f0 = rsi26;
            if (r14_21 != 0x181c0) {
                fun_25b0(r14_21, r14_21);
                rsi26 = rsi26;
            }
            rax27 = xcharalloc(rsi26, rsi26);
            r8d28 = rcx->f0;
            rbx18->f8 = rax27;
            v29 = rcx->f30;
            r14_21 = rax27;
            v30 = rcx->f28;
            quotearg_buffer_restyled(rax27, rsi26, rsi, rdx, r8d28, r15d24, rsi26, v30, v29, 0xa96a);
        }
        *reinterpret_cast<void***>(rax8) = v10;
        rax31 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax31) {
            fun_2710();
        } else {
            return r14_21;
        }
    }
}

int32_t fun_29e0(int64_t rdi, void** rsi);

int32_t fun_2670(int64_t rdi, void** rsi, void** rdx);

void fun_2890(int64_t rdi, void** rsi, void** rdx);

void** set_tz(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rax7;
    uint32_t eax8;
    void** rax9;
    void** r12_10;
    int32_t eax11;
    int32_t eax12;
    void** rax13;
    void** ebx14;
    void** rbp15;
    void** rdi16;

    rax7 = fun_2590("TZ", rsi, rdx, rcx, r8, r9);
    if (!rax7) {
        if (!*reinterpret_cast<void***>(rdi + 8)) {
            return 1;
        }
    } else {
        if (*reinterpret_cast<void***>(rdi + 8) && (rsi = rax7, eax8 = fun_2840(rdi + 9, rsi, rdx), !eax8)) {
            return 1;
        }
    }
    rax9 = tzalloc(rax7, rsi);
    r12_10 = rax9;
    if (!rax9) {
        addr_b961_7:
        return r12_10;
    } else {
        if (!*reinterpret_cast<void***>(rdi + 8)) {
            eax11 = fun_29e0("TZ", rsi);
            if (eax11) 
                goto addr_b9cd_10; else 
                goto addr_b95c_11;
        }
        rsi = rdi + 9;
        *reinterpret_cast<int32_t*>(&rdx) = 1;
        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
        eax12 = fun_2670("TZ", rsi, 1);
        if (!eax12) {
            addr_b95c_11:
            fun_2890("TZ", rsi, rdx);
            goto addr_b961_7;
        } else {
            addr_b9cd_10:
            rax13 = fun_25e0();
            ebx14 = *reinterpret_cast<void***>(rax13);
            rbp15 = rax13;
            if (r12_10 != 1) {
                do {
                    rdi16 = r12_10;
                    r12_10 = *reinterpret_cast<void***>(r12_10);
                    fun_25b0(rdi16, rdi16);
                } while (r12_10);
            }
        }
    }
    *reinterpret_cast<void***>(rbp15) = ebx14;
    return 0;
}

signed char save_abbr(void** rdi, void** rsi) {
    void** r12_3;
    void** rbp4;
    void** r13_5;
    void** rdx6;
    int32_t eax7;
    void** rbx8;
    void** rsi9;
    uint32_t eax10;
    void** rax11;
    uint32_t eax12;
    void** rax13;
    void** rdx14;
    void** rax15;

    r12_3 = *reinterpret_cast<void***>(rsi + 48);
    if (!r12_3) {
        return 1;
    }
    rbp4 = rdi;
    r13_5 = rsi;
    if (reinterpret_cast<unsigned char>(rsi) > reinterpret_cast<unsigned char>(r12_3) || (rdx6 = rsi + 56, eax7 = 1, reinterpret_cast<unsigned char>(r12_3) >= reinterpret_cast<unsigned char>(rdx6))) {
        rbx8 = rbp4 + 9;
        if (!*reinterpret_cast<void***>(r12_3)) {
            rbx8 = reinterpret_cast<void**>(0x13341);
        } else {
            while (rsi9 = r12_3, eax10 = fun_2840(rbx8, rsi9, rdx6), !!eax10) {
                do {
                    if (*reinterpret_cast<void***>(rbx8)) 
                        goto addr_b843_9;
                    if (rbx8 != rbp4 + 9) 
                        goto addr_b8b0_11;
                    if (!*reinterpret_cast<void***>(rbp4 + 8)) 
                        goto addr_b8b0_11;
                    addr_b843_9:
                    rax11 = fun_26e0(rbx8, rbx8);
                    rbx8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx8) + reinterpret_cast<unsigned char>(rax11) + 1);
                    if (*reinterpret_cast<void***>(rbx8)) 
                        break;
                    if (!*reinterpret_cast<void***>(rbp4)) 
                        break;
                    rbx8 = *reinterpret_cast<void***>(rbp4) + 9;
                    rsi9 = r12_3;
                    rbp4 = *reinterpret_cast<void***>(rbp4);
                    eax12 = fun_2840(rbx8, rsi9, rdx6);
                } while (eax12);
                goto addr_b874_15;
            }
        }
    } else {
        addr_b881_16:
        return *reinterpret_cast<signed char*>(&eax7);
    }
    addr_b878_17:
    *reinterpret_cast<void***>(r13_5 + 48) = rbx8;
    eax7 = 1;
    goto addr_b881_16;
    addr_b8b0_11:
    rax13 = fun_26e0(r12_3, r12_3);
    rdx14 = rax13 + 1;
    if (reinterpret_cast<signed char>(reinterpret_cast<uint64_t>(rbp4 + 0x80) - reinterpret_cast<unsigned char>(rbx8)) <= reinterpret_cast<signed char>(rdx14)) {
        rax15 = tzalloc(r12_3, rsi9);
        *reinterpret_cast<void***>(rbp4) = rax15;
        if (!rax15) {
            eax7 = 0;
            goto addr_b881_16;
        } else {
            *reinterpret_cast<void***>(rax15 + 8) = reinterpret_cast<void**>(0);
            rbx8 = rax15 + 9;
            goto addr_b878_17;
        }
    } else {
        fun_2880(rbx8, r12_3, rdx14);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx8) + reinterpret_cast<unsigned char>(rax13) + 1) = 0;
        goto addr_b878_17;
    }
    addr_b874_15:
    goto addr_b878_17;
}

/* revert_tz.part.0 */
signed char revert_tz_part_0(void** rdi, void** rsi) {
    void** rbx3;
    void** rax4;
    void** r12d5;
    void** rbp6;
    int32_t eax7;
    int32_t r13d8;
    void** rdi9;
    int32_t eax10;
    int32_t eax11;

    rbx3 = rdi;
    rax4 = fun_25e0();
    r12d5 = *reinterpret_cast<void***>(rax4);
    rbp6 = rax4;
    if (*reinterpret_cast<void***>(rbx3 + 8)) {
        rsi = rbx3 + 9;
        eax7 = fun_2670("TZ", rsi, 1);
        if (eax7) {
            addr_b6de_3:
            r12d5 = *reinterpret_cast<void***>(rbp6);
            r13d8 = 0;
        } else {
            addr_b729_4:
            fun_2890("TZ", rsi, 1);
            r13d8 = 1;
        }
        do {
            rdi9 = rbx3;
            rbx3 = *reinterpret_cast<void***>(rbx3);
            fun_25b0(rdi9, rdi9);
        } while (rbx3);
        *reinterpret_cast<void***>(rbp6) = r12d5;
        eax10 = r13d8;
        return *reinterpret_cast<signed char*>(&eax10);
    } else {
        eax11 = fun_29e0("TZ", rsi);
        if (!eax11) 
            goto addr_b729_4; else 
            goto addr_b6de_3;
    }
}

uint64_t ydhms_diff(int64_t rdi, int64_t rsi, int32_t edx, uint32_t ecx, int32_t r8d, int32_t r9d, int32_t a7, int32_t a8, int32_t a9, int32_t a10) {
    int64_t rcx11;
    uint64_t rcx12;
    int64_t rax13;
    int64_t rbx14;
    int64_t rdx15;
    int64_t rax16;
    int64_t r9_17;
    int64_t r9_18;
    int64_t rbp19;
    int64_t rdi20;
    int32_t ecx21;
    int64_t rdx22;
    uint32_t edx23;
    int64_t rbp24;
    int32_t r12d25;
    int64_t rdx26;
    int64_t rcx27;
    uint32_t ecx28;
    int64_t rcx29;
    int64_t rdx30;
    int64_t rax31;
    int64_t rax32;
    int64_t rax33;

    rcx11 = rdi;
    *reinterpret_cast<uint32_t*>(&rcx12) = *reinterpret_cast<uint32_t*>(&rcx11) & 3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx12) + 4) = 0;
    rax13 = rdi >> 2;
    rbx14 = r9d;
    rdx15 = rbx14;
    *reinterpret_cast<uint32_t*>(&rax16) = *reinterpret_cast<uint32_t*>(&rax13) - (0x1db - reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax13) < 0x1db - reinterpret_cast<uint1_t>(rcx12 < 1)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
    r9_17 = rbx14 >> 2;
    *reinterpret_cast<uint32_t*>(&r9_18) = *reinterpret_cast<uint32_t*>(&r9_17) - (0x1db - reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r9_17) < 0x1db - reinterpret_cast<uint1_t>((*reinterpret_cast<uint32_t*>(&rdx15) & 3) < 1)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_18) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rbp19) = *reinterpret_cast<uint32_t*>(&rax16) >> 31;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp19) + 4) = 0;
    rdi20 = rdi - rbx14;
    ecx21 = static_cast<int32_t>(rbp19 + rax16);
    rdx22 = ecx21 * 0x51eb851f >> 35;
    edx23 = *reinterpret_cast<int32_t*>(&rdx22) - (ecx21 >> 31) - *reinterpret_cast<uint32_t*>(&rbp19);
    *reinterpret_cast<uint32_t*>(&rbp24) = *reinterpret_cast<uint32_t*>(&r9_18) >> 31;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp24) + 4) = 0;
    r12d25 = static_cast<int32_t>(rbp24 + r9_18);
    rdx26 = static_cast<int64_t>(reinterpret_cast<int32_t>(edx23)) >> 2;
    rcx27 = r12d25 * 0x51eb851f >> 35;
    ecx28 = *reinterpret_cast<int32_t*>(&rcx27) - (r12d25 >> 31) - *reinterpret_cast<uint32_t*>(&rbp24);
    rcx29 = static_cast<int64_t>(reinterpret_cast<int32_t>(ecx28)) >> 2;
    rdx30 = rdi20 + (rdi20 + rdi20 * 8) * 8;
    rax31 = reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax16) - *reinterpret_cast<uint32_t*>(&r9_18) - (edx23 - ecx28) + (*reinterpret_cast<int32_t*>(&rdx26) - *reinterpret_cast<uint32_t*>(&rcx29))) + (rdx30 + rdx30 * 4 + rsi - a7);
    rax32 = edx + (rax31 + rax31 * 2) * 8 - a8;
    rax33 = reinterpret_cast<int32_t>(ecx) + ((rax32 << 4) - rax32) * 4 - a9;
    return r8d + ((rax33 << 4) - rax33) * 4 - reinterpret_cast<uint64_t>(static_cast<int64_t>(a10));
}

struct s9 {
    int32_t f0;
    int32_t f4;
    int32_t f8;
    int32_t fc;
    int32_t f10;
    int32_t f14;
    int32_t f18;
    int32_t f1c;
    int32_t f20;
    signed char[4] pad40;
    int64_t f28;
    int64_t f30;
};

struct s9* ranged_convert(int64_t rdi, uint64_t* rsi, struct s9* rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t rbx7;
    uint64_t r12_8;
    uint64_t* v9;
    void* rbp10;
    void** rax11;
    void** v12;
    struct s9* rax13;
    struct s9* v14;
    void** rax15;
    int1_t zf16;
    void** v17;
    uint64_t rcx18;
    int64_t rcx19;
    uint64_t r13_20;
    void* rax21;
    int32_t v22;
    uint64_t r14_23;
    uint64_t r12_24;
    uint64_t r13_25;
    struct s9* r15_26;
    int64_t rax27;
    int32_t v28;
    int32_t v29;
    int32_t v30;
    int32_t v31;
    int32_t v32;
    int32_t v33;
    int32_t v34;
    int32_t v35;
    int64_t v36;
    int64_t v37;
    uint64_t rax38;
    uint64_t rax39;

    rbx7 = rdi;
    r12_8 = *rsi;
    v9 = rsi;
    rbp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x68 + 80);
    rax11 = g28;
    v12 = rax11;
    rax13 = reinterpret_cast<struct s9*>(rbx7(rbp10, rdx));
    v14 = rax13;
    if (!rax13) {
        rax15 = fun_25e0();
        zf16 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax15) == 75);
        v17 = rax15;
        if (!zf16 || ((rcx18 = r12_8, *reinterpret_cast<uint32_t*>(&rcx19) = *reinterpret_cast<uint32_t*>(&rcx18) & 1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx19) + 4) = 0, r13_20 = rcx19 + reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_8) >> 1), r12_8 == r13_20) || !r13_20)) {
            addr_cb5e_3:
            rax21 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v12) - reinterpret_cast<unsigned char>(g28));
            if (rax21) {
                fun_2710();
            } else {
                return v14;
            }
        } else {
            v22 = -1;
            r14_23 = r12_8;
            r12_24 = r13_20;
            r13_25 = 0;
            r15_26 = rdx;
            do {
                rax27 = reinterpret_cast<int64_t>(rbx7(rbp10, r15_26));
                if (rax27) {
                    r13_25 = r12_24;
                    v22 = r15_26->f0;
                    v28 = r15_26->f4;
                    v29 = r15_26->f8;
                    v30 = r15_26->fc;
                    v31 = r15_26->f10;
                    v32 = r15_26->f14;
                    v33 = r15_26->f18;
                    v34 = r15_26->f1c;
                    v35 = r15_26->f20;
                    v36 = r15_26->f28;
                    v37 = r15_26->f30;
                } else {
                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(v17) == 75)) 
                        goto addr_cb5e_3;
                    r14_23 = r12_24;
                }
                rax38 = r13_25 | r14_23;
                *reinterpret_cast<uint32_t*>(&rax39) = *reinterpret_cast<uint32_t*>(&rax38) & 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax39) + 4) = 0;
                r12_24 = (reinterpret_cast<int64_t>(r13_25) >> 1) + (reinterpret_cast<int64_t>(r14_23) >> 1) + rax39;
            } while (r12_24 != r13_25 && r12_24 != r14_23);
        }
        if (v22 >= 0) {
            v14 = r15_26;
            *v9 = r13_25;
            r15_26->f0 = v22;
            r15_26->f4 = v28;
            r15_26->f8 = v29;
            r15_26->fc = v30;
            r15_26->f10 = v31;
            r15_26->f14 = v32;
            r15_26->f18 = v33;
            r15_26->f1c = v34;
            r15_26->f20 = v35;
            r15_26->f28 = v36;
            r15_26->f30 = v37;
            goto addr_cb5e_3;
        }
    } else {
        *rsi = r12_8;
        goto addr_cb5e_3;
    }
}

void fun_2790(int64_t rdi, void** rsi, void** rdx, void** rcx);

/* cdb_free.part.0 */
void cdb_free_part_0(int64_t rdi, void** rsi, void** rdx, void** rcx) {
    fun_2790("! close_fail", "lib/chdir-long.c", 64, "cdb_free");
}

void** xstrdup(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** xasprintf(int64_t rdi, void** rsi, void** rdx);

void** default_format(signed char dil, signed char sil, int32_t edx, void** rcx, void** r8, void** r9) {
    int32_t ebx7;
    void** rax8;
    void** rax9;
    void** rax10;
    void** rax11;
    void** rax12;
    void** rax13;
    void** rax14;
    void** rax15;

    if (!dil) {
        if (!sil) {
            ebx7 = edx;
            rax8 = fun_26c0();
            rax9 = xstrdup(rax8, "  File: %N\n  Size: %-10s\tBlocks: %-10b IO Block: %-6o %F\n", 5, rcx, r8);
            if (!*reinterpret_cast<signed char*>(&ebx7)) {
            }
            rax10 = fun_26c0();
            rax11 = xasprintf("%s%s", rax9, rax10);
            fun_25b0(rax9, rax9);
            rax12 = fun_26c0();
            rax13 = xasprintf("%s%s", rax11, rax12);
            fun_25b0(rax11, rax11);
            rax14 = fun_26c0();
            rax15 = xasprintf("%s%s", rax13, rax14);
            fun_25b0(rax13, rax13);
            return rax15;
        }
    } else {
        if (!sil) {
            fun_26c0();
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0x180a0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s10 {
    signed char f0;
    void** f1;
};

void** format_code_offset(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, ...) {
    void* rax7;
    void** rbx8;
    void* rax9;
    struct s10* rbx10;
    void* rax11;

    rax7 = fun_27e0(rdi + 1, "'-+ #0I", rdx, rcx, r8, r9);
    rbx8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<uint64_t>(rax7) + 1);
    rax9 = fun_27e0(rbx8, "0123456789", rdx, rcx, r8, r9);
    rbx10 = reinterpret_cast<struct s10*>(reinterpret_cast<unsigned char>(rbx8) + reinterpret_cast<uint64_t>(rax9));
    if (rbx10->f0 == 46) {
        rax11 = fun_27e0(&rbx10->f1, "0123456789", rdx, rcx, r8, r9);
        rbx10 = reinterpret_cast<struct s10*>(reinterpret_cast<uint64_t>(rbx10) + reinterpret_cast<uint64_t>(rax11) + 1);
    }
    return reinterpret_cast<uint64_t>(rbx10) - reinterpret_cast<unsigned char>(rdi);
}

struct s11 {
    signed char f0;
    void** f1;
};

void** human_time(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7) {
    void*** rsp8;
    void** rdi9;
    void** rax10;
    void** rax11;
    void** rax12;
    void** r12_13;
    void** rbx14;
    int64_t rax15;
    void** rax16;
    void** r9_17;
    void** rdx18;
    void** r8_19;
    void** rcx20;
    void* rax21;
    void* rax22;
    void** rbx23;
    void* rax24;
    struct s11* rbx25;

    rsp8 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 0x78);
    rdi9 = tz_1;
    rax10 = g28;
    if (!rdi9) {
        rax11 = fun_2590("TZ", rsi, rdx, rcx, r8, r9);
        rax12 = tzalloc(rax11, rsi);
        rsp8 = rsp8 - 8 + 8 - 8 + 8;
        tz_1 = rax12;
        rdi9 = rax12;
    }
    r12_13 = reinterpret_cast<void**>(rsp8 + 16);
    rbx14 = rsi;
    rax15 = localtime_rz(rdi9, rsp8, r12_13, rcx);
    if (!rax15) {
        rax16 = imaxtostr(rdi, rsp8 - 8 + 8 + 80, r12_13, rcx);
        *reinterpret_cast<int32_t*>(&r9_17) = *reinterpret_cast<int32_t*>(&rbx14);
        *reinterpret_cast<int32_t*>(&r9_17 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdx18) = 61;
        *reinterpret_cast<int32_t*>(&rdx18 + 4) = 0;
        r8_19 = rax16;
        rcx20 = reinterpret_cast<void**>("%s.%09d");
        fun_2a90(0x18100, 1, 61, "%s.%09d", r8_19, r9_17, rdi);
    } else {
        rcx20 = r12_13;
        *reinterpret_cast<int32_t*>(&r9_17) = *reinterpret_cast<int32_t*>(&rbx14);
        *reinterpret_cast<int32_t*>(&r9_17 + 4) = 0;
        r8_19 = tz_1;
        rdx18 = reinterpret_cast<void**>("%Y-%m-%d %H:%M:%S.%N %z");
        nstrftime();
    }
    rax21 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax10) - reinterpret_cast<unsigned char>(g28));
    if (!rax21) {
        return 0x18100;
    }
    fun_2710();
    rax22 = fun_27e0(0x18101, "'-+ #0I", rdx18, rcx20, r8_19, r9_17);
    rbx23 = reinterpret_cast<void**>(0x18100 + reinterpret_cast<uint64_t>(rax22) + 1);
    rax24 = fun_27e0(rbx23, "0123456789", rdx18, rcx20, r8_19, r9_17);
    rbx25 = reinterpret_cast<struct s11*>(reinterpret_cast<unsigned char>(rbx23) + reinterpret_cast<uint64_t>(rax24));
    if (rbx25->f0 == 46) 
        goto addr_34e6_10;
    addr_34f7_11:
    goto rdi;
    addr_34e6_10:
    fun_27e0(&rbx25->f1, "0123456789", rdx18, rcx20, r8_19, r9_17);
    goto addr_34f7_11;
}

struct s12 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s12* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s12* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0x12f29);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0x12f24);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0x12f2d);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0x12f20);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t g17d18 = 0;

void fun_2033() {
    __asm__("cli ");
    goto g17d18;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2403() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2413() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2423() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2433() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2443() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2453() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2463() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2473() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2483() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2493() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2503() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2513() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2523() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2533() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2543() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t localtime_r = 0;

void fun_2553() {
    __asm__("cli ");
    goto localtime_r;
}

int64_t gmtime_r = 0;

void fun_2563() {
    __asm__("cli ");
    goto gmtime_r;
}

int64_t __cxa_finalize = 0;

void fun_2573() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __ctype_toupper_loc = 0x2030;

void fun_2583() {
    __asm__("cli ");
    goto __ctype_toupper_loc;
}

int64_t getenv = 0x2040;

void fun_2593() {
    __asm__("cli ");
    goto getenv;
}

int64_t __snprintf_chk = 0x2050;

void fun_25a3() {
    __asm__("cli ");
    goto __snprintf_chk;
}

int64_t free = 0x2060;

void fun_25b3() {
    __asm__("cli ");
    goto free;
}

int64_t endmntent = 0x2070;

void fun_25c3() {
    __asm__("cli ");
    goto endmntent;
}

int64_t abort = 0x2080;

void fun_25d3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2090;

void fun_25e3() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x20a0;

void fun_25f3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x20b0;

void fun_2603() {
    __asm__("cli ");
    goto _exit;
}

int64_t strcpy = 0x20c0;

void fun_2613() {
    __asm__("cli ");
    goto strcpy;
}

int64_t __fpending = 0x20d0;

void fun_2623() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x20e0;

void fun_2633() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t localeconv = 0x20f0;

void fun_2643() {
    __asm__("cli ");
    goto localeconv;
}

int64_t readlink = 0x2100;

void fun_2653() {
    __asm__("cli ");
    goto readlink;
}

int64_t fcntl = 0x2110;

void fun_2663() {
    __asm__("cli ");
    goto fcntl;
}

int64_t setenv = 0x2120;

void fun_2673() {
    __asm__("cli ");
    goto setenv;
}

int64_t textdomain = 0x2130;

void fun_2683() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x2140;

void fun_2693() {
    __asm__("cli ");
    goto fclose;
}

int64_t getpwuid = 0x2150;

void fun_26a3() {
    __asm__("cli ");
    goto getpwuid;
}

int64_t bindtextdomain = 0x2160;

void fun_26b3() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2170;

void fun_26c3() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2180;

void fun_26d3() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2190;

void fun_26e3() {
    __asm__("cli ");
    goto strlen;
}

int64_t openat = 0x21a0;

void fun_26f3() {
    __asm__("cli ");
    goto openat;
}

int64_t chdir = 0x21b0;

void fun_2703() {
    __asm__("cli ");
    goto chdir;
}

int64_t __stack_chk_fail = 0x21c0;

void fun_2713() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x21d0;

void fun_2723() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x21e0;

void fun_2733() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x21f0;

void fun_2743() {
    __asm__("cli ");
    goto strchr;
}

int64_t getgrgid = 0x2200;

void fun_2753() {
    __asm__("cli ");
    goto getgrgid;
}

int64_t __overflow = 0x2210;

void fun_2763() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x2220;

void fun_2773() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2230;

void fun_2783() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x2240;

void fun_2793() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x2250;

void fun_27a3() {
    __asm__("cli ");
    goto memset;
}

int64_t getcwd = 0x2260;

void fun_27b3() {
    __asm__("cli ");
    goto getcwd;
}

int64_t canonicalize_file_name = 0x2270;

void fun_27c3() {
    __asm__("cli ");
    goto canonicalize_file_name;
}

int64_t close = 0x2280;

void fun_27d3() {
    __asm__("cli ");
    goto close;
}

int64_t strspn = 0x2290;

void fun_27e3() {
    __asm__("cli ");
    goto strspn;
}

int64_t memchr = 0x22a0;

void fun_27f3() {
    __asm__("cli ");
    goto memchr;
}

int64_t memcmp = 0x22b0;

void fun_2803() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x22c0;

void fun_2813() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x22d0;

void fun_2823() {
    __asm__("cli ");
    goto calloc;
}

int64_t __getdelim = 0x22e0;

void fun_2833() {
    __asm__("cli ");
    goto __getdelim;
}

int64_t strcmp = 0x22f0;

void fun_2843() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2300;

void fun_2853() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t stat = 0x2310;

void fun_2863() {
    __asm__("cli ");
    goto stat;
}

int64_t strtol = 0x2320;

void fun_2873() {
    __asm__("cli ");
    goto strtol;
}

int64_t memcpy = 0x2330;

void fun_2883() {
    __asm__("cli ");
    goto memcpy;
}

int64_t tzset = 0x2340;

void fun_2893() {
    __asm__("cli ");
    goto tzset;
}

int64_t fileno = 0x2350;

void fun_28a3() {
    __asm__("cli ");
    goto fileno;
}

int64_t statfs = 0x2360;

void fun_28b3() {
    __asm__("cli ");
    goto statfs;
}

int64_t malloc = 0x2370;

void fun_28c3() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2380;

void fun_28d3() {
    __asm__("cli ");
    goto fflush;
}

int64_t getmntent = 0x2390;

void fun_28e3() {
    __asm__("cli ");
    goto getmntent;
}

int64_t setmntent = 0x23a0;

void fun_28f3() {
    __asm__("cli ");
    goto setmntent;
}

int64_t nl_langinfo = 0x23b0;

void fun_2903() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __isoc99_sscanf = 0x23c0;

void fun_2913() {
    __asm__("cli ");
    goto __isoc99_sscanf;
}

int64_t __freading = 0x23d0;

void fun_2923() {
    __asm__("cli ");
    goto __freading;
}

int64_t fchdir = 0x23e0;

void fun_2933() {
    __asm__("cli ");
    goto fchdir;
}

int64_t realloc = 0x23f0;

void fun_2943() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2400;

void fun_2953() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2410;

void fun_2963() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t statx = 0x2420;

void fun_2973() {
    __asm__("cli ");
    goto statx;
}

int64_t strftime = 0x2430;

void fun_2983() {
    __asm__("cli ");
    goto strftime;
}

int64_t error = 0x2440;

void fun_2993() {
    __asm__("cli ");
    goto error;
}

int64_t memrchr = 0x2450;

void fun_29a3() {
    __asm__("cli ");
    goto memrchr;
}

int64_t open = 0x2460;

void fun_29b3() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x2470;

void fun_29c3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fopen = 0x2480;

void fun_29d3() {
    __asm__("cli ");
    goto fopen;
}

int64_t unsetenv = 0x2490;

void fun_29e3() {
    __asm__("cli ");
    goto unsetenv;
}

int64_t __cxa_atexit = 0x24a0;

void fun_29f3() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x24b0;

void fun_2a03() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x24c0;

void fun_2a13() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x24d0;

void fun_2a23() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x24e0;

void fun_2a33() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x24f0;

void fun_2a43() {
    __asm__("cli ");
    goto iswprint;
}

int64_t hasmntopt = 0x2500;

void fun_2a53() {
    __asm__("cli ");
    goto hasmntopt;
}

int64_t strstr = 0x2510;

void fun_2a63() {
    __asm__("cli ");
    goto strstr;
}

int64_t __ctype_tolower_loc = 0x2520;

void fun_2a73() {
    __asm__("cli ");
    goto __ctype_tolower_loc;
}

int64_t __ctype_b_loc = 0x2530;

void fun_2a83() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

int64_t __sprintf_chk = 0x2540;

void fun_2a93() {
    __asm__("cli ");
    goto __sprintf_chk;
}

void set_program_name(void** rdi);

void** fun_2950(int64_t rdi, ...);

void fun_26b0(int64_t rdi, int64_t rsi);

void fun_2680(int64_t rdi, int64_t rsi);

void*** fun_2640(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int64_t fun_2720(int64_t rdi, void*** rsi, void** rdx, void** rcx);

int32_t optind = 0;

void** optarg = reinterpret_cast<void**>(0);

void** argmatch_die = reinterpret_cast<void**>(32);

int64_t __xargmatch_internal(int64_t rdi, void** rsi, int64_t rdx, int64_t rcx, int64_t r8, void** r9);

void** Version = reinterpret_cast<void**>(0x6e);

void version_etc(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8);

void fun_2a00();

int64_t usage();

struct s13 {
    signed char[3] pad3;
    void** f3;
};

struct s13* fun_2a60(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void set_quoting_style();

int64_t argmatch(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t fun_28b0(void** rdi);

void fun_2af3(int32_t edi, void*** rsi) {
    void** rbp3;
    void*** rbx4;
    void** rdi5;
    void*** rax6;
    void** rdi7;
    int32_t r12d8;
    void** rax9;
    void* rsp10;
    unsigned char v11;
    void** v12;
    void** r8_13;
    void** rcx14;
    void** rdx15;
    int64_t rdi16;
    int64_t rax17;
    int1_t zf18;
    void** rax19;
    void** rax20;
    uint1_t zf21;
    void** r9_22;
    void** rsi23;
    int64_t rax24;
    void** rdi25;
    uint32_t r13d26;
    uint32_t r14d27;
    uint32_t edi28;
    uint32_t esi29;
    void** rax30;
    uint32_t esi31;
    uint32_t edi32;
    void** rax33;
    void* rsp34;
    void** v35;
    struct s13* rax36;
    void** rax37;
    void* rsp38;
    int64_t rax39;
    void* rsp40;
    void** rax41;
    void** rax42;
    int64_t v43;
    int64_t r14_44;
    void** r15_45;
    void* rsp46;
    uint32_t eax47;
    void* rsp48;
    void** v49;
    void** rax50;
    void** rax51;
    int32_t eax52;
    void** v53;
    void** rax54;
    void** rax55;
    void** v56;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rbp3) = edi;
    *reinterpret_cast<int32_t*>(&rbp3 + 4) = 0;
    rbx4 = rsi;
    rdi5 = *rsi;
    set_program_name(rdi5);
    fun_2950(6, 6);
    fun_26b0("coreutils", "/usr/local/share/locale");
    fun_2680("coreutils", "/usr/local/share/locale");
    rax6 = fun_2640("coreutils", "/usr/local/share/locale");
    rdi7 = *rax6;
    if (!*reinterpret_cast<void***>(rdi7)) {
        rdi7 = reinterpret_cast<void**>(".");
    }
    r12d8 = 0;
    decimal_point = rdi7;
    rax9 = fun_26e0(rdi7, rdi7);
    decimal_point_len = rax9;
    atexit(0x66b0, "/usr/local/share/locale");
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xa8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    v11 = 0;
    v12 = reinterpret_cast<void**>(0);
    while (1) {
        *reinterpret_cast<int32_t*>(&r8_13) = 0;
        *reinterpret_cast<int32_t*>(&r8_13 + 4) = 0;
        rcx14 = reinterpret_cast<void**>(0x17980);
        rdx15 = reinterpret_cast<void**>("c:fLt");
        *reinterpret_cast<int32_t*>(&rdi16) = *reinterpret_cast<int32_t*>(&rbp3);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
        rax17 = fun_2720(rdi16, rbx4, "c:fLt", 0x17980);
        rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
        if (*reinterpret_cast<int32_t*>(&rax17) == -1) {
            addr_2d51_5:
            zf18 = optind == *reinterpret_cast<int32_t*>(&rbp3);
            if (!zf18) 
                break;
        } else {
            if (*reinterpret_cast<int32_t*>(&rax17) == 99) {
                rax19 = optarg;
                interpret_backslash_escapes = 0;
                v12 = rax19;
                trailing_delim = reinterpret_cast<void**>("\n");
                continue;
            }
            if (*reinterpret_cast<int32_t*>(&rax17) > 99) 
                goto addr_2c00_9; else 
                goto addr_2bd7_10;
        }
        fun_26c0();
        fun_2990();
        rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8);
        goto addr_2c83_12;
        addr_2c00_9:
        if (*reinterpret_cast<int32_t*>(&rax17) == 0x74) {
            v11 = 1;
            continue;
        } else {
            if (*reinterpret_cast<int32_t*>(&rax17) != 0x80) {
                if (*reinterpret_cast<int32_t*>(&rax17) == 0x66) {
                    r12d8 = 1;
                    continue;
                }
            } else {
                rax20 = optarg;
                interpret_backslash_escapes = 1;
                trailing_delim = reinterpret_cast<void**>(0x13341);
                v12 = rax20;
                continue;
            }
        }
        addr_2bd7_10:
        zf21 = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax17) == 0);
        if (zf21) {
            addr_2c8d_18:
            r9_22 = argmatch_die;
            rsi23 = optarg;
            rax24 = __xargmatch_internal("--cached", rsi23, 0x17aa0, 0x12a18, 4, r9_22);
            rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 - 8 - 8 + 8 + 8 + 8);
            if (*reinterpret_cast<int32_t*>(0x12a18 + rax24 * 4) == 1) {
                force_sync = 1;
                dont_sync = 0;
                continue;
            } else {
                if (*reinterpret_cast<int32_t*>(0x12a18 + rax24 * 4) == 2) {
                    force_sync = 0;
                    dont_sync = 1;
                    continue;
                } else {
                    if (*reinterpret_cast<int32_t*>(0x12a18 + rax24 * 4)) 
                        continue;
                    force_sync = 0;
                    dont_sync = 0;
                    continue;
                }
            }
        } else {
            if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax17) < 0) | zf21)) {
                if (*reinterpret_cast<int32_t*>(&rax17) == 76) {
                    follow_links = 1;
                    continue;
                }
            }
            if (*reinterpret_cast<int32_t*>(&rax17) != 0xffffff7d) 
                goto addr_2bec_28;
        }
        rdi25 = stdout;
        rcx14 = Version;
        *reinterpret_cast<int32_t*>(&r9_22) = 0;
        *reinterpret_cast<int32_t*>(&r9_22 + 4) = 0;
        r8_13 = reinterpret_cast<void**>("Michael Meskes");
        rdx15 = reinterpret_cast<void**>("GNU coreutils");
        version_etc(rdi25, "stat", "GNU coreutils", rcx14, "Michael Meskes");
        fun_2a00();
        rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8);
        goto addr_2d51_5;
        addr_2bec_28:
        if (*reinterpret_cast<int32_t*>(&rax17) != 0xffffff7e) {
            addr_2c83_12:
            usage();
            rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
            goto addr_2c8d_18;
        } else {
            rax17 = usage();
            rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
            goto addr_2c00_9;
        }
    }
    if (!v12) {
        r13d26 = v11;
        r14d27 = *reinterpret_cast<unsigned char*>(&r12d8);
        edi28 = r14d27;
        esi29 = r13d26;
        rax30 = default_format(*reinterpret_cast<signed char*>(&edi28), *reinterpret_cast<signed char*>(&esi29), 0, rcx14, r8_13, r9_22);
        *reinterpret_cast<int32_t*>(&rdx15) = 1;
        *reinterpret_cast<int32_t*>(&rdx15 + 4) = 0;
        esi31 = r13d26;
        edi32 = r14d27;
        v12 = rax30;
        rax33 = default_format(*reinterpret_cast<signed char*>(&edi32), *reinterpret_cast<signed char*>(&esi31), 1, rcx14, r8_13, r9_22);
        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8);
        v35 = rax33;
    } else {
        rax36 = fun_2a60(v12, "%N", rdx15, rcx14, r8_13, r9_22);
        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
        v35 = v12;
        if (rax36) {
            rax37 = fun_2590("QUOTING_STYLE", "%N", rdx15, rcx14, r8_13, r9_22);
            rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
            if (!rax37) {
                set_quoting_style();
                rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp38) - 8 + 8);
            } else {
                *reinterpret_cast<int32_t*>(&rcx14) = 4;
                *reinterpret_cast<int32_t*>(&rcx14 + 4) = 0;
                rdx15 = reinterpret_cast<void**>(0x132c0);
                rax39 = argmatch(rax37, 0x17ac0, 0x132c0, 4, r8_13, r9_22);
                rsp40 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp38) - 8 + 8);
                if (*reinterpret_cast<int32_t*>(&rax39) < 0) {
                    set_quoting_style();
                    rax41 = quote(rax37, 4, 0x132c0, 4, r8_13, r9_22);
                    rax42 = fun_26c0();
                    rcx14 = rax41;
                    rdx15 = rax42;
                    fun_2990();
                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp40) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                } else {
                    set_quoting_style();
                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp40) - 8 + 8);
                }
            }
        }
    }
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v43) + 4) = *reinterpret_cast<int32_t*>(&rbp3);
    r14_44 = optind;
    r15_45 = v12;
    while (1) {
        if (*reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v43) + 4) <= *reinterpret_cast<int32_t*>(&r14_44)) {
            fun_2a00();
            rsp46 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
            goto addr_2ecb_42;
        } else {
            rbp3 = rbx4[r14_44 * 8];
            if (*reinterpret_cast<unsigned char*>(&r12d8)) {
                eax47 = fun_2840(rbp3, "-", rdx15, rbp3, "-", rdx15);
                rsp48 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                if (!eax47) {
                    rax50 = quotearg_style(4, rbp3, rdx15, rcx14, r8_13, r9_22, v49);
                    rbp3 = rax50;
                    rax51 = fun_26c0();
                    rcx14 = rbp3;
                    rdx15 = rax51;
                    fun_2990();
                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8 - 8 + 8 - 8 + 8);
                } else {
                    eax52 = fun_28b0(rbp3);
                    rsp46 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp48) - 8 + 8);
                    r8_13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp48) + 32);
                    if (eax52) {
                        addr_2ecb_42:
                        rax54 = quotearg_style(4, rbp3, rdx15, rcx14, r8_13, r9_22, v53);
                        rax55 = fun_26c0();
                        rbp3 = rax55;
                        fun_25e0();
                        rcx14 = rax54;
                        rdx15 = rbp3;
                        fun_2990();
                        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp46) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                    } else {
                        rcx14 = reinterpret_cast<void**>(0x41b0);
                        rdx15 = rbp3;
                        print_it(r15_45, 0xffffffff, rdx15, 0x41b0, r8_13, r9_22, v56, v43);
                        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp46) - 8 + 8);
                    }
                }
            } else {
                rdx15 = v35;
                do_stat(rbp3, r15_45, rdx15, rcx14, r8_13, r9_22);
                rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
            }
            ++r14_44;
        }
    }
}

int64_t __libc_start_main = 0;

void fun_2fa3() {
    __asm__("cli ");
    __libc_start_main(0x2af0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0x18008;

void fun_2570(int64_t rdi);

int64_t fun_3043() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2570(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_3083() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

int64_t fun_41b3(int64_t rdi, int64_t rsi) {
    int32_t ecx3;
    int32_t ecx4;
    void** rdi5;
    void** rax6;
    int64_t rcx7;

    __asm__("cli ");
    ecx3 = ecx4 - 83;
    if (*reinterpret_cast<unsigned char*>(&ecx3) > 33) {
        rdi5 = stdout;
        rax6 = *reinterpret_cast<void***>(rdi5 + 40);
        if (reinterpret_cast<unsigned char>(rax6) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi5 + 48))) {
            fun_2760();
        } else {
            *reinterpret_cast<void***>(rdi5 + 40) = rax6 + 1;
            *reinterpret_cast<void***>(rax6) = reinterpret_cast<void**>(63);
        }
        return 0;
    } else {
        *reinterpret_cast<uint32_t*>(&rcx7) = *reinterpret_cast<unsigned char*>(&ecx3);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx7) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x1282c + rcx7 * 4) + 0x1282c;
    }
}

int64_t fun_5003(int64_t rdi, int64_t rsi) {
    int32_t ecx3;
    int32_t ecx4;
    void** rdi5;
    void** rax6;
    int64_t rcx7;

    __asm__("cli ");
    ecx3 = ecx4 - 65;
    if (*reinterpret_cast<unsigned char*>(&ecx3) > 57) {
        rdi5 = stdout;
        rax6 = *reinterpret_cast<void***>(rdi5 + 40);
        if (reinterpret_cast<unsigned char>(rax6) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi5 + 48))) {
            fun_2760();
        } else {
            *reinterpret_cast<void***>(rdi5 + 40) = rax6 + 1;
            *reinterpret_cast<void***>(rax6) = reinterpret_cast<void**>(63);
        }
        return 0;
    } else {
        *reinterpret_cast<uint32_t*>(&rcx7) = *reinterpret_cast<unsigned char*>(&ecx3);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx7) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x128b4 + rcx7 * 4) + 0x128b4;
    }
}

void** program_name = reinterpret_cast<void**>(0);

int32_t fun_25f0(void** rdi, void** rsi, void** rdx, ...);

void** stderr = reinterpret_cast<void**>(0);

void fun_2a20(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, void*** a8, int64_t a9, void** a10, int64_t a11, void*** a12);

void fun_5743(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** rcx6;
    void** r12_7;
    void** rax8;
    void** r8_9;
    void** r9_10;
    void** r12_11;
    void** rax12;
    void** r8_13;
    void** r9_14;
    void** r12_15;
    void** rax16;
    void** r8_17;
    void** r9_18;
    void** r12_19;
    void** rax20;
    void** r8_21;
    void** r9_22;
    void** r12_23;
    void** rax24;
    void** r8_25;
    void** r9_26;
    void** r12_27;
    void** rax28;
    void** r8_29;
    void** r9_30;
    void** r12_31;
    void** rax32;
    void** r8_33;
    void** r9_34;
    void** r12_35;
    void** rax36;
    void** r8_37;
    void** r9_38;
    void** r12_39;
    void** rax40;
    void** r8_41;
    void** r9_42;
    void** r12_43;
    void** rax44;
    void** r8_45;
    void** r9_46;
    void** r12_47;
    void** rax48;
    void** r8_49;
    void** r9_50;
    void** r12_51;
    void** rax52;
    void** r8_53;
    void** r9_54;
    void** r12_55;
    void** rax56;
    void** r8_57;
    void** r9_58;
    void** r12_59;
    void** rax60;
    void** r8_61;
    void** r9_62;
    void** rax63;
    void** rax64;
    void** rax65;
    uint32_t eax66;
    void** r13_67;
    void** rax68;
    void** rax69;
    int32_t eax70;
    void** rax71;
    void** rax72;
    void** rax73;
    int32_t eax74;
    void** rax75;
    void** r15_76;
    void** rax77;
    void** r8_78;
    void** r9_79;
    void** rax80;
    void** rax81;
    void** rdi82;
    void** r8_83;
    void** r9_84;
    int64_t v85;
    void*** v86;
    int64_t v87;
    void** v88;
    int64_t v89;
    void*** v90;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_26c0();
            fun_2960(1, rax5, r12_2, rcx6, 1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_26c0();
            fun_2810(rax8, r12_7, 5, rcx6, r8_9, r9_10, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            r12_11 = stdout;
            rax12 = fun_26c0();
            fun_2810(rax12, r12_11, 5, rcx6, r8_13, r9_14, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            r12_15 = stdout;
            rax16 = fun_26c0();
            fun_2810(rax16, r12_15, 5, rcx6, r8_17, r9_18, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            r12_19 = stdout;
            rax20 = fun_26c0();
            fun_2810(rax20, r12_19, 5, rcx6, r8_21, r9_22, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            r12_23 = stdout;
            rax24 = fun_26c0();
            fun_2810(rax24, r12_23, 5, rcx6, r8_25, r9_26, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            r12_27 = stdout;
            rax28 = fun_26c0();
            fun_2810(rax28, r12_27, 5, rcx6, r8_29, r9_30, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            r12_31 = stdout;
            rax32 = fun_26c0();
            fun_2810(rax32, r12_31, 5, rcx6, r8_33, r9_34, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            r12_35 = stdout;
            rax36 = fun_26c0();
            fun_2810(rax36, r12_35, 5, rcx6, r8_37, r9_38, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            r12_39 = stdout;
            rax40 = fun_26c0();
            fun_2810(rax40, r12_39, 5, rcx6, r8_41, r9_42, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            r12_43 = stdout;
            rax44 = fun_26c0();
            fun_2810(rax44, r12_43, 5, rcx6, r8_45, r9_46, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            r12_47 = stdout;
            rax48 = fun_26c0();
            fun_2810(rax48, r12_47, 5, rcx6, r8_49, r9_50, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            r12_51 = stdout;
            rax52 = fun_26c0();
            fun_2810(rax52, r12_51, 5, rcx6, r8_53, r9_54, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            r12_55 = stdout;
            rax56 = fun_26c0();
            fun_2810(rax56, r12_55, 5, rcx6, r8_57, r9_58, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            r12_59 = stdout;
            rax60 = fun_26c0();
            fun_2810(rax60, r12_59, 5, rcx6, r8_61, r9_62, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            rax63 = fun_26c0();
            fun_2960(1, rax63, "%n %s %b %f %u %g %D %i %h %t %T %X %Y %Z %W %o\n", rcx6, 1, rax63, "%n %s %b %f %u %g %D %i %h %t %T %X %Y %Z %W %o\n", rcx6);
            rax64 = fun_26c0();
            fun_2960(1, rax64, "%n %i %l %t %s %S %b %f %a %c %d\n", rcx6, 1, rax64, "%n %i %l %t %s %S %b %f %a %c %d\n", rcx6);
            rax65 = fun_26c0();
            fun_2960(1, rax65, "stat", rcx6, 1, rax65, "stat", rcx6);
            do {
                if (1) 
                    break;
                eax66 = fun_2840("stat", 0, "stat", "stat", 0, "stat");
            } while (eax66);
            r13_67 = v4;
            if (!r13_67) {
                rax68 = fun_26c0();
                fun_2960(1, rax68, "GNU coreutils", "https://www.gnu.org/software/coreutils/", 1, rax68, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax69 = fun_2950(5);
                if (!rax69 || (eax70 = fun_25f0(rax69, "en_", 3, rax69, "en_", 3), !eax70)) {
                    rax71 = fun_26c0();
                    r13_67 = reinterpret_cast<void**>("stat");
                    fun_2960(1, rax71, "https://www.gnu.org/software/coreutils/", "stat", 1, rax71, "https://www.gnu.org/software/coreutils/", "stat");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_67 = reinterpret_cast<void**>("stat");
                    goto addr_5c60_9;
                }
            } else {
                rax72 = fun_26c0();
                fun_2960(1, rax72, "GNU coreutils", "https://www.gnu.org/software/coreutils/", 1, rax72, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax73 = fun_2950(5);
                if (!rax73 || (eax74 = fun_25f0(rax73, "en_", 3, rax73, "en_", 3), !eax74)) {
                    addr_5b66_11:
                    rax75 = fun_26c0();
                    fun_2960(1, rax75, "https://www.gnu.org/software/coreutils/", "stat", 1, rax75, "https://www.gnu.org/software/coreutils/", "stat");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_67 == "stat")) {
                        r12_2 = reinterpret_cast<void**>(0x13341);
                    }
                } else {
                    addr_5c60_9:
                    r15_76 = stdout;
                    rax77 = fun_26c0();
                    fun_2810(rax77, r15_76, 5, "https://www.gnu.org/software/coreutils/", r8_78, r9_79, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                    goto addr_5b66_11;
                }
            }
            rax80 = fun_26c0();
            rcx6 = r12_2;
            fun_2960(1, rax80, r13_67, rcx6, 1, rax80, r13_67, rcx6);
            addr_579e_14:
            fun_2a00();
        }
    } else {
        rax81 = fun_26c0();
        rdi82 = stderr;
        rcx6 = r12_2;
        fun_2a20(rdi82, 1, rax81, rcx6, r8_83, r9_84, v85, v86, v87, v88, v89, v90);
        goto addr_579e_14;
    }
}

int32_t save_cwd(void* rdi);

int32_t fun_2700(void** rdi, void** rsi, void** rdx);

void** dir_name(void** rdi);

struct s14 {
    int64_t f0;
    void** f8;
};

struct s15 {
    int64_t f0;
    void** f8;
};

int32_t restore_cwd(void* rdi, void** rsi, void** rdx, void** rcx);

void free_cwd(void* rdi, void** rsi, void** rdx, void** rcx);

int64_t xgetcwd(void** rdi, void** rsi, void** rdx);

int64_t fun_5c93(void** rdi, void** rsi, void** rdx) {
    void* rsp4;
    void* rbp5;
    void* r14_6;
    void** rax7;
    void** v8;
    int32_t eax9;
    void** rax10;
    void* rsp11;
    void** r13_12;
    int64_t r12_13;
    void*** rsp14;
    int32_t eax15;
    void*** rsp16;
    void** rsi17;
    void** rbx18;
    void** rax19;
    void** r12_20;
    void** rax21;
    void** rsp22;
    void** r8_23;
    void*** rax24;
    void** rcx25;
    uint64_t rdx26;
    void* rdx27;
    void* rsp28;
    int64_t* rsp29;
    void** rax30;
    int64_t* rsp31;
    struct s14* rsp32;
    int32_t eax33;
    void* rax34;
    struct s15* rsp35;
    void*** rsp36;
    struct s15* rsp37;
    void** r9_38;
    struct s15* rsp39;
    struct s15* rsp40;
    void** rsi41;
    struct s15* rsp42;
    int32_t eax43;
    void*** rsp44;
    int64_t v45;
    int64_t v46;
    int64_t v47;
    int64_t v48;
    struct s15* rsp49;
    int32_t eax50;
    struct s15* rsp51;
    void** r9_52;
    void** rax53;
    void*** rsp54;
    void** r12_55;
    struct s15* rsp56;
    void** rax57;
    struct s15* rsp58;
    void*** rsp59;
    void** ebx60;
    struct s15* rsp61;
    int32_t eax62;
    struct s15* rsp63;
    struct s15* rsp64;
    struct s15* rsp65;
    int64_t rax66;
    struct s15* rsp67;
    void** r9_68;
    void** rax69;
    struct s15* rsp70;
    int32_t eax71;
    struct s15* rsp72;
    void** r9_73;
    void** rax74;
    struct s15* rsp75;
    void** rax76;
    struct s15* rsp77;

    __asm__("cli ");
    rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp5 = rsp4;
    r14_6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp5) - 0x170);
    rax7 = g28;
    v8 = rax7;
    eax9 = save_cwd(r14_6);
    rax10 = fun_25e0();
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 - 8 - 8 - 8 - 8 - 0x148 - 8 + 8 - 8 + 8);
    r13_12 = rax10;
    if (eax9) {
        *reinterpret_cast<int32_t*>(&r12_13) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_13) + 4) = 0;
        fun_26c0();
        fun_2990();
        rsp14 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8);
    } else {
        if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 24)) & 0xf000) == 0x4000) {
            __asm__("movdqu xmm1, [rbx]");
            __asm__("movdqu xmm2, [rbx+0x10]");
            __asm__("movdqu xmm3, [rbx+0x20]");
            __asm__("movdqu xmm4, [rbx+0x30]");
            __asm__("movdqu xmm5, [rbx+0x40]");
            __asm__("movdqu xmm6, [rbx+0x50]");
            __asm__("movaps [rbp-0x160], xmm1");
            __asm__("movdqu xmm7, [rbx+0x60]");
            __asm__("movdqu xmm1, [rbx+0x70]");
            __asm__("movaps [rbp-0x150], xmm2");
            __asm__("movdqu xmm2, [rbx+0x80]");
            __asm__("movaps [rbp-0x140], xmm3");
            __asm__("movaps [rbp-0x130], xmm4");
            __asm__("movaps [rbp-0x120], xmm5");
            __asm__("movaps [rbp-0x110], xmm6");
            __asm__("movaps [rbp-0x100], xmm7");
            __asm__("movaps [rbp-0xf0], xmm1");
            __asm__("movaps [rbp-0xe0], xmm2");
            eax15 = fun_2700(rdi, rsi, rdx);
            rsp16 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
            if (eax15 < 0) {
                rsi17 = rdi;
                goto addr_6023_6;
            } else {
                addr_5e5e_7:
                rbx18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp5) + 0xffffffffffffff30);
                goto addr_5f2b_8;
            }
        } else {
            rax19 = dir_name(rdi);
            r12_20 = rax19;
            rax21 = fun_26e0(rax19);
            rsp22 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8);
            r8_23 = rax21 + 1;
            rax24 = reinterpret_cast<void***>(rax21 + 24);
            rcx25 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsp22) - (reinterpret_cast<uint64_t>(rax24) & 0xfffffffffffff000));
            rdx26 = reinterpret_cast<uint64_t>(rax24) & 0xfffffffffffffff0;
            if (rsp22 != rcx25) {
                do {
                    rsp22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsp22) - reinterpret_cast<uint64_t>("hr"));
                } while (rsp22 != rcx25);
            }
            *reinterpret_cast<uint32_t*>(&rdx27) = *reinterpret_cast<uint32_t*>(&rdx26) & reinterpret_cast<uint32_t>("chr");
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx27) + 4) = 0;
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rsp22) - reinterpret_cast<uint64_t>(rdx27));
            if (rdx27) {
                *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp28) + reinterpret_cast<uint64_t>(rdx27) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp28) + reinterpret_cast<uint64_t>(rdx27) - 8);
            }
            rdx = r8_23;
            rsp29 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp28) - 8);
            *rsp29 = 0x5d63;
            rax30 = fun_2880(reinterpret_cast<uint64_t>(rsp28) + 15 & 0xfffffffffffffff0, r12_20, rdx);
            rsp31 = rsp29 + 1 - 1;
            *rsp31 = 0x5d6e;
            fun_25b0(r12_20, r12_20);
            rsp32 = reinterpret_cast<struct s14*>(rsp31 + 1 - 1);
            rsp32->f0 = 0x5d76;
            eax33 = fun_2700(rax30, r12_20, rdx);
            rsp16 = &rsp32->f8;
            if (eax33 < 0) 
                goto addr_6020_14; else 
                goto addr_5d7e_15;
        }
    }
    addr_5fa8_16:
    rax34 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
    if (rax34) {
        rsp35 = reinterpret_cast<struct s15*>(rsp14 - 8);
        rsp35->f0 = 0x6068;
        fun_2710();
        rsp36 = &rsp35->f8;
        goto addr_6068_18;
    } else {
        return r12_13;
    }
    addr_6023_6:
    rsp37 = reinterpret_cast<struct s15*>(rsp16 - 8);
    rsp37->f0 = 0x602d;
    quotearg_style(4, rsi17, rdx, rcx25, r8_23, r9_38, rsp37->f8);
    rsp39 = reinterpret_cast<struct s15*>(&rsp37->f8 - 8);
    rsp39->f0 = 0x6043;
    fun_26c0();
    *reinterpret_cast<int32_t*>(&r12_13) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_13) + 4) = 0;
    rsp40 = reinterpret_cast<struct s15*>(&rsp39->f8 - 8);
    rsp40->f0 = 0x6059;
    fun_2990();
    rsp14 = &rsp40->f8;
    goto addr_5fa8_16;
    addr_5f2b_8:
    while (rsi41 = rbx18, rsp42 = reinterpret_cast<struct s15*>(rsp16 - 8), rsp42->f0 = 0x5f36, eax43 = fun_2860("..", rsi41, rdx, rcx25), rsp44 = &rsp42->f8, eax43 >= 0) {
        if (v45 != v46) 
            goto addr_5f80_21;
        if (v47 == v48) 
            goto addr_5f80_21;
        rsp49 = reinterpret_cast<struct s15*>(rsp44 - 8);
        rsp49->f0 = 0x5ea8;
        eax50 = fun_2700("..", rsi41, rdx);
        rsp16 = &rsp49->f8;
        if (eax50 < 0) 
            goto addr_5fd0_24;
        __asm__("movdqa xmm0, [rbp-0xd0]");
        __asm__("movdqa xmm1, [rbp-0xc0]");
        __asm__("movdqa xmm2, [rbp-0xb0]");
        __asm__("movdqa xmm3, [rbp-0xa0]");
        __asm__("movdqa xmm4, [rbp-0x90]");
        __asm__("movdqa xmm5, [rbp-0x80]");
        __asm__("movaps [rbp-0x160], xmm0");
        __asm__("movdqa xmm6, [rbp-0x70]");
        __asm__("movdqa xmm7, [rbp-0x60]");
        __asm__("movaps [rbp-0x150], xmm1");
        __asm__("movdqa xmm0, [rbp-0x50]");
        __asm__("movaps [rbp-0x140], xmm2");
        __asm__("movaps [rbp-0x130], xmm3");
        __asm__("movaps [rbp-0x120], xmm4");
        __asm__("movaps [rbp-0x110], xmm5");
        __asm__("movaps [rbp-0x100], xmm6");
        __asm__("movaps [rbp-0xf0], xmm7");
        __asm__("movaps [rbp-0xe0], xmm0");
    }
    rsp51 = reinterpret_cast<struct s15*>(rsp44 - 8);
    rsp51->f0 = 0x5f4b;
    rax53 = quotearg_style(4, "..", rdx, rcx25, r8_23, r9_52, rsp51->f8);
    rsp54 = &rsp51->f8;
    r12_55 = rax53;
    addr_5f5a_27:
    rsp56 = reinterpret_cast<struct s15*>(rsp54 - 8);
    rsp56->f0 = 0x5f61;
    rax57 = fun_26c0();
    rsi41 = *reinterpret_cast<void***>(r13_12);
    *reinterpret_cast<int32_t*>(&rsi41 + 4) = 0;
    rcx25 = r12_55;
    rdx = rax57;
    *reinterpret_cast<int32_t*>(&r12_13) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_13) + 4) = 0;
    rsp58 = reinterpret_cast<struct s15*>(&rsp56->f8 - 8);
    rsp58->f0 = 0x5f77;
    fun_2990();
    rsp59 = &rsp58->f8;
    addr_5f88_28:
    ebx60 = *reinterpret_cast<void***>(r13_12);
    rsp61 = reinterpret_cast<struct s15*>(rsp59 - 8);
    rsp61->f0 = 0x5f94;
    eax62 = restore_cwd(r14_6, rsi41, rdx, rcx25);
    rsp36 = &rsp61->f8;
    if (eax62) {
        addr_6068_18:
        rsp63 = reinterpret_cast<struct s15*>(rsp36 - 8);
        rsp63->f0 = 0x607b;
        fun_26c0();
        *reinterpret_cast<int64_t*>(&rsp63->f8 - 8) = 0x608e;
        fun_2990();
    } else {
        rsp64 = reinterpret_cast<struct s15*>(rsp36 - 8);
        rsp64->f0 = 0x5fa4;
        free_cwd(r14_6, rsi41, rdx, rcx25);
        rsp14 = &rsp64->f8;
        *reinterpret_cast<void***>(r13_12) = ebx60;
        goto addr_5fa8_16;
    }
    addr_5f80_21:
    rsp65 = reinterpret_cast<struct s15*>(rsp44 - 8);
    rsp65->f0 = 0x5f85;
    rax66 = xgetcwd("..", rsi41, rdx);
    rsp59 = &rsp65->f8;
    r12_13 = rax66;
    goto addr_5f88_28;
    addr_5fd0_24:
    rsp67 = reinterpret_cast<struct s15*>(rsp16 - 8);
    rsp67->f0 = 0x5fdd;
    rax69 = quotearg_style(4, "..", rdx, rcx25, r8_23, r9_68, rsp67->f8);
    rsp54 = &rsp67->f8;
    r12_55 = rax69;
    goto addr_5f5a_27;
    addr_6020_14:
    rsi17 = rax30;
    goto addr_6023_6;
    addr_5d7e_15:
    rsp70 = reinterpret_cast<struct s15*>(rsp16 - 8);
    rsp70->f0 = 0x5d91;
    eax71 = fun_2860(".", reinterpret_cast<int64_t>(rbp5) + 0xfffffffffffffea0, rdx, rcx25);
    rsp16 = &rsp70->f8;
    if (eax71 >= 0) 
        goto addr_5e5e_7;
    rsp72 = reinterpret_cast<struct s15*>(rsp16 - 8);
    rsp72->f0 = 0x5da6;
    rax74 = quotearg_style(4, rax30, rdx, rcx25, r8_23, r9_73, rsp72->f8);
    rsp75 = reinterpret_cast<struct s15*>(&rsp72->f8 - 8);
    rsp75->f0 = 0x5dbc;
    rax76 = fun_26c0();
    rsi41 = *reinterpret_cast<void***>(r13_12);
    *reinterpret_cast<int32_t*>(&rsi41 + 4) = 0;
    rcx25 = rax74;
    rdx = rax76;
    *reinterpret_cast<int32_t*>(&r12_13) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_13) + 4) = 0;
    rsp77 = reinterpret_cast<struct s15*>(&rsp75->f8 - 8);
    rsp77->f0 = 0x5dd2;
    fun_2990();
    rsp59 = &rsp77->f8;
    goto addr_5f88_28;
}

void** fun_28c0(void** rdi, void** rsi, ...);

void** fun_2650(int64_t rdi, void** rsi, void** rdx);

void** fun_2940(void** rdi, void** rsi, ...);

void** fun_6093(int64_t rdi, void** rsi) {
    int64_t rbp3;
    void** rbx4;
    void** rax5;
    void** v6;
    int1_t zf7;
    unsigned char r14b8;
    void** v9;
    void** rax10;
    void** r15_11;
    void* rax12;
    void** rdi13;
    void** rax14;
    void** r13_15;
    void** rax16;
    void** r12_17;
    void** rax18;
    void** rax19;

    __asm__("cli ");
    rbp3 = rdi;
    *reinterpret_cast<int32_t*>(&rbx4) = 0x80;
    *reinterpret_cast<int32_t*>(&rbx4 + 4) = 0;
    rax5 = g28;
    v6 = rax5;
    zf7 = rsi == 0;
    r14b8 = reinterpret_cast<uint1_t>(!zf7);
    if (!zf7 && (rbx4 = rsi + 1, reinterpret_cast<unsigned char>(rsi) >= reinterpret_cast<unsigned char>(0x401))) {
        rbx4 = reinterpret_cast<void**>(0x401);
    }
    v9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xa8 + 16);
    goto addr_60f8_4;
    addr_61e0_5:
    rax10 = fun_25e0();
    *reinterpret_cast<int32_t*>(&r15_11) = 0;
    *reinterpret_cast<int32_t*>(&r15_11 + 4) = 0;
    *reinterpret_cast<void***>(rax10) = reinterpret_cast<void**>(12);
    addr_618b_6:
    rax12 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v6) - reinterpret_cast<unsigned char>(g28));
    if (rax12) {
        fun_2710();
    } else {
        return r15_11;
    }
    addr_6180_9:
    rdi13 = r15_11;
    *reinterpret_cast<int32_t*>(&r15_11) = 0;
    *reinterpret_cast<int32_t*>(&r15_11 + 4) = 0;
    fun_25b0(rdi13, rdi13);
    goto addr_618b_6;
    while (rax14 = fun_28c0(rbx4, rsi, rbx4, rsi), r13_15 = rax14, !!rax14) {
        r15_11 = rax14;
        do {
            rsi = r13_15;
            rax16 = fun_2650(rbp3, rsi, rbx4);
            if (reinterpret_cast<signed char>(rax16) < reinterpret_cast<signed char>(0)) 
                goto addr_6180_9;
            if (reinterpret_cast<unsigned char>(rbx4) > reinterpret_cast<unsigned char>(rax16)) 
                goto addr_61b8_14;
            fun_25b0(r15_11, r15_11);
            if (reinterpret_cast<unsigned char>(rbx4) > reinterpret_cast<unsigned char>(0x3fffffffffffffff)) {
                if (rbx4 == 0x7fffffffffffffff) 
                    goto addr_61e0_5;
                rbx4 = reinterpret_cast<void**>(0x7fffffffffffffff);
                addr_60f8_4:
                if (!reinterpret_cast<int1_t>(rbx4 == 0x80)) 
                    break;
            } else {
                rbx4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx4) + reinterpret_cast<unsigned char>(rbx4));
                if (rbx4 != 0x80) 
                    break;
            }
            r13_15 = v9;
            *reinterpret_cast<int32_t*>(&r15_11) = 0;
            *reinterpret_cast<int32_t*>(&r15_11 + 4) = 0;
        } while (!r14b8);
    }
    goto addr_61e0_5;
    addr_61b8_14:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r13_15) + reinterpret_cast<unsigned char>(rax16)) = 0;
    r12_17 = rax16 + 1;
    if (!r15_11) {
        rax18 = fun_28c0(r12_17, rsi, r12_17, rsi);
        r15_11 = rax18;
        if (rax18) {
            fun_2880(rax18, r13_15, r12_17);
            goto addr_618b_6;
        }
    } else {
        if (reinterpret_cast<unsigned char>(rbx4) > reinterpret_cast<unsigned char>(r12_17)) {
            rax19 = fun_2940(r15_11, r12_17, r15_11, r12_17);
            if (rax19) {
                r15_11 = rax19;
            }
            goto addr_618b_6;
        }
    }
}

void fun_6223() {
    __asm__("cli ");
    goto usage;
}

uint32_t fun_2800(void** rdi, void** rsi, void** rdx, ...);

int64_t fun_6233(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void** r14_5;
    void** r13_6;
    void** rbp7;
    void*** v8;
    void** v9;
    void** rax10;
    void** r15_11;
    int64_t v12;
    unsigned char v13;
    void** r12_14;
    int64_t rbx15;
    int64_t v16;
    int32_t eax17;
    void** rax18;
    uint32_t eax19;
    uint32_t eax20;
    int64_t rax21;

    __asm__("cli ");
    r14_5 = rdi;
    r13_6 = rcx;
    rbp7 = rdx;
    v8 = rsi;
    v9 = rdx;
    rax10 = fun_26e0(rdi);
    r15_11 = *rsi;
    if (!r15_11) {
        v12 = -1;
    } else {
        v13 = 0;
        r12_14 = rax10;
        *reinterpret_cast<int32_t*>(&rbx15) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx15) + 4) = 0;
        v16 = -1;
        while (1) {
            eax17 = fun_25f0(r15_11, r14_5, r12_14);
            if (eax17) {
                addr_62b3_5:
                ++rbx15;
                rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(r13_6));
                r15_11 = v8[rbx15 * 8];
                if (!r15_11) 
                    goto addr_6300_6; else 
                    continue;
            } else {
                rax18 = fun_26e0(r15_11, r15_11);
                if (rax18 == r12_14) 
                    goto addr_6330_8;
                if (v16 == -1) 
                    goto addr_62ee_10;
            }
            if (!v9) {
                v13 = 1;
                goto addr_62b3_5;
            } else {
                eax19 = fun_2800(reinterpret_cast<uint64_t>(v16 * reinterpret_cast<unsigned char>(r13_6)) + reinterpret_cast<unsigned char>(v9), rbp7, r13_6);
                eax20 = v13;
                if (eax19) {
                    eax20 = 1;
                }
                v13 = *reinterpret_cast<unsigned char*>(&eax20);
                goto addr_62b3_5;
            }
            addr_62ee_10:
            v16 = rbx15;
            goto addr_62b3_5;
        }
    }
    addr_6315_16:
    return v12;
    addr_6300_6:
    rax21 = -2;
    if (!v13) {
        rax21 = v16;
    }
    v12 = rax21;
    goto addr_6315_16;
    addr_6330_8:
    v12 = rbx15;
    goto addr_6315_16;
}

int64_t fun_6343(void** rdi, void*** rsi, void** rdx) {
    void** r12_4;
    void** rdi5;
    void*** rbp6;
    int64_t rbx7;
    uint32_t eax8;

    __asm__("cli ");
    r12_4 = rdi;
    rdi5 = *rsi;
    if (!rdi5) {
        addr_6388_2:
        return -1;
    } else {
        rbp6 = rsi;
        *reinterpret_cast<int32_t*>(&rbx7) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx7) + 4) = 0;
        do {
            eax8 = fun_2840(rdi5, r12_4, rdx);
            if (!eax8) 
                break;
            ++rbx7;
            rdi5 = rbp6[rbx7 * 8];
        } while (rdi5);
        goto addr_6388_2;
    }
    return rbx7;
}

int64_t quote_n(int64_t rdi, int64_t rsi, int64_t rdx);

int64_t quotearg_n_style();

void fun_63a3(int64_t rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (rdx == -1) {
        fun_26c0();
    } else {
        fun_26c0();
    }
    quote_n(1, rdi, 5);
    quotearg_n_style();
    goto fun_2990;
}

void fun_6433(void*** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, void** a9, int64_t a10) {
    void** r13_11;
    void*** v12;
    void*** r12_13;
    void** r12_14;
    void** rdx15;
    int64_t v16;
    int64_t rbp17;
    void** rbp18;
    void** v19;
    void** rbx20;
    void** r14_21;
    void*** v22;
    void** rax23;
    void** rsi24;
    void** v25;
    void** v26;
    int64_t r13_27;
    int64_t r14_28;
    int64_t r15_29;
    void** r15_30;
    int64_t rbx31;
    uint32_t eax32;
    void** rax33;
    void** rdi34;
    int64_t v35;
    int64_t v36;
    void** rax37;
    void** rdi38;
    int64_t v39;
    int64_t v40;
    void** rdi41;
    void** rax42;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r13_11) = 0;
    *reinterpret_cast<int32_t*>(&r13_11 + 4) = 0;
    v12 = r12_13;
    r12_14 = rdx;
    *reinterpret_cast<int32_t*>(&rdx15) = 5;
    *reinterpret_cast<int32_t*>(&rdx15 + 4) = 0;
    v16 = rbp17;
    rbp18 = rsi;
    v19 = rbx20;
    r14_21 = stderr;
    v22 = rdi;
    rax23 = fun_26c0();
    rsi24 = r14_21;
    fun_2810(rax23, rsi24, 5, rcx, r8, r9, v25, v22, v26, v19, v16, v12, r13_27, r14_28, r15_29, __return_address(), a7, a8, a9, a10);
    r15_30 = *rdi;
    *reinterpret_cast<int32_t*>(&rbx31) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx31) + 4) = 0;
    if (r15_30) {
        do {
            if (!rbx31 || (rdx15 = r12_14, rsi24 = rbp18, eax32 = fun_2800(r13_11, rsi24, rdx15, r13_11, rsi24, rdx15), !!eax32)) {
                r13_11 = rbp18;
                rax33 = quote(r15_30, rsi24, rdx15, rcx, r8, r9);
                rdi34 = stderr;
                rdx15 = reinterpret_cast<void**>("\n  - %s");
                *reinterpret_cast<int32_t*>(&rsi24) = 1;
                *reinterpret_cast<int32_t*>(&rsi24 + 4) = 0;
                rcx = rax33;
                fun_2a20(rdi34, 1, "\n  - %s", rcx, r8, r9, v35, v22, v36, v19, v16, v12);
            } else {
                rax37 = quote(r15_30, rsi24, rdx15, rcx, r8, r9);
                rdi38 = stderr;
                *reinterpret_cast<int32_t*>(&rsi24) = 1;
                *reinterpret_cast<int32_t*>(&rsi24 + 4) = 0;
                rdx15 = reinterpret_cast<void**>(", %s");
                rcx = rax37;
                fun_2a20(rdi38, 1, ", %s", rcx, r8, r9, v39, v22, v40, v19, v16, v12);
            }
            ++rbx31;
            rbp18 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp18) + reinterpret_cast<unsigned char>(r12_14));
            r15_30 = v22[rbx31 * 8];
        } while (r15_30);
    }
    rdi41 = stderr;
    rax42 = *reinterpret_cast<void***>(rdi41 + 40);
    if (reinterpret_cast<unsigned char>(rax42) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi41 + 48))) {
        goto fun_2760;
    } else {
        *reinterpret_cast<void***>(rdi41 + 40) = rax42 + 1;
        *reinterpret_cast<void***>(rax42) = reinterpret_cast<void**>(10);
        return;
    }
}

void argmatch_invalid(int64_t rdi, void** rsi, int64_t rdx, void** rcx);

void argmatch_valid(void** rdi, void** rsi, void** rdx, void** rcx);

int64_t fun_6563(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, signed char a7) {
    int64_t r15_8;
    void** r14_9;
    void** r13_10;
    void** r12_11;
    void** rbp12;
    void** v13;
    int64_t rax14;
    void** rdi15;
    int64_t rbx16;
    uint32_t eax17;

    __asm__("cli ");
    r15_8 = rdi;
    r14_9 = rsi;
    r13_10 = r8;
    r12_11 = rcx;
    rbp12 = rdx;
    v13 = r9;
    if (a7) {
        rcx = r8;
        rax14 = argmatch(r14_9, rbp12, r12_11, rcx, r8, r9);
        if (rax14 < 0) {
            addr_65a7_3:
            argmatch_invalid(r15_8, r14_9, rax14, rcx);
            argmatch_valid(rbp12, r12_11, r13_10, rcx);
            v13(rbp12, r12_11, r13_10, rcx);
            rax14 = -1;
            goto addr_661e_4;
        } else {
            addr_661e_4:
            return rax14;
        }
    }
    rdi15 = *reinterpret_cast<void***>(rdx);
    *reinterpret_cast<int32_t*>(&rbx16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
    if (rdi15) {
        do {
            eax17 = fun_2840(rdi15, r14_9, rdx);
            if (!eax17) 
                break;
            ++rbx16;
            rdi15 = *reinterpret_cast<void***>(rbp12 + rbx16 * 8);
        } while (rdi15);
        goto addr_65a0_8;
    } else {
        goto addr_65a0_8;
    }
    return rbx16;
    addr_65a0_8:
    rax14 = -1;
    goto addr_65a7_3;
}

struct s16 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_6633(void** rdi, struct s16* rsi, void** rdx, void** rcx) {
    int64_t r14_5;
    void** r12_6;
    void** r13_7;
    int64_t* rbx8;
    void** rbp9;
    uint32_t eax10;

    __asm__("cli ");
    r14_5 = rsi->f0;
    if (r14_5) {
        r12_6 = rdi;
        r13_7 = rcx;
        rbx8 = &rsi->f8;
        rbp9 = rdx;
        do {
            eax10 = fun_2800(r12_6, rbp9, r13_7);
            if (!eax10) 
                break;
            r14_5 = *rbx8;
            rbp9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(r13_7));
            ++rbx8;
        } while (r14_5);
    }
    return r14_5;
}

int64_t file_name = 0;

void fun_6693(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_66a3(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_2600(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_66b3() {
    void** rdi1;
    int32_t eax2;
    void** rax3;
    int1_t zf4;
    void** rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_25e0(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax3) == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_26c0();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_6743_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2990();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2600(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_6743_5:
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2990();
    }
}

int64_t mdir_name();

void xalloc_die();

void fun_6763() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = mdir_name();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void* last_component();

void fun_6783(signed char* rdi) {
    void* rbp2;
    signed char* rbx3;
    void* rax4;
    void* rax5;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rbp2) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp2) + 4) = 0;
    rbx3 = rdi;
    *reinterpret_cast<unsigned char*>(&rbp2) = reinterpret_cast<uint1_t>(*rdi == 47);
    rax4 = last_component();
    rax5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax4) - reinterpret_cast<uint64_t>(rbx3));
    while (reinterpret_cast<uint64_t>(rax5) > reinterpret_cast<uint64_t>(rbp2) && *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rbx3) + reinterpret_cast<uint64_t>(rax5) - 1) == 47) {
        rax5 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax5) - 1);
    }
    return;
}

void** fun_67c3(void** rdi, void** rsi) {
    void** rbp3;
    void** rbx4;
    void* rax5;
    void** r12_6;
    void* rax7;
    uint32_t ebx8;
    void** rax9;
    void** r8_10;
    void** rax11;
    void** rax12;
    void** rax13;

    __asm__("cli ");
    rbp3 = rdi;
    *reinterpret_cast<int32_t*>(&rbx4) = 0;
    *reinterpret_cast<int32_t*>(&rbx4 + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rbx4) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rdi) == 47);
    rax5 = last_component();
    r12_6 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax5) - reinterpret_cast<unsigned char>(rbp3));
    while (reinterpret_cast<unsigned char>(rbx4) < reinterpret_cast<unsigned char>(r12_6)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp3) + reinterpret_cast<unsigned char>(r12_6) + 0xffffffffffffffff) != 47) 
            goto addr_6840_4;
        --r12_6;
    }
    rax7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_6) ^ 1);
    ebx8 = *reinterpret_cast<uint32_t*>(&rax7) & 1;
    rax9 = fun_28c0(reinterpret_cast<unsigned char>(r12_6) + reinterpret_cast<uint64_t>(rax7) + 1, rsi);
    if (!rax9) {
        addr_686d_7:
        *reinterpret_cast<int32_t*>(&r8_10) = 0;
        *reinterpret_cast<int32_t*>(&r8_10 + 4) = 0;
    } else {
        rax11 = fun_2880(rax9, rbp3, r12_6);
        r8_10 = rax11;
        if (!*reinterpret_cast<signed char*>(&ebx8)) {
            *reinterpret_cast<int32_t*>(&r12_6) = 1;
            *reinterpret_cast<int32_t*>(&r12_6 + 4) = 0;
            goto addr_682e_10;
        } else {
            *reinterpret_cast<void***>(rax11) = reinterpret_cast<void**>(46);
            *reinterpret_cast<int32_t*>(&r12_6) = 1;
            *reinterpret_cast<int32_t*>(&r12_6 + 4) = 0;
            goto addr_682e_10;
        }
    }
    addr_6833_12:
    return r8_10;
    addr_682e_10:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r8_10) + reinterpret_cast<unsigned char>(r12_6)) = 0;
    goto addr_6833_12;
    addr_6840_4:
    rax12 = fun_28c0(r12_6 + 1, rsi);
    if (!rax12) 
        goto addr_686d_7;
    rax13 = fun_2880(rax12, rbp3, r12_6);
    r8_10 = rax13;
    goto addr_682e_10;
}

struct s17 {
    signed char[24] pad24;
    uint32_t f18;
    signed char[20] pad48;
    int64_t f30;
};

void fun_6883(struct s17* rdi) {
    uint32_t eax2;

    __asm__("cli ");
    eax2 = rdi->f18 & 0xf000;
    if (eax2 == 0x8000) {
        if (rdi->f30) {
            goto fun_26c0;
        } else {
            goto fun_26c0;
        }
    } else {
        if (eax2 == 0x4000) {
            goto fun_26c0;
        } else {
            if (eax2 == 0xa000) {
                goto fun_26c0;
            } else {
                if (eax2 == 0x6000) {
                    goto fun_26c0;
                } else {
                    if (eax2 == 0x2000) {
                        goto fun_26c0;
                    } else {
                        if (eax2 == "hr") {
                            goto fun_26c0;
                        } else {
                            if (eax2 == 0xc000) {
                                goto fun_26c0;
                            } else {
                                goto fun_26c0;
                            }
                        }
                    }
                }
            }
        }
    }
}

struct s18 {
    signed char f0;
    signed char f1;
    signed char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
    signed char f8;
    signed char f9;
    int16_t fa;
};

void fun_6973(uint32_t edi, struct s18* rsi) {
    uint32_t eax3;
    int32_t ecx4;
    uint32_t esi5;
    uint32_t ecx6;
    uint32_t ecx7;
    uint32_t ecx8;
    uint32_t ecx9;
    uint32_t ecx10;
    uint32_t ecx11;
    uint32_t ecx12;
    uint32_t ecx13;
    uint32_t ecx14;
    uint32_t ecx15;
    uint32_t ecx16;
    uint32_t ecx17;
    uint32_t ecx18;
    uint32_t ecx19;
    uint32_t ecx20;
    uint32_t ecx21;
    uint32_t ecx22;
    uint32_t ecx23;
    uint32_t ecx24;
    uint32_t eax25;
    uint32_t eax26;

    __asm__("cli ");
    eax3 = edi;
    ecx4 = 45;
    esi5 = edi & 0xf000;
    if (esi5 != 0x8000 && ((ecx4 = 100, esi5 != 0x4000) && ((ecx4 = 98, esi5 != 0x6000) && ((ecx4 = 99, esi5 != 0x2000) && ((ecx4 = 0x6c, esi5 != 0xa000) && ((ecx4 = 0x70, esi5 != "hr") && (ecx4 = 0x73, esi5 != 0xc000))))))) {
        ecx4 = 63;
    }
    rsi->f0 = *reinterpret_cast<signed char*>(&ecx4);
    ecx6 = eax3 & 0x100;
    ecx7 = (ecx6 - (ecx6 + reinterpret_cast<uint1_t>(ecx6 < ecx6 + reinterpret_cast<uint1_t>(ecx6 < 1))) & 0xffffffbb) + 0x72;
    rsi->f1 = *reinterpret_cast<signed char*>(&ecx7);
    ecx8 = eax3 & 0x80;
    ecx9 = (ecx8 - (ecx8 + reinterpret_cast<uint1_t>(ecx8 < ecx8 + reinterpret_cast<uint1_t>(ecx8 < 1))) & 0xffffffb6) + 0x77;
    rsi->f2 = *reinterpret_cast<signed char*>(&ecx9);
    ecx10 = eax3 & 64;
    ecx11 = ecx10 - (ecx10 + reinterpret_cast<uint1_t>(ecx10 < ecx10 + reinterpret_cast<uint1_t>(ecx10 < 1)));
    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax3) + 1) & 8)) {
        ecx12 = (ecx11 & 0xffffffb5) + 0x78;
    } else {
        ecx12 = (ecx11 & 0xffffffe0) + 0x73;
    }
    rsi->f3 = *reinterpret_cast<signed char*>(&ecx12);
    ecx13 = eax3 & 32;
    ecx14 = (ecx13 - (ecx13 + reinterpret_cast<uint1_t>(ecx13 < ecx13 + reinterpret_cast<uint1_t>(ecx13 < 1))) & 0xffffffbb) + 0x72;
    rsi->f4 = *reinterpret_cast<signed char*>(&ecx14);
    ecx15 = eax3 & 16;
    ecx16 = (ecx15 - (ecx15 + reinterpret_cast<uint1_t>(ecx15 < ecx15 + reinterpret_cast<uint1_t>(ecx15 < 1))) & 0xffffffb6) + 0x77;
    rsi->f5 = *reinterpret_cast<signed char*>(&ecx16);
    ecx17 = eax3 & 8;
    ecx18 = ecx17 - (ecx17 + reinterpret_cast<uint1_t>(ecx17 < ecx17 + reinterpret_cast<uint1_t>(ecx17 < 1)));
    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax3) + 1) & 4)) {
        ecx19 = (ecx18 & 0xffffffb5) + 0x78;
    } else {
        ecx19 = (ecx18 & 0xffffffe0) + 0x73;
    }
    rsi->f6 = *reinterpret_cast<signed char*>(&ecx19);
    ecx20 = eax3 & 4;
    ecx21 = (ecx20 - (ecx20 + reinterpret_cast<uint1_t>(ecx20 < ecx20 + reinterpret_cast<uint1_t>(ecx20 < 1))) & 0xffffffbb) + 0x72;
    rsi->f7 = *reinterpret_cast<signed char*>(&ecx21);
    ecx22 = eax3 & 2;
    ecx23 = (ecx22 - (ecx22 + reinterpret_cast<uint1_t>(ecx22 < ecx22 + reinterpret_cast<uint1_t>(ecx22 < 1))) & 0xffffffb6) + 0x77;
    rsi->f8 = *reinterpret_cast<signed char*>(&ecx23);
    ecx24 = eax3 & 1;
    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax3) + 1) & 2)) {
        eax25 = (eax3 - (eax3 + reinterpret_cast<uint1_t>(eax3 < eax3 + reinterpret_cast<uint1_t>(ecx24 < 1))) & 0xffffffb5) + 0x78;
        rsi->f9 = *reinterpret_cast<signed char*>(&eax25);
        rsi->fa = 32;
        return;
    } else {
        eax26 = (eax3 - (eax3 + reinterpret_cast<uint1_t>(eax3 < eax3 + reinterpret_cast<uint1_t>(ecx24 < 1))) & 0xffffffe0) + 0x74;
        rsi->f9 = *reinterpret_cast<signed char*>(&eax26);
        rsi->fa = 32;
        return;
    }
}

void fun_6af3(int64_t rdi) {
    __asm__("cli ");
}

struct s19 {
    signed char[20] pad20;
    signed char f14;
};

signed char* fun_6b03(uint64_t rdi, struct s19* rsi) {
    signed char* r8_3;
    uint64_t rcx4;
    signed char* rsi5;
    uint64_t rdx6;
    int32_t eax7;
    uint64_t rdx8;
    uint64_t rax9;
    uint64_t rcx10;
    int32_t ecx11;

    __asm__("cli ");
    rsi->f14 = 0;
    r8_3 = &rsi->f14;
    rcx4 = rdi;
    if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) {
        do {
            rsi5 = r8_3;
            --r8_3;
            rdx6 = (__intrinsic() >> 2) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rcx4) >> 63);
            eax7 = static_cast<int32_t>(48 + (rdx6 + rdx6 * 4) * 2) - *reinterpret_cast<int32_t*>(&rcx4);
            rcx4 = rdx6;
            *r8_3 = *reinterpret_cast<signed char*>(&eax7);
        } while (rdx6);
        *(r8_3 - 1) = 45;
        return rsi5 - 2;
    } else {
        do {
            --r8_3;
            rdx8 = __intrinsic() >> 3;
            rax9 = rdx8 + rdx8 * 4;
            rcx10 = rcx4 - (rax9 + rax9);
            ecx11 = *reinterpret_cast<int32_t*>(&rcx10) + 48;
            *r8_3 = *reinterpret_cast<signed char*>(&ecx11);
            rcx4 = rdx8;
        } while (rdx8);
        return r8_3;
    }
}

void** fun_29d0(int64_t rdi, int64_t rsi);

void** fun_28f0(int64_t rdi, void** rsi);

int64_t fun_2830(void** rdi, void* rsi, void** rdx, void** rcx, void** r8);

int32_t fun_2910(int64_t rdi, int64_t rsi, void** rdx, void** rcx, void** r8);

int64_t rpl_fclose(void** rdi, void* rsi, void** rdx, void** rcx, void** r8);

void** fun_28e0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

int64_t fun_2a50(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

int32_t fun_25c0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** fun_6c33() {
    void** rax1;
    void** v2;
    void** rax3;
    void**** rsp4;
    void** rsi5;
    void** rax6;
    void** r14_7;
    void** r15_8;
    void*** v9;
    void** r12_10;
    void* r14_11;
    void** rbx12;
    void** rcx13;
    void** r8_14;
    int64_t rax15;
    void* rsp16;
    void** rcx17;
    void** rdx18;
    int32_t eax19;
    void** r13_20;
    int32_t v21;
    struct s3* rax22;
    void** rbp23;
    struct s3* rax24;
    void** r9_25;
    struct s13* rax26;
    void** rax27;
    struct s3* rax28;
    struct s3* rax29;
    void** rax30;
    void** rax31;
    void** rax32;
    uint32_t r13d33;
    void** rax34;
    void** rax35;
    uint64_t rcx36;
    int32_t v37;
    uint64_t rax38;
    int32_t v39;
    uint64_t rsi40;
    uint64_t rsi41;
    uint64_t rsi42;
    void** rcx43;
    uint32_t eax44;
    void* rsp45;
    void** rdx46;
    uint32_t eax47;
    uint32_t eax48;
    uint32_t eax49;
    uint32_t eax50;
    uint32_t eax51;
    uint32_t eax52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint32_t eax56;
    uint32_t eax57;
    uint32_t eax58;
    uint32_t eax59;
    uint32_t eax60;
    uint32_t eax61;
    struct s3* rax62;
    void** rdx63;
    int32_t ecx64;
    uint32_t eax65;
    uint32_t eax66;
    uint32_t eax67;
    uint32_t eax68;
    void** v69;
    uint32_t eax70;
    uint32_t eax71;
    uint32_t eax72;
    uint32_t eax73;
    uint32_t eax74;
    uint32_t eax75;
    uint32_t eax76;
    uint32_t eax77;
    uint32_t eax78;
    uint32_t eax79;
    void** rax80;
    void** r14d81;
    int64_t rax82;
    uint32_t eax83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    uint32_t eax90;
    uint32_t eax91;
    uint32_t eax92;
    uint32_t eax93;
    uint32_t eax94;
    void** rax95;
    uint32_t r12d96;
    int64_t rax97;
    void** rax98;
    void** rdi99;
    void** rax100;
    void** rdi101;
    void** rax102;
    void** rdi103;
    void** rax104;
    uint32_t eax105;
    uint32_t eax106;
    uint32_t eax107;
    uint32_t eax108;
    uint32_t eax109;
    uint32_t eax110;
    uint32_t eax111;
    uint32_t eax112;
    uint32_t eax113;
    uint32_t eax114;
    uint32_t eax115;
    uint32_t eax116;
    uint32_t eax117;
    uint32_t eax118;
    uint32_t eax119;
    struct s3* rax120;
    int32_t eax121;
    void** v122;
    void** rax123;
    void* rax124;
    uint32_t eax125;
    uint32_t eax126;
    uint32_t eax127;
    void** rdi128;
    void** rdi129;
    void** rdi130;
    void** rdi131;

    __asm__("cli ");
    rax1 = g28;
    v2 = rax1;
    rax3 = fun_29d0("/proc/self/mountinfo", "re");
    rsp4 = reinterpret_cast<void****>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 88 - 8 + 8);
    if (!rax3) {
        rsi5 = reinterpret_cast<void**>("r");
        rax6 = fun_28f0("/etc/mtab", "r");
        r14_7 = rax6;
        if (!rax6) 
            goto addr_6f06_3;
        r15_8 = reinterpret_cast<void**>("autofs");
        v9 = reinterpret_cast<void***>(rsp4 - 1 + 1 + 6);
        goto addr_7373_5;
    }
    r12_10 = rax3;
    r14_11 = reinterpret_cast<void*>(rsp4 + 8);
    v9 = reinterpret_cast<void***>(rsp4 + 6);
    rbx12 = reinterpret_cast<void**>(rsp4 + 7);
    r15_8 = reinterpret_cast<void**>("%*u %*u %u:%u %n");
    while (rcx13 = r12_10, rax15 = fun_2830(rbx12, r14_11, 10, rcx13, r8_14), rsp16 = reinterpret_cast<void*>(rsp4 - 1 + 1), rax15 != -1) {
        rcx17 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp16) + 40);
        rdx18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp16) + 36);
        r8_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp16) + 44);
        eax19 = fun_2910(0, "%*u %*u %u:%u %n", rdx18, rcx17, r8_14);
        rsp4 = reinterpret_cast<void****>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
        if (reinterpret_cast<uint32_t>(eax19 - 2) > 1) 
            continue;
        r13_20 = reinterpret_cast<void**>(static_cast<uint64_t>(reinterpret_cast<unsigned char>(static_cast<int64_t>(v21))));
        rax22 = fun_2740(r13_20, 32, rdx18, rcx17, r8_14);
        rsp4 = rsp4 - 1 + 1;
        if (!rax22) 
            continue;
        rax22->f0 = 0;
        rbp23 = reinterpret_cast<void**>(&rax22->f1);
        rax24 = fun_2740(rbp23, 32, rdx18, rcx17, r8_14);
        rsp4 = rsp4 - 1 + 1;
        if (!rax24) 
            continue;
        rax24->f0 = 0;
        rax26 = fun_2a60(&rax24->f1, " - ", rdx18, rcx17, r8_14, r9_25);
        rsp4 = rsp4 - 1 + 1;
        if (!rax26) 
            continue;
        rax27 = reinterpret_cast<void**>(&rax26->f3);
        rax28 = fun_2740(rax27, 32, rdx18, rcx17, r8_14);
        rsp4 = rsp4 - 1 + 1;
        if (!rax28) 
            continue;
        rax28->f0 = 0;
        r8_14 = reinterpret_cast<void**>(&rax28->f1);
        rax29 = fun_2740(r8_14, 32, rdx18, rcx17, r8_14);
        rsp4 = rsp4 - 1 + 1;
        if (!rax29) 
            continue;
        r8_14 = r8_14;
        rax29->f0 = 0;
        unescape_tab(r8_14, 32, rdx18, rcx17);
        unescape_tab(rbp23, 32, rdx18, rcx17);
        unescape_tab(r13_20, 32, rdx18, rcx17);
        unescape_tab(rax27, 32, rdx18, rcx17);
        rax30 = xmalloc(56, 32, rdx18, rcx17, r8_14);
        rax31 = xstrdup(r8_14, 32, rdx18, rcx17, r8_14);
        *reinterpret_cast<void***>(rax30) = rax31;
        rax32 = xstrdup(rbp23, 32, rax30, rcx17, r8_14);
        r13d33 = 1;
        *reinterpret_cast<void***>(rax30 + 8) = rax32;
        rax34 = xstrdup(r13_20, 32, rax30, rcx17, r8_14);
        *reinterpret_cast<void***>(rax30 + 16) = rax34;
        rax35 = xstrdup(rax27, 32, rax30, rcx17, r8_14);
        *reinterpret_cast<int32_t*>(&rcx36) = v37;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx36) + 4) = 0;
        rbp23 = rax35;
        *reinterpret_cast<void***>(rax30 + 24) = rax35;
        *reinterpret_cast<int32_t*>(&rax38) = v39;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax38) + 4) = 0;
        rsi40 = rcx36 << 8;
        *reinterpret_cast<void***>(rax30 + 40) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax30 + 40)) | 4);
        *reinterpret_cast<uint32_t*>(&rsi41) = *reinterpret_cast<uint32_t*>(&rsi40) & 0xfff00;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi41) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rsi42) = *reinterpret_cast<unsigned char*>(&rax38);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi42) + 4) = 0;
        rcx43 = reinterpret_cast<void**>(rcx36 << 32 & 0xfffff00000000000 | rsi41 | rsi42);
        *reinterpret_cast<void***>(rax30 + 32) = reinterpret_cast<void**>(rax38 << 12 & 0xffffff00000 | reinterpret_cast<unsigned char>(rcx43));
        eax44 = fun_2840(rbp23, "autofs", rax30);
        rsp45 = reinterpret_cast<void*>(rsp4 - 1 + 1 - 1 + 1 - 1 + 1 - 1 + 1 - 1 + 1 - 1 + 1 - 1 + 1 - 1 + 1 - 1 + 1 - 1 + 1);
        rdx46 = rax30;
        if (eax44 && ((eax47 = fun_2840(rbp23, "proc", rdx46), rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8), rdx46 = rax30, !!eax47) && ((eax48 = fun_2840(rbp23, "subfs", rdx46), rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8), rdx46 = rax30, !!eax48) && ((eax49 = fun_2840(rbp23, "debugfs", rdx46), rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8), rdx46 = rax30, !!eax49) && ((eax50 = fun_2840(rbp23, "devpts", rdx46), rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8), rdx46 = rax30, !!eax50) && ((eax51 = fun_2840(rbp23, "fusectl", rdx46), rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8), rdx46 = rax30, !!eax51) && ((eax52 = fun_2840(rbp23, "fuse.portal", rdx46), rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8), rdx46 = rax30, !!eax52) && ((eax53 = fun_2840(rbp23, "mqueue", rdx46), rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8), rdx46 = rax30, !!eax53) && ((eax54 = fun_2840(rbp23, "rpc_pipefs", rdx46), rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8), rdx46 = rax30, !!eax54) && ((eax55 = fun_2840(rbp23, "sysfs", rdx46), rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8), rdx46 = rax30, !!eax55) && ((eax56 = fun_2840(rbp23, "devfs", rdx46), rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8), rdx46 = rax30, !!eax56) && ((eax57 = fun_2840(rbp23, "kernfs", rdx46), rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8), rdx46 = rax30, !!eax57) && (eax58 = fun_2840(rbp23, "ignore", rdx46), rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8), rdx46 = rax30, !!eax58))))))))))))) {
            eax59 = fun_2840(rbp23, "none", rdx46);
            rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
            rdx46 = rax30;
            *reinterpret_cast<unsigned char*>(&r13d33) = reinterpret_cast<uint1_t>(eax59 == 0);
        }
        eax60 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx46 + 40));
        eax61 = eax60 & 0xfffffffe | r13d33;
        r13_20 = *reinterpret_cast<void***>(rdx46);
        *reinterpret_cast<void***>(rdx46 + 40) = *reinterpret_cast<void***>(&eax61);
        rax62 = fun_2740(r13_20, 58, rdx46, rcx43, r8_14);
        rsp4 = reinterpret_cast<void****>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
        rdx63 = rdx46;
        ecx64 = 1;
        if (rax62) 
            goto addr_6ea4_17;
        if (*reinterpret_cast<void***>(r13_20) == 47 && reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_20 + 1) == 47)) {
            eax65 = fun_2840(rbp23, "smbfs", rdx63, rbp23, "smbfs", rdx63);
            rsp4 = rsp4 - 1 + 1;
            rdx63 = rdx46;
            ecx64 = 1;
            if (!eax65 || ((eax66 = fun_2840(rbp23, "smb3", rdx63, rbp23, "smb3", rdx63), rsp4 = rsp4 - 1 + 1, rdx63 = rdx46, ecx64 = 1, eax66 == 0) || (eax67 = fun_2840(rbp23, "cifs", rdx63, rbp23, "cifs", rdx63), rsp4 = rsp4 - 1 + 1, rdx63 = rdx46, ecx64 = 1, eax67 == 0))) {
                addr_6ea4_17:
                eax68 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx63 + 40))) & 0xfffffffd | reinterpret_cast<uint32_t>(ecx64 + ecx64);
                *reinterpret_cast<void***>(rdx63 + 40) = *reinterpret_cast<void***>(&eax68);
                *v9 = rdx63;
                v9 = reinterpret_cast<void***>(rdx63 + 48);
                continue;
            }
        }
        v69 = rdx63;
        eax70 = fun_2840(rbp23, "acfs", rdx63, rbp23, "acfs", rdx63);
        rsp4 = rsp4 - 1 + 1;
        rdx63 = v69;
        ecx64 = 1;
        if (eax70 && ((eax71 = fun_2840(rbp23, "afs", rdx63, rbp23, "afs", rdx63), rsp4 = rsp4 - 1 + 1, rdx63 = v69, ecx64 = 1, !!eax71) && ((eax72 = fun_2840(rbp23, "coda", rdx63, rbp23, "coda", rdx63), rsp4 = rsp4 - 1 + 1, rdx63 = v69, ecx64 = 1, !!eax72) && ((eax73 = fun_2840(rbp23, "auristorfs", rdx63, rbp23, "auristorfs", rdx63), rsp4 = rsp4 - 1 + 1, rdx63 = v69, ecx64 = 1, !!eax73) && ((eax74 = fun_2840(rbp23, "fhgfs", rdx63, rbp23, "fhgfs", rdx63), rsp4 = rsp4 - 1 + 1, rdx63 = v69, ecx64 = 1, !!eax74) && ((eax75 = fun_2840(rbp23, "gpfs", rdx63, rbp23, "gpfs", rdx63), rsp4 = rsp4 - 1 + 1, rdx63 = v69, ecx64 = 1, !!eax75) && ((eax76 = fun_2840(rbp23, "ibrix", rdx63, rbp23, "ibrix", rdx63), rsp4 = rsp4 - 1 + 1, rdx63 = v69, ecx64 = 1, !!eax76) && ((eax77 = fun_2840(rbp23, "ocfs2", rdx63, rbp23, "ocfs2", rdx63), rsp4 = rsp4 - 1 + 1, rdx63 = v69, ecx64 = 1, !!eax77) && (eax78 = fun_2840(rbp23, "vxfs", rdx63, rbp23, "vxfs", rdx63), rsp4 = rsp4 - 1 + 1, rdx63 = v69, ecx64 = 1, !!eax78))))))))) {
            eax79 = fun_2840("-hosts", r13_20, rdx63, "-hosts", r13_20, rdx63);
            rsp4 = rsp4 - 1 + 1;
            rdx63 = v69;
            *reinterpret_cast<unsigned char*>(&ecx64) = reinterpret_cast<uint1_t>(eax79 == 0);
            goto addr_6ea4_17;
        }
    }
    fun_25b0(0, 0);
    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_10)) & 32)) 
        goto addr_6ee5_24;
    rax80 = fun_25e0();
    r14d81 = *reinterpret_cast<void***>(rax80);
    r13_20 = rax80;
    rpl_fclose(r12_10, r14_11, 10, rcx13, r8_14);
    *reinterpret_cast<void***>(r13_20) = r14d81;
    goto addr_7217_26;
    addr_6ee5_24:
    rax82 = rpl_fclose(r12_10, r14_11, 10, rcx13, r8_14);
    if (*reinterpret_cast<int32_t*>(&rax82) == -1) 
        goto addr_7294_27; else 
        goto addr_6ef6_28;
    addr_6f1a_29:
    return r14_7;
    while (1) {
        addr_7689_30:
        while (1) {
            rsi5 = reinterpret_cast<void**>("acfs");
            *reinterpret_cast<uint32_t*>(&r12_10) = 1;
            eax83 = fun_2840(rbp23, "acfs", 10, rbp23, "acfs", 10);
            if (eax83 && ((rsi5 = reinterpret_cast<void**>("afs"), eax84 = fun_2840(rbp23, "afs", 10, rbp23, "afs", 10), !!eax84) && ((rsi5 = reinterpret_cast<void**>("coda"), eax85 = fun_2840(rbp23, "coda", 10, rbp23, "coda", 10), !!eax85) && ((rsi5 = reinterpret_cast<void**>("auristorfs"), eax86 = fun_2840(rbp23, "auristorfs", 10, rbp23, "auristorfs", 10), !!eax86) && ((rsi5 = reinterpret_cast<void**>("fhgfs"), eax87 = fun_2840(rbp23, "fhgfs", 10, rbp23, "fhgfs", 10), !!eax87) && ((rsi5 = reinterpret_cast<void**>("gpfs"), eax88 = fun_2840(rbp23, "gpfs", 10, rbp23, "gpfs", 10), !!eax88) && ((rsi5 = reinterpret_cast<void**>("ibrix"), eax89 = fun_2840(rbp23, "ibrix", 10, rbp23, "ibrix", 10), !!eax89) && ((rsi5 = reinterpret_cast<void**>("ocfs2"), eax90 = fun_2840(rbp23, "ocfs2", 10, rbp23, "ocfs2", 10), !!eax90) && (rsi5 = reinterpret_cast<void**>("vxfs"), eax91 = fun_2840(rbp23, "vxfs", 10, rbp23, "vxfs", 10), !!eax91))))))))) {
                rsi5 = r13_20;
                eax92 = fun_2840("-hosts", rsi5, 10, "-hosts", rsi5, 10);
                *reinterpret_cast<unsigned char*>(&r12_10) = reinterpret_cast<uint1_t>(eax92 == 0);
            }
            while (1) {
                do {
                    eax93 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx12 + 40));
                    *reinterpret_cast<uint32_t*>(&r12_10) = *reinterpret_cast<uint32_t*>(&r12_10) + *reinterpret_cast<uint32_t*>(&r12_10);
                    *reinterpret_cast<void***>(rbx12 + 32) = reinterpret_cast<void**>(0xffffffffffffffff);
                    eax94 = eax93 & 0xfffffffd | *reinterpret_cast<uint32_t*>(&r12_10);
                    *reinterpret_cast<void***>(rbx12 + 40) = *reinterpret_cast<void***>(&eax94);
                    *v9 = rbx12;
                    v9 = reinterpret_cast<void***>(rbx12 + 48);
                    addr_7373_5:
                    rax95 = fun_28e0(r14_7, rsi5, 10, rcx13, r8_14);
                    rbp23 = rax95;
                    if (!rax95) 
                        break;
                    r12d96 = 1;
                    rax97 = fun_2a50(rbp23, "bind", 10, rcx13, r8_14);
                    rax98 = xmalloc(56, "bind", 10, rcx13, r8_14, 56, "bind", 10, rcx13, r8_14);
                    rdi99 = *reinterpret_cast<void***>(rbp23);
                    rbx12 = rax98;
                    rax100 = xstrdup(rdi99, "bind", 10, rcx13, r8_14);
                    rdi101 = *reinterpret_cast<void***>(rbp23 + 8);
                    *reinterpret_cast<void***>(rbx12) = rax100;
                    rax102 = xstrdup(rdi101, "bind", 10, rcx13, r8_14);
                    *reinterpret_cast<void***>(rbx12 + 16) = reinterpret_cast<void**>(0);
                    rdi103 = *reinterpret_cast<void***>(rbp23 + 16);
                    *reinterpret_cast<void***>(rbx12 + 8) = rax102;
                    rax104 = xstrdup(rdi103, "bind", 10, rcx13, r8_14);
                    *reinterpret_cast<void***>(rbx12 + 40) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx12 + 40)) | 4);
                    *reinterpret_cast<void***>(rbx12 + 24) = rax104;
                    rbp23 = rax104;
                    eax105 = fun_2840(rax104, r15_8, 10, rax104, r15_8, 10);
                    if (eax105 && ((eax106 = fun_2840(rbp23, "proc", 10, rbp23, "proc", 10), !!eax106) && ((eax107 = fun_2840(rbp23, "subfs", 10, rbp23, "subfs", 10), !!eax107) && ((eax108 = fun_2840(rbp23, "debugfs", 10, rbp23, "debugfs", 10), !!eax108) && ((eax109 = fun_2840(rbp23, "devpts", 10, rbp23, "devpts", 10), !!eax109) && ((eax110 = fun_2840(rbp23, "fusectl", 10, rbp23, "fusectl", 10), !!eax110) && ((eax111 = fun_2840(rbp23, "fuse.portal", 10, rbp23, "fuse.portal", 10), !!eax111) && ((eax112 = fun_2840(rbp23, "mqueue", 10, rbp23, "mqueue", 10), !!eax112) && ((eax113 = fun_2840(rbp23, "rpc_pipefs", 10, rbp23, "rpc_pipefs", 10), !!eax113) && ((eax114 = fun_2840(rbp23, "sysfs", 10, rbp23, "sysfs", 10), !!eax114) && ((eax115 = fun_2840(rbp23, "devfs", 10, rbp23, "devfs", 10), !!eax115) && ((eax116 = fun_2840(rbp23, "kernfs", 10, rbp23, "kernfs", 10), !!eax116) && (eax117 = fun_2840(rbp23, "ignore", 10, rbp23, "ignore", 10), !!eax117))))))))))))) {
                        eax118 = fun_2840(rbp23, "none", 10, rbp23, "none", 10);
                        *reinterpret_cast<unsigned char*>(&r12d96) = reinterpret_cast<uint1_t>(rax97 == 0);
                        *reinterpret_cast<unsigned char*>(&eax118) = reinterpret_cast<uint1_t>(eax118 == 0);
                        r12d96 = r12d96 & eax118;
                    }
                    r13_20 = *reinterpret_cast<void***>(rbx12);
                    *reinterpret_cast<int32_t*>(&rsi5) = 58;
                    *reinterpret_cast<int32_t*>(&rsi5 + 4) = 0;
                    eax119 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx12 + 40))) & 0xfffffffe | r12d96;
                    *reinterpret_cast<uint32_t*>(&r12_10) = 1;
                    *reinterpret_cast<void***>(rbx12 + 40) = *reinterpret_cast<void***>(&eax119);
                    rax120 = fun_2740(r13_20, 58, 10, rcx13, r8_14);
                } while (rax120);
                goto addr_753a_37;
                eax121 = fun_25c0(r14_7, rsi5, 10, rcx13, r8_14);
                if (eax121) {
                    addr_6ef6_28:
                    *v9 = reinterpret_cast<void**>(0);
                    r14_7 = v122;
                } else {
                    addr_7294_27:
                    rax123 = fun_25e0();
                    r14d81 = *reinterpret_cast<void***>(rax123);
                    r13_20 = rax123;
                    addr_7217_26:
                    *v9 = reinterpret_cast<void**>(0);
                    rbx12 = v122;
                    if (rbx12) 
                        goto addr_7242_39; else 
                        goto addr_722c_40;
                }
                addr_6f06_3:
                rax124 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v2) - reinterpret_cast<unsigned char>(g28));
                if (!rax124) 
                    goto addr_6f1a_29;
                fun_2710();
                addr_7639_42:
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_20 + 1) == 47)) 
                    break;
                rsi5 = reinterpret_cast<void**>("smbfs");
                eax125 = fun_2840(rbp23, "smbfs", 10, rbp23, "smbfs", 10);
                if (!eax125) 
                    continue;
                rsi5 = reinterpret_cast<void**>("smb3");
                eax126 = fun_2840(rbp23, "smb3", 10, rbp23, "smb3", 10);
                if (!eax126) 
                    continue;
                rsi5 = reinterpret_cast<void**>("cifs");
                eax127 = fun_2840(rbp23, "cifs", 10, rbp23, "cifs", 10);
                if (!eax127) 
                    continue; else 
                    goto addr_7689_30;
                do {
                    addr_7242_39:
                    rbp23 = rbx12;
                    rbx12 = *reinterpret_cast<void***>(rbx12 + 48);
                    rdi128 = *reinterpret_cast<void***>(rbp23);
                    fun_25b0(rdi128, rdi128);
                    rdi129 = *reinterpret_cast<void***>(rbp23 + 8);
                    fun_25b0(rdi129, rdi129);
                    rdi130 = *reinterpret_cast<void***>(rbp23 + 16);
                    fun_25b0(rdi130, rdi130);
                    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp23 + 40)) & 4) {
                        rdi131 = *reinterpret_cast<void***>(rbp23 + 24);
                        fun_25b0(rdi131, rdi131);
                    }
                    fun_25b0(rbp23, rbp23);
                    v122 = rbx12;
                } while (rbx12);
                addr_7278_48:
                *reinterpret_cast<void***>(r13_20) = r14d81;
                *reinterpret_cast<int32_t*>(&r14_7) = 0;
                *reinterpret_cast<int32_t*>(&r14_7 + 4) = 0;
                goto addr_6f06_3;
                addr_722c_40:
                goto addr_7278_48;
                addr_753a_37:
                if (*reinterpret_cast<void***>(r13_20) == 47) 
                    goto addr_7639_42; else 
                    break;
            }
        }
    }
}

struct s20 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[15] pad40;
    unsigned char f28;
};

void fun_7693(struct s20* rdi) {
    void** rdi2;
    void** rdi3;
    void** rdi4;
    void** rdi5;

    __asm__("cli ");
    rdi2 = rdi->f0;
    fun_25b0(rdi2);
    rdi3 = rdi->f8;
    fun_25b0(rdi3);
    rdi4 = rdi->f10;
    fun_25b0(rdi4);
    if (rdi->f28 & 4) {
        rdi5 = rdi->f18;
        fun_25b0(rdi5);
        goto fun_25b0;
    } else {
        goto fun_25b0;
    }
}

void** fun_9253(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8, int64_t r9) {
    int64_t v7;
    int64_t v8;
    void** rax9;

    __asm__("cli ");
    rax9 = __strftime_internal_isra_0(rdi, rsi, rdx, rcx, 0, 0, 0xff, r8, r9, v7, v8);
    return rax9;
}

void fun_2a10(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

struct s21 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s21* fun_2770();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_9273(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s21* rax4;
    void** r12_5;
    int32_t eax6;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2a10("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_25d0("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_2770();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax6 = fun_25f0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7), !eax6))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<unsigned char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_aa13(int64_t rdi) {
    int64_t rbp2;
    void** rax3;
    void** r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_25e0();
    r12d4 = *reinterpret_cast<void***>(rax3);
    if (!rbp2) {
        rbp2 = 0x182c0;
    }
    xmemdup(rbp2, 56);
    *reinterpret_cast<void***>(rax3) = r12d4;
    return;
}

int64_t fun_aa53(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x182c0);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_aa73(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x182c0);
    }
    *rdi = esi;
    return 0x182c0;
}

int64_t fun_aa93(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x182c0);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s22 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_aad3(struct s22* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s22*>(0x182c0);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s23 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s23* fun_aaf3(struct s23* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s23*>(0x182c0);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x2aaa;
    if (!rdx) 
        goto 0x2aaa;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x182c0;
}

struct s24 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_ab33(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s24* r8) {
    struct s24* rbx6;
    void** rax7;
    void** r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s24*>(0x182c0);
    }
    rax7 = fun_25e0();
    r15d8 = *reinterpret_cast<void***>(rax7);
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0xab66);
    *reinterpret_cast<void***>(rax7) = r15d8;
    return rax13;
}

struct s25 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_abb3(int64_t rdi, int64_t rsi, void*** rdx, struct s25* rcx) {
    struct s25* rbx5;
    void** rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    void** v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s25*>(0x182c0);
    }
    rax6 = fun_25e0();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *reinterpret_cast<void***>(rax6);
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0xabe1);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0xac3c);
    *reinterpret_cast<void***>(rax6) = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_aca3() {
    __asm__("cli ");
}

void** g18098 = reinterpret_cast<void**>(0xc0);

int64_t slotvec0 = 0x100;

void fun_acb3() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_25b0(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi7 != 0x181c0) {
        fun_25b0(rdi7);
        g18098 = reinterpret_cast<void**>(0x181c0);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0x18090) {
        fun_25b0(r12_2);
        slotvec = reinterpret_cast<void**>(0x18090);
    }
    nslots = 1;
    return;
}

void fun_ad53() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_ad73() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_ad83(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_ada3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_adc3(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s7* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2ab0;
    rcx5 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2710();
    } else {
        return rax6;
    }
}

void** fun_ae53(void** rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s7* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2ab5;
    rcx6 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2710();
    } else {
        return rax7;
    }
}

void** fun_aee3(int32_t edi, int64_t rsi) {
    void** rax3;
    struct s7* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x2aba;
    rcx4 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2710();
    } else {
        return rax5;
    }
}

void** fun_af73(int32_t edi, int64_t rsi, int64_t rdx) {
    void** rax4;
    struct s7* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x2abf;
    rcx5 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2710();
    } else {
        return rax6;
    }
}

void** fun_b003(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s7* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xd2b0]");
    __asm__("movdqa xmm1, [rip+0xd2b8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0xd2a1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2710();
    } else {
        return rax10;
    }
}

void** fun_b0a3(int64_t rdi, uint32_t esi) {
    struct s7* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xd210]");
    __asm__("movdqa xmm1, [rip+0xd218]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0xd201]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2710();
    } else {
        return rax9;
    }
}

void** fun_b143(int64_t rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xd170]");
    __asm__("movdqa xmm1, [rip+0xd178]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0xd159]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2710();
    } else {
        return rax3;
    }
}

void** fun_b1d3(int64_t rdi, int64_t rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xd0e0]");
    __asm__("movdqa xmm1, [rip+0xd0e8]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0xd0d6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2710();
    } else {
        return rax4;
    }
}

void** fun_b263(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s7* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2ac4;
    rcx5 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2710();
    } else {
        return rax6;
    }
}

void** fun_b303(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s7* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xcfaa]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0xcfa2]");
    __asm__("movdqa xmm2, [rip+0xcfaa]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2ac9;
    if (!rdx) 
        goto 0x2ac9;
    rcx6 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2710();
    } else {
        return rax7;
    }
}

void** fun_b3a3(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    void** rcx6;
    struct s7* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xcf0a]");
    __asm__("movdqa xmm1, [rip+0xcf12]");
    __asm__("movdqa xmm2, [rip+0xcf1a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2ace;
    if (!rdx) 
        goto 0x2ace;
    rcx7 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2710();
    } else {
        return rax9;
    }
}

void** fun_b453(int64_t rdi, int64_t rsi, int64_t rdx) {
    void** rdx4;
    struct s7* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xce5a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0xce52]");
    __asm__("movdqa xmm2, [rip+0xce5a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2ad3;
    if (!rsi) 
        goto 0x2ad3;
    rcx5 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2710();
    } else {
        return rax6;
    }
}

void** fun_b4f3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s7* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xcdba]");
    __asm__("movdqa xmm1, [rip+0xcdc2]");
    __asm__("movdqa xmm2, [rip+0xcdca]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2ad8;
    if (!rsi) 
        goto 0x2ad8;
    rcx6 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2710();
    } else {
        return rax7;
    }
}

void fun_b593() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_b5a3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_b5c3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_b5e3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s26 {
    int32_t f0;
    signed char[4] pad8;
    uint64_t f8;
};

int32_t open_safer(int64_t rdi, int64_t rsi);

uint64_t fun_27b0();

int64_t fun_b603(struct s26* rdi) {
    int32_t eax2;
    uint64_t rax3;
    int64_t rax4;

    __asm__("cli ");
    rdi->f8 = 0;
    eax2 = open_safer(".", 0x80000);
    rdi->f0 = eax2;
    if (eax2 < 0) {
        rax3 = fun_27b0();
        rdi->f8 = rax3;
        *reinterpret_cast<uint32_t*>(&rax4) = -static_cast<uint32_t>(reinterpret_cast<uint1_t>(0 < static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax3 < 1))));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        return rax4;
    } else {
        return 0;
    }
}

int32_t fun_2930(int64_t rdi, void** rsi, void** rdx);

void fun_b663(int32_t* rdi) {
    __asm__("cli ");
    if (*rdi < 0) {
        goto 0xd3b0;
    } else {
        goto fun_2930;
    }
}

int32_t fun_27d0();

void fun_b693(int32_t* rdi) {
    __asm__("cli ");
    if (*rdi >= 0) {
        fun_27d0();
    }
    goto fun_25b0;
}

void** fun_b743(void** rdi, void** rsi) {
    void** rax3;
    void** r12_4;
    void** rax5;
    void** rbx6;
    void** rax7;
    void** rax8;

    __asm__("cli ");
    if (!rdi) {
        rax3 = fun_28c0(0x80, rsi);
        r12_4 = rax3;
        if (rax3) {
            *reinterpret_cast<void***>(r12_4) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_4 + 8) = reinterpret_cast<void**>(0);
            return r12_4;
        }
    } else {
        rax5 = fun_26e0(rdi);
        rbx6 = rax5 + 1;
        *reinterpret_cast<int32_t*>(&rax7) = 0x76;
        *reinterpret_cast<int32_t*>(&rax7 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rbx6) >= reinterpret_cast<unsigned char>(0x76)) {
            rax7 = rbx6;
        }
        rax8 = fun_28c0(reinterpret_cast<uint64_t>(rax7 + 17) & 0xfffffffffffffff8, rsi);
        r12_4 = rax8;
        if (rax8) {
            *reinterpret_cast<void***>(rax8) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_4 + 8) = reinterpret_cast<void**>(1);
            fun_2880(r12_4 + 9, rdi, rbx6);
            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_4) + reinterpret_cast<unsigned char>(rbx6) + 9) = 0;
        }
    }
    return r12_4;
}

void fun_ba13(void** rdi) {
    void** rbx2;
    void** rdi3;

    __asm__("cli ");
    if (rdi == 1) {
        return;
    } else {
        rbx2 = rdi;
        if (rdi) {
            do {
                rdi3 = rbx2;
                rbx2 = *reinterpret_cast<void***>(rbx2);
                fun_25b0(rdi3);
            } while (rbx2);
        }
        return;
    }
}

int64_t fun_2550(void** rdi, void** rsi);

void** fun_ba53(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rax7;
    void** rsi8;
    int64_t rax9;
    signed char al10;
    signed char al11;

    __asm__("cli ");
    if (rdi) {
        rax7 = set_tz(rdi, rsi, rdx, rcx, r8, r9);
        if (rax7) {
            rsi8 = rdx;
            rax9 = fun_2550(rsi, rsi8);
            if (!rax9 || (rsi8 = rdx, al10 = save_abbr(rdi, rsi8), al10 == 0)) {
                if (rax7 != 1) {
                    revert_tz_part_0(rax7, rsi8);
                }
            } else {
                if (reinterpret_cast<int1_t>(rax7 == 1) || (al11 = revert_tz_part_0(rax7, rsi8), !!al11)) {
                    return rdx;
                }
            }
        }
        return 0;
    }
}

int64_t rpl_mktime(void** rdi);

int64_t fun_bb03(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rbp7;
    void** rax8;
    void* rax9;
    void** rax10;
    int64_t r13_11;
    void** r15_12;
    int64_t rax13;
    signed char al14;
    signed char al15;
    void** v16;
    void* rax17;

    __asm__("cli ");
    rbp7 = rsi;
    rax8 = g28;
    if (!rdi) {
        rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(g28));
        if (rax9) {
            fun_2710();
        }
    }
    rax10 = set_tz(rdi, rsi, rdx, rcx, r8, r9);
    if (!rax10) {
        addr_bb90_7:
        r13_11 = -1;
    } else {
        r15_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 64) - 8 + 8);
        rax13 = rpl_mktime(r15_12);
        r13_11 = rax13;
        if (!0 || (rsi = r15_12, al14 = save_abbr(rdi, rsi), al14 == 0)) {
            if (rax10 != 1) {
                revert_tz_part_0(rax10, rsi);
                goto addr_bb90_7;
            }
        } else {
            if (reinterpret_cast<int1_t>(rax10 == 1) || (al15 = revert_tz_part_0(rax10, rsi), !!al15)) {
                __asm__("movdqa xmm0, [rsp]");
                __asm__("movdqa xmm1, [rsp+0x10]");
                __asm__("movdqa xmm2, [rsp+0x20]");
                __asm__("movups [rbp+0x0], xmm0");
                *reinterpret_cast<void***>(rbp7 + 48) = v16;
                __asm__("movups [rbp+0x10], xmm1");
                __asm__("movups [rbp+0x20], xmm2");
            } else {
                goto addr_bb90_7;
            }
        }
    }
    rax17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(g28));
    if (!rax17) {
        return r13_11;
    }
}

struct s27 {
    signed char[32] pad32;
    int32_t f20;
};

void fun_bc53(struct s27* rdi) {
    __asm__("cli ");
    rdi->f20 = 0;
    goto 0xcd10;
}

struct s28 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    void*** f20;
    int64_t f28;
    void** f30;
    signed char[7] pad56;
    int64_t f38;
    void*** f40;
};

void fun_2850(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_bc73(void** rdi, void** rsi, void** rdx, void** rcx, struct s28* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    void*** v9;
    int64_t v10;
    void** v11;
    int64_t v12;
    void*** v13;
    int64_t v14;
    void*** v15;
    int64_t v16;
    void** v17;
    int64_t v18;
    void*** v19;
    void** rax20;
    int64_t v21;
    void*** v22;
    int64_t v23;
    void** v24;
    int64_t v25;
    void*** v26;
    void** rax27;
    int64_t v28;
    void*** v29;
    int64_t v30;
    void** v31;
    int64_t v32;
    void*** v33;
    int64_t r10_34;
    void** r9_35;
    int64_t r8_36;
    void*** rcx37;
    int64_t r15_38;
    void*** v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2a20(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2a20(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_26c0();
    fun_2a20(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2850(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_26c0();
    fun_2a20(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2850(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_26c0();
        fun_2a20(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x135e8 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x135e8;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_c0e3() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s29 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_c103(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s29* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s29* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2710();
    } else {
        return;
    }
}

void fun_c1a3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_c246_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_c250_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2710();
    } else {
        return;
    }
    addr_c246_5:
    goto addr_c250_7;
}

void fun_c283() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2850(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_26c0();
    fun_2960(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_26c0();
    fun_2960(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_26c0();
    goto fun_2960;
}

int64_t fun_2630();

void fun_c323(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2630();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_c363(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_28c0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_c383(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_28c0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_c3a3(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_28c0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_c3c3(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2940(rdi, rsi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_c3f3(void** rdi, uint64_t rsi) {
    uint64_t rax3;
    void** rax4;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rsi == 0);
    rax4 = fun_2940(rdi, rsi | rax3);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_c423(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2630();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_c463() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2630();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_c4a3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2630();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_c4d3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2630();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_c523(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2630();
            if (rax5) 
                break;
            addr_c56d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_c56d_5;
        rax8 = fun_2630();
        if (rax8) 
            goto addr_c556_9;
        if (rbx4) 
            goto addr_c56d_5;
        addr_c556_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_c5b3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2630();
            if (rax8) 
                break;
            addr_c5fa_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_c5fa_5;
        rax11 = fun_2630();
        if (rax11) 
            goto addr_c5e2_9;
        if (!rbx6) 
            goto addr_c5e2_9;
        if (r12_4) 
            goto addr_c5fa_5;
        addr_c5e2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_c643(void** rdi, void*** rsi, void** rdx, void** rcx, uint64_t r8) {
    void** r13_6;
    void** rdi7;
    void*** r12_8;
    void** rsi9;
    void** rcx10;
    void** rbx11;
    void** rax12;
    void** rbp13;
    void* rbp14;
    void** rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx10) >> 1) + reinterpret_cast<unsigned char>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void**>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<signed char>(rbx11) <= reinterpret_cast<signed char>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_c6ed_9:
            rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_c700_10:
                *r12_8 = reinterpret_cast<void**>(0);
            }
            addr_c6a0_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_c6c6_12;
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_c714_14;
            if (reinterpret_cast<signed char>(rcx10) <= reinterpret_cast<signed char>(rsi9)) 
                goto addr_c6bd_16;
            if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) 
                goto addr_c714_14;
            addr_c6bd_16:
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_c714_14;
            addr_c6c6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2940(rdi7, rsi9);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_c714_14;
            if (!rbp13) 
                break;
            addr_c714_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp13) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_c6ed_9;
        } else {
            if (!r13_6) 
                goto addr_c700_10;
            goto addr_c6a0_11;
        }
    }
}

int64_t fun_2820();

void fun_c743() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2820();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_c773() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2820();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_c7a3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2820();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_c7c3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2820();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_c7e3(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_28c0(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2880;
    }
}

void fun_c823(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_28c0(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2880;
    }
}

void fun_c863(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_28c0(rsi + 1, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_2880;
    }
}

void fun_c8a3(void** rdi, void** rsi) {
    void** rax3;
    void** rax4;

    __asm__("cli ");
    rax3 = fun_26e0(rdi);
    rax4 = fun_28c0(rax3 + 1, rsi);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_2880;
    }
}

void fun_c8e3() {
    void** rdi1;

    __asm__("cli ");
    fun_26c0();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_2990();
    fun_25d0(rdi1);
}

uint64_t fun_c923() {
    uint64_t rax1;
    void** rax2;

    __asm__("cli ");
    rax1 = fun_27b0();
    if (rax1 || (rax2 = fun_25e0(), !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax2) == 12))) {
        return rax1;
    } else {
        xalloc_die();
    }
}

void xvasprintf();

void fun_c953() {
    signed char al1;
    void** rax2;
    void* rdx3;

    __asm__("cli ");
    if (al1) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax2 = g28;
    xvasprintf();
    rdx3 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx3) {
        fun_2710();
    } else {
        return;
    }
}

struct s30 {
    int32_t f0;
    uint32_t f4;
    int32_t f8;
    int32_t fc;
    int32_t f10;
    int32_t f14;
    signed char[8] pad32;
    uint32_t f20;
    signed char[12] pad48;
    int64_t f30;
};

uint64_t fun_cd13(struct s30* rdi, int64_t rsi, uint64_t* rdx) {
    int64_t rcx4;
    struct s30* v5;
    int64_t v6;
    int64_t rsi7;
    uint64_t* v8;
    void** rax9;
    void** v10;
    int32_t v11;
    uint32_t v12;
    int64_t rdi13;
    int32_t v14;
    int64_t rcx15;
    uint32_t v16;
    int64_t rcx17;
    uint32_t edi18;
    int64_t r15_19;
    int64_t rdx20;
    int64_t rdx21;
    int64_t rdx22;
    uint64_t r12_23;
    int64_t r9_24;
    int64_t rsi25;
    uint64_t rax26;
    int64_t v27;
    int64_t r8_28;
    uint64_t v29;
    int64_t rax30;
    int32_t v31;
    int64_t v32;
    int64_t rcx33;
    uint64_t rax34;
    void* rsp35;
    uint64_t v36;
    uint64_t rbp37;
    struct s9* r13_38;
    uint64_t* r14_39;
    uint64_t v40;
    uint64_t v41;
    int32_t v42;
    uint32_t v43;
    struct s9* rax44;
    int64_t rbx45;
    int32_t v46;
    int64_t v47;
    int64_t rax48;
    int32_t v49;
    int64_t v50;
    int64_t rax51;
    int32_t v52;
    int64_t v53;
    int64_t rax54;
    int32_t v55;
    int64_t v56;
    int32_t v57;
    uint64_t rax58;
    uint64_t r10_59;
    int32_t v60;
    unsigned char dl61;
    uint32_t eax62;
    int32_t v63;
    unsigned char v64;
    uint32_t edi65;
    unsigned char v66;
    int32_t v67;
    int64_t rcx68;
    int32_t v69;
    uint64_t rbp70;
    int64_t r12_71;
    struct s9* v72;
    int64_t r14_73;
    uint64_t* v74;
    void* v75;
    int64_t v76;
    struct s9* v77;
    uint64_t* rax78;
    uint64_t rdx79;
    int64_t rax80;
    int64_t v81;
    void* rax82;
    uint64_t rax83;
    uint64_t v84;
    int32_t v85;
    struct s9* rax86;
    int32_t v87;
    int64_t rax88;
    int32_t v89;
    int64_t v90;
    int64_t rax91;
    int32_t v92;
    int64_t v93;
    int64_t rax94;
    int32_t v95;
    int64_t v96;
    int64_t rax97;
    int32_t v98;
    int64_t v99;
    int32_t v100;
    int64_t rdx101;
    int64_t rax102;
    void** rax103;
    int32_t ebx104;
    int32_t r15d105;
    int32_t r13d106;
    int64_t rax107;
    void** rax108;
    int32_t v109;

    __asm__("cli ");
    rcx4 = rdi->f10;
    v5 = rdi;
    v6 = rsi;
    rsi7 = rdi->fc;
    v8 = rdx;
    rax9 = g28;
    v10 = rax9;
    v11 = rdi->f0;
    v12 = rdi->f4;
    rdi13 = rcx4;
    v14 = rdi->f8;
    rcx15 = rcx4 * 0x2aaaaaab >> 33;
    v16 = rdi->f20;
    *reinterpret_cast<int32_t*>(&rcx17) = *reinterpret_cast<int32_t*>(&rcx15) - (*reinterpret_cast<int32_t*>(&rdi13) >> 31);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx17) + 4) = 0;
    edi18 = *reinterpret_cast<int32_t*>(&rdi13) - (static_cast<uint32_t>(rcx17 + rcx17 * 2) << 2);
    r15_19 = reinterpret_cast<int32_t>(*reinterpret_cast<int32_t*>(&rcx17) - (edi18 >> 31)) + static_cast<int64_t>(rdi->f14);
    *reinterpret_cast<uint32_t*>(&rdx20) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
    if (!(*reinterpret_cast<unsigned char*>(&r15_19) & 3) && (*reinterpret_cast<uint32_t*>(&rdx20) = 1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0, reinterpret_cast<uint64_t>(0x8f5c28f5c28f5c29 * r15_19 + 0x51eb851eb851eb8) <= 0x28f5c28f5c28f5c)) {
        rdx21 = (__intrinsic() + r15_19 >> 6) - (r15_19 >> 63);
        *reinterpret_cast<uint32_t*>(&rdx22) = *reinterpret_cast<uint32_t*>(&rdx21) & 3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx22) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx20) = reinterpret_cast<uint1_t>(rdx22 == 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
    }
    *reinterpret_cast<int32_t*>(&r12_23) = 59;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_23) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r9_24) = 70;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_24) + 4) = 0;
    rsi25 = rsi7 + reinterpret_cast<int32_t>(*reinterpret_cast<uint16_t*>(0x13660 + ((reinterpret_cast<uint32_t>(reinterpret_cast<int32_t>(edi18) >> 31) & 12) + edi18 + (rdx20 + (rdx20 + rdx20 * 2) * 4)) * 2) - 1);
    rax26 = reinterpret_cast<uint64_t>(static_cast<int64_t>(v11));
    v27 = rsi25;
    if (*reinterpret_cast<int32_t*>(&rax26) <= 59) {
        r12_23 = rax26;
    }
    if (*reinterpret_cast<int32_t*>(&r12_23) < 0) {
        r12_23 = 0;
    }
    *reinterpret_cast<int32_t*>(&r8_28) = *reinterpret_cast<int32_t*>(&r12_23);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_28) + 4) = 0;
    v29 = *v8;
    *reinterpret_cast<int32_t*>(&rax30) = -*reinterpret_cast<int32_t*>(&v29);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax30) + 4) = 0;
    v31 = *reinterpret_cast<int32_t*>(&rax30);
    v32 = rax30;
    *reinterpret_cast<uint32_t*>(&rcx33) = v12;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx33) + 4) = 0;
    rax34 = ydhms_diff(r15_19, rsi25, v14, *reinterpret_cast<uint32_t*>(&rcx33), *reinterpret_cast<int32_t*>(&r8_28), 70, 0, 0, 0, *reinterpret_cast<int32_t*>(&v32));
    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x128 - 8 - 8 - 8 - 8 - 8 + 8 + 32);
    v36 = rax34;
    rbp37 = rax34;
    r13_38 = reinterpret_cast<struct s9*>(reinterpret_cast<int64_t>(rsp35) + 0xa0);
    r14_39 = reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rsp35) + 0x88);
    v40 = rax34;
    v41 = rax34;
    v42 = 6;
    v43 = 0;
    while (rax44 = ranged_convert(v6, r14_39, r13_38, rcx33, r8_28, r9_24), rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8), !!rax44) {
        *reinterpret_cast<int32_t*>(&rbx45) = v46;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx45) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r8_28) = *reinterpret_cast<int32_t*>(&r12_23);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_28) + 4) = 0;
        v47 = rbx45;
        *reinterpret_cast<int32_t*>(&rax48) = v49;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax48) + 4) = 0;
        v50 = rax48;
        *reinterpret_cast<int32_t*>(&rax51) = v52;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax51) + 4) = 0;
        v53 = rax51;
        *reinterpret_cast<int32_t*>(&rax54) = v55;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax54) + 4) = 0;
        v56 = rax54;
        *reinterpret_cast<int32_t*>(&r9_24) = v57;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_24) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx33) = v12;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx33) + 4) = 0;
        rax58 = ydhms_diff(r15_19, v27, v14, *reinterpret_cast<uint32_t*>(&rcx33), *reinterpret_cast<int32_t*>(&r8_28), *reinterpret_cast<int32_t*>(&r9_24), *reinterpret_cast<int32_t*>(&v56), *reinterpret_cast<int32_t*>(&v53), *reinterpret_cast<int32_t*>(&v50), *reinterpret_cast<int32_t*>(&v47));
        rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 - 8 - 8 - 8 - 8 + 8 + 32);
        r10_59 = v40;
        if (!rax58) 
            goto addr_d0b0_10;
        if (rbp37 == r10_59 || v41 != r10_59) {
            addr_cf10_12:
            --v42;
            if (!v42) 
                goto addr_d020_13;
        } else {
            if (v60 < 0) 
                goto addr_cf70_15;
            *reinterpret_cast<uint32_t*>(&rcx33) = v16;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx33) + 4) = 0;
            dl61 = reinterpret_cast<uint1_t>(!!v60);
            if (*reinterpret_cast<int32_t*>(&rcx33) < reinterpret_cast<int32_t>(0)) 
                goto addr_d098_17; else 
                goto addr_cf62_18;
        }
        v41 = rbp37;
        rbp37 = r10_59;
        v40 = rax58 + r10_59;
        eax62 = 0;
        *reinterpret_cast<unsigned char*>(&eax62) = reinterpret_cast<uint1_t>(!!v63);
        v43 = eax62;
        continue;
        addr_d098_17:
        if (reinterpret_cast<int32_t>(static_cast<uint32_t>(dl61)) < reinterpret_cast<int32_t>(v43)) 
            goto addr_cf10_12; else 
            goto addr_d0a5_20;
        addr_cf62_18:
        *reinterpret_cast<unsigned char*>(&rcx33) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<uint32_t*>(&rcx33));
        if (*reinterpret_cast<unsigned char*>(&rcx33) == dl61) 
            goto addr_cf10_12; else 
            goto addr_cf69_21;
    }
    goto addr_d02b_22;
    addr_d0b0_10:
    v64 = reinterpret_cast<uint1_t>(v16 == 0);
    edi65 = v64;
    v66 = reinterpret_cast<uint1_t>(v67 == 0);
    *reinterpret_cast<uint32_t*>(&rcx68) = v66;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx68) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rcx68) != *reinterpret_cast<signed char*>(&edi65) && !__intrinsic()) {
        v69 = *reinterpret_cast<int32_t*>(&r12_23);
        rbp70 = r10_59;
        r12_71 = v6;
        v72 = reinterpret_cast<struct s9*>(reinterpret_cast<int64_t>(rsp35) + 0xe0);
        *reinterpret_cast<int32_t*>(&r14_73) = 0x92c70;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_73) + 4) = 0;
        v74 = reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rsp35) + 0x90);
        v75 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) + 0x98);
        v76 = r15_19;
        v77 = r13_38;
        goto addr_d144_24;
    }
    addr_cf70_15:
    rax78 = v8;
    *rax78 = r10_59 - (v31 + v36);
    if (v11 == *reinterpret_cast<int32_t*>(&rbx45)) 
        goto addr_cfdf_25;
    *reinterpret_cast<unsigned char*>(&rax78) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbx45) == 60);
    *reinterpret_cast<int32_t*>(&rdx79) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx79) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rdx79) = reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(v11 < 0) | reinterpret_cast<uint1_t>(v11 == 0));
    if (!__intrinsic()) {
        rax80 = reinterpret_cast<int64_t>(v6(reinterpret_cast<int64_t>(rsp35) + 0xe0, r13_38));
        r10_59 = v11 + ((rdx79 & reinterpret_cast<uint64_t>(rax78)) - r12_23) + r10_59;
        if (!rax80) {
            addr_d02b_22:
            r10_59 = 0xffffffffffffffff;
        } else {
            addr_cfdf_25:
            __asm__("movdqa xmm0, [rsp+0xa0]");
            __asm__("movdqa xmm1, [rsp+0xb0]");
            __asm__("movdqa xmm2, [rsp+0xc0]");
            __asm__("movups [rcx], xmm0");
            v5->f30 = v81;
            __asm__("movups [rcx+0x10], xmm1");
            __asm__("movups [rcx+0x20], xmm2");
        }
        rax82 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
        if (rax82) {
            fun_2710();
        } else {
            return r10_59;
        }
    }
    addr_d0a5_20:
    goto addr_cf70_15;
    addr_cf69_21:
    goto addr_cf70_15;
    addr_d2a2_31:
    r12_23 = reinterpret_cast<uint64_t>(static_cast<int64_t>(v69));
    r13_38 = v77;
    r10_59 = rax83 + v84;
    *reinterpret_cast<int32_t*>(&rbx45) = v85;
    goto addr_cf70_15;
    addr_d23b_32:
    goto addr_d02b_22;
    while (rax86 = ranged_convert(r12_71, v74, v72, rcx68, r8_28, r9_24), rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8), !!rax86) {
        if (v64 == static_cast<unsigned char>(reinterpret_cast<uint1_t>(v87 == 0)) || v87 < 0) {
            *reinterpret_cast<int32_t*>(&rax88) = v89;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax88) + 4) = 0;
            v90 = rax88;
            *reinterpret_cast<int32_t*>(&rax91) = v92;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax91) + 4) = 0;
            v93 = rax91;
            *reinterpret_cast<int32_t*>(&rax94) = v95;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax94) + 4) = 0;
            v96 = rax94;
            *reinterpret_cast<int32_t*>(&rax97) = v98;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax97) + 4) = 0;
            v99 = rax97;
            *reinterpret_cast<int32_t*>(&r9_24) = v100;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_24) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r8_28) = v69;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_28) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx68) = v12;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx68) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx101) = v14;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx101) + 4) = 0;
            rax83 = ydhms_diff(v76, v27, *reinterpret_cast<int32_t*>(&rdx101), *reinterpret_cast<uint32_t*>(&rcx68), *reinterpret_cast<int32_t*>(&r8_28), *reinterpret_cast<int32_t*>(&r9_24), *reinterpret_cast<int32_t*>(&v99), *reinterpret_cast<int32_t*>(&v96), *reinterpret_cast<int32_t*>(&v93), *reinterpret_cast<int32_t*>(&v90));
            rax102 = reinterpret_cast<int64_t>(r12_71(v75, v77, rdx101, rcx68, r8_28, r9_24));
            rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 - 8 - 8 - 8 - 8 + 8 + 32 - 8 + 8);
            if (rax102) 
                goto addr_d2a2_31;
            rax103 = fun_25e0();
            rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
            if (*reinterpret_cast<void***>(rax103) != 75) 
                goto addr_d23b_32;
        }
        while (1) {
            do {
                ebx104 = ebx104 + r15d105;
                if (r13d106 == 1) 
                    break;
                r13d106 = 1;
                v84 = ebx104 + rbp70;
            } while (__intrinsic());
            break;
            *reinterpret_cast<int32_t*>(&r14_73) = *reinterpret_cast<int32_t*>(&r14_73) + 0x92c70;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_73) + 4) = 0;
            if (*reinterpret_cast<int32_t*>(&r14_73) == 0xdb04f20) 
                goto addr_d240_40;
            addr_d144_24:
            r15d105 = static_cast<int32_t>(r14_73 + r14_73);
            r13d106 = 2;
            ebx104 = -*reinterpret_cast<int32_t*>(&r14_73);
            v84 = ebx104 + rbp70;
            if (!__intrinsic()) 
                break;
        }
    }
    goto addr_d02b_22;
    addr_d240_40:
    r13_38 = v77;
    r12_23 = reinterpret_cast<uint64_t>(static_cast<int64_t>(v69));
    rax107 = reinterpret_cast<int64_t>(v6(v72, r13_38));
    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
    if (!rax107) {
        addr_d020_13:
        rax108 = fun_25e0();
        *reinterpret_cast<void***>(rax108) = reinterpret_cast<void**>(75);
        goto addr_d02b_22;
    } else {
        *reinterpret_cast<int32_t*>(&rbx45) = v109;
        r10_59 = rbp70 + static_cast<int64_t>(reinterpret_cast<int32_t>((v64 - v66) * reinterpret_cast<uint32_t>("cur_max")));
        goto addr_cf70_15;
    }
}

void fun_d2c3(int64_t rdi, void** rsi, void** rdx) {
    __asm__("cli ");
    fun_2890(rdi, rsi, rdx);
    goto 0xcd10;
}

struct s31 {
    unsigned char f0;
    unsigned char f1;
};

struct s31* fun_d2f3(struct s31* rdi) {
    uint32_t edx2;
    struct s31* rax3;
    struct s31* rcx4;
    int32_t esi5;

    __asm__("cli ");
    edx2 = rdi->f0;
    rax3 = rdi;
    if (*reinterpret_cast<signed char*>(&edx2) == 47) {
        do {
            edx2 = rax3->f1;
            rax3 = reinterpret_cast<struct s31*>(&rax3->f1);
        } while (*reinterpret_cast<signed char*>(&edx2) == 47);
    }
    if (*reinterpret_cast<signed char*>(&edx2)) {
        rcx4 = rax3;
        esi5 = 0;
        while (1) {
            if (*reinterpret_cast<signed char*>(&edx2) != 47) {
                if (*reinterpret_cast<signed char*>(&esi5)) {
                    rax3 = rcx4;
                    esi5 = 0;
                }
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s31*>(&rcx4->f1);
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            } else {
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s31*>(&rcx4->f1);
                esi5 = 1;
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            }
        }
    }
    return rax3;
}

void fun_d353(void** rdi) {
    void** rbx2;
    void** rax3;

    __asm__("cli ");
    rbx2 = rdi;
    rax3 = fun_26e0(rdi);
    while (reinterpret_cast<unsigned char>(rax3) > reinterpret_cast<unsigned char>(1) && *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx2) + reinterpret_cast<unsigned char>(rax3) + 0xffffffffffffffff) == 47) {
        --rax3;
    }
    return;
}

int32_t fun_26f0(int64_t rdi, void** rsi, void** rdx);

void** fun_29a0(void** rdi, int64_t rsi, int64_t rdx);

int64_t fun_d3b3(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    int32_t eax7;
    int32_t r12d8;
    void** rax9;
    int1_t zf10;
    void** v11;
    int64_t rax12;
    void** rax13;
    void** rbx14;
    void** rsi15;
    void* rax16;
    void* r15_17;
    struct s1* rax18;
    int32_t eax19;
    int32_t r13d20;
    void** r14_21;
    void* rax22;
    void** r14_23;
    int32_t eax24;
    void** rbp25;
    void** rax26;
    int64_t rdi27;
    int32_t eax28;
    int64_t rdi29;
    int32_t eax30;
    void* rax31;
    int64_t rdi32;
    int32_t eax33;
    int32_t eax34;
    int64_t rdi35;
    int32_t eax36;
    int32_t eax37;
    int64_t rdi38;
    int32_t eax39;
    int32_t eax40;

    __asm__("cli ");
    eax7 = fun_2700(rdi, rsi, rdx);
    r12d8 = eax7;
    if (!eax7 || (rax9 = fun_25e0(), zf10 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax9) == 36), v11 = rax9, !zf10)) {
        addr_d520_2:
        *reinterpret_cast<int32_t*>(&rax12) = r12d8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
        return rax12;
    } else {
        rax13 = fun_26e0(rdi);
        rbx14 = rax13;
        if (!rax13) {
            fun_2790("0 < len", "lib/chdir-long.c", 0x7e, "chdir_long");
            goto addr_d6f2_5;
        }
        if (reinterpret_cast<unsigned char>(rax13) <= reinterpret_cast<unsigned char>("chr")) {
            addr_d6f2_5:
            fun_2790("4096 <= len", "lib/chdir-long.c", 0x7f, "chdir_long");
            goto addr_d711_7;
        } else {
            rsi15 = reinterpret_cast<void**>("/");
            rax16 = fun_27e0(rdi, "/", rdx, rcx, r8, r9);
            r15_17 = rax16;
            if (rax16 == 2) {
                rax18 = fun_27f0(rdi + 3, 47, rbx14 + 0xfffffffffffffffd);
                if (!rax18) {
                    r12d8 = -1;
                    goto addr_d520_2;
                } else {
                    rax18->f0 = 0;
                    *reinterpret_cast<int32_t*>(&rdx) = 0x10900;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                    eax19 = fun_26f0(0xffffff9c, rdi, 0x10900);
                    rax18->f0 = 47;
                    r13d20 = eax19;
                    if (eax19 < 0) {
                        addr_d6a0_12:
                        rbx14 = *reinterpret_cast<void***>(v11);
                    } else {
                        r14_21 = reinterpret_cast<void**>(&rax18->f1);
                        rsi15 = reinterpret_cast<void**>("/");
                        rax22 = fun_27e0(r14_21, "/", 0x10900, rcx, r8, r9);
                        r14_23 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_21) + reinterpret_cast<uint64_t>(rax22));
                        goto addr_d439_14;
                    }
                }
            } else {
                r14_23 = rdi;
                r13d20 = -100;
                if (rax16) {
                    *reinterpret_cast<int32_t*>(&rdx) = 0x10900;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                    rsi15 = reinterpret_cast<void**>("/");
                    eax24 = fun_26f0(0xffffff9c, "/", 0x10900);
                    r13d20 = eax24;
                    if (eax24 < 0) 
                        goto addr_d6a0_12;
                    r14_23 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<uint64_t>(r15_17));
                    goto addr_d439_14;
                }
            }
        }
    }
    addr_d513_18:
    r12d8 = -1;
    *reinterpret_cast<void***>(v11) = rbx14;
    goto addr_d520_2;
    addr_d439_14:
    if (*reinterpret_cast<void***>(r14_23) == 47) {
        addr_d711_7:
        fun_2790("*dir != '/'", "lib/chdir-long.c", 0xa2, "chdir_long");
        goto addr_d730_19;
    } else {
        rbp25 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(rbx14));
        if (reinterpret_cast<unsigned char>(r14_23) > reinterpret_cast<unsigned char>(rbp25)) {
            addr_d730_19:
            fun_2790("dir <= dir_end", "lib/chdir-long.c", 0xa3, "chdir_long");
        } else {
            if (reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(rbp25) - reinterpret_cast<unsigned char>(r14_23)) > reinterpret_cast<int64_t>("chr")) {
                while (rax26 = fun_29a0(r14_23, 47, "hr"), rbx14 = rax26, !!rax26) {
                    *reinterpret_cast<void***>(rax26) = reinterpret_cast<void**>(0);
                    if (reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(rax26) - reinterpret_cast<unsigned char>(r14_23)) > reinterpret_cast<int64_t>("chr")) 
                        goto addr_d6ac_24;
                    *reinterpret_cast<int32_t*>(&rdx) = 0x10900;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                    rsi15 = r14_23;
                    *reinterpret_cast<int32_t*>(&rdi27) = r13d20;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi27) + 4) = 0;
                    eax28 = fun_26f0(rdi27, rsi15, 0x10900);
                    *reinterpret_cast<int32_t*>(&r15_17) = eax28;
                    if (eax28 < 0) 
                        goto addr_d500_26;
                    if (r13d20 < 0) 
                        goto addr_d468_28;
                    *reinterpret_cast<int32_t*>(&rdi29) = r13d20;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi29) + 4) = 0;
                    eax30 = fun_27d0();
                    if (eax30) 
                        goto addr_d4f1_30;
                    addr_d468_28:
                    *reinterpret_cast<void***>(rbx14) = reinterpret_cast<void**>(47);
                    ++rbx14;
                    rsi15 = reinterpret_cast<void**>("/");
                    rax31 = fun_27e0(rbx14, "/", 0x10900, rcx, r8, r9);
                    r14_23 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx14) + reinterpret_cast<uint64_t>(rax31));
                    if (reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(rbp25) - reinterpret_cast<unsigned char>(r14_23)) <= reinterpret_cast<int64_t>("chr")) 
                        goto addr_d5d0_31;
                    r13d20 = *reinterpret_cast<int32_t*>(&r15_17);
                }
            } else {
                *reinterpret_cast<int32_t*>(&r15_17) = r13d20;
                goto addr_d5d0_31;
            }
        }
    }
    r12d8 = -1;
    *reinterpret_cast<void***>(v11) = reinterpret_cast<void**>(36);
    goto addr_d520_2;
    addr_d6ac_24:
    rcx = reinterpret_cast<void**>("chdir_long");
    *reinterpret_cast<int32_t*>(&rdx) = 0xb3;
    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
    rsi15 = reinterpret_cast<void**>("lib/chdir-long.c");
    fun_2790("slash - dir < 4096", "lib/chdir-long.c", 0xb3, "chdir_long");
    goto addr_d6cb_36;
    addr_d5d0_31:
    if (reinterpret_cast<unsigned char>(rbp25) <= reinterpret_cast<unsigned char>(r14_23)) {
        *reinterpret_cast<int32_t*>(&rdi32) = *reinterpret_cast<int32_t*>(&r15_17);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi32) + 4) = 0;
        r13d20 = *reinterpret_cast<int32_t*>(&r15_17);
        eax33 = fun_2930(rdi32, "/", rdx);
        if (eax33) {
            addr_d503_38:
            while (rbx14 = *reinterpret_cast<void***>(v11), *reinterpret_cast<int32_t*>(&rbx14 + 4) = 0, r13d20 >= 0) {
                addr_d667_39:
                *reinterpret_cast<int32_t*>(&rdi29) = r13d20;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi29) + 4) = 0;
                eax34 = fun_27d0();
                if (!eax34) 
                    break;
                addr_d4f1_30:
                cdb_free_part_0(rdi29, rsi15, rdx, rcx);
                addr_d500_26:
                *reinterpret_cast<void***>(rbx14) = reinterpret_cast<void**>(47);
            }
            goto addr_d513_18;
        } else {
            if (*reinterpret_cast<int32_t*>(&r15_17) < 0) {
                addr_d617_42:
                r12d8 = 0;
                goto addr_d520_2;
            }
        }
    } else {
        *reinterpret_cast<int32_t*>(&rdx) = 0x10900;
        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
        rsi15 = r14_23;
        *reinterpret_cast<int32_t*>(&rdi35) = *reinterpret_cast<int32_t*>(&r15_17);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi35) + 4) = 0;
        eax36 = fun_26f0(rdi35, rsi15, 0x10900);
        r13d20 = eax36;
        if (eax36 < 0) {
            addr_d6cb_36:
            r13d20 = *reinterpret_cast<int32_t*>(&r15_17);
            goto addr_d503_38;
        } else {
            if (*reinterpret_cast<int32_t*>(&r15_17) < 0 || (*reinterpret_cast<int32_t*>(&rdi29) = *reinterpret_cast<int32_t*>(&r15_17), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi29) + 4) = 0, eax37 = fun_27d0(), eax37 == 0)) {
                *reinterpret_cast<int32_t*>(&rdi38) = r13d20;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi38) + 4) = 0;
                eax39 = fun_2930(rdi38, rsi15, 0x10900);
                if (eax39) {
                    rbx14 = *reinterpret_cast<void***>(v11);
                    *reinterpret_cast<int32_t*>(&rbx14 + 4) = 0;
                    goto addr_d667_39;
                }
            } else {
                goto addr_d4f1_30;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rdi29) = r13d20;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi29) + 4) = 0;
    eax40 = fun_27d0();
    if (eax40) 
        goto addr_d4f1_30; else 
        goto addr_d617_42;
}

int64_t fun_2620();

int64_t fun_d753(void** rdi, void* rsi, void** rdx, void** rcx, void** r8) {
    int64_t rax6;
    uint32_t ebx7;
    int64_t rax8;
    void** rax9;
    void** rax10;

    __asm__("cli ");
    rax6 = fun_2620();
    ebx7 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 32;
    rax8 = rpl_fclose(rdi, rsi, rdx, rcx, r8);
    if (ebx7) {
        if (*reinterpret_cast<int32_t*>(&rax8)) {
            addr_d7ae_3:
            *reinterpret_cast<int32_t*>(&rax8) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        } else {
            rax9 = fun_25e0();
            *reinterpret_cast<void***>(rax9) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax8) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax8)) {
            if (rax6) 
                goto addr_d7ae_3;
            rax10 = fun_25e0();
            *reinterpret_cast<int32_t*>(&rax8) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax10) == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        }
    }
    return rax8;
}

struct s32 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_28a0(struct s32* rdi);

int32_t fun_2920(struct s32* rdi);

int64_t fun_2780(int64_t rdi, ...);

int32_t rpl_fflush(struct s32* rdi);

int64_t fun_2690(struct s32* rdi);

int64_t fun_d7c3(struct s32* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    void** rax8;
    void** r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_28a0(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2920(rdi);
        if (!(eax3 && (eax4 = fun_28a0(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2780(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_25e0();
            r12d9 = *reinterpret_cast<void***>(rax8);
            rax10 = fun_2690(rdi);
            if (r12d9) {
                *reinterpret_cast<void***>(rax8) = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2690;
}

int32_t fun_29b0();

void fd_safer(int64_t rdi);

void fun_d853() {
    void** rax1;
    unsigned char sil2;
    int32_t eax3;
    int64_t rdi4;
    void* rdx5;

    __asm__("cli ");
    rax1 = g28;
    if (sil2 & 64) {
    }
    eax3 = fun_29b0();
    *reinterpret_cast<int32_t*>(&rdi4) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
    fd_safer(rdi4);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2710();
    } else {
        return;
    }
}

void rpl_fseeko(struct s32* rdi);

void fun_d8d3(struct s32* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2920(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_d923(struct s32* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_28a0(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2780(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_2900(int64_t rdi);

signed char* fun_d9a3() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2900(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2730(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_d9e3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2730(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2710();
    } else {
        return r12_7;
    }
}

int32_t dup_safer();

int64_t fun_da73(uint32_t edi) {
    int32_t eax2;
    void** rax3;
    void** r13d4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    if (edi <= 2) {
        eax2 = dup_safer();
        rax3 = fun_25e0();
        r13d4 = *reinterpret_cast<void***>(rax3);
        fun_27d0();
        *reinterpret_cast<int32_t*>(&rax5) = eax2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        *reinterpret_cast<void***>(rax3) = r13d4;
        return rax5;
    } else {
        *reinterpret_cast<uint32_t*>(&rax6) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    }
}

int32_t rpl_vasprintf(void*** rdi, void** rsi);

struct s33 {
    signed char[1] pad1;
    void** f1;
};

void** fun_dc33(void** rdi, void** rsi) {
    void*** rsp3;
    void** r8_4;
    void** rdx5;
    void** rax6;
    void** v7;
    uint32_t eax8;
    int64_t rdi9;
    int32_t eax10;
    void** rax11;
    void** v12;
    void** rax13;
    void* rdx14;
    void* rax15;
    void** rsi16;
    void** rax17;
    void** v18;
    void** r15_19;
    void** rcx20;
    int64_t rcx21;
    void** rax22;
    void** rbp23;
    void** r14_24;
    void* rax25;
    int64_t r13_26;
    void** r12_27;
    int64_t rbp28;
    struct s33* rbx29;
    uint32_t v30;
    void* rdx31;
    void** rdx32;
    void** v33;
    void** rdi34;
    void** rax35;
    struct s33* tmp64_36;
    int1_t cf37;
    void** rax38;
    void** rdi39;
    void** rcx40;
    int64_t rcx41;
    void** rax42;
    void* rdx43;
    void** rdx44;
    void** r15_45;
    void** rax46;

    __asm__("cli ");
    rsp3 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 24);
    r8_4 = rdi;
    rdx5 = rsi;
    rax6 = g28;
    v7 = rax6;
    eax8 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi));
    *reinterpret_cast<int32_t*>(&rdi9) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi9) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&eax8)) {
        do {
            if (*reinterpret_cast<signed char*>(&eax8) != 37) 
                break;
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r8_4) + reinterpret_cast<uint64_t>(rdi9 * 2) + 1) != 0x73) 
                break;
            ++rdi9;
            eax8 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r8_4) + reinterpret_cast<uint64_t>(rdi9 * 2)));
        } while (*reinterpret_cast<signed char*>(&eax8));
        goto addr_dcc0_5;
    } else {
        goto addr_dcc0_5;
    }
    eax10 = rpl_vasprintf(rsp3, r8_4);
    rax11 = v12;
    if (eax10 < 0) {
        rax13 = fun_25e0();
        if (*reinterpret_cast<void***>(rax13) == 12) {
            xalloc_die();
            goto addr_dce1_10;
        } else {
            *reinterpret_cast<int32_t*>(&rax11) = 0;
            *reinterpret_cast<int32_t*>(&rax11 + 4) = 0;
            goto addr_dc90_12;
        }
    } else {
        addr_dc90_12:
        rdx14 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
        if (!rdx14) {
            return rax11;
        }
    }
    addr_dcc0_5:
    rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
    if (rax15) {
        addr_dce1_10:
        fun_2710();
    } else {
        rsi16 = rdx5;
        rax17 = g28;
        v18 = rax17;
        __asm__("movdqu xmm0, [rsi]");
        __asm__("movups [rsp], xmm0");
        r15_19 = *reinterpret_cast<void***>(rsi16 + 16);
        if (rdi9) 
            goto addr_db08_16;
    }
    rax22 = xmalloc(1, rsi16, rdx5, rcx20, r8_4, 1, rsi16, rdx5, rcx21, r8_4);
    rbp23 = rax22;
    r14_24 = rax22;
    addr_dbe0_18:
    *reinterpret_cast<void***>(rbp23) = reinterpret_cast<void**>(0);
    addr_dbe4_19:
    rax25 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v18) - reinterpret_cast<unsigned char>(g28));
    if (rax25) {
        fun_2710();
    } else {
        return r14_24;
    }
    addr_db08_16:
    r13_26 = rdi9;
    r12_27 = rsi16;
    rbp28 = rdi9;
    *reinterpret_cast<int32_t*>(&rbx29) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx29) + 4) = 0;
    do {
        if (v30 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx31) = v30;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx31) + 4) = 0;
            v30 = v30 + 8;
            rdx32 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rdx31) + reinterpret_cast<unsigned char>(r15_19));
        } else {
            rdx32 = v33;
            v33 = rdx32 + 8;
        }
        rdi34 = *reinterpret_cast<void***>(rdx32);
        rax35 = fun_26e0(rdi34);
        tmp64_36 = reinterpret_cast<struct s33*>(reinterpret_cast<uint64_t>(rbx29) + reinterpret_cast<unsigned char>(rax35));
        cf37 = reinterpret_cast<uint64_t>(tmp64_36) < reinterpret_cast<uint64_t>(rbx29);
        rbx29 = tmp64_36;
        if (cf37) {
            rbx29 = reinterpret_cast<struct s33*>(0xffffffffffffffff);
        }
        --rbp28;
    } while (rbp28);
    if (reinterpret_cast<uint64_t>(rbx29) <= 0x7fffffff) 
        goto addr_db6d_29;
    rax38 = fun_25e0();
    *reinterpret_cast<int32_t*>(&r14_24) = 0;
    *reinterpret_cast<int32_t*>(&r14_24 + 4) = 0;
    *reinterpret_cast<void***>(rax38) = reinterpret_cast<void**>(75);
    goto addr_dbe4_19;
    addr_db6d_29:
    rdi39 = reinterpret_cast<void**>(&rbx29->f1);
    rax42 = xmalloc(rdi39, rsi16, rdx32, rcx40, r8_4, rdi39, rsi16, rdx32, rcx41, r8_4);
    r14_24 = rax42;
    rbp23 = rax42;
    do {
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_27)) <= reinterpret_cast<unsigned char>(47)) {
            *reinterpret_cast<void***>(&rdx43) = *reinterpret_cast<void***>(r12_27);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx43) + 4) = 0;
            rdx44 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rdx43) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_27 + 16)));
            *reinterpret_cast<void***>(r12_27) = *reinterpret_cast<void***>(r12_27) + 8;
        } else {
            rdx44 = *reinterpret_cast<void***>(r12_27 + 8);
            *reinterpret_cast<void***>(r12_27 + 8) = rdx44 + 8;
        }
        r15_45 = *reinterpret_cast<void***>(rdx44);
        rax46 = fun_26e0(r15_45, r15_45);
        fun_2880(rbp23, r15_45, rax46);
        rbp23 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp23) + reinterpret_cast<unsigned char>(rax46));
        --r13_26;
    } while (r13_26);
    goto addr_dbe0_18;
}

void** vasnprintf();

uint64_t fun_dcf3(void*** rdi, int64_t rsi, int64_t rdx) {
    void** rax4;
    void** rax5;
    uint64_t rax6;
    uint64_t v7;
    void** rax8;
    void* rdx9;

    __asm__("cli ");
    rax4 = g28;
    rax5 = vasnprintf();
    if (!rax5) {
        *reinterpret_cast<int32_t*>(&rax6) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    } else {
        rax6 = v7;
        if (rax6 > 0x7fffffff) {
            fun_25b0(rax5, rax5);
            rax8 = fun_25e0();
            *reinterpret_cast<void***>(rax8) = reinterpret_cast<void**>(75);
            *reinterpret_cast<int32_t*>(&rax6) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        } else {
            *rdi = rax5;
        }
    }
    rdx9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx9) {
        fun_2710();
    } else {
        return rax6;
    }
}

int32_t setlocale_null_r();

int64_t fun_dd73() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2710();
    } else {
        return rax3;
    }
}

int64_t fun_ddf3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2950(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax6 = fun_26e0(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2880(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2880(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_dea3() {
    __asm__("cli ");
    goto fun_2950;
}

void fun_deb3() {
    __asm__("cli ");
}

int32_t printf_parse(void** rdi, void* rsi, void** rdx);

int32_t printf_fetchargs(void** rdi, void** rsi, void** rdx);

struct s34 {
    signed char[7] pad7;
    void** f7;
};

struct s35 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int32_t f10;
    signed char[4] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
    signed char[7] pad40;
    int64_t f28;
    void** f30;
    signed char[7] pad56;
    void** f38;
    signed char[7] pad64;
    int64_t f40;
    unsigned char f48;
    signed char[7] pad80;
    int64_t f50;
    void** f58;
};

struct s36 {
    uint32_t f0;
    signed char[12] pad16;
    int32_t f10;
};

void** fun_ded3(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void* rsp5;
    void* rbp6;
    void** r14_7;
    void** r13_8;
    void** r12_9;
    void** v10;
    void*** v11;
    void** rax12;
    void** v13;
    int32_t eax14;
    void* rsp15;
    void** r10_16;
    void* rax17;
    int64_t* rsp18;
    void** rbx19;
    int64_t* rsp20;
    void** rax21;
    void** v22;
    int64_t* rsp23;
    void** v24;
    int64_t* rsp25;
    void** v26;
    int64_t* rsp27;
    void** v28;
    int64_t* rsp29;
    void** rax30;
    void** r15_31;
    void** v32;
    int64_t* rsp33;
    int64_t* rsp34;
    void** v35;
    int64_t* rsp36;
    void** v37;
    int64_t* rsp38;
    void** rsi39;
    int32_t eax40;
    void** rax41;
    void** rax42;
    struct s34* v43;
    void** tmp64_44;
    void* v45;
    void** r8_46;
    void** tmp64_47;
    uint1_t cf48;
    void* rax49;
    void* rcx50;
    uint64_t rdx51;
    void* rdx52;
    void** v53;
    void** rax54;
    void** rax55;
    struct s35* r14_56;
    struct s35* v57;
    void** r9_58;
    void** r8_59;
    int64_t v60;
    int64_t v61;
    uint32_t edx62;
    void** tmp64_63;
    void** r10_64;
    int64_t* rsp65;
    int64_t* rsp66;
    void** rax67;
    int64_t* rsp68;
    void** rax69;
    int64_t* rsp70;
    void** rax71;
    int64_t* rsp72;
    void** rax73;
    int64_t* rsp74;
    void** rax75;
    int64_t* rsp76;
    void** rax77;
    void** tmp64_78;
    int64_t* rsp79;
    void** rax80;
    int64_t* rsp81;
    void** rax82;
    int64_t* rsp83;
    void** rax84;
    uint32_t ecx85;
    uint32_t* v86;
    int64_t r13_87;
    int32_t eax88;
    void** rsi89;
    void** rax90;
    int64_t* rsp91;
    void** rsi92;
    void** rax93;
    int64_t* rsp94;
    int64_t rax95;
    uint32_t eax96;
    int32_t v97;
    struct s36* rcx98;
    int64_t rax99;
    void** tmp64_100;
    void** r15_101;
    void** rax102;
    int64_t rax103;
    int64_t* rsp104;
    void** rax105;
    int64_t* rsp106;
    void** rax107;
    int64_t* rsp108;
    int64_t* rsp109;
    void** rax110;
    int64_t* rsp111;
    void** rax112;

    __asm__("cli ");
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp6 = rsp5;
    r14_7 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc50);
    r13_8 = rdx;
    r12_9 = rcx;
    v10 = rdi;
    v11 = rsi;
    rax12 = g28;
    v13 = rax12;
    eax14 = printf_parse(r13_8, reinterpret_cast<int64_t>(rbp6) - 0x2c0, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 - 8 - 8 - 8 - 8 - 0x3f8 - 8 + 8);
    if (eax14 < 0) {
        while (1) {
            *reinterpret_cast<int32_t*>(&r10_16) = 0;
            *reinterpret_cast<int32_t*>(&r10_16 + 4) = 0;
            while (rax17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v13) - reinterpret_cast<unsigned char>(g28)), !!rax17) {
                rsp18 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp18 = 0xee0a;
                fun_2710();
                rsp15 = reinterpret_cast<void*>(rsp18 + 1);
                addr_ee0a_5:
                if (rbx19 != 0xffffffffffffffff) 
                    goto addr_ee14_6;
                addr_ecfe_7:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 0;
                if (reinterpret_cast<unsigned char>(rbx19) > reinterpret_cast<unsigned char>(r13_8) && (r10_16 != v10 && (rsp20 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8), *rsp20 = 0xed23, rax21 = fun_2940(r10_16, r13_8, r10_16, r13_8), rsp15 = reinterpret_cast<void*>(rsp20 + 1), r10_16 = r10_16, !!rax21))) {
                    r10_16 = rax21;
                }
                if (v22) {
                    rsp23 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp23 = 0xed49;
                    fun_25b0(v22, v22);
                    rsp15 = reinterpret_cast<void*>(rsp23 + 1);
                    r10_16 = r10_16;
                }
                if (v24 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                    rsp25 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp25 = 0xed6f;
                    fun_25b0(v24, v24);
                    rsp15 = reinterpret_cast<void*>(rsp25 + 1);
                    r10_16 = r10_16;
                }
                if (v26 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) {
                    rsp27 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp27 = 0xed95;
                    fun_25b0(v26, v26);
                    rsp15 = reinterpret_cast<void*>(rsp27 + 1);
                    r10_16 = r10_16;
                }
                *v11 = r12_9;
            }
            break;
            addr_ee14_6:
            addr_e9f8_16:
            v28 = r10_16;
            addr_e9ff_17:
            rsp29 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp29 = 0xea04;
            rax30 = fun_25e0();
            rsp15 = reinterpret_cast<void*>(rsp29 + 1);
            r15_31 = v28;
            v32 = rax30;
            addr_ea12_18:
            *reinterpret_cast<void***>(v32) = reinterpret_cast<void**>(12);
            if (r15_31 != v10) {
                rsp33 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp33 = 0xe5b1;
                fun_25b0(r15_31, r15_31);
                rsp15 = reinterpret_cast<void*>(rsp33 + 1);
            }
            if (v22) {
                rsp34 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp34 = 0xe5c9;
                fun_25b0(v22, v22);
                rsp15 = reinterpret_cast<void*>(rsp34 + 1);
            }
            addr_e208_23:
            if (v35 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                rsp36 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp36 = 0xe220;
                fun_25b0(v35, v35);
                rsp15 = reinterpret_cast<void*>(rsp36 + 1);
            }
            if (v37 == reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) 
                continue;
            rsp38 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp38 = 0xe238;
            fun_25b0(v37, v37);
            rsp15 = reinterpret_cast<void*>(rsp38 + 1);
        }
        return r10_16;
    }
    rsi39 = r14_7;
    eax40 = printf_fetchargs(r12_9, rsi39, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
    if (eax40 < 0) {
        rax41 = fun_25e0();
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
        *reinterpret_cast<void***>(rax41) = reinterpret_cast<void**>(22);
        goto addr_e208_23;
    }
    rax42 = reinterpret_cast<void**>(&v43->f7);
    if (reinterpret_cast<uint64_t>(v43) >= 0xfffffffffffffff9) {
        rax42 = reinterpret_cast<void**>(0xffffffffffffffff);
    }
    tmp64_44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax42) + reinterpret_cast<uint64_t>(v45));
    if (reinterpret_cast<unsigned char>(tmp64_44) < reinterpret_cast<unsigned char>(rax42)) 
        goto addr_e1fd_33;
    *reinterpret_cast<int32_t*>(&r8_46) = 0;
    *reinterpret_cast<int32_t*>(&r8_46 + 4) = 0;
    tmp64_47 = tmp64_44 + 6;
    cf48 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_47) < reinterpret_cast<unsigned char>(tmp64_44));
    *reinterpret_cast<unsigned char*>(&r8_46) = cf48;
    if (cf48) 
        goto addr_e1fd_33;
    if (reinterpret_cast<unsigned char>(tmp64_47) <= reinterpret_cast<unsigned char>("dmntent")) {
        rax49 = reinterpret_cast<void*>(tmp64_44 + 29);
        rcx50 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - (reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffff000));
        rdx51 = reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffffff0;
        if (rsp15 != rcx50) {
            do {
                rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - reinterpret_cast<int64_t>("hr"));
            } while (rsp15 != rcx50);
        }
        *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<uint32_t*>(&rdx51) & reinterpret_cast<uint32_t>("chr");
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0;
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - reinterpret_cast<int64_t>(rdx52));
        if (rdx52) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8);
        }
        v22 = reinterpret_cast<void**>(0);
        v53 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp15) + 15 & 0xfffffffffffffff0);
    } else {
        if (tmp64_47 == 0xffffffffffffffff || (rax54 = fun_28c0(tmp64_47, rsi39), rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8), v53 = rax54, rax54 == 0)) {
            addr_e1fd_33:
            rax55 = fun_25e0();
            rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
            *reinterpret_cast<void***>(rax55) = reinterpret_cast<void**>(12);
            goto addr_e208_23;
        } else {
            v22 = rax54;
            r8_46 = r8_46;
        }
    }
    *reinterpret_cast<int32_t*>(&rbx19) = 0;
    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
    if (v10) {
        rbx19 = *v11;
    }
    r14_56 = v57;
    r9_58 = r8_46;
    r8_59 = r13_8;
    v60 = 0;
    r15_31 = v10;
    r13_8 = r14_56->f0;
    if (r13_8 != r8_59) 
        goto addr_dffc_46;
    while (1) {
        addr_e954_47:
        r12_9 = r9_58;
        r10_16 = r15_31;
        while (v61 != v60) {
            edx62 = r14_56->f48;
            if (*reinterpret_cast<signed char*>(&edx62) != 37) 
                goto addr_e0bf_50;
            if (r14_56->f50 != -1) 
                goto 0x2add;
            r9_58 = reinterpret_cast<void**>(0xffffffffffffffff);
            if (reinterpret_cast<unsigned char>(r12_9) < reinterpret_cast<unsigned char>(0xffffffffffffffff)) {
                r9_58 = r12_9 + 1;
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r9_58)) {
                addr_e92f_55:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 37;
                r15_31 = r10_16;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_e9f8_16;
                    rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r9_58)) {
                    rbx19 = r9_58;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_e9f8_16;
                if (r10_16 == v10) 
                    goto addr_ec44_64; else 
                    goto addr_e903_65;
            }
            r8_59 = r14_56->f8;
            r13_8 = r14_56->f58;
            r14_56 = reinterpret_cast<struct s35*>(&r14_56->f58);
            ++v60;
            if (r13_8 == r8_59) 
                goto addr_e954_47;
            addr_dffc_46:
            r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) - reinterpret_cast<unsigned char>(r8_59));
            tmp64_63 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_58) + reinterpret_cast<unsigned char>(r13_8));
            r12_9 = tmp64_63;
            if (reinterpret_cast<unsigned char>(tmp64_63) < reinterpret_cast<unsigned char>(r9_58)) {
                r12_9 = reinterpret_cast<void**>(0xffffffffffffffff);
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r12_9)) {
                r10_64 = r15_31;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_eab0_73;
                    rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r12_9)) {
                    rbx19 = r12_9;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_eab0_73;
                if (r15_31 == v10) 
                    goto addr_ea40_79; else 
                    goto addr_e057_80;
            }
            addr_e07c_81:
            rsi39 = r8_59;
            rsp65 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp65 = 0xe092;
            fun_2880(reinterpret_cast<unsigned char>(r10_64) + reinterpret_cast<unsigned char>(r9_58), rsi39, r13_8);
            rsp15 = reinterpret_cast<void*>(rsp65 + 1);
            r10_16 = r10_64;
            continue;
            addr_ea40_79:
            rsp66 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp66 = 0xea48;
            rax67 = fun_28c0(rbx19, rsi39, rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp66 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax67;
            if (!rax67) 
                goto addr_eab0_73;
            if (!r9_58) 
                goto addr_e07c_81;
            rsp68 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp68 = 0xea87;
            rax69 = fun_2880(rax67, v10, r9_58);
            rsp15 = reinterpret_cast<void*>(rsp68 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax69;
            goto addr_e07c_81;
            addr_e057_80:
            rsp70 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp70 = 0xe062;
            rax71 = fun_2940(r15_31, rbx19, r15_31, rbx19);
            rsp15 = reinterpret_cast<void*>(rsp70 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax71;
            if (!rax71) 
                goto addr_eab0_73; else 
                goto addr_e07c_81;
            addr_ec44_64:
            rsp72 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp72 = 0xec5a;
            rax73 = fun_28c0(rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp72 + 1);
            r9_58 = r9_58;
            if (!rax73) 
                goto addr_ee19_84;
            if (r12_9) 
                goto addr_ec7a_86;
            r10_16 = rax73;
            goto addr_e92f_55;
            addr_ec7a_86:
            rsi39 = r10_16;
            rsp74 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp74 = 0xec8f;
            rax75 = fun_2880(rax73, rsi39, r12_9);
            rsp15 = reinterpret_cast<void*>(rsp74 + 1);
            r9_58 = r9_58;
            r10_16 = rax75;
            goto addr_e92f_55;
            addr_e903_65:
            rsi39 = rbx19;
            v28 = r10_16;
            rsp76 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp76 = 0xe91c;
            rax77 = fun_2940(r10_16, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp76 + 1);
            r9_58 = r9_58;
            if (!rax77) 
                goto addr_e9ff_17;
            r10_16 = rax77;
            goto addr_e92f_55;
        }
        break;
    }
    tmp64_78 = r12_9 + 1;
    r13_8 = tmp64_78;
    if (reinterpret_cast<unsigned char>(tmp64_78) < reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_ee0a_5;
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r13_8)) 
        goto addr_ecfe_7;
    if (rbx19) {
        if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
            goto addr_e9f8_16;
        rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
        if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r13_8)) 
            goto addr_edbd_94;
    } else {
        if (reinterpret_cast<unsigned char>(r13_8) > reinterpret_cast<unsigned char>(12)) {
            addr_edbd_94:
            if (r13_8 == 0xffffffffffffffff) 
                goto addr_e9f8_16; else 
                goto addr_edc7_96;
        } else {
            *reinterpret_cast<int32_t*>(&rbx19) = 12;
            *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
        }
    }
    addr_ecd3_98:
    if (r10_16 == v10) {
        rsp79 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp79 = 0xedde;
        rax80 = fun_28c0(rbx19, rsi39, rbx19, rsi39);
        rsp15 = reinterpret_cast<void*>(rsp79 + 1);
        if (rax80) {
            if (!r12_9) {
                r10_16 = rax80;
                goto addr_ecfe_7;
            } else {
                rsp81 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp81 = 0xedfd;
                rax82 = fun_2880(rax80, r10_16, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp81 + 1);
                r10_16 = rax82;
                goto addr_ecfe_7;
            }
        }
    } else {
        v28 = r10_16;
        rsp83 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp83 = 0xecf2;
        rax84 = fun_2940(r10_16, rbx19, r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp83 + 1);
        r10_16 = rax84;
        if (!rax84) 
            goto addr_e9ff_17; else 
            goto addr_ecfe_7;
    }
    addr_edc7_96:
    rbx19 = r13_8;
    goto addr_ecd3_98;
    addr_e0bf_50:
    if (r14_56->f50 == -1) 
        goto 0x2add;
    ecx85 = *reinterpret_cast<uint32_t*>((r14_56->f50 << 5) + reinterpret_cast<int64_t>(v86));
    if (*reinterpret_cast<signed char*>(&edx62) == 0x6e) {
        *reinterpret_cast<uint32_t*>(&r13_87) = ecx85 - 18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_87) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&r13_87) > 4) 
            goto 0x2ae2;
        goto *reinterpret_cast<int32_t*>(0x13794 + r13_87 * 4) + 0x13794;
    }
    eax88 = r14_56->f10;
    *reinterpret_cast<void***>(v53) = reinterpret_cast<void**>(37);
    r13_8 = v53 + 1;
    if (*reinterpret_cast<unsigned char*>(&eax88) & 1) {
        *reinterpret_cast<void***>(v53 + 1) = reinterpret_cast<void**>(39);
        r13_8 = v53 + 2;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 2) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(45);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 4) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(43);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 8) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(32);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 16) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(35);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 64) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(73);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 32) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(48);
        ++r13_8;
    }
    rsi89 = r14_56->f18;
    if (rsi89 != r14_56->f20) {
        rax90 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_56->f20) - reinterpret_cast<unsigned char>(rsi89));
        rsp91 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp91 = 0xe18f;
        fun_2880(r13_8, rsi89, rax90);
        rsp15 = reinterpret_cast<void*>(rsp91 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax90));
    }
    rsi92 = r14_56->f30;
    if (rsi92 != r14_56->f38) {
        rax93 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_56->f38) - reinterpret_cast<unsigned char>(rsi92));
        rsp94 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp94 = 0xe1c9;
        fun_2880(r13_8, rsi92, rax93);
        rsp15 = reinterpret_cast<void*>(rsp94 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax93));
    }
    *reinterpret_cast<uint32_t*>(&rax95) = ecx85 - 7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax95) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax95) <= 9) {
        goto *reinterpret_cast<int32_t*>(0x13724 + rax95 * 4) + 0x13724;
    }
    eax96 = r14_56->f48;
    *reinterpret_cast<void***>(r13_8 + 1) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r13_8) = *reinterpret_cast<void***>(&eax96);
    if (r14_56->f28 == -1) {
        v97 = 0;
    } else {
        if (*reinterpret_cast<uint32_t*>((r14_56->f28 << 5) + reinterpret_cast<int64_t>(v86)) != 5) 
            goto 0x2add;
        v97 = 1;
    }
    if (r14_56->f40 != -1) {
        rcx98 = reinterpret_cast<struct s36*>(reinterpret_cast<int64_t>(v86) + (r14_56->f40 << 5));
        if (rcx98->f0 != 5) 
            goto 0x2add;
        *reinterpret_cast<int32_t*>(&rax99) = v97;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax99) + 4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rbp6) + rax99 * 4 - 0x3b8) = rcx98->f10;
    }
    tmp64_100 = r12_9 + 2;
    if (reinterpret_cast<unsigned char>(tmp64_100) >= reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_e302_135;
    if (rbx19 != 0xffffffffffffffff) {
        goto addr_e9f8_16;
    }
    addr_e302_135:
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) {
        r15_101 = r10_16;
    } else {
        if (rbx19) {
            if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                goto addr_e9f8_16;
            rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) 
                goto addr_e323_142; else 
                goto addr_ebb2_143;
        } else {
            if (reinterpret_cast<unsigned char>(tmp64_100) > reinterpret_cast<unsigned char>(12)) {
                addr_ebb2_143:
                if (tmp64_100 == 0xffffffffffffffff) 
                    goto addr_e9f8_16; else 
                    goto addr_ebbc_145;
            } else {
                *reinterpret_cast<int32_t*>(&rbx19) = 12;
                *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                goto addr_e323_142;
            }
        }
    }
    addr_e355_147:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_101) + reinterpret_cast<unsigned char>(r12_9)) = 0;
    *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8) = 0xe35f;
    rax102 = fun_25e0();
    *reinterpret_cast<void***>(rax102) = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint32_t*>(&rax103) = ecx85;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax103) + 4) = 0;
    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx19) - reinterpret_cast<unsigned char>(r12_9)) <= 0x7fffffff) {
    }
    if (*reinterpret_cast<uint32_t*>(&rax103) > 17) 
        goto 0x2ae2;
    goto *reinterpret_cast<int32_t*>(0x1374c + rax103 * 4) + 0x1374c;
    addr_e323_142:
    if (r10_16 == v10) {
        rsp104 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp104 = 0xec18;
        rax105 = fun_28c0(rbx19, rsi92);
        rsp15 = reinterpret_cast<void*>(rsp104 + 1);
        r15_101 = rax105;
        if (!rax105) {
            addr_ee19_84:
            rsp106 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp106 = 0xee1e;
            rax107 = fun_25e0();
            rsp15 = reinterpret_cast<void*>(rsp106 + 1);
            r15_31 = v10;
            v32 = rax107;
            goto addr_ea12_18;
        } else {
            if (r12_9) {
                rsp108 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp108 = 0xec3f;
                fun_2880(rax105, v10, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp108 + 1);
                goto addr_e355_147;
            }
        }
    } else {
        rsp109 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp109 = 0xe342;
        rax110 = fun_2940(r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp109 + 1);
        r10_16 = r10_16;
        r15_101 = rax110;
        if (!rax110) 
            goto addr_e9f8_16; else 
            goto addr_e355_147;
    }
    addr_ebbc_145:
    rbx19 = tmp64_100;
    goto addr_e323_142;
    addr_eab0_73:
    rsp111 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
    *rsp111 = 0xeab5;
    rax112 = fun_25e0();
    rsp15 = reinterpret_cast<void*>(rsp111 + 1);
    v32 = rax112;
    goto addr_ea12_18;
}

uint32_t fun_2660(int64_t rdi, ...);

/* have_dupfd_cloexec.0 */
int32_t have_dupfd_cloexec_0 = 0;

int64_t fun_ee53(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7) {
    void** rax8;
    uint32_t eax9;
    uint32_t r12d10;
    int32_t eax11;
    uint32_t eax12;
    int1_t zf13;
    void* rax14;
    int64_t rax15;
    int64_t rdi16;
    uint32_t eax17;
    int64_t rdi18;
    uint32_t eax19;
    void** rax20;
    void** r13d21;
    uint32_t eax22;
    void** rax23;
    int64_t rdi24;
    uint32_t eax25;
    uint32_t ecx26;
    int64_t rax27;
    uint32_t eax28;
    uint32_t eax29;
    uint32_t eax30;
    int32_t ecx31;
    int64_t rax32;

    __asm__("cli ");
    rax8 = g28;
    if (!*reinterpret_cast<int32_t*>(&rsi)) {
        eax9 = fun_2660(rdi);
        r12d10 = eax9;
        goto addr_ef54_3;
    }
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x406) {
        eax11 = have_dupfd_cloexec_0;
        if (eax11 < 0) {
            eax12 = fun_2660(rdi);
            r12d10 = eax12;
            if (reinterpret_cast<int32_t>(eax12) < reinterpret_cast<int32_t>(0) || (zf13 = have_dupfd_cloexec_0 == -1, !zf13)) {
                addr_ef54_3:
                rax14 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(g28));
                if (rax14) {
                    fun_2710();
                } else {
                    *reinterpret_cast<uint32_t*>(&rax15) = r12d10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                    return rax15;
                }
            } else {
                addr_f009_9:
                *reinterpret_cast<uint32_t*>(&rdi16) = r12d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
                eax17 = fun_2660(rdi16, rdi16);
                if (reinterpret_cast<int32_t>(eax17) < reinterpret_cast<int32_t>(0) || (*reinterpret_cast<uint32_t*>(&rdi18) = r12d10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi18) + 4) = 0, eax19 = fun_2660(rdi18, rdi18), eax19 == 0xffffffff)) {
                    rax20 = fun_25e0();
                    r12d10 = 0xffffffff;
                    r13d21 = *reinterpret_cast<void***>(rax20);
                    fun_27d0();
                    *reinterpret_cast<void***>(rax20) = r13d21;
                    goto addr_ef54_3;
                }
            }
        } else {
            eax22 = fun_2660(rdi, rdi);
            r12d10 = eax22;
            if (reinterpret_cast<int32_t>(eax22) >= reinterpret_cast<int32_t>(0) || (rax23 = fun_25e0(), *reinterpret_cast<int32_t*>(&rdi24) = *reinterpret_cast<int32_t*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0, !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax23) == 22))) {
                have_dupfd_cloexec_0 = 1;
                goto addr_ef54_3;
            } else {
                eax25 = fun_2660(rdi24);
                r12d10 = eax25;
                if (reinterpret_cast<int32_t>(eax25) < reinterpret_cast<int32_t>(0)) 
                    goto addr_ef54_3;
                have_dupfd_cloexec_0 = -1;
                goto addr_f009_9;
            }
        }
    }
    if (*reinterpret_cast<int32_t*>(&rsi) <= 11) 
        goto addr_eeb9_16;
    ecx26 = static_cast<uint32_t>(rsi - 0x400);
    if (ecx26 > 10) 
        goto addr_eebd_18;
    rax27 = 1 << *reinterpret_cast<unsigned char*>(&ecx26);
    if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x2c5)) {
        if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x502)) {
            addr_eebd_18:
            if (0) {
            }
        } else {
            addr_ef05_23:
            eax28 = fun_2660(rdi);
            r12d10 = eax28;
            goto addr_ef54_3;
        }
        eax29 = fun_2660(rdi);
        r12d10 = eax29;
        goto addr_ef54_3;
    }
    if (0) {
    }
    eax30 = fun_2660(rdi);
    r12d10 = eax30;
    goto addr_ef54_3;
    addr_eeb9_16:
    if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) == 0))) 
        goto addr_eebd_18;
    ecx31 = *reinterpret_cast<int32_t*>(&rsi);
    rax32 = 1 << *reinterpret_cast<unsigned char*>(&ecx31);
    if (!(*reinterpret_cast<uint32_t*>(&rax32) & 0x514)) {
        if (*reinterpret_cast<uint32_t*>(&rax32) & 0xa0a) 
            goto addr_ef05_23;
        goto addr_eebd_18;
    }
}

struct s37 {
    int64_t f0;
    uint32_t* f8;
};

int64_t fun_f0c3(int64_t rdi, struct s37* rsi) {
    int64_t rdx3;

    __asm__("cli ");
    if (!rsi->f0) {
        return 0;
    }
    if (*rsi->f8 <= 22) 
        goto addr_f0f9_5;
    return 0xffffffff;
    addr_f0f9_5:
    *reinterpret_cast<uint32_t*>(&rdx3) = *rsi->f8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x137b0 + rdx3 * 4) + 0x137b0;
}

struct s38 {
    int64_t f0;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
    int64_t f18;
    void** f20;
};

struct s39 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

struct s40 {
    unsigned char f0;
    signed char[1] pad2;
    void** f2;
};

struct s41 {
    unsigned char f0;
    signed char[1] pad2;
    void** f2;
};

int64_t fun_f2f3(void** rdi, struct s38* rsi, struct s39* rdx) {
    void** r10_4;
    void** rax5;
    void** rdi6;
    struct s39* r15_7;
    struct s38* r14_8;
    void** rcx9;
    void** r9_10;
    void** v11;
    void** v12;
    uint32_t edx13;
    void** rbx14;
    int64_t rax15;
    void** r12_16;
    int64_t rbp17;
    int32_t edx18;
    void** rdx19;
    int64_t rcx20;
    int32_t esi21;
    void** rdx22;
    struct s40* rax23;
    uint64_t rdi24;
    int32_t edx25;
    struct s40* rcx26;
    uint64_t rdx27;
    uint64_t rsi28;
    uint64_t rsi29;
    uint64_t tmp64_30;
    int32_t edx31;
    int32_t eax32;
    int64_t rax33;
    int64_t rcx34;
    int32_t eax35;
    int32_t eax36;
    uint32_t eax37;
    void** rdx38;
    uint32_t eax39;
    void* rax40;
    void** rdx41;
    uint32_t eax42;
    void* rax43;
    uint32_t eax44;
    void** rcx45;
    int64_t rsi46;
    int32_t eax47;
    void** rbx48;
    void** rax49;
    int64_t rsi50;
    int32_t edi51;
    void** rbp52;
    void** rdx53;
    void** r8_54;
    void** rdx55;
    void*** rax56;
    void*** rcx57;
    void** r9_58;
    void*** rbp59;
    void** rsi60;
    void** rax61;
    void** rax62;
    void** rdx63;
    void** rax64;
    struct s40* rbx65;
    void* rsi66;
    int32_t eax67;
    struct s40* rdx68;
    void* rax69;
    void* rcx70;
    void* rcx71;
    void* tmp64_72;
    int32_t eax73;
    void** rax74;
    int64_t rdx75;
    int32_t edi76;
    void** rbx77;
    void** r9_78;
    void** rdx79;
    void*** rax80;
    void*** rsi81;
    void** rsi82;
    void** rax83;
    void** rdi84;
    void** r8_85;
    void** rdi86;
    void** rdx87;
    void** rax88;
    void** rax89;
    void** rax90;
    void*** rax91;
    void** rdi92;
    void** rax93;
    struct s41* rbx94;
    void* rdi95;
    int32_t eax96;
    struct s41* rcx97;
    void* rax98;
    void* rdx99;
    void* rdx100;
    void* tmp64_101;
    int32_t eax102;
    int32_t eax103;
    int32_t eax104;
    int64_t rax105;
    int64_t rax106;

    __asm__("cli ");
    r10_4 = reinterpret_cast<void**>(&rsi->f20);
    rax5 = rdi;
    rdi6 = reinterpret_cast<void**>(&rdx->f10);
    r15_7 = rdx;
    r14_8 = rsi;
    rcx9 = r10_4;
    *reinterpret_cast<int32_t*>(&r9_10) = 7;
    *reinterpret_cast<int32_t*>(&r9_10 + 4) = 0;
    rsi->f0 = 0;
    rsi->f8 = r10_4;
    v11 = rdi6;
    rdx->f0 = reinterpret_cast<void**>(0);
    rdx->f8 = rdi6;
    v12 = reinterpret_cast<void**>(0);
    while (edx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax5)), !!*reinterpret_cast<signed char*>(&edx13)) {
        rbx14 = rax5 + 1;
        if (*reinterpret_cast<signed char*>(&edx13) == 37) 
            goto addr_f3a8_4;
        rax5 = rbx14;
    }
    *reinterpret_cast<void***>(rcx9) = rax5;
    r14_8->f10 = 0;
    r14_8->f18 = 0;
    *reinterpret_cast<int32_t*>(&rax15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    addr_f395_7:
    return rax15;
    addr_f3a8_4:
    r12_16 = rcx9;
    *reinterpret_cast<void***>(r12_16) = rax5;
    *reinterpret_cast<void***>(r12_16 + 16) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 24) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 32) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 40) = reinterpret_cast<void**>(0xffffffffffffffff);
    *reinterpret_cast<void***>(r12_16 + 48) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 56) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 64) = reinterpret_cast<void**>(0xffffffffffffffff);
    *reinterpret_cast<int64_t*>(r12_16 + 80) = -1;
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax5 + 1));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    edx18 = static_cast<int32_t>(rbp17 - 48);
    if (*reinterpret_cast<unsigned char*>(&edx18) <= 9) {
        rdx19 = rbx14;
        do {
            *reinterpret_cast<uint32_t*>(&rcx20) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx19 + 1));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx20) + 4) = 0;
            ++rdx19;
            esi21 = static_cast<int32_t>(rcx20 - 48);
        } while (*reinterpret_cast<unsigned char*>(&esi21) <= 9);
        if (*reinterpret_cast<signed char*>(&rcx20) != 36) 
            goto addr_f419_11;
    } else {
        addr_f419_11:
        rdx22 = rbx14 + 1;
        if (*reinterpret_cast<signed char*>(&rbp17) == 39) {
            do {
                *reinterpret_cast<void***>(r12_16 + 16) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_16 + 16)) | 1);
                *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx22));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                rbx14 = rdx22;
                rdx22 = rbx14 + 1;
            } while (*reinterpret_cast<signed char*>(&rbp17) == 39);
            goto addr_f430_14;
        } else {
            goto addr_f430_14;
        }
    }
    rax23 = reinterpret_cast<struct s40*>(rax5 + 2);
    *reinterpret_cast<int32_t*>(&rdi24) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
    while (1) {
        edx25 = static_cast<int32_t>(rbp17 - 48);
        rcx26 = reinterpret_cast<struct s40*>(reinterpret_cast<uint64_t>(rax23) + 0xffffffffffffffff);
        rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx25)));
        if (rdi24 > 0x1999999999999999) {
            rsi28 = 0xffffffffffffffff;
        } else {
            rsi29 = rdi24 + rdi24 * 4;
            rsi28 = rsi29 + rsi29;
        }
        while (*reinterpret_cast<uint32_t*>(&rbp17) = rax23->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0, tmp64_30 = rsi28 + rdx27, rdi24 = tmp64_30, edx31 = static_cast<int32_t>(rbp17 - 48), tmp64_30 < rsi28) {
            if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
                goto addr_f8f8_22;
            rcx26 = rax23;
            rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx31)));
            rax23 = reinterpret_cast<struct s40*>(&rax23->pad2);
            rsi28 = 0xffffffffffffffff;
        }
        if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
            break;
        rax23 = reinterpret_cast<struct s40*>(&rax23->pad2);
    }
    if (tmp64_30 - 1 > 0xfffffffffffffffd) 
        goto addr_f8f8_22;
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rcx26->f2));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    rbx14 = reinterpret_cast<void**>(&rcx26->f2);
    goto addr_f419_11;
    addr_f430_14:
    eax32 = static_cast<int32_t>(rbp17 - 32);
    if (*reinterpret_cast<unsigned char*>(&eax32) <= 41) {
        *reinterpret_cast<uint32_t*>(&rax33) = *reinterpret_cast<unsigned char*>(&eax32);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x1382c + rax33 * 4) + 0x1382c;
    }
    if (*reinterpret_cast<signed char*>(&rbp17) == 42) {
        *reinterpret_cast<void***>(r12_16 + 24) = rbx14;
        *reinterpret_cast<void***>(r12_16 + 32) = rdx22;
        *reinterpret_cast<uint32_t*>(&rcx34) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0;
        if (0) {
        }
        eax35 = static_cast<int32_t>(rcx34 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax35) > 9) 
            goto addr_f577_33;
    } else {
        eax36 = static_cast<int32_t>(rbp17 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax36) <= 9) {
            *reinterpret_cast<void***>(r12_16 + 24) = rbx14;
            eax37 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14) - 48);
            if (*reinterpret_cast<unsigned char*>(&eax37) > 9) {
                addr_fc79_36:
                *reinterpret_cast<void***>(r12_16 + 32) = rbx14;
                goto addr_fc7e_37;
            } else {
                rdx38 = rbx14;
                do {
                    ++rdx38;
                    eax39 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx38 + 1) - 48);
                } while (*reinterpret_cast<unsigned char*>(&eax39) <= 9);
                rax40 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx38) - reinterpret_cast<unsigned char>(rbx14));
                rbx14 = rdx38;
                if (0 >= reinterpret_cast<uint64_t>(rax40)) 
                    goto label_41; else 
                    goto addr_fc74_42;
            }
        } else {
            addr_f45d_43:
            if (*reinterpret_cast<signed char*>(&rbp17) == 46) {
                addr_f678_44:
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx14 + 1) == 42)) {
                    *reinterpret_cast<void***>(r12_16 + 48) = rbx14;
                    rdx41 = rbx14 + 1;
                    eax42 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14 + 1) - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax42) > 9) {
                        rbx14 = rdx41;
                        *reinterpret_cast<int32_t*>(&rax43) = 1;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax43) + 4) = 0;
                    } else {
                        do {
                            ++rdx41;
                            eax44 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx41 + 1) - 48);
                        } while (*reinterpret_cast<unsigned char*>(&eax44) <= 9);
                        rax43 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx41) - reinterpret_cast<unsigned char>(rbx14));
                        rbx14 = rdx41;
                    }
                    *reinterpret_cast<void***>(r12_16 + 56) = rdx41;
                    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                    if (0 >= reinterpret_cast<uint64_t>(rax43)) {
                    }
                    goto addr_f467_52;
                } else {
                    rcx45 = rbx14 + 2;
                    *reinterpret_cast<void***>(r12_16 + 48) = rbx14;
                    *reinterpret_cast<void***>(r12_16 + 56) = rcx45;
                    *reinterpret_cast<uint32_t*>(&rsi46) = *reinterpret_cast<unsigned char*>(rbx14 + 2);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0;
                    if (0) {
                    }
                    eax47 = static_cast<int32_t>(rsi46 - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax47) <= 9) 
                        goto addr_fe64_56; else 
                        goto addr_f6b5_57;
                }
            } else {
                addr_f467_52:
                rbx48 = rbx14 + 1;
                if (*reinterpret_cast<signed char*>(&rbp17) == 0x68) {
                    do {
                        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx48));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                        ++rbx48;
                    } while (*reinterpret_cast<signed char*>(&rbp17) == 0x68);
                    goto addr_f488_60;
                } else {
                    goto addr_f488_60;
                }
            }
        }
    }
    rax49 = rdx22;
    do {
        *reinterpret_cast<uint32_t*>(&rsi50) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax49 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi50) + 4) = 0;
        ++rax49;
        edi51 = static_cast<int32_t>(rsi50 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi51) <= 9);
    if (*reinterpret_cast<signed char*>(&rsi50) == 36) 
        goto addr_f9fb_65;
    addr_f577_33:
    *reinterpret_cast<void***>(r12_16 + 40) = reinterpret_cast<void**>(0);
    if (0) 
        goto addr_f8f8_22;
    rbp52 = reinterpret_cast<void**>(0);
    v12 = reinterpret_cast<void**>(1);
    rbx14 = rdx22;
    addr_f59c_67:
    rdx53 = r15_7->f8;
    r8_54 = rdx53;
    if (reinterpret_cast<unsigned char>(7) > reinterpret_cast<unsigned char>(rbp52)) {
        addr_f61a_68:
        rdx55 = r15_7->f0;
        rax56 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx55) << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (reinterpret_cast<unsigned char>(rdx55) <= reinterpret_cast<unsigned char>(rbp52)) {
            do {
                ++rdx55;
                *rax56 = reinterpret_cast<void**>(0);
                rcx57 = rax56;
                rax56 = rax56 + 32;
            } while (reinterpret_cast<unsigned char>(rdx55) <= reinterpret_cast<unsigned char>(rbp52));
            r15_7->f0 = rdx55;
            *rcx57 = reinterpret_cast<void**>(0);
        }
    } else {
        r9_58 = reinterpret_cast<void**>(14);
        if (reinterpret_cast<unsigned char>(14) <= reinterpret_cast<unsigned char>(rbp52)) {
            r9_58 = rbp52 + 1;
        }
        if (reinterpret_cast<unsigned char>(r9_58) >> 59) 
            goto addr_ff1b_75; else 
            goto addr_f5c3_76;
    }
    rbp59 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp52) << 5) + reinterpret_cast<unsigned char>(r8_54));
    if (*rbp59) {
        if (*rbp59 != 5) {
            goto addr_f8fc_80;
        }
    } else {
        *rbp59 = reinterpret_cast<void**>(5);
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rbp17) != 46) 
            goto addr_f467_52;
        goto addr_f678_44;
    }
    addr_fc7e_37:
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    goto addr_f45d_43;
    addr_ff1b_75:
    if (v11 != rdx53) {
        fun_25b0(rdx53, rdx53);
        r10_4 = r10_4;
        goto addr_fd2a_84;
    } else {
        goto addr_fd2a_84;
    }
    addr_f5c3_76:
    rsi60 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_58) << 5);
    if (v11 == rdx53) {
        rax61 = fun_28c0(rsi60, rsi60);
        rdx53 = rdx53;
        r9_10 = r9_58;
        r10_4 = r10_4;
        r8_54 = rax61;
    } else {
        rax62 = fun_2940(rdx53, rsi60);
        rdx53 = r15_7->f8;
        r10_4 = r10_4;
        r9_10 = r9_58;
        r8_54 = rax62;
    }
    if (!r8_54) 
        goto addr_ff1b_75;
    if (v11 == rdx53) {
        rdx63 = r15_7->f0;
        rax64 = fun_2880(r8_54, v11, reinterpret_cast<unsigned char>(rdx63) << 5);
        r9_10 = r9_10;
        r10_4 = r10_4;
        r8_54 = rax64;
    }
    r15_7->f8 = r8_54;
    goto addr_f61a_68;
    addr_f9fb_65:
    rbx65 = reinterpret_cast<struct s40*>(rbx14 + 2);
    *reinterpret_cast<int32_t*>(&rsi66) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi66) + 4) = 0;
    while (1) {
        eax67 = static_cast<int32_t>(rcx34 - 48);
        rdx68 = reinterpret_cast<struct s40*>(reinterpret_cast<uint64_t>(rbx65) + 0xffffffffffffffff);
        rax69 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax67)));
        if (reinterpret_cast<uint64_t>(rsi66) > 0x1999999999999999) {
            rcx70 = reinterpret_cast<void*>(0xffffffffffffffff);
        } else {
            rcx71 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi66) + reinterpret_cast<uint64_t>(rsi66) * 4);
            rcx70 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx71) + reinterpret_cast<uint64_t>(rcx71));
        }
        while (tmp64_72 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx70) + reinterpret_cast<uint64_t>(rax69)), rsi66 = tmp64_72, *reinterpret_cast<uint32_t*>(&rcx34) = rbx65->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0, eax73 = static_cast<int32_t>(rcx34 - 48), reinterpret_cast<uint64_t>(tmp64_72) < reinterpret_cast<uint64_t>(rcx70)) {
            if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
                goto addr_f8f8_22;
            rdx68 = rbx65;
            rax69 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax73)));
            rbx65 = reinterpret_cast<struct s40*>(&rbx65->pad2);
            rcx70 = reinterpret_cast<void*>(0xffffffffffffffff);
        }
        if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
            break;
        rbx65 = reinterpret_cast<struct s40*>(&rbx65->pad2);
    }
    rbp52 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(tmp64_72) - 1);
    if (reinterpret_cast<unsigned char>(rbp52) > reinterpret_cast<unsigned char>(0xfffffffffffffffd)) 
        goto addr_f8f8_22;
    *reinterpret_cast<void***>(r12_16 + 40) = rbp52;
    rbx14 = reinterpret_cast<void**>(&rdx68->f2);
    goto addr_f59c_67;
    label_41:
    addr_fc74_42:
    goto addr_fc79_36;
    addr_fe64_56:
    rax74 = rcx45;
    do {
        *reinterpret_cast<uint32_t*>(&rdx75) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax74 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
        ++rax74;
        edi76 = static_cast<int32_t>(rdx75 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi76) <= 9);
    if (*reinterpret_cast<signed char*>(&rdx75) == 36) 
        goto addr_fe8a_104;
    addr_f6b5_57:
    rbx77 = *reinterpret_cast<void***>(r12_16 + 64);
    if (rbx77 == 0xffffffffffffffff) {
        *reinterpret_cast<void***>(r12_16 + 64) = v12;
        if (0) {
            addr_f8f8_22:
            r8_54 = r15_7->f8;
            goto addr_f8fc_80;
        } else {
            rbx77 = v12;
        }
    }
    addr_f6c4_107:
    r8_54 = r15_7->f8;
    if (reinterpret_cast<unsigned char>(r9_10) <= reinterpret_cast<unsigned char>(rbx77)) {
        r9_78 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_10) + reinterpret_cast<unsigned char>(r9_10));
        if (reinterpret_cast<unsigned char>(r9_78) <= reinterpret_cast<unsigned char>(rbx77)) {
            r9_78 = rbx77 + 1;
        }
        if (!(reinterpret_cast<unsigned char>(r9_78) >> 59)) 
            goto addr_fd72_111;
    } else {
        addr_f6d1_112:
        rdx79 = r15_7->f0;
        rax80 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx79) << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (reinterpret_cast<unsigned char>(rdx79) <= reinterpret_cast<unsigned char>(rbx77)) {
            do {
                ++rdx79;
                *rax80 = reinterpret_cast<void**>(0);
                rsi81 = rax80;
                rax80 = rax80 + 32;
            } while (reinterpret_cast<unsigned char>(rdx79) <= reinterpret_cast<unsigned char>(rbx77));
            r15_7->f0 = rdx79;
            *rsi81 = reinterpret_cast<void**>(0);
            goto addr_f707_116;
        }
    }
    rdx53 = r8_54;
    goto addr_ff1b_75;
    addr_fd72_111:
    rsi82 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_78) << 5);
    if (v11 == r8_54) {
        rax83 = fun_28c0(rsi82, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        rdi84 = rax83;
        r8_85 = r8_54;
        if (!rax83) {
            addr_fd2a_84:
            rdi86 = r14_8->f8;
            if (r10_4 != rdi86) {
                fun_25b0(rdi86, rdi86);
            }
        } else {
            addr_ffb0_120:
            rdx87 = r15_7->f0;
            rax88 = fun_2880(rdi84, r8_85, reinterpret_cast<unsigned char>(rdx87) << 5);
            r10_4 = r10_4;
            rcx45 = rcx45;
            r8_54 = rax88;
            goto addr_fdcf_121;
        }
    } else {
        rax89 = fun_2940(r8_54, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        r8_54 = rax89;
        if (!rax89) {
            rdx53 = r15_7->f8;
            goto addr_ff1b_75;
        } else {
            if (v11 == r15_7->f8) {
                rdi84 = r8_54;
                r8_85 = v11;
                goto addr_ffb0_120;
            }
        }
    }
    rax90 = fun_25e0();
    *reinterpret_cast<void***>(rax90) = reinterpret_cast<void**>(12);
    return 0xffffffff;
    addr_fdcf_121:
    r15_7->f8 = r8_54;
    goto addr_f6d1_112;
    addr_f707_116:
    rax91 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r8_54) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx77) << 5));
    if (*rax91) {
        if (!reinterpret_cast<int1_t>(*rax91 == 5)) {
            addr_f8fc_80:
            if (v11 != r8_54) {
                fun_25b0(r8_54, r8_54);
                r10_4 = r10_4;
            }
        } else {
            *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx45));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
            rbx14 = rcx45;
            goto addr_f467_52;
        }
    } else {
        *rax91 = reinterpret_cast<void**>(5);
        rbx14 = rcx45;
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx45));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        goto addr_f467_52;
    }
    rdi92 = r14_8->f8;
    if (r10_4 != rdi92) {
        fun_25b0(rdi92, rdi92);
    }
    rax93 = fun_25e0();
    *reinterpret_cast<void***>(rax93) = reinterpret_cast<void**>(22);
    *reinterpret_cast<int32_t*>(&rax15) = -1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    goto addr_f395_7;
    addr_fe8a_104:
    rbx94 = reinterpret_cast<struct s41*>(rbx14 + 3);
    *reinterpret_cast<int32_t*>(&rdi95) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi95) + 4) = 0;
    while (1) {
        eax96 = static_cast<int32_t>(rsi46 - 48);
        rcx97 = reinterpret_cast<struct s41*>(reinterpret_cast<uint64_t>(rbx94) + 0xffffffffffffffff);
        rax98 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax96)));
        if (reinterpret_cast<uint64_t>(rdi95) > 0x1999999999999999) {
            rdx99 = reinterpret_cast<void*>(0xffffffffffffffff);
        } else {
            rdx100 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi95) + reinterpret_cast<uint64_t>(rdi95) * 4);
            rdx99 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx100) + reinterpret_cast<uint64_t>(rdx100));
        }
        while (*reinterpret_cast<uint32_t*>(&rsi46) = rbx94->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0, tmp64_101 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx99) + reinterpret_cast<uint64_t>(rax98)), rdi95 = tmp64_101, eax102 = static_cast<int32_t>(rsi46 - 48), reinterpret_cast<uint64_t>(tmp64_101) < reinterpret_cast<uint64_t>(rdx99)) {
            if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
                goto addr_f8f8_22;
            rcx97 = rbx94;
            rax98 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax102)));
            rbx94 = reinterpret_cast<struct s41*>(&rbx94->pad2);
            rdx99 = reinterpret_cast<void*>(0xffffffffffffffff);
        }
        if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
            break;
        rbx94 = reinterpret_cast<struct s41*>(&rbx94->pad2);
    }
    rbx77 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(tmp64_101) + 0xffffffffffffffff);
    if (reinterpret_cast<unsigned char>(rbx77) > reinterpret_cast<unsigned char>(0xfffffffffffffffd)) 
        goto addr_f8f8_22;
    *reinterpret_cast<void***>(r12_16 + 64) = rbx77;
    rcx45 = reinterpret_cast<void**>(&rcx97->f2);
    goto addr_f6c4_107;
    addr_f488_60:
    eax103 = static_cast<int32_t>(rbp17 - 76);
    if (*reinterpret_cast<unsigned char*>(&eax103) > 46) {
        eax104 = static_cast<int32_t>(rbp17 - 37);
        if (*reinterpret_cast<unsigned char*>(&eax104) <= 83) {
            *reinterpret_cast<uint32_t*>(&rax105) = *reinterpret_cast<unsigned char*>(&eax104);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0x13990 + rax105 * 4) + 0x13990;
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rax106) = *reinterpret_cast<unsigned char*>(&eax103);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax106) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x138d4 + rax106 * 4) + 0x138d4;
    }
}

void fun_10083() {
    __asm__("cli ");
}

void fun_10097() {
    __asm__("cli ");
    return;
}

void fun_386e() {
}

struct s42 {
    signed char[8] pad8;
    void** f8;
    signed char[63] pad72;
    void** f48;
};

void fun_4210() {
    void** r12_1;
    struct s42* v2;
    void** rbp3;
    void** r13_4;
    void** r8_5;
    void** r9_6;
    void** rbp7;
    int64_t v8;

    r12_1 = v2->f48;
    if (!r12_1) {
        r12_1 = v2->f8;
    }
    make_format(rbp3, r13_4, "'-0", "lu", r8_5, r9_6, __return_address());
    fun_2960(1, rbp7, r12_1, "lu");
    goto v8;
}

void fun_4260() {
    void** r8_1;
    void*** v2;
    void** r9_3;

    r8_1 = *v2;
    if (r8_1 != 0x42494e4d) {
        if (reinterpret_cast<signed char>(r8_1) > reinterpret_cast<signed char>(0x42494e4d)) {
            if (r8_1 != 0x67596969) {
                if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x67596969)) {
                    if (r8_1 != 0x58295829) {
                        if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x58295829)) {
                            if (r8_1 != 0x5345434d) {
                                if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x5345434d)) {
                                    if (r8_1 != 0x45584653) {
                                        if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x45584653)) {
                                            if (r8_1 != 0x453dcd28) {
                                                if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x453dcd28)) {
                                                    if (r8_1 != 0x43415d53) {
                                                        if (!reinterpret_cast<int1_t>(r8_1 == 0x444d4142)) {
                                                            addr_4fc8_17:
                                                            fun_2a90(0x18170, 1, 29, "UNKNOWN (0x%lx)", r8_1, r9_3, __return_address());
                                                        }
                                                    }
                                                } else {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x454d444d)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        } else {
                                            if (r8_1 != 0x50495045) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x52654973)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x47504653)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (r8_1 != 0x5346544e) {
                                        if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x5346544e)) {
                                            if (r8_1 != 0x5346414f) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x53464846)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x5346314d)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        } else {
                                            if (r8_1 != 0x54190100) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x565a4653)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x534f434b)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (r8_1 != 0x62646576) {
                                if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x62646576)) {
                                    if (r8_1 != 0x5dca2df5) {
                                        if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x5dca2df5)) {
                                            if (r8_1 != 0x5a3c69f0) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x5a4f4653)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x58465342)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        } else {
                                            if (r8_1 != 0x6165676c) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x61756673)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x61636673)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (r8_1 != 0x64626720) {
                                        if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x64626720)) {
                                            if (r8_1 != 0x62656572) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x63677270)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x62656570)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        } else {
                                            if (r8_1 != 0x65735543) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x65735546)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x64646178)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (r8_1 != 0x958458f6) {
                        if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x958458f6)) {
                            if (r8_1 != 0x73757245) {
                                if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x73757245)) {
                                    if (r8_1 != 0x6e736673) {
                                        if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x6e736673)) {
                                            if (r8_1 != 0x6c6f6f70) {
                                                if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x6c6f6f70)) {
                                                    if (r8_1 != 0x68191122) {
                                                        if (!reinterpret_cast<int1_t>(r8_1 == 0x6b414653)) 
                                                            goto addr_4fc8_17;
                                                    }
                                                } else {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x6e667364)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        } else {
                                            if (r8_1 != 0x73717368) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x73727279)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x73636673)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (r8_1 != 0x794c7630) {
                                        if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x794c7630)) {
                                            if (r8_1 != 0x74726163) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x786f4256)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x7461636f)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        } else {
                                            if (r8_1 != 0x858458f6) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x9123683e)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x7c7c6673)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (r8_1 != 0xcafe4a11) {
                                if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0xcafe4a11)) {
                                    if (r8_1 != 0xbacbacbc) {
                                        if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0xbacbacbc)) {
                                            if (r8_1 != 0xaad7aaea) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0xabba1974)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0xa501fcf5)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        } else {
                                            if (r8_1 != 0xc7571590) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0xc97e8168)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0xbeefdead)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (r8_1 != 0xf97cff8c) {
                                        if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0xf97cff8c)) {
                                            if (r8_1 != 0xe0f5e1e2) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0xf2f52010)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0xde5e81e4)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        } else {
                                            if (r8_1 != 0xfe534d42) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0xff534d42)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0xf995e849)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (r8_1 != 0xef53) {
                if (reinterpret_cast<signed char>(r8_1) > reinterpret_cast<signed char>(0xef53)) {
                    if (r8_1 != 0x9041934) {
                        if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x9041934)) {
                            if (r8_1 != 0x1161970) {
                                if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x1161970)) {
                                    if (r8_1 != 0xc0ffee) {
                                        if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0xc0ffee)) {
                                            if (r8_1 != 0x27e0eb) {
                                                if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x27e0eb)) {
                                                    if (r8_1 != 0xf15f) {
                                                        if (!reinterpret_cast<int1_t>(r8_1 == "%g   group ID of owner\n  %G   group name of owner\n")) 
                                                            goto addr_4fc8_17;
                                                    }
                                                } else {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x414a53)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        } else {
                                            if (r8_1 != 0x1021994) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x1021997)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0xc36400)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (r8_1 != 0x12ff7b6) {
                                        if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x12ff7b6)) {
                                            if (r8_1 != 0x12ff7b4) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x12ff7b5)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x12fd16d)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        } else {
                                            if (r8_1 != 0x13111a8) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x7655821)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x12ff7b7)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (r8_1 != 0x1badface) {
                                if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x1badface)) {
                                    if (r8_1 != 0x13661366) {
                                        if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x13661366)) {
                                            if (r8_1 != 0xbd00bd0) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x11307854)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0xbad1dea)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        } else {
                                            if (r8_1 != 0x19800202) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x19830326)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x15013346)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (r8_1 != 0x2bad1dea) {
                                        if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x2bad1dea)) {
                                            if (r8_1 != 0x24051905) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x28cd3d45)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x2011bab0)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        } else {
                                            if (r8_1 != 0x3153464a) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x42465331)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x2fc12fc1)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (r8_1 != 0x482b) {
                        if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x482b)) {
                            if (r8_1 != 0x1cd1) {
                                if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x1cd1)) {
                                    if (r8_1 != 0x1373) {
                                        if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x1373)) {
                                            if (r8_1 != 0x187) {
                                                if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x187)) {
                                                    if (r8_1 != 47) {
                                                        if (!reinterpret_cast<int1_t>(r8_1 == 51)) 
                                                            goto addr_4fc8_17;
                                                    }
                                                } else {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x7c0)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        } else {
                                            if (r8_1 != 0x137f) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x138f)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x137d)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (r8_1 != 0x4000) {
                                        if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x4000)) {
                                            if (r8_1 != 0x2478) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x3434)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x2468)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        } else {
                                            if (r8_1 != 0x4006) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x4244)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x4004)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (r8_1 != 0x72b6) {
                                if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x72b6)) {
                                    if (r8_1 != 0x564c) {
                                        if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x564c)) {
                                            if (r8_1 != 0x4d5a) {
                                                if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x4d5a)) {
                                                    if (r8_1 != 0x4858) {
                                                        if (!reinterpret_cast<int1_t>(r8_1 == 0x4d44)) 
                                                            goto addr_4fc8_17;
                                                    }
                                                } else {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x517b)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        } else {
                                            if (r8_1 != 0x6969) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x7275)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0x5df5)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (r8_1 != 0x9fa2) {
                                        if (reinterpret_cast<signed char>(r8_1) <= reinterpret_cast<signed char>(0x9fa2)) {
                                            if (r8_1 != 0x9fa0) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0x9fa1)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == "\n")) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        } else {
                                            if (r8_1 != 0xadff) {
                                                if (!reinterpret_cast<int1_t>(r8_1 == 0xef51)) {
                                                    if (!reinterpret_cast<int1_t>(r8_1 == 0xadf5)) 
                                                        goto addr_4fc8_17;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    goto 0x4230;
}

void fun_4630() {
    goto 0x4230;
}

struct s43 {
    signed char[112] pad112;
    void** f70;
};

struct s44 {
    signed char[104] pad104;
    void** f68;
};

void fun_5050() {
    void** rsi1;
    struct s43* r14_2;
    void** rdi3;
    struct s44* r14_4;
    void** rdx5;
    void** rcx6;
    void** r8_7;
    void** r9_8;
    void** rax9;
    void** rbp10;
    void** r12_11;
    void** r8_12;
    void** r9_13;
    void** rbp14;

    rsi1 = r14_2->f70;
    rdi3 = r14_4->f68;
    rax9 = human_time(rdi3, rsi1, rdx5, rcx6, r8_7, r9_8, __return_address());
    make_format(rbp10, r12_11, "-", "s", r8_12, r9_13, __return_address());
    fun_2960(1, rbp14, rax9, "s");
}

struct s45 {
    signed char[56] pad56;
    int64_t f38;
};

void fun_50c8() {
    struct s45* r14_1;
    void** rbp2;
    void** r12_3;
    void** r8_4;
    void** r9_5;

    if (reinterpret_cast<uint64_t>(r14_1->f38 - 1) > 0x1fffffffffffffff) {
    }
    make_format(rbp2, r12_3, "'-0", "lu", r8_4, r9_5, __return_address());
    goto 0x507c;
}

struct s46 {
    signed char[24] pad24;
    uint32_t f18;
};

void** fun_27c0(int64_t rdi);

void** find_mount_point(int64_t rdi, int64_t rsi);

void fun_5118() {
    int1_t zf1;
    struct s46* r14_2;
    int64_t r13_3;
    void** rax4;
    void** rbx5;
    void** r13_6;
    void** rdx7;
    void** rcx8;
    void** r8_9;
    void** r9_10;
    int64_t rsi11;
    void** rax12;
    void** rdi13;
    void** rax14;
    int64_t r13_15;
    int64_t r14_16;
    void** rax17;
    void** rbp18;
    void** r12_19;
    void** r8_20;
    void** r9_21;
    void** rbp22;

    zf1 = follow_links == 0;
    if (!zf1 || (r14_2->f18 & 0xf000) != 0xa000) {
        rax4 = fun_27c0(r13_3);
        rbx5 = rax4;
        if (!rax4) {
            quotearg_style(4, r13_6, rdx7, rcx8, r8_9, r9_10, __return_address());
            fun_26c0();
            fun_25e0();
            fun_2990();
            goto addr_5726_4;
        }
        rax12 = find_bind_mount(rax4, rsi11);
        rdi13 = rbx5;
        *reinterpret_cast<int32_t*>(&rbx5) = 0;
        *reinterpret_cast<int32_t*>(&rbx5 + 4) = 0;
        fun_25b0(rdi13);
        rax14 = rax12;
        if (!rax14) 
            goto addr_5660_6;
    } else {
        addr_5660_6:
        rax17 = find_mount_point(r13_15, r14_16);
        rbx5 = rax17;
        if (!rax17) {
            addr_5726_4:
            rax14 = reinterpret_cast<void**>("?");
        } else {
            rax14 = find_bind_mount(rax17, r14_16);
            if (!rax14) {
                rax14 = rbx5;
            }
        }
    }
    make_format(rbp18, r12_19, "-", "s", r8_20, r9_21, __return_address());
    fun_2960(1, rbp22, rax14, "s");
    fun_25b0(rbx5, rbx5);
    goto 0x508e;
}

void fun_51b0() {
    goto 0x50f0;
}

void fun_5250() {
    goto 0x5058;
}

void fun_5320() {
    goto 0x5058;
}

void fun_5360() {
    goto 0x50f0;
}

struct s47 {
    signed char[28] pad28;
    int32_t f1c;
};

int64_t fun_26a0(int64_t rdi);

void fun_5420() {
    int64_t rdi1;
    struct s47* r14_2;
    int64_t rax3;

    *reinterpret_cast<int32_t*>(&rdi1) = r14_2->f1c;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi1) + 4) = 0;
    rax3 = fun_26a0(rdi1);
    if (!rax3) 
        goto 0x5060;
    goto 0x5060;
}

void fun_5448() {
    void** rbp1;
    void** r12_2;
    void** r8_3;
    void** r9_4;

    make_format(rbp1, r12_2, "-#0", "lx", r8_3, r9_4, __return_address());
    goto 0x507c;
}

struct s48 {
    signed char[32] pad32;
    int32_t f20;
};

int64_t fun_2750(int64_t rdi);

void fun_5560() {
    int64_t rdi1;
    struct s48* r14_2;
    int64_t rax3;

    *reinterpret_cast<int32_t*>(&rdi1) = r14_2->f20;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi1) + 4) = 0;
    rax3 = fun_2750(rdi1);
    if (rax3) 
        goto 0x5439;
    goto 0x5060;
}

void file_type(int64_t rdi);

void fun_5580() {
    int64_t r14_1;

    file_type(r14_1);
    goto 0x505d;
}

void fun_7910(int32_t edi) {
    if (edi != 79) {
        if (edi) 
            goto 0x7d90;
    }
}

void fun_7dd9(int32_t edi) {
    void* rsp2;
    int64_t rbp3;
    int32_t edx4;
    int32_t v5;
    int32_t r15d6;
    int32_t r15d7;
    int64_t rax8;
    int64_t rcx9;
    int64_t rcx10;
    int32_t esi11;
    int64_t rax12;
    void* rsi13;
    void* rdi14;
    signed char* rcx15;
    int64_t r8_16;
    signed char* rdi17;
    int32_t r10d18;
    void* v19;
    uint64_t r13_20;
    int64_t r14_21;
    signed char v22;
    void* rdx23;
    int64_t r14_24;
    int64_t* r14_25;
    int64_t v26;
    void* r14_27;
    void* rax28;
    void* r14_29;
    void* rdi30;
    uint64_t rax31;
    void* rax32;
    void* rcx33;
    int32_t* r14_34;
    int32_t v35;
    void* r14_36;
    uint32_t eax37;
    unsigned char v38;
    signed char* r14_39;
    uint32_t eax40;
    void* r14_41;
    uint32_t** rax42;
    void* rdx43;
    void* rdi44;
    uint32_t** rcx45;
    int64_t r8_46;
    uint32_t eax47;
    void* r14_48;
    int1_t cf49;
    void** r14_50;
    void* r14_51;
    uint64_t r13_52;
    int64_t r13_53;
    int32_t edx54;
    uint64_t v55;
    uint64_t rdx56;
    int64_t v57;
    int64_t rax58;
    int64_t rax59;
    int32_t r8d60;
    int32_t edx61;

    rsp2 = __zero_stack_offset();
    if (edi == 69) 
        goto 0x7920;
    *reinterpret_cast<int32_t*>(&rbp3) = 9;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp3) + 4) = 0;
    edx4 = v5;
    if (reinterpret_cast<uint1_t>(r15d6 < 0) | reinterpret_cast<uint1_t>(r15d7 == 0)) {
    }
    while (1) {
        rax8 = edx4;
        if (*reinterpret_cast<int32_t*>(&rbp3) <= 9) {
            rcx9 = rax8 * 0x66666667 >> 34;
            *reinterpret_cast<int32_t*>(&rcx10) = *reinterpret_cast<int32_t*>(&rcx9) - (edx4 >> 31);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx10) + 4) = 0;
            esi11 = static_cast<int32_t>(rcx10 + rcx10 * 4);
            if (*reinterpret_cast<int32_t*>(&rbp3) == 1) 
                break;
            if (edx4 - (esi11 + esi11)) 
                goto addr_7e47_8;
        }
        *reinterpret_cast<int32_t*>(&rbp3) = *reinterpret_cast<int32_t*>(&rbp3) - 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp3) + 4) = 0;
        rax12 = rax8 * 0x66666667 >> 34;
        edx4 = *reinterpret_cast<int32_t*>(&rax12) - (edx4 >> 31);
    }
    *reinterpret_cast<int32_t*>(&rsi13) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi14) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
    goto addr_7e55_11;
    addr_7e47_8:
    rsi13 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbp3)));
    rdi14 = rsi13;
    if (!*reinterpret_cast<int32_t*>(&rbp3)) {
        *reinterpret_cast<int32_t*>(&rsi13) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0;
    } else {
        addr_7e55_11:
        rcx15 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp2) + reinterpret_cast<uint64_t>(rdi14) + 0xb0);
        *reinterpret_cast<int32_t*>(&r8_16) = static_cast<int32_t>(rbp3 - 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_16) + 4) = 0;
        rdi17 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp2) + reinterpret_cast<uint64_t>(rdi14) + 0xaf - r8_16);
        goto addr_7e72_13;
    }
    addr_7ea1_14:
    if (!r10d18) {
    }
    if (reinterpret_cast<int64_t>(v19) - r13_20 <= reinterpret_cast<uint64_t>(rsi13)) 
        goto 0x7800;
    if (r14_21) {
        if (!v22) {
            if (reinterpret_cast<uint64_t>(rsi13) >= 8) {
                rdx23 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_24 + 8) & 0xfffffffffffffff8);
                *r14_25 = v26;
                *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(r14_27) + reinterpret_cast<uint64_t>(rsi13) - 8) = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp2) + reinterpret_cast<uint64_t>(rsi13) + 0xa8);
                rax28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r14_29) - reinterpret_cast<uint64_t>(rdx23));
                rdi30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) + 0xb0 - reinterpret_cast<uint64_t>(rax28));
                rax31 = reinterpret_cast<uint64_t>(rax28) + reinterpret_cast<uint64_t>(rsi13) & 0xfffffffffffffff8;
                if (rax31 >= 8) {
                    rax32 = reinterpret_cast<void*>(rax31 & 0xfffffffffffffff8);
                    *reinterpret_cast<int32_t*>(&rcx33) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx33) + 4) = 0;
                    do {
                        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdx23) + reinterpret_cast<uint64_t>(rcx33)) = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi30) + reinterpret_cast<uint64_t>(rcx33));
                        rcx33 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx33) + 8);
                    } while (reinterpret_cast<uint64_t>(rcx33) < reinterpret_cast<uint64_t>(rax32));
                }
            } else {
                if (*reinterpret_cast<unsigned char*>(&rsi13) & 4) {
                    *r14_34 = v35;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(r14_36) + reinterpret_cast<uint64_t>(rsi13) - 4) = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rsp2) + reinterpret_cast<uint64_t>(rsi13) + 0xac);
                } else {
                    if (rsi13 && (eax37 = v38, *r14_39 = *reinterpret_cast<signed char*>(&eax37), !!(*reinterpret_cast<unsigned char*>(&rsi13) & 2))) {
                        eax40 = *reinterpret_cast<uint16_t*>(reinterpret_cast<int64_t>(rsp2) + reinterpret_cast<uint64_t>(rsi13) + 0xae);
                        *reinterpret_cast<int16_t*>(reinterpret_cast<int64_t>(r14_41) + reinterpret_cast<uint64_t>(rsi13) - 2) = *reinterpret_cast<int16_t*>(&eax40);
                    }
                }
            }
        } else {
            if (rsi13) {
                rax42 = fun_2580();
                rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
                rdx43 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi13) + 0xffffffffffffffff);
                rdi44 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) + 0xb0);
                rsi13 = rsi13;
                rcx45 = rax42;
                do {
                    *reinterpret_cast<uint32_t*>(&r8_46) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi44) + reinterpret_cast<uint64_t>(rdx43));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_46) + 4) = 0;
                    eax47 = (*rcx45)[r8_46];
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_48) + reinterpret_cast<uint64_t>(rdx43)) = *reinterpret_cast<signed char*>(&eax47);
                    cf49 = reinterpret_cast<uint64_t>(rdx43) < 1;
                    rdx43 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx43) - 1);
                } while (!cf49);
            }
        }
        r14_50 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r14_51) + reinterpret_cast<uint64_t>(rsi13));
    }
    r13_52 = r13_53 + reinterpret_cast<uint64_t>(rsi13);
    if (edx54 = 9 - *reinterpret_cast<int32_t*>(&rbp3), edx54 < 0) {
        if (v55 != r13_52) 
            goto 0x77c8; else 
            goto "???";
    }
    rdx56 = reinterpret_cast<uint64_t>(static_cast<int64_t>(edx54));
    if (rdx56 >= v57 - r13_52) 
        goto 0x7800;
    if (r14_50) 
        goto addr_7f53_36;
    goto 0x77c8;
    addr_7f53_36:
    if (!rdx56) 
        goto 0x77c8;
    if (1) 
        goto addr_9169_39;
    if (!0) 
        goto addr_7f77_41;
    addr_9169_39:
    fun_27a0(r14_50, r14_50);
    goto 0x77c8;
    addr_7f77_41:
    fun_27a0(r14_50, r14_50);
    goto 0x77c8;
    addr_7e72_13:
    while (--rcx15, rax58 = rax8 * 0x66666667 >> 34, *reinterpret_cast<int32_t*>(&rax59) = *reinterpret_cast<int32_t*>(&rax58) - (edx4 >> 31), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax59) + 4) = 0, r8d60 = static_cast<int32_t>(rax59 + rax59 * 4), edx61 = edx4 - (r8d60 + r8d60) + 48, *rcx15 = *reinterpret_cast<signed char*>(&edx61), edx4 = *reinterpret_cast<int32_t*>(&rax59), rcx15 != rdi17) {
        rax8 = *reinterpret_cast<int32_t*>(&rax59);
    }
    goto addr_7ea1_14;
}

void fun_7f90(int32_t edi) {
    if (edi == 69) 
        goto 0x7920;
    goto 0x7d70;
}

void fun_7fcd(int32_t edi) {
    if (edi == 69) 
        goto 0x7920;
    goto 0x7fb0;
}

void fun_7fe4(int32_t edi) {
    if (edi == 69) 
        goto 0x7920;
    goto 0x7fb0;
}

void fun_7fff(int32_t edi) {
    if (edi == 69) 
        goto 0x7920;
    goto 0x7fb0;
}

void fun_801a(int32_t edi) {
    if (edi == 69) 
        goto 0x7920;
    goto 0x7fb0;
}

int64_t mktime_z(int64_t rdi, void* rsi);

void fun_8088() {
    int64_t v1;
    int64_t rax2;
    int64_t rsi3;
    int32_t* v4;
    int64_t rcx5;

    __asm__("movdqu xmm4, [rax+0x20]");
    __asm__("movdqu xmm0, [rax]");
    __asm__("movdqu xmm2, [rax+0x10]");
    __asm__("movaps [rsp+0x70], xmm0");
    __asm__("movaps [rsp+0x80], xmm2");
    __asm__("movaps [rsp+0x40], xmm4");
    __asm__("movaps [rsp+0x90], xmm4");
    rax2 = mktime_z(v1, reinterpret_cast<int64_t>(__zero_stack_offset()) + 0x70);
    rsi3 = rax2;
    if (1) {
        *v4 = 75;
        goto 0x780b;
    } else {
        rcx5 = rsi3;
        do {
            rcx5 = (__intrinsic() >> 2) - (rcx5 >> 63);
            if (rsi3 < 0) {
            }
        } while (rcx5);
    }
}

void fun_8275() {
    goto 0x7920;
}

void fun_8293() {
    uint64_t rax1;
    int64_t v2;
    uint64_t r13_3;
    int32_t r10d4;
    int32_t r15d5;
    int64_t r14_6;
    uint64_t rdx7;
    int32_t r15d8;
    uint64_t rcx9;
    int64_t r14_10;
    signed char* r14_11;
    int32_t r15d12;
    signed char* r15_13;
    int64_t r14_14;
    int32_t r10d15;
    int32_t r10d16;
    void** r14_17;
    void** r14_18;

    rax1 = v2 - r13_3;
    if (r10d4 == 45 || r15d5 < 0) {
        if (rax1 <= 1) 
            goto 0x7800;
        if (!r14_6) 
            goto addr_8d08_4;
    } else {
        rdx7 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r15d8));
        *reinterpret_cast<int32_t*>(&rcx9) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
        if (rdx7) {
            rcx9 = rdx7;
        }
        if (rcx9 >= rax1) 
            goto 0x7800;
        if (!r14_10) 
            goto addr_8308_9; else 
            goto addr_82ce_10;
    }
    addr_8300_11:
    *r14_11 = 9;
    addr_8308_9:
    goto 0x77c8;
    addr_8d08_4:
    goto addr_8308_9;
    addr_82ce_10:
    if (r15d12 > 1) {
        r15_13 = reinterpret_cast<signed char*>(r14_14 + (rdx7 - 1));
        if (r10d15 == 48 || r10d16 == 43) {
            r14_11 = r15_13;
            fun_27a0(r14_17, r14_17);
            goto addr_8300_11;
        } else {
            r14_11 = r15_13;
            fun_27a0(r14_18, r14_18);
            goto addr_8300_11;
        }
    }
}

struct s49 {
    signed char[20] pad20;
    uint32_t f14;
};

void fun_8330(int32_t edi) {
    int64_t rcx2;
    struct s49* v3;
    uint32_t eax4;
    uint32_t eax5;
    int64_t rdx6;
    int32_t r10d7;
    int32_t r10d8;
    int32_t v9;

    if (edi == 69) 
        goto 0x7d81;
    *reinterpret_cast<uint32_t*>(&rcx2) = v3->f14;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx2) + 4) = 0;
    eax4 = static_cast<uint32_t>(rcx2 + 0x76c);
    eax5 = (eax4 - (eax4 + reinterpret_cast<uint1_t>(eax4 < eax4 + reinterpret_cast<uint1_t>(eax4 < 0x76c))) & 0xffffff9d) + *reinterpret_cast<uint32_t*>(&rcx2);
    rdx6 = reinterpret_cast<int32_t>(eax5) * 0x51eb851f >> 37;
    if (r10d7) {
        if (r10d8 != 43) {
            addr_8396_4:
            goto 0x7d63;
        } else {
            addr_8f22_5:
        }
        if (99 >= reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rdx6) - (reinterpret_cast<int32_t>(eax5) >> 31) + 19)) 
            goto 0x8e2c;
        goto 0x7d63;
    } else {
        if (v9 == 43) 
            goto addr_8f22_5; else 
            goto addr_8396_4;
    }
}

void fun_83a8(int32_t edi) {
    uint32_t r8d2;
    unsigned char v3;
    int64_t rax4;
    int32_t v5;
    void** v6;
    void** r10d7;
    int64_t v8;
    int64_t v9;
    void** rax10;
    void** r10d11;
    void** r10d12;
    uint32_t r8d13;
    int32_t r15d14;
    void** v15;
    void** rdx16;
    int32_t r15d17;
    void** rax18;
    void** r15_19;
    void* v20;
    uint64_t r13_21;
    int64_t r14_22;
    void** v23;
    void* r14_24;
    int64_t v25;
    void** r14_26;
    void** r14_27;
    void** r14_28;
    int64_t rax29;
    int32_t v30;
    void** v31;
    int64_t v32;

    if (edi) 
        goto 0x7920;
    r8d2 = v3;
    *reinterpret_cast<int32_t*>(&rax4) = v5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    rax10 = __strftime_internal_isra_0(0, 0xffffffffffffffff, "%m/%d/%y", v6, *reinterpret_cast<unsigned char*>(&r8d2), r10d7, -1, v8, rax4, v9, __return_address());
    r10d11 = r10d12;
    r8d13 = r8d2;
    if (r10d11 == 45 || r15d14 < 0) {
        v15 = rax10;
        *reinterpret_cast<int32_t*>(&rdx16) = 0;
        *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
    } else {
        rdx16 = reinterpret_cast<void**>(static_cast<int64_t>(r15d17));
        rax18 = rdx16;
        if (reinterpret_cast<unsigned char>(rax10) >= reinterpret_cast<unsigned char>(rdx16)) {
            rax18 = rax10;
        }
        v15 = rax18;
    }
    r15_19 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v20) - r13_21);
    if (reinterpret_cast<unsigned char>(r15_19) <= reinterpret_cast<unsigned char>(v15)) 
        goto 0x7800;
    if (r14_22) {
        if (reinterpret_cast<unsigned char>(rdx16) > reinterpret_cast<unsigned char>(rax10)) {
            v23 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r14_24) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx16) - reinterpret_cast<unsigned char>(rax10)));
            if (r10d11 == 48 || r10d11 == 43) {
                v25 = 0x8d5e;
                fun_27a0(r14_26, r14_26);
                r14_27 = v23;
                r8d13 = r8d13;
                r10d11 = r10d11;
            } else {
                v25 = 0x849a;
                fun_27a0(r14_28, r14_28);
                r14_27 = v23;
                r10d11 = r10d11;
                r8d13 = r8d13;
            }
        }
        *reinterpret_cast<int32_t*>(&rax29) = v30;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax29) + 4) = 0;
        __strftime_internal_isra_0(r14_27, r15_19, "%m/%d/%y", v31, *reinterpret_cast<unsigned char*>(&r8d13), r10d11, -1, v32, rax29, v25, __return_address());
    }
    goto 0x77c8;
}

void fun_851b(int32_t edi) {
    if (edi == 69) 
        goto 0x7920;
    goto 0x7fb0;
}

void fun_8539(int32_t edi) {
    if (edi == 79) 
        goto 0x7d81;
}

void fun_85c7() {
    goto 0x7fb0;
}

void fun_8631(int32_t edi) {
    if (edi == 69) 
        goto 0x7920;
    goto 0x7d70;
}

void fun_8665() {
    uint64_t rax1;
    int64_t v2;
    uint64_t r13_3;
    int32_t r10d4;
    int32_t r15d5;
    int64_t r14_6;
    uint64_t rdx7;
    int32_t r15d8;
    uint64_t rcx9;
    int64_t r14_10;
    int32_t r15d11;
    signed char* r15_12;
    int64_t r14_13;
    int32_t r10d14;
    int32_t r10d15;
    signed char* r14_16;
    void** r14_17;
    void** r14_18;

    rax1 = v2 - r13_3;
    if (r10d4 == 45 || r15d5 < 0) {
        if (rax1 <= 1) 
            goto 0x7800;
        if (!r14_6) 
            goto addr_8d25_4;
    } else {
        rdx7 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r15d8));
        *reinterpret_cast<int32_t*>(&rcx9) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
        if (rdx7) {
            rcx9 = rdx7;
        }
        if (rcx9 >= rax1) 
            goto 0x7800;
        if (!r14_10) 
            goto 0x8308;
        if (r15d11 > 1) {
            r15_12 = reinterpret_cast<signed char*>(r14_13 + (rdx7 - 1));
            if (r10d14 == 48 || r10d15 == 43) {
                r14_16 = r15_12;
                fun_27a0(r14_17, r14_17);
            } else {
                r14_16 = r15_12;
                fun_27a0(r14_18, r14_18);
            }
        }
    }
    *r14_16 = 10;
    goto 0x8308;
    addr_8d25_4:
    goto 0x8308;
}

void fun_86ff(int32_t edi) {
    int32_t r10d2;
    int32_t r10d3;
    int32_t v4;

    if (edi == 69) 
        goto 0x7d81;
    if (edi == 79) 
        goto 0x7920;
    if (r10d2) {
        if (r10d3 == 43) 
            goto "???";
        goto 0x7d63;
    } else {
        if (v4 == 43) {
            goto 0x8e6f;
        }
    }
}

void fun_8769() {
    uint32_t edi1;
    unsigned char v2;
    signed char r8b3;
    void** rdi4;
    void** r12_5;
    void** rax6;
    uint32_t r8d7;
    unsigned char r8b8;
    int32_t r10d9;
    int32_t r15d10;
    void** v11;
    void** r15_12;
    int32_t r15d13;
    void** rax14;
    void* v15;
    uint64_t r13_16;
    int64_t r14_17;
    void** r15_18;
    void* r14_19;
    void** r14_20;
    void** r14_21;
    void** r14_22;
    signed char* r15_23;
    uint32_t** rax24;
    uint32_t** rsi25;
    int64_t rdx26;
    void* r12_27;
    uint32_t eax28;
    int1_t cf29;
    void** r12_30;
    signed char* r15_31;
    uint32_t** rax32;
    uint32_t** rsi33;
    int64_t rdx34;
    void* r12_35;
    uint32_t eax36;
    int1_t cf37;

    edi1 = v2;
    if (r8b3) {
        edi1 = 0;
    }
    rdi4 = r12_5;
    rax6 = fun_26e0(rdi4);
    r8d7 = r8b8;
    if (r10d9 == 45 || r15d10 < 0) {
        v11 = rax6;
        *reinterpret_cast<int32_t*>(&r15_12) = 0;
        *reinterpret_cast<int32_t*>(&r15_12 + 4) = 0;
    } else {
        r15_12 = reinterpret_cast<void**>(static_cast<int64_t>(r15d13));
        rax14 = r15_12;
        if (reinterpret_cast<unsigned char>(rax6) >= reinterpret_cast<unsigned char>(r15_12)) {
            rax14 = rax6;
        }
        v11 = rax14;
    }
    if (reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(v15) - r13_16) <= reinterpret_cast<unsigned char>(v11)) 
        goto 0x7800;
    if (r14_17) {
        if (reinterpret_cast<unsigned char>(rax6) < reinterpret_cast<unsigned char>(r15_12)) {
            r15_18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r14_19) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r15_12) - reinterpret_cast<unsigned char>(rax6)));
            if (r10d9 == 48 || r10d9 == 43) {
                rdi4 = r14_20;
                r14_21 = r15_18;
                fun_27a0(rdi4, rdi4);
                r8d7 = *reinterpret_cast<unsigned char*>(&r8d7);
            } else {
                rdi4 = r14_22;
                r14_21 = r15_18;
                fun_27a0(rdi4, rdi4);
                r8d7 = *reinterpret_cast<unsigned char*>(&r8d7);
            }
        }
        if (*reinterpret_cast<unsigned char*>(&r8d7)) {
            r15_23 = reinterpret_cast<signed char*>(rax6 + 0xffffffffffffffff);
            if (rax6) {
                rax24 = fun_2a70(rdi4, rdi4);
                rsi25 = rax24;
                do {
                    *reinterpret_cast<uint32_t*>(&rdx26) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_27) + reinterpret_cast<uint64_t>(r15_23));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx26) + 4) = 0;
                    eax28 = (*rsi25)[rdx26];
                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_21) + reinterpret_cast<uint64_t>(r15_23)) = *reinterpret_cast<signed char*>(&eax28);
                    cf29 = reinterpret_cast<uint64_t>(r15_23) < 1;
                    --r15_23;
                } while (!cf29);
            }
        } else {
            if (!*reinterpret_cast<signed char*>(&edi1)) {
                fun_2880(r14_21, r12_30, rax6);
            } else {
                r15_31 = reinterpret_cast<signed char*>(rax6 + 0xffffffffffffffff);
                if (rax6) {
                    rax32 = fun_2580();
                    rsi33 = rax32;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdx34) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_35) + reinterpret_cast<uint64_t>(r15_31));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx34) + 4) = 0;
                        eax36 = (*rsi33)[rdx34];
                        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_21) + reinterpret_cast<uint64_t>(r15_31)) = *reinterpret_cast<signed char*>(&eax36);
                        cf37 = reinterpret_cast<uint64_t>(r15_31) < 1;
                        --r15_31;
                    } while (!cf37);
                }
            }
        }
    }
    goto 0x77c8;
}

void fun_88bd(int32_t edi) {
    if (edi == 69) 
        goto 0x7920;
    goto 0x7fb0;
}

void fun_88db(int32_t edi) {
    int32_t r10d2;

    if (edi == 69) 
        goto 0x7920;
    if (!r10d2) {
    }
    goto 0x7fb0;
}

void fun_88f1(int32_t edi) {
    if (edi == 69) 
        goto 0x7920;
    goto 0x7d70;
}

void fun_8925(int32_t edi) {
    if (edi == 69) 
        goto 0x7920;
    goto 0x8618;
}

struct s50 {
    signed char[1] pad1;
    unsigned char f1;
};

struct s51 {
    signed char[32] pad32;
    int32_t f20;
    signed char[4] pad40;
    int64_t f28;
};

void fun_893b() {
    uint32_t eax1;
    struct s50* rbx2;
    uint64_t r11_3;
    int64_t rbx4;
    struct s51* v5;
    int64_t rdx6;
    uint64_t rax7;
    int32_t eax8;
    uint64_t r9_9;

    eax1 = rbx2->f1;
    *reinterpret_cast<int32_t*>(&r11_3) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_3) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&eax1) == 58) {
        while (++r11_3, eax1 = *reinterpret_cast<unsigned char*>(rbx4 + r11_3), *reinterpret_cast<signed char*>(&eax1) == 58) {
        }
    }
    if (*reinterpret_cast<signed char*>(&eax1) != 0x7a) 
        goto 0x7920;
    if (v5->f20 < 0) 
        goto 0x77c8;
    rdx6 = v5->f28;
    if (*reinterpret_cast<int32_t*>(&rdx6) >= 0 && !*reinterpret_cast<int32_t*>(&rdx6)) {
    }
    rax7 = *reinterpret_cast<int32_t*>(&rdx6) * 0xffffffff88888889 >> 32;
    eax8 = (*reinterpret_cast<int32_t*>(&rax7) + *reinterpret_cast<int32_t*>(&rdx6) >> 5) - (*reinterpret_cast<int32_t*>(&rdx6) >> 31);
    r9_9 = eax8 * 0xffffffff88888889 >> 32;
    if (r11_3 == 2) {
        addr_9006_10:
        goto 0x7d70;
    } else {
        if (r11_3 > 2) {
            if (r11_3 != 3) 
                goto 0x7920;
            if (*reinterpret_cast<int32_t*>(&rdx6) - eax8 * 60) 
                goto addr_9006_10;
        } else {
            if (!r11_3) {
                goto 0x7d70;
            }
        }
    }
    if (!(eax8 - ((*reinterpret_cast<int32_t*>(&r9_9) + eax8 >> 5) - (eax8 >> 31)) * 60)) {
        goto 0x7d70;
    }
    goto 0x7d70;
}

void fun_8a29() {
    goto 0x895c;
}

void fun_8a55() {
    goto 0x83bf;
}

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_2a40(int64_t rdi, void** rsi);

uint32_t fun_2a30(void** rdi, void** rsi);

void fun_94a5() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_26c0();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_26c0();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_26e0(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_97a3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_97a3_22; else 
                            goto addr_9b9d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_9c5d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_9fb0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_97a0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_97a0_30; else 
                                goto addr_9fc9_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_26e0(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_9fb0_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2800(rdi39, rsi30, rdx10, rdi39, rsi30, rdx10);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_9fb0_28; else 
                            goto addr_964c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_a110_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_9f90_40:
                        if (r11_27 == 1) {
                            addr_9b1d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_a0d8_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_9757_44;
                            }
                        } else {
                            goto addr_9fa0_46;
                        }
                    } else {
                        addr_a11f_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_9b1d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_97a3_22:
                                if (v47 != 1) {
                                    addr_9cf9_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_26e0(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_9d44_54;
                                    }
                                } else {
                                    goto addr_97b0_56;
                                }
                            } else {
                                addr_9755_57:
                                ebp36 = 0;
                                goto addr_9757_44;
                            }
                        } else {
                            addr_9f84_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_a11f_47; else 
                                goto addr_9f8e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_9b1d_41;
                        if (v47 == 1) 
                            goto addr_97b0_56; else 
                            goto addr_9cf9_52;
                    }
                }
                addr_9811_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_96a8_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_96cd_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_99d0_65;
                    } else {
                        addr_9839_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_a088_67;
                    }
                } else {
                    goto addr_9830_69;
                }
                addr_96e1_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_972c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_a088_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_972c_81;
                }
                addr_9830_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_96cd_64; else 
                    goto addr_9839_66;
                addr_9757_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_980f_91;
                if (v22) 
                    goto addr_976f_93;
                addr_980f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_9811_62;
                addr_9d44_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_a4cb_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_a53b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_a33f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2a40(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2a30(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_9e3e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_97fc_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_9e48_112;
                    }
                } else {
                    addr_9e48_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_9f19_114;
                }
                addr_9808_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_980f_91;
                while (1) {
                    addr_9f19_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_a427_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_9e86_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_a435_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_9f07_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_9f07_128;
                        }
                    }
                    addr_9eb5_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_9f07_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_9e86_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_9eb5_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_972c_81;
                addr_a435_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_a088_67;
                addr_a4cb_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_9e3e_109;
                addr_a53b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_9e3e_109;
                addr_97b0_56:
                rax93 = fun_2a80(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_97fc_110;
                addr_9f8e_59:
                goto addr_9f90_40;
                addr_9c5d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_97a3_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_9808_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_9755_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_97a3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_9ca2_160;
                if (!v22) 
                    goto addr_a077_162; else 
                    goto addr_a283_163;
                addr_9ca2_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_a077_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_a088_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_9b4b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_99b3_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_96e1_70; else 
                    goto addr_99c7_169;
                addr_9b4b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_96a8_63;
                goto addr_9830_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_9f84_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_a0bf_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_97a0_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_9698_178; else 
                        goto addr_a042_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_9f84_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_97a3_22;
                }
                addr_a0bf_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_97a0_30:
                    r8d42 = 0;
                    goto addr_97a3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_9811_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_a0d8_42;
                    }
                }
                addr_9698_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_96a8_63;
                addr_a042_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_9fa0_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_9811_62;
                } else {
                    addr_a052_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_97a3_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_a802_188;
                if (v28) 
                    goto addr_a077_162;
                addr_a802_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_99b3_168;
                addr_964c_37:
                if (v22) 
                    goto addr_a643_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_9663_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_a110_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_a19b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_97a3_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_9698_178; else 
                        goto addr_a177_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_9f84_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_97a3_22;
                }
                addr_a19b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_97a3_22;
                }
                addr_a177_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_9fa0_46;
                goto addr_a052_186;
                addr_9663_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_97a3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_97a3_22; else 
                    goto addr_9674_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_a74e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_a5d4_210;
            if (1) 
                goto addr_a5d2_212;
            if (!v29) 
                goto addr_a20e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_26d0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_a741_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_99d0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_978b_219; else 
            goto addr_99ea_220;
        addr_976f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_9783_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_99ea_220; else 
            goto addr_978b_219;
        addr_a33f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_99ea_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_26d0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_a35d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_26d0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_a7d0_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_a236_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_a427_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_9783_221;
        addr_a283_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_9783_221;
        addr_99c7_169:
        goto addr_99d0_65;
        addr_a74e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_99ea_220;
        goto addr_a35d_222;
        addr_a5d4_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_a62e_236;
        fun_2710();
        rsp25 = rsp25 - 8 + 8;
        goto addr_a7d0_225;
        addr_a5d2_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_a5d4_210;
        addr_a20e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_a5d4_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_a236_226;
        }
        addr_a741_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_9b9d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x130ac + rax113 * 4) + 0x130ac;
    addr_9fc9_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x131ac + rax114 * 4) + 0x131ac;
    addr_a643_190:
    addr_978b_219:
    goto 0x9470;
    addr_9674_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x12fac + rax115 * 4) + 0x12fac;
    addr_a62e_236:
    goto v116;
}

void fun_9690() {
}

void fun_9848() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x9542;
}

void fun_98a1() {
    goto 0x9542;
}

void fun_998e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x9811;
    }
    if (v2) 
        goto 0xa283;
    if (!r10_3) 
        goto addr_a3ee_5;
    if (!v4) 
        goto addr_a2be_7;
    addr_a3ee_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_a2be_7:
    goto 0x96c4;
}

void fun_99ac() {
}

void fun_9a57() {
    signed char v1;

    if (v1) {
        goto 0x99df;
    } else {
        goto 0x971a;
    }
}

void fun_9a71() {
    signed char v1;

    if (!v1) 
        goto 0x9a6a; else 
        goto "???";
}

void fun_9a98() {
    goto 0x99b3;
}

void fun_9b18() {
}

void fun_9b30() {
}

void fun_9b5f() {
    goto 0x99b3;
}

void fun_9bb1() {
    goto 0x9b40;
}

void fun_9be0() {
    goto 0x9b40;
}

void fun_9c13() {
    goto 0x9b40;
}

void fun_9fe0() {
    goto 0x9698;
}

void fun_a2de() {
    signed char v1;

    if (v1) 
        goto 0xa283;
    goto 0x96c4;
}

void fun_a385() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x96c4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x96a8;
        goto 0x96c4;
    }
}

void fun_a7a2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x9a10;
    } else {
        goto 0x9542;
    }
}

void fun_bd48() {
    fun_26c0();
}

int32_t fun_25a0(int64_t rdi);

struct s52 {
    signed char[1] pad1;
    signed char f1;
};

struct s53 {
    signed char[72] pad72;
    unsigned char f48;
};

void fun_e3c8() {
    int64_t rdi1;
    int64_t r15_2;
    int64_t r12_3;
    int64_t rbp4;
    int64_t rbp5;
    int64_t rsi6;
    int32_t eax7;
    int64_t rdx8;
    int64_t rbp9;
    void** rsi10;
    int64_t rbp11;
    int64_t rbp12;
    int64_t rsi13;
    int64_t rbp14;
    int64_t rbp15;
    int64_t rsi16;
    int64_t rbp17;
    int64_t rbp18;
    void** rcx19;
    uint64_t r15_20;
    int64_t r12_21;
    void** rax22;
    int64_t rbp23;
    int64_t rbp24;
    int64_t rbp25;
    uint64_t r13_26;
    int64_t rbp27;
    int64_t rbx28;
    int64_t rbx29;
    void** rax30;
    void** rcx31;
    void* rbx32;
    void* rbx33;
    void** tmp64_34;
    void* r12_35;
    void** rax36;
    void** rbx37;
    int64_t r15_38;
    int64_t rbp39;
    void** r15_40;
    void** rax41;
    void** rax42;
    int64_t r12_43;
    void** r15_44;
    void** r12_45;
    int64_t rbp46;
    int64_t rbp47;
    uint32_t eax48;
    struct s53* r14_49;
    int32_t eax50;
    int64_t rbp51;

    rdi1 = r15_2 + r12_3;
    if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 1) {
        *reinterpret_cast<int64_t*>(rbp5 - 0x418) = rsi6;
        eax7 = fun_25a0(rdi1);
        *reinterpret_cast<int32_t*>(&rdx8) = *reinterpret_cast<int32_t*>(rbp9 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        rsi10 = *reinterpret_cast<void***>(rbp11 - 0x418);
        if (*reinterpret_cast<int32_t*>(&rdx8) < 0) 
            goto addr_e553_5;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 2) {
            *reinterpret_cast<int64_t*>(rbp12 - 0x418) = rsi13;
            eax7 = fun_25a0(rdi1);
            rsi10 = *reinterpret_cast<void***>(rbp14 - 0x418);
        } else {
            *reinterpret_cast<int64_t*>(rbp15 - 0x418) = rsi16;
            eax7 = fun_25a0(rdi1);
            rsi10 = *reinterpret_cast<void***>(rbp17 - 0x418);
        }
        *reinterpret_cast<int32_t*>(&rdx8) = *reinterpret_cast<int32_t*>(rbp18 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rdx8) < 0) 
            goto addr_e553_5;
    }
    rcx19 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rdx8)));
    if (reinterpret_cast<unsigned char>(rcx19) < reinterpret_cast<unsigned char>(rsi10)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx19) + r15_20 + r12_21)) 
            goto 0x2add;
    }
    if (*reinterpret_cast<int32_t*>(&rdx8) >= eax7) {
        addr_e45d_16:
        *reinterpret_cast<int32_t*>(&rax22) = static_cast<int32_t>(rdx8 + 1);
        *reinterpret_cast<int32_t*>(&rax22 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rax22) < reinterpret_cast<unsigned char>(rsi10)) {
            **reinterpret_cast<int32_t**>(rbp23 - 0x3d0) = *reinterpret_cast<int32_t*>(rbp24 - 0x40c);
            goto 0xe937;
        }
    } else {
        addr_e455_18:
        *reinterpret_cast<int32_t*>(rbp25 - 0x3bc) = eax7;
        *reinterpret_cast<int32_t*>(&rdx8) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        goto addr_e45d_16;
    }
    if (r13_26 > 0x7ffffffe) {
        **reinterpret_cast<int32_t**>(rbp27 - 0x3d0) = 75;
        goto 0xe5a0;
    }
    if (rbx28 < 0) {
        if (rbx29 == -1) 
            goto 0xe378;
        goto 0xea12;
    }
    *reinterpret_cast<int32_t*>(&rax30) = static_cast<int32_t>(rdx8 + 2);
    *reinterpret_cast<int32_t*>(&rax30 + 4) = 0;
    rcx31 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbx32) + reinterpret_cast<uint64_t>(rbx33));
    tmp64_34 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax30) + reinterpret_cast<uint64_t>(r12_35));
    rax36 = tmp64_34;
    if (reinterpret_cast<unsigned char>(tmp64_34) < reinterpret_cast<unsigned char>(rax30)) 
        goto 0xea12;
    if (reinterpret_cast<unsigned char>(rax36) >= reinterpret_cast<unsigned char>(rcx31)) 
        goto addr_e496_26;
    rax36 = rcx31;
    addr_e496_26:
    if (reinterpret_cast<unsigned char>(rbx37) >= reinterpret_cast<unsigned char>(rax36)) 
        goto 0xe378;
    if (reinterpret_cast<unsigned char>(rcx31) >= reinterpret_cast<unsigned char>(rax36)) {
        rax36 = rcx31;
    }
    if (rax36 == 0xffffffffffffffff) 
        goto 0xea12;
    if (r15_38 != *reinterpret_cast<int64_t*>(rbp39 - 0x3e8)) {
        rax41 = fun_2940(r15_40, rax36);
        if (!rax41) 
            goto 0xea12;
        goto 0xe378;
    }
    rax42 = fun_28c0(rax36, rsi10);
    if (!rax42) 
        goto 0xea12;
    if (r12_43) 
        goto addr_e85a_36;
    goto 0xe378;
    addr_e85a_36:
    fun_2880(rax42, r15_44, r12_45, rax42, r15_44, r12_45);
    goto 0xe378;
    addr_e553_5:
    if ((*reinterpret_cast<struct s52**>(rbp46 - 0x3f0))->f1) {
        (*reinterpret_cast<struct s52**>(rbp46 - 0x3f0))->f1 = 0;
        goto 0xe378;
    }
    if (eax7 >= 0) 
        goto addr_e455_18;
    if (**reinterpret_cast<int32_t**>(rbp47 - 0x3d0)) 
        goto 0xe5a0;
    eax48 = static_cast<uint32_t>(r14_49->f48) & 0xffffffef;
    eax50 = 22;
    if (*reinterpret_cast<signed char*>(&eax48) != 99) 
        goto addr_e597_42;
    eax50 = 84;
    addr_e597_42:
    **reinterpret_cast<int32_t**>(rbp51 - 0x3d0) = eax50;
}

void fun_e4e0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
        }
    }
}

void fun_e5d0() {
    int64_t rbp1;
    int64_t rbp2;
    int64_t rsi3;
    int64_t r15_4;
    int64_t r12_5;

    __asm__("fld tword [rax+0x10]");
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) {
        __asm__("fstp tword [rsp]");
        goto 0xe715;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
            *reinterpret_cast<int64_t*>(rbp2 - 0x418) = rsi3;
            __asm__("fstp tword [rsp+0x8]");
            fun_25a0(r15_4 + r12_5);
            goto 0xe42d;
        } else {
            __asm__("fstp tword [rsp]");
            goto 0xe403;
        }
    }
}

void fun_e618() {
    int32_t* rdi1;
    int64_t r15_2;
    int64_t r12_3;
    int32_t* rsi4;
    int64_t rdi5;
    int64_t rsi6;
    int64_t rsi7;
    int64_t rbp8;
    int64_t rbp9;
    int64_t rbp10;

    rdi1 = reinterpret_cast<int32_t*>(r15_2 + r12_3);
    *rdi1 = *rsi4;
    rdi5 = reinterpret_cast<int64_t>(rdi1 + 1);
    rsi6 = rsi7 + 4;
    if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 2) {
            *reinterpret_cast<int64_t*>(rbp9 - 0x418) = rsi6;
            fun_25a0(rdi5);
            goto 0xe42d;
        }
    }
    *reinterpret_cast<int64_t*>(rbp10 - 0x418) = rsi6;
    fun_25a0(rdi5);
    goto 0xe42d;
}

void fun_e680() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0xe506;
}

void fun_e6d0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) 
        goto 0xe6b0;
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 2) 
        goto 0xe50f;
}

void fun_e780() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0xe506;
    goto 0xe6b0;
}

void fun_e813() {
    signed char* r13_1;

    *r13_1 = 76;
    goto 0xe282;
}

void fun_e9b0() {
    int64_t* rax1;
    int64_t r12_2;

    *rax1 = r12_2;
    goto 0xe937;
}

struct s54 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s55 {
    signed char[8] pad8;
    int64_t f8;
};

struct s56 {
    signed char[16] pad16;
    int64_t f10;
};

struct s57 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_f108() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s54* rcx3;
    struct s55* rcx4;
    int64_t r11_5;
    struct s56* rcx6;
    uint32_t* rcx7;
    struct s57* rax8;
    int64_t rsi9;
    int64_t r8_10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    if (rsi9 + 1 != r8_10) 
        goto 0xf0f0; else 
        goto "???";
}

struct s58 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s59 {
    signed char[8] pad8;
    int64_t f8;
};

struct s60 {
    signed char[16] pad16;
    int64_t f10;
};

struct s61 {
    signed char[16] pad16;
    int32_t f10;
};

void fun_f140() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s58* rcx3;
    struct s59* rcx4;
    int64_t r11_5;
    struct s60* rcx6;
    uint32_t* rcx7;
    struct s61* rax8;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    goto 0xf126;
}

struct s62 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s63 {
    signed char[8] pad8;
    int64_t f8;
};

struct s64 {
    signed char[16] pad16;
    int64_t f10;
};

struct s65 {
    signed char[16] pad16;
    int16_t f10;
};

void fun_f160() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s62* rcx3;
    struct s63* rcx4;
    int64_t r11_5;
    struct s64* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s65* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<int16_t*>(&edx8);
    goto 0xf126;
}

struct s66 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s67 {
    signed char[8] pad8;
    int64_t f8;
};

struct s68 {
    signed char[16] pad16;
    int64_t f10;
};

struct s69 {
    signed char[16] pad16;
    signed char f10;
};

void fun_f180() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s66* rcx3;
    struct s67* rcx4;
    int64_t r11_5;
    struct s68* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s69* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<signed char*>(&edx8);
    goto 0xf126;
}

struct s70 {
    signed char[8] pad8;
    uint64_t f8;
};

struct s71 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_f200() {
    struct s70* rcx1;
    struct s71* rcx2;

    rcx1->f8 = (reinterpret_cast<uint64_t>(rcx2->f8 + 15) & 0xfffffffffffffff0) + 16;
    __asm__("fld tword [rdx]");
    __asm__("fstp tword [rax+0x10]");
    goto 0xf126;
}

struct s72 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s73 {
    signed char[8] pad8;
    int64_t f8;
};

struct s74 {
    signed char[16] pad16;
    int64_t f10;
};

struct s75 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_f250() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s72* rcx3;
    struct s73* rcx4;
    int64_t r11_5;
    struct s74* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r9_9;
    struct s75* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r9_9;
    }
    rax10->f10 = rdx8;
    goto 0xf126;
}

void fun_f49c() {
}

void fun_f4cb() {
    goto 0xf4a8;
}

void fun_f520() {
}

void fun_f730() {
    goto 0xf523;
}

struct s76 {
    signed char[8] pad8;
    void** f8;
};

struct s77 {
    signed char[8] pad8;
    int64_t f8;
};

struct s78 {
    signed char[8] pad8;
    void** f8;
};

struct s79 {
    signed char[8] pad8;
    void** f8;
};

void fun_f7d8() {
    int64_t r11_1;
    int64_t r11_2;
    int64_t r11_3;
    void** rbp4;
    struct s76* r14_5;
    void** rsi6;
    int64_t r11_7;
    int64_t r11_8;
    int64_t r11_9;
    void** r10_10;
    void** rax11;
    void** rcx12;
    int64_t v13;
    struct s77* r15_14;
    void** rax15;
    struct s78* r14_16;
    void** r10_17;
    int64_t r13_18;
    void** rax19;
    struct s79* r14_20;
    int64_t* r14_21;

    if (r11_1 < 0) 
        goto 0xff17;
    if (reinterpret_cast<uint64_t>(r11_2 + r11_3) > 0x2e8ba2e8ba2e8ba) 
        goto 0xff17;
    rbp4 = r14_5->f8;
    rsi6 = reinterpret_cast<void**>(r11_7 + (r11_8 + r11_9 * 4) * 2 << 4);
    if (r10_10 == rbp4) {
        rax11 = fun_28c0(rsi6, rsi6);
        rcx12 = rax11;
        if (!rax11) {
            if (v13 == r15_14->f8) 
                goto 0xfd38; else 
                goto "???";
        }
    } else {
        rax15 = fun_2940(rbp4, rsi6);
        rcx12 = rax15;
        if (!rax15) 
            goto 0xff17;
        rbp4 = r14_16->f8;
        if (r10_17 == rbp4) 
            goto addr_1006a_9; else 
            goto addr_f84e_10;
    }
    addr_f9a4_11:
    rax19 = fun_2880(rcx12, rbp4, r13_18 + (r13_18 + r13_18 * 4) * 2 << 3);
    rcx12 = rax19;
    addr_f84e_10:
    r14_20->f8 = rcx12;
    goto 0xf369;
    addr_1006a_9:
    r13_18 = *r14_21;
    goto addr_f9a4_11;
}

struct s80 {
    signed char[11] pad11;
    void** fb;
};

struct s81 {
    signed char[80] pad80;
    int64_t f50;
};

struct s82 {
    signed char[80] pad80;
    int64_t f50;
};

struct s83 {
    signed char[8] pad8;
    void** f8;
};

struct s84 {
    signed char[8] pad8;
    void** f8;
};

struct s85 {
    signed char[8] pad8;
    void** f8;
};

struct s86 {
    signed char[72] pad72;
    signed char f48;
};

struct s87 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_fa61() {
    void** ecx1;
    int32_t edx2;
    struct s80* ecx3;
    uint32_t edx4;
    int64_t r13_5;
    struct s81* r12_6;
    int64_t v7;
    uint64_t r13_8;
    uint64_t v9;
    struct s82* r12_10;
    int64_t r13_11;
    void** r8_12;
    struct s83* r15_13;
    uint64_t r9_14;
    uint64_t r9_15;
    int64_t r9_16;
    uint64_t r9_17;
    void** rsi18;
    void** v19;
    uint64_t rdx20;
    uint64_t* r15_21;
    void*** rax22;
    void*** rsi23;
    uint64_t* r15_24;
    void** rax25;
    uint64_t r11_26;
    uint64_t r11_27;
    void** rdi28;
    void** r8_29;
    uint64_t rdx30;
    uint64_t* r15_31;
    void** rax32;
    struct s84* r15_33;
    void** rax34;
    uint64_t r11_35;
    void** v36;
    struct s85* r15_37;
    void*** r13_38;
    struct s86* r12_39;
    signed char bpl40;
    int64_t rax41;
    int64_t* r14_42;
    struct s87* r12_43;
    int64_t rbx44;
    uint64_t r13_45;
    uint64_t* r14_46;

    ecx1 = reinterpret_cast<void**>(12);
    if (edx2 <= 15) {
        ecx3 = reinterpret_cast<struct s80*>(0);
        *reinterpret_cast<unsigned char*>(&ecx3) = reinterpret_cast<uint1_t>(!!(edx4 & 4));
        ecx1 = reinterpret_cast<void**>(&ecx3->fb);
    }
    if (r13_5 == -1) {
        r12_6->f50 = v7;
        if (v7 == -1) 
            goto 0xf8f8;
        r13_8 = v9;
    } else {
        r12_10->f50 = r13_11;
    }
    r8_12 = r15_13->f8;
    if (r9_14 <= r13_8) {
        r9_15 = r9_16 + r9_17;
        if (r9_15 <= r13_8) {
            r9_15 = r13_8 + 1;
        }
        if (r9_15 >> 59) 
            goto 0x1004c;
        rsi18 = reinterpret_cast<void**>(r9_15 << 5);
        if (v19 != r8_12) 
            goto addr_fbbd_12;
    } else {
        addr_f764_13:
        rdx20 = *r15_21;
        rax22 = reinterpret_cast<void***>((rdx20 << 5) + reinterpret_cast<unsigned char>(r8_12));
        if (rdx20 <= r13_8) {
            do {
                ++rdx20;
                *rax22 = reinterpret_cast<void**>(0);
                rsi23 = rax22;
                rax22 = rax22 + 32;
            } while (rdx20 <= r13_8);
            *r15_24 = rdx20;
            *rsi23 = reinterpret_cast<void**>(0);
            goto addr_f79f_17;
        }
    }
    rax25 = fun_28c0(rsi18, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_27;
    rdi28 = rax25;
    r8_29 = r8_12;
    if (!rax25) 
        goto 0xfd2a;
    addr_fcd0_19:
    rdx30 = *r15_31;
    rax32 = fun_2880(rdi28, r8_29, rdx30 << 5);
    r11_26 = r11_26;
    ecx1 = ecx1;
    r8_12 = rax32;
    addr_fc06_20:
    r15_33->f8 = r8_12;
    goto addr_f764_13;
    addr_fbbd_12:
    rax34 = fun_2940(r8_12, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_35;
    r8_12 = rax34;
    if (!rax34) 
        goto 0xff17;
    if (v36 != r15_37->f8) 
        goto addr_fc06_20;
    rdi28 = r8_12;
    r8_29 = v36;
    goto addr_fcd0_19;
    addr_f79f_17:
    r13_38 = reinterpret_cast<void***>((r13_8 << 5) + reinterpret_cast<unsigned char>(r8_12));
    if (*r13_38) {
        if (*r13_38 != ecx1) {
            goto 0xf8fc;
        }
    } else {
        *r13_38 = ecx1;
    }
    r12_39->f48 = bpl40;
    rax41 = *r14_42;
    r12_43->f8 = rbx44;
    r13_45 = reinterpret_cast<uint64_t>(rax41 + 1);
    *r14_46 = r13_45;
    if (r11_26 <= r13_45) 
        goto 0xf7e0;
    goto 0xf369;
}

void fun_fa7f() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0xf748;
    if (dl2 & 4) 
        goto 0xf748;
    if (edx3 > 7) 
        goto 0xf748;
    if (dl4 & 2) 
        goto 0xf748;
    goto 0xf748;
}

void fun_fac8() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0xf748;
    if (dl2 & 4) 
        goto 0xf748;
    if (edx3 > 7) 
        goto 0xf748;
    if (dl4 & 2) 
        goto 0xf748;
    goto 0xf748;
}

void fun_fb10() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0xf748;
    if (dl2 & 4) 
        goto 0xf748;
    if (edx3 > 7) 
        goto 0xf748;
    if (dl4 & 2) 
        goto 0xf748;
    goto 0xf748;
}

void fun_fb58() {
    goto 0xf748;
}

void fun_3921() {
    goto 0x3880;
}

void fun_4650() {
    goto 0x4639;
}

void fun_51c0() {
    goto 0x50f0;
}

void fun_5260() {
    goto 0x50f0;
}

void fun_5330() {
    int64_t rsi1;
    void** rbp2;
    void** r12_3;
    void** r8_4;
    void** r9_5;

    if (rsi1 >= 0) 
        goto 0x5058;
    make_format(rbp2, r12_3, "-", "s", r8_4, r9_5, __return_address());
    goto 0x507c;
}

void fun_5370() {
    void** rbp1;
    void** r12_2;
    void** r8_3;
    void** r9_4;

    make_format(rbp1, r12_2, "-#0", "lo", r8_3, r9_4, __return_address());
    goto 0x507c;
}

void fun_5480() {
}

void fun_5590() {
    goto 0x51e4;
}

void fun_7a88(int32_t edi) {
    signed char r8b2;

    if (r8b2) {
    }
    if (edi == 69) 
        goto 0x7920; else 
        goto "???";
}

void fun_7da5(int32_t edi) {
    signed char r8b2;

    if (edi) 
        goto 0x7920;
    if (r8b2) {
    }
    goto 0x7ac2;
}

struct s88 {
    signed char[20] pad20;
    int32_t f14;
    uint32_t f18;
    int32_t f1c;
};

void fun_7bdf(int32_t edi, signed char sil) {
    int64_t rcx3;
    struct s88* v4;
    uint32_t edx5;
    int64_t rax6;
    int32_t r9d7;
    uint64_t rax8;
    int64_t rax9;
    int64_t r8_10;
    int64_t rax11;
    int32_t r9d12;
    int64_t rax13;
    uint32_t eax14;
    uint32_t eax15;
    uint32_t r11d16;
    uint32_t edx17;
    int64_t r11_18;
    uint64_t rax19;
    int64_t rax20;
    int64_t r9_21;
    int32_t eax22;
    int32_t r10d23;
    int32_t v24;
    int64_t rdx25;
    int32_t r8d26;
    int64_t rdx27;
    int32_t r10d28;
    int32_t r10d29;
    int32_t v30;

    if (edi == 69) 
        goto 0x7920;
    *reinterpret_cast<int32_t*>(&rcx3) = v4->f14;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    edx5 = v4->f1c - v4->f18 + 0x17e;
    *reinterpret_cast<uint32_t*>(&rax6) = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rcx3) >> 31) & 0x190;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    r9d7 = static_cast<int32_t>(rcx3 + rax6 - 100);
    rax8 = reinterpret_cast<int32_t>(edx5) * 0xffffffff92492493 >> 32;
    *reinterpret_cast<int32_t*>(&rax9) = (reinterpret_cast<int32_t>(*reinterpret_cast<int32_t*>(&rax8) + edx5) >> 2) - (reinterpret_cast<int32_t>(edx5) >> 31);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_10) = static_cast<int32_t>(rax9 * 8) - *reinterpret_cast<int32_t*>(&rax9);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_10) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax11) = v4->f1c - edx5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    if (static_cast<int32_t>(rax11 + r8_10 + 3) < 0) {
        r9d12 = r9d7 - 1;
        if (!(*reinterpret_cast<unsigned char*>(&r9d12) & 3) && reinterpret_cast<uint32_t>(r9d12 * 0xc28f5c29 + 0x51eb850) <= 0x28f5c28) {
            __asm__("cdq ");
        }
        *reinterpret_cast<int32_t*>(&rax13) = -1;
    } else {
        eax14 = 0x16d;
        if (!(*reinterpret_cast<unsigned char*>(&r9d7) & 3) && (eax14 = 0x16e, reinterpret_cast<uint32_t>(r9d7 * 0xc28f5c29 + 0x51eb850) <= 0x28f5c28)) {
            __asm__("cdq ");
            eax15 = reinterpret_cast<uint32_t>(r9d7 / 0x190);
            eax14 = eax15 - (eax15 + reinterpret_cast<uint1_t>(eax15 < eax15 + reinterpret_cast<uint1_t>(!!(r9d7 % 0x190)))) + 0x16e;
        }
        r11d16 = v4->f1c - eax14;
        edx17 = r11d16 - v4->f18 + 0x17e;
        *reinterpret_cast<uint32_t*>(&r11_18) = r11d16 - edx17;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_18) + 4) = 0;
        rax19 = reinterpret_cast<int32_t>(edx17) * 0xffffffff92492493 >> 32;
        *reinterpret_cast<int32_t*>(&rax20) = (reinterpret_cast<int32_t>(*reinterpret_cast<int32_t*>(&rax19) + edx17) >> 2) - (reinterpret_cast<int32_t>(edx17) >> 31);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax20) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r9_21) = static_cast<int32_t>(rax20 * 8) - *reinterpret_cast<int32_t*>(&rax20);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_21) + 4) = 0;
        eax22 = static_cast<int32_t>(r11_18 + r9_21 + 3);
        if (eax22 >= 0) {
        }
        *reinterpret_cast<int32_t*>(&rax13) = (eax22 >> 31) + 1;
    }
    if (sil == 71) {
        if (r10d23) 
            goto 0x8e58;
        if (v24 == 43) 
            goto 0x90f4;
        goto 0x7d63;
    } else {
        if (sil != 0x67) {
            goto 0x7fb0;
        } else {
            rdx25 = *reinterpret_cast<int32_t*>(&rcx3) * 0x51eb851f >> 37;
            r8d26 = *reinterpret_cast<int32_t*>(&rcx3) - (*reinterpret_cast<int32_t*>(&rdx25) - (*reinterpret_cast<int32_t*>(&rcx3) >> 31)) * 100 + *reinterpret_cast<int32_t*>(&rax13);
            rdx27 = r8d26 * 0x51eb851f >> 37;
            if (r8d26 - (*reinterpret_cast<int32_t*>(&rdx27) - (r8d26 >> 31)) * 100 < 0) {
                if (*reinterpret_cast<int32_t*>(&rcx3) >= 0xfffff894 - *reinterpret_cast<int32_t*>(&rax13)) 
                    goto 0x91fd;
                if (!r10d28) 
                    goto 0x88a8; else 
                    goto "???";
            } else {
                if (r10d29) 
                    goto 0x8e15;
                if (v30 == 43) 
                    goto 0x8e1f; else 
                    goto "???";
            }
        }
    }
}

void fun_827e() {
    int64_t rbx1;
    int64_t rbp2;
    uint64_t rax3;
    int64_t v4;
    uint64_t r13_5;
    int32_t r10d6;
    int32_t r15d7;
    int64_t r14_8;
    uint64_t rdx9;
    int32_t r15d10;
    uint64_t rcx11;
    int64_t r14_12;
    int32_t r15d13;
    uint64_t r15_14;
    int64_t r14_15;
    int32_t r10d16;
    int32_t r10d17;
    uint64_t r14_18;
    void** r14_19;
    void** r14_20;
    uint32_t eax21;
    unsigned char* rbx22;

    if (rbx1 - 1 != rbp2) {
        goto 0x7920;
    }
    rax3 = v4 - r13_5;
    if (r10d6 == 45 || r15d7 < 0) {
        if (rax3 <= 1) 
            goto 0x7800;
        if (!r14_8) 
            goto addr_8fbf_6;
    } else {
        rdx9 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r15d10));
        *reinterpret_cast<int32_t*>(&rcx11) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx11) + 4) = 0;
        if (rdx9) {
            rcx11 = rdx9;
        }
        if (rcx11 >= rax3) 
            goto 0x7800;
        if (!r14_12) 
            goto 0x8308;
        if (r15d13 > 1) {
            r15_14 = r14_15 + (rdx9 - 1);
            if (r10d16 == 48 || r10d17 == 43) {
                r14_18 = r15_14;
                fun_27a0(r14_19, r14_19);
            } else {
                r14_18 = r15_14;
                fun_27a0(r14_20, r14_20);
            }
        }
    }
    eax21 = *rbx22;
    *reinterpret_cast<signed char*>(r14_18 + 1 - 1) = *reinterpret_cast<signed char*>(&eax21);
    goto 0x8308;
    addr_8fbf_6:
    goto 0x8308;
}

void fun_8310(int32_t edi) {
    signed char r8b2;

    if (edi == 69) 
        goto 0x7920;
    if (r8b2) {
    }
    goto 0x7aa3;
}

void fun_84ea(int32_t edi, int64_t rsi) {
    int32_t r15d3;
    int32_t r10d4;
    int64_t r15_5;
    uint32_t r8d6;
    unsigned char v7;
    int64_t rax8;
    int32_t v9;
    void** v10;
    int64_t v11;
    int64_t rax12;

    if (edi) 
        goto 0x7920;
    if (r15d3 >= 0 || r10d4) {
        if (static_cast<int32_t>(r15_5 - 6) >= 0) {
        }
        goto 0x83bf;
    } else {
        r8d6 = v7;
        *reinterpret_cast<int32_t*>(&rax8) = v9;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        __strftime_internal_isra_0(0, 0xffffffffffffffff, "%Y-%m-%d", v10, *reinterpret_cast<unsigned char*>(&r8d6), 43, 4, v11, rax8, rax12, __return_address());
        goto 0x843d;
    }
}

void fun_860b(int32_t edi) {
    if (edi == 69) 
        goto 0x7920;
}

void fun_86e3(int32_t edi) {
    if (edi == 69) 
        goto 0x7920;
    goto 0x8063;
}

struct s89 {
    signed char[20] pad20;
    int32_t f14;
};

void fun_8860(int32_t edi) {
    int64_t rdx2;
    struct s89* v3;
    int64_t rax4;
    int64_t rdx5;
    int32_t r10d6;

    if (edi == 69) 
        goto 0x7d81;
    rdx2 = v3->f14;
    rax4 = rdx2;
    rdx5 = rdx2 * 0x51eb851f >> 37;
    if (*reinterpret_cast<int32_t*>(&rax4) - (*reinterpret_cast<int32_t*>(&rdx5) - (*reinterpret_cast<int32_t*>(&rax4) >> 31)) * 100 < 0 && *reinterpret_cast<int32_t*>(&rax4) <= 0xfffff893) {
    }
    if (r10d6) 
        goto 0x8e15; else 
        goto "???";
}

void fun_8a31() {
    int1_t zf1;
    signed char r8b2;

    zf1 = r8b2 == 0;
    if (!zf1) {
    }
    if (!zf1) {
    }
    goto 0x7aa3;
}

void fun_8a69() {
    goto 0x8a33;
}

void fun_98ce() {
    goto 0x9542;
}

void fun_9aa4() {
    goto 0x9a5c;
}

void fun_9b6b() {
    goto 0x9698;
}

void fun_9bbd() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x9b40;
    goto 0x976f;
}

void fun_9bef() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x9b4b;
        goto 0x9570;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x99ea;
        goto 0x978b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0xa388;
    if (r10_8 > r15_9) 
        goto addr_9ad5_9;
    addr_9ada_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0xa393;
    goto 0x96c4;
    addr_9ad5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_9ada_10;
}

void fun_9c22() {
    goto 0x9757;
}

void fun_9ff0() {
    goto 0x9757;
}

void fun_a78f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x98ac;
    } else {
        goto 0x9a10;
    }
}

void fun_be00() {
}

struct s90 {
    signed char[1] pad1;
    signed char f1;
};

void fun_e270() {
    signed char* r13_1;
    struct s90* r13_2;

    *r13_1 = 0x6c;
    r13_2->f1 = 0x6c;
}

void fun_e9be() {
    int32_t* rax1;
    int32_t r12d2;

    *rax1 = r12d2;
    goto 0xe937;
}

struct s91 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s92 {
    signed char[8] pad8;
    int64_t f8;
};

struct s93 {
    signed char[16] pad16;
    int64_t f10;
};

struct s94 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_f220() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s91* rcx3;
    struct s92* rcx4;
    int64_t r11_5;
    struct s93* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r10_9;
    struct s94* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r10_9;
    }
    rax10->f10 = rdx8;
    goto 0xf126;
}

void fun_f4d5() {
    goto 0xf4a8;
}

void fun_fe24() {
    goto 0xf748;
}

void fun_f738() {
}

void fun_fb68() {
    goto 0xf748;
}

void fun_39c1() {
    goto 0x3880;
}

void fun_3932() {
    goto 0x3880;
}

void fun_4660() {
    goto 0x4222;
}

void fun_51d0() {
    goto 0x50f0;
}

void fun_5270() {
    void** rbp1;
    void** r12_2;
    void** r8_3;
    void** r9_4;

    make_format(rbp1, r12_2, "-#0", "lx", r8_3, r9_4, __return_address());
    goto 0x507c;
}

struct s95 {
    signed char[112] pad112;
    void** f70;
};

struct s96 {
    signed char[104] pad104;
    void** f68;
};

void fun_5390() {
    void** rcx1;
    struct s95* r14_2;
    void** rdx3;
    struct s96* r14_4;
    void** rbp5;
    void** r12_6;

    rcx1 = r14_2->f70;
    rdx3 = r14_4->f68;
    out_epoch_sec(rbp5, r12_6, rdx3, rcx1);
    goto 0x508e;
}

int32_t get_quoting_style();

struct s97 {
    signed char[24] pad24;
    uint32_t f18;
};

struct s98 {
    signed char[48] pad48;
    int64_t f30;
};

void** areadlink_with_size(int64_t rdi, int64_t rsi, void** rdx, void** rcx);

void fun_5490() {
    int32_t eax1;
    int64_t rdi2;
    void** r13_3;
    void** rdx4;
    void** rcx5;
    void** r8_6;
    void** r9_7;
    void** rax8;
    void** rbp9;
    void** r12_10;
    void** r8_11;
    void** r9_12;
    void** rbp13;
    struct s97* r14_14;
    int64_t rsi15;
    struct s98* r14_16;
    int64_t r13_17;
    void** rax18;
    void** r13_19;
    void** r8_20;
    void** r9_21;
    int32_t eax22;
    int64_t rdi23;
    void** r8_24;
    void** r9_25;
    void** rax26;
    void** rbp27;
    void** r12_28;
    void** r8_29;
    void** r9_30;
    void** rbp31;

    eax1 = get_quoting_style();
    *reinterpret_cast<int32_t*>(&rdi2) = eax1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi2) + 4) = 0;
    rax8 = quotearg_style(rdi2, r13_3, rdx4, rcx5, r8_6, r9_7, __return_address());
    make_format(rbp9, r12_10, "-", "s", r8_11, r9_12, __return_address());
    fun_2960(1, rbp13, rax8, "s");
    if ((r14_14->f18 & 0xf000) != 0xa000) 
        goto 0x508e;
    rsi15 = r14_16->f30;
    rax18 = areadlink_with_size(r13_17, rsi15, rax8, "s");
    if (!rax18) {
        quotearg_style(4, r13_19, rax8, "s", r8_20, r9_21, __return_address());
        fun_26c0();
        fun_25e0();
        fun_2990();
        goto 0x508e;
    } else {
        fun_2960(1, " -> ", rax8, "s");
        eax22 = get_quoting_style();
        *reinterpret_cast<int32_t*>(&rdi23) = eax22;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi23) + 4) = 0;
        rax26 = quotearg_style(rdi23, rax18, rax8, "s", r8_24, r9_25, __return_address());
        make_format(rbp27, r12_28, "-", "s", r8_29, r9_30, __return_address());
        fun_2960(1, rbp31, rax26, "s");
        fun_25b0(rax18, rax18);
        goto 0x508e;
    }
}

void fun_55a0() {
    void** rax1;
    void** r13_2;
    void** rdx3;
    void** rcx4;
    void** r8_5;
    void** r9_6;
    void** rax7;
    int64_t rbp8;
    int64_t r12_9;
    void** rbp10;

    rax1 = fun_25e0();
    *reinterpret_cast<void***>(rax1) = reinterpret_cast<void**>(95);
    rax7 = quotearg_style(4, r13_2, rdx3, rcx4, r8_5, r9_6, __return_address());
    fun_26c0();
    fun_2990();
    *reinterpret_cast<int16_t*>(rbp8 + r12_9) = 0x73;
    fun_2960(1, rbp10, "?", rax7);
    goto 0x508e;
}

void fun_8a70() {
    goto 0x83bf;
}

void fun_9c2c() {
    goto 0x9bc7;
}

void fun_9ffa() {
    goto 0x9b1d;
}

void fun_be60() {
    fun_26c0();
    goto fun_2a20;
}

void fun_e750() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0xe506;
    goto 0xe6b0;
}

void fun_e9cc() {
    int16_t* rax1;
    int16_t r12w2;

    *rax1 = r12w2;
    goto 0xe937;
}

struct s99 {
    int32_t f0;
    int32_t f4;
};

struct s100 {
    int32_t f0;
    int32_t f4;
};

struct s101 {
    signed char[4] pad4;
    uint32_t f4;
};

struct s102 {
    signed char[8] pad8;
    int64_t f8;
};

struct s103 {
    signed char[8] pad8;
    int64_t f8;
};

struct s104 {
    signed char[4] pad4;
    uint32_t f4;
};

void fun_f1d0(struct s99* rdi, struct s100* rsi) {
    struct s101* rcx3;
    struct s102* rcx4;
    struct s103* rcx5;
    struct s104* rcx6;

    if (rcx3->f4 > 0xaf) {
        rcx4->f8 = rcx5->f8 + 8;
    } else {
        rcx6->f4 = rcx3->f4 + 16;
    }
    rdi->f0 = rsi->f0;
    rdi->f4 = rsi->f4;
    goto 0xf126;
}

void fun_f4df() {
    goto 0xf4a8;
}

void fun_fe2e() {
    goto 0xf748;
}

void fun_3943() {
    goto 0x3880;
}

void fun_4670() {
    goto 0x4639;
}

void fun_51e0() {
}

void fun_52b0() {
    void** rbp1;
    void** r12_2;
    void** r8_3;
    void** r9_4;

    make_format(rbp1, r12_2, "'-+ 0", "ld", r8_3, r9_4, __return_address());
    goto 0x507c;
}

void fun_5208() {
    signed char dl1;
    void** rbp2;
    void** r12_3;
    void** r8_4;
    void** r9_5;
    signed char dl6;
    void** rbp7;
    void** r12_8;
    void** r8_9;
    void** r9_10;

    if (dl1 == 72) {
        make_format(rbp2, r12_3, "'-0", "lu", r8_4, r9_5, __return_address());
        goto 0x507c;
    } else {
        if (dl6 != 76) 
            goto 0x50f0;
        make_format(rbp7, r12_8, "'-0", "lu", r8_9, r9_10, __return_address());
        goto 0x507c;
    }
}

struct s105 {
    signed char[96] pad96;
    void** f60;
};

struct s106 {
    signed char[88] pad88;
    void** f58;
};

void fun_53b0() {
    void** rcx1;
    struct s105* r14_2;
    void** rdx3;
    struct s106* r14_4;
    void** rbp5;
    void** r12_6;

    rcx1 = r14_2->f60;
    rdx3 = r14_4->f58;
    out_epoch_sec(rbp5, r12_6, rdx3, rcx1);
    goto 0x508e;
}

void fun_5610() {
    void** rbp1;
    void** r12_2;
    void** r8_3;
    void** r9_4;

    make_format(rbp1, r12_2, "'-0", "lu", r8_3, r9_4, __return_address());
    goto 0x507c;
}

void fun_98fd() {
    goto 0x9542;
}

void fun_9c38() {
    goto 0x9bc7;
}

void fun_a007() {
    goto 0x9b6e;
}

void fun_bea0() {
    fun_26c0();
    goto fun_2a20;
}

void fun_e9db() {
    signed char* rax1;
    signed char r12b2;

    *rax1 = r12b2;
    goto 0xe937;
}

void fun_f4e9() {
    goto 0xf4a8;
}

void fun_3954() {
    goto 0x3880;
}

void fun_4680() {
    goto 0x4639;
}

void fun_52d8() {
    signed char dl1;

    if (dl1 != 72) 
        goto 0x5214; else 
        goto "???";
}

struct s107 {
    signed char[80] pad80;
    void** f50;
};

struct s108 {
    signed char[72] pad72;
    void** f48;
};

void fun_53d0() {
    void** rcx1;
    struct s107* r14_2;
    void** rdx3;
    struct s108* r14_4;
    void** rbp5;
    void** r12_6;

    rcx1 = r14_2->f50;
    rdx3 = r14_4->f48;
    out_epoch_sec(rbp5, r12_6, rdx3, rcx1);
    goto 0x508e;
}

void filemodestring(int64_t rdi, int64_t rsi);

signed char g18162 = 0;

void fun_5638() {
    int64_t r14_1;

    filemodestring(r14_1, 0x18158);
    g18162 = 0;
    goto 0x5060;
}

void fun_992a() {
    goto 0x9542;
}

void fun_9c44() {
    goto 0x9b40;
}

void fun_bee0() {
    fun_26c0();
    goto fun_2a20;
}

void fun_f4f3() {
    goto 0xf4a8;
}

void fun_3965() {
    goto 0x3880;
}

void fun_4690() {
    goto 0x4230;
}

void fun_53f0(void** rdi, int64_t rsi) {
    void** r9_3;
    void** rbp4;
    void** r12_5;

    r9_3 = rdi;
    if (rsi < 0) {
        *reinterpret_cast<int32_t*>(&r9_3) = 0;
        *reinterpret_cast<int32_t*>(&r9_3 + 4) = 0;
    }
    out_epoch_sec(rbp4, r12_5, r9_3, 0);
    goto 0x508e;
}

void fun_994c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0xa2e0;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x9811;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x9811;
    }
    if (v11) 
        goto 0xa643;
    if (r10_12 > r15_13) 
        goto addr_a693_8;
    addr_a698_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0xa3d1;
    addr_a693_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_a698_9;
}

struct s109 {
    signed char[24] pad24;
    int64_t f18;
};

struct s110 {
    signed char[16] pad16;
    void** f10;
};

struct s111 {
    signed char[8] pad8;
    void** f8;
};

void fun_bf30() {
    int64_t r15_1;
    struct s109* rbx2;
    void** r14_3;
    struct s110* rbx4;
    void** r13_5;
    struct s111* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    void** rbp10;
    void** v11;
    int64_t v12;
    void*** v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_26c0();
    fun_2a20(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0xbf52, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_3976() {
    goto 0x3880;
}

void fun_46c0() {
    goto 0x4230;
}

void fun_bf88() {
    fun_26c0();
    goto 0xbf59;
}

void fun_46e0(void** rdi, void** rsi) {
    void** r8_3;
    void** r9_4;
    void** rbp5;
    void** r9_6;

    make_format(rdi, rsi, "-", "s", r8_3, r9_4, __return_address());
    fun_2960(1, rbp5, r9_6, "s");
    goto 0x4200;
}

struct s112 {
    signed char[32] pad32;
    void*** f20;
};

struct s113 {
    signed char[24] pad24;
    int64_t f18;
};

struct s114 {
    signed char[16] pad16;
    void** f10;
};

struct s115 {
    signed char[8] pad8;
    void** f8;
};

struct s116 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_bfc0() {
    void*** rcx1;
    struct s112* rbx2;
    int64_t r15_3;
    struct s113* rbx4;
    void** r14_5;
    struct s114* rbx6;
    void** r13_7;
    struct s115* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s116* rbx12;
    void** rax13;
    void** rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_26c0();
    fun_2a20(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0xbff4, __return_address(), rcx1);
    goto v15;
}

void fun_4718() {
    goto 0x4230;
}

void fun_c038() {
    fun_26c0();
    goto 0xbffb;
}

void fun_4738() {
    goto 0x4230;
}
