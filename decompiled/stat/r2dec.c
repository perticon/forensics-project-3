#include <stdint.h>

/* /tmp/tmprn8a__c4 @ 0x2fa0 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (dbg.main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmprn8a__c4 @ 0x9320 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x00012f2d;
        rdx = 0x00012f20;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x00012f27;
        rdx = 0x000136d0;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x00012f29;
    rdx = 0x00012f24;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0xd9a0 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = "ASCII";
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = "ASCII";
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0x2900 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmprn8a__c4 @ 0x9400 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x2aa0)() ();
    }
    rdx = 0x00012f80;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0x12f80 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x00012f31;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x000136d0;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x00012fac;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0x12fac */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x00012f27;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x000136d0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x00012f27;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x000136d0;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x000130ac;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0x130ac */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x000131ac;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0x131ac */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x000136d0;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x00012f27;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x00012f27;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x000136d0;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmprn8a__c4 @ 0x2aa0 */
 
void quotearg_buffer_restyled_cold (void) {
    /* [16] -r-x section size 54770 named .text */
    return abort ();
}

/* /tmp/tmprn8a__c4 @ 0xa820 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x2aa5)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                *((rsp + 0x10)) = rsi;
                free (r14);
                rsi = *((rsp + 0x10));
            }
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc (*((rsp + 0x10)));
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0x2aa5 */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmprn8a__c4 @ 0x2aaa */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmprn8a__c4 @ 0x25d0 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmprn8a__c4 @ 0x2ab0 */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmprn8a__c4 @ 0x2ab5 */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmprn8a__c4 @ 0x2aba */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmprn8a__c4 @ 0x2abf */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmprn8a__c4 @ 0x2ac4 */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmprn8a__c4 @ 0x2ac9 */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmprn8a__c4 @ 0x2ace */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmprn8a__c4 @ 0x2ad3 */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmprn8a__c4 @ 0x2ad8 */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmprn8a__c4 @ 0x2add */
 
void vasnprintf_cold (void) {
    return abort ();
}

/* /tmp/tmprn8a__c4 @ 0x3090 */
 
uint64_t dbg_default_format (int64_t arg3) {
    rdx = arg3;
    /* char * default_format(_Bool fs,_Bool terse,_Bool device); */
    if (dil == 0) {
        goto label_0;
    }
    rdi = "%n %i %l %t %s %S %b %f %a %c %d\n";
    if (sil != 0) {
        goto label_1;
    }
    edx = 5;
    rax = dcgettext (0, "  File: \"%n\"\n    ID: %-8i Namelen: %-7l Type: %T\nBlock size: %-10s Fundamental block size: %S\nBlocks: Total: %-10b Free: %-10f Available: %a\nInodes: Total: %-10c Free: %d\n");
    rdi = rax;
    do {
label_1:
        void (*0xc8a0)() ();
label_0:
        if (sil == 0) {
            goto label_2;
        }
        rdi = "%n %s %b %f %u %g %D %i %h %t %T %X %Y %Z %W %o\n";
    } while (1);
label_2:
    ebx = edx;
    edx = 5;
    rax = dcgettext (0, "  File: %N\n  Size: %-10s\tBlocks: %-10b IO Block: %-6o %F\n");
    rax = xstrdup (rax);
    edx = 5;
    rsi = "Device: %Hd,%Ld\tInode: %-10i  Links: %-5h Device type: %Hr,%Lr\n";
    if (bl == 0) {
    }
    r13 = "%s%s";
    rax = dcgettext (0, "Device: %Hd,%Ld\tInode: %-10i  Links: %h\n");
    eax = 0;
    rax = xasprintf (r13, rbp, rax, rcx, r8, r9);
    r12 = rax;
    free (rbp);
    edx = 5;
    rax = dcgettext (0, "Access: (%04a/%10.10A)  Uid: (%5u/%8U)   Gid: (%5g/%8G)\n");
    eax = 0;
    rax = xasprintf (r13, r12, rax, rcx, r8, r9);
    free (r12);
    edx = 5;
    rax = dcgettext (0, "Access: %x\nModify: %y\nChange: %z\n Birth: %w\n");
    eax = 0;
    rax = xasprintf (r13, rbp, rax, rcx, r8, r9);
    r12 = rax;
    free (rbp);
    rax = r12;
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0x31b0 */
 
uint64_t dbg_make_format (int64_t arg1, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    rdi = arg1;
    rdx = arg3;
    rcx = arg4;
    /* void make_format(char * pformat,size_t prefix_len,char const * allowed_flags,char const * suffix); */
    rax = rdi + rsi;
    r15 = '-+ #0I";
    r13 = rdx;
    rbp = rdi + 1;
    rbx = rbp;
    *((rsp + 8)) = rcx;
    *(rsp) = rax;
    if (rbp < rax) {
        goto label_0;
    }
    goto label_1;
    do {
        rax = strchr (r13, r12d);
        if (rax != 0) {
            *(rbp) = r14b;
            rbp++;
        }
        rbx++;
        if (*(rsp) <= rbx) {
            goto label_1;
        }
label_0:
        r12d = *(rbx);
        esi = r12d;
        r14d = r12d;
        rax = strchr (r15, esi);
    } while (rax != 0);
    rax = *(rsp);
    if (rax <= rbx) {
        goto label_1;
    }
    r12 = rax;
    eax = 0;
    r12 -= rbx;
    while (r12 != rax) {
        r14d = *((rbx + rax));
        *((rbp + rax)) = r14b;
        rax++;
    }
    rbp += r12;
label_1:
    rsi = *((rsp + 8));
    rdi = rbp;
    return strcpy ();
}

/* /tmp/tmprn8a__c4 @ 0x3270 */
 
int64_t dbg_find_bind_mount (int64_t arg1, _Bool tried_mount_list) {
    stat name_stats;
    stat dev_stats;
    uint32_t var_8h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_128h;
    rdi = arg1;
    xmm0 = tried_mount_list;
    /* char const * find_bind_mount(char const * name); */
    r12 = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x128)) = rax;
    eax = 0;
    if (*(obj.tried_mount_list.3) == 0) {
        goto label_2;
    }
label_1:
    rsi = rsp;
    rdi = r12;
    eax = stat ();
    if (eax != 0) {
        goto label_3;
    }
    rbx = mount_list.2;
    if (rbx == 0) {
        goto label_3;
    }
    r13 = rsp + 0x90;
    while ((*((rbx + 0x28)) & 1) == 0) {
label_0:
        rbx = *((rbx + 0x30));
        if (rbx == 0) {
            goto label_3;
        }
    }
    rbp = *(rbx);
    if (*(rbp) != 0x2f) {
        goto label_0;
    }
    eax = strcmp (*((rbx + 8)), r12);
    if (eax != 0) {
        goto label_0;
    }
    rsi = r13;
    rdi = rbp;
    eax = stat ();
    if (eax != 0) {
        goto label_0;
    }
    rax = *((rsp + 0x98));
    if (*((rsp + 8)) != rax) {
        goto label_0;
    }
    rax = *((rsp + 0x90));
    if (*(rsp) != rax) {
        goto label_0;
    }
    rax = *(rbx);
    goto label_4;
label_3:
    eax = 0;
label_4:
    rdx = *((rsp + 0x128));
    rdx -= *(fs:0x28);
    if (rdx != 0) {
        goto label_5;
    }
    return rax;
label_2:
    rax = read_file_system_list (0, rsi, rdx, rcx);
    *(obj.mount_list.2) = rax;
    while (1) {
        *(obj.tried_mount_list.3) = 1;
        goto label_1;
        edx = 5;
        rax = dcgettext (0, "cannot read table of mounted file systems");
        r13 = rax;
        rax = errno_location ();
        rcx = r13;
        eax = 0;
        error (0, *(rax), 0x00012bd2);
    }
label_5:
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0x33b0 */
 
int64_t dbg_human_time (int64_t arg1, int64_t arg2, timezone_t tz) {
    tm tm;
    char[21] secbuf;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_50h;
    int64_t var_68h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = tz;
    /* char * human_time(timespec t); */
    *(rsp) = rdi;
    rdi = tz.1;
    *((rsp + 8)) = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x68)) = rax;
    eax = 0;
    if (rdi == 0) {
        goto label_1;
    }
label_0:
    r12 = rsp + 0x10;
    rbx = *((rsp + 8));
    rax = localtime_rz (rdi, rsp, r12);
    if (rax == 0) {
        goto label_2;
    }
    rcx = r12;
    r9d = ebx;
    r12 = obj_str_0;
    r8 = tz.1;
    rdx = "%Y-%m-%d %H:%M:%S.%N %z";
    nstrftime (r12, 0x3d);
    do {
        rax = *((rsp + 0x68));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        rax = r12;
        return rax;
label_2:
        r12 = obj_str_0;
        rax = imaxtostr (*(rsp), rsp + 0x50, rdx);
        r9d = ebx;
        edx = 0x3d;
        rdi = r12;
        r8 = rax;
        rcx = "%s.%09d";
        esi = 1;
        eax = 0;
        sprintf_chk ();
    } while (1);
label_1:
    rax = getenv (0x00012250);
    rax = tzalloc (rax);
    *(obj.tz.1) = rax;
    rdi = rax;
    goto label_0;
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0x34b0 */
 
uint64_t dbg_format_code_offset (char * arg_1h, char * s1) {
    rdi = s1;
    /* size_t format_code_offset(char const * directive); */
    r12 = "0123456789";
    rdi++;
    strspn (rdi, '-+ #0I");
    rbx = rbp + rax + 1;
    rax = strspn (rbx, r12);
    rbx += rax;
    if (*(rbx) == 0x2e) {
        strspn (rbx + 1, r12);
        rbx = rbx + rax + 1;
    }
    rax = rbx;
    rax -= rbp;
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0x27e0 */
 
void strspn (void) {
    __asm ("bnd jmp qword [reloc.strspn]");
}

/* /tmp/tmprn8a__c4 @ 0x3510 */
 
void dbg_print_it (int64_t arg_ch, int64_t arg_10h, int64_t arg_18h, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_40h, int64_t arg_48h, int64_t arg_50h, int64_t arg_58h, int64_t arg_5ch, int64_t arg_68h, int64_t arg_70h, int64_t arg_78h, int64_t arg_80h, int64_t arg_88h, int64_t arg_90h, int64_t arg_98h, int64_t arg_a0h, int64_t arg_a8h, int64_t arg_b0h, int64_t arg_d1h, int64_t arg_d4h, int64_t arg_e0h, int64_t arg_e4h, int64_t arg_ech, int64_t arg_f0h, int64_t arg_f8h, int64_t arg_100h, int64_t arg_110h, int64_t arg_118h, int64_t arg_120h, int64_t arg_128h, int64_t arg_130h, int64_t arg_138h, int64_t arg_140h, int64_t arg_148h, int64_t arg_150h, int64_t arg_154h, int64_t arg_158h, int64_t arg_15ch, int64_t arg_1d8h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_d0h;
    int64_t var_10h_2;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* _Bool print_it(char const * format,int fd,char const * filename,_Bool (*)() print_func, const * data); */
    r13 = rdi;
    r12d = 0;
    *((rsp + 0xc)) = esi;
    *((rsp + 0x10)) = rdx;
    *(rsp) = rcx;
    *((rsp + 0x18)) = r8;
    strlen (rdi);
    rax = xmalloc (rax + 3);
    eax = *(r13);
    if (al != 0) {
        goto label_0;
    }
    goto label_8;
    do {
        rdi = stdout;
        r14 = r13 + 1;
        rcx = *((rdi + 0x28));
        if (rcx >= *((rdi + 0x30))) {
            goto label_9;
        }
        rsi = rcx + 1;
        rbx = r13;
        r13 = r14;
        *((rdi + 0x28)) = rsi;
        *(rcx) = al;
label_1:
        eax = *((rbx + 1));
        if (al == 0) {
            goto label_8;
        }
label_0:
        if (al == 0x25) {
            goto label_10;
        }
    } while (al != 0x5c);
    rbx = r13 + 1;
    if (*(obj.interpret_backslash_escapes) != 0) {
        r14d = *((r13 + 1));
        eax = r14 - 0x30;
        if (al <= 7) {
            goto label_11;
        }
        if (r14b == 0x78) {
            goto label_12;
        }
        if (r14b != 0) {
            goto label_13;
        }
        edx = 5;
        rax = dcgettext (0, "warning: backslash at end of format");
        eax = 0;
        error (0, 0, rax);
    }
    rdi = stdout;
    rax = *((rdi + 0x28));
    if (rax >= *((rdi + 0x30))) {
        goto label_14;
    }
    rdx = rax + 1;
    *((rdi + 0x28)) = rdx;
    *(rax) = 0x5c;
    rax = rbx;
    rbx = r13;
    r13 = rax;
    eax = *((rbx + 1));
    if (al != 0) {
        goto label_0;
    }
label_8:
    free (rbp);
    rsi = stdout;
    rdi = trailing_delim;
    fputs_unlocked ();
    eax = r12d;
    return rax;
label_10:
    rax = format_code_offset (r13, rsi);
    rbx = r13 + rax;
    rdx = rax;
    r15 = rax;
    r14d = *(rbx);
    memcpy (rbp, r13, rdx);
    if (r14b != 0x25) {
        if (r14b > 0x25) {
            goto label_15;
        }
        edx = 0;
        if (r14b != 0) {
            goto label_2;
        }
        rbx--;
    }
    if (r15 > 1) {
        goto label_16;
    }
    rdi = stdout;
    r13 = rbx + 1;
    rax = *((rdi + 0x28));
    if (rax >= *((rdi + 0x30))) {
        goto label_17;
    }
    rdx = rax + 1;
    *((rdi + 0x28)) = rdx;
    *(rax) = 0x25;
    goto label_1;
label_15:
    eax = r14d;
    edx = 0;
    eax &= 0xfffffffb;
    while (*(rsp) != rsi) {
label_2:
        ecx = (int32_t) r14b;
        rsi = r15;
        rdi = rbp;
        r9 = *((rsp + 0x20));
        r13 = rbx + 1;
        r8d = *((rsp + 0x1c));
        rax = *((rsp + 0x10));
        eax = void (*rax)(uint64_t*) (*((rsp + 0x20)));
        r12d |= eax;
        goto label_1;
        rsi = dbg_print_stat;
    }
    eax = *((rbx + 1));
    if (al == 0x64) {
        goto label_18;
    }
    if (al != 0x72) {
        goto label_2;
    }
label_18:
    edx = (int32_t) r14b;
    rbx++;
    r14d = eax;
    goto label_2;
label_11:
    esi = *((r13 + 2));
    eax = (int32_t) al;
    edx = rsi - 0x30;
    if (dl > 7) {
        goto label_19;
    }
    esi = *((r13 + 3));
    edx = (int32_t) dl;
    eax = rdx + rax*8;
    edx = rsi - 0x30;
    if (dl > 7) {
        goto label_20;
    }
    edx = (int32_t) dl;
    r13 += 4;
    eax = rdx + rax*8;
label_7:
    rdi = stdout;
    rdx = *((rdi + 0x28));
    if (rdx >= *((rdi + 0x30))) {
        goto label_21;
    }
    rcx = rdx + 1;
    *((rdi + 0x28)) = rcx;
    *(rdx) = al;
label_6:
    rbx = r13 - 1;
    goto label_1;
label_12:
    rax = ctype_b_loc ();
    ecx = *((r13 + 2));
    rdx = *(rax);
    rax = rcx;
    if ((*((rdx + rcx*2 + 1)) & 0x10) == 0) {
        goto label_22;
    }
    esi = rax - 0x61;
    ecx = (int32_t) cl;
    if (sil <= 5) {
        goto label_23;
    }
    eax -= 0x41;
    esi = rcx - 0x37;
    ecx -= 0x30;
    eax = esi;
    if (al > 5) {
        eax = ecx;
    }
label_4:
    esi = *((r13 + 3));
    rbx = r13 + 2;
    rcx = rsi;
    if ((*((rdx + rsi*2 + 1)) & 0x10) != 0) {
        edx = (int32_t) sil;
        esi = rsi - 0x61;
        rbx = r13 + 3;
        eax <<= 4;
        if (sil > 5) {
            goto label_24;
        }
        edx -= 0x57;
label_5:
        eax += edx;
    }
    rdi = stdout;
    r13 = rbx + 1;
    rdx = *((rdi + 0x28));
    if (rdx >= *((rdi + 0x30))) {
        goto label_25;
    }
    rcx = rdx + 1;
    *((rdi + 0x28)) = rcx;
    *(rdx) = al;
    goto label_1;
label_9:
    esi = (int32_t) al;
    rbx = r13;
    r13 = r14;
    overflow ();
    goto label_1;
label_13:
    r15d = (int32_t) r14b;
    if (r14b == 0x22) {
        goto label_26;
    }
    do {
        eax = r14 - 0x5c;
        if (al > 0x1a) {
            goto label_27;
        }
        rsi = 0x000127c0;
        eax = (int32_t) al;
        rax = *((rsi + rax*4));
        rax += rsi;
        /* switch table (27 cases) at 0x127c0 */
        void (*rax)() ();
        r15d = 0xb;
        r14d = 0xb;
label_3:
        rdi = stdout;
        r13 += 2;
        rax = *((rdi + 0x28));
        if (rax >= *((rdi + 0x30))) {
            goto label_28;
        }
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = r14b;
        goto label_1;
label_22:
        r15d = 0x78;
    } while (1);
label_14:
    esi = 0x5c;
    overflow ();
    rax = rbx;
    rbx = r13;
    r13 = rax;
    goto label_1;
label_17:
    esi = 0x25;
    overflow ();
    goto label_1;
label_27:
    edx = 5;
    rax = dcgettext (0, "warning: unrecognized escape '\\%c');
    ecx = r15d;
    eax = 0;
    error (0, 0, rax);
    goto label_3;
label_23:
    eax = rcx - 0x57;
    goto label_4;
    r15d = 0xd;
    r14d = 0xd;
    goto label_3;
    r15d = 9;
    r14d = 9;
    goto label_3;
    r15d = 8;
    r14d = 8;
    goto label_3;
    r15d = 0x1b;
    r14d = 0x1b;
    goto label_3;
    r15d = 0xc;
    r14d = 0xc;
    goto label_3;
    r15d = 0xa;
    r14d = 0xa;
    goto label_3;
label_24:
    ecx -= 0x41;
    esi = rdx - 0x37;
    edx -= 0x30;
    if (cl <= 5) {
        edx = esi;
    }
    goto label_5;
label_28:
    esi = (int32_t) r15b;
    al = overflow ();
    goto label_1;
label_21:
    esi = (int32_t) al;
    al = overflow ();
    goto label_6;
label_26:
    r15d = 0x22;
    goto label_3;
    r15d = 7;
    r14d = 7;
    goto label_3;
label_20:
    r13 += 3;
    goto label_7;
label_19:
    r13 += 2;
    goto label_7;
label_25:
    esi = (int32_t) al;
    overflow ();
    goto label_1;
label_16:
    *((rbp + r15)) = r14b;
    *((rbp + r15 + 1)) = 0;
    rax = quote (rbp, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "%s: invalid directive");
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
}

/* /tmp/tmprn8a__c4 @ 0x5000 */
 
int64_t dbg_print_stat (int64_t arg_50h, int64_t arg_8h, int64_t arg1, int64_t arg2, int64_t arg4, int64_t arg6) {
     const * data;
    rdi = arg1;
    rsi = arg2;
    rcx = arg4;
    r9 = arg6;
    /* _Bool print_stat(char * pformat,size_t prefix_len,char mod,char m,int fd,char const * filename, const * data); */
    ecx -= 0x41;
    r12 = rsi;
    rax = *((rsp + 0x50));
    r14 = *(rax);
    rdi = *((rax + 8));
    rsi = *((rax + 0x10));
    if (cl > 0x39) {
        goto label_11;
    }
    r10 = 0x000128b4;
    ecx = (int32_t) cl;
    r13 = r9;
    r8 = rsi;
    rax = *((r10 + rcx*4));
    rax += r10;
    /* switch table (58 cases) at 0x128b4 */
    void (*rax)() ();
label_3:
    rax = human_time (*((r14 + 0x68)), *((r14 + 0x70)));
label_9:
    r13 = rax;
label_6:
    rcx = 0x00012bd3;
    eax = make_format (rbp, r12, 0x00012606);
    rdx = r13;
label_0:
    rsi = rbp;
    edi = 1;
    eax = 0;
    r15d = 0;
    printf_chk ();
    do {
label_1:
        eax = r15d;
        return rax;
label_11:
        rdi = stdout;
        rax = *((rdi + 0x28));
        if (rax >= *((rdi + 0x30))) {
            goto label_12;
        }
        rdx = rax + 1;
        r15d = 0;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0x3f;
    } while (1);
    rax = 0x1fffffffffffffff;
    r13 = *((r14 + 0x38));
    rdx = r13 - 1;
    eax = 0x200;
    if (rdx > rax) {
        r13 = rax;
    }
    do {
label_2:
        rcx = 0x0001260f;
        make_format (rbp, r12, 0x00012612);
        rdx = r13;
        goto label_0;
        if (*(obj.follow_links) == 0) {
            eax = *((r14 + 0x18));
            eax &= 0xf000;
            if (eax == 0xa000) {
                goto label_13;
            }
        }
        rdi = r13;
        rax = canonicalize_file_name ();
        rbx = rax;
        if (rax == 0) {
            goto label_14;
        }
        r15d = 0;
        rax = find_bind_mount (rax);
        ebx = 0;
        *((rsp + 8)) = rax;
        free (rbx);
        rax = *((rsp + 8));
        if (rax == 0) {
            goto label_13;
        }
label_10:
        rcx = 0x00012bd3;
        *((rsp + 8)) = rax;
        eax = make_format (rbp, r12, 0x00012606);
        rdx = *((rsp + 8));
        rsi = rbp;
        eax = 0;
        edi = 1;
        printf_chk ();
        free (rbx);
        goto label_1;
        r13 = *((r14 + 8));
    } while (1);
    r13 = *((r14 + 0x10));
    goto label_2;
    r13d = *((r14 + 0x20));
    goto label_2;
    r13d = *((r14 + 0x18));
label_7:
    rcx = 0x00012608;
label_5:
    make_format (rbp, r12, 0x0001260b);
    rdx = r13;
    goto label_0;
    r13 = *(r14);
    if (dl == 0x48) {
        goto label_15;
    }
label_4:
    if (dl != 0x4c) {
        goto label_2;
    }
    rcx = 0x0001260f;
    make_format (rbp, r12, 0x00012612);
    rdx = r13;
    ebx = (int32_t) r13b;
    rdx >>= 0xc;
    dl = 0;
    edx |= ebx;
    goto label_0;
    rsi = *((r14 + 0x60));
    rdi = *((r14 + 0x58));
    goto label_3;
    r13d = *((r14 + 0x1c));
    goto label_2;
    rbx = *((r14 + 0x28));
    rcx = 0x00012608;
    make_format (rbp, r12, 0x0001260b);
    r13 = rbx;
    rbx >>= 0x20;
    r13 >>= 8;
    rdx = rbx;
    r13d &= 0xfff;
    edx &= 0xfffff000;
    edx |= r13d;
    goto label_0;
    r13 = *((r14 + 0x30));
    rcx = 0x00012603;
    make_format (rbp, r12, '-+ 0");
    rdx = r13;
    goto label_0;
    r13 = *((r14 + 0x28));
    if (dl != 0x48) {
        goto label_4;
    }
label_15:
    rbx = r13;
    rcx = 0x0001260f;
    rbx >>= 8;
    make_format (rbp, r12, 0x00012612);
    rdx = r13;
    ebx &= 0xfff;
    rdx >>= 0x20;
    edx &= 0xfffff000;
    edx |= ebx;
    goto label_0;
    rsi = *((r14 + 0x50));
    rdi = *((r14 + 0x48));
    goto label_3;
    if (rsi >= 0) {
        goto label_3;
    }
    r13 = 0x00012606;
    rcx = 0x00012bd3;
    make_format (rbp, r12, r13);
    rdx = r13;
    goto label_0;
    r13 = *((r14 + 0x40));
    goto label_2;
    r13d = *((r14 + 0x18));
    rcx = 0x00012652;
    r13d &= 0xfff;
    goto label_5;
    r15d = 0;
    out_epoch_sec (rbp, r12, *((r14 + 0x68)), *((r14 + 0x70)));
    goto label_1;
    r15d = 0;
    out_epoch_sec (rbp, r12, *((r14 + 0x58)), *((r14 + 0x60)));
    goto label_1;
    r15d = 0;
    out_epoch_sec (rbp, r12, *((r14 + 0x48)), *((r14 + 0x50)));
    goto label_1;
    r9 = rdi;
    if (rsi < 0) {
        r8d = 0;
        r9d = 0;
    }
    out_epoch_sec (rbp, r12, r9, r8);
    r15d = 0;
    goto label_1;
    edi = *((r14 + 0x1c));
    r13 = "UNKNOWN";
    rax = getpwuid ();
    if (rax == 0) {
        goto label_6;
    }
label_8:
    r13 = *(rax);
    goto label_6;
    rbx = *((r14 + 0x28));
    rcx = 0x00012608;
    make_format (rbp, r12, 0x0001260b);
    r13d = (int32_t) bl;
    rbx >>= 0xc;
    rdx = rbx;
    dl = 0;
    edx |= r13d;
    goto label_0;
    r13 = *((r14 + 0x28));
    goto label_7;
    rbx = 0x00012606;
    eax = get_quoting_style (0);
    rsi = r13;
    edi = eax;
    rax = quotearg_style ();
    r15 = rax;
    rcx = 0x00012bd3;
    eax = make_format (rbp, r12, rbx);
    rdx = r15;
    rsi = rbp;
    edi = 1;
    eax = 0;
    r15d = 0;
    printf_chk ();
    eax = *((r14 + 0x18));
    eax &= 0xf000;
    if (eax != 0xa000) {
        goto label_1;
    }
    rax = areadlink_with_size (r13, *((r14 + 0x30)), rdx, rcx, r8);
    r14 = rax;
    if (rax == 0) {
        goto label_16;
    }
    rsi = " -> ";
    edi = 1;
    eax = 0;
    printf_chk ();
    eax = get_quoting_style (0);
    rsi = r14;
    edi = eax;
    rax = quotearg_style ();
    r13 = rax;
    rcx = 0x00012bd3;
    eax = make_format (rbp, r12, rbx);
    rdx = r13;
    rsi = rbp;
    edi = 1;
    eax = 0;
    printf_chk ();
    free (r14);
    goto label_1;
    edi = *((r14 + 0x20));
    r13 = "UNKNOWN";
    rax = getgrgid ();
    if (rax != 0) {
        goto label_8;
    }
    goto label_6;
    file_type (r14);
    goto label_9;
    r13 = *(r14);
    goto label_7;
    errno_location ();
    rsi = r13;
    edi = 4;
    r15d = 1;
    *(rax) = 0x5f;
    rbx = rax;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "failed to get security context of %s");
    rcx = r13;
    eax = 0;
    error (0, *(rbx), rax);
    eax = 0x73;
    rsi = rbp;
    rdx = 0x0001262e;
    *((rbp + r12)) = ax;
    edi = 1;
    eax = 0;
    printf_chk ();
    goto label_1;
    rcx = 0x0001260f;
    make_format (rbp, r12, 0x00012612);
    edx = 0x200;
    goto label_0;
    r13 = obj_modebuf_4;
    filemodestring (r14, r13);
    *(0x00018162) = 0;
    goto label_6;
label_13:
    rax = find_mount_point (r13, r14, rdx, rcx, r8);
    rbx = rax;
    if (rax == 0) {
        goto label_17;
    }
    rax = find_bind_mount (rax);
    while (1) {
        r15d = 0;
        goto label_10;
        rax = rbx;
    }
label_12:
    esi = 0x3f;
    r15d = 0;
    overflow ();
    goto label_1;
label_16:
    rsi = r13;
    edi = 4;
    r15d = 1;
    rax = quotearg_style ();
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "cannot read symbolic link %s");
    r12 = rax;
    rax = errno_location ();
    rcx = r13;
    eax = 0;
    error (0, *(rax), r12);
    goto label_1;
label_14:
    rsi = r13;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r14 = rax;
    rax = dcgettext (0, "failed to canonicalize %s");
    r13 = rax;
    rax = errno_location ();
    rcx = r14;
    eax = 0;
    error (0, *(rax), r13);
label_17:
    r15d = 1;
    rax = 0x0001262e;
    goto label_10;
}

/* /tmp/tmprn8a__c4 @ 0x3a30 */
 
int64_t dbg_do_stat (int64_t arg_ch, int64_t arg_10h, int64_t arg_18h, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_40h, int64_t arg_48h, int64_t arg_50h, int64_t arg_58h, int64_t arg_5ch, int64_t arg_68h, int64_t arg_70h, int64_t arg_78h, int64_t arg_80h, int64_t arg_88h, int64_t arg_90h, int64_t arg_98h, int64_t arg_a0h, int64_t arg_a8h, int64_t arg_b0h, int64_t arg_d1h, int64_t arg_d4h, int64_t arg_e0h, int64_t arg_e4h, int64_t arg_ech, int64_t arg_f0h, int64_t arg_f8h, int64_t arg_100h, int64_t arg_110h, int64_t arg_118h, int64_t arg_120h, int64_t arg_128h, int64_t arg_130h, int64_t arg_138h, int64_t arg_140h, int64_t arg_148h, int64_t arg_150h, int64_t arg_154h, int64_t arg_158h, int64_t arg_15ch, int64_t arg_1d8h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    print_args pa;
    stat st;
    statx stx;
    int64_t var_d0h;
    int64_t var_1d8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* _Bool do_stat(char const * filename,char const * format,char const * format2); */
    r14 = rdx;
    r13 = rdi;
    *(rsp) = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x1d8)) = rax;
    eax = 0;
    rdi = rsp + 0xd0;
    if (*(rdi) != 0x2d) {
        goto label_4;
    }
    eax = *((r13 + 1));
    *((rsp + 0xc)) = eax;
    if (eax != 0) {
        goto label_4;
    }
    eax = 0;
    ecx = 0x20;
    *((rsp + 0x18)) = rdi;
    ebx = 0x1000;
    do {
        *(rdi) = rax;
        rcx--;
        rdi += 8;
    } while (rcx != 0);
    rax = rsp + 0x40;
    *((rsp + 0x28)) = 0xffffffffffffffff;
    *((rsp + 0x20)) = rax;
    rax = 0x00013341;
    *((rsp + 0x30)) = 0xffffffffffffffff;
    *((rsp + 0x10)) = rax;
label_2:
    eax = *(obj.force_sync);
    if (*(obj.dont_sync) == 0) {
        goto label_5;
    }
    bh |= 0x40;
    if (al == 0) {
        goto label_6;
    }
label_1:
    rbp = *(rsp);
    r12d = 0;
    eax = *(rbp);
    if (al == 0) {
        goto label_7;
    }
    r15 = obj_CSWTCH_124;
    while (al == 0x25) {
        rax = format_code_offset (rbp, rsi);
        rax += rbp;
        edx = *(rax);
        if (dl == 0) {
            goto label_7;
        }
        edx -= 0x41;
        if (dl <= 0x39) {
            edx = (int32_t) dl;
            edx = *((r15 + rdx*2));
            r12d |= edx;
        }
        rbp = rax + 1;
        eax = *((rax + 1));
        if (al == 0) {
            goto label_7;
        }
label_0:
    }
    rax = rbp;
    rbp = rax + 1;
    eax = *((rax + 1));
    if (al != 0) {
        goto label_0;
    }
label_7:
    rsi = *((rsp + 0x10));
    r8 = *((rsp + 0x18));
    ecx = r12d;
    edx = ebx;
    edi = *((rsp + 0xc));
    eax = statx ();
    esi = eax;
    if (eax < 0) {
        goto label_8;
    }
    ecx = *((rsp + 0xec));
    edx = *((rsp + 0x158));
    rdi = 0xfffff00000000000;
    eax = ecx;
    r8 = rdx;
    *((rsp + 0x58)) = ecx;
    ax &= sym.quotearg_char_mem;
    eax = *((rsp + 0x15c));
    if (ax != sym._init) {
        r14 = *(rsp);
    }
    r8 <<= 8;
    rdx <<= 0x20;
    r8d &= 0xfff00;
    rdx &= rdi;
    rdx |= r8;
    r8d = (int32_t) al;
    rax <<= 0xc;
    rdx |= r8;
    r8 = 0xffffff00000;
    rax &= r8;
    rax |= rdx;
    edx = *((rsp + 0x150));
    *((rsp + 0x40)) = rax;
    rax = *((rsp + 0xf0));
    rcx = rdx;
    rdx <<= 0x20;
    *((rsp + 0x48)) = rax;
    eax = *((rsp + 0xe0));
    rcx <<= 8;
    rdx &= rdi;
    ecx &= 0xfff00;
    *((rsp + 0x50)) = rax;
    rax = *((rsp + 0xe4));
    rdx |= rcx;
    *((rsp + 0x5c)) = rax;
    eax = *((rsp + 0x154));
    ecx = (int32_t) al;
    rax <<= 0xc;
    rdx |= rcx;
    rax &= r8;
    rax |= rdx;
    *((rsp + 0x68)) = rax;
    rax = *((rsp + 0xf8));
    *((rsp + 0x70)) = rax;
    eax = *((rsp + 0xd4));
    *((rsp + 0x78)) = rax;
    rax = *((rsp + 0x100));
    *((rsp + 0x80)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    eax = *((rsp + 0x118));
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x140));
    *((rsp + 0x98)) = rax;
    eax = *((rsp + 0x148));
    *((rsp + 0xa0)) = rax;
    rax = *((rsp + 0x130));
    *((rsp + 0xa8)) = rax;
    eax = *((rsp + 0x138));
    *((rsp + 0xb0)) = rax;
    while (1) {
        eax = print_it (r14, rsi, r13, dbg.print_stat, rsp + 0x20, r9);
        eax ^= 1;
label_3:
        rdx = *((rsp + 0x1d8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_9;
        }
        return rax;
label_5:
        if (al != 0) {
            goto label_10;
        }
label_6:
        bh |= 8;
        goto label_1;
label_4:
        ebx = *(obj.follow_links);
        eax = 0;
        ecx = 0x20;
        *((rsp + 0x18)) = rdi;
        *(rdi) = rax;
        rcx--;
        rdi += 8;
        rax = rsp + 0x40;
        *((rsp + 0x28)) = 0xffffffffffffffff;
        ebx ^= 1;
        *((rsp + 0x20)) = rax;
        ebx = (int32_t) bl;
        *((rsp + 0x10)) = r13;
        *((rsp + 0x30)) = 0xffffffffffffffff;
        ebx <<= 8;
        *((rsp + 0xc)) = 0xffffff9c;
        goto label_2;
        rax = *((rsp + 0x120));
        *((rsp + 0x28)) = rax;
        eax = *((rsp + 0x128));
        *((rsp + 0x30)) = rax;
    }
label_10:
    bh |= 0x20;
    goto label_1;
label_8:
    rax = errno_location ();
    bh &= 0x10;
    if (bh != 0) {
        edx = 5;
        rax = dcgettext (0, "cannot stat standard input");
        eax = 0;
        eax = error (0, *(rbp), rax);
        eax = 0;
        goto label_3;
    }
    rsi = r13;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "cannot statx %s");
    rcx = r12;
    eax = 0;
    eax = error (0, *(rbp), rax);
    eax = 0;
    goto label_3;
label_9:
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0x3e20 */
 
void dbg_out_epoch_sec (int64_t arg1, int64_t arg3, int64_t arg4, size_t n) {
    long var_ch;
    int64_t var_10h;
    int64_t var_18h;
    rdi = arg1;
    rdx = arg3;
    rcx = arg4;
    rsi = n;
    /* void out_epoch_sec(char * pformat,size_t prefix_len,timespec arg); */
    r15 = rdx;
    r13 = rdi;
    r12 = rdx;
    rdx = rsi;
    rbx = rcx;
    rax = memchr (rdi, 0x2e, rdx);
    if (rax != 0) {
        r10 = rax;
        *((r13 + rbp)) = 0;
        r8 = rax;
        rcx = rax;
        eax = *((rax + 1));
        r10 -= r13;
        eax -= 0x30;
        if (eax > 9) {
            eax = *((r8 - 1));
            eax -= 0x30;
            if (eax <= 9) {
                goto label_6;
            }
            r8 = rbx;
            esi = 1;
            ecx = 1;
            *((rsp + 0xc)) = 0;
            r14d = 9;
label_0:
            if (r15 < 0) {
                if (rbx != 0) {
                    goto label_7;
                }
            }
label_1:
            rcx = 0x00012603;
            *(rsp) = r8d;
            eax = make_format (r13, rbp, '-+ 0");
            rdx = r12;
            rsi = r13;
            edi = 1;
            eax = 0;
            eax = printf_chk ();
            r8d = *(rsp);
label_2:
            if (r14d != 0) {
                ecx = 9;
                ebx = *((rsp + 0xc));
                if (r14d <= ecx) {
                    ecx = r14d;
                }
                edx = 0;
                r14d -= ecx;
                __asm ("cmovs eax, edx");
                r9d = 0;
                if (eax < ebx) {
                    ebp -= eax;
                    rax = decimal_point_len;
                    rdx = (int64_t) ebp;
                    esi = rcx + rax;
                    ebp -= esi;
                    if (rdx <= rax) {
                        r9d = ebp;
                        goto label_8;
                    }
                }
label_8:
                rdx = decimal_point;
                edi = 1;
                eax = 0;
                rsi = "%s%.*d%-*.*d";
                printf_chk ();
            }
            return;
        }
        *(rsp) = r10;
        *((rsp + 0x18)) = r8;
        *((rsp + 0x10)) = r8;
        rax = strtol (r8 + 1, 0, 0xa);
        edx = 0x7fffffff;
        r10 = *(rsp);
        if (rax <= rdx) {
            rdx = rax;
        }
        *((rsp + 0xc)) = edx;
        r14 = rdx;
        if (edx == 0) {
            goto label_9;
        }
        r8 = *((rsp + 0x10));
        eax = *((r8 - 1));
        eax -= 0x30;
        if (eax > 9) {
            goto label_10;
        }
        rcx = *((rsp + 0x18));
        goto label_11;
    }
    *((rsp + 0xc)) = 0;
    r14d = 0;
label_4:
    edx = r14d;
    ecx = 1;
    do {
        ecx = rcx * 5;
        edx++;
        ecx += ecx;
    } while (edx != 9);
    rax = rbx;
    rsi = (int64_t) ecx;
    __asm ("cqo");
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    r8 = rax;
    goto label_0;
label_7:
    eax = 0x3b9aca00;
    edx:eax = (int64_t) eax;
    eax = edx:eax / ecx;
    edx = edx:eax % ecx;
    eax -= r8d;
    ecx = eax;
    rax = rbx;
    __asm ("cqo");
    rax = rdx:rax / rsi;
    rdx = rdx:rax % rsi;
    ecx += 0xffffffff;
    r8d = ecx;
    r15 -= 0xffffffffffffffff;
    r12 = r15;
    if (r15 != 0) {
        goto label_1;
    }
    *(rsp) = ecx;
    rcx = 0x000122b4;
    make_format (r13, rbp, '-+ 0");
    xmm0 = *(0x00012ac0);
    rsi = r13;
    edi = 1;
    eax = 1;
    printf_chk ();
    r8d = *(rsp);
    goto label_2;
label_6:
    r14d = 9;
label_11:
    *(r8) = 0;
    do {
        eax = *((rcx - 2));
        rcx--;
        eax -= 0x30;
    } while (eax <= 9);
    rdi = rcx;
    *(rsp) = r10;
    *((rsp + 0x10)) = rcx;
    rax = strtol (rdi, 0, 0xa);
    edx = 0x7fffffff;
    rbp = *(rsp);
    if (rax > rdx) {
        rax = rdx;
    }
    *((rsp + 0xc)) = eax;
    if (eax <= 1) {
        goto label_5;
    }
    rcx = *((rsp + 0x10));
    edx = 0;
    rsi = (int64_t) eax;
    dl = (*(rcx) == 0x30) ? 1 : 0;
    rcx += rdx;
    rdx = decimal_point_len;
    rbp -= r13;
    if (rsi <= rdx) {
        goto label_5;
    }
    eax -= edx;
    if (eax <= 1) {
        goto label_5;
    }
    eax -= r14d;
    if (eax <= 1) {
        goto label_5;
    }
    if (r13 >= rcx) {
        goto label_12;
    }
    rdx = r13;
    rdi = r13;
    r8d = 0;
    while (sil != 0x2d) {
        *(rdi) = sil;
        rdi++;
label_3:
        rdx++;
        if (rcx == rdx) {
            goto label_13;
        }
        esi = *(rdx);
    }
    r8d = 1;
    goto label_3;
label_10:
    *((rsp + 0xc)) = 0;
label_5:
    if (r14d <= 8) {
        goto label_4;
    }
    r8 = rbx;
    esi = 1;
    ecx = 1;
    goto label_0;
label_9:
    r14d = 0;
    goto label_4;
label_13:
    rbp -= r13;
    if (r8b != 0) {
        goto label_5;
    }
    do {
        r8d = eax;
        rcx = 0x00012ce1;
        rdx = 0xffffffffffffffff;
        eax = 0;
        esi = 1;
        rax = sprintf_chk ();
        rax = (int64_t) eax;
        rbp += rax;
        goto label_5;
label_12:
        rdi = r13;
        ebp = 0;
    } while (1);
}

/* /tmp/tmprn8a__c4 @ 0x41b0 */
 
int64_t dbg_print_statfs (int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg4, int64_t arg6) {
     const * data;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rcx = arg4;
    r9 = arg6;
    /* _Bool print_statfs(char * pformat,size_t prefix_len,char mod,char m,int fd,char const * filename, const * data); */
    ecx -= 0x53;
    if (cl <= 0x21) {
        rdx = 0x0001282c;
        ecx = (int32_t) cl;
        r13 = rsi;
        rax = *((rdx + rcx*4));
        rax += rdx;
        /* switch table (34 cases) at 0x1282c */
        void (*rax)() ();
    }
    rdi = stdout;
    rax = *((rdi + 0x28));
    if (rax >= *((rdi + 0x30))) {
        goto label_5;
    }
    rdx = rax + 1;
    *((rdi + 0x28)) = rdx;
    *(rax) = 0x3f;
label_4:
    eax = 0;
    return rax;
    rax = *((rsp + 0x30));
    r12 = *((rax + 0x48));
    if (r12 == 0) {
        goto label_6;
    }
label_2:
    rcx = 0x0001260f;
    do {
label_1:
        eax = make_format (rbp, r13, 0x00012612);
        rdx = r12;
        rsi = rbp;
        edi = 1;
        eax = 0;
        eax = printf_chk ();
        eax = 0;
        return rax;
        rax = *((rsp + 0x30));
        r8 = *(rax);
        if (r8 == 0x42494e4d) {
            goto label_7;
        }
        if (r8 > 0x42494e4d) {
            goto label_8;
        }
        if (r8 == 0xef53) {
            goto label_9;
        }
        if (r8 <= 0xef53) {
            r12 = "hfs+";
            if (r8 == 0x482b) {
                goto label_0;
            }
            if (r8 <= 0x482b) {
                goto label_10;
            }
            r12 = "jffs2";
            if (r8 == 0x72b6) {
                goto label_0;
            }
            if (r8 <= 0x72b6) {
                goto label_11;
            }
            r12 = "usbdevfs";
            if (r8 == 0x9fa2) {
                goto label_0;
            }
            if (r8 <= 0x9fa2) {
                goto label_12;
            }
            r12 = "affs";
            if (r8 == 0xadff) {
                goto label_0;
            }
            if (r8 != 0xef51) {
                goto label_13;
            }
            r12 = "ext2";
        } else {
            r12 = "anon-inode FS";
            if (r8 == 0x9041934) {
                goto label_0;
            }
            if (r8 <= 0x9041934) {
                goto label_14;
            }
            r12 = 0x00012427;
            if (r8 == 0x1badface) {
                goto label_0;
            }
            if (r8 <= 0x1badface) {
                goto label_15;
            }
            r12 = "inotifyfs";
            if (r8 == 0x2bad1dea) {
                goto label_0;
            }
            if (r8 <= 0x2bad1dea) {
                goto label_16;
            }
            r12 = 0x0001245f;
            if (r8 == 0x3153464a) {
                goto label_0;
            }
            if (r8 != 0x42465331) {
                goto label_17;
            }
            r12 = "befs";
        }
label_0:
        rcx = 0x00012bd3;
        rdx = 0x00012606;
    } while (1);
label_8:
    if (r8 == 0x67596969) {
        goto label_18;
    }
    if (r8 <= 0x67596969) {
        goto label_19;
    }
    eax = 0x958458f6;
    r12 = "hugetlbfs";
    if (r8 == rax) {
        goto label_0;
    }
    if (r8 <= rax) {
        goto label_20;
    }
    eax = 0xcafe4a11;
    r12 = "bpf_fs";
    if (r8 == rax) {
        goto label_0;
    }
    if (r8 <= rax) {
        goto label_21;
    }
    eax = 0xf97cff8c;
    r12 = "selinux";
    if (r8 == rax) {
        goto label_0;
    }
    if (r8 <= rax) {
        goto label_22;
    }
    eax = 0xfe534d42;
    r12 = "smb2";
    if (r8 == rax) {
        goto label_0;
    }
    rax += 0x1000000;
    if (r8 != rax) {
        goto label_23;
    }
    r12 = "cifs";
    goto label_0;
label_19:
    r12 = "zsmallocfs";
    if (r8 == 0x58295829) {
        goto label_0;
    }
    if (r8 <= 0x58295829) {
        goto label_24;
    }
    r12 = 0x000125c5;
    if (r8 == 0x62646576) {
        goto label_0;
    }
    if (r8 <= 0x62646576) {
        goto label_25;
    }
    r12 = "debugfs";
    if (r8 == 0x64626720) {
        goto label_0;
    }
    if (r8 <= 0x64626720) {
        goto label_26;
    }
    r12 = "fusectl";
    if (r8 == 0x65735543) {
        goto label_0;
    }
    if (r8 != 0x65735546) {
        goto label_27;
    }
    r12 = "fuseblk";
    goto label_0;
label_24:
    r12 = "secretmem";
    if (r8 == 0x5345434d) {
        goto label_0;
    }
    if (r8 <= 0x5345434d) {
        goto label_28;
    }
    r12 = "ntfs";
    if (r8 == 0x5346544e) {
        goto label_0;
    }
    if (r8 <= 0x5346544e) {
        goto label_29;
    }
    r12 = 0x000122ee;
    if (r8 == 0x54190100) {
        goto label_0;
    }
    if (r8 != 0x565a4653) {
        goto label_30;
    }
    r12 = "vzfs";
    goto label_0;
label_20:
    r12 = "coda";
    if (r8 == 0x73757245) {
        goto label_0;
    }
    if (r8 <= 0x73757245) {
        goto label_31;
    }
    r12 = "overlayfs";
    if (r8 == 0x794c7630) {
        goto label_0;
    }
    if (r8 <= 0x794c7630) {
        goto label_32;
    }
    eax = 0x858458f6;
    r12 = 0x0001236a;
    if (r8 == rax) {
        goto label_0;
    }
    rax += 0xb9f0f48;
    if (r8 != rax) {
        goto label_33;
    }
    r12 = "btrfs";
    goto label_0;
label_14:
    r12 = "gfs/gfs2";
    if (r8 == 0x1161970) {
        goto label_0;
    }
    if (r8 <= 0x1161970) {
        goto label_34;
    }
    r12 = "sysv2";
    if (r8 == 0x12ff7b6) {
        goto label_0;
    }
    if (r8 <= 0x12ff7b6) {
        goto label_35;
    }
    r12 = "ibrix";
    if (r8 == 0x13111a8) {
        goto label_0;
    }
    if (r8 != 0x7655821) {
        goto label_36;
    }
    r12 = 0x00012528;
    goto label_0;
label_10:
    r12 = "devpts";
    if (r8 == 0x1cd1) {
        goto label_0;
    }
    if (r8 <= 0x1cd1) {
        goto label_37;
    }
    r12 = "isofs";
    if (r8 == 0x4000) {
        goto label_0;
    }
    if (r8 <= 0x4000) {
        goto label_38;
    }
    r12 = 0x000123bd;
    if (r8 == 0x4006) {
        goto label_0;
    }
    if (r8 != 0x4244) {
        goto label_39;
    }
    r12 = 0x0001258e;
    goto label_0;
    rax = *((rsp + 0x30));
    r12 = *((rax + 0x20));
    do {
label_3:
        rcx = 0x00012603;
        rdx = '-+ 0";
        goto label_1;
        rax = *((rsp + 0x30));
        r12 = *((rax + 0x10));
    } while (1);
    rax = *((rsp + 0x30));
    r12 = *((rax + 0x28));
    goto label_2;
    rax = *((rsp + 0x30));
    r12 = *((rax + 0x30));
    goto label_3;
    rax = *((rsp + 0x30));
    r12 = *((rax + 0x18));
    goto label_3;
    rax = *((rsp + 0x30));
    rcx = 0x00012608;
    rdx = 0x0001260b;
    r12d = *((rax + 0x38));
    eax = *((rax + 0x3c));
    r12 <<= 0x20;
    r12 |= rax;
    goto label_1;
    rax = *((rsp + 0x30));
    rcx = 0x0001260f;
    rdx = 0x00012612;
    r12 = *((rax + 0x40));
    goto label_1;
    rcx = 0x00012bd3;
    *((rsp + 8)) = r9;
    eax = make_format (rdi, rsi, 0x00012606);
    rdx = *((rsp + 8));
    rsi = rbp;
    eax = 0;
    edi = 1;
    printf_chk ();
    goto label_4;
    rax = *((rsp + 0x30));
    rcx = 0x0001260f;
    rdx = 0x00012612;
    r12 = *((rax + 8));
    goto label_1;
    rax = *((rsp + 0x30));
    rcx = 0x00012608;
    rdx = 0x0001260b;
    r12 = *(rax);
    goto label_1;
label_15:
    r12 = "balloon-kvm-fs";
    if (r8 == 0x13661366) {
        goto label_0;
    }
    if (r8 <= 0x13661366) {
        goto label_40;
    }
    r12 = "mqueue";
    if (r8 == 0x19800202) {
        goto label_0;
    }
    if (r8 == 0x19830326) {
        r12 = "fhgfs";
        goto label_0;
label_33:
        if (r8 != 0x7c7c6673) {
            goto label_41;
        }
        r12 = "prl_fs";
        goto label_0;
    }
    if (r8 != 0x15013346) {
        goto label_41;
    }
    r12 = 0x000125b8;
    goto label_0;
label_40:
    r12 = "lustre";
    if (r8 == 0xbd00bd0) {
        goto label_0;
    }
    if (r8 != 0x11307854) {
        goto label_42;
    }
    r12 = "inodefs";
    goto label_0;
label_32:
    r12 = "tracefs";
    if (r8 == 0x74726163) {
        goto label_0;
    }
    if (r8 != 0x786f4256) {
        goto label_43;
    }
    r12 = "vboxsf";
    goto label_0;
label_31:
    r12 = "nsfs";
    if (r8 == 0x6e736673) {
        goto label_0;
    }
    if (r8 <= 0x6e736673) {
        goto label_44;
    }
    r12 = "squashfs";
    if (r8 == 0x73717368) {
        goto label_0;
    }
    if (r8 == 0x73727279) {
        r12 = "btrfs_test";
        goto label_0;
label_42:
        if (r8 != 0xbad1dea) {
            goto label_41;
        }
        r12 = "futexfs";
        goto label_0;
label_43:
        if (r8 != 0x7461636f) {
            goto label_41;
        }
        r12 = "ocfs2";
        goto label_0;
    }
    if (r8 != 0x73636673) {
        goto label_41;
    }
    r12 = "securityfs";
    goto label_0;
label_44:
    r12 = "binderfs";
    if (r8 == 0x6c6f6f70) {
        goto label_0;
    }
    if (r8 <= 0x6c6f6f70) {
        goto label_45;
    }
    if (r8 != 0x6e667364) {
        goto label_41;
    }
    r12 = "nfsd";
    goto label_0;
label_13:
    if (r8 != 0xadf5) {
        goto label_41;
    }
    r12 = "adfs";
    goto label_0;
label_35:
    r12 = "xenix";
    if (r8 == 0x12ff7b4) {
        goto label_0;
    }
    if (r8 != 0x12ff7b5) {
        goto label_46;
    }
    r12 = "sysv4";
    goto label_0;
label_34:
    r12 = "hostfs";
    if (r8 == 0xc0ffee) {
        goto label_0;
    }
    if (r8 <= 0xc0ffee) {
        goto label_47;
    }
    r12 = "tmpfs";
    if (r8 == 0x1021994) {
        goto label_0;
    }
    if (r8 == 0x1021997) {
        r12 = "v9fs";
        goto label_0;
label_46:
        if (r8 != 0x12fd16d) {
            goto label_41;
        }
        r12 = 0x000125fb;
        goto label_0;
    }
    if (r8 != 0xc36400) {
        goto label_41;
    }
    r12 = "ceph";
    goto label_0;
label_47:
    r12 = "cgroupfs";
    if (r8 == 0x27e0eb) {
        goto label_0;
    }
    if (r8 <= 0x27e0eb) {
        goto label_48;
    }
    if (r8 != 0x414a53) {
        goto label_41;
    }
    r12 = 0x00012309;
    goto label_0;
label_36:
    if (r8 != 0x12ff7b7) {
        goto label_41;
    }
    r12 = 0x0001235c;
    goto label_0;
label_45:
    r12 = "qnx6";
    if (r8 == 0x68191122) {
        goto label_0;
    }
    if (r8 != 0x6b414653) {
        goto label_41;
    }
    r12 = "k-afs";
    goto label_0;
label_48:
    r12 = "ecryptfs";
    if (r8 == 0xf15f) {
        goto label_0;
    }
    if (r8 != 0x11954) {
        goto label_41;
    }
    r12 = 0x000122ee;
    goto label_0;
label_27:
    if (r8 != 0x64646178) {
        goto label_41;
    }
    r12 = "daxfs";
    goto label_0;
label_39:
    if (r8 != 0x4004) {
        goto label_41;
    }
    r12 = "isofs";
    goto label_0;
label_38:
    r12 = "minix v2 (30 char.)";
    if (r8 == 0x2478) {
        goto label_0;
    }
    if (r8 != 0x3434) {
        goto label_49;
    }
    r12 = "nilfs";
    goto label_0;
label_37:
    r12 = 0x000125c6;
    if (r8 == 0x1373) {
        goto label_0;
    }
    if (r8 <= 0x1373) {
        goto label_50;
    }
    r12 = "minix";
    if (r8 == 0x137f) {
        goto label_0;
    }
    if (r8 == 0x138f) {
        r12 = "minix (30 char.)";
        goto label_0;
label_49:
        if (r8 != 0x2468) {
            goto label_41;
        }
        r12 = "minix v2";
        goto label_0;
    }
    if (r8 != 0x137d) {
        goto label_41;
    }
    r12 = 0x000123c7;
    goto label_0;
label_50:
    r12 = "autofs";
    if (r8 == 0x187) {
        goto label_0;
    }
    if (r8 <= 0x187) {
        goto label_51;
    }
    if (r8 != 0x7c0) {
        goto label_41;
    }
    r12 = "jffs";
    goto label_0;
label_23:
    rax -= 0x5bd64f9;
    if (r8 != rax) {
        goto label_41;
    }
    r12 = "hpfs";
    goto label_0;
label_29:
    r12 = 0x00012465;
    if (r8 == 0x5346414f) {
        goto label_0;
    }
    if (r8 != 0x53464846) {
        goto label_52;
    }
    r12 = "wslfs";
    goto label_0;
label_28:
    r12 = 0x000123f8;
    if (r8 == 0x45584653) {
        goto label_0;
    }
    if (r8 <= 0x45584653) {
        goto label_53;
    }
    r12 = 0x0001253f;
    if (r8 == 0x50495045) {
        goto label_0;
    }
    if (r8 == 0x52654973) {
        r12 = "reiserfs";
        goto label_0;
label_52:
        if (r8 != 0x5346314d) {
            goto label_41;
        }
        r12 = "m1fs";
        goto label_0;
    }
    if (r8 != 0x47504653) {
        goto label_41;
    }
    r12 = "gpfs";
    goto label_0;
label_53:
    r12 = "cramfs-wend";
    if (r8 == 0x453dcd28) {
        goto label_0;
    }
    if (r8 <= 0x453dcd28) {
        goto label_54;
    }
    if (r8 != 0x454d444d) {
        goto label_41;
    }
    r12 = "devmem";
    goto label_0;
label_30:
    if (r8 != 0x534f434b) {
        goto label_41;
    }
    r12 = "sockfs";
    goto label_0;
label_51:
    r12 = "qnx4";
    if (r8 == 0x2f) {
        goto label_0;
    }
    if (r8 != 0x33) {
        goto label_41;
    }
    r12 = "z3fold";
    goto label_0;
label_54:
    r12 = "smackfs";
    if (r8 == 0x43415d53) {
        goto label_0;
    }
    if (r8 != 0x444d4142) {
        goto label_41;
    }
    r12 = "dma-buf-fs";
    goto label_0;
label_22:
    rax -= 0x18871daa;
    r12 = "erofs";
    if (r8 == rax) {
        goto label_0;
    }
    rax += 0x11ff3e2e;
    if (r8 != rax) {
        goto label_55;
    }
    r12 = "f2fs";
    goto label_0;
label_21:
    rax -= 0x10329d55;
    r12 = "vmhgfs";
    if (r8 == rax) {
        goto label_0;
    }
    if (r8 <= rax) {
        goto label_56;
    }
    eax = 0xc7571590;
    r12 = "ppc-cmm-fs";
    if (r8 == rax) {
        goto label_0;
    }
    rax += 0x2276bd8;
    if (r8 == rax) {
        r12 = "logfs";
        goto label_0;
label_55:
        rax -= 0x14969e2c;
        if (r8 != rax) {
            goto label_41;
        }
        r12 = "efivarfs";
        goto label_0;
    }
    rax -= 0xa8ea2bb;
    if (r8 != rax) {
        goto label_41;
    }
    r12 = "snfs";
    goto label_0;
label_56:
    rax -= 0xff401d2;
    r12 = "panfs";
    if (r8 == rax) {
        goto label_0;
    }
    rax += 0xe26e8a;
    if (r8 != rax) {
        goto label_57;
    }
    r12 = "xenfs";
    goto label_0;
label_17:
    if (r8 != 0x2fc12fc1) {
        goto label_41;
    }
    r12 = 0x000125e5;
    goto label_0;
label_16:
    r12 = "ubifs";
    if (r8 == 0x24051905) {
        goto label_0;
    }
    if (r8 == 0x28cd3d45) {
        r12 = "cramfs";
        goto label_0;
label_57:
        rax -= 0x6b81c7f;
        if (r8 != rax) {
            goto label_41;
        }
        r12 = "vxfs";
        goto label_0;
    }
    if (r8 != 0x2011bab0) {
        goto label_41;
    }
    r12 = "exfat";
    goto label_0;
label_12:
    r12 = "proc";
    if (r8 == 0x9fa0) {
        goto label_0;
    }
    if (r8 != 0x9fa1) {
        goto label_58;
    }
    r12 = "openprom";
    goto label_0;
label_11:
    r12 = "novell";
    if (r8 == 0x564c) {
        goto label_0;
    }
    if (r8 <= 0x564c) {
        goto label_59;
    }
    r12 = 0x000124fa;
    if (r8 == 0x6969) {
        goto label_0;
    }
    if (r8 == 0x7275) {
        r12 = "romfs";
        goto label_0;
label_58:
        if (r8 != 0x9660) {
            goto label_41;
        }
        r12 = "isofs";
        goto label_0;
    }
    if (r8 != 0x5df5) {
        goto label_41;
    }
    r12 = "exofs";
    goto label_0;
label_59:
    r12 = "minix3";
    if (r8 == 0x4d5a) {
        goto label_0;
    }
    if (r8 <= 0x4d5a) {
        goto label_60;
    }
    if (r8 != 0x517b) {
        goto label_41;
    }
    r12 = 0x00012574;
    goto label_0;
label_26:
    r12 = "sysfs";
    if (r8 == 0x62656572) {
        goto label_0;
    }
    if (r8 != 0x63677270) {
        goto label_61;
    }
    r12 = "cgroup2fs";
    goto label_0;
label_25:
    r12 = "sdcardfs";
    if (r8 == 0x5dca2df5) {
        goto label_0;
    }
    if (r8 <= 0x5dca2df5) {
        goto label_62;
    }
    r12 = "pstorefs";
    if (r8 == 0x6165676c) {
        goto label_0;
    }
    if (r8 != 0x61756673) {
        goto label_63;
    }
    r12 = "aufs";
    goto label_0;
label_60:
    r12 = "hfsx";
    if (r8 == 0x4858) {
        goto label_0;
    }
    if (r8 != 0x4d44) {
        goto label_41;
    }
    r12 = "msdos";
    goto label_0;
label_61:
    if (r8 != 0x62656570) {
        goto label_41;
    }
    r12 = "configfs";
    goto label_0;
label_63:
    if (r8 != 0x61636673) {
        goto label_41;
    }
    r12 = "acfs";
    goto label_0;
label_62:
    r12 = "aafs";
    if (r8 == 0x5a3c69f0) {
        goto label_0;
    }
    if (r8 == 0x5a4f4653) {
        r12 = "zonefs";
        goto label_0;
    }
    if (r8 == 0x58465342) {
        r12 = 0x0001237e;
        goto label_0;
label_6:
        r12 = *((rax + 8));
        goto label_2;
label_5:
        esi = 0x3f;
        eax = overflow ();
        goto label_4;
label_18:
        r12 = "rpc_pipefs";
        goto label_0;
label_9:
        r12 = "ext2/ext3";
        goto label_0;
    }
label_41:
    r12 = obj_buf_5;
    rcx = "UNKNOWN (0x%lx)";
    edx = 0x1d;
    eax = 0;
    esi = 1;
    rdi = r12;
    sprintf_chk ();
    goto label_0;
label_7:
    r12 = "binfmt_misc";
    goto label_0;
}

/* /tmp/tmprn8a__c4 @ 0x2fd0 */
 
uint64_t deregister_tm_clones (void) {
    rdi = obj___progname;
    rax = obj___progname;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0x3000 */
 
int64_t register_tm_clones (void) {
    rdi = obj___progname;
    rsi = obj___progname;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0x3040 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_00002570 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmprn8a__c4 @ 0x2570 */
 
void fcn_00002570 (void) {
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmprn8a__c4 @ 0x3080 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmprn8a__c4 @ 0x6220 */
 
void dbg_argmatch_die (void) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    /* void __argmatch_die(); */
    edi = 1;
    return void (*0x5740)() ();
}

/* /tmp/tmprn8a__c4 @ 0x26c0 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmprn8a__c4 @ 0x2a20 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmprn8a__c4 @ 0x2a00 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmprn8a__c4 @ 0x2960 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmprn8a__c4 @ 0x2810 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmprn8a__c4 @ 0x2840 */
 
void strcmp (void) {
    __asm ("bnd jmp qword [reloc.strcmp]");
}

/* /tmp/tmprn8a__c4 @ 0x2950 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmprn8a__c4 @ 0x25f0 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmprn8a__c4 @ 0x6ba0 */
 
uint64_t dbg_unescape_tab (int64_t arg1) {
    rdi = arg1;
    /* void unescape_tab(char * str); */
    rbx = rdi;
    rax = strlen (rdi);
    rcx = rbx;
    edx = 0;
    rax++;
    while (r8b != 0x5c) {
label_0:
        *(rcx) = r8b;
        rcx++;
        if (rax <= rsi) {
            goto label_2;
        }
label_1:
        rdx = rsi;
        r8d = *((rbx + rdx));
        rsi = rdx + 1;
    }
    r9 = rdx + 4;
    if (r9 >= rax) {
        goto label_0;
    }
    edi = *((rbx + rsi));
    edi -= 0x30;
    if (dil > 3) {
        goto label_0;
    }
    r11d = *((rbx + rdx + 2));
    r10d = r11 - 0x30;
    if (r10b > 7) {
        goto label_0;
    }
    r11d = *((rbx + rdx + 3));
    r11d -= 0x30;
    if (r11b > 7) {
        goto label_0;
    }
    edx = r10 + rdi*8;
    rcx++;
    rsi = r9;
    edx = r11 + rdx*8;
    *((rcx - 1)) = dl;
    goto label_1;
label_2:
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0x76e0 */
 
uint32_t rotate_right32 (uint32_t value, uint32_t count) {
    const uint32_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value >> count) | (value << (-count & mask));
}
 
int64_t strftime_internal_isra_0 (int64_t arg_500h, int64_t arg_508h, int64_t arg_518h, int64_t arg1, uint32_t arg2, int64_t arg3, tm * arg4, uint32_t arg6) {
    int64_t var_1h;
    uint32_t var_8h;
    int64_t var_10h;
    int64_t var_1fh;
    tm * timeptr;
    tm * var_28h;
    int64_t var_30h;
    uint32_t var_34h;
    size_t var_38h;
    size_t var_40h;
    size_t var_50h;
    tm * var_58h;
    int64_t var_60h;
    tm * var_64h;
    tm * var_68h;
    int64_t var_70h;
    int64_t var_80h;
    int64_t var_8ch;
    int64_t var_90h;
    int64_t var_a0h;
    int64_t var_a8h;
    char * format;
    int64_t var_ach;
    int64_t var_adh;
    int64_t var_aeh;
    int64_t var_afh;
    char * s;
    void * s2;
    int64_t var_c7h;
    int64_t var_4b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r9 = arg6;
label_19:
    r14 = rdi;
    rbx = rcx;
    rax = *((rsp + 0x508));
    *((rsp + 8)) = rsi;
    *((rsp + 0x20)) = rcx;
    r15 = *((rsp + 0x500));
    *((rsp + 0x34)) = r9d;
    *((rsp + 0x28)) = rax;
    *((rsp + 0x1f)) = r8b;
    rax = *(fs:0x28);
    *((rsp + 0x4b8)) = rax;
    eax = 0;
    rax = errno_location ();
    r11 = *((rbx + 0x30));
    r10d = *((rbx + 8));
    *((rsp + 0x10)) = rax;
    eax = *(rax);
    *((rsp + 0x30)) = eax;
    rax = 0x00013341;
    if (r11 == 0) {
        r11 = rax;
    }
    if (r10d <= 0xc) {
        goto label_63;
    }
    r10d -= 0xc;
label_1:
    eax = *(rbp);
    r13d = 0;
    if (al == 0) {
        goto label_64;
    }
    *((rsp + 0x60)) = r10d;
    r12 = r11;
    do {
        if (al == 0x25) {
            goto label_65;
        }
        eax = 0;
        rdx = *((rsp + 8));
        ecx = 1;
        __asm ("cmovns rax, r15");
        if (rax != 0) {
            rcx = rax;
        }
        rdx -= r13;
        if (rcx >= rdx) {
            goto label_9;
        }
        if (r14 != 0) {
            if (r15d > 1) {
                goto label_66;
            }
label_2:
            eax = *(rbp);
            r14++;
            *((r14 - 1)) = al;
        }
        r13 += rcx;
        rbx = rbp;
label_0:
        eax = *((rbx + 1));
        rbp = rbx + 1;
        r15 = 0xffffffffffffffff;
    } while (al != 0);
label_64:
    if (r14 != 0) {
        if (*((rsp + 8)) == 0) {
            goto label_67;
        }
        *(r14) = 0;
    }
label_67:
    rax = *((rsp + 0x10));
    edi = *((rsp + 0x30));
    *(rax) = edi;
    goto label_68;
label_12:
    if (*((rsp + 8)) != r13) {
        goto label_0;
    }
    do {
label_9:
        rax = *((rsp + 0x10));
        *(rax) = 0x22;
label_62:
        r13d = 0;
label_68:
        rax = *((rsp + 0x4b8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_69;
        }
        rax = r13;
        return rax;
label_63:
        eax = 0xc;
        if (r10d == 0) {
            r10d = eax;
        }
        goto label_1;
label_66:
        rbx = rax - 1;
        *((rsp + 0x38)) = rcx;
        r14 += rbx;
        memset (r14, 0x20, rbx);
        rcx = *((rsp + 0x38));
        goto label_2;
label_65:
        eax = *((rsp + 0x1f));
        rbx = rbp;
        r8d = 0;
        r10d = 0;
        *((rsp + 0x38)) = al;
label_7:
        edx = *((rbx + 1));
        rbx++;
        ecx = rdx - 0x23;
        esi = edx;
        edi = edx;
        if (cl <= 0x3c) {
            r9 = 0x1000000000002500;
            eax = 1;
            rax <<= cl;
            if ((rax & r9) != 0) {
                goto label_70;
            }
            if (cl == 0x3b) {
                goto label_71;
            }
            eax &= 1;
            if (eax != 0) {
                goto label_72;
            }
        }
        edx -= 0x30;
        if (edx <= 9) {
            goto label_73;
        }
label_4:
        if (sil == 0x45) {
            goto label_74;
        }
        if (sil == 0x4f) {
            goto label_74;
        }
        edi = 0;
label_3:
        if (sil <= 0x7a) {
            rcx = 0x00012cf4;
            eax = (int32_t) sil;
            rax = *((rcx + rax*4));
            rax += rcx;
            /* switch table (123 cases) at 0x12cf4 */
            rax = void (*rax)() ();
            if (edi != 0x4f) {
                goto label_75;
            }
        }
label_8:
        rcx = rbx;
        rcx -= rbp;
        r8 = rcx + 1;
        if (r15d >= 0) {
            if (r10d != 0x2d) {
                rdx = (int64_t) r15d;
                r9 = rdx;
                if (r8 >= rdx) {
                    r9 = r8;
                }
            }
        } else {
            r9 = r8;
            edx = 0;
        }
        rax = *((rsp + 8));
        rax -= r13;
    } while (rax <= r9);
    if (r14 == 0) {
        goto label_76;
    }
    if (r8 < rdx) {
        rdx -= r8;
        *((rsp + 0x58)) = r9;
        *((rsp + 0x50)) = r8;
        r15 = r14 + rdx;
        *((rsp + 0x40)) = rcx;
        if (r10d == 0x30) {
            goto label_77;
        }
        if (r10d == 0x2b) {
            goto label_77;
        }
        r14 = r15;
        memset (r14, 0x20, rdx);
        r9 = *((rsp + 0x58));
        r8 = *((rsp + 0x50));
        rcx = *((rsp + 0x40));
    }
label_37:
    if (*((rsp + 0x38)) == 0) {
        goto label_78;
    }
    r15 = rcx;
    if (r8 == 0) {
        goto label_79;
    }
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r8;
    rax = ctype_toupper_loc ();
    r8 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    do {
        ecx = *((rbp + r15));
        rdx = *(rax);
        edx = *((rdx + rcx*4));
        *((r14 + r15)) = dl;
        r15--;
    } while (r15 >= 0);
    goto label_79;
label_74:
    esi = *((rbx + 1));
    rbx++;
    goto label_3;
label_78:
    rdx = r8;
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r8;
    memcpy (r14, rbp, rdx);
    r9 = *((rsp + 0x40));
    r8 = *((rsp + 0x38));
label_79:
    r14 += r8;
label_76:
    r13 += r9;
    goto label_0;
label_73:
    r15d = 0;
    goto label_80;
label_5:
    eax = *(rbx);
    eax -= 0x30;
    r15d += eax;
    if (r15d overflow 0) {
        goto label_81;
    }
label_6:
    edi = *((rbx + 1));
    rbx++;
    eax = rdi - 0x30;
    esi = edi;
    if (eax > 9) {
        goto label_4;
    }
label_80:
    r15d *= 0xa;
    if (eax !overflow 9) {
        goto label_5;
    }
label_81:
    r15d = 0x7fffffff;
    goto label_6;
label_70:
    r10d = edx;
    goto label_7;
    eax = *((rsp + 0x38));
    if (r8b != 0) {
        eax = r8d;
    }
    *((rsp + 0x38)) = al;
    if (edi == 0x45) {
        goto label_8;
    }
label_75:
    ebp = 0;
label_16:
    r11d = fcn.00002520;
    *((rsp + 0xab)) = r11w;
    if (edi != 0) {
        goto label_82;
    }
    rax = rsp + 0xad;
label_10:
    *(rax) = sil;
    *((rax + 1)) = 0;
    *((rsp + 0x40)) = r10d;
    rax = strftime (rsp + 0xb0, section..dynsym, rsp + 0xab, *((rsp + 0x20)));
    rcx = rax;
    if (rax == 0) {
        goto label_0;
    }
    r10d = *((rsp + 0x40));
    r8 = rax - 1;
    if (r10d == 0x2d) {
        goto label_83;
    }
    if (r15d < 0) {
        goto label_83;
    }
    rdx = (int64_t) r15d;
    r15 = rdx;
    if (r8 >= rdx) {
        r15 = r8;
    }
label_31:
    rax = *((rsp + 8));
    rax -= r13;
    if (rax <= r15) {
        goto label_9;
    }
    if (r14 == 0) {
        goto label_15;
    }
    if (r8 < rdx) {
        rdx -= r8;
        *((rsp + 0x58)) = r8;
        rax = r14 + rdx;
        *((rsp + 0x50)) = rcx;
        *((rsp + 0x40)) = rax;
        if (r10d == 0x30) {
            goto label_84;
        }
        if (r10d == 0x2b) {
            goto label_84;
        }
        memset (r14, 0x20, rdx);
        r14 = *((rsp + 0x40));
        r8 = *((rsp + 0x58));
        rcx = *((rsp + 0x50));
    }
label_41:
    if (bpl != 0) {
        goto label_85;
    }
    if (*((rsp + 0x38)) == 0) {
        goto label_86;
    }
    rbp = rcx - 2;
    if (r8 == 0) {
        goto label_38;
    }
    *((rsp + 0x38)) = r8;
    rax = ctype_toupper_loc ();
    r8 = *((rsp + 0x38));
    rsi = rsp + 0xb1;
    do {
        ecx = *((rsi + rbp));
        rdx = *(rax);
        edx = *((rdx + rcx*4));
        *((r14 + rbp)) = dl;
        rbp--;
    } while (rbp >= 0);
label_38:
    r14 += r8;
label_15:
    r13 += r15;
    goto label_0;
    if (edi == 0x45) {
        goto label_8;
    }
    rdx = *((rsp + 0x20));
    ecx = *((rdx + 0x14));
    r11d = *((rdx + 0x1c));
    ebp = *((rdx + 0x18));
    eax = ecx;
    edx = r11d;
    eax >>= 0x1f;
    edx -= ebp;
    edx += 0x17e;
    eax &= 0x190;
    r9d = rcx + rax - 0x64;
    rax = (int64_t) edx;
    r8d = edx;
    rax *= 0xffffffff92492493;
    r8d >>= 0x1f;
    rax >>= 0x20;
    eax += edx;
    eax >>= 2;
    eax -= r8d;
    r8d = rax*8;
    r8d -= eax;
    eax = r11d;
    eax -= edx;
    r8d = rax + r8 + 3;
    if (r8d < 0) {
        goto label_87;
    }
    eax = 0x16d;
    if ((r9b & 3) == 0) {
        edx = r9d * 0xc28f5c29;
        eax = 0x16e;
        edx += 0x51eb850;
        edx = rotate_right32 (edx, 2);
        if (edx > 0x28f5c28) {
            goto label_88;
        }
        eax = r9d;
        r9d = 0x190;
        edx:eax = (int64_t) eax;
        eax = edx:eax / r9d;
        edx = edx:eax % r9d;
        edx = -edx;
        eax -= eax;
        eax += 0x16e;
    }
label_88:
    r11d -= eax;
    edx = r11d;
    edx -= ebp;
    edx += 0x17e;
    rax = (int64_t) edx;
    r9d = edx;
    r11d -= edx;
    rax *= 0xffffffff92492493;
    r9d >>= 0x1f;
    rax >>= 0x20;
    eax += edx;
    eax >>= 2;
    eax -= r9d;
    r9d = rax*8;
    r9d -= eax;
    eax = r11 + r9 + 3;
    __asm ("cmovns r8d, eax");
    eax >>= 0x1f;
    eax++;
label_52:
    if (sil == 0x47) {
        goto label_89;
    }
    if (sil != 0x67) {
        goto label_90;
    }
    rdx = (int64_t) ecx;
    r8d = ecx;
    rdx *= 0x51eb851f;
    r8d >>= 0x1f;
    rdx >>= 0x25;
    edx -= r8d;
    r8d = ecx;
    edx *= 0x64;
    r8d -= edx;
    r8d += eax;
    rdx = (int64_t) r8d;
    r9d = r8d;
    rdx *= 0x51eb851f;
    r9d >>= 0x1f;
    rdx >>= 0x25;
    edx -= r9d;
    r9d = edx * 0x64;
    edx = r8d;
    edx -= r9d;
    if (edx < 0) {
        goto label_91;
    }
    if (r10d != 0) {
        goto label_61;
    }
    r10d = *((rsp + 0x34));
    if (r10d == 0x2b) {
        goto label_92;
    }
label_26:
    *((rsp + 0x58)) = 0;
    eax = 1;
    *((rsp + 0x40)) = 0;
    *((rsp + 0x50)) = 2;
label_18:
    r9d = 0;
label_13:
    if (edi != 0x4f) {
        goto label_93;
    }
    if (al == 0) {
        goto label_93;
    }
label_17:
    ecx = fcn.00002520;
    ebp = 0;
    *((rsp + 0xab)) = cx;
label_82:
    *((rsp + 0xad)) = dil;
    rax = rsp + 0xae;
    goto label_10;
    if (edi != 0) {
        goto label_8;
    }
    eax = *((rsp + 0x38));
    edx = fcn.00002520;
    *((rsp + 0xab)) = dx;
    if (r8b != 0) {
        eax = r8d;
    }
    ebp = 0;
    *((rsp + 0x38)) = al;
    rax = rsp + 0xad;
    goto label_10;
    if (edi == 0x45) {
        goto label_8;
    }
    eax = 9;
    edx = *((rsp + 0x510));
    if (r15d <= 0) {
        r15d = eax;
    }
    while (ebp > r15d) {
label_11:
        rax *= 0x66666667;
        ecx = edx;
        ebp--;
        ecx >>= 0x1f;
        rax >>= 0x22;
        eax -= ecx;
        edx = eax;
        rax = (int64_t) edx;
    }
    rcx = rax * 0x66666667;
    esi = edx;
    esi >>= 0x1f;
    rcx >>= 0x22;
    ecx -= esi;
    esi = rcx * 5;
    ecx = edx;
    esi += esi;
    ecx -= esi;
    if (ebp == 1) {
        goto label_94;
    }
    if (ecx == 0) {
        goto label_11;
    }
    rsi = (int64_t) ebp;
    rdi = rsi;
    if (ebp == 0) {
        goto label_95;
    }
label_51:
    rcx = rsp + rdi + 0xb0;
    r8d = rbp - 1;
    rdi = rsp + rdi + 0xaf;
    rdi -= r8;
    while (rcx != rdi) {
        rax = (int64_t) eax;
        rax *= 0x66666667;
        r8d = edx;
        rcx--;
        r8d >>= 0x1f;
        rax >>= 0x22;
        eax -= r8d;
        r8d = rax * 5;
        r8d += r8d;
        edx -= r8d;
        edx += 0x30;
        *(rcx) = dl;
        edx = eax;
    }
label_60:
    eax = 0x30;
    if (r10d == 0) {
        r10d = eax;
    }
    rax = *((rsp + 8));
    rax -= r13;
    if (rax <= rsi) {
        goto label_9;
    }
    if (r14 == 0) {
        goto label_96;
    }
    if (*((rsp + 0x38)) == 0) {
        goto label_97;
    }
    rdx = rsi - 1;
    *((rsp + 0x50)) = rdx;
    if (rsi == 0) {
        goto label_58;
    }
    *((rsp + 0x40)) = rsi;
    *((rsp + 0x38)) = r10d;
    rax = ctype_toupper_loc ();
    r10d = *((rsp + 0x38));
    rdx = *((rsp + 0x50));
    rdi = rsp + 0xb0;
    rsi = *((rsp + 0x40));
    rcx = rax;
    do {
        r8d = *((rdi + rdx));
        rax = *(rcx);
        eax = *((rax + r8*4));
        *((r14 + rdx)) = al;
        rdx--;
    } while (rdx >= 0);
label_58:
    r14 += rsi;
label_96:
    r13 += rsi;
    if (r10d == 0x2d) {
        goto label_12;
    }
    edx = r15d;
    edx -= ebp;
    if (edx < 0) {
        goto label_12;
    }
    rax = *((rsp + 8));
    rdx = (int64_t) edx;
    rax -= r13;
    if (rdx >= rax) {
        goto label_9;
    }
    if (r14 == 0) {
        goto label_98;
    }
    if (rdx == 0) {
        goto label_0;
    }
    rbp = r14 + rdx;
    r13 += rdx;
    if (r10d == 0x30) {
        goto label_99;
    }
    if (r10d == 0x2b) {
        goto label_99;
    }
    r14 = rbp;
    memset (r14, 0x20, rdx);
    goto label_0;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x50)) = 2;
    edx = *(rax);
label_14:
    eax = edx;
    *((rsp + 0x58)) = 0;
    r9d = 0;
    eax >>= 0x1f;
    *((rsp + 0x40)) = eax;
    eax = edx;
    eax = ~eax;
    eax >>= 0x1f;
    goto label_13;
    if (edi == 0x45) {
        goto label_8;
    }
    *((rsp + 0x50)) = 2;
    edx = *((rsp + 0x60));
    goto label_14;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x50)) = 2;
    edx = *((rax + 4));
    goto label_14;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x50)) = 2;
    edx = *((rax + 8));
    goto label_14;
    if (edi == 0x45) {
        goto label_8;
    }
    r11 = *((rsp + 0x20));
    eax = *((r11 + 0x18));
    r8d = *((r11 + 0x1c));
    edx = rax + 6;
    *((rsp + 0x40)) = eax;
    rax = (int64_t) edx;
    ecx = edx;
    rax *= 0xffffffff92492493;
    ecx >>= 0x1f;
    rax >>= 0x20;
    eax += edx;
    eax >>= 2;
    eax -= ecx;
    ecx = rax*8;
    ecx -= eax;
    eax = ecx;
    eax -= edx;
    eax = rax + r8 + 7;
label_24:
    rdx = (int64_t) eax;
    *((rsp + 0x50)) = 2;
    rdx *= 0xffffffff92492493;
    rdx >>= 0x20;
    edx += eax;
    eax >>= 0x1f;
    edx >>= 2;
    edx -= eax;
    goto label_14;
    rax = *((rsp + 0x20));
    *((rsp + 0x50)) = r10d;
    __asm ("movdqu xmm4, xmmword [rax + 0x20]");
    __asm ("movdqu xmm0, xmmword [rax]");
    __asm ("movdqu xmm2, xmmword [rax + 0x10]");
    rax = *((rax + 0x30));
    *((rsp + 0x70)) = xmm0;
    *((rsp + 0x80)) = xmm2;
    *((rsp + 0xa0)) = rax;
    *((rsp + 0x8c)) = 0xffffffff;
    *((rsp + 0x40)) = xmm4;
    *((rsp + 0x90)) = xmm4;
    rax = mktime_z (*((rsp + 0x28)), rsp + 0x70, rdx, rcx, r8, r9);
    r9d = *((rsp + 0x8c));
    r10d = *((rsp + 0x50));
    rsi = rax;
    if (r9d < 0) {
        goto label_100;
    }
    rax >>= 0x3f;
    rcx = rsi;
    rdi = rsp + 0xc7;
    r11 = 0x6666666666666667;
    *((rsp + 0x40)) = rax;
    r8 = rdi;
    r9d = 0x30;
    do {
        rax = rcx;
        rdx:rax = rax * r11;
        rax = rcx;
        rax >>= 0x3f;
        rdx >>= 2;
        rdx -= rax;
        rbp = rdx * 5;
        rax = rdx;
        rbp += rbp;
        rcx -= rbp;
        rdx = rcx;
        rcx = rax;
        eax = r9d;
        eax -= edx;
        edx += 0x30;
        __asm ("cmovs edx, eax");
        r8--;
        *(r8) = dl;
    } while (rcx != 0);
    *((rsp + 0x58)) = 0;
    *((rsp + 0x50)) = 1;
label_21:
    if (r10d == 0) {
        goto label_101;
    }
    al = (r10d != 0x2d) ? 1 : 0;
label_34:
    __asm ("cmovs r15d, dword [rsp + 0x50]");
    rdi -= r8;
    if (*((rsp + 0x40)) != 0) {
        goto label_102;
    }
    if (*((rsp + 0x58)) != 0) {
        goto label_103;
    }
    rcx = (int64_t) edi;
    if (r15d <= edi) {
        goto label_104;
    }
    if (al == 0) {
        goto label_104;
    }
label_33:
    rdx = (int64_t) r15d;
    r15 = rcx;
    if (rdx >= rcx) {
        r15 = rdx;
    }
label_32:
    rax = *((rsp + 8));
    rax -= r13;
    if (rax <= r15) {
        goto label_9;
    }
    if (r14 == 0) {
        goto label_15;
    }
    if (rdx > rcx) {
        rdx -= rcx;
        *((rsp + 0x50)) = r8;
        *((rsp + 0x40)) = rcx;
        rbp = r14 + rdx;
        if (r10d == 0x30) {
            goto label_105;
        }
        if (r10d == 0x2b) {
            goto label_105;
        }
        r14 = rbp;
        memset (r14, 0x20, rdx);
        r8 = *((rsp + 0x50));
        rcx = *((rsp + 0x40));
    }
label_43:
    if (*((rsp + 0x38)) == 0) {
        goto label_106;
    }
    *((rsp + 0x38)) = r8;
    rbp = rcx - 1;
    if (rcx == 0) {
        goto label_36;
    }
    *((rsp + 0x40)) = rcx;
    rax = ctype_toupper_loc ();
    rcx = *((rsp + 0x40));
    r8 = *((rsp + 0x38));
    do {
        esi = *((r8 + rbp));
        rdx = *(rax);
        edx = *((rdx + rsi*4));
        *((r14 + rbp)) = dl;
        rbp--;
    } while (rbp >= 0);
label_36:
    r14 += rcx;
    goto label_15;
    rbx--;
    goto label_8;
    rax = rbx - 1;
    if (rax == rbp) {
        goto label_107;
    }
    rbx = rax;
    goto label_8;
    rax = *((rsp + 8));
    rax -= r13;
    if (r10d == 0x2d) {
        goto label_108;
    }
    if (r15d < 0) {
        goto label_108;
    }
    rdx = (int64_t) r15d;
    ecx = 1;
    if (rdx != 0) {
        rcx = rdx;
    }
    if (rcx >= rax) {
        goto label_9;
    }
    if (r14 != 0) {
        if (r15d > 1) {
            rdx--;
            r15 = r14 + rdx;
            if (r10d == 0x30) {
                goto label_109;
            }
            if (r10d == 0x2b) {
                goto label_109;
            }
            r14 = r15;
            memset (r14, 0x20, rdx);
        }
label_45:
        *(r14) = 9;
        r14++;
    }
label_23:
    r13 += rbp;
    goto label_0;
    if (edi == 0x45) {
        goto label_8;
    }
    eax = *((rsp + 0x38));
    if (r8b != 0) {
        eax = r8d;
    }
    ebp = 0;
    *((rsp + 0x38)) = al;
    goto label_16;
    if (edi == 0x45) {
        goto label_17;
    }
    rax = *((rsp + 0x20));
    ecx = *((rax + 0x14));
    eax = rcx + 0x76c;
    rsp + 0x40 = (ecx < 0xfffff894) ? 1 : 0;
    eax -= eax;
    eax &= 0xffffff9d;
    eax += ecx;
    rdx = (int64_t) eax;
    eax >>= 0x1f;
    rdx *= 0x51eb851f;
    rdx >>= 0x25;
    edx -= eax;
    edx += 0x13;
    al = (ecx >= 0xfffff894) ? 1 : 0;
    if (r10d != 0) {
        goto label_110;
    }
    r10d = *((rsp + 0x34));
    if (*((rsp + 0x34)) == 0x2b) {
        goto label_111;
    }
label_53:
    *((rsp + 0x58)) = 0;
    *((rsp + 0x50)) = 2;
    goto label_18;
    if (edi != 0) {
        goto label_8;
    }
    *((rsp + 0x40)) = 0xffffffff;
    r11 = "%m/%d/%y";
label_20:
    r8d = *((rsp + 0x38));
    r9d = r10d;
    rdx = r11;
    eax = *((rsp + 0x518));
    edi = 0;
    rsi = 0xffffffffffffffff;
    eax = *((rsp + 0x58));
    rcx = *((rsp + 0x40));
    *((rsp + 0x78)) = r10d;
    *((rsp + 0x70)) = r8d;
    *((rsp + 0x58)) = r11;
    rax = _strftime_internal_isra_0 ();
    goto label_19;
    r10d = *((rsp + 0x78));
    r11 = *((rsp + 0x38));
    r8d = *((rsp + 0x50));
    if (r10d == 0x2d) {
        goto label_112;
    }
    if (r15d < 0) {
        goto label_112;
    }
    rdx = (int64_t) r15d;
    rax = rdx;
    if (rax >= rdx) {
        rax = rbp;
    }
    *((rsp + 0x38)) = rax;
label_39:
    r15 = *((rsp + 8));
    r15 -= r13;
    if (r15 <= *((rsp + 0x38))) {
        goto label_9;
    }
    if (r14 != 0) {
        if (rdx > rbp) {
            rdx -= rbp;
            *((rsp + 0x64)) = r10d;
            rcx = r14 + rdx;
            *((rsp + 0x58)) = r11;
            *((rsp + 0x68)) = rcx;
            *((rsp + 0x50)) = r8d;
            if (r10d == 0x30) {
                goto label_113;
            }
            if (r10d == 0x2b) {
                goto label_113;
            }
            memset (r14, 0x20, rdx);
            r14 = *((rsp + 0x68));
            r10d = *((rsp + 0x64));
            r11 = *((rsp + 0x58));
            r8d = *((rsp + 0x50));
        }
label_49:
        rdi = r14;
        r9d = r10d;
        rdx = r11;
        eax = *((rsp + 0x518));
        rsi = r15;
        r14 += rbp;
        eax = *((rsp + 0x58));
        rcx = *((rsp + 0x40));
        eax = _strftime_internal_isra_0 ();
        goto label_19;
    }
    r13 += *((rsp + 0x38));
    goto label_0;
    if (edi != 0) {
        goto label_8;
    }
    if (r15d < 0) {
        if (r10d == 0) {
            goto label_114;
        }
    }
    edi = r15 - 6;
    eax = 0;
    r11 = "%Y-%m-%d";
    __asm ("cmovns eax, edi");
    *((rsp + 0x40)) = eax;
    goto label_20;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x50)) = 1;
    edx = *((rax + 0x18));
    goto label_14;
    if (edi == 0x4f) {
        goto label_17;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x58)) = 0;
    r9d = 0;
    *((rsp + 0x50)) = 1;
    eax = *((rax + 0x10));
    *((rsp + 0x40)) = 0;
    edx = rax * 5;
    edx = rax + rdx*2;
    edx >>= 5;
    edx++;
label_25:
    rdi = rsp + 0xc7;
    r8 = rdi;
label_22:
    rsi = rdi;
    if ((r9b & 1) != 0) {
        *((r8 - 1)) = 0x3a;
        rsi--;
    }
    eax = edx;
    ecx = edx;
    r9d >>= 1;
    r8 = rsi - 1;
    rax *= rbp;
    rax >>= 0x23;
    r11d = rax * 5;
    r11d += r11d;
    ecx -= r11d;
    ecx += 0x30;
    *((rsi - 1)) = cl;
    if (edx > 9) {
        goto label_115;
    }
    if (r9d == 0) {
        goto label_21;
    }
label_115:
    edx = eax;
    goto label_22;
    rax = *((rsp + 0x20));
    *((rsp + 0x50)) = 1;
    eax = *((rax + 0x18));
    edx = rax + 6;
    *((rsp + 0x40)) = eax;
    rax = (int64_t) edx;
    ecx = edx;
    rax *= 0xffffffff92492493;
    ecx >>= 0x1f;
    rax >>= 0x20;
    eax += edx;
    eax >>= 2;
    eax -= ecx;
    ecx = rax*8;
    ecx -= eax;
    edx -= ecx;
    edx++;
    goto label_14;
    if (edi == 0x45) {
        goto label_8;
    }
    edx = *((rsp + 0x60));
label_27:
    eax = 0x5f;
    *((rsp + 0x50)) = 2;
    if (r10d == 0) {
        r10d = eax;
    }
    goto label_14;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x58)) = 0;
    *((rsp + 0x50)) = 2;
    eax = *((rax + 0x10));
    edx = rax + 1;
    rsp + 0x40 = (eax < 0xffffffff) ? 1 : 0;
    al = (eax >= 0xffffffff) ? 1 : 0;
    r9d = 0;
    goto label_13;
    rax = *((rsp + 8));
    rax -= r13;
    if (r10d == 0x2d) {
        goto label_116;
    }
    if (r15d < 0) {
        goto label_116;
    }
    rdx = (int64_t) r15d;
    ecx = 1;
    if (rdx != 0) {
        rcx = rdx;
    }
    if (rcx >= rax) {
        goto label_9;
    }
    if (r14 == 0) {
        goto label_23;
    }
    if (r15d > 1) {
        rdx--;
        r15 = r14 + rdx;
        if (r10d == 0x30) {
            goto label_117;
        }
        if (r10d == 0x2b) {
            goto label_117;
        }
        r14 = r15;
        memset (r14, 0x20, rdx);
    }
label_46:
    *(r14) = 0xa;
    r14++;
    goto label_23;
    if (edi == 0x45) {
        goto label_8;
    }
    rcx = *((rsp + 0x20));
    eax = *((rcx + 0x1c));
    eax -= *((rcx + 0x18));
    eax += 7;
    goto label_24;
    if (edi == 0x45) {
        goto label_17;
    }
    if (edi == 0x4f) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    edx = *((rax + 0x14));
    rsp + 0x40 = (edx < 0xfffff894) ? 1 : 0;
    edx += 0x76c;
    if (r10d != 0) {
        goto label_55;
    }
    r10d = *((rsp + 0x34));
    if (r10d == 0x2b) {
        goto label_118;
    }
    *((rsp + 0x58)) = 0;
    r9d = 0;
    *((rsp + 0x50)) = 4;
label_93:
    eax = edx;
    eax = -eax;
    if (*((rsp + 0x40)) != 0) {
        edx = eax;
    }
    goto label_25;
    edi = *((rsp + 0x38));
    eax = 0;
    *((rsp + 0x50)) = r10d;
    *((rsp + 0x40)) = r8b;
    if (r8b != 0) {
        edi = eax;
    }
    *((rsp + 0x38)) = dil;
    rax = strlen (r12);
    r10d = *((rsp + 0x50));
    r8d = *((rsp + 0x40));
    if (r10d == 0x2d) {
        goto label_119;
    }
    if (r15d < 0) {
        goto label_119;
    }
    r15 = (int64_t) r15d;
    rax = r15;
    if (rax >= r15) {
        rax = rbp;
    }
    *((rsp + 0x40)) = rax;
label_47:
    rax = *((rsp + 8));
    rax -= r13;
    if (rax <= *((rsp + 0x40))) {
        goto label_9;
    }
    if (r14 == 0) {
        goto label_120;
    }
    if (rbp < r15) {
        rdx = r15;
        *((rsp + 0x50)) = r8b;
        rdx -= rbp;
        r15 = r14 + rdx;
        if (r10d == 0x30) {
            goto label_121;
        }
        if (r10d == 0x2b) {
            goto label_121;
        }
        r14 = r15;
        memset (r14, 0x20, rdx);
        r8d = *((rsp + 0x50));
    }
label_59:
    if (r8b != 0) {
        goto label_122;
    }
    if (*((rsp + 0x38)) == 0) {
        goto label_123;
    }
    r15 = rbp - 1;
    if (rbp == 0) {
        goto label_57;
    }
    rax = ctype_toupper_loc ();
    rsi = rax;
    do {
        edx = *((r12 + r15));
        rax = *(rsi);
        eax = *((rax + rdx*4));
        *((r14 + r15)) = al;
        r15--;
    } while (r15 >= 0);
label_57:
    r14 += rbp;
label_120:
    r13 += *((rsp + 0x40));
    goto label_0;
    if (edi == 0x45) {
        goto label_17;
    }
    rax = *((rsp + 0x20));
    rdx = *((rax + 0x14));
    rax = rdx;
    rdx *= 0x51eb851f;
    ecx = eax;
    ecx >>= 0x1f;
    rdx >>= 0x25;
    edx -= ecx;
    ecx = edx * 0x64;
    edx = eax;
    edx -= ecx;
    if (edx < 0) {
        ecx = edx;
        edx += 0x64;
        ecx = -ecx;
        if (eax > 0xfffff893) {
            edx = ecx;
            goto label_124;
        }
    }
label_124:
    if (r10d != 0) {
        goto label_61;
    }
label_50:
    eax = *((rsp + 0x34));
    if (eax == 0x2b) {
        goto label_92;
    }
    r10d = eax;
    goto label_26;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x50)) = 2;
    edx = *((rax + 0xc));
    goto label_14;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    edx = *((rax + 0xc));
    goto label_27;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    *((rsp + 0x58)) = 0;
    *((rsp + 0x50)) = 3;
    eax = *((rax + 0x1c));
    edx = rax + 1;
    rsp + 0x40 = (eax < 0xffffffff) ? 1 : 0;
    al = (eax >= 0xffffffff) ? 1 : 0;
    r9d = 0;
    goto label_13;
    if (edi == 0x45) {
        goto label_8;
    }
    rax = *((rsp + 0x20));
    edx = *((rax + 8));
    goto label_27;
    eax = *((rbx + 1));
    rdx = rbx + 1;
    r11d = 1;
    if (al == 0x3a) {
        goto label_30;
    }
label_29:
    if (al != 0x7a) {
        goto label_8;
    }
    rbx = rdx;
label_28:
    rax = *((rsp + 0x20));
    r8d = *((rax + 0x20));
    if (r8d < 0) {
        goto label_0;
    }
    rdx = *((rax + 0x28));
    *((rsp + 0x40)) = 1;
    if (edx >= 0) {
        *((rsp + 0x40)) = 0;
        if (edx != 0) {
            goto label_125;
        }
        rsp + 0x40 = (*(r12) == 0x2d) ? 1 : 0;
    }
label_125:
    rax = (int64_t) edx;
    ecx = edx;
    r8 = rax * 0xffffffff91a2b3c5;
    ecx >>= 0x1f;
    rax *= 0xffffffff88888889;
    r8 >>= 0x20;
    rax >>= 0x20;
    r8d += edx;
    eax += edx;
    r8d >>= 0xb;
    eax >>= 5;
    r8d -= ecx;
    eax -= ecx;
    r9 = (int64_t) eax;
    ecx = eax;
    r9 *= 0xffffffff88888889;
    ecx >>= 0x1f;
    r9 >>= 0x20;
    r9d += eax;
    r9d >>= 5;
    r9d -= ecx;
    ecx = r9d * 0x3c;
    r9d = eax;
    eax *= 0x3c;
    r9d -= ecx;
    ecx = edx;
    ecx -= eax;
    if (r11 == 2) {
        goto label_126;
    }
    if (r11 > 2) {
        goto label_127;
    }
    if (r11 == 0) {
        goto label_128;
    }
label_44:
    edx = r8d * 0x64;
    eax = *((rsp + 0x40));
    *((rsp + 0x58)) = 1;
    *((rsp + 0x50)) = 6;
    eax ^= 1;
    edx += r9d;
    r9d = 4;
    goto label_13;
    r11d = 0;
    goto label_28;
    ebp = 0;
    do {
        ecx = *((rsp + 0x38));
        eax = 0;
        esi = 0x70;
        if (r8b != 0) {
        }
        if (r8b != 0) {
            ecx = eax;
        }
        *((rsp + 0x38)) = cl;
        goto label_16;
        *((rsp + 0x40)) = 0xffffffff;
        r11 = "%H:%M";
        goto label_20;
    } while (1);
    *((rsp + 0x40)) = 0xffffffff;
    r11 = "%H:%M:%S";
    goto label_20;
label_30:
    r11++;
    eax = *((rbx + r11));
    rdx = rbx + r11;
    if (al != 0x3a) {
        goto label_29;
    }
    goto label_30;
label_83:
    r15 = r8;
    edx = 0;
    goto label_31;
label_102:
    ecx = 0x2d;
label_40:
    esi = r15 - 1;
    edx = esi;
    edx -= ebp;
    if (edx <= 0) {
        goto label_129;
    }
    if (al == 0) {
        goto label_129;
    }
label_35:
    if (r10d == 0x5f) {
        goto label_130;
    }
    rax = *((rsp + 8));
    rax -= r13;
    if (rax <= 1) {
        goto label_9;
    }
    if (r14 != 0) {
label_42:
        *(r14) = cl;
        r14++;
    }
    r13++;
    rcx = (int64_t) ebp;
    if (r10d == 0x2d) {
        goto label_131;
    }
label_48:
    r15 = rcx;
    edx = 0;
    if (esi < 0) {
        goto label_32;
    }
    r15d = esi;
    goto label_33;
label_101:
    eax = 1;
    r10d = 0x30;
    goto label_34;
label_129:
    edx = 0;
    goto label_35;
label_104:
    if (r10d != 0x2d) {
        goto label_33;
    }
label_131:
    r15 = rcx;
    edx = 0;
    goto label_32;
label_106:
    rdx = rcx;
    *((rsp + 0x38)) = rcx;
    eax = memcpy (r14, r8, rdx);
    rcx = *((rsp + 0x38));
    goto label_36;
label_71:
    *((rsp + 0x38)) = 1;
    goto label_7;
label_72:
    r8d = eax;
    goto label_7;
label_77:
    r14 = r15;
    memset (r14, 0x30, rdx);
    rcx = *((rsp + 0x40));
    r8 = *((rsp + 0x50));
    r9 = *((rsp + 0x58));
    goto label_37;
label_86:
    rdx = r8;
    *((rsp + 0x38)) = r8;
    memcpy (r14, rsp + 0xb1, rdx);
    r8 = *((rsp + 0x38));
    goto label_38;
label_85:
    rbp = rcx - 2;
    if (r8 == 0) {
        goto label_38;
    }
    *((rsp + 0x38)) = r8;
    rax = ctype_tolower_loc ();
    r8 = *((rsp + 0x38));
    rsi = rsp + 0xb1;
    do {
        ecx = *((rsi + rbp));
        rdx = *(rax);
        edx = *((rdx + rcx*4));
        *((r14 + rbp)) = dl;
        rbp--;
    } while (rbp >= 0);
    goto label_38;
label_112:
    *((rsp + 0x38)) = rbp;
    edx = 0;
    goto label_39;
label_103:
    ecx = 0x2b;
    goto label_40;
label_84:
    memset (r14, 0x30, rdx);
    r14 = *((rsp + 0x40));
    rcx = *((rsp + 0x50));
    r8 = *((rsp + 0x58));
    goto label_41;
label_130:
    r9d = r15d;
    r15 = *((rsp + 8));
    r9d -= edx;
    rdx = (int64_t) edx;
    r13 += rdx;
    r15 -= r13;
    if (r14 == 0) {
        goto label_132;
    }
    *((rsp + 0x40)) = rdx;
    *((rsp + 0x68)) = cl;
    *((rsp + 0x64)) = r9d;
    *((rsp + 0x58)) = r10d;
    *((rsp + 0x50)) = r8;
    memset (r14, 0x20, rdx);
    rdx = *((rsp + 0x40));
    r14 += rdx;
    if (r15 <= 1) {
        goto label_9;
    }
    r9d = *((rsp + 0x64));
    r8 = *((rsp + 0x50));
    r10d = *((rsp + 0x58));
    ecx = *((rsp + 0x68));
    esi = r9 - 1;
    goto label_42;
label_105:
    r14 = rbp;
    memset (r14, 0x30, rdx);
    rcx = *((rsp + 0x40));
    r8 = *((rsp + 0x50));
    goto label_43;
label_127:
    if (r11 != 3) {
        goto label_8;
    }
    if (ecx != 0) {
        goto label_126;
    }
    if (r9d != 0) {
        goto label_44;
    }
    eax = *((rsp + 0x40));
    *((rsp + 0x58)) = 1;
    edx = r8d;
    *((rsp + 0x50)) = 3;
    eax ^= 1;
    goto label_13;
label_108:
    if (rax <= 1) {
        goto label_9;
    }
    if (r14 != 0) {
        goto label_45;
    }
    goto label_23;
label_116:
    if (rax <= 1) {
        goto label_9;
    }
    if (r14 != 0) {
        goto label_46;
    }
    goto label_23;
label_119:
    *((rsp + 0x40)) = rbp;
    r15d = 0;
    goto label_47;
label_132:
    if (r15 <= 1) {
        goto label_9;
    }
    r13++;
    esi = r9 - 1;
    rcx = (int64_t) ebp;
    goto label_48;
label_113:
    memset (r14, 0x30, rdx);
    r14 = *((rsp + 0x68));
    r8d = *((rsp + 0x50));
    r11 = *((rsp + 0x58));
    r10d = *((rsp + 0x64));
    goto label_49;
label_107:
    rax = *((rsp + 8));
    rax -= r13;
    if (r10d == 0x2d) {
        goto label_133;
    }
    if (r15d < 0) {
        goto label_133;
    }
    rdx = (int64_t) r15d;
    ecx = 1;
    if (rdx != 0) {
        rcx = rdx;
    }
    if (rcx >= rax) {
        goto label_9;
    }
    if (r14 == 0) {
        goto label_23;
    }
    r15d--;
    if (r15d > 0) {
        rdx--;
        r15 = r14 + rdx;
        if (r10d == 0x30) {
            goto label_134;
        }
        if (r10d == 0x2b) {
            goto label_134;
        }
        r14 = r15;
        memset (r14, 0x20, rdx);
    }
label_56:
    eax = *(rbx);
    r14++;
    *((r14 - 1)) = al;
    goto label_23;
label_91:
    r8d = 0xfffff894;
    r8d -= eax;
    if (ecx >= r8d) {
        goto label_135;
    }
    edx = -edx;
    if (r10d == 0) {
        goto label_50;
    }
label_61:
    if (r10d != 0x2b) {
        goto label_26;
    }
label_92:
    *((rsp + 0x40)) = 0;
    *((rsp + 0x50)) = 2;
    do {
        eax = *((rsp + 0x40));
        r10d = 0x2b;
        rsp + 0x58 = (r15d > *((rsp + 0x50))) ? 1 : 0;
        eax ^= 1;
        goto label_18;
label_94:
        esi = 1;
        edi = 1;
        goto label_51;
label_55:
        *((rsp + 0x50)) = 4;
        eax = 0x270f;
        if (r10d != 0x2b) {
            goto label_136;
        }
label_54:
    } while (eax >= edx);
    eax = *((rsp + 0x40));
    *((rsp + 0x58)) = 1;
    r10d = 0x2b;
    eax ^= 1;
    goto label_18;
label_87:
    r9d--;
    eax = 0x16d;
    if ((r9b & 3) == 0) {
        edx = r9d * 0xc28f5c29;
        eax = 0x16e;
        edx += 0x51eb850;
        edx = rotate_right32 (edx, 2);
        if (edx > 0x28f5c28) {
            goto label_137;
        }
        eax = r9d;
        r8d = 0x190;
        edx:eax = (int64_t) eax;
        eax = edx:eax / r8d;
        edx = edx:eax % r8d;
        edx = -edx;
        eax -= eax;
        eax += 0x16e;
    }
label_137:
    eax += r11d;
    r8d = eax;
    r8d -= ebp;
    r8d += 0x17e;
    rdx = (int64_t) r8d;
    r9d = r8d;
    eax -= r8d;
    rdx *= 0xffffffff92492493;
    r9d >>= 0x1f;
    rdx >>= 0x20;
    edx += r8d;
    edx >>= 2;
    edx -= r9d;
    r9d = rdx*8;
    r9d -= edx;
    r8d = rax + r9 + 3;
    eax = 0xffffffff;
    goto label_52;
label_110:
    if (r10d != 0x2b) {
        goto label_53;
    }
label_111:
    *((rsp + 0x50)) = 2;
    eax = 0x63;
    goto label_54;
label_90:
    rdx = (int64_t) r8d;
    *((rsp + 0x50)) = 2;
    rdx *= 0xffffffff92492493;
    rdx >>= 0x20;
    edx += r8d;
    r8d >>= 0x1f;
    edx >>= 2;
    edx -= r8d;
    edx++;
    goto label_14;
label_89:
    r8d = 0xfffff894;
    edx = rcx + rax + 0x76c;
    r8d -= eax;
    rsp + 0x40 = (ecx < r8d) ? 1 : 0;
    if (r10d != 0) {
        goto label_55;
    }
    r10d = *((rsp + 0x34));
    if (r10d == 0x2b) {
        goto label_118;
    }
    *((rsp + 0x58)) = 0;
    *((rsp + 0x50)) = 4;
    al = (ecx >= r8d) ? 1 : 0;
    goto label_18;
label_133:
    if (rax <= 1) {
        goto label_9;
    }
    if (r14 != 0) {
        goto label_56;
    }
    goto label_23;
label_123:
    memcpy (r14, r12, rbp);
    goto label_57;
label_122:
    r15 = rbp - 1;
    if (rbp == 0) {
        goto label_57;
    }
    rax = ctype_tolower_loc ();
    rsi = rax;
    do {
        edx = *((r12 + r15));
        rax = *(rsi);
        eax = *((rax + rdx*4));
        *((r14 + r15)) = al;
        r15--;
    } while (r15 >= 0);
    goto label_57;
label_126:
    r9d *= 0x64;
    eax = *((rsp + 0x40));
    *((rsp + 0x58)) = 1;
    edx = r8d * sym.imp.__stack_chk_fail;
    *((rsp + 0x50)) = 9;
    eax ^= 1;
    edx += r9d;
    r9d = 0x14;
    edx += ecx;
    goto label_13;
label_117:
    r14 = r15;
    memset (r14, 0x30, rdx);
    goto label_46;
label_97:
    rdi = rsp + 0xb0;
    if (rsi >= 8) {
        goto label_138;
    }
    if ((sil & 4) != 0) {
        goto label_139;
    }
    if (rsi == 0) {
        goto label_58;
    }
    eax = *((rsp + 0xb0));
    *(r14) = al;
    if ((sil & 2) == 0) {
        goto label_58;
    }
    eax = *((rsp + rsi + 0xae));
    *((r14 + rsi - 2)) = ax;
    goto label_58;
label_109:
    r14 = r15;
    memset (r14, 0x30, rdx);
    goto label_45;
label_128:
    edx = r8d * 0x64;
    eax = *((rsp + 0x40));
    *((rsp + 0x58)) = 1;
    *((rsp + 0x50)) = 5;
    eax ^= 1;
    edx += r9d;
    r9d = 0;
    goto label_13;
label_121:
    r14 = r15;
    memset (r14, 0x30, rdx);
    r8d = *((rsp + 0x50));
    goto label_59;
label_118:
    *((rsp + 0x50)) = 4;
    eax = 0x270f;
    goto label_54;
label_114:
    r8d = *((rsp + 0x38));
    r15 = "%Y-%m-%d";
    rsi |= 0xffffffffffffffff;
    eax = *((rsp + 0x518));
    rdx = r15;
    r9d = 0x2b;
    edi = 0;
    rcx = *((rsp + 0x40));
    *((rsp + 0x70)) = r8d;
    rax = _strftime_internal_isra_0 ();
    goto label_19;
    r11 = r15;
    edx = 0;
    r10d = 0x2b;
    r8d = *((rsp + 0x50));
    *((rsp + 0x38)) = rax;
    *((rsp + 0x40)) = 4;
    goto label_39;
label_99:
    r14 = rbp;
    memset (r14, 0x30, rdx);
    goto label_0;
label_138:
    rax = *((rsp + 0xb0));
    rdx = r14 + 8;
    rdx &= 0xfffffffffffffff8;
    *(r14) = rax;
    rax = *((rsp + rsi + 0xa8));
    *((r14 + rsi - 8)) = rax;
    rax = r14;
    rax -= rdx;
    rdi -= rax;
    rax += rsi;
    rax &= 0xfffffffffffffff8;
    if (rax < 8) {
        goto label_58;
    }
    rax &= 0xfffffffffffffff8;
    ecx = 0;
    do {
        r8 = *((rdi + rcx));
        *((rdx + rcx)) = r8;
        rcx += 8;
    } while (rcx < rax);
    goto label_58;
label_134:
    r14 = r15;
    memset (r14, 0x30, rdx);
    goto label_56;
label_95:
    esi = 0;
    goto label_60;
label_98:
    r13 += rdx;
    goto label_0;
label_69:
    stack_chk_fail ();
label_135:
    edx += 0x64;
    if (r10d == 0) {
        goto label_50;
    }
    goto label_61;
label_136:
    eax = *((rsp + 0x40));
    *((rsp + 0x58)) = 0;
    eax ^= 1;
    goto label_18;
label_100:
    rax = *((rsp + 0x10));
    *(rax) = 0x4b;
    goto label_62;
label_139:
    eax = *((rsp + 0xb0));
    *(r14) = eax;
    eax = *((rsp + rsi + 0xac));
    *((r14 + rsi - 4)) = eax;
    goto label_58;
}

/* /tmp/tmprn8a__c4 @ 0xb6b0 */
 
uint64_t revert_tz_part_0 (int64_t arg1) {
    rdi = arg1;
    rbx = rdi;
    rax = errno_location ();
    r12d = *(rax);
    if (*((rbx + 8)) != 0) {
        goto label_2;
    }
    rdi = 0x00012250;
    eax = unsetenv ();
    if (eax == 0) {
        goto label_3;
    }
label_0:
    r12d = *(rbp);
    r13d = 0;
    do {
label_1:
        rdi = rbx;
        rbx = *(rbx);
        free (rdi);
    } while (rbx != 0);
    *(rbp) = r12d;
    eax = r13d;
    return rax;
label_2:
    eax = setenv (0x00012250, rbx + 9, 1);
    if (eax != 0) {
        goto label_0;
    }
label_3:
    tzset ();
    r13d = 1;
    goto label_1;
}

/* /tmp/tmprn8a__c4 @ 0xb7e0 */
 
uint64_t dbg_save_abbr (uint32_t arg_8h, int64_t arg_9h, int64_t arg_80h, int64_t arg1, uint32_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* _Bool save_abbr(timezone_t tz,tm * tm); */
    r12 = *((rsi + 0x30));
    if (r12 == 0) {
        goto label_4;
    }
    r13 = rsi;
    if (rsi <= r12) {
        rdx = rsi + 0x38;
        eax = 1;
        if (r12 < rdx) {
            goto label_3;
        }
    }
    rbx = rbp + 9;
    if (*(r12) == 0) {
        goto label_5;
    }
    do {
label_0:
        eax = strcmp (rbx, r12);
        if (eax == 0) {
            goto label_2;
        }
label_1:
        if (*(rbx) == 0) {
            rax = rbp + 9;
            if (rbx != rax) {
                goto label_6;
            }
            if (*((rbp + 8)) == 0) {
                goto label_6;
            }
        }
        strlen (rbx);
        rbx = rbx + rax + 1;
    } while (*(rbx) != 0);
    rax = *(rbp);
    if (rax == 0) {
        goto label_0;
    }
    rbx = rax + 9;
    eax = strcmp (rbx, r12);
    if (eax != 0) {
        goto label_1;
    }
    do {
label_2:
        *((r13 + 0x30)) = rbx;
        eax = 1;
label_3:
        return rax;
label_5:
        rbx = 0x00013341;
    } while (1);
label_4:
    eax = 1;
    return rax;
label_6:
    rax = strlen (r12);
    r14 = rax;
    rdx = rax + 1;
    rax = rbp + 0x80;
    rax -= rbx;
    if (rax > rdx) {
        memcpy (rbx, r12, rdx);
        *((rbx + r14 + 1)) = 0;
        goto label_2;
    }
    rax = tzalloc (r12);
    *(rbp) = rax;
    if (rax != 0) {
        *((rax + 8)) = 0;
        rbx = rax + 9;
        goto label_2;
    }
    eax = 0;
    goto label_3;
}

/* /tmp/tmprn8a__c4 @ 0xb910 */
 
uint64_t dbg_set_tz (int64_t arg1) {
    rdi = arg1;
    /* timezone_t set_tz(timezone_t tz); */
    r13 = 0x00012250;
    rbx = rdi;
    rax = getenv (r13);
    if (rax == 0) {
        goto label_2;
    }
    while (eax != 0) {
label_0:
        rax = tzalloc (rbp);
        r12 = rax;
        if (rax != 0) {
            if (*((rbx + 8)) != 0) {
                goto label_3;
            }
            rdi = r13;
            eax = unsetenv ();
            if (eax != 0) {
                goto label_4;
            }
label_1:
            tzset ();
        }
        rax = r12;
        return rax;
        r12d = 1;
        eax = strcmp (rbx + 9, rax);
    }
    rax = r12;
    return rax;
label_2:
    r12d = 1;
    if (*((rbx + 8)) != 0) {
        goto label_0;
    }
    rax = r12;
    return rax;
label_3:
    eax = setenv (r13, rbx + 9, 1);
    if (eax == 0) {
        goto label_1;
    }
label_4:
    rax = errno_location ();
    ebx = *(rax);
    if (r12 == 1) {
        goto label_5;
    }
    do {
        rdi = r12;
        r12 = *(r12);
        free (rdi);
    } while (r12 != 0);
label_5:
    *(rbp) = ebx;
    r12d = 0;
    rax = r12;
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0xca10 */
 
int64_t dbg_ydhms_diff (int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg1, int64_t arg3, int64_t arg4, int64_t arg6, int32_t sec1, long_int yday1) {
    int32_t yday0;
    int32_t hour0;
    int32_t min0;
    int32_t sec0;
    rdi = arg1;
    rdx = arg3;
    rcx = arg4;
    r9 = arg6;
    r8 = sec1;
    rsi = yday1;
    /* long_int ydhms_diff(long_int year1,long_int yday1,int hour1,int min1,int sec1,int year0,int yday0,int hour0,int min0,int sec0); */
    r11 = (int64_t) ecx;
    rcx = rdi;
    rax = rdi;
    ecx &= 3;
    r10 = (int64_t) edx;
    rax >>= 2;
    rbx = (int64_t) r9d;
    r8 = (int64_t) r8d;
    rdx = rbx;
    eax -= 0xfffffe25;
    r9 = rbx;
    edx &= 3;
    r9 >>= 2;
    r9d -= 0xfffffe25;
    ebp >>= 0x1f;
    rdi -= rbx;
    ecx = rbp + rax;
    eax -= r9d;
    rdx = (int64_t) ecx;
    ecx >>= 0x1f;
    rdx *= 0x51eb851f;
    rdx >>= 0x23;
    edx -= ecx;
    edx -= ebp;
    ebp >>= 0x1f;
    r12d = rbp + r9;
    r9d = edx;
    rdx = (int64_t) edx;
    rcx = (int64_t) r12d;
    r12d >>= 0x1f;
    rcx *= 0x51eb851f;
    rdx >>= 2;
    rcx >>= 0x23;
    ecx -= r12d;
    ecx -= ebp;
    r9d -= ecx;
    rcx = (int64_t) ecx;
    rcx >>= 2;
    eax -= r9d;
    edx -= ecx;
    rcx = *((rsp + 0x20));
    eax += edx;
    rdx = rdi * 9;
    rdx = rdi + rdx*8;
    rax = (int64_t) eax;
    rdx *= 5;
    rdx += rsi;
    rdx -= rcx;
    rax += rdx;
    rdx = *((rsp + 0x28));
    rax *= 3;
    rax = r10 + rax*8;
    rax -= rdx;
    rdx = rax;
    rdx <<= 4;
    rdx -= rax;
    rax = r11 + rdx*4;
    rdx = *((rsp + 0x30));
    rax -= rdx;
    rdx = rax;
    rdx <<= 4;
    rdx -= rax;
    rax = r8 + rdx*4;
    rdx = *((rsp + 0x38));
    r12 = rbx;
    rax -= rdx;
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0xcb10 */
 
int64_t dbg_ranged_convert (int64_t arg1, signed int64_t arg2, int64_t arg3) {
    time_t x;
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_24h;
    int64_t var_28h;
    int64_t var_2ch;
    int64_t canary;
    int64_t var_38h;
    int64_t var_40h;
    signed int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* tm * ranged_convert(tm * (*)() convert,long_int * t,tm * tp); */
    r15 = rsi;
    r14 = rdx;
    rbx = rdi;
    r12 = *(rsi);
    rbp = rsp + 0x50;
    rsi = rdx;
    rdi = rbp;
    rax = *(fs:0x28);
    eax = 0;
    rax = void (*rbx)(uint64_t, uint64_t, uint64_t, uint64_t, uint64_t) (rbx, rbp, r12, r13, r14);
    *((rsp + 0x38)) = rax;
    if (rax == 0) {
        goto label_2;
    }
    *(r15) = r12;
    do {
label_0:
        rax = *((rsp + 0x58));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        rax = *((rsp + 0x38));
        return rax;
label_2:
        rax = errno_location ();
        *((rsp + 0x40)) = rax;
    } while (*(rax) != 0x4b);
    rcx = r12;
    rax = r12;
    ecx &= 1;
    rax >>= 1;
    r13 = rcx + rax;
    if (r12 == r13) {
        goto label_0;
    }
    if (r13 == 0) {
        goto label_0;
    }
    r15d = 0;
    rax = r14;
    *((rsp + 4)) = 0xffffffff;
    r14 = r12;
    r12 = r13;
    r13 = r15;
    r15 = rax;
    while (rax != 0) {
        eax = *(r15);
        r13 = r12;
        *((rsp + 4)) = eax;
        eax = *((r15 + 4));
        *((rsp + 0x2c)) = eax;
        eax = *((r15 + 8));
        *((rsp + 0x28)) = eax;
        eax = *((r15 + 0xc));
        *((rsp + 0x24)) = eax;
        eax = *((r15 + 0x10));
        *((rsp + 0x20)) = eax;
        eax = *((r15 + 0x14));
        *((rsp + 0xc)) = eax;
        eax = *((r15 + 0x18));
        *((rsp + 0x18)) = eax;
        eax = *((r15 + 0x1c));
        *((rsp + 0x1c)) = eax;
        eax = *((r15 + 0x20));
        *((rsp + 8)) = eax;
        rax = *((r15 + 0x28));
        *((rsp + 0x10)) = rax;
        rax = *((r15 + 0x30));
        *((rsp + 0x30)) = rax;
label_1:
        rdx = r13;
        rax = r14;
        rax >>= 1;
        rdx >>= 1;
        rdx += rax;
        rax = r13;
        rax |= r14;
        eax &= 1;
        r12 = rdx + rax;
        if (r12 == r13) {
            goto label_4;
        }
        if (r12 == r14) {
            goto label_4;
        }
        rsi = r15;
        rdi = rbp;
        rax = void (*rbx)(uint64_t) (r12);
    }
    rax = *((rsp + 0x40));
    if (*(rax) != 0x4b) {
        goto label_0;
    }
    r14 = r12;
    goto label_1;
label_4:
    eax = *((rsp + 4));
    r14 = r15;
    if (eax < 0) {
        goto label_0;
    }
    rcx = *((rsp + 0x48));
    *((rsp + 0x38)) = r14;
    *(rcx) = r13;
    *(r14) = eax;
    eax = *((rsp + 0x2c));
    *((r14 + 4)) = eax;
    eax = *((rsp + 0x28));
    *((r14 + 8)) = eax;
    eax = *((rsp + 0x24));
    *((r14 + 0xc)) = eax;
    eax = *((rsp + 0x20));
    *((r14 + 0x10)) = eax;
    eax = *((rsp + 0xc));
    *((r14 + 0x14)) = eax;
    eax = *((rsp + 0x18));
    *((r14 + 0x18)) = eax;
    eax = *((rsp + 0x1c));
    *((r14 + 0x1c)) = eax;
    eax = *((rsp + 8));
    *((r14 + 0x20)) = eax;
    rax = *((rsp + 0x10));
    *((r14 + 0x28)) = rax;
    rax = *((rsp + 0x30));
    *((r14 + 0x30)) = rax;
    goto label_0;
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0xd380 */
 
void cdb_free_part_0 (void) {
    return assert_fail ("! close_fail", "lib/chdir-long.c", 0x40, "cdb_free");
}

/* /tmp/tmprn8a__c4 @ 0x2790 */
 
void assert_fail (void) {
    __asm ("bnd jmp qword [reloc.__assert_fail]");
}

/* /tmp/tmprn8a__c4 @ 0xdad0 */
 
int64_t dbg_xstrcat (int64_t arg1, int64_t arg2, int64_t arg7, char const * next) {
    va_list ap;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    r15 = next;
    /* char * xstrcat(size_t argcount,__va_list_tag * args); */
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    __asm ("movdqu xmm0, xmmword [rsi]");
    __asm ("movups xmmword [rsp], xmm0");
    r15 = *((rsi + 0x10));
    *((rsp + 0x10)) = r15;
    if (rdi == 0) {
        goto label_2;
    }
    r13 = rdi;
    r12 = rsi;
    ebx = 0;
    r14 = 0xffffffffffffffff;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        *(rsp) = eax;
        rdx += r15;
label_0:
        rax = strlen (*(rdx));
        rbx += rax;
        if (rbx < 0) {
            rbx = r14;
        }
        rbp--;
        if (rbp == 0) {
            goto label_3;
        }
        eax = *(rsp);
    }
    rdx = *((rsp + 8));
    rax = rdx + 8;
    *((rsp + 8)) = rax;
    goto label_0;
label_3:
    if (rbx > 0x7fffffff) {
        goto label_4;
    }
    rax = xmalloc (rbx + 1);
    r14 = rax;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((r12 + 0x10));
        *(r12) = eax;
label_1:
        r15 = *(rdx);
        rax = strlen (*(rdx));
        rbx = rax;
        memcpy (rbp, r15, rax);
        rbp += rbx;
        r13--;
        if (r13 == 0) {
            goto label_5;
        }
        eax = *(r12);
    }
    rdx = *((r12 + 8));
    rax = rdx + 8;
    *((r12 + 8)) = rax;
    goto label_1;
label_2:
    rax = xmalloc (1);
    r14 = rax;
label_5:
    *(rbp) = 0;
    do {
        rax = *((rsp + 0x18));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_6;
        }
        rax = r14;
        return rax;
label_4:
        errno_location ();
        r14d = 0;
        *(rax) = 0x4b;
    } while (1);
label_6:
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0x10080 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmprn8a__c4 @ 0xad70 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmprn8a__c4 @ 0xd350 */
 
uint64_t dbg_base_len (int64_t arg1) {
    rdi = arg1;
    /* size_t base_len(char const * name); */
    rbx = rdi;
    rax = strlen (rdi);
    do {
        if (rax > 1) {
            rdx = rax - 1;
            if (*((rbx + rax - 1)) == 0x2f) {
                goto label_0;
            }
        }
        return rax;
label_0:
        rax = rdx;
    } while (1);
}

/* /tmp/tmprn8a__c4 @ 0xb0a0 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x000182d0]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x000182e0]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0x2710 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmprn8a__c4 @ 0xc1a0 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0x6af0 */
 
void dbg_filemodestring (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void filemodestring(stat const * statp,char * str); */
    edi = *((rdi + 0x18));
    return void (*0x6970)() ();
}

/* /tmp/tmprn8a__c4 @ 0xabb0 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc (rdi);
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0x25e0 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmprn8a__c4 @ 0xc3a0 */
 
uint64_t dbg_xcharalloc (size_t size) {
    rdi = size;
    /* char * xcharalloc(size_t n); */
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprn8a__c4 @ 0x28c0 */
 
void malloc (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmprn8a__c4 @ 0xc8e0 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x00012bd2);
    return abort ();
}

/* /tmp/tmprn8a__c4 @ 0x2940 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmprn8a__c4 @ 0x2630 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmprn8a__c4 @ 0x6b00 */
 
int64_t dbg_imaxtostr (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * imaxtostr(intmax_t i,char * buf); */
    *((rsi + 0x14)) = 0;
    r8 = rsi + 0x14;
    rcx = rdi;
    rsi = 0xcccccccccccccccd;
    if (rdi < 0) {
        goto label_0;
    }
    do {
        rax = rcx;
        r8--;
        rdx:rax = rax * rsi;
        rdx >>= 3;
        rax = rdx * 5;
        rax += rax;
        rcx -= rax;
        ecx += 0x30;
        *(r8) = cl;
        rcx = rdx;
    } while (rdx != 0);
    rax = r8;
    return rax;
label_0:
    r9 = 0x6666666666666667;
    edi = 0x30;
    do {
        rax = rcx;
        rsi = r8;
        r8--;
        rdx:rax = rax * r9;
        rax = rcx;
        rax >>= 0x3f;
        rdx >>= 2;
        rdx -= rax;
        rax = rdx * 5;
        eax = rdi + rax*2;
        eax -= ecx;
        rcx = rdx;
        *(r8) = al;
    } while (rdx != 0);
    *((r8 - 1)) = 0x2d;
    r8 = rsi - 2;
    rax = r8;
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0xba10 */
 
void tzfree (uint32_t arg1) {
    rdi = arg1;
    if (rdi == 1) {
        goto label_0;
    }
    rbx = rdi;
    if (rdi == 0) {
        goto label_1;
    }
    do {
        rdi = rbx;
        rbx = *(rbx);
        free (rdi);
    } while (rbx != 0);
label_1:
    return;
label_0:
}

/* /tmp/tmprn8a__c4 @ 0xaad0 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0xd9e0 */
 
int64_t dbg_rpl_mbrtowc (int64_t arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0xadc0 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x2ab0)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0xab30 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0xd3b0 */
 
uint64_t dbg_chdir_long (void * s, int64_t arg1) {
    int64_t var_8h;
    rdi = arg1;
    /* int chdir_long(char * dir); */
    eax = chdir ();
    r12d = eax;
    if (eax == 0) {
        goto label_2;
    }
    rax = errno_location ();
    *((rsp + 8)) = rax;
    if (*(rax) != 0x24) {
        goto label_2;
    }
    rax = strlen (rbp);
    rbx = rax;
    if (rax == 0) {
        goto label_9;
    }
    if (rax <= 0xfff) {
        goto label_10;
    }
    r12 = 0x0001339b;
    rax = strspn (rbp, r12);
    r15 = rax;
    if (rax == 2) {
        goto label_11;
    }
    r14 = rbp;
    r13d = 0xffffff9c;
    if (rax != 0) {
        goto label_12;
    }
label_1:
    if (*(r14) == 0x2f) {
        goto label_13;
    }
    rbp += rbx;
    if (r14 > rbp) {
        goto label_14;
    }
    rax = rbp;
    rax -= r14;
    if (rax > 0xfff) {
        goto label_15;
    }
    goto label_16;
    do {
label_0:
        *(rbx) = 0x2f;
        rbx++;
        strspn (rbx, r12);
        r14 = rbx + rax;
        rax = rbp;
        rax -= r14;
        if (rax <= 0xfff) {
            goto label_17;
        }
        r13d = r15d;
label_15:
        edx = 0x1000;
        esi = 0x2f;
        rdi = r14;
        rax = memrchr ();
        rbx = rax;
        if (rax == 0) {
            goto label_18;
        }
        *(rax) = 0;
        rax -= r14;
        if (rax > 0xfff) {
            goto label_19;
        }
        eax = 0;
        eax = openat (r13d, r14, 0x10900);
        r15d = eax;
        if (eax < 0) {
            goto label_20;
        }
    } while (r13d < 0);
    eax = close (r13d);
    if (eax == 0) {
        goto label_0;
    }
    do {
label_7:
        cdb_free_part_0 ();
label_20:
        *(rbx) = 0x2f;
label_3:
        rax = *((rsp + 8));
        ebx = *(rax);
        if (r13d >= 0) {
            goto label_21;
        }
label_6:
        rax = *((rsp + 8));
        r12d = 0xffffffff;
        *(rax) = ebx;
label_2:
        eax = r12d;
        return rax;
label_12:
        eax = 0;
        eax = openat (0xffffff9c, r12, 0x10900);
        r13d = eax;
        if (eax < 0) {
            goto label_22;
        }
        r14 = rbp + r15;
        goto label_1;
label_11:
        rax = memchr (rbp + 3, 0x2f, rbx - 3);
        r14 = rax;
        if (rax == 0) {
            goto label_23;
        }
        *(rax) = 0;
        eax = 0;
        eax = openat (0xffffff9c, rbp, 0x10900);
        *(r14) = 0x2f;
        r13d = eax;
        if (eax < 0) {
            goto label_22;
        }
        r14++;
        rax = strspn (r14, r12);
        r14 += rax;
        goto label_1;
label_16:
        r15d = r13d;
label_17:
        if (rbp <= r14) {
            goto label_24;
        }
        eax = 0;
        eax = openat (r15d, r14, 0x10900);
        r13d = eax;
        if (eax < 0) {
            goto label_25;
        }
        if (r15d >= 0) {
            goto label_26;
        }
label_8:
        edi = r13d;
        eax = fchdir ();
        if (eax != 0) {
            goto label_27;
        }
label_5:
        eax = close (r13d);
    } while (eax != 0);
label_4:
    r12d = 0;
    goto label_2;
label_18:
    rax = *((rsp + 8));
    r12d = 0xffffffff;
    *(rax) = 0x24;
    goto label_2;
label_24:
    edi = r15d;
    r13d = r15d;
    eax = fchdir ();
    if (eax != 0) {
        goto label_3;
    }
    if (r15d < 0) {
        goto label_4;
    }
    goto label_5;
label_27:
    rax = *((rsp + 8));
    ebx = *(rax);
label_21:
    eax = close (r13d);
    if (eax == 0) {
        goto label_6;
    }
    goto label_7;
label_23:
    r12d = 0xffffffff;
    goto label_2;
label_26:
    eax = close (r15d);
    if (eax == 0) {
        goto label_8;
    }
    goto label_7;
label_22:
    rax = *((rsp + 8));
    ebx = *(rax);
    goto label_6;
label_19:
    assert_fail ("slash - dir < 4096", "lib/chdir-long.c", 0xb3, "chdir_long");
label_25:
    r13d = r15d;
    goto label_3;
label_9:
    assert_fail ("0 < len", "lib/chdir-long.c", 0x7e, "chdir_long");
label_10:
    assert_fail ("4096 <= len", "lib/chdir-long.c", 0x7f, "chdir_long");
label_13:
    assert_fail ("*dir != '/', "lib/chdir-long.c", 0xa2, "chdir_long");
label_14:
    return assert_fail ("dir <= dir_end", "lib/chdir-long.c", 0xa3, "chdir_long");
}

/* /tmp/tmprn8a__c4 @ 0x9270 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite ("A NULL argv[0] was passed through an exec system call.\n", 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmprn8a__c4 @ 0x2770 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmprn8a__c4 @ 0x2a10 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmprn8a__c4 @ 0x63a0 */
 
uint64_t dbg_argmatch_invalid (int64_t arg1, int64_t arg2, uint32_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_invalid(char const * context,char const * value,ptrdiff_t problem); */
    r13 = rsi;
    edx = 5;
    if (rdx == -1) {
        goto label_0;
    }
    rax = dcgettext (0, "ambiguous argument %s for %s");
    r12 = rax;
    do {
        rsi = rbp;
        edi = 1;
        rax = quote_n ();
        rdx = r13;
        esi = 8;
        edi = 0;
        rbx = rax;
        rax = quotearg_n_style ();
        r8 = rbx;
        rdx = r12;
        rcx = rax;
        esi = 0;
        edi = 0;
        eax = 0;
        void (*0x2990)() ();
label_0:
        rax = dcgettext (0, "invalid argument %s for %s");
        r12 = rax;
    } while (1);
}

/* /tmp/tmprn8a__c4 @ 0xb5c0 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmprn8a__c4 @ 0xb300 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x000182d0]");
    __asm ("movdqa xmm2, xmmword [0x000182e0]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x2ac9)() ();
    }
    if (rdx == 0) {
        void (*0x2ac9)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0xb660 */
 
void dbg_restore_cwd (int64_t arg_3h, int64_t arg1) {
    int64_t var_8h;
    rdi = arg1;
    /* int restore_cwd(saved_cwd const * cwd); */
    r8d = *(rdi);
    if (r8d >= 0) {
        edi = r8d;
        void (*0x2930)() ();
    }
    rdi = *((rdi + 8));
    return void (*0xd3b0)() ();
}

/* /tmp/tmprn8a__c4 @ 0xb3a0 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x000182d0]");
    __asm ("movdqa xmm2, xmmword [0x000182e0]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x2ace)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x2ace)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0xaee0 */
 
int64_t quotearg_style (uint32_t arg1, int64_t arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x2aba)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0x67c0 */
 
uint64_t dbg_mdir_name (uint32_t arg1) {
    rdi = arg1;
    /* char * mdir_name(char const * file); */
    ebx = 0;
    bl = (*(rdi) == 0x2f) ? 1 : 0;
    rax = last_component ();
    rax -= rbp;
    r12 = rax;
    while (rbx < r12) {
        rax = r12 - 1;
        if (*((rbp + r12 - 1)) != 0x2f) {
            goto label_2;
        }
        r12 = rax;
    }
    rax = r12;
    rax ^= 1;
    ebx = eax;
    ebx &= 1;
    rax = malloc (r12 + rax + 1);
    if (rax == 0) {
        goto label_3;
    }
    rax = memcpy (rax, rbp, r12);
    r8 = rax;
    if (bl == 0) {
        goto label_4;
    }
    *(rax) = 0x2e;
    r12d = 1;
    do {
label_0:
        *((r8 + r12)) = 0;
label_1:
        rax = r8;
        return rax;
label_2:
        rax = malloc (r12 + 1);
        r8 = rax;
        if (rax == 0) {
            goto label_3;
        }
        rax = memcpy (r8, rbp, r12);
        r8 = rax;
    } while (1);
label_4:
    r12d = 1;
    goto label_0;
label_3:
    r8d = 0;
    goto label_1;
}

/* /tmp/tmprn8a__c4 @ 0xb260 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x2ac4)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0x10094 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmprn8a__c4 @ 0xc420 */
 
uint64_t xreallocarray (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmprn8a__c4 @ 0xb600 */
 
uint64_t dbg_save_cwd (int64_t arg1, int64_t arg3) {
    rdi = arg1;
    rdx = arg3;
    /* int save_cwd(saved_cwd * cwd); */
    rbx = rdi;
    eax = 0;
    *((rdi + 8)) = 0;
    eax = open_safer (0x00012b04, 0x80000, rdx, rcx);
    r8d = 0;
    *(rbx) = eax;
    if (eax >= 0) {
        eax = r8d;
        return eax;
    }
    esi = 0;
    edi = 0;
    rax = getcwd ();
    *((rbx + 8)) = rax;
    r8d -= r8d;
    eax = r8d;
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0xd850 */
 
int64_t dbg_open_safer (int64_t arg_60h, int64_t arg3, int32_t oflag, const char * path) {
    va_list ap;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_30h;
    rdx = arg3;
    rsi = oflag;
    rdi = path;
    /* int open_safer(char const * file,int flags,va_args ...); */
    *((rsp + 0x30)) = rdx;
    edx = 0;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    while (1) {
        eax = 0;
        eax = open (rdi, rsi, rdx);
        fd_safer (eax, rsi, rdx, rcx);
        rdx = *((rsp + 0x18));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_0;
        }
        return rax;
        rax = rsp + 0x60;
        *(rsp) = 0x10;
        edx = *((rsp + 0x30));
        *((rsp + 8)) = rax;
        rax = rsp + 0x20;
        *((rsp + 0x10)) = rax;
    }
label_0:
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0x27b0 */
 
void getcwd (void) {
    __asm ("bnd jmp qword [reloc.getcwd]");
}

/* /tmp/tmprn8a__c4 @ 0xc520 */
 
int64_t dbg_x2realloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0xc3c0 */
 
uint64_t xrealloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmprn8a__c4 @ 0x6760 */
 
uint64_t dbg_dir_name (void) {
    /* char * dir_name(char const * file); */
    rax = mdir_name (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprn8a__c4 @ 0xd2f0 */
 
int64_t dbg_last_component (char const * name) {
    rdi = name;
    /* char * last_component(char const * name); */
    edx = *(rdi);
    rax = rdi;
    if (dl != 0x2f) {
        goto label_1;
    }
    do {
        edx = *((rax + 1));
        rax++;
    } while (dl == 0x2f);
label_1:
    if (dl == 0) {
        goto label_2;
    }
    rcx = rax;
    esi = 0;
    while (dl != 0x2f) {
        if (sil != 0) {
            rax = rcx;
            esi = 0;
        }
        edx = *((rcx + 1));
        rcx++;
        if (dl == 0) {
            goto label_2;
        }
label_0:
    }
    edx = *((rcx + 1));
    rcx++;
    esi = 1;
    if (dl != 0) {
        goto label_0;
    }
label_2:
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0xd8d0 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x28d0)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmprn8a__c4 @ 0xc360 */
 
uint64_t xmalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprn8a__c4 @ 0xada0 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmprn8a__c4 @ 0xb690 */
 
void dbg_free_cwd (int64_t arg1) {
    rdi = arg1;
    /* void free_cwd(saved_cwd * cwd); */
    rbx = rdi;
    edi = *(rdi);
    if (edi >= 0) {
        close (rdi);
    }
    rdi = *((rbx + 8));
    return free ();
}

/* /tmp/tmprn8a__c4 @ 0x27d0 */
 
void close (void) {
    __asm ("bnd jmp qword [reloc.close]");
}

/* /tmp/tmprn8a__c4 @ 0xc8a0 */
 
uint64_t dbg_xstrdup (void * arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rax = malloc (r12);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2880)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmprn8a__c4 @ 0xc380 */
 
uint64_t ximalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprn8a__c4 @ 0x6230 */
 
uint64_t dbg_argmatch (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    uint32_t var_17h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* ptrdiff_t argmatch(char const * arg,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = rdi;
    r13 = rcx;
    rbx = rsi;
    *((rsp + 8)) = rsi;
    *((rsp + 0x18)) = rdx;
    rax = strlen (rdi);
    r15 = *(rbx);
    if (r15 == 0) {
        goto label_3;
    }
    *((rsp + 0x17)) = 0;
    r12 = rax;
    ebx = 0;
    *(rsp) = 0xffffffffffffffff;
    goto label_4;
label_0:
    rax = *((rsp + 0x18));
    if (rax == 0) {
        goto label_5;
    }
    rdi *= r13;
    rdi += rax;
    eax = memcmp (*(rsp), rbp, r13);
    ecx = 1;
    eax = *((rsp + 0x17));
    if (eax != 0) {
        eax = ecx;
    }
    *((rsp + 0x17)) = al;
    do {
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r13;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_6;
        }
label_4:
        eax = strncmp (r15, r14, r12);
    } while (eax != 0);
    rax = strlen (r15);
    if (rax == r12) {
        goto label_7;
    }
    if (*(rsp) != -1) {
        goto label_0;
    }
    *(rsp) = rbx;
    goto label_1;
label_5:
    *((rsp + 0x17)) = 1;
    goto label_1;
label_6:
    rax = 0xfffffffffffffffe;
    if (*((rsp + 0x17)) == 0) {
        rax = *(rsp);
    }
    *(rsp) = rax;
    do {
label_2:
        rax = *(rsp);
        return rax;
label_7:
        *(rsp) = rbx;
    } while (1);
label_3:
    *(rsp) = 0xffffffffffffffff;
    goto label_2;
}

/* /tmp/tmprn8a__c4 @ 0xc0e0 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0xbc70)() ();
}

/* /tmp/tmprn8a__c4 @ 0x66b0 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, "write error");
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x00012bd2);
    } while (1);
}

/* /tmp/tmprn8a__c4 @ 0xddf0 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmprn8a__c4 @ 0xc320 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmprn8a__c4 @ 0x5c90 */
 
int64_t dbg_find_mount_point (int64_t arg_8h, int64_t arg_10h, int64_t arg_98h, int64_t arg1, int64_t arg2) {
    saved_cwd cwd;
    stat last_stat;
    int64_t var_110h;
    int64_t var_100h;
    int64_t var_f0h;
    int64_t var_e0h;
    stat st;
    int64_t var_80h;
    int64_t var_70h;
    int64_t var_60h;
    int64_t var_50h;
    int64_t var_38h;
    int64_t var_28h;
    int64_t var_ff8h;
    void * s1;
    rdi = arg1;
    rsi = arg2;
    /* char * find_mount_point(char const * file,stat const * file_stat); */
    r14 = cwd_desc;
    r12 = rdi;
    rbx = rsi;
    rax = *(fs:0x28);
    *((rbp - 0x38)) = rax;
    eax = 0;
    eax = save_cwd (r14, rsi);
    r15d = eax;
    rax = errno_location ();
    r13 = rax;
    if (r15d != 0) {
        goto label_1;
    }
    eax = *((rbx + 0x18));
    eax &= 0xf000;
    if (eax == 0x4000) {
        goto label_2;
    }
    rdi = r12;
    rax = dir_name ();
    rdi = rax;
    r12 = rax;
    rax = strlen (rdi);
    rcx = rsp;
    r8 = rax + 1;
    rax += 0x18;
    rdx = rax;
    rax &= 0xfffffffffffff000;
    rcx -= rax;
    rdx &= 0xfffffffffffffff0;
    if (rsp == rcx) {
        goto label_3;
    }
    do {
    } while (rsp != rcx);
label_3:
    edx &= 0xfff;
    if (rdx != 0) {
    }
    rdi &= 0xfffffffffffffff0;
    rax = memcpy (rsp + 0xf, r12, r8);
    r15 = rax;
    free (r12);
    rdi = r15;
    eax = chdir ();
    if (eax < 0) {
        goto label_4;
    }
    rsi = last_stat_st_dev;
    rdi = 0x00012b04;
    eax = stat ();
    if (eax < 0) {
        rsi = r15;
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, "cannot stat current directory (now %s)");
        rcx = r12;
        eax = 0;
        r12d = 0;
        error (0, *(r13), rax);
        goto label_5;
label_2:
        __asm ("movdqu xmm1, xmmword [rbx]");
        __asm ("movdqu xmm2, xmmword [rbx + 0x10]");
        rdi = r12;
        __asm ("movdqu xmm3, xmmword [rbx + 0x20]");
        __asm ("movdqu xmm4, xmmword [rbx + 0x30]");
        __asm ("movdqu xmm5, xmmword [rbx + 0x40]");
        __asm ("movdqu xmm6, xmmword [rbx + 0x50]");
        *(last_stat.st_dev) = xmm1;
        __asm ("movdqu xmm7, xmmword [rbx + 0x60]");
        __asm ("movdqu xmm1, xmmword [rbx + 0x70]");
        *(last_stat.st_nlink) = xmm2;
        __asm ("movdqu xmm2, xmmword [rbx + 0x80]");
        *(last_stat.st_gid) = xmm3;
        *(last_stat.st_size) = xmm4;
        *(last_stat.st_blocks) = xmm5;
        *((rbp - 0x110)) = xmm6;
        *((rbp - 0x100)) = xmm7;
        *((rbp - 0xf0)) = xmm1;
        *((rbp - 0xe0)) = xmm2;
        eax = chdir ();
        if (eax < 0) {
            goto label_6;
        }
    }
    rbx = st_st_dev;
    r12 = 0x00012b03;
    while (eax >= 0) {
        rax = *(last_stat.st_dev);
        if (*(st.st_dev) != rax) {
            goto label_7;
        }
        rax = *(last_stat.st_ino);
        if (*(st.st_ino) == rax) {
            goto label_7;
        }
        rdi = r12;
        eax = chdir ();
        if (eax < 0) {
            goto label_8;
        }
        __asm ("movdqa xmm0, xmmword [st.st_dev]");
        __asm ("movdqa xmm1, xmmword [st.st_nlink]");
        __asm ("movdqa xmm2, xmmword [st.st_gid]");
        __asm ("movdqa xmm3, xmmword [st.st_size]");
        __asm ("movdqa xmm4, xmmword [st.st_blocks]");
        __asm ("movdqa xmm5, xmmword [rbp - 0x80]");
        *(last_stat.st_dev) = xmm0;
        __asm ("movdqa xmm6, xmmword [rbp - 0x70]");
        __asm ("movdqa xmm7, xmmword [rbp - 0x60]");
        *(last_stat.st_nlink) = xmm1;
        __asm ("movdqa xmm0, xmmword [rbp - 0x50]");
        *(last_stat.st_gid) = xmm2;
        *(last_stat.st_size) = xmm3;
        *(last_stat.st_blocks) = xmm4;
        *((rbp - 0x110)) = xmm5;
        *((rbp - 0x100)) = xmm6;
        *((rbp - 0xf0)) = xmm7;
        *((rbp - 0xe0)) = xmm0;
        rsi = rbx;
        rdi = r12;
        eax = stat ();
    }
    rsi = r12;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    r12 = rax;
    do {
        rax = dcgettext (0, "cannot stat %s");
        rcx = r12;
        eax = 0;
        r12d = 0;
        error (0, *(r13), rax);
        goto label_5;
label_7:
        rax = xgetcwd (rdi);
        r12 = rax;
label_5:
        ebx = *(r13);
        eax = restore_cwd (r14, rsi);
        if (eax != 0) {
            goto label_9;
        }
        free_cwd (r14);
        *(r13) = ebx;
label_0:
        rax = *((rbp - 0x38));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_10;
        }
        rsp = rbp - 0x28;
        rax = r12;
        return rax;
label_8:
        rsi = r12;
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        rsi = "cannot change to directory %s";
        r12 = rax;
    } while (1);
label_1:
    edx = 5;
    r12d = 0;
    rax = dcgettext (0, 0x00012ac8);
    eax = 0;
    error (0, *(r13), rax);
    goto label_0;
label_4:
    rsi = r15;
    do {
        edi = 4;
        rax = quotearg_style ();
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, "cannot change to directory %s");
        rcx = r12;
        eax = 0;
        r12d = 0;
        error (0, *(r13), rax);
        goto label_0;
label_6:
        rsi = r12;
    } while (1);
label_10:
    stack_chk_fail ();
label_9:
    edx = 5;
    rax = dcgettext (0, "failed to return to initial working directory");
    eax = 0;
    error (1, *(r13), rax);
}

/* /tmp/tmprn8a__c4 @ 0xdd70 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0x6560 */
 
int64_t dbg_xargmatch_internal (uint32_t arg_50h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    _Bool allow_abbreviation;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* ptrdiff_t __xargmatch_internal(char const * context,char const * arg,char const * const * arglist, const * vallist,size_t valsize,argmatch_exit_fn exit_fn,_Bool allow_abbreviation); */
    r15 = rdi;
    r14 = rsi;
    r13 = r8;
    r12 = rcx;
    *((rsp + 8)) = r9;
    if (*((rsp + 0x50)) != 0) {
        goto label_2;
    }
    rdi = *(rdx);
    ebx = 0;
    if (rdi != 0) {
        goto label_3;
    }
    do {
        rax = 0xffffffffffffffff;
label_1:
        argmatch_invalid (r15, r14, 0xffffffffffffffff);
        argmatch_valid (rbp, r12, r13);
        rax = *((rsp + 8));
        void (*rax)() ();
        rax = 0xffffffffffffffff;
        goto label_4;
label_0:
        rbx++;
        rdi = *((rbp + rbx*8));
    } while (rdi == 0);
label_3:
    eax = strcmp (rdi, r14);
    if (eax != 0) {
        goto label_0;
    }
    rax = rbx;
    return rax;
label_2:
    rax = argmatch (r14, rbp, r12, r8);
    if (rax < 0) {
        goto label_1;
    }
label_4:
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0xb450 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x000182d0]");
    __asm ("movdqa xmm2, xmmword [0x000182e0]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x2ad3)() ();
    }
    if (rax == 0) {
        void (*0x2ad3)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0xb1d0 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x000182d0]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x000182e0]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0xc770 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprn8a__c4 @ 0x2820 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmprn8a__c4 @ 0xaa70 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0xb590 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmprn8a__c4 @ 0xaa10 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0xc7e0 */
 
uint64_t xmemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2880)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmprn8a__c4 @ 0xacb0 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rbx += 0x10;
        free (*(rbx));
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        free (rdi);
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        free (r12);
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0x9250 */
 
void dbg_nstrftime (int64_t arg5, int64_t arg6) {
    r8 = arg5;
    r9 = arg6;
    /* size_t nstrftime(char * s,size_t maxsize,char const * format,tm const * tp,timezone_t tz,int ns); */
    r9d = 0;
    r8d = 0;
    _strftime_internal_isra_0 ();
}

/* /tmp/tmprn8a__c4 @ 0x66a0 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmprn8a__c4 @ 0xc3f0 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprn8a__c4 @ 0xc5b0 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0x6340 */
 
uint64_t argmatch_exact (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rdi;
    rdi = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    ebx = 0;
    while (eax != 0) {
        rbx++;
        rdi = *((rbp + rbx*8));
        if (rdi == 0) {
            goto label_0;
        }
        eax = strcmp (rdi, r12);
    }
    rax = rbx;
    return rax;
label_0:
    rax = 0xffffffffffffffff;
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0xd7c0 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x2690)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmprn8a__c4 @ 0xc4d0 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmprn8a__c4 @ 0xb5a0 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmprn8a__c4 @ 0xb4f0 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x000182d0]");
    __asm ("movdqa xmm2, xmmword [0x000182e0]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x2ad8)() ();
    }
    if (rax == 0) {
        void (*0x2ad8)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0x6c30 */
 
int64_t dbg_read_file_system_list (void * arg_8h, void * arg_10h, void * arg_18h, int64_t arg_28h) {
    uint32_t devmaj;
    uint32_t devmin;
    int32_t mntroot_s;
    mount_entry * mount_list;
    char * line;
    size_t buf_size;
    void * var_8h;
    void * var_10h;
    void ** var_18h;
    char * s2;
    int64_t var_28h;
    int64_t var_2ch;
    int64_t var_30h;
    char * ptr;
    char * var_40h;
    int64_t var_48h;
    /* mount_entry * read_file_system_list(_Bool need_fs_type); */
    rax = *(fs:0x28);
    *((rsp + 0x48)) = rax;
    eax = 0;
    rax = fopen ("/proc/self/mountinfo", 0x00012c9e);
    if (rax == 0) {
        goto label_11;
    }
    r12 = rax;
    rax = rsp + 0x30;
    *((rsp + 0x38)) = 0;
    r14 = rsp + 0x40;
    *(rsp) = rax;
    rbx = rsp + 0x38;
    r15 = "%*u %*u %u:%u %n";
    *((rsp + 0x40)) = 0;
    do {
label_0:
        rcx = r12;
        edx = 0xa;
        rsi = r14;
        rdi = rbx;
        rax = getdelim ();
        if (rax == -1) {
            goto label_12;
        }
        rdi = *((rsp + 0x38));
        rcx = rsp + 0x28;
        rsi = r15;
        eax = 0;
        rdx = rsp + 0x24;
        r8 = rsp + 0x2c;
        eax = isoc99_sscanf ();
        eax -= 2;
    } while (eax > 1);
    r13 = *((rsp + 0x2c));
    r13 += *((rsp + 0x38));
    rax = strchr (r13, 0x20);
    if (rax == 0) {
        goto label_0;
    }
    *(rax) = 0;
    rbp = rax + 1;
    rax = strchr (rbp, 0x20);
    if (rax == 0) {
        goto label_0;
    }
    *(rax) = 0;
    rax = strstr (rax + 1, 0x00012c7d);
    if (rax == 0) {
        goto label_0;
    }
    rax += 3;
    rdi = rax;
    *((rsp + 8)) = rax;
    rax = strchr (rdi, 0x20);
    if (rax == 0) {
        goto label_0;
    }
    *(rax) = 0;
    r8 = rax + 1;
    rdi = r8;
    *((rsp + 0x10)) = r8;
    rax = strchr (rdi, 0x20);
    if (rax == 0) {
        goto label_0;
    }
    r8 = *((rsp + 0x10));
    *(rax) = 0;
    rdi = r8;
    *((rsp + 0x18)) = r8;
    unescape_tab (rdi);
    unescape_tab (rbp);
    unescape_tab (r13);
    unescape_tab (*((rsp + 8)));
    rax = xmalloc (0x38);
    *((rsp + 0x10)) = rax;
    rax = xstrdup (*((rsp + 0x18)));
    rdx = *((rsp + 0x10));
    *(rdx) = rax;
    rax = xstrdup (rbp);
    rdx = *((rsp + 0x10));
    r13d = 1;
    *((rdx + 8)) = rax;
    rax = xstrdup (r13);
    rdx = *((rsp + 0x10));
    *((rdx + 0x10)) = rax;
    rax = xstrdup (*((rsp + 8)));
    ecx = *((rsp + 0x24));
    rdx = *((rsp + 0x10));
    rdi = 0xfffff00000000000;
    rsi = rcx;
    *((rdx + 0x18)) = rax;
    rcx <<= 0x20;
    eax = *((rsp + 0x28));
    rsi <<= 8;
    rcx &= rdi;
    *((rdx + 0x28)) |= 4;
    esi &= 0xfff00;
    *((rsp + 8)) = rdx;
    rcx |= rsi;
    esi = (int32_t) al;
    rax <<= 0xc;
    rcx |= rsi;
    rsi = 0xffffff00000;
    rax &= rsi;
    rax |= rcx;
    *((rdx + 0x20)) = rax;
    eax = strcmp (rbp, "autofs");
    rdx = *((rsp + 8));
    while (eax == 0) {
label_1:
        eax = *((rdx + 0x28));
        *((rsp + 8)) = rdx;
        eax &= 0xfffffffe;
        eax |= r13d;
        r13 = *(rdx);
        *((rdx + 0x28)) = al;
        rax = strchr (r13, 0x3a);
        rdx = *((rsp + 8));
        ecx = 1;
        if (rax == 0) {
            goto label_13;
        }
label_2:
        eax = *((rdx + 0x28));
        ecx += ecx;
        eax &= 0xfffffffd;
        eax |= ecx;
        *((rdx + 0x28)) = al;
        rax = *(rsp);
        *(rax) = rdx;
        rax = rdx + 0x30;
        *(rsp) = rax;
        goto label_0;
label_12:
        free (*((rsp + 0x38)));
        if ((*(r12) & 0x20) != 0) {
            goto label_14;
        }
        eax = rpl_fclose (r12);
        if (eax == 0xffffffff) {
            goto label_15;
        }
label_5:
        rax = *(rsp);
        *(rax) = 0;
        r14 = *((rsp + 0x30));
label_4:
        rax = *((rsp + 0x48));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_16;
        }
        rax = r14;
        return rax;
        eax = strcmp (rbp, "proc");
        rdx = *((rsp + 8));
    }
    eax = strcmp (rbp, "subfs");
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, "debugfs");
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, "devpts");
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, "fusectl");
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, "fuse.portal");
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, "mqueue");
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, "rpc_pipefs");
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, "sysfs");
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, 0x000125c6);
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, "kernfs");
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, "ignore");
    rdx = *((rsp + 8));
    if (eax == 0) {
        goto label_1;
    }
    eax = strcmp (rbp, "none");
    rdx = *((rsp + 8));
    r13b = (eax == 0) ? 1 : 0;
    goto label_1;
label_13:
    if (*(r13) == 0x2f) {
        goto label_17;
    }
label_7:
    *((rsp + 8)) = rdx;
    eax = strcmp (rbp, "acfs");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    *((rsp + 0x10)) = cl;
    eax = strcmp (rbp, 0x00012465);
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    eax = strcmp (rbp, "coda");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    eax = strcmp (rbp, "auristorfs");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    eax = strcmp (rbp, "fhgfs");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    eax = strcmp (rbp, "gpfs");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    eax = strcmp (rbp, "ibrix");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    eax = strcmp (rbp, "ocfs2");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    eax = strcmp (rbp, "vxfs");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    eax = strcmp ("-hosts", r13);
    rdx = *((rsp + 8));
    cl = (eax == 0) ? 1 : 0;
    goto label_2;
label_14:
    rax = errno_location ();
    r14d = *(rax);
    r13 = rax;
    rpl_fclose (r12);
    *(r13) = r14d;
label_6:
    rax = *(rsp);
    *(rax) = 0;
    rbx = *((rsp + 0x30));
    if (rbx != 0) {
        goto label_18;
    }
    goto label_19;
    do {
label_3:
        free (rbp);
        *((rsp + 0x30)) = rbx;
        if (rbx == 0) {
            goto label_19;
        }
label_18:
        rbx = *((rbx + 0x30));
        free (*(rbp));
        free (*((rbp + 8)));
        free (*((rbp + 0x10)));
    } while ((*((rbp + 0x28)) & 4) == 0);
    free (*((rbp + 0x18)));
    goto label_3;
label_19:
    *(r13) = r14d;
    r14d = 0;
    goto label_4;
label_8:
    rdi = r14;
    eax = endmntent ();
    if (eax != 0) {
        goto label_5;
    }
label_15:
    rax = errno_location ();
    r14d = *(rax);
    r13 = rax;
    goto label_6;
label_17:
    if (*((r13 + 1)) != 0x2f) {
        goto label_7;
    }
    *((rsp + 0x10)) = cl;
    eax = strcmp (rbp, "smbfs");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    eax = strcmp (rbp, "smb3");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    eax = strcmp (rbp, "cifs");
    rdx = *((rsp + 8));
    ecx = 1;
    if (eax == 0) {
        goto label_2;
    }
    goto label_7;
label_11:
    rsi = 0x000127b3;
    rdi = "/etc/mtab";
    rax = setmntent ();
    r14 = rax;
    if (rax == 0) {
        goto label_4;
    }
    rax = rsp + 0x30;
    r15 = "autofs";
    *(rsp) = rax;
    goto label_20;
label_9:
    eax = *((rbx + 0x28));
    r12d += r12d;
    *((rbx + 0x20)) = 0xffffffffffffffff;
    eax &= 0xfffffffd;
    eax |= r12d;
    *((rbx + 0x28)) = al;
    rax = *(rsp);
    *(rax) = rbx;
    rax = rbx + 0x30;
    *(rsp) = rax;
label_20:
    rdi = r14;
    rax = getmntent ();
    if (rax == 0) {
        goto label_8;
    }
    rsi = "bind";
    rdi = rbp;
    r12d = 1;
    rax = hasmntopt ();
    r13 = rax;
    rax = xmalloc (0x38);
    rbx = rax;
    rax = xstrdup (*(rbp));
    *(rbx) = rax;
    rax = xstrdup (*((rbp + 8)));
    *((rbx + 0x10)) = 0;
    *((rbx + 8)) = rax;
    rax = xstrdup (*((rbp + 0x10)));
    *((rbx + 0x28)) |= 4;
    *((rbx + 0x18)) = rax;
    rdi = rax;
    eax = strcmp (rdi, r15);
    if (eax != 0) {
        eax = strcmp (rbp, "proc");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "subfs");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "debugfs");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "devpts");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "fusectl");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "fuse.portal");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "mqueue");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "rpc_pipefs");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "sysfs");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, 0x000125c6);
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "kernfs");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "ignore");
        if (eax == 0) {
            goto label_21;
        }
        eax = strcmp (rbp, "none");
        r12b = (r13 == 0) ? 1 : 0;
        al = (eax == 0) ? 1 : 0;
        r12d &= eax;
    }
label_21:
    eax = *((rbx + 0x28));
    r13 = *(rbx);
    eax &= 0xfffffffe;
    eax |= r12d;
    r12d = 1;
    *((rbx + 0x28)) = al;
    rax = strchr (r13, 0x3a);
    if (rax != 0) {
        goto label_9;
    }
    if (*(r13) == 0x2f) {
        goto label_22;
    }
label_10:
    r12d = 1;
    eax = strcmp (rbp, "acfs");
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp (rbp, 0x00012465);
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp (rbp, "coda");
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp (rbp, "auristorfs");
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp (rbp, "fhgfs");
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp (rbp, "gpfs");
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp (rbp, "ibrix");
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp (rbp, "ocfs2");
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp (rbp, "vxfs");
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp ("-hosts", r13);
    r12b = (eax == 0) ? 1 : 0;
    goto label_9;
label_16:
    stack_chk_fail ();
label_22:
    if (*((r13 + 1)) != 0x2f) {
        goto label_10;
    }
    eax = strcmp (rbp, "smbfs");
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp (rbp, "smb3");
    if (eax == 0) {
        goto label_9;
    }
    eax = strcmp (rbp, "cifs");
    if (eax == 0) {
        goto label_9;
    }
    goto label_10;
}

/* /tmp/tmprn8a__c4 @ 0x6430 */
 
uint64_t dbg_argmatch_valid (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_valid(char const * const * arglist, const * vallist,size_t valsize); */
    r13d = 0;
    r12 = rdx;
    edx = 5;
    rbx = rdi;
    r14 = stderr;
    *((rsp + 8)) = rdi;
    rax = dcgettext (0, "Valid arguments are:");
    rsi = r14;
    r14 = "\n  - %s";
    rdi = rax;
    fputs_unlocked ();
    r15 = *(rbx);
    ebx = 0;
    if (r15 != 0) {
        goto label_2;
    }
    goto label_3;
    do {
label_0:
        r13 = rbp;
        rax = quote (r15, rsi, rdx, rcx, r8);
        rdi = stderr;
        rdx = r14;
        esi = 1;
        rcx = rax;
        eax = 0;
        fprintf_chk ();
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r12;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_3;
        }
label_2:
    } while (rbx == 0);
    eax = memcmp (r13, rbp, r12);
    if (eax != 0) {
        goto label_0;
    }
    rax = quote (r15, rsi, rdx, rcx, r8);
    rdi = stderr;
    esi = 1;
    rdx = ", %s";
    rcx = rax;
    eax = 0;
    fprintf_chk ();
    goto label_1;
label_3:
    rdi = stderr;
    rax = *((rdi + 0x28));
    if (rax < *((rdi + 0x30))) {
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0xa;
        return rax;
    }
    esi = 0xa;
    return overflow ();
}

/* /tmp/tmprn8a__c4 @ 0xc820 */
 
uint64_t dbg_ximemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2880)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmprn8a__c4 @ 0xb740 */
 
int64_t dbg_tzalloc (int64_t arg1) {
    rdi = arg1;
    /* timezone_t tzalloc(char const * name); */
    if (rdi == 0) {
        goto label_0;
    }
    strlen (rdi);
    rbx = rax + 1;
    eax = 0x76;
    if (rbx >= rax) {
        rax = rbx;
    }
    rdi += 0x11;
    rdi &= 0xfffffffffffffff8;
    rax = malloc (rax);
    r12 = rax;
    if (rax == 0) {
        goto label_1;
    }
    *(rax) = 0;
    eax = 1;
    *((r12 + 8)) = ax;
    memcpy (r12 + 9, rbp, rbx);
    *((r12 + rbx + 9)) = 0;
    do {
label_1:
        rax = r12;
        return rax;
label_0:
        rax = malloc (0x80);
        r12 = rax;
    } while (rax == 0);
    edx = 0;
    *(r12) = 0;
    rax = r12;
    *((r12 + 8)) = dx;
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0x26e0 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmprn8a__c4 @ 0x2880 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmprn8a__c4 @ 0x6780 */
 
uint64_t dir_len (uint32_t arg1) {
    rdi = arg1;
    ebp = 0;
    rbx = rdi;
    bpl = (*(rdi) == 0x2f) ? 1 : 0;
    rax = last_component ();
    rax -= rbx;
    while (rax > rbp) {
        rdx = rax - 1;
        if (*((rbx + rax - 1)) != 0x2f) {
            goto label_0;
        }
        rax = rdx;
    }
label_0:
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0xdeb0 */
 
int32_t dbg_dup_safer (int64_t arg_80h, int64_t arg1, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* int dup_safer(int fd); */
    edx = 3;
    esi = 0;
    eax = 0;
    return void (*0xee50)() ();
}

/* /tmp/tmprn8a__c4 @ 0xaca0 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0xabb0)() ();
}

/* /tmp/tmprn8a__c4 @ 0xba50 */
 
uint64_t dbg_localtime_rz (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
label_1:
    __asm ("bnd jmp qword [reloc.gmtime_r]");
    /* tm * localtime_rz(timezone_t tz,time_t const * t,tm * tm); */
    r14 = rsi;
    if (rdi == 0) {
        goto label_2;
    }
    r12 = rdi;
    rax = set_tz (rdi);
    r13 = rax;
    if (rax == 0) {
        goto label_0;
    }
    rsi = rbp;
    rdi = r14;
    rax = fcn_00002550 ();
    if (rax == 0) {
        goto label_3;
    }
    al = save_abbr (r12, rbp, rdx, rcx, r8);
    if (al == 0) {
        goto label_3;
    }
    while (al != 0) {
        rax = rbp;
        return rax;
label_3:
        if (r13 != 1) {
            rdi = r13;
            eax = revert_tz_part_0 ();
        }
label_0:
        eax = 0;
        return rax;
        rdi = r13;
        al = revert_tz_part_0 ();
    }
    goto label_0;
label_2:
    rdi = r14;
    rsi = rdx;
    goto label_1;
}

/* /tmp/tmprn8a__c4 @ 0xc950 */
 
int64_t dbg_xasprintf (int64_t arg_e0h, int64_t arg1, int64_t arg10, int64_t arg11, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6, int64_t arg7, int64_t arg8, int64_t arg9) {
    va_list args;
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_60h;
    int64_t var_70h;
    int64_t var_80h;
    int64_t var_90h;
    int64_t var_a0h;
    int64_t var_b0h;
    int64_t var_c0h;
    rdi = arg1;
    xmm3 = arg10;
    xmm4 = arg11;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * xasprintf(char const * format,va_args ...); */
    *((rsp + 0x28)) = rsi;
    *((rsp + 0x30)) = rdx;
    *((rsp + 0x38)) = rcx;
    *((rsp + 0x40)) = r8;
    *((rsp + 0x48)) = r9;
    if (al != 0) {
        *((rsp + 0x50)) = xmm0;
        *((rsp + 0x60)) = xmm1;
        *((rsp + 0x70)) = xmm2;
        *((rsp + 0x80)) = xmm3;
        *((rsp + 0x90)) = xmm4;
        *((rsp + 0xa0)) = xmm5;
        *((rsp + 0xb0)) = xmm6;
        *((rsp + 0xc0)) = xmm7;
    }
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    rax = rsp + 0xe0;
    *(rsp) = 8;
    *((rsp + 8)) = rax;
    rax = rsp + 0x20;
    *((rsp + 4)) = 0x30;
    *((rsp + 0x10)) = rax;
    xvasprintf (rdi, rsp, rdx);
    rdx = *((rsp + 0x18));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0xded0 */
 
int64_t vasnprintf (void * arg1, void ** arg2, int64_t arg3, int64_t arg4) {
    int64_t var_418h;
    int64_t var_40ch;
    int64_t var_408h;
    void ** var_400h;
    int64_t var_3f8h;
    int64_t var_3f0h;
    void * s2;
    void * var_3e0h;
    int64_t var_3d8h;
    void ** var_3d0h;
    void ** var_3c8h;
    int64_t var_3bch;
    int64_t var_3b8h;
    int64_t var_3b4h;
    int64_t var_3b0h;
    void ** var_3a8h;
    int64_t var_3a0h;
    uint32_t var_2c0h;
    void ** var_2b8h;
    int64_t var_2b0h;
    int64_t var_2a8h;
    int64_t var_2a0h;
    int64_t var_38h;
    int64_t var_28h;
    int64_t var_ff8h;
    int64_t var_fh;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
label_0:
    abort ();
    r14 = section__gnu_hash;
    r13 = rdx;
    r12 = rcx;
    *((rbp - 0x3e8)) = rdi;
    *((rbp - section..dynsym)) = rsi;
    rax = *(fs:0x28);
    *((rbp - 0x38)) = rax;
    eax = 0;
    eax = printf_parse (r13, rbp - 0x2c0, r14);
    if (eax < 0) {
        goto label_34;
    }
    eax = printf_fetchargs (r12, r14);
    if (eax < 0) {
        goto label_35;
    }
    rdx = *((rbp - 0x2b0));
    rax = rdx + 7;
    rdx = 0xffffffffffffffff;
    if (rdx >= 0xfffffffffffffff9) {
        rax = rdx;
    }
    rax += *((rbp - 0x2a8));
    if (rax < 0) {
        goto label_36;
    }
    rdi = rax;
    r8d = 0;
    rdi += 6;
    r8b = (rdi < 0) ? 1 : 0;
    if (rdi < 0) {
        goto label_36;
    }
    if (rdi <= 0xf9f) {
        goto label_37;
    }
    *((rbp - 0x3c8)) = r8;
    if (rdi == -1) {
        goto label_36;
    }
    rax = malloc (rdi);
    *((rbp - 0x3e0)) = rax;
    if (rax == 0) {
        goto label_36;
    }
    *((rbp - 0x408)) = rax;
    r8 = *((rbp - 0x3c8));
label_11:
    ebx = 0;
    if (*((rbp - 0x3e8)) != 0) {
        rax = *((rbp - section..dynsym));
        rbx = *(rax);
    }
    r14 = *((rbp - 0x2b8));
    r9 = r8;
    r8 = r13;
    *((rbp - 0x3f8)) = 0;
    r15 = *((rbp - 0x3e8));
    r13 = *(r14);
    if (r13 == r8) {
        goto label_38;
    }
label_15:
    r13 -= r8;
    r12 = r9;
    rax = 0xffffffffffffffff;
    r12 += r13;
    if (r12 < 0) {
        r12 = rax;
    }
    if (rbx >= r12) {
        goto label_39;
    }
    if (rbx == 0) {
        goto label_40;
    }
    if (rbx < 0) {
        goto label_41;
    }
    rbx += rbx;
label_17:
    if (rbx < r12) {
        rbx = r12;
    }
    if (rbx == -1) {
        goto label_41;
    }
    *((rbp - 0x3d0)) = r8;
    *((rbp - 0x3c8)) = r9;
    if (r15 == *((rbp - 0x3e8))) {
        goto label_42;
    }
    rax = realloc (r15, rbx);
    r9 = *((rbp - 0x3c8));
    r8 = *((rbp - 0x3d0));
    r10 = rax;
    if (rax == 0) {
        goto label_41;
    }
label_18:
    *((rbp - 0x3c8)) = r10;
    memcpy (r10 + r9, r8, r13);
    r10 = *((rbp - 0x3c8));
label_16:
    rax = *((rbp - 0x3f8));
    if (*((rbp - 0x2c0)) == rax) {
        goto label_43;
    }
    edx = *((r14 + 0x48));
    rax = *((r14 + 0x50));
    if (dl == 0x25) {
        goto label_44;
    }
    if (rax == -1) {
        void (*0x2add)() ();
    }
    r15 = *((rbp - 0x3a8));
    rax <<= 5;
    rax += r15;
    ecx = *(rax);
    *((rbp - 0x3c8)) = ecx;
    if (dl == 0x6e) {
        goto label_45;
    }
    rcx = *((rbp - 0x3e0));
    eax = *((r14 + 0x10));
    *((rbp - 0x3b8)) = 0;
    *(rcx) = 0x25;
    r13 = rcx + 1;
    if ((al & 1) != 0) {
        rdx = *((rbp - 0x3e0));
        *((rdx + 1)) = 0x27;
        r13 = rdx + 2;
    }
    if ((al & 2) != 0) {
        *(r13) = 0x2d;
        r13++;
    }
    if ((al & 4) != 0) {
        *(r13) = 0x2b;
        r13++;
    }
    if ((al & 8) != 0) {
        *(r13) = 0x20;
        r13++;
    }
    if ((al & 0x10) != 0) {
        *(r13) = 0x23;
        r13++;
    }
    if ((al & 0x40) != 0) {
        *(r13) = 0x49;
        r13++;
    }
    if ((al & 0x20) != 0) {
        *(r13) = 0x30;
        r13++;
    }
    rsi = *((r14 + 0x18));
    rax = *((r14 + 0x20));
    if (rsi != rax) {
        rax -= rsi;
        *((rbp - 0x3d8)) = r10;
        rdx = rax;
        *((rbp - 0x3d0)) = rax;
        memcpy (r13, rsi, rdx);
        rdx = *((rbp - 0x3d0));
        r10 = *((rbp - 0x3d8));
        r13 += rdx;
    }
    rsi = *((r14 + 0x30));
    rax = *((r14 + 0x38));
    if (rsi != rax) {
        rax -= rsi;
        *((rbp - 0x3d8)) = r10;
        rdx = rax;
        *((rbp - 0x3d0)) = rax;
        memcpy (r13, rsi, rdx);
        rdx = *((rbp - 0x3d0));
        r10 = *((rbp - 0x3d8));
        r13 += rdx;
    }
    eax = *((rbp - 0x3c8));
    eax -= 7;
    if (eax > 9) {
        goto label_12;
    }
    rcx = 0x00013724;
    rax = *((rcx + rax*4));
    rax += rcx;
    /* switch table (10 cases) at 0x13724 */
    void (*rax)() ();
label_36:
    errno_location ();
    *(rax) = 0xc;
    do {
label_4:
        rdi = *((rbp - 0x2b8));
        rax = rbp - 0x2a0;
        if (rdi != rax) {
            free (rdi);
        }
        rdi = *((rbp - 0x3a8));
        rax = rbp - 0x3a0;
        if (rdi != rax) {
            free (rdi);
        }
label_34:
        r10d = 0;
label_31:
        rax = *((rbp - 0x38));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_46;
        }
        rsp = rbp - 0x28;
        rax = r10;
        return rax;
label_35:
        errno_location ();
        *(rax) = 0x16;
    } while (1);
    *(r13) = 0x6c;
    r13++;
    *(r13) = 0x6c;
    r13++;
label_12:
    eax = *((r14 + 0x48));
    *((r13 + 1)) = 0;
    *(r13) = al;
    rax = *((r14 + 0x28));
    if (rax == -1) {
        goto label_47;
    }
    rax <<= 5;
    rax += r15;
    if (*(rax) != 5) {
        void (*0x2add)() ();
    }
    *((rbp - 0x3d8)) = 1;
    eax = *((rax + 0x10));
    *((rbp - 0x3b8)) = eax;
label_21:
    rax = *((r14 + 0x40));
    if (rax == -1) {
        goto label_48;
    }
    rax <<= 5;
    rcx = r15 + rax;
    if (*(rcx) != 5) {
        void (*0x2add)() ();
    }
    eax = *((rbp - 0x3d8));
    edx = *((rcx + 0x10));
    *((rbp + rax*4 - 0x3b8)) = edx;
    eax = rax + 1;
    *((rbp - 0x3d8)) = eax;
label_48:
    rax = r12;
    rax += 2;
    if (rax < 0) {
        goto label_49;
    }
    if (rbx >= rax) {
        goto label_50;
    }
    if (rbx != 0) {
        goto label_51;
    }
    if (rax > 0xc) {
        goto label_52;
    }
    ebx = 0xc;
label_26:
    if (r10 == *((rbp - 0x3e8))) {
        goto label_53;
    }
    rdi = r10;
    *((rbp - 0x3d0)) = r10;
    rax = realloc (rdi, rbx);
    r10 = *((rbp - 0x3d0));
    r15 = rax;
    if (rax == 0) {
        goto label_25;
    }
label_22:
    *((r15 + r12)) = 0;
    rax = errno_location ();
    *((rbp - 0x3f0)) = r13;
    *((rbp - 0x3d0)) = rax;
    eax = *(rax);
    *((rbp - 0x40c)) = eax;
label_1:
    rax = *((rbp - 0x3d0));
    r13 = rbx;
    esi = 0x7fffffff;
    *((rbp - 0x3bc)) = 0xffffffff;
    r13 -= r12;
    *(rax) = 0;
    eax = *((rbp - 0x3c8));
    if (r13 <= rsi) {
        rsi = r13;
    }
    if (eax > 0x11) {
        goto label_0;
    }
    rdi = 0x0001374c;
    rax = *((rdi + rax*4));
    rax += rdi;
    /* switch table (18 cases) at 0x1374c */
    void (*rax)() ();
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9 = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax == 1) {
        goto label_54;
    }
    if (eax == 2) {
        goto label_55;
    }
    rax = rbp - 0x3bc;
label_5:
    r8 = *((rbp - 0x3e0));
    rcx = 0xffffffffffffffff;
    edx = 1;
    eax = 0;
    *((rbp - 0x418)) = rsi;
    eax = snprintf_chk ();
    rsi = *((rbp - 0x418));
label_6:
    edx = *((rbp - 0x3bc));
    if (edx < 0) {
        goto label_56;
    }
label_2:
    rcx = (int64_t) edx;
    if (rcx >= rsi) {
        goto label_57;
    }
    rcx += r15;
    if (*((rcx + r12)) != 0) {
        void (*0x2add)() ();
    }
label_57:
    if (edx < eax) {
label_3:
        *((rbp - 0x3bc)) = eax;
        edx = eax;
    }
    eax = rdx + 1;
    if (rax < rsi) {
        goto label_58;
    }
    if (r13 > 0x7ffffffe) {
        goto label_59;
    }
    if (rbx < 0) {
        goto label_60;
    }
    eax = rdx + 2;
    rcx = rbx + rbx;
    rax += r12;
    if (rax < 0) {
        goto label_23;
    }
    if (rax < rcx) {
        rax = rcx;
    }
    if (rbx >= rax) {
        goto label_1;
    }
    if (rcx >= rax) {
        rax = rcx;
    }
    rbx = rax;
    if (rax == -1) {
        goto label_23;
    }
    if (r15 == *((rbp - 0x3e8))) {
        goto label_61;
    }
    rax = realloc (r15, rax);
    if (rax == 0) {
        goto label_23;
    }
    r15 = rax;
    goto label_1;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9d = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax == 1) {
        goto label_10;
    }
label_7:
    if (eax == 2) {
        goto label_62;
    }
label_9:
    rax = rbp - 0x3bc;
label_8:
    r8 = *((rbp - 0x3e0));
    rcx = 0xffffffffffffffff;
    edx = 1;
    eax = 0;
    *((rbp - 0x418)) = rsi;
    eax = snprintf_chk ();
    edx = *((rbp - 0x3bc));
    rsi = *((rbp - 0x418));
    if (edx >= 0) {
        goto label_2;
    }
label_56:
    rcx = *((rbp - 0x3f0));
    if (*((rcx + 1)) != 0) {
        goto label_63;
    }
    if (eax >= 0) {
        goto label_3;
    }
    rax = *((rbp - 0x3d0));
    eax = *(rax);
    if (eax == 0) {
        eax = *((r14 + 0x48));
        edx = 0x54;
        rbx = *((rbp - 0x3d0));
        eax &= 0xffffffef;
        eax = 0x16;
        if (al == 0x63) {
            eax = edx;
        }
        *(rbx) = eax;
    }
label_20:
    if (r15 != *((rbp - 0x3e8))) {
        free (r15);
    }
    rax = *((rbp - 0x408));
    if (rax == 0) {
        goto label_4;
    }
    free (rax);
    goto label_4;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    *(fp_stack--) = fp_stack[?];
    eax = *((rbp - 0x3d8));
    if (eax == 1) {
        goto label_64;
    }
    if (eax == 2) {
        goto label_65;
    }
    r9 = rbp - 0x3bc;
    ? = fp_stack[0];
    fp_stack--;
    goto label_5;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    xmm0 = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax == 1) {
        goto label_66;
    }
    if (eax == 2) {
        goto label_67;
    }
    r8 = *((rbp - 0x3e0));
    edx = 1;
    eax = 1;
    r9 = rbp - 0x3bc;
    rcx = 0xffffffffffffffff;
    *((rbp - 0x418)) = rsi;
    snprintf_chk ();
    rsi = *((rbp - 0x418));
    goto label_6;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9d = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax != 1) {
        goto label_7;
    }
    do {
label_10:
        rax = rbp - 0x3bc;
label_14:
        r9d = *((rbp - 0x3b8));
        goto label_8;
        rax = *((r14 + 0x50));
        rdi = r15 + r12;
        rax <<= 5;
        rax += *((rbp - 0x3a8));
        r9d = *((rax + 0x10));
        eax = *((rbp - 0x3d8));
    } while (eax == 1);
    if (eax != 2) {
        goto label_9;
    }
label_62:
    rax = rbp - 0x3bc;
label_13:
    eax = *((rbp - 0x3b4));
label_24:
    r9d = *((rbp - 0x3b8));
    r8 = *((rbp - 0x3e0));
    rcx = 0xffffffffffffffff;
    eax = 0;
    edx = 1;
    *((rbp - 0x418)) = rsi;
    snprintf_chk ();
    rsi = *((rbp - 0x418));
    goto label_6;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9d = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax != 1) {
        goto label_7;
    }
    goto label_10;
    rax = *((r14 + 0x50));
    rdi = r15 + r12;
    rax <<= 5;
    rax += *((rbp - 0x3a8));
    r9d = *((rax + 0x10));
    eax = *((rbp - 0x3d8));
    if (eax != 1) {
        goto label_7;
    }
    goto label_10;
label_37:
    rax += 0x1d;
    rcx = rsp;
    rdx = rax;
    rax &= 0xfffffffffffff000;
    rcx -= rax;
    rdx &= 0xfffffffffffffff0;
    if (rsp == rcx) {
        goto label_68;
    }
    do {
    } while (rsp != rcx);
label_68:
    edx &= 0xfff;
    if (rdx != 0) {
        goto label_69;
    }
label_29:
    *((rbp - 0x408)) = 0;
    rax = rsp + 0xf;
    rax &= 0xfffffffffffffff0;
    *((rbp - 0x3e0)) = rax;
    goto label_11;
    *(r13) = 0x4c;
    r13++;
    goto label_12;
label_60:
    if (rbx == -1) {
        goto label_1;
    }
    goto label_23;
label_61:
    rax = malloc (rax);
    if (rax == 0) {
        goto label_23;
    }
    if (r12 == 0) {
        goto label_70;
    }
    rax = memcpy (rax, r15, r12);
    r15 = rax;
    goto label_1;
label_55:
    rax = rbp - 0x3bc;
    goto label_13;
label_54:
    rax = rbp - 0x3bc;
    goto label_14;
label_63:
    *((rcx + 1)) = 0;
    goto label_1;
label_44:
    if (rax != -1) {
        void (*0x2add)() ();
    }
    r15 = r12 + 1;
    r9 = 0xffffffffffffffff;
    if (r12 < -1) {
        r9 = r15;
    }
    if (rbx < r9) {
        if (rbx == 0) {
            goto label_71;
        }
        if (rbx < 0) {
            goto label_25;
        }
        rbx += rbx;
label_27:
        if (rbx < r9) {
            rbx = r9;
        }
        if (rbx == -1) {
            goto label_25;
        }
        if (r10 == *((rbp - 0x3e8))) {
            goto label_72;
        }
        rdi = r10;
        *((rbp - 0x3d0)) = r9;
        *((rbp - 0x3c8)) = r10;
        rax = realloc (rdi, rbx);
        r9 = *((rbp - 0x3d0));
        if (rax == 0) {
            goto label_30;
        }
        r10 = rax;
    }
label_28:
    *((r10 + r12)) = 0x25;
    r15 = r10;
label_19:
    r8 = *((r14 + 8));
    r13 = *((r14 + 0x58));
    r14 += 0x58;
    *((rbp - 0x3f8))++;
    if (r13 != r8) {
        goto label_15;
    }
label_38:
    r12 = r9;
    r10 = r15;
    goto label_16;
label_40:
    ebx = 0xc;
    goto label_17;
label_39:
    r10 = r15;
    goto label_18;
label_45:
    r13d = ecx;
    r13d -= 0x12;
    if (r13d > 4) {
        goto label_0;
    }
    rcx = 0x00013794;
    rax = *((rax + 0x10));
    rdx = *((rcx + r13*4));
    rdx += rcx;
    /* switch table (5 cases) at 0x13794 */
    void (*rdx)() ();
    *(rax) = r12;
    r9 = r12;
    r15 = r10;
    goto label_19;
    *(rax) = r12d;
    r9 = r12;
    r15 = r10;
    goto label_19;
    *(rax) = r12w;
    r9 = r12;
    r15 = r10;
    goto label_19;
    *(rax) = r12b;
    r9 = r12;
    r15 = r10;
    goto label_19;
label_49:
    if (rbx == -1) {
        goto label_50;
    }
label_25:
    *((rbp - 0x3c8)) = r10;
label_30:
    rax = errno_location ();
    r15 = *((rbp - 0x3c8));
    *((rbp - 0x3d0)) = rax;
label_23:
    rax = *((rbp - 0x3d0));
    *(rax) = 0xc;
    goto label_20;
label_47:
    *((rbp - 0x3d8)) = 0;
    goto label_21;
label_42:
    rax = malloc (rbx);
    r9 = *((rbp - 0x3c8));
    r8 = *((rbp - 0x3d0));
    r10 = rax;
    if (rax == 0) {
        goto label_41;
    }
    if (r9 == 0) {
        goto label_18;
    }
    rdx = r9;
    *((rbp - 0x3d0)) = r8;
    *((rbp - 0x3c8)) = r9;
    rax = memcpy (rax, *((rbp - 0x3e8)), rdx);
    r9 = *((rbp - 0x3c8));
    r8 = *((rbp - 0x3d0));
    r10 = rax;
    goto label_18;
label_50:
    r15 = r10;
    goto label_22;
label_41:
    rax = errno_location ();
    *((rbp - 0x3d0)) = rax;
    goto label_23;
label_65:
    rax = rbp - 0x3bc;
    r9d = *((rbp - 0x3b8));
    r8 = *((rbp - 0x3e0));
    eax = *((rbp - 0x3b4));
    rcx = 0xffffffffffffffff;
    edx = 1;
    *((rbp - 0x418)) = rsi;
    ? = fp_stack[0];
    fp_stack--;
    eax = 0;
    snprintf_chk ();
    rsi = *((rbp - 0x418));
    goto label_6;
label_64:
    rax = rbp - 0x3bc;
    ? = fp_stack[0];
    fp_stack--;
    goto label_24;
label_67:
    rax = rbp - 0x3bc;
    eax = *((rbp - 0x3b4));
    do {
        r9d = *((rbp - 0x3b8));
        edx = 1;
        eax = 1;
        r8 = *((rbp - 0x3e0));
        rcx = 0xffffffffffffffff;
        *((rbp - 0x418)) = rsi;
        snprintf_chk ();
        rsi = *((rbp - 0x418));
        goto label_6;
label_66:
        rax = rbp - 0x3bc;
    } while (1);
    if (r9 < 0) {
label_51:
        goto label_25;
    }
    rbx += rbx;
    if (rbx >= rax) {
        goto label_26;
    }
label_52:
    if (rax == -1) {
        goto label_25;
    }
    rbx = rax;
    goto label_26;
label_58:
    rax = (int64_t) edx;
    edx = *((rbp - 0x40c));
    r9 = rax + r12;
    rax = *((rbp - 0x3d0));
    *(rax) = edx;
    goto label_19;
label_59:
    rax = *((rbp - 0x3d0));
    *(rax) = 0x4b;
    goto label_20;
label_71:
    ebx = 0xc;
    goto label_27;
label_53:
    rax = malloc (rbx);
    r15 = rax;
    if (rax == 0) {
        goto label_73;
    }
    if (r12 == 0) {
        goto label_22;
    }
    memcpy (rax, *((rbp - 0x3e8)), r12);
    goto label_22;
label_72:
    *((rbp - 0x3d0)) = r10;
    *((rbp - 0x3c8)) = r9;
    rax = malloc (rbx);
    r9 = *((rbp - 0x3c8));
    r10 = *((rbp - 0x3d0));
    if (rax == 0) {
        goto label_73;
    }
    if (r12 == 0) {
        goto label_74;
    }
    *((rbp - 0x3c8)) = r9;
    rax = memcpy (rax, r10, r12);
    r9 = *((rbp - 0x3c8));
    r10 = rax;
    goto label_28;
label_69:
    goto label_29;
label_43:
    r13 = r12;
    r13++;
    if (r13 < 0) {
        goto label_75;
    }
    if (rbx >= r13) {
        goto label_33;
    }
    if (rbx != 0) {
        goto label_76;
    }
    if (r13 > 0xc) {
        goto label_77;
    }
    ebx = 0xc;
label_32:
    if (r10 == *((rbp - 0x3e8))) {
        goto label_78;
    }
    rdi = r10;
    *((rbp - 0x3c8)) = r10;
    rax = realloc (rdi, rbx);
    r10 = rax;
    if (rax == 0) {
        goto label_30;
    }
label_33:
    *((r10 + r12)) = 0;
    if (rbx > r13) {
        if (r10 == *((rbp - 0x3e8))) {
            goto label_79;
        }
        rdi = r10;
        *((rbp - 0x3c8)) = r10;
        rax = realloc (rdi, r13);
        r10 = *((rbp - 0x3c8));
        if (rax == 0) {
            r10 = rax;
            goto label_79;
        }
    }
label_79:
    rdi = *((rbp - 0x408));
    if (rdi != 0) {
        *((rbp - 0x3c8)) = r10;
        free (rdi);
        r10 = *((rbp - 0x3c8));
    }
    rdi = *((rbp - 0x2b8));
    rax = rbp - 0x2a0;
    if (rdi != rax) {
        *((rbp - 0x3c8)) = r10;
        free (rdi);
        r10 = *((rbp - 0x3c8));
    }
    rdi = *((rbp - 0x3a8));
    rax = rbp - 0x3a0;
    if (rdi != rax) {
        *((rbp - 0x3c8)) = r10;
        free (rdi);
        r10 = *((rbp - 0x3c8));
    }
    rax = *((rbp - section..dynsym));
    *(rax) = r12;
    goto label_31;
    if (rdi < rax) {
label_76:
        goto label_25;
    }
    rbx += rbx;
    if (rbx >= r13) {
        goto label_32;
    }
label_77:
    if (r13 == -1) {
        goto label_25;
    }
    rbx = r13;
    goto label_32;
label_78:
    *((rbp - 0x3c8)) = r10;
    rax = malloc (rbx);
    r10 = *((rbp - 0x3c8));
    if (rax == 0) {
        goto label_73;
    }
    if (r12 == 0) {
        goto label_80;
    }
    rax = memcpy (rax, r10, r12);
    r10 = rax;
    goto label_33;
label_46:
    stack_chk_fail ();
label_75:
    if (rbx == -1) {
        goto label_33;
    }
    goto label_25;
label_73:
    rax = errno_location ();
    r15 = *((rbp - 0x3e8));
    *((rbp - 0x3d0)) = rax;
    goto label_23;
label_70:
    r15 = rax;
    goto label_1;
label_74:
    r10 = rax;
    goto label_28;
label_80:
    r10 = rax;
    goto label_33;
}

/* /tmp/tmprn8a__c4 @ 0xf0c0 */
 
int64_t dbg_printf_fetchargs (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* int printf_fetchargs(__va_list_tag * args,arguments * a); */
    r8 = *(rsi);
    rax = *((rsi + 8));
    rcx = rdi;
    if (r8 == 0) {
        goto label_6;
    }
    esi = 0;
    rdi = 0x000137b0;
    r10 = "(NULL)";
    r9 = "(NULL)";
    do {
        if (*(rax) > 0x16) {
            goto label_7;
        }
        edx = *(rax);
        rdx = *((rdi + rdx*4));
        rdx += rdi;
        /* switch table (23 cases) at 0x137b0 */
        rax = void (*rdx)() ();
        edx = *(rcx);
        if (edx > 0x2f) {
            goto label_8;
        }
        r11d = edx;
        edx += 8;
        r11 += *((rcx + 0x10));
        *(rcx) = edx;
label_1:
        rdx = *(r11);
        *((rax + 0x10)) = rdx;
label_0:
        rsi++;
        rax += 0x20;
    } while (rsi != r8);
label_6:
    eax = 0;
    return rax;
    edx = *(rcx);
    if (edx > 0x2f) {
        goto label_9;
    }
    r11d = edx;
    edx += 8;
    r11 += *((rcx + 0x10));
    *(rcx) = edx;
    do {
        edx = *(r11);
        *((rax + 0x10)) = edx;
        goto label_0;
        edx = *(rcx);
        if (edx > 0x2f) {
            goto label_10;
        }
        r11d = edx;
        edx += 8;
        r11 += *((rcx + 0x10));
        *(rcx) = edx;
label_2:
        edx = *(r11);
        *((rax + 0x10)) = dx;
        goto label_0;
        edx = *(rcx);
        if (edx > 0x2f) {
            goto label_11;
        }
        r11d = edx;
        edx += 8;
        r11 += *((rcx + 0x10));
        *(rcx) = edx;
label_3:
        edx = *(r11);
        *((rax + 0x10)) = dl;
        goto label_0;
label_8:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
        goto label_1;
label_9:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
    } while (1);
    edx = *((rcx + 4));
    if (edx > 0xaf) {
        goto label_12;
    }
    r11d = edx;
    edx += 0x10;
    r11 += *((rcx + 0x10));
    *((rcx + 4)) = edx;
label_4:
    xmm0 = *(r11);
    *((rax + 0x10)) = xmm0;
    goto label_0;
    rdx = *((rcx + 8));
    rdx += 0xf;
    rdx &= 0xfffffffffffffff0;
    r11 = rdx + 0x10;
    *((rcx + 8)) = r11;
    *(fp_stack--) = fp_stack[?];
    ? = fp_stack[0];
    fp_stack--;
    goto label_0;
    edx = *(rcx);
    if (edx > 0x2f) {
        goto label_13;
    }
    r11d = edx;
    edx += 8;
    r11 += *((rcx + 0x10));
    *(rcx) = edx;
label_5:
    rdx = *(r11);
    if (rdx == 0) {
        rdx = r10;
    }
    *((rax + 0x10)) = rdx;
    goto label_0;
    edx = *(rcx);
    if (edx > 0x2f) {
        goto label_14;
    }
    r11d = edx;
    edx += 8;
    r11 += *((rcx + 0x10));
    *(rcx) = edx;
    do {
        rdx = *(r11);
        if (rdx == 0) {
            rdx = r9;
        }
        *((rax + 0x10)) = rdx;
        goto label_0;
label_10:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
        goto label_2;
label_11:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
        goto label_3;
label_14:
        r11 = *((rcx + 8));
        rdx = r11 + 8;
        *((rcx + 8)) = rdx;
    } while (1);
label_12:
    r11 = *((rcx + 8));
    rdx = r11 + 8;
    *((rcx + 8)) = rdx;
    goto label_4;
label_13:
    r11 = *((rcx + 8));
    rdx = r11 + 8;
    *((rcx + 8)) = rdx;
    goto label_5;
label_7:
    eax |= 0xffffffff;
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0xd2c0 */
 
void dbg_rpl_mktime (int64_t arg1, mktime_offset_t localtime_offset) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_78h;
    int64_t var_7fh;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_a0h;
    int64_t var_ach;
    int64_t var_b8h;
    int64_t var_b0h;
    int64_t var_d4h_2;
    int64_t var_d4h;
    int64_t var_c0h;
    int64_t var_d0h;
    int64_t var_e0h;
    int64_t var_ech;
    int64_t var_f8h;
    int64_t var_114h_2;
    int64_t var_114h;
    int64_t var_100h;
    int64_t var_118h;
    rdi = arg1;
    xmm2 = localtime_offset;
    /* time_t rpl_mktime(tm * tp); */
    tzset ();
    rsi = *(reloc.localtime_r);
    rdi = rbp;
    rdx = obj_localtime_offset_0;
    return void (*0xcd10)() ();
}

/* /tmp/tmprn8a__c4 @ 0xad80 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmprn8a__c4 @ 0x7690 */
 
void free_mount_entry (void * arg_8h, void * arg_10h, void * arg_18h, int64_t arg_28h, void ** ptr) {
    rdi = ptr;
    free (*(rdi));
    free (*((rbp + 8)));
    free (*((rbp + 0x10)));
    if ((*((rbp + 0x28)) & 4) == 0) {
        rdi = rbp;
        void (*0x25b0)() ();
    }
    free (*((rbp + 0x18)));
    rdi = rbp;
    return free ();
}

/* /tmp/tmprn8a__c4 @ 0x25b0 */
 
void free (void) {
    __asm ("bnd jmp qword [reloc.free]");
}

/* /tmp/tmprn8a__c4 @ 0x6090 */
 
int64_t dbg_areadlink_with_size (int64_t arg_8h, int64_t arg_10h, int64_t arg_98h, int64_t arg1, uint32_t arg2) {
    char[128] stackbuf;
    int64_t var_98h;
    rdi = arg1;
    rsi = arg2;
    /* char * areadlink_with_size(char const * file,size_t size); */
    ebx = 0x80;
    rax = *(fs:0x28);
    *((rsp + 0x98)) = rax;
    eax = 0;
    r14b = (rsi != 0) ? 1 : 0;
    if (rsi != 0) {
        rbx = rsi + 1;
        eax = 0x401;
        if (rsi < 0x401) {
            rbx = rax;
            goto label_3;
        }
    }
label_3:
    r12 = 0x3fffffffffffffff;
    rax = rsp + 0x10;
    *((rsp + 8)) = rax;
label_1:
    if (rbx != 0x80) {
        goto label_4;
    }
    do {
        r13 = *((rsp + 8));
        r15d = 0;
        if (r14b != 0) {
            goto label_4;
        }
label_0:
        rdx = rbx;
        rsi = r13;
        rdi = rbp;
        rax = readlink ();
        if (rax < 0) {
            goto label_5;
        }
        if (rbx > rax) {
            goto label_6;
        }
        free (r15);
        if (rbx > r12) {
            goto label_7;
        }
        rbx += rbx;
    } while (rbx == 0x80);
label_4:
    rax = malloc (rbx);
    r13 = rax;
    if (rax == 0) {
        goto label_8;
    }
    r15 = rax;
    goto label_0;
label_7:
    rax = 0x7fffffffffffffff;
    if (rbx == rax) {
        goto label_8;
    }
    rbx = 0x7fffffffffffffff;
    goto label_1;
label_5:
    r15d = 0;
    free (r15);
    do {
label_2:
        rax = *((rsp + 0x98));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_9;
        }
        rax = r15;
        return rax;
label_6:
        *((r13 + rax)) = 0;
        r12 = rax + 1;
        if (r15 == 0) {
            goto label_10;
        }
    } while (rbx <= r12);
    rax = realloc (r15, r12);
    if (rax != 0) {
        r15 = rax;
    }
    goto label_2;
label_8:
    errno_location ();
    r15d = 0;
    *(rax) = 0xc;
    goto label_2;
label_10:
    rax = malloc (r12);
    r15 = rax;
    if (rax == 0) {
        goto label_2;
    }
    memcpy (rax, r13, r12);
    goto label_2;
label_9:
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0x2af0 */
 
int64_t dbg_main (int32_t argc, char ** argv) {
    statfs statfsbuf;
    signed int64_t var_ch;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_98h;
    rdi = argc;
    rsi = argv;
    /* int main(int argc,char ** argv); */
    r15 = 0x00013341;
    r14 = obj_long_options;
    r13 = "c:fLt";
    r12 = 0x000126ee;
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x98)) = rax;
    eax = 0;
    set_program_name (*(rsi), rsi, rdx);
    setlocale (6, r15);
    bindtextdomain (r12, "/usr/local/share/locale");
    textdomain (r12, rsi);
    rax = localeconv ();
    rdi = *(rax);
    rax = 0x00012b04;
    if (*(rdi) == 0) {
        rdi = rax;
    }
    r12d = 0;
    *(obj.decimal_point) = rdi;
    rax = strlen (rdi);
    rdi = dbg_close_stdout;
    *(obj.decimal_point_len) = rax;
    atexit ();
    *((rsp + 0xc)) = 0;
    *((rsp + 0x10)) = 0;
    do {
label_0:
        r8d = 0;
        rcx = r14;
        rdx = r13;
        rsi = rbx;
        edi = ebp;
        eax = getopt_long ();
        if (eax == 0xffffffff) {
            goto label_4;
        }
        if (eax == 0x63) {
            goto label_5;
        }
        if (eax <= 0x63) {
            if (eax == 0) {
                goto label_6;
            }
            if (eax > 0) {
                goto label_7;
            }
            if (eax == 0xffffff7d) {
                goto label_8;
            }
            if (eax != 0xffffff7e) {
                goto label_9;
            }
            eax = usage (0);
        }
        if (eax == 0x74) {
            goto label_10;
        }
        if (eax != 0x80) {
            goto label_11;
        }
        rax = optarg;
        *(obj.interpret_backslash_escapes) = 1;
        *(obj.trailing_delim) = r15;
        *((rsp + 0x10)) = rax;
    } while (1);
label_7:
    if (eax == 0x4c) {
        *(obj.follow_links) = 1;
        goto label_0;
label_11:
        if (eax != 0x66) {
            goto label_9;
        }
        r12d = 1;
        goto label_0;
label_10:
        *((rsp + 0xc)) = 1;
        goto label_0;
label_1:
        edx = 5;
        rax = dcgettext (0, "missing operand");
        eax = 0;
        error (0, 0, rax);
    }
label_9:
    rax = usage (1);
label_6:
    _xargmatch_internal ("--cached", *(obj.optarg), obj.cached_args, obj.cached_modes, 4, *(obj.argmatch_die));
    rcx = obj_cached_modes;
    eax = *((rcx + rax*4));
    if (eax == 1) {
        goto label_12;
    }
    if (eax == 2) {
        goto label_13;
    }
    if (eax != 0) {
        goto label_0;
    }
    *(obj.force_sync) = 0;
    *(obj.dont_sync) = 0;
    goto label_0;
label_5:
    rax = optarg;
    *(obj.interpret_backslash_escapes) = 0;
    *((rsp + 0x10)) = rax;
    rax = 0x00013340;
    *(obj.trailing_delim) = rax;
    goto label_0;
label_8:
    eax = 0;
    version_etc (*(obj.stdout), "stat", "GNU coreutils", *(obj.Version), "Michael Meskes", 0);
    exit (0);
label_4:
    if (*(obj.optind) == ebp) {
        goto label_1;
    }
    r15 = *((rsp + 0x10));
    if (r15 == 0) {
        goto label_14;
    }
    rax = strstr (r15, 0x0001275d);
    *((rsp + 0x18)) = r15;
    if (rax != 0) {
        rax = getenv ("QUOTING_STYLE");
        r13 = rax;
        if (rax == 0) {
            goto label_15;
        }
        r15 = obj_quoting_style_vals;
        eax = argmatch (rax, obj.quoting_style_args, r15, 4);
        if (eax < 0) {
            goto label_16;
        }
        rax = (int64_t) eax;
        esi = *((r15 + rax*4));
        set_quoting_style (0);
    }
label_3:
    *((rsp + 0xc)) = ebp;
    r14 = *(obj.optind);
    r13d = 1;
    r15 = *((rsp + 0x10));
    while (r12b != 0) {
        eax = strcmp (rbp, 0x00012606);
        if (eax == 0) {
            goto label_17;
        }
        r8 = rsp + 0x20;
        rdi = rbp;
        rsi = r8;
        *((rsp + 0x10)) = r8;
        eax = statfs ();
        r8 = *((rsp + 0x10));
        if (eax != 0) {
            goto label_18;
        }
        esi |= 0xffffffff;
        eax = print_it (r15, rsi, rbp, dbg.print_statfs, r8, r9);
        eax ^= 1;
label_2:
        eax = (int32_t) al;
        r14++;
        r13d &= eax;
        if (*((rsp + 0xc)) <= r14d) {
            goto label_19;
        }
        rbp = *((rbx + r14*8));
    }
    do_stat (rbp, r15, *((rsp + 0x18)), rcx, r8, r9);
    goto label_2;
label_17:
    rsi = rbp;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    rax = dcgettext (0, "using %s to denote standard input does not work in file system mode");
    rcx = rbp;
    eax = 0;
    eax = error (0, 0, rax);
    eax = 0;
    goto label_2;
label_13:
    *(obj.force_sync) = 0;
    *(obj.dont_sync) = 1;
    goto label_0;
label_12:
    *(obj.force_sync) = 1;
    *(obj.dont_sync) = 0;
    goto label_0;
label_19:
    r13d ^= 1;
    edi = (int32_t) r13b;
    exit (rdi);
label_18:
    rsi = rbp;
    edi = 4;
    rax = quotearg_style ();
    edx = 5;
    *((rsp + 0x10)) = rax;
    rax = dcgettext (0, "cannot read file system information for %s");
    rax = errno_location ();
    rcx = *((rsp + 0x10));
    eax = 0;
    eax = error (0, *(rax), rbp);
    eax = 0;
    goto label_2;
label_14:
    r13d = *((rsp + 0xc));
    r14d = (int32_t) r12b;
    edx = 0;
    esi = r13d;
    rax = default_format (r14d);
    edx = 1;
    esi = r13d;
    *((rsp + 0x10)) = rax;
    rax = default_format (r14d);
    *((rsp + 0x18)) = rax;
    goto label_3;
label_16:
    esi = 4;
    set_quoting_style (0);
    rax = quote (r13, rsi, rdx, rcx, r8);
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "ignoring invalid value of environment variable QUOTING_STYLE: %s");
    rcx = r13;
    eax = 0;
    error (0, 0, rax);
    goto label_3;
label_15:
    esi = 4;
    set_quoting_style (0);
    goto label_3;
}

/* /tmp/tmprn8a__c4 @ 0x5740 */
 
int64_t dbg_usage (int64_t arg1) {
    infomap const[7] const infomap;
    char * var_8h;
    int64_t var_10h;
    char * var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    char * var_60h;
    int64_t var_68h;
    int64_t var_78h;
    rdi = arg1;
    /* void usage(int status); */
    edx = 5;
    r12 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r12;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_0:
        exit (ebp);
    }
    rbx = rsp;
    rax = dcgettext (0, "Usage: %s [OPTION]... FILE...\n");
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "Display file or file system status.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nMandatory arguments to long options are mandatory for short options too.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -L, --dereference     follow links\n  -f, --file-system     display file system status instead of file status\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --cached=MODE     specify how to use cached attributes;\n                          useful on remote file systems. See MODE below\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -c  --format=FORMAT   use the specified FORMAT instead of the default;\n                          output a newline after each use of FORMAT\n      --printf=FORMAT   like --format, but interpret backslash escapes,\n                          and do not output a mandatory trailing newline;\n                          if you want a newline, include \\n in FORMAT\n  -t, --terse           print the information in terse form\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nThe MODE argument of --cached can be: always, never, or default.\n'always' will use cached attributes if available, while\n'never' will try to synchronize with the latest attributes, and\n'default' will leave it up to the underlying file system.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nThe valid format sequences for files (without --file-system):\n\n  %a   permission bits in octal (note '#' and '0' printf flags)\n  %A   permission bits and file type in human readable form\n  %b   number of blocks allocated (see %B)\n  %B   the size in bytes of each block reported by %b\n  %C   SELinux security context string\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  %d   device number in decimal (st_dev)\n  %D   device number in hex (st_dev)\n  %Hd  major device number in decimal\n  %Ld  minor device number in decimal\n  %f   raw mode in hex\n  %F   file type\n  %g   group ID of owner\n  %G   group name of owner\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  %h   number of hard links\n  %i   inode number\n  %m   mount point\n  %n   file name\n  %N   quoted file name with dereference if symbolic link\n  %o   optimal I/O transfer size hint\n  %s   total size, in bytes\n  %r   device type in decimal (st_rdev)\n  %R   device type in hex (st_rdev)\n  %Hr  major device type in decimal, for character/block device special files\n  %Lr  minor device type in decimal, for character/block device special files\n  %t   major device type in hex, for character/block device special files\n  %T   minor device type in hex, for character/block device special files\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  %u   user ID of owner\n  %U   user name of owner\n  %w   time of file birth, human-readable; - if unknown\n  %W   time of file birth, seconds since Epoch; 0 if unknown\n  %x   time of last access, human-readable\n  %X   time of last access, seconds since Epoch\n  %y   time of last data modification, human-readable\n  %Y   time of last data modification, seconds since Epoch\n  %z   time of last status change, human-readable\n  %Z   time of last status change, seconds since Epoch\n\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "Valid format sequences for file systems:\n\n  %a   free blocks available to non-superuser\n  %b   total data blocks in file system\n  %c   total file nodes in file system\n  %d   free file nodes in file system\n  %f   free blocks in file system\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  %i   file system ID in hex\n  %l   maximum length of filenames\n  %n   file name\n  %s   block size (for faster transfers)\n  %S   fundamental block size (for block counts)\n  %t   file system type in hex\n  %T   file system type in human readable form\n");
    rsi = r12;
    r12 = "stat";
    rdi = rax;
    fputs_unlocked ();
    edx = 5;
    rax = dcgettext (0, "\n--terse is equivalent to the following FORMAT:\n    %s");
    rdx = "%n %s %b %f %u %g %D %i %h %t %T %X %Y %Z %W %o\n";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "--terse --file-system is equivalent to the following FORMAT:\n    %s");
    rdx = "%n %i %l %t %s %S %b %f %a %c %d\n";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell's documentation\nfor details about the options it supports.\n");
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    rax = 0x00012674;
    rcx = "sha256sum";
    *((rsp + 0x60)) = 0;
    *(rsp) = rax;
    rax = "test invocation";
    *((rsp + 8)) = rax;
    rax = 0x000126ee;
    *((rsp + 0x10)) = rax;
    rax = "Multi-call invocation";
    *((rsp + 0x18)) = rax;
    rax = "sha224sum";
    *((rsp + 0x30)) = rcx;
    rcx = "sha384sum";
    *((rsp + 0x20)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x40)) = rcx;
    rcx = "sha512sum";
    *((rsp + 0x28)) = rax;
    *((rsp + 0x38)) = rax;
    *((rsp + 0x48)) = rax;
    *((rsp + 0x50)) = rcx;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x68)) = 0;
    do {
        rsi = *((rbx + 0x10));
        rbx += 0x10;
        if (rsi == 0) {
            goto label_2;
        }
        eax = strcmp (r12, rsi);
    } while (eax != 0);
label_2:
    r13 = *((rbx + 8));
    edx = 5;
    rsi = "\n%s online help: <%s>\n";
    edi = 0;
    if (r13 == 0) {
        goto label_3;
    }
    rax = dcgettext (rdi, rsi);
    r14 = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    rcx = r14;
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax != 0) {
        eax = strncmp (rdi, 0x000126f8, 3);
        if (eax != 0) {
            goto label_4;
        }
    }
label_1:
    edx = 5;
    rax = dcgettext (0, "Full documentation <%s%s>\n");
    rcx = r12;
    rdx = r14;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    rax = 0x00013341;
    r12 = 0x00012690;
    r12 = rax;
    while (1) {
        edx = 5;
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rcx = r12;
        rdx = r13;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        goto label_0;
label_3:
        rax = dcgettext (rdi, rsi);
        r14 = "https://www.gnu.org/software/coreutils/";
        edi = 1;
        rdx = "GNU coreutils";
        rsi = rax;
        rcx = r14;
        eax = 0;
        printf_chk ();
        rax = setlocale (5, 0);
        rdi = rax;
        if (rax != 0) {
            eax = strncmp (rdi, 0x000126f8, 3);
            if (eax != 0) {
                goto label_5;
            }
        }
        edx = 5;
        rax = dcgettext (0, "Full documentation <%s%s>\n");
        rcx = r12;
        rdx = r14;
        edi = 1;
        rsi = rax;
        eax = 0;
        r13 = "stat";
        printf_chk ();
        r12 = 0x00012690;
    }
label_5:
    r13 = "stat";
label_4:
    r15 = stdout;
    edx = 5;
    rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
    rdi = rax;
    rsi = r15;
    fputs_unlocked ();
    goto label_1;
}

/* /tmp/tmprn8a__c4 @ 0xcd10 */
 
uint64_t rotate_right64 (uint64_t value, uint32_t count) {
    const uint64_t mask = (CHAR_BIT * sizeof (value)) - 1;
    count &= mask;
    return (value >> count) | (value << (-count & mask));
}
 
int64_t dbg_mktime_internal (int64_t arg2, int64_t arg3, int32_t mon) {
    long_int t;
    long_int ot;
    tm tm;
    time_t x;
    uint32_t var_8h;
    int64_t var_10h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    signed int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_78h;
    uint32_t var_7fh;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_a0h;
    int64_t var_ach;
    int64_t var_b8h;
    int64_t var_b0h;
    int64_t var_d4h_2;
    int64_t var_d4h;
    int64_t var_c0h;
    int64_t var_d0h;
    int64_t var_e0h;
    int64_t var_ech;
    int64_t var_f8h;
    int64_t var_114h_2;
    int64_t var_114h;
    int64_t var_100h;
    int64_t var_118h;
    rsi = arg2;
    rdx = arg3;
    rdi = mon;
    /* time_t mktime_internal(tm * tp,tm * (*)() convert,mktime_offset_t * offset); */
    rbx = rdi;
    rcx = *((rdi + 0x10));
    *((rsp + 0x58)) = rdi;
    *((rsp + 0x20)) = rsi;
    rsi = *((rdi + 0xc));
    *((rsp + 0x40)) = rdx;
    rax = *(fs:0x28);
    *((rsp + 0x118)) = rax;
    eax = *(rdi);
    *((rsp + 0x48)) = eax;
    eax = *((rdi + 4));
    *((rsp + 0x10)) = eax;
    eax = *((rdi + 8));
    rdi = rcx;
    rcx *= 0x2aaaaaab;
    *((rsp + 0x14)) = eax;
    eax = *((rbx + 0x20));
    rcx >>= 0x21;
    *((rsp + 0x38)) = eax;
    eax = edi;
    eax >>= 0x1f;
    ecx -= eax;
    edx = rcx * 3;
    eax = ecx;
    edx <<= 2;
    edi -= edx;
    edx = edi;
    ecx = edi;
    edx >>= 0x1f;
    eax -= edx;
    rdx = *((rbx + 0x14));
    rax = (int64_t) eax;
    r15 = rax + rdx;
    edx = 0;
    if ((r15b & 3) == 0) {
        rax = 0x8f5c28f5c28f5c29;
        rdx = 0x51eb851eb851eb8;
        rdi = 0x28f5c28f5c28f5c;
        rax *= r15;
        rax += rdx;
        edx = 1;
        rax = rotate_right64 (rax, 2);
        if (rax <= rdi) {
            goto label_6;
        }
    }
label_1:
    eax = ecx;
    r12d = 0x3b;
    r9d = 0x46;
    rdi = r15;
    eax >>= 0x1f;
    eax &= 0xc;
    eax += ecx;
    rcx = rdx * 3;
    rdx = rdx + rcx*4;
    rax = (int64_t) eax;
    rax += rdx;
    rdx = obj___mon_yday;
    eax = *((rdx + rax*2));
    eax--;
    rax = (int64_t) eax;
    rsi += rax;
    rax = *((rsp + 0x48));
    *((rsp + 0x18)) = rsi;
    if (eax <= r12d) {
        r12 = rax;
    }
    eax = 0;
    __asm ("cmovs r12, rax");
    rax = *((rsp + 0x40));
    rax = *(rax);
    *((rsp + 8)) = rax;
    eax = *((rsp + 8));
    eax = -eax;
    *((rsp + 0x4c)) = eax;
    rax = ydhms_diff (rdi, rsi, *((rsp + 0x34)), *((rsp + 0x30)), r12d, r9);
    *((rsp + 0x50)) = rax;
    r13 = rsp + 0xa0;
    r14 = rsp + 0x88;
    *((rsp + 0x88)) = rax;
    *((rsp + 8)) = rax;
    *((rsp + 0x28)) = 6;
    *((rsp + 0x30)) = 0;
    do {
        rax = ranged_convert (*((rsp + 0x20)), r14, r13);
        if (rax == 0) {
            goto label_4;
        }
        ebx = *((rsp + 0xa0));
        eax = *((rsp + 0xac));
        eax = *((rsp + 0xb8));
        eax = *((rsp + 0xd4));
        rax = ydhms_diff (r15, *((rsp + 0x38)), *((rsp + 0x34)), *((rsp + 0x30)), r12d, *((rsp + 0xd4)));
        r10 = *((rsp + 0x88));
        if (rax == 0) {
            goto label_7;
        }
        if (rbp != r10) {
            if (*((rsp + 8)) == r10) {
                goto label_8;
            }
        }
label_0:
        if (*((rsp + 8)) == r10) {
            goto label_5;
        }
        edx = *((rsp + 0xc0));
        rax += r10;
        *((rsp + 8)) = rbp;
        *((rsp + 0x88)) = rax;
        eax = 0;
        al = (edx != 0) ? 1 : 0;
        *((rsp + 0x30)) = eax;
    } while (1);
label_8:
    esi = *((rsp + 0xc0));
    if (esi < 0) {
        goto label_2;
    }
    ecx = *((rsp + 0x38));
    dl = (esi != 0) ? 1 : 0;
    if (ecx < 0) {
        goto label_9;
    }
    cl = (ecx != 0) ? 1 : 0;
    if (cl == dl) {
        goto label_0;
    }
label_2:
    rax = *((rsp + 0x4c));
    rdx = r10;
    rax += *((rsp + 0x50));
    rdx -= rax;
    edi = *((rsp + 0x48));
    rax = *((rsp + 0x40));
    *(rax) = rdx;
    if (edi != ebx) {
        al = (ebx == 0x3c) ? 1 : 0;
        edx = 0;
        dl = (edi <= 0) ? 1 : 0;
        rdx &= rax;
        rax = (int64_t) edi;
        rdx -= r12;
        rax += rdx;
        rax += r10;
        *((rsp + 0x88)) = rax;
        if (rax overflow 0) {
            goto label_5;
        }
        rdi = rsp + 0xe0;
        rsi = r13;
        rax = *((rsp + 0x20));
        rax = void (*rax)(uint64_t, uint64_t) (rax, rax);
        r10 = *((rsp + 8));
        if (rax == 0) {
            goto label_4;
        }
    }
    rcx = *((rsp + 0x58));
    __asm ("movdqa xmm0, xmmword [rsp + 0xa0]");
    __asm ("movdqa xmm1, xmmword [rsp + 0xb0]");
    rax = *((rsp + 0xd0));
    __asm ("movdqa xmm2, xmmword [rsp + 0xc0]");
    __asm ("movups xmmword [rcx], xmm0");
    *((rcx + 0x30)) = rax;
    __asm ("movups xmmword [rcx + 0x10], xmm1");
    __asm ("movups xmmword [rcx + 0x20], xmm2");
    goto label_10;
label_5:
    errno_location ();
    *(rax) = 0x4b;
label_4:
    r10 = 0xffffffffffffffff;
label_10:
    rax = *((rsp + 0x118));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_11;
    }
    rax = r10;
    return rax;
label_6:
    rdx = 0xa3d70a3d70a3d70b;
    rax = r15;
    rdx:rax = rax * rdx;
    rax = r15;
    rax >>= 0x3f;
    rdx += r15;
    rdx >>= 6;
    rdx -= rax;
    edx &= 3;
    dl = (rdx == 1) ? 1 : 0;
    edx = (int32_t) dl;
    goto label_1;
label_9:
    edx = (int32_t) dl;
    if (edx < *((rsp + 0x30))) {
        goto label_0;
    }
    goto label_2;
label_7:
    esi = *((rsp + 0x38));
    eax = *((rsp + 0xc0));
    rsp + 8 = (esi == 0) ? 1 : 0;
    edi = *((rsp + 8));
    rsp + 0x7f = (eax == 0) ? 1 : 0;
    ecx = *((rsp + 0x7f));
    if (cl == dil) {
        goto label_2;
    }
    eax |= esi;
    if (eax < 0) {
        goto label_2;
    }
    rax = rsp + 0xe0;
    *((rsp + 0x78)) = r12d;
    r12 = *((rsp + 0x20));
    *((rsp + 0x28)) = rax;
    rax = rsp + 0x90;
    r14d = 0x92c70;
    *((rsp + 0x30)) = rax;
    rax = rsp + 0x98;
    *((rsp + 0x68)) = rax;
    *((rsp + 0x70)) = r15;
    *((rsp + 0x60)) = r13;
    while (r13d == 1) {
        r14d += 0x92c70;
        if (r14d == 0xdb04f20) {
            goto label_12;
        }
        ebx = r14d;
        r15d = r14 + r14;
        r13d = 2;
        ebx = -ebx;
        rax = (int64_t) ebx;
        rax += rbp;
        *((rsp + 0x90)) = rax;
        if (rax !overflow 0) {
            goto label_13;
        }
label_3:
        ebx += r15d;
    }
    rax = (int64_t) ebx;
    r13d = 1;
    rax += rbp;
    *((rsp + 0x90)) = rax;
    if (rax overflow 0) {
        goto label_3;
    }
label_13:
    rax = ranged_convert (r12, *((rsp + 0x30)), *((rsp + 0x28)));
    if (rax == 0) {
        goto label_4;
    }
    eax = *((rsp + 0x100));
    dl = (eax == 0) ? 1 : 0;
    if (*((rsp + 8)) == dl) {
        goto label_14;
    }
    if (eax >= 0) {
        goto label_3;
    }
label_14:
    eax = *((rsp + 0xe0));
    eax = *((rsp + 0xec));
    eax = *((rsp + 0xf8));
    eax = *((rsp + 0x114));
    rax = ydhms_diff (*((rsp + 0x90)), *((rsp + 0x38)), *((rsp + 0x34)), *((rsp + 0x30)), *((rsp + 0x98)), *((rsp + 0x114)));
    rax += *((rsp + 0xb0));
    rsi = *((rsp + 0x60));
    rdi = *((rsp + 0x68));
    rax = void (*r12)(uint64_t) (rax);
    rdx = *((rsp + 0x38));
    if (rax != 0) {
        goto label_15;
    }
    rax = errno_location ();
    if (*(rax) == 0x4b) {
        goto label_3;
    }
    goto label_4;
label_12:
    eax = *((rsp + 8));
    edx = *((rsp + 0x7f));
    r10 = rbp;
    r13 = *((rsp + 0x60));
    r12 = *((rsp + 0x78));
    eax -= edx;
    rdi = *((rsp + 0x28));
    eax *= 0xe10;
    rsi = r13;
    rax = (int64_t) eax;
    r10 += rax;
    rax = *((rsp + 0x20));
    rax = void (*rax)(uint64_t, uint64_t) (r10, r10);
    if (rax == 0) {
        goto label_5;
    }
    ebx = *((rsp + 0xa0));
    r10 = *((rsp + 8));
    goto label_2;
label_15:
    r12 = *((rsp + 0x78));
    r13 = *((rsp + 0x60));
    r10 = rdx;
    ebx = *((rsp + 0xa0));
    goto label_2;
label_11:
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0xaa50 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0xb140 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x000182d0]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x000182e0]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0xee50 */
 
int64_t dbg_rpl_fcntl (int64_t arg_80h, int64_t arg1, int32_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6, int32_t target) {
    va_list arg;
    int64_t var_ch;
    int64_t var_10h;
    int64_t canary;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r13 = target;
    /* int rpl_fcntl(int fd,int action,va_args ...); */
    *((rsp + 0x40)) = rdx;
    *((rsp + 0x48)) = rcx;
    *((rsp + 0x50)) = r8;
    *((rsp + 0x58)) = r9;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = rsp + 0x80;
    *((rsp + 0x10)) = 0x10;
    *((rsp + 0x18)) = rax;
    rax = rsp + 0x30;
    *((rsp + 0x20)) = rax;
    if (esi == 0) {
        goto label_6;
    }
    if (esi == 0x406) {
        goto label_7;
    }
    if (esi > 0xb) {
        goto label_8;
    }
    if (esi > 0) {
        goto label_9;
    }
    do {
label_0:
        eax = *((rsp + 0x10));
        if (eax > 0x2f) {
            goto label_10;
        }
        rax += *((rsp + 0x20));
label_5:
        rdx = *(rax);
        eax = 0;
        eax = fcntl ();
        r12d = eax;
        goto label_1;
label_8:
        ecx = section__dynsym;
    } while (ecx > 0xa);
    eax = 1;
    rax <<= cl;
    if ((eax & 0x2c5) != 0) {
        goto label_11;
    }
    if ((eax & 0x502) == 0) {
        goto label_0;
    }
    do {
        eax = 0;
        eax = fcntl ();
        r12d = eax;
        goto label_1;
label_9:
        eax = 1;
        ecx = esi;
        rax <<= cl;
        if ((eax & 0x514) != 0) {
            goto label_11;
        }
    } while ((eax & 0xa0a) != 0);
    goto label_0;
label_6:
    rax = *((rsp + 0x20));
    esi = 0;
    edx = *((rax + 0x10));
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    do {
label_1:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_12;
        }
        eax = r12d;
        return rax;
label_7:
        rax = *((rsp + 0x20));
        *((rsp + 0x10)) = 0x18;
        r13d = *((rax + 0x10));
        eax = have_dupfd_cloexec.0;
        edx = r13d;
        if (eax < 0) {
            goto label_13;
        }
        esi = 0x406;
        eax = 0;
        *((rsp + 0xc)) = edi;
        eax = fcntl ();
        r12d = eax;
        if (eax < 0) {
            goto label_14;
        }
label_2:
        *(obj.have_dupfd_cloexec.0) = 1;
    } while (1);
label_11:
    eax = *((rsp + 0x10));
    if (eax > 0x2f) {
        goto label_15;
    }
    rax += *((rsp + 0x20));
label_4:
    edx = *(rax);
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    goto label_1;
label_13:
    esi = 0;
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    if (eax < 0) {
        goto label_1;
    }
    if (*(obj.have_dupfd_cloexec.0) != 0xffffffff) {
        goto label_1;
    }
label_3:
    eax = 0;
    esi = 1;
    edi = r12d;
    eax = fcntl ();
    if (eax < 0) {
        goto label_16;
    }
    eax |= 1;
    esi = 2;
    edi = r12d;
    edx = eax;
    eax = 0;
    eax = fcntl ();
    if (eax != 0xffffffff) {
        goto label_1;
    }
label_16:
    rax = errno_location ();
    r12d |= 0xffffffff;
    r13d = *(rax);
    close (r12d);
    *(rbp) = r13d;
    goto label_1;
label_14:
    rax = errno_location ();
    edi = *((rsp + 0xc));
    if (*(rax) != 0x16) {
        goto label_2;
    }
    edx = r13d;
    esi = 0;
    eax = 0;
    eax = fcntl ();
    r12d = eax;
    if (eax < 0) {
        goto label_1;
    }
    *(obj.have_dupfd_cloexec.0) = 0xffffffff;
    goto label_3;
label_15:
    rax = *((rsp + 0x18));
    goto label_4;
label_10:
    rax = *((rsp + 0x18));
    goto label_5;
label_12:
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0x6970 */
 
int32_t dbg_strmode (char * bp, int32_t mode) {
    rsi = bp;
    rdi = mode;
    /* void strmode(mode_t mode,char * str); */
    rdx = rsi;
    esi = edi;
    eax = edi;
    ecx = 0x2d;
    esi &= 0xf000;
    if (esi != 0x8000) {
        ecx = 0x64;
        if (esi == 0x4000) {
            goto label_1;
        }
        ecx = 0x62;
        if (esi == 0x6000) {
            goto label_1;
        }
        ecx = 0x63;
        if (esi == sym._init) {
            goto label_1;
        }
        ecx = 0x6c;
        if (esi == 0xa000) {
            goto label_1;
        }
        ecx = 0x70;
        if (esi == 0x1000) {
            goto label_1;
        }
        ecx = 0x73;
        esi = 0x3f;
        if (esi == 0xc000) {
            ecx = esi;
            goto label_1;
        }
    }
label_1:
    *(rdx) = cl;
    ecx = eax;
    ecx &= 0x100;
    ecx -= ecx;
    ecx &= 0xffffffbb;
    ecx += 0x72;
    *((rdx + 1)) = cl;
    ecx = eax;
    ecx &= 0x80;
    ecx -= ecx;
    ecx &= 0xffffffb6;
    ecx += 0x77;
    *((rdx + 2)) = cl;
    ecx = eax;
    ecx &= 0x40;
    ecx -= ecx;
    if ((ah & 8) == 0) {
        goto label_2;
    }
    ecx &= 0xffffffe0;
    ecx += 0x73;
    do {
        *((rdx + 3)) = cl;
        ecx = eax;
        ecx &= 0x20;
        ecx -= ecx;
        ecx &= 0xffffffbb;
        ecx += 0x72;
        *((rdx + 4)) = cl;
        ecx = eax;
        ecx &= 0x10;
        ecx -= ecx;
        ecx &= 0xffffffb6;
        ecx += 0x77;
        *((rdx + 5)) = cl;
        ecx = eax;
        ecx &= 8;
        ecx -= ecx;
        if ((ah & 4) == 0) {
            goto label_3;
        }
        ecx &= 0xffffffe0;
        ecx += 0x73;
label_0:
        *((rdx + 6)) = cl;
        ecx = eax;
        ecx &= 4;
        ecx -= ecx;
        ecx &= 0xffffffbb;
        ecx += 0x72;
        *((rdx + 7)) = cl;
        ecx = eax;
        ecx &= 2;
        ecx -= ecx;
        ecx &= 0xffffffb6;
        ecx += 0x77;
        *((rdx + 8)) = cl;
        ecx = eax;
        ecx &= 1;
        if ((ah & 2) == 0) {
            goto label_4;
        }
        eax -= eax;
        eax &= 0xffffffe0;
        eax += 0x74;
        *((rdx + 9)) = al;
        eax = 0x20;
        *((rdx + 0xa)) = ax;
        return eax;
label_2:
        ecx &= 0xffffffb5;
        ecx += 0x78;
    } while (1);
label_4:
    eax -= eax;
    eax &= 0xffffffb5;
    eax += 0x78;
    *((rdx + 9)) = al;
    eax = 0x20;
    *((rdx + 0xa)) = ax;
    return eax;
label_3:
    ecx &= 0xffffffb5;
    ecx += 0x78;
    goto label_0;
}

/* /tmp/tmprn8a__c4 @ 0xaf70 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x2abf)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0xae50 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x2ab5)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0xdcf0 */
 
int64_t dbg_rpl_vasprintf (int64_t arg1, int64_t arg2, int64_t arg3) {
    size_t length;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_vasprintf(char ** resultp,char const * format,__va_list_tag * args); */
    r8 = rsi;
    rbx = rdi;
    rcx = rdx;
    edi = 0;
    rdx = r8;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rsi = rsp;
    rax = vasnprintf ();
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = *(rsp);
    if (rax > 0x7fffffff) {
        goto label_2;
    }
    *(rbx) = rdi;
    do {
label_0:
        rdx = *((rsp + 8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_3;
        }
        return rax;
label_2:
        free (rdi);
        errno_location ();
        *(rax) = 0x4b;
        eax = 0xffffffff;
    } while (1);
label_1:
    eax = 0xffffffff;
    goto label_0;
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0xb5e0 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmprn8a__c4 @ 0xaaf0 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x2aaa)() ();
    }
    if (rdx == 0) {
        void (*0x2aaa)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0xb000 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x000182d0]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x000182e0]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0xc740 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprn8a__c4 @ 0xc7c0 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprn8a__c4 @ 0x6880 */
 
int32_t dbg_file_type (uint32_t arg1) {
    rdi = arg1;
    /* char const * file_type(stat const * st); */
    eax = *((rdi + 0x18));
    edx = 5;
    eax &= 0xf000;
    if (eax != 0x8000) {
        if (eax == 0x4000) {
            goto label_0;
        }
        if (eax == 0xa000) {
            goto label_1;
        }
        if (eax == 0x6000) {
            goto label_2;
        }
        if (eax == sym._init) {
            goto label_3;
        }
        if (eax == 0x1000) {
            goto label_4;
        }
        if (eax == 0xc000) {
            goto label_5;
        }
        rsi = "weird file";
        edi = 0;
        void (*0x26c0)() ();
    }
    if (*((rdi + 0x30)) == 0) {
        rsi = "regular empty file";
        edi = 0;
        void (*0x26c0)() ();
    }
    rsi = "regular file";
    edi = 0;
    void (*0x26c0)() ();
label_3:
    rsi = "character special file";
    edi = 0;
    void (*0x26c0)() ();
label_0:
    rsi = 0x00012adb;
    edi = 0;
    void (*0x26c0)() ();
label_5:
    rsi = "socket";
    edi = 0;
    void (*0x26c0)() ();
label_1:
    rsi = "symbolic link";
    edi = 0;
    void (*0x26c0)() ();
label_2:
    rsi = "block special file";
    edi = 0;
    void (*0x26c0)() ();
label_4:
    rsi = "fifo";
    edi = 0;
    return dcgettext ();
}

/* /tmp/tmprn8a__c4 @ 0xad50 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmprn8a__c4 @ 0xc460 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmprn8a__c4 @ 0xdc30 */
 
int64_t dbg_xvasprintf (int64_t arg1, int64_t arg2, int64_t arg7) {
    char * result;
    int64_t var_8h;
    int64_t var_8h_2;
    int64_t var_10h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    /* char * xvasprintf(char const * format,__va_list_tag * args); */
    r8 = rdi;
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    eax = *(rdi);
    edi = 0;
    if (al != 0) {
        goto label_0;
    }
    goto label_1;
    do {
        if (*((r8 + rdi*2 + 1)) != 0x73) {
            goto label_2;
        }
        rdi++;
        eax = *((r8 + rdi*2));
        if (al == 0) {
            goto label_1;
        }
label_0:
    } while (al == 0x25);
label_2:
    eax = rpl_vasprintf (rsp, r8, rdx);
    r8d = eax;
    rax = *(rsp);
    while (1) {
        rdx = *((rsp + 8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_3;
        }
        return rax;
        rax = errno_location ();
        if (*(rax) == 0xc) {
            goto label_4;
        }
        eax = 0;
    }
label_1:
    rax = *((rsp + 8));
    rax -= *(fs:0x28);
    if (rax == 0) {
        rsi = rdx;
        void (*0xdad0)() ();
label_4:
        xalloc_die ();
    }
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0xd920 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x29c0)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmprn8a__c4 @ 0xaa90 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0xc640 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmprn8a__c4 @ 0xbb00 */
 
int64_t dbg_mktime_z (int64_t arg_8h, int64_t arg_10h, int64_t arg_20h, int64_t arg_30h, tm * arg1, int64_t arg2) {
    tm tm_1;
    int64_t var_114h_2;
    int64_t var_8h_2;
    int64_t var_114h;
    int64_t var_100h;
    int64_t var_1ch;
    int64_t var_20h_2;
    int64_t var_118h;
    int64_t var_30h_2;
    int64_t var_38h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h_2;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_78h;
    int64_t var_7fh;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_a0h;
    int64_t var_ach;
    int64_t var_b8h;
    int64_t var_b0h;
    int64_t var_d4h_2;
    int64_t var_d4h;
    int64_t var_c0h;
    int64_t var_d0h;
    int64_t var_e0h;
    int64_t var_ech;
    int64_t var_f8h;
    rdi = arg1;
    rsi = arg2;
    /* time_t mktime_z(timezone_t tz,tm * tm); */
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (rdi == 0) {
        goto label_2;
    }
    r14 = rdi;
    rax = set_tz (rdi);
    r12 = rax;
    if (rax == 0) {
        goto label_1;
    }
    rax = *(rbp);
    r15 = rsp;
    *((rsp + 0x1c)) = 0xffffffff;
    *(rsp) = rax;
    rax = *((rbp + 8));
    *((rsp + 8)) = rax;
    rax = *((rbp + 0x10));
    *((rsp + 0x10)) = rax;
    eax = *((rbp + 0x20));
    *((rsp + 0x20)) = eax;
    rax = rpl_mktime (r15);
    r13 = rax;
    eax = *((rsp + 0x1c));
    while (al == 0) {
        if (r12 != 1) {
            rdi = r12;
            revert_tz_part_0 ();
        }
label_1:
        r13 = 0xffffffffffffffff;
label_0:
        rax = *((rsp + 0x38));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        rax = r13;
        return rax;
        al = save_abbr (r14, r15, rdx, rcx, r8);
    }
    while (al != 0) {
        __asm ("movdqa xmm0, xmmword [rsp]");
        __asm ("movdqa xmm1, xmmword [rsp + 0x10]");
        __asm ("movdqa xmm2, xmmword [rsp + 0x20]");
        rax = *((rsp + 0x30));
        __asm ("movups xmmword [rbp], xmm0");
        *((rbp + 0x30)) = rax;
        __asm ("movups xmmword [rbp + 0x10], xmm1");
        __asm ("movups xmmword [rbp + 0x20], xmm2");
        goto label_0;
        rdi = r12;
        al = revert_tz_part_0 ();
    }
    goto label_1;
label_2:
    rax = *((rsp + 0x38));
    rax -= *(fs:0x28);
    if (rax == 0) {
        rdi = rsi;
        void (*0xbc50)() ();
    }
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0xc280 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmprn8a__c4 @ 0x2850 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmprn8a__c4 @ 0x6690 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmprn8a__c4 @ 0xd750 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmprn8a__c4 @ 0x2620 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmprn8a__c4 @ 0xbc70 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = 0x000132e8;
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x000132fb);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x000135e8;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0x135e8 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x2a20)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2a20)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2a20)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmprn8a__c4 @ 0xf2f0 */
 
int64_t dbg_printf_parse (int64_t arg1, int64_t arg2, int64_t arg3, size_t sum) {
    int64_t var_1h;
    int64_t var_4ch;
    int64_t var_30h;
    int64_t var_25h;
    int64_t var_bp_20h;
    int64_t var_8h;
    int64_t var_10h;
    void * s2;
    int64_t var_20h;
    void ** var_28h;
    void ** var_sp_30h;
    void ** var_38h;
    void ** var_40h;
    int64_t var_48h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r9 = sum;
    /* int printf_parse(char const * format,char_directives * d,arguments * a); */
    r10 = rsi + 0x20;
    rax = rdi;
    rdi = rdx + 0x10;
    r15 = rdx;
    r14 = rsi;
    rcx = r10;
    r9d = 7;
    r13d = 0;
    r11d = 7;
    *(rsi) = 0;
    *((rsi + 8)) = r10;
    *((rsp + 0x18)) = rdi;
    *(rdx) = 0;
    *((rdx + 8)) = rdi;
    *((rsp + 0x10)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x20)) = 0;
    while (dl != 0) {
        rbx = rax + 1;
        if (dl == 0x25) {
            goto label_34;
        }
label_4:
        rax = rbx;
        edx = *(rax);
    }
    rdx = r13 * 5;
    rdx = r13 + rdx*2;
    *((rcx + rdx*8)) = rax;
    rax = *((rsp + 8));
    *((r14 + 0x10)) = rax;
    rax = *((rsp + 0x10));
    *((r14 + 0x18)) = rax;
    eax = 0;
label_7:
    return rax;
label_34:
    rdx = r13 * 5;
    rdx = r13 + rdx*2;
    r13 = 0xffffffffffffffff;
    r12 = rcx + rdx*8;
    *(r12) = rax;
    *((r12 + 0x10)) = 0;
    *((r12 + 0x18)) = 0;
    *((r12 + 0x20)) = 0;
    *((r12 + 0x28)) = 0xffffffffffffffff;
    *((r12 + 0x30)) = 0;
    *((r12 + 0x38)) = 0;
    *((r12 + 0x40)) = 0xffffffffffffffff;
    *((r12 + 0x50)) = 0xffffffffffffffff;
    ebp = *((rax + 1));
    edx = rbp - 0x30;
    if (dl <= 9) {
        goto label_35;
    }
label_5:
    rcx = 0x0001382c;
    rdx = rbx + 1;
    if (bpl == 0x27) {
        goto label_36;
    }
    do {
        eax = rbp - 0x20;
        if (al <= 0x29) {
            eax = (int32_t) al;
            rax = *((rcx + rax*4));
            rax += rcx;
            /* switch table (42 cases) at 0x1382c */
            void (*rax)() ();
        }
        if (bpl == 0x2a) {
            goto label_37;
        }
        eax = rbp - 0x30;
        if (al <= 9) {
            goto label_38;
        }
label_17:
        if (bpl == 0x2e) {
            goto label_39;
        }
label_3:
        edx = 0;
        rsi = 0x000138d4;
        edi = 1;
        rbx++;
        if (bpl == 0x68) {
            goto label_40;
        }
label_1:
        eax = rbp - 0x4c;
        if (al > 0x2e) {
            goto label_41;
        }
        eax = (int32_t) al;
        rax = *((rsi + rax*4));
        rax += rsi;
        /* switch table (47 cases) at 0x138d4 */
        void (*rax)() ();
        eax = *((r12 + 0x10));
        eax |= 0x40;
label_0:
        *((r12 + 0x10)) = eax;
        ebp = *(rdx);
        rbx = rdx;
        rdx = rbx + 1;
    } while (bpl != 0x27);
label_36:
    eax = *((r12 + 0x10));
    eax |= 1;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 0x20;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 2;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 4;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 0x10;
    goto label_0;
    eax = *((r12 + 0x10));
    eax |= 8;
    goto label_0;
label_41:
    eax = rbp - 0x25;
    if (al > 0x53) {
        goto label_10;
    }
    rcx = 0x00013990;
    eax = (int32_t) al;
    rax = *((rcx + rax*4));
    rax += rcx;
    /* switch table (84 cases) at 0x13990 */
    void (*rax)() ();
    edx += 8;
label_2:
    ebp = *(rbx);
    rbx++;
    if (bpl != 0x68) {
        goto label_1;
    }
label_40:
    ecx = edx;
    eax = edi;
    ecx &= 1;
    eax <<= cl;
    edx |= eax;
    goto label_2;
label_37:
    rdi = *((rsp + 8));
    eax = 1;
    *((r12 + 0x18)) = rbx;
    *((r12 + 0x20)) = rdx;
    ecx = *((rbx + 1));
    if (rdi != 0) {
        rax = rdi;
    }
    *((rsp + 8)) = rax;
    eax = rcx - 0x30;
    if (al <= 9) {
        goto label_42;
    }
label_9:
    rdi = *((rsp + 0x20));
    *((r12 + 0x28)) = rdi;
    rax = rdi + 1;
    if (rdi == -1) {
        goto label_10;
    }
    rbp = *((rsp + 0x20));
    *((rsp + 0x20)) = rax;
    rbx = rdx;
label_29:
    rdx = *((r15 + 8));
    r8 = *((r15 + 8));
    if (r9 <= rbp) {
        r9 += r9;
        rax = rbp + 1;
        if (r9 <= rbp) {
            r9 = rax;
        }
        rax = r9;
        rax >>= 0x3b;
        if (rax != 0) {
            goto label_30;
        }
        rsi = r9;
        rsi <<= 5;
        if (*((rsp + 0x18)) == rdx) {
            goto label_43;
        }
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r11;
        *((rsp + 0x28)) = r10;
        rax = realloc (rdx, rsi);
        rdx = *((r15 + 8));
        r10 = *((rsp + 0x28));
        r11 = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r8 = rax;
label_20:
        if (r8 == 0) {
            goto label_30;
        }
        if (*((rsp + 0x18)) == rdx) {
            goto label_44;
        }
label_26:
        *((r15 + 8)) = r8;
    }
    rdx = *(r15);
    rax = *(r15);
    rax <<= 5;
    rax += r8;
    if (rdx > rbp) {
        goto label_45;
    }
    do {
        rdx++;
        *(rax) = 0;
        rcx = rax;
        rax += 0x20;
    } while (rdx <= rbp);
    *(r15) = rdx;
    *(rcx) = 0;
label_45:
    rbp <<= 5;
    rbp += r8;
    eax = *(rbp);
    if (eax != 0) {
        goto label_46;
    }
    *(rbp) = 5;
    ebp = *(rbx);
    if (bpl != 0x2e) {
        goto label_3;
    }
label_39:
    if (*((rbx + 1)) != 0x2a) {
        goto label_47;
    }
    rdi = *((rsp + 0x10));
    eax = 2;
    rcx = rbx + 2;
    *((r12 + 0x30)) = rbx;
    *((r12 + 0x38)) = rcx;
    esi = *((rbx + 2));
    if (rdi >= rax) {
        rax = rdi;
    }
    *((rsp + 0x10)) = rax;
    eax = rsi - 0x30;
    if (al <= 9) {
        goto label_48;
    }
label_22:
    rbx = *((r12 + 0x40));
    if (rbx == -1) {
        goto label_49;
    }
label_21:
    r8 = *((r15 + 8));
    if (r9 <= rbx) {
        goto label_50;
    }
label_19:
    rdx = *(r15);
    rax = *(r15);
    rax <<= 5;
    rax += r8;
    if (rdx > rbx) {
        goto label_51;
    }
    do {
        rdx++;
        *(rax) = 0;
        rsi = rax;
        rax += 0x20;
    } while (rdx <= rbx);
    *(r15) = rdx;
    *(rsi) = 0;
label_51:
    rbx <<= 5;
    rax = r8 + rbx;
    edx = *(rax);
    if (edx != 0) {
        goto label_52;
    }
    *(rax) = 5;
    rbx = rcx;
    ebp = *(rcx);
    goto label_3;
    edx |= 4;
    goto label_2;
    ecx = 0;
    cl = (edx > 7) ? 1 : 0;
    ecx += 0xf;
label_12:
    if (r13 == -1) {
        goto label_53;
    }
    *((r12 + 0x50)) = r13;
label_16:
    r8 = *((r15 + 8));
    if (r9 <= r13) {
        goto label_54;
    }
label_15:
    rdx = *(r15);
    rax = *(r15);
    rax <<= 5;
    rax += r8;
    if (rdx > r13) {
        goto label_55;
    }
    do {
        rdx++;
        *(rax) = 0;
        rsi = rax;
        rax += 0x20;
    } while (rdx <= r13);
    *(r15) = rdx;
    *(rsi) = 0;
label_55:
    r13 <<= 5;
    r13 += r8;
    eax = *(r13);
    if (eax != 0) {
        goto label_56;
    }
    *(r13) = ecx;
label_13:
    *((r12 + 0x48)) = bpl;
    rax = *(r14);
    *((r12 + 8)) = rbx;
    r13 = rax + 1;
    *(r14) = r13;
    if (r11 > r13) {
        rcx = *((r14 + 8));
        goto label_4;
    }
    if (r11 < 0) {
        goto label_57;
    }
    rax = 0x2e8ba2e8ba2e8ba;
    r12 = r11 + r11;
    if (r12 > rax) {
        goto label_57;
    }
    rax = r11 * 5;
    rbp = *((r14 + 8));
    *((rsp + 0x30)) = r9;
    rsi = r11 + rax*2;
    *((rsp + 0x28)) = r10;
    rsi <<= 4;
    if (r10 == rbp) {
        goto label_58;
    }
    rax = realloc (rbp, rsi);
    r10 = *((rsp + 0x28));
    r9 = *((rsp + 0x30));
    rcx = rax;
    if (rax == 0) {
        goto label_57;
    }
    rbp = *((r14 + 8));
    if (r10 == rbp) {
        goto label_59;
    }
label_8:
    *((r14 + 8)) = rcx;
    r13 = *(r14);
    r11 = r12;
    goto label_4;
label_35:
    rdx = rbx;
    do {
        ecx = *((rdx + 1));
        rdx++;
        esi = rcx - 0x30;
    } while (sil <= 9);
    r13 = 0xffffffffffffffff;
    if (cl != 0x24) {
        goto label_5;
    }
    rax += 2;
    edi = 0;
    while (rsi >= 0) {
        if (dl > 9) {
            goto label_60;
        }
        rax++;
        edx = rbp - 0x30;
        rcx = rax - 1;
        rsi = 0x1999999999999999;
        rdx = (int64_t) dl;
        if (rdi > rsi) {
            goto label_61;
        }
        rsi = rdi * 5;
        rsi += rsi;
label_6:
        ebp = *(rax);
        rsi += rdx;
        rdi = rsi;
        edx = rbp - 0x30;
    }
    if (dl <= 9) {
        rcx = rax;
        rdx = (int64_t) dl;
        rax++;
        rsi = 0xffffffffffffffff;
        goto label_6;
    }
label_10:
    r8 = *((r15 + 8));
label_14:
    if (*((rsp + 0x18)) != r8) {
        *((rsp + 8)) = r10;
        free (r8);
        r10 = *((rsp + 8));
    }
    rdi = *((r14 + 8));
    if (r10 != rdi) {
        free (rdi);
    }
    errno_location ();
    *(rax) = 0x16;
    eax = 0xffffffff;
    goto label_7;
label_47:
    *((r12 + 0x30)) = rbx;
    eax = *((rbx + 1));
    rdx = rbx + 1;
    eax -= 0x30;
    if (al > 9) {
        goto label_62;
    }
    do {
        eax = *((rdx + 1));
        rdx++;
        eax -= 0x30;
    } while (al <= 9);
    rax = rdx;
    rax -= rbx;
    rbx = rdx;
label_28:
    rdi = *((rsp + 0x10));
    *((r12 + 0x38)) = rdx;
    ebp = *(rdx);
    if (rdi >= rax) {
        rax = rdi;
    }
    *((rsp + 0x10)) = rax;
    goto label_3;
label_58:
    rax = malloc (rsi);
    r10 = *((rsp + 0x28));
    r9 = *((rsp + 0x30));
    rcx = rax;
    if (rax == 0) {
        goto label_63;
    }
label_33:
    rax = r13 * 5;
    *((rsp + 0x30)) = r9;
    *((rsp + 0x28)) = r10;
    rdx <<= 3;
    rax = memcpy (rcx, rbp, r13 + rax*2);
    r9 = *((rsp + 0x30));
    r10 = *((rsp + 0x28));
    rcx = rax;
    goto label_8;
label_42:
    rax = rdx;
    do {
        esi = *((rax + 1));
        rax++;
        edi = rsi - 0x30;
    } while (dil <= 9);
    if (sil != 0x24) {
        goto label_9;
    }
    rbx += 2;
    esi = 0;
    while (rcx >= 0) {
        if (al > 9) {
            goto label_64;
        }
        rbx++;
        eax = rcx - 0x30;
        rdx = rbx - 1;
        rdi = 0x1999999999999999;
        rax = (int64_t) al;
        if (rsi > rdi) {
            goto label_65;
        }
        rcx = rsi * 5;
        rcx += rcx;
label_11:
        rcx += rax;
        rsi = rcx;
        ecx = *(rbx);
        eax = rcx - 0x30;
    }
    if (al > 9) {
        goto label_10;
    }
    rdx = rbx;
    rax = (int64_t) al;
    rbx++;
    rcx = 0xffffffffffffffff;
    goto label_11;
    ecx = 0xc;
    if (edx > 0xf) {
        goto label_12;
    }
    ecx = 0;
    edx &= 4;
    cl = (edx != 0) ? 1 : 0;
    ecx += 0xb;
    goto label_12;
    ecx = 0xa;
    if (edx > 0xf) {
        goto label_12;
    }
    if ((dl & 4) != 0) {
        goto label_12;
    }
    ecx = 8;
    if (edx > 7) {
        goto label_12;
    }
    ecx = 2;
    if ((dl & 2) != 0) {
        goto label_12;
    }
    edx &= 1;
    ecx -= ecx;
    ecx &= 2;
    ecx += 4;
    goto label_12;
    ecx = 9;
    if (edx > 0xf) {
        goto label_12;
    }
    if ((dl & 4) != 0) {
        goto label_12;
    }
    ecx = 7;
    if (edx > 7) {
        goto label_12;
    }
    ecx = 1;
    if ((dl & 2) != 0) {
        goto label_12;
    }
    edx &= 1;
    ecx -= ecx;
    ecx &= 2;
    ecx += 3;
    goto label_12;
    ecx = 0x16;
    if (edx > 0xf) {
        goto label_12;
    }
    if ((dl & 4) != 0) {
        goto label_12;
    }
    ecx = 0x15;
    if (edx > 7) {
        goto label_12;
    }
    ecx = 0x12;
    if ((dl & 2) != 0) {
        goto label_12;
    }
    edx &= 1;
    ecx = 0x14;
    ecx -= edx;
    goto label_12;
    ecx = 0;
    cl = (edx > 7) ? 1 : 0;
    ecx += 0xd;
    goto label_12;
    ecx = 0xe;
    goto label_12;
label_56:
    if (eax == ecx) {
        goto label_13;
    }
    goto label_14;
label_54:
    r9 += r9;
    rax = r13 + 1;
    if (r9 <= r13) {
        r9 = rax;
    }
    rax = r9;
    rax >>= 0x3b;
    if (rax != 0) {
        goto label_66;
    }
    rsi = r9;
    rsi <<= 5;
    if (*((rsp + 0x18)) == r8) {
        goto label_67;
    }
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = ecx;
    rax = realloc (r8, rsi);
    ecx = *((rsp + 0x28));
    r10 = *((rsp + 0x30));
    r11 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    r8 = rax;
    if (rax == 0) {
        goto label_57;
    }
    rax = *((rsp + 0x18));
    if (rax == *((r15 + 8))) {
        goto label_68;
    }
label_18:
    *((r15 + 8)) = r8;
    goto label_15;
label_53:
    rdi = *((rsp + 0x20));
    *((r12 + 0x50)) = rdi;
    rax = rdi + 1;
    if (rdi == -1) {
        goto label_10;
    }
    r13 = *((rsp + 0x20));
    *((rsp + 0x20)) = rax;
    goto label_16;
label_38:
    *((r12 + 0x18)) = rbx;
    eax = *(rbx);
    eax -= 0x30;
    if (al > 9) {
        goto label_69;
    }
    rdx = rbx;
    do {
        eax = *((rdx + 1));
        rdx++;
        eax -= 0x30;
    } while (al <= 9);
    rdi = *((rsp + 8));
    rax = rdx;
    rax -= rbx;
    rbx = rdx;
    if (rdi >= rax) {
        rax = rdi;
    }
    *((rsp + 8)) = rax;
label_69:
    *((r12 + 0x20)) = rbx;
    do {
        ebp = *(rbx);
        goto label_17;
label_46:
    } while (eax == 5);
    goto label_14;
label_67:
    *((rsp + 0x48)) = r8;
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = ecx;
    rax = malloc (rsi);
    ecx = *((rsp + 0x28));
    r10 = *((rsp + 0x30));
    r11 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    rdi = rax;
    r8 = *((rsp + 0x48));
    if (rax != 0) {
label_31:
        *((rsp + 0x40)) = r9;
        *((rsp + 0x38)) = r11;
        rdx <<= 5;
        *((rsp + 0x30)) = r10;
        *((rsp + 0x28)) = ecx;
        rax = memcpy (rdi, r8, *(r15));
        r9 = *((rsp + 0x40));
        r11 = *((rsp + 0x38));
        r10 = *((rsp + 0x30));
        ecx = *((rsp + 0x28));
        r8 = rax;
        goto label_18;
label_63:
        rdx = *((r15 + 8));
        if (*((rsp + 0x18)) == rdx) {
            goto label_70;
        }
label_24:
        *((rsp + 8)) = r10;
        free (rdx);
        r10 = *((rsp + 8));
    }
label_25:
    rdi = *((r14 + 8));
    if (r10 != rdi) {
        free (rdi);
    }
label_70:
    errno_location ();
    *(rax) = 0xc;
    eax = 0xffffffff;
    return rax;
label_50:
    r9 += r9;
    rax = rbx + 1;
    if (r9 <= rbx) {
        r9 = rax;
    }
    rax = r9;
    rax >>= 0x3b;
    if (rax != 0) {
        goto label_66;
    }
    rsi = r9;
    rsi <<= 5;
    if (*((rsp + 0x18)) == r8) {
        goto label_71;
    }
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = rcx;
    rax = realloc (r8, rsi);
    rcx = *((rsp + 0x28));
    r10 = *((rsp + 0x30));
    r11 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    r8 = rax;
    if (rax == 0) {
        goto label_57;
    }
    rax = *((rsp + 0x18));
    if (rax == *((r15 + 8))) {
        goto label_72;
    }
label_27:
    *((r15 + 8)) = r8;
    goto label_19;
label_52:
    if (edx != 5) {
        goto label_14;
    }
    ebp = *(rcx);
    rbx = rcx;
    goto label_3;
label_43:
    *((rsp + 0x40)) = rdx;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x28)) = r10;
    rax = malloc (rsi);
    rdx = *((rsp + 0x40));
    r9 = *((rsp + 0x38));
    r11 = *((rsp + 0x30));
    r10 = *((rsp + 0x28));
    r8 = rax;
    goto label_20;
    ecx = 0x11;
    goto label_12;
    ecx = 0x10;
    goto label_12;
label_49:
    rdi = *((rsp + 0x20));
    *((r12 + 0x40)) = rdi;
    rax = rdi + 1;
    if (rdi == -1) {
        goto label_10;
    }
    rbx = *((rsp + 0x20));
    *((rsp + 0x20)) = rax;
    goto label_21;
label_48:
    rax = rcx;
    do {
        edx = *((rax + 1));
        rax++;
        edi = rdx - 0x30;
    } while (dil <= 9);
    if (dl != 0x24) {
        goto label_22;
    }
    rbx += 3;
    edi = 0;
    while (rdx >= 0) {
        if (al > 9) {
            goto label_73;
        }
        rbx++;
        eax = rsi - 0x30;
        rcx = rbx - 1;
        rsi = 0x1999999999999999;
        rax = (int64_t) al;
        if (rdi > rsi) {
            goto label_74;
        }
        rdx = rdi * 5;
        rdx += rdx;
label_23:
        esi = *(rbx);
        rdx += rax;
        rdi = rdx;
        eax = rsi - 0x30;
    }
    if (al > 9) {
        goto label_10;
    }
    rcx = rbx;
    rax = (int64_t) al;
    rbx++;
    rdx = 0xffffffffffffffff;
    goto label_23;
label_61:
    rsi = 0xffffffffffffffff;
    goto label_6;
label_60:
    r13 = rsi;
    r13--;
    if (r13 > 0xfffffffffffffffd) {
        goto label_10;
    }
    ebp = *((rcx + 2));
    rbx = rcx + 2;
    goto label_5;
label_57:
    rdx = *((r15 + 8));
label_30:
    if (*((rsp + 0x18)) != rdx) {
        goto label_24;
    }
    goto label_25;
label_44:
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r11;
    rdx <<= 5;
    *((rsp + 0x28)) = r10;
    rax = memcpy (r8, *((rsp + 0x18)), *(r15));
    r9 = *((rsp + 0x38));
    r11 = *((rsp + 0x30));
    r10 = *((rsp + 0x28));
    r8 = rax;
    goto label_26;
label_71:
    *((rsp + 0x48)) = r8;
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = rcx;
    rax = malloc (rsi);
    rcx = *((rsp + 0x28));
    r10 = *((rsp + 0x30));
    r11 = *((rsp + 0x38));
    r9 = *((rsp + 0x40));
    rdi = rax;
    r8 = *((rsp + 0x48));
    if (rax == 0) {
        goto label_25;
    }
label_32:
    *((rsp + 0x40)) = r9;
    *((rsp + 0x38)) = r11;
    rdx <<= 5;
    *((rsp + 0x30)) = r10;
    *((rsp + 0x28)) = rcx;
    rax = memcpy (rdi, r8, *(r15));
    r9 = *((rsp + 0x40));
    r11 = *((rsp + 0x38));
    r10 = *((rsp + 0x30));
    rcx = *((rsp + 0x28));
    r8 = rax;
    goto label_27;
label_65:
    rcx = 0xffffffffffffffff;
    goto label_11;
label_62:
    rbx = rdx;
    eax = 1;
    goto label_28;
label_64:
    rbp--;
    if (rbp > 0xfffffffffffffffd) {
        goto label_10;
    }
    *((r12 + 0x28)) = rbp;
    rbx = rdx + 2;
    goto label_29;
label_74:
    rdx = 0xffffffffffffffff;
    goto label_23;
label_73:
    rbx = rdx - 1;
    if (rbx > 0xfffffffffffffffd) {
        goto label_10;
    }
    *((r12 + 0x40)) = rbx;
    rcx += 2;
    goto label_21;
label_66:
    rdx = r8;
    goto label_30;
label_68:
    rdi = r8;
    r8 = rax;
    goto label_31;
label_72:
    rdi = r8;
    r8 = rax;
    goto label_32;
label_59:
    r13 = *(r14);
    goto label_33;
}

/* /tmp/tmprn8a__c4 @ 0xda70 */
 
uint64_t dbg_fd_safer (uint32_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* int fd_safer(int fd); */
    r12d = edi;
    if (edi > 2) {
        eax = r12d;
        return eax;
    }
    eax = dup_safer (rdi, rsi, rdx, rcx, r8);
    rax = errno_location ();
    r12d = ebp;
    r13d = *(rax);
    rbx = rax;
    close (r12d);
    eax = r12d;
    *(rbx) = r13d;
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0xc100 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprn8a__c4 @ 0x2000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0xc7a0 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprn8a__c4 @ 0xdea0 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmprn8a__c4 @ 0xc4a0 */
 
uint64_t dbg_xnmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprn8a__c4 @ 0xbc50 */
 
void dbg_rpl_timegm (int64_t arg1, mktime_offset_t gmtime_offset) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_4ch;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_78h;
    int64_t var_7fh;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_a0h;
    int64_t var_ach;
    int64_t var_b8h;
    int64_t var_b0h;
    int64_t var_d4h_2;
    int64_t var_d4h;
    int64_t var_c0h;
    int64_t var_d0h;
    int64_t var_e0h;
    int64_t var_ech;
    int64_t var_f8h;
    int64_t var_114h_2;
    int64_t var_114h;
    int64_t var_100h;
    int64_t var_118h;
    rdi = arg1;
    xmm1 = gmtime_offset;
    /* time_t rpl_timegm(tm * tmp); */
    *((rdi + 0x20)) = 0;
    rsi = *(reloc.gmtime_r);
    rdx = obj_gmtime_offset_0;
    return void (*0xcd10)() ();
}

/* /tmp/tmprn8a__c4 @ 0xc920 */
 
uint64_t dbg_xgetcwd (int64_t arg_e0h) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_60h;
    int64_t var_70h;
    int64_t var_80h;
    int64_t var_90h;
    int64_t var_a0h;
    int64_t var_b0h;
    int64_t var_c0h;
    /* char * xgetcwd(); */
    esi = 0;
    edi = 0;
    rax = getcwd ();
    r12 = rax;
    while (*(rax) != 0xc) {
        rax = r12;
        return rax;
        rax = errno_location ();
    }
    return xalloc_die ();
}

/* /tmp/tmprn8a__c4 @ 0xc860 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi + 1);
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2880)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmprn8a__c4 @ 0x6630 */
 
uint64_t dbg_argmatch_to_argument (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char const * argmatch_to_argument( const * value,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = *(rsi);
    if (r14 == 0) {
        goto label_0;
    }
    r12 = rdi;
    r13 = rcx;
    rbx = rsi + 8;
    while (eax != 0) {
        r14 = *(rbx);
        rbp += r13;
        rbx += 8;
        if (r14 == 0) {
            goto label_0;
        }
        eax = memcmp (r12, rbp, r13);
    }
label_0:
    rax = r14;
    return rax;
}

/* /tmp/tmprn8a__c4 @ 0x2580 */
 
void ctype_toupper_loc (void) {
    /* [15] -r-x section size 1312 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmprn8a__c4 @ 0x2590 */
 
void getenv (void) {
    __asm ("bnd jmp qword [reloc.getenv]");
}

/* /tmp/tmprn8a__c4 @ 0x25a0 */
 
void snprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__snprintf_chk]");
}

/* /tmp/tmprn8a__c4 @ 0x25c0 */
 
void endmntent (void) {
    __asm ("bnd jmp qword [reloc.endmntent]");
}

/* /tmp/tmprn8a__c4 @ 0x0 */
 
int64_t libc_start_main (int32_t argc, func init, func main, char ** ubp_av) {
    rsi = argc;
    rcx = init;
    rdi = main;
    rdx = ubp_av;
    bh &= *(rdi);
    *((rdi + riz + 0x5c)) ^= bl;
    *((rcx + rsi)) ^= esi;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdi) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
}

/* /tmp/tmprn8a__c4 @ 0x2600 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmprn8a__c4 @ 0x2610 */
 
void strcpy (void) {
    __asm ("bnd jmp qword [reloc.strcpy]");
}

/* /tmp/tmprn8a__c4 @ 0x2640 */
 
void localeconv (void) {
    __asm ("bnd jmp qword [reloc.localeconv]");
}

/* /tmp/tmprn8a__c4 @ 0x2650 */
 
void readlink (void) {
    __asm ("bnd jmp qword [reloc.readlink]");
}

/* /tmp/tmprn8a__c4 @ 0x2660 */
 
void fcntl (void) {
    __asm ("bnd jmp qword [reloc.fcntl]");
}

/* /tmp/tmprn8a__c4 @ 0x2670 */
 
void setenv (void) {
    __asm ("bnd jmp qword [reloc.setenv]");
}

/* /tmp/tmprn8a__c4 @ 0x2680 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmprn8a__c4 @ 0x2690 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmprn8a__c4 @ 0x26a0 */
 
void getpwuid (void) {
    __asm ("bnd jmp qword [reloc.getpwuid]");
}

/* /tmp/tmprn8a__c4 @ 0x26b0 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmprn8a__c4 @ 0x26d0 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmprn8a__c4 @ 0x26f0 */
 
void openat (void) {
    __asm ("bnd jmp qword [reloc.openat]");
}

/* /tmp/tmprn8a__c4 @ 0x2700 */
 
void chdir (void) {
    __asm ("bnd jmp qword [reloc.chdir]");
}

/* /tmp/tmprn8a__c4 @ 0x2720 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmprn8a__c4 @ 0x2730 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmprn8a__c4 @ 0x2740 */
 
void strchr (void) {
    __asm ("bnd jmp qword [reloc.strchr]");
}

/* /tmp/tmprn8a__c4 @ 0x2750 */
 
void getgrgid (void) {
    __asm ("bnd jmp qword [reloc.getgrgid]");
}

/* /tmp/tmprn8a__c4 @ 0x2760 */
 
void overflow (void) {
    __asm ("bnd jmp qword [reloc.__overflow]");
}

/* /tmp/tmprn8a__c4 @ 0x2780 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmprn8a__c4 @ 0x27a0 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmprn8a__c4 @ 0x27c0 */
 
void canonicalize_file_name (void) {
    __asm ("bnd jmp qword [reloc.canonicalize_file_name]");
}

/* /tmp/tmprn8a__c4 @ 0x27f0 */
 
void memchr (void) {
    __asm ("bnd jmp qword [reloc.memchr]");
}

/* /tmp/tmprn8a__c4 @ 0x2800 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmprn8a__c4 @ 0x2830 */
 
void getdelim (void) {
    __asm ("bnd jmp qword [reloc.__getdelim]");
}

/* /tmp/tmprn8a__c4 @ 0x2860 */
 
void stat (void) {
    __asm ("bnd jmp qword [reloc.stat]");
}

/* /tmp/tmprn8a__c4 @ 0x2870 */
 
void strtol (void) {
    __asm ("bnd jmp qword [reloc.strtol]");
}

/* /tmp/tmprn8a__c4 @ 0x2890 */
 
void tzset (void) {
    __asm ("bnd jmp qword [reloc.tzset]");
}

/* /tmp/tmprn8a__c4 @ 0x28a0 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmprn8a__c4 @ 0x28b0 */
 
void statfs (void) {
    __asm ("bnd jmp qword [reloc.statfs]");
}

/* /tmp/tmprn8a__c4 @ 0x28d0 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmprn8a__c4 @ 0x28e0 */
 
void getmntent (void) {
    __asm ("bnd jmp qword [reloc.getmntent]");
}

/* /tmp/tmprn8a__c4 @ 0x28f0 */
 
void setmntent (void) {
    __asm ("bnd jmp qword [reloc.setmntent]");
}

/* /tmp/tmprn8a__c4 @ 0x2910 */
 
void isoc99_sscanf (void) {
    __asm ("bnd jmp qword [reloc.__isoc99_sscanf]");
}

/* /tmp/tmprn8a__c4 @ 0x2920 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmprn8a__c4 @ 0x2930 */
 
void fchdir (void) {
    __asm ("bnd jmp qword [reloc.fchdir]");
}

/* /tmp/tmprn8a__c4 @ 0x2970 */
 
void statx (void) {
    __asm ("bnd jmp qword [reloc.statx]");
}

/* /tmp/tmprn8a__c4 @ 0x2980 */
 
void strftime (void) {
    __asm ("bnd jmp qword [reloc.strftime]");
}

/* /tmp/tmprn8a__c4 @ 0x2990 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmprn8a__c4 @ 0x29a0 */
 
void memrchr (void) {
    __asm ("bnd jmp qword [reloc.memrchr]");
}

/* /tmp/tmprn8a__c4 @ 0x29b0 */
 
void open (void) {
    __asm ("bnd jmp qword [reloc.open]");
}

/* /tmp/tmprn8a__c4 @ 0x29c0 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmprn8a__c4 @ 0x29d0 */
 
void fopen (void) {
    __asm ("bnd jmp qword [reloc.fopen]");
}

/* /tmp/tmprn8a__c4 @ 0x29e0 */
 
void unsetenv (void) {
    __asm ("bnd jmp qword [reloc.unsetenv]");
}

/* /tmp/tmprn8a__c4 @ 0x29f0 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmprn8a__c4 @ 0x2a30 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmprn8a__c4 @ 0x2a40 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmprn8a__c4 @ 0x2a50 */
 
void hasmntopt (void) {
    __asm ("bnd jmp qword [reloc.hasmntopt]");
}

/* /tmp/tmprn8a__c4 @ 0x2a60 */
 
void strstr (void) {
    __asm ("bnd jmp qword [reloc.strstr]");
}

/* /tmp/tmprn8a__c4 @ 0x2a70 */
 
void ctype_tolower_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_tolower_loc]");
}

/* /tmp/tmprn8a__c4 @ 0x2a80 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmprn8a__c4 @ 0x2a90 */
 
void sprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__sprintf_chk]");
}

/* /tmp/tmprn8a__c4 @ 0x2550 */
 
void fcn_00002550 (void) {
    /* [14] -r-x section size 48 named .plt.got */
    __asm ("bnd jmp qword [reloc.localtime_r]");
}

/* /tmp/tmprn8a__c4 @ 0x2030 */
 
void fcn_00002030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 1328 named .plt */
    __asm ("bnd jmp qword [0x00017d18]");
}

/* /tmp/tmprn8a__c4 @ 0x2040 */
 
void fcn_00002040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2050 */
 
void fcn_00002050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2060 */
 
void fcn_00002060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2070 */
 
void fcn_00002070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2080 */
 
void fcn_00002080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2090 */
 
void fcn_00002090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x20a0 */
 
void fcn_000020a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x20b0 */
 
void fcn_000020b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x20c0 */
 
void fcn_000020c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x20d0 */
 
void fcn_000020d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x20e0 */
 
void fcn_000020e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x20f0 */
 
void fcn_000020f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2100 */
 
void fcn_00002100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2110 */
 
void fcn_00002110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2120 */
 
void fcn_00002120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2130 */
 
void fcn_00002130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2140 */
 
void fcn_00002140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2150 */
 
void fcn_00002150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2160 */
 
void fcn_00002160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2170 */
 
void fcn_00002170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2180 */
 
void fcn_00002180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2190 */
 
void fcn_00002190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x21a0 */
 
void fcn_000021a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x21b0 */
 
void fcn_000021b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x21c0 */
 
void fcn_000021c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x21d0 */
 
void fcn_000021d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x21e0 */
 
void fcn_000021e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x21f0 */
 
void fcn_000021f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2200 */
 
void fcn_00002200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2210 */
 
void fcn_00002210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2220 */
 
void fcn_00002220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2230 */
 
void fcn_00002230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2240 */
 
void fcn_00002240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2250 */
 
void fcn_00002250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2260 */
 
void fcn_00002260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2270 */
 
void fcn_00002270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2280 */
 
void fcn_00002280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2290 */
 
void fcn_00002290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x22a0 */
 
void fcn_000022a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x22b0 */
 
void fcn_000022b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x22c0 */
 
void fcn_000022c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x22d0 */
 
void fcn_000022d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x22e0 */
 
void fcn_000022e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x22f0 */
 
void fcn_000022f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2300 */
 
void fcn_00002300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2310 */
 
void fcn_00002310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2320 */
 
void fcn_00002320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2330 */
 
void fcn_00002330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2340 */
 
void fcn_00002340 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2350 */
 
void fcn_00002350 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2360 */
 
void fcn_00002360 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2370 */
 
void fcn_00002370 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2380 */
 
void fcn_00002380 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2390 */
 
void fcn_00002390 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x23a0 */
 
void fcn_000023a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x23b0 */
 
void fcn_000023b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x23c0 */
 
void fcn_000023c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x23d0 */
 
void fcn_000023d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x23e0 */
 
void fcn_000023e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x23f0 */
 
void fcn_000023f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2400 */
 
void fcn_00002400 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2410 */
 
void fcn_00002410 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2420 */
 
void fcn_00002420 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2430 */
 
void fcn_00002430 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2440 */
 
void fcn_00002440 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2450 */
 
void fcn_00002450 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2460 */
 
void fcn_00002460 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2470 */
 
void fcn_00002470 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2480 */
 
void fcn_00002480 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2490 */
 
void fcn_00002490 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x24a0 */
 
void fcn_000024a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x24b0 */
 
void fcn_000024b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x24c0 */
 
void fcn_000024c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x24d0 */
 
void fcn_000024d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x24e0 */
 
void fcn_000024e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x24f0 */
 
void fcn_000024f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2500 */
 
void fcn_00002500 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2510 */
 
void fcn_00002510 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2520 */
 
void fcn_00002520 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2530 */
 
void fcn_00002530 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprn8a__c4 @ 0x2540 */
 
void fcn_00002540 (void) {
    return __asm ("bnd jmp section..plt");
}
