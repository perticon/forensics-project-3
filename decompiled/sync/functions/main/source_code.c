main (int argc, char **argv)
{
  int c;
  bool args_specified;
  bool arg_data = false, arg_file_system = false;
  enum sync_mode mode;
  bool ok = true;

  initialize_main (&argc, &argv);
  set_program_name (argv[0]);
  setlocale (LC_ALL, "");
  bindtextdomain (PACKAGE, LOCALEDIR);
  textdomain (PACKAGE);

  atexit (close_stdout);

  while ((c = getopt_long (argc, argv, "df", long_options, NULL))
         != -1)
    {
      switch (c)
        {
        case 'd':
          arg_data = true;
          break;

        case 'f':
          arg_file_system = true;
          break;

        case_GETOPT_HELP_CHAR;

        case_GETOPT_VERSION_CHAR (PROGRAM_NAME, AUTHORS);

        default:
          usage (EXIT_FAILURE);
        }
    }

  args_specified = optind < argc;

  if (arg_data && arg_file_system)
    {
      die (EXIT_FAILURE, 0,
           _("cannot specify both --data and --file-system"));
    }

  if (!args_specified && arg_data)
    die (EXIT_FAILURE, 0, _("--data needs at least one argument"));

  if (! args_specified || (arg_file_system && ! HAVE_SYNCFS))
    mode = MODE_SYNC;
  else if (arg_file_system)
    mode = MODE_FILE_SYSTEM;
  else if (! arg_data)
    mode = MODE_FILE;
  else
    mode = MODE_DATA;

  if (mode == MODE_SYNC)
    sync ();
  else
    {
      for (; optind < argc; optind++)
        ok &= sync_arg (mode, argv[optind]);
    }

  return ok ? EXIT_SUCCESS : EXIT_FAILURE;
}