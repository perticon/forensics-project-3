uint main(int param_1,undefined8 *param_2)

{
  char *__file;
  bool bVar1;
  int iVar2;
  int iVar3;
  int *piVar4;
  undefined8 uVar5;
  char *pcVar6;
  byte bVar7;
  uint uVar8;
  uint uVar9;
  undefined auVar10 [16];
  undefined8 uVar11;
  undefined8 local_48;
  undefined auStack64 [4];
  uint local_3c;
  
  bVar7 = 0;
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  bVar1 = false;
  textdomain("coreutils");
  atexit(close_stdout);
  while( true ) {
    uVar11 = 0x102721;
    iVar2 = getopt_long(param_1,param_2,&DAT_00107101,long_options,0);
    if (iVar2 == -1) break;
    if (iVar2 == 100) {
LAB_001027b0:
      bVar7 = 1;
    }
    else {
      if (iVar2 < 0x65) {
        if (iVar2 == -0x83) {
          version_etc(stdout,&DAT_00107004,"GNU coreutils",Version,"Jim Meyering",
                      "Giuseppe Scrivano",0,uVar11);
                    /* WARNING: Subroutine does not return */
          exit(0);
        }
        if (iVar2 == -0x82) {
          usage();
          goto LAB_001027b0;
        }
LAB_00102a1f:
        usage(1);
        goto LAB_00102a29;
      }
      if (iVar2 != 0x66) goto LAB_00102a1f;
      bVar1 = true;
    }
  }
  iVar2 = optind;
  if ((bVar7 != 0) && (bVar1)) {
    uVar11 = dcgettext(0,"cannot specify both --data and --file-system",5);
    iVar2 = error(1,0,uVar11);
  }
  if ((param_1 <= iVar2 & bVar7) != 0) {
LAB_00102a29:
    uVar11 = dcgettext(0,"--data needs at least one argument",5);
    auVar10 = error(1,0,uVar11);
    uVar11 = local_48;
    local_48 = SUB168(auVar10,0);
    (*(code *)PTR___libc_start_main_0010afc8)
              (main,uVar11,auStack64,0,0,SUB168(auVar10 >> 0x40,0),&local_48);
    do {
                    /* WARNING: Do nothing block with infinite loop */
    } while( true );
  }
  if (iVar2 < param_1) {
    local_3c = 2;
    if (!bVar1) {
      local_3c = (uint)bVar7;
    }
    uVar8 = 1;
    do {
      __file = (char *)param_2[iVar2];
      iVar2 = open(__file,0x800);
      if (iVar2 < 0) {
        piVar4 = __errno_location();
        iVar3 = *piVar4;
        iVar2 = open(__file,0x801);
        if (-1 < iVar2) goto LAB_00102821;
        uVar11 = quotearg_style(4,__file);
        uVar5 = dcgettext(0,"error opening %s",5);
        uVar9 = 0;
        error(0,iVar3,uVar5,uVar11);
      }
      else {
LAB_00102821:
        uVar9 = rpl_fcntl(iVar2,3);
        if (uVar9 == 0xffffffff) {
LAB_0010292a:
          local_48 = quotearg_style(4,__file);
          pcVar6 = "couldn\'t reset non-blocking mode %s";
LAB_00102947:
          uVar11 = dcgettext(0,pcVar6,5);
          piVar4 = __errno_location();
          uVar9 = 0;
          error(0,*piVar4,uVar11,local_48);
        }
        else {
          iVar3 = rpl_fcntl(iVar2,4,uVar9 & 0xfffff7ff);
          if (iVar3 < 0) goto LAB_0010292a;
          if (local_3c == 1) {
            iVar3 = fdatasync(iVar2);
          }
          else if (local_3c == 2) {
            iVar3 = syncfs();
          }
          else {
            iVar3 = fsync(iVar2);
          }
          uVar9 = 1;
          if (iVar3 < 0) {
            local_48 = quotearg_style(4,__file);
            pcVar6 = "error syncing %s";
            goto LAB_00102947;
          }
        }
        iVar2 = close(iVar2);
        if (iVar2 < 0) {
          uVar11 = quotearg_style(4,__file);
          uVar5 = dcgettext(0,"failed to close %s",5);
          piVar4 = __errno_location();
          uVar9 = 0;
          error(0,*piVar4,uVar5,uVar11);
        }
      }
      uVar8 = uVar8 & uVar9;
      optind = optind + 1;
      iVar2 = optind;
    } while (optind < param_1);
  }
  else {
    sync();
    uVar8 = 1;
  }
  return uVar8 ^ 1;
}