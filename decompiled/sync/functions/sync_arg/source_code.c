sync_arg (enum sync_mode mode, char const *file)
{
  bool ret = true;
  int open_flags = O_RDONLY | O_NONBLOCK;
  int fd;

#if defined _AIX || defined __CYGWIN__
  /* AIX 7.1, CYGWIN 2.9.0, fsync requires write access to file.  */
  if (mode == MODE_FILE)
    open_flags = O_WRONLY | O_NONBLOCK;
#endif

  /* Note O_PATH might be supported with syncfs(),
     though as of Linux 3.18 is not.  */
  fd = open (file, open_flags);
  if (fd < 0)
    {
      /* Use the O_RDONLY errno, which is significant
         with directories for example.  */
      int rd_errno = errno;
      if (open_flags != (O_WRONLY | O_NONBLOCK))
        fd = open (file, O_WRONLY | O_NONBLOCK);
      if (fd < 0)
        {
          error (0, rd_errno, _("error opening %s"), quoteaf (file));
          return false;
        }
    }

  /* We used O_NONBLOCK above to not hang with fifos,
     so reset that here.  */
  int fdflags = fcntl (fd, F_GETFL);
  if (fdflags == -1
      || fcntl (fd, F_SETFL, fdflags & ~O_NONBLOCK) < 0)
    {
      error (0, errno, _("couldn't reset non-blocking mode %s"),
             quoteaf (file));
      ret = false;
    }

  if (ret == true)
    {
      int sync_status = -1;

      switch (mode)
        {
        case MODE_DATA:
          sync_status = fdatasync (fd);
          break;

        case MODE_FILE:
          sync_status = fsync (fd);
          break;

#if HAVE_SYNCFS
        case MODE_FILE_SYSTEM:
          sync_status = syncfs (fd);
          break;
#endif

        default:
          assert ("invalid sync_mode");
        }

      if (sync_status < 0)
        {
          error (0, errno, _("error syncing %s"), quoteaf (file));
          ret = false;
        }
    }

  if (close (fd) < 0)
    {
      error (0, errno, _("failed to close %s"), quoteaf (file));
      ret = false;
    }

  return ret;
}