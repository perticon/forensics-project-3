void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002580(fn00000000000023A0(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l000000000000290E;
	}
	fn0000000000002520(fn00000000000023A0(0x05, "Usage: %s [OPTION] NAME...\n", null), 0x01);
	fn0000000000002450(stdout, fn00000000000023A0(0x05, "Output each NAME with its last non-slash component and trailing slashes\nremoved; if NAME contains no /'s, output '.' (meaning the current directory).\n\n", null));
	fn0000000000002450(stdout, fn00000000000023A0(0x05, "  -z, --zero     end each output line with NUL, not newline\n", null));
	fn0000000000002450(stdout, fn00000000000023A0(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002450(stdout, fn00000000000023A0(0x05, "      --version     output version information and exit\n", null));
	fn0000000000002520(fn00000000000023A0(0x05, "\nExamples:\n  %s /usr/bin/          -> \"/usr\"\n  %s dir1/str dir2/str  -> \"dir1\" followed by \"dir2\"\n  %s stdio.h            -> \".\"\n", null), 0x01);
	struct Eq_640 * rbx_174 = fp - 0xB8 + 16;
	do
	{
		char * rsi_176 = rbx_174->qw0000;
		++rbx_174;
	} while (rsi_176 != null && fn0000000000002470(rsi_176, "dirname") != 0x00);
	ptr64 r13_189 = rbx_174->qw0008;
	if (r13_189 != 0x00)
	{
		fn0000000000002520(fn00000000000023A0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_276 = fn0000000000002510(null, 0x05);
		if (rax_276 == 0x00 || fn0000000000002330(0x03, "en_", rax_276) == 0x00)
			goto l0000000000002B1E;
	}
	else
	{
		fn0000000000002520(fn00000000000023A0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_218 = fn0000000000002510(null, 0x05);
		if (rax_218 == 0x00 || fn0000000000002330(0x03, "en_", rax_218) == 0x00)
		{
			fn0000000000002520(fn00000000000023A0(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000002B5B:
			fn0000000000002520(fn00000000000023A0(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l000000000000290E:
			fn0000000000002560(edi);
		}
		r13_189 = 0x7004;
	}
	fn0000000000002450(stdout, fn00000000000023A0(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000002B1E:
	fn0000000000002520(fn00000000000023A0(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000002B5B;
}