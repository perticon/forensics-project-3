main (int argc, char **argv)
{
  static char const dot = '.';
  bool use_nuls = false;
  char const *result;
  size_t len;

  initialize_main (&argc, &argv);
  set_program_name (argv[0]);
  setlocale (LC_ALL, "");
  bindtextdomain (PACKAGE, LOCALEDIR);
  textdomain (PACKAGE);

  atexit (close_stdout);

  while (true)
    {
      int c = getopt_long (argc, argv, "z", longopts, NULL);

      if (c == -1)
        break;

      switch (c)
        {
        case 'z':
          use_nuls = true;
          break;

        case_GETOPT_HELP_CHAR;
        case_GETOPT_VERSION_CHAR (PROGRAM_NAME, AUTHORS);

        default:
          usage (EXIT_FAILURE);
        }
    }

  if (argc < optind + 1)
    {
      error (0, 0, _("missing operand"));
      usage (EXIT_FAILURE);
    }

  for (; optind < argc; optind++)
    {
      result = argv[optind];
      len = dir_len (result);

      if (! len)
        {
          result = &dot;
          len = 1;
        }

      fwrite (result, 1, len, stdout);
      putchar (use_nuls ? '\0' :'\n');
    }

  return EXIT_SUCCESS;
}