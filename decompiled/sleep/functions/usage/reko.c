void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn00000000000025E0(fn00000000000023F0(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000002A4E;
	}
	fn0000000000002580(fn00000000000023F0(0x05, "Usage: %s NUMBER[SUFFIX]...\n  or:  %s OPTION\nPause for NUMBER seconds.  SUFFIX may be 's' for seconds (the default),\n'm' for minutes, 'h' for hours or 'd' for days.  NUMBER need not be an\ninteger.  Given two or more arguments, pause for the amount of time\nspecified by the sum of their values.\n\n", null), 0x01);
	fn00000000000024B0(stdout, fn00000000000023F0(0x05, "      --help        display this help and exit\n", null));
	fn00000000000024B0(stdout, fn00000000000023F0(0x05, "      --version     output version information and exit\n", null));
	struct Eq_699 * rbx_127 = fp - 0xB8 + 16;
	do
	{
		char * rsi_129 = rbx_127->qw0000;
		++rbx_127;
	} while (rsi_129 != null && fn00000000000024D0(rsi_129, "sleep") != 0x00);
	ptr64 r13_142 = rbx_127->qw0008;
	if (r13_142 != 0x00)
	{
		fn0000000000002580(fn00000000000023F0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_229 = fn0000000000002570(null, 0x05);
		if (rax_229 == 0x00 || fn0000000000002360(0x03, "en_", rax_229) == 0x00)
			goto l0000000000002BE6;
	}
	else
	{
		fn0000000000002580(fn00000000000023F0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_171 = fn0000000000002570(null, 0x05);
		if (rax_171 == 0x00 || fn0000000000002360(0x03, "en_", rax_171) == 0x00)
		{
			fn0000000000002580(fn00000000000023F0(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000002C23:
			fn0000000000002580(fn00000000000023F0(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000002A4E:
			fn00000000000025C0(edi);
		}
		r13_142 = 0x7004;
	}
	fn00000000000024B0(stdout, fn00000000000023F0(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000002BE6:
	fn0000000000002580(fn00000000000023F0(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000002C23;
}