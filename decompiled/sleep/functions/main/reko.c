void main(char ** rsi, int32 edi, struct Eq_359 * fs, word128 xmm0)
{
	ptr64 fp;
	word32 dwLoc38;
	Eq_363 rLoc4C;
	word32 dwLoc44;
	word64 rax_31 = fs->qw0028;
	set_program_name(*rsi);
	fn0000000000002570("", 0x06);
	fn00000000000023E0("/usr/local/share/locale", "coreutils");
	fn00000000000023B0("coreutils");
	atexit(&g_t2DF0);
	parse_gnu_standard_options_only(0x00, 0x7004, rsi, (uint64) edi, 0x01, fs, &g_t29F0);
	word64 qwLoc3C_389 = SEQ(dwLoc38, SLICE(rax_31, word32, 32));
	struct Eq_412 * qwLoc44_446 = SEQ((word32) rax_31, dwLoc44);
	if (edi == 0x01)
	{
		fn0000000000002590(fn00000000000023F0(0x05, "missing operand", null), 0x00, 0x00);
		usage(0x01);
	}
	else
	{
		real64 rLoc64_368;
		int32 eax_103 = g_dwB090;
		if (edi > eax_103)
		{
			int64 rdx_110 = (int64) eax_103;
			byte r14b_248 = 0x01;
			word64 * rbx_116 = (char *) rsi + rdx_110 * 0x08;
			rLoc64_368 = 0.0;
			word64 * rax_122 = (char *) rsi + 8 + ((uint64) ((edi - eax_103) - 0x01) + rdx_110) * 0x08;
			do
			{
				word128 xmm0_157;
				if (xstrtod(&g_t2D10, fp - 0x4C, fp - 0x44, *rbx_116, fs, xmm0) != 0x00 || *fn0000000000002350() == 0x22)
				{
					xmm0_157 = SEQ(0x00, rLoc4C);
					if (rLoc4C < 0.0)
						goto l000000000000277A;
					ci8 al_166 = qwLoc44_446->b0000;
					if (al_166 == 0x00)
						goto l00000000000027AE;
					if (qwLoc44_446->b0001 != 0x00)
						goto l000000000000277A;
					if (al_166 != 0x68)
					{
						if (al_166 <= 0x68)
						{
							if (al_166 != 100)
								goto l000000000000277A;
							xmm0_157 = SEQ(0x00, rLoc4C * 86400.0);
							goto l00000000000027AE;
						}
						if (al_166 == 0x6D)
						{
							xmm0_157 = SEQ(0x00, rLoc4C * 60.0);
							goto l00000000000027AE;
						}
						if (al_166 == 115)
							goto l00000000000027AE;
						goto l000000000000277A;
					}
					xmm0_157 = SEQ(0x00, rLoc4C * 3600.0);
				}
				else
				{
l000000000000277A:
					quote(*rbx_116, fs);
					fn0000000000002590(fn00000000000023F0(0x05, "invalid time interval %s", null), 0x00, 0x00);
					r14b_248 = 0x00;
					xmm0_157 = SEQ(0x00, rLoc4C);
				}
l00000000000027AE:
				real64 v41_240 = (real64) xmm0_157 + rLoc64_368;
				xmm0 = SEQ(SLICE(xmm0_157, word64, 64), v41_240);
				++rbx_116;
				rLoc64_368 = v41_240;
			} while (rbx_116 != rax_122);
			if (r14b_248 == 0x00)
				usage(0x01);
		}
		else
			rLoc64_368 = 0.0;
		if (xnanosleep(fs, SEQ(0x00, rLoc64_368)) != 0x00)
		{
			Eq_26 rax_315 = fn00000000000023F0(0x05, "cannot read realtime clock", null);
			fn0000000000002590(rax_315, *fn0000000000002350(), 0x01);
			_start(rax_315, (word32) rLoc64_368);
		}
		else
		{
			if (qwLoc3C_389 - fs->qw0028 == 0x00)
				return;
			fn0000000000002420();
		}
	}
}