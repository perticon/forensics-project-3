undefined8 main(int param_1,undefined8 *param_2)

{
  bool bVar1;
  char cVar2;
  int iVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  int *piVar6;
  undefined8 *puVar7;
  long in_FS_OFFSET;
  double dVar8;
  undefined auVar9 [16];
  double local_68;
  undefined8 *local_60 [2];
  double local_50;
  char *local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  parse_gnu_standard_options_only
            (param_1,param_2,"sleep","GNU coreutils",Version,1,usage,"Jim Meyering","Paul Eggert",0)
  ;
  if (param_1 == 1) {
LAB_00102896:
    uVar5 = dcgettext(0,"missing operand",5);
    error(0,0,uVar5);
    usage(1);
  }
  else {
    if (optind < param_1) {
      bVar1 = true;
      puVar7 = param_2 + optind;
      local_68 = 0.0;
      local_60[0] = param_2 + (ulong)((param_1 - optind) - 1) + (long)optind + 1;
      do {
        cVar2 = xstrtod(*puVar7,&local_48,&local_50,cl_strtod);
        if (((cVar2 == '\0') && (piVar6 = __errno_location(), *piVar6 != 0x22)) || (local_50 < 0.0))
        {
LAB_0010277a:
          uVar5 = quote(*puVar7);
          uVar4 = dcgettext(0,"invalid time interval %s",5);
          bVar1 = false;
          error(0,0,uVar4,uVar5);
          dVar8 = local_50;
        }
        else {
          cVar2 = *local_48;
          dVar8 = local_50;
          if (cVar2 != '\0') {
            if (local_48[1] != '\0') goto LAB_0010277a;
            if (cVar2 == 'h') {
              dVar8 = local_50 * _DAT_00107398;
            }
            else if (cVar2 < 'i') {
              if (cVar2 != 'd') goto LAB_0010277a;
              dVar8 = local_50 * _DAT_001073a0;
            }
            else if (cVar2 == 'm') {
              dVar8 = local_50 * _DAT_00107390;
            }
            else if (cVar2 != 's') goto LAB_0010277a;
          }
        }
        local_68 = dVar8 + local_68;
        puVar7 = puVar7 + 1;
      } while (puVar7 != local_60[0]);
      if (!bVar1) {
        usage(1);
        goto LAB_00102896;
      }
    }
    else {
      local_68 = 0.0;
    }
    iVar3 = xnanosleep(local_68);
    if (iVar3 != 0) {
      uVar5 = dcgettext(0,"cannot read realtime clock",5);
      piVar6 = __errno_location();
      auVar9 = error(1,*piVar6,uVar5);
      dVar8 = local_68;
      local_68 = SUB168(auVar9,0);
      (*(code *)PTR___libc_start_main_0010afd8)
                (main,dVar8,local_60,0,0,SUB168(auVar9 >> 0x40,0),&local_68);
      do {
                    /* WARNING: Do nothing block with infinite loop */
      } while( true );
    }
    if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
      return 0;
    }
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}