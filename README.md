# Decompiler wars
## Eurecom Forensics course project
### Authors: Capano Francesco, Perticone Davide, Vergari Cosma Alex

## Description
This project is a tool to compare different decompilers performances. In particular, it allows you to compare the functions extracted from the different decompilers. The metrics used are:
- Lines Of Code (LOC)
- McCabes cyclomatic complexity (MVG)
- Lines of Comment (COM)
- Lines of code per line of comment (L_C)
- McCabes cyclomatic complexity per line of comment (M_C)
- Number of gotos (NG)

This project is based on two more projects : 
- MDEC (https://github.com/mborgerson/mdec) : a tool that allows you to decompile code with all decompilers
- cccc (https://github.com/sarnold/cccc) : a tool that allows you to extract different metrics from the source code
- exuberant-ctags (http://ctags.sourceforge.net/) : a tool that parses C/C++ files and highlights the required parts of the source (functions, prototypes, declarations, ...)

We ran this tool with the dataset provided in the repo and the results of our analysis are provided in the file [Report.pdf](./Report.pdf)

## Requirements

In order to run the project you need to install the following tools :
- cccc
```
sudo apt install cccc
```
- mdec (following the instructions on the github page : https://github.com/mborgerson/mdec)
```
COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker compose build \
  frontend \
  angr \
  ghidra \
  r2dec \
  reko \
  retdec \
  snowman
```
- python 3 (the scripts were tested using version 3.8)
- exuberant-ctags (shipped with some Linux distros)

*Note: in this version mdec uses only the free/open source decompilers*

## Usage
The project is composed by three main python scripts :
- `setup.py` : allows you to decompile the binaries and extracts the functions
- `stats.py` : allows you to extract the metrics from the source code of the decompilers
- `analyze.py` : performs some basic calculations on the extracted metrics and shows some plots on the data

The python packages requirements are basic but can be found in the `requirements.txt` file. Then you can also use a conda-environment or python virtual environment.

Before running `setup.py` you need to :
1) start the mdec tool using the following command :
```
docker compose up 
``` 
or
```
docker.compose up
```

2) download the binaries and put them in the `binaries` folder.
3) download the source code of the binaries and put them in the `sources` folder, in particular the source code should be in the `src` folder.

Then you can run the following commands in order:

```Bash
$ python setup.py   # This step takes a while
$ python stats.py
$ python analyze.py
```

**IMPORTANT : the source folder name and the binary name should be the same**



## Folder structure
 ```C
forensics-project/
├─ binaries/                    // folder containing the binaries
│  ├─ binary_file_1             // binary file 
│  ├─ binary_file_n
├─ decompiled/                  // folder containig the results of the decompilation
│  ├─ name_of_binary_file_1/    // folder containing the decompiled code of the binary_file_1
│  │  ├─ functions/             // folder containing the functions extracted from the binary_file_1
│  │  │  ├─ function_1/         // folder containing the function_1 extracted from the different decompilers files
│  │  │  │  ├─ decompiler_1.c   // decompiler_1.c function_1 extracted from the binary_file_1 by the decompiler_1
│  │  │  │  ├─ decompiler_n.c
│  │  │  ├─ function_2/
│  │  │  │  ├─ decompiler_1.c
│  │  │  │  ├─ decompiler_n.c
│  │  ├─ decompiler_1.c         // decompiler_1.c extracted from the binary_file_1 by the decompiler_1
│  │  ├─ decompiler_2.c
│  │  ├─ decomplier_n.c
│  ├─ name_of_binary_file_n/
├─ source/                      // folder containing the source code of the binaries
│  ├─ name_of_binary_file_1/
│  │  ├─ src/                   // folder containing the source code of the binary_file_1
│  ├─ name_of_binary_file_n/
├─ stats/                       // folder containing the results of the stats.py script
│  ├─ name_of_binary_file_1/    // folder containing the results of the stats.py script for the binary_file_1
│  │  ├─ functions/             // folder containing the results of the stats.py script for the functions extracted from the binary_file_1
│  │  │  ├─ function_1/         
│  │  │  │  ├─ decompiler_1/    // folder containing the results of the stats.py (using cccc) script for the function_1 extracted from the binary_file_1 by the decompiler_1
│  │  │  │  │  ├─ anonymous.xml // file by cccc script containing the metrics of the function_1 (metrics used in the db)
│  │  │  │  │  ├─ cccc.xml      // file by cccc script containing the metrics of the function_1 (general overwiew of the metrics)
│  │  │  │  ├─ decompiler_n/
|  │  ├─ decompiler_1/
|  │  │  ├─ anonymous.xml
|  │  │  ├─ cccc.xml
│  |  ├─ decompiler_n/
|  │  │  ├─ anonymous.xml
|  │  │  ├─ cccc.xml
│  ├─ name_of_binary_file_n/
│  ├─ db.json                   // file containing the metrics of the functions extracted from the binaries
│  ├─ errors.json               // file containing the errors that occured during the metrics extraction by cccc
├─ .gitignore
├─ requirements.txt
├─ analyze.py                   // script to perform the analysis on the extracted metrics
├─ setup.py                     // script to decompile the binaries and extract the functions
├─ stats.py                     // script to extract the metrics from the source code of the decompilers
├─ README.md
```

## Database structure
The database is structured as a json file, and it is composed by the following fiels:
```C
{
    progname: {
        function_name: {
            decompiler_name: {
                LOC: int,
                MVG: int,
                COM: int,
                L_C: float,
                M_C: float,
                goto: int
            }
        }
    }
}
```

### Additional information

The errors.json, which contains the errors that occured during the metrics extraction by cccc, is structured as a json file, and it is composed by the following fields:
```C
{
    progname: {
        function_name: {
            decompiler_name: {
                error: str      // error message from CCCC
            }
        }
    }
}
```
