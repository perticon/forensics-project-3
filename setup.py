from itertools import count
import os
from multiprocessing import Process
import requests
import subprocess

"""
    Script to extract the functions from the decompiled files
    It uses the ctags command to extract the functions names and lines from the decompiled files
    It uses the mdec services to decompile the binaries
"""

decompilers = ['ghidra', 'angr', 'r2dec', 'reko', 'retdec', 'snowman', 'jeb']
binary_base_path = "binaries"
decompiled_base_path = "decompiled"
source_code_path = "source"



def create_dir(filenames):
    """
    Create the directories for the function extraction from the decompiled files
    :param filenames: list of filenames
    :return: list of filenames that are not analysed
    Note: if the directory of the decompiled file already exists, the decompiled file is not analysed again
    """
    not_analysed = list()
    for filename in filenames:
        if not os.path.isdir(f"{decompiled_base_path}/{filename}"):
            print("Processing:",filename)
            os.makedirs(f"decompiled/{filename}/functions", exist_ok=True)
            not_analysed.append(filename)
    return not_analysed


def extract_names():
    """
    Extract the name of the binaries from the binaries directory
    """
    return os.listdir(binary_base_path)


def _decompile(decompiler, filename):
    """
    Send the request to the mdec services and save the decompiled file in the decompiled directory
    :param decompiler: name of the decompiler
    :param filename: name of the binary
    """
    with open(f"{binary_base_path}/{filename}", 'rb') as f:
        print(f"Using {decompiler}")
        r = requests.post(f'http://127.0.0.1/{decompiler}/decompile', files={'file': f})

        if r.status_code != 200:
            print(f"Error in {decompiler}")
            return

        f = open(f'{decompiled_base_path}/{filename}/{decompiler}.c', "w")
        f.write(r.text)
        f.close()
        print(f"{decompiler} finished")


def decompile_files(filenames):
    """
    Decompile the binaries using mdec services 
    It sents different requests in parallel to the different decompilers
    Each request is sent by the _decompile function
    :param filenames: list of filenames
    """
    # number of binaries analyzed per time is limited because of the number 
    # of requests sent to the mdec services at the same time
    max_running_processes = 2
    cnt_proc = 0
    proc_list = list()
    for filename in filenames:
        cnt_proc += 1
        print("Decompiling: ", filename)
        for decompiler in decompilers:
            p = Process(target=_decompile, args=(decompiler, filename))
            proc_list.append(p)
            p.start()
        if cnt_proc == max_running_processes:
            cnt_proc = 0
            for p in proc_list:
                p.join()
            proc_list = list()


def _write_extracted_function(source_lines, line, function_name, filename, source):
    """
    Write the function found in the decompiled/source file in the functions directory
    :param source_lines: list of lines of the source file
    :param line: line number of the function in the source file
    :param function_name: name of the function
    :param filename: name of the binary
    :param source: name of the decompiler
    """
    i = line - 1
    funct_body = ""
    while i < len(source_lines) and not source_lines[i].startswith("}"):
        funct_body += source_lines[i]
        i += 1
    funct_body += "}"
    f = open(f'{decompiled_base_path}/{filename}/functions/{function_name}/{source}.c', "w")
    f.write(funct_body)
    f.close()


def _extract_function_decompiled(filename, function):
    """
    Extract the function from the decompiled file
    :param filename: name of the binary
    :param function: name of the function
    """
    for decompiler in decompilers:
        # ctage extract the function names and lines form the decompiled files | grep the function name of our interest
        ctag_output = subprocess.getoutput(
            f"ctags -x --c-types=f {decompiled_base_path}/{filename}/{decompiler}.c | grep {function}")
        # case of error
        if ctag_output.startswith('ctags') or len(ctag_output) == 0:
            continue

        # parse the ctag output to extract the function name and line number
        for ctag_function in ctag_output.splitlines():     
            # there could exist some functions that contains the function name
            line, name = int(ctag_function.split()[2]), ctag_function.split()[0]
            if name == function:
                break
        if name != function:
            continue

        # write the function to be extracted from the decompiled file
        f = open(f"{decompiled_base_path}/{filename}/{decompiler}.c")
        lines = f.readlines()
        _write_extracted_function(lines, line, name, filename, decompiler)


def extract_functions(prognames):
    """
    Extract the functions from the decompiled files
    It proceeds as follows:
    For each file in the source directory (.c ot .cpp files)
        1. It extracts the functions from the source files
        2. Write the functions found in the source files in the functions directory (using _write_extracted_function)
        3. It extracts the functions from the decompiled files (using _extract_function_decompiled))
    :param prognames: list of filenames 
    """
    for progname in prognames:
        print("Extracting functions from: ",progname)

        for root, _, files in os.walk(fr"{source_code_path}/{progname}/"):
            for file in files:
                if file.endswith(".c") or file.endswith(".cpp") or file.endswith(".cc"):
                    filename = os.path.join(root, file)
                    ctag_output = subprocess.getoutput(f"ctags -x --c-types=f {filename}").splitlines()

                    # creates a map of the functions found in the source file
                    # in particular for each function found by ctags, it stores the line number in the source file
                    functions_map = sorted([(int(line.split()[2]), line.split()[0]) for line in ctag_output])
                    print(f"Extracting {len(functions_map)} function{'s' if len(functions_map) > 1 else ''} from {filename}")
                    
                    # read the source file
                    f = open(f"{filename}", "r")
                    source_lines = f.readlines()

                    for line, function_name in functions_map:
                        # creates the directory for the extracted function
                        os.makedirs(f"{decompiled_base_path}/{progname}/functions/{function_name}", exist_ok=True)
                        # write the function from the source code in the functions directory
                        _write_extracted_function(source_lines, line, function_name, progname, "source_code")
                        # extract the function from the decompiled file
                        _extract_function_decompiled(progname, function_name)


def main():
    # extract the names of the binaries
    filenames = extract_names()
    # create_dir created the directories for the binaries to be decompiled
    # returns only the filenames of the binary not analysed
    filenames = create_dir(filenames)
    # decompile the binaries
    decompile_files(filenames)
    # extract the functions from the decompiled files
    extract_functions(filenames)


if __name__ == '__main__':
    main()
